package com.localvalu.common.ui

import android.content.Context
import android.widget.EditText
import android.widget.TextView
import android.widget.TimePicker
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatEditText
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import com.google.android.material.textfield.TextInputLayout
import com.localvalu.*
import com.localvalu.base.MainActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.time.format.DateTimeFormatter
import java.util.*
import androidx.test.espresso.Espresso.onView

import androidx.test.espresso.matcher.ViewMatchers.withTagValue
import org.hamcrest.Matchers
import java.time.LocalTime
import androidx.test.espresso.matcher.ViewMatchers.withClassName

import androidx.test.espresso.matcher.ViewMatchers.isDisplayed

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import com.google.android.material.textfield.TextInputEditText
import com.localvalu.common.repository.InsertLitterRepository
import com.localvalu.common.viewmodel.FakeInsertLitterApi
import com.localvalu.common.viewmodel.InsertLitterViewModel
import com.localvalu.extensions.convertDateTimeToStringInDisplayFormat
import java.text.SimpleDateFormat
import kotlin.math.min
import androidx.test.espresso.IdlingResource
import com.localvalu.util.DataBindingIdlingResource
import com.localvalu.util.EspressoIdingResource
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import androidx.test.espresso.matcher.ViewMatchers.withClassName

import androidx.test.espresso.matcher.ViewMatchers.isDisplayed

import androidx.test.espresso.Espresso.onView
import com.google.android.material.chip.Chip
import androidx.test.espresso.Espresso.onView
import com.google.android.material.timepicker.MaterialTimePicker


@ExperimentalCoroutinesApi
@HiltAndroidTest
class TestInsertLitterFragmentTest
{
    @get:Rule
    val hiltRule = HiltAndroidRule(this)

    @get:Rule
    var instantTaskExecutor = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    lateinit var context:Context

    private lateinit var  insertLitterViewModel: InsertLitterViewModel

    private lateinit var fakeInsertLitterApi: FakeInsertLitterApi

    // An Idling Resource that waits for Data Binding to have no pending bindings
    private val dataBindingIdlingResource = DataBindingIdlingResource()


    @Before
    fun setUp()
    {
        hiltRule.inject()
        context = ApplicationProvider.getApplicationContext()
        fakeInsertLitterApi = FakeInsertLitterApi()
        fakeInsertLitterApi.setSuccess(true)
        context = ApplicationProvider.getApplicationContext() as Context;
        insertLitterViewModel = InsertLitterViewModel(context!! ,InsertLitterRepository(fakeInsertLitterApi))
    }

    /**
     * Idling resources tell Espresso that the app is idle or busy. This is needed when operations
     * are not scheduled in the main Looper (for example when executed on a different thread).
     */
    @Before
    fun registerIdlingResource() {
        //IdlingRegistry.getInstance().register(EspressoIdingResource.countingIdlingResource)
        //IdlingRegistry.getInstance().register(dataBindingIdlingResource)
    }

    @Test
    fun isAllDataVisible()
    {
        val scenario = launchFragmentInHiltContainer<TestInsertLitterFragment>()
        isVisible(R.id.tilStartTime)
        isVisible(R.id.tilEndTime)
        isVisible(R.id.tilPostCode)
        isVisible(R.id.btnInsertLitterSubmit)
    }

    private fun isVisible(id:Int)
    {
        Espresso.onView(ViewMatchers.withId(id))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun givenPostCode_SeesInCorrectErrorMessage()
    {
        val scenario = launchFragmentInHiltContainer<TestInsertLitterFragment>()
        typeTheText(R.id.etPostCode,"L1 88")
        //onView(isRoot()).perform(waitFor(5000))
        checkErrorMessage(R.id.tilPostCode,context?.getString(R.string.msg_enter_valid_post_code))
    }

    @Test
    fun givenPostCode_SeesEmptyMessage()
    {
        val scenario = launchFragmentInHiltContainer<TestInsertLitterFragment>()
        typeTheText(R.id.etPostCode,"")
        checkErrorMessage(R.id.tilPostCode,context?.getString(R.string.msg_enter_post_code))
    }

    @Test
    fun givenPostCode_SeesNoErrorMessage()
    {
        val scenario = launchFragmentInHiltContainer<TestInsertLitterFragment>()
        typeTheText(R.id.etPostCode,"L1 8JQ")
        checkNoErrorMessage(R.id.tilPostCode)
    }

    @Test
    fun startTimeShouldNotShowError()
    {
        val scenario = launchFragmentInHiltContainer<TestInsertLitterFragment>()
        val calendarStart = Calendar.getInstance()
        val calendarEnd = Calendar.getInstance()
        calendarStart.add(Calendar.HOUR,-2)
        val simpleHourFormat = SimpleDateFormat("HH");
        val simpleMinuteFormat = SimpleDateFormat("mm");

        var strStartHour = simpleHourFormat.format(calendarStart.time)
        var startHour = strStartHour.toInt()
        var strStartMinute = simpleMinuteFormat.format(calendarStart.time)
        var startMinute = strStartMinute.toInt()

        var strEndHour = simpleHourFormat.format(calendarStart.time)
        var endHour = strStartHour.toInt()
        var strEndMinute = simpleMinuteFormat.format(calendarStart.time)
        var endMinute = strStartMinute.toInt()
        //insertLitterViewModel.setTime(false,hour =endHour,minute = endMinute)
        insertLitterViewModel.setTime(true,hour =startHour,minute = startMinute)

        onView(withId(R.id.etStartTime)).perform(replaceText(calendarStart.time.convertDateTimeToStringInDisplayFormat()))
        checkNoErrorMessage(R.id.tilStartTime)
    }

    @Test
    fun startTimeShouldShowError() {
        val scenario = launchFragmentInHiltContainer<TestInsertLitterFragment>()
        val calendarStart = Calendar.getInstance()
        calendarStart.add(Calendar.HOUR,+1)
        val simpleHourFormat = SimpleDateFormat("HH");
        val simpleMinuteFormat = SimpleDateFormat("mm");

        var strStartHour = simpleHourFormat.format(calendarStart.time)
        var startHour = strStartHour.toInt()
        var strStartMinute = simpleMinuteFormat.format(calendarStart.time)
        var startMinute = strStartMinute.toInt()
        //clickIt(R.id.etStartTime)
        //onView(withIndex(withId(com.google.android.material.R.id.material_timepicker_edit_text),0)).perform(click())
        /*onView(withId(com.google.android.material.R.id.material_timepicker_mode_button)).perform(
            click()
        )*/
        //onView(withIndex(withId(com.google.android.material.R.id.material_timepicker_edit_text),0)).perform(click())
        /*onView(
            allOf(
                isDisplayed(), withClassName(
                    `is`(
                        AppCompatEditText::class.java.name
                    )
                )
            )
        ).perform(replaceText(strStartHour))
        onView(withId(com.google.android.material.R.id.material_timepicker_edit_text)).perform(click())
        onView(
            allOf(
                isDisplayed(), withClassName(
                    `is`(
                        AppCompatEditText::class.java.name
                    )
                )
            )
        ).perform(replaceText(strStartMinute))*/
        //insertLitterViewModel.setTime(true,hour =startHour,minute = startMinute)
        //checkErrorMessage(R.id.tilStartTime,context.getString(R.string.msg_start_time_should_not_be_future_time))
    }

    private fun typeTheText(id:Int, strText:String)
    {
        onView(withId(id)).perform(replaceText(strText))
    }

    private fun clickIt(id:Int)
    {
        onView(withId(id)).perform(click())
    }

    private fun checkErrorMessage(viewId:Int,message:String)
    {
        onView(withId(viewId)).check(matches(hasTextInputLayoutErrorText(message)))
    }

    private fun checkNoErrorMessage(viewId:Int)
    {
        onView(withId(viewId)).check(matches(hasNoErrorText()))
    }
}