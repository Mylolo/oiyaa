package com.localvalu.common.viewmodel

import com.localvalu.common.api.InsertLitterApi
import com.localvalu.common.model.ErrorDetails
import com.localvalu.common.model.litter.InsertLitterDetails
import com.localvalu.common.model.litter.InsertLitterRequest
import com.localvalu.common.model.litter.InsertLitterResponse
import com.localvalu.common.model.litter.InsertLitterResult

class FakeInsertLitterApi : InsertLitterApi
{
    private var success:Boolean = false;

    private fun getInsertLitterResponse() : InsertLitterResponse
    {
        val insertLitterResult:InsertLitterResult = InsertLitterResult(getSuccessOrError(success),"1.00")
        val insertLitterDetails: InsertLitterDetails = InsertLitterDetails(insertLitterResult)
        return InsertLitterResponse(insertLitterDetails)
    }

    private fun getSuccessOrError(success:Boolean) : ErrorDetails
    {
        return if(success)
        {
            val errorDetails = ErrorDetails()
            errorDetails.errorCode=0
            errorDetails.errorMessage="SuccessMessage"
            errorDetails
        }
        else
        {
            val errorDetails = ErrorDetails()
            errorDetails.errorCode=1
            errorDetails.errorMessage="ErrorMessage"
            errorDetails
        }
    }

    override suspend fun insertLitter(insertLitterRequest: InsertLitterRequest): InsertLitterResponse
    {
        return getInsertLitterResponse()
    }

    fun setSuccess(value: Boolean)
    {
        this.success = value
    }

}