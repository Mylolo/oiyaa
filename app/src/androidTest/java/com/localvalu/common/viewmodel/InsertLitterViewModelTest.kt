package com.localvalu.common.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import com.localvalu.base.Resource
import com.localvalu.common.model.litter.InsertLitterRequest
import com.localvalu.common.model.litter.InsertLitterResponse
import com.localvalu.common.repository.InsertLitterRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.ResponseBody
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import androidx.test.core.app.ApplicationProvider
import com.localvalu.MainCoroutineRule
import com.localvalu.R
import com.localvalu.getOrAwaitValueTest
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Assert.assertEquals
import java.util.*

@ExperimentalCoroutinesApi
class InsertLitterViewModelTest
{
    @get:Rule
    var instantTaskExecutor = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var  insertLitterViewModel:InsertLitterViewModel

    private lateinit var fakeInsertLitterApi: FakeInsertLitterApi

    private var context:Context?=null

    @Before
    fun setUp()
    {
        fakeInsertLitterApi = FakeInsertLitterApi()
        fakeInsertLitterApi.setSuccess(true)
        context = ApplicationProvider.getApplicationContext() as Context;
        insertLitterViewModel = InsertLitterViewModel(context!! ,InsertLitterRepository(fakeInsertLitterApi))
    }

    @Test
    fun testInsertLitterCorrectData()
    {
        val insertLitterRequest = InsertLitterRequest("","","2022-12-23 17:00","2022-12-23 17:00","L1 8JQ")
        insertLitterViewModel.insertLitter(insertLitterRequest)

        when(val response: Resource<InsertLitterResponse> = insertLitterViewModel.insertLitterResponse.getOrAwaitValueTest())
        {
            is Resource.Success ->
            {
                MatcherAssert.assertThat(
                    response.value.insertLitterDetails.insertLitterResult.errorDetails.errorCode.toString(),
                    CoreMatchers.`is`("0")
                )
            }
            else->
            {

            }
        }
    }

    @Test
    fun testInsertLitterInCorrectData()
    {
        fakeInsertLitterApi.setSuccess(false)

        val insertLitterRequest = InsertLitterRequest("","","2022-12-23 17:00","2022-12-23 17:00","L1 8JQ")
        insertLitterViewModel.insertLitter(insertLitterRequest)

        when(val response: Resource<InsertLitterResponse> = insertLitterViewModel.insertLitterResponse.getOrAwaitValueTest())
        {
            is Resource.Success ->
            {
                MatcherAssert.assertThat(
                    response.value.insertLitterDetails.insertLitterResult.errorDetails.errorCode.toString(),
                    CoreMatchers.`is`("1")
                )
            }
            else->
            {

            }
        }
    }

    @Test
    fun testInsertLitterReceivesErrorData()
    {
        val insertLitterRequest = InsertLitterRequest("","","2022-12-23 17:00","2022-12-23 17:00","L1 8JQ")
        insertLitterViewModel.insertLitter(insertLitterRequest)

        val JSON = "application/json; charset=utf-8".toMediaTypeOrNull()
        val body: ResponseBody = ResponseBody.create(JSON, "json")
        val response: Resource<InsertLitterResponse> = Resource.Failure(true,400,body)

        when(response)
        {
            is Resource.Success ->
            {
                MatcherAssert.assertThat(
                    response.value.insertLitterDetails.insertLitterResult.errorDetails.errorCode.toString(),
                    CoreMatchers.`is`("1")
                )
            }
            is Resource.Failure->
            {
                MatcherAssert.assertThat(response.isNetworkError, CoreMatchers.`is`(true))
            }
        }
    }

    @Test
    fun testStartTimeAsFutureTime()
    {
        val calendar:Calendar = Calendar.getInstance()
        calendar.add(Calendar.MINUTE,5);
        insertLitterViewModel.setStartTime(calendar.time)
        insertLitterViewModel.validateStartTime()
        val result: Boolean = insertLitterViewModel.validStartTime.getOrAwaitValueTest()
        assertThat(result.toString(),true)
        val message: String = insertLitterViewModel.observeShowStartTimeError.getOrAwaitValueTest()
        assertEquals(message,context!!.getString(R.string.msg_start_time_should_not_be_future_time))
    }

    @Test
    fun testStartTimeAndEndTimeAsCurrentTime()
    {
        val calendar:Calendar = Calendar.getInstance()
        insertLitterViewModel.setStartTime(calendar.time)
        insertLitterViewModel.setEndTime(calendar.time)
        insertLitterViewModel.validateStartTime()
        val result = insertLitterViewModel.validStartTime.getOrAwaitValueTest()
        assertThat(result.toString(),true)
        val message: String = insertLitterViewModel.observeShowStartTimeError.getOrAwaitValueTest()
        assertEquals(message,context!!.getString(R.string.msg_end_time_start_time_should_not_be_same))
    }

    @Test
    fun testStartTimeAfterEndTime()
    {
        val calendar:Calendar = Calendar.getInstance()
        calendar.add(Calendar.HOUR,-1)
        insertLitterViewModel.setStartTime(calendar.time)
        calendar.add(Calendar.HOUR,-2)
        insertLitterViewModel.setEndTime(calendar.time)
        insertLitterViewModel.validateStartTime()
        val result: Boolean = insertLitterViewModel.validStartTime.getOrAwaitValueTest()
        assertThat(result.toString(),true)
        val message: String = insertLitterViewModel.observeShowStartTimeError.getOrAwaitValueTest()
        assertEquals(message,context!!.getString(R.string.msg_start_time_should_be_less_time))
    }

    @Test
    fun testEndTimeAsFutureTime()
    {
        val calendar:Calendar = Calendar.getInstance()
        calendar.add(Calendar.MINUTE,5);
        insertLitterViewModel.setEndTime(calendar.time)
        insertLitterViewModel.validateEndTime()
        val result: Boolean = insertLitterViewModel.validEndTime.getOrAwaitValueTest()
        assertThat(result.toString(),true)
        val message: String = insertLitterViewModel.observeShowEndTimeError.getOrAwaitValueTest()
        assertEquals(message,context!!.getString(R.string.msg_end_time_should_not_be_future_time))
    }

    @Test
    fun testEndTimeAsBeforeStartTime()
    {
        val calendar:Calendar = Calendar.getInstance()
        insertLitterViewModel.setStartTime(calendar.time)
        calendar.add(Calendar.MINUTE,-10);
        insertLitterViewModel.setEndTime(calendar.time)
        insertLitterViewModel.validateEndTime()
        val result: Boolean = insertLitterViewModel.validEndTime.getOrAwaitValueTest()
        assertThat(result.toString(),true)
        val message: String = insertLitterViewModel.observeShowEndTimeError.getOrAwaitValueTest()
        assertEquals(message,context!!.getString(R.string.msg_end_time_should_not_be_less_start_time))
    }


}