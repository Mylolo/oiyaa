package com.imageCaptured;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.localvalu.R;
import com.utility.AlertDialogCallBack;
import com.utility.CommonUtility;
import com.utility.DialogUtility;
import com.utility.MarshMallowPermission;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

public class PhotoSelectionDialog extends Activity {//.event.dialog.PhotoSelectionDialog
    public final static int STILL_IMAGE = 1;
    private static final int PICTURE_GALLERY_REQUEST = 1111;
    private static final int CAMERA_PIC_REQUEST = 2222;
    private final int COMPRESS = 100;
    public String picturePath = "";
    public int mediaType = 0;
//    private Uri uri;
    private boolean isBigLogo = false;
    private ArrayList<String> pathArray;

    private File temp_dir;
    private File temp_path;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        getWindow().getDecorView().findViewById(android.R.id.content).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        setContentView(R.layout.option_dialogue);
        MarshMallowPermission marshMallowPermission = new MarshMallowPermission(this);
        if (!marshMallowPermission.checkPermissionForExternalStorage())
            marshMallowPermission.requestPermissionForExternalStorage();
        readFromBundle();
        initView();
    }

    private void readFromBundle() {
        try {
            mediaType = getIntent().getIntExtra("type", 1);
            System.out.println("mediaType " + mediaType);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startImageCaptured() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_PIC_REQUEST);
    }

    private void initView() {
        temp_dir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.app_name));
        if (!temp_dir.exists()) {
            temp_dir.mkdirs();
        }
        temp_path = new File(temp_dir, System.currentTimeMillis() + ".jpg");
        if (temp_path.exists())
            temp_path.delete();
    }


    public void onButtonClick(View view) {
        switch (view.getId()) {
            case R.id.btnChoosePicLibrary:
                if (mediaType == STILL_IMAGE) {
                    loadImageFromAlbum();
                }
                break;
            case R.id.btnChoosePicCamera:
                if (mediaType == STILL_IMAGE)
                    startImageCaptured();
                break;
            case R.id.rl_root:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.btnChoosePicCancel:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
    }

    private void loadImageFromAlbum() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICTURE_GALLERY_REQUEST);
    }

    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == RESULT_OK) {
                switch (requestCode) {
                    case CAMERA_PIC_REQUEST:
                        if (data != null) {
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            System.out.println("photo " + photo);

                            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                            Uri tempUri = getImageUri(this, photo);

                            // CALL THIS METHOD TO GET THE ACTUAL PATH
                            File finalFile = new File(getRealPathFromURI(tempUri));

                            System.out.println("finalFile " + finalFile);

                            finishAndSetResult(RESULT_OK, getRealPathFromURI(tempUri));
                        } else {
                            picturePath = temp_path.getAbsolutePath();
                            finishAndSetResult(RESULT_OK, picturePath);
                        }
                        break;
                    case PICTURE_GALLERY_REQUEST:
                        if (data != null) {
                            if (Build.VERSION.SDK_INT < 19) {
                                Uri selectedImage = data.getData();
                                System.out.println("selectedImage " + selectedImage + " SDK " + Build.VERSION.SDK_INT);
                                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                                if (cursor != null) {
                                    cursor.moveToFirst();
                                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                    picturePath = cursor.getString(columnIndex);
                                    cursor.close();
                                }
                                picturePath = saveScaledBitMap(CommonUtility.decodeFile(800, 800, picturePath),
                                        picturePath.substring(picturePath.indexOf("."), picturePath.length()));

                            } else {
                                try {
                                    InputStream imInputStream = getContentResolver().openInputStream(data.getData());
                                    Bitmap bitmap = BitmapFactory.decodeStream(imInputStream);
                                    bitmap = getResizedBitmap(bitmap, 400);
                                    picturePath = saveGalleryImageOnKitkat(bitmap);

                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }
                                // finishAndSetResult(RESULT_OK, picturePath, false);
                            }

                            sendImagePath(picturePath, false);

                        } else {
                            picturePath = temp_path.getAbsolutePath();
                            finishAndSetResult(RESULT_CANCELED, picturePath);
                        }
                        break;
                    default:
                        finish();
                        break;
                }
            } else {
                finish();
                Log.w("DialogChoosePicture", "Warning: activity result not ok");
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void sendImagePath(String filePath, final boolean isVideo) {
        int fileSize = checkFileSize(filePath);
        System.out.println("upload file size is " + fileSize + " in mb: " + (fileSize / 1024));
        if (isVideo) {
            if (fileSize < 1024 * 6) {
                finishAndSetResult((filePath == null || filePath.length() == 0) ? RESULT_CANCELED : RESULT_OK, filePath);
            } else {
                DialogUtility.showMessageOkWithCallback(getResources().getString(R.string.video_validation_alert), this,
                        new AlertDialogCallBack() {

                            @Override
                            public void onSubmit() {
                                finishAndSetResult(RESULT_CANCELED, "");

                            }

                            @Override
                            public void onCancel() {
                                finishAndSetResult(RESULT_CANCELED, "");
                            }
                        });
            }
        } else {
            if (fileSize < 1024 * 5) {
                finishAndSetResult((filePath == null || filePath.length() == 0) ? RESULT_CANCELED : RESULT_OK, filePath);
            } else {
                DialogUtility.showMessageOkWithCallback(getResources().getString(R.string.image_validation_alert), this,
                        new AlertDialogCallBack() {

                            @Override
                            public void onSubmit() {
                                finishAndSetResult(RESULT_CANCELED, "");

                            }

                            @Override
                            public void onCancel() {
                                finishAndSetResult(RESULT_CANCELED, "");
                            }
                        });
            }
        }
    }

    public int checkFileSize(String filepath) {
        try {
            File fileNew = new File(filepath);
            // System.out.println("Size of : " + file_size);
            return Integer.parseInt(String.valueOf(fileNew.length() / 1024));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private String saveScaledBitMap(Bitmap bitmap, String extension) {
        FileOutputStream out = null;
        try {

            File cacheDir;

            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
                cacheDir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.app_name));
            else
                cacheDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            if (!cacheDir.exists())
                cacheDir.mkdirs();

            try {
                File noMediaFile = new File(cacheDir, ".nomedia");
                if (!noMediaFile.exists())
                    noMediaFile.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }

            String filename = System.currentTimeMillis() + extension;// ".png";
            File file = new File(cacheDir, filename);
            temp_path = file.getAbsoluteFile();
            // if(!file.exists())
            file.createNewFile();

            out = new FileOutputStream(file);
            if (bitmap != null) {
//				System.out.println("captured bitmap height " + bitmap.getHeight() + "     weight: " + bitmap.getWidth());
                bitmap.compress(Bitmap.CompressFormat.JPEG, COMPRESS, out);
//                bitmap = null;
                return file.getAbsolutePath();
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (Throwable ignore) {
                ignore.printStackTrace();
            }
        }
        return null;
    }

    private String saveGalleryImageOnKitkat(Bitmap bitmap) {
        try {
            File cacheDir;
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
                cacheDir = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.app_name));
            else
                cacheDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            if (!cacheDir.exists())
                cacheDir.mkdirs();
            String filename = System.currentTimeMillis() + ".png";
            File file = new File(cacheDir, filename);
            temp_path = file.getAbsoluteFile();
            // if(!file.exists())
            file.createNewFile();
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, COMPRESS, out);
            return file.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public void finishAndSetResult(int result, String path) {
        Log.e("IMG", "finishAndSetResult called " + false + "  Path: " + path);
        Intent intent = getIntent();
        intent.putExtra("path", path);
        if (path != null && path.length() > 0)
            setResult(result, intent);
        else
            setResult(RESULT_CANCELED);
        finish();
    }

}
