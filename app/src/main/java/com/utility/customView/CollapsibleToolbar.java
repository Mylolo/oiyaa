package com.utility.customView;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

import androidx.constraintlayout.motion.widget.MotionLayout;

import com.google.android.material.appbar.AppBarLayout;

public class CollapsibleToolbar extends MotionLayout implements AppBarLayout.OnOffsetChangedListener
{
    public static final String TAG="CollapsibleToolbar";

    public CollapsibleToolbar(Context context)
    {
        super(context);
    }

    public CollapsibleToolbar(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public CollapsibleToolbar(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset)
    {
        Log.d(TAG,"onOffsetChanged");

        if(appBarLayout!=null)
        {
            setProgress(-verticalOffset / appBarLayout.getTotalScrollRange());
            Log.d(TAG,"onOffsetChanged-" + getProgress());
        }
    }

    @Override
    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        if(getParent() instanceof AppBarLayout)
        {
            if(getParent()!=null)
            {
                ((AppBarLayout) getParent()).addOnOffsetChangedListener(this);
            }
        }
    }

}
