package com.utility;

import android.content.Context;
import android.graphics.Typeface;

import androidx.core.content.res.ResourcesCompat;

import com.localvalu.R;

public class FontUtility
{
    // static variable single_instance of type Singleton
    private static FontUtility fontInstance = null;

    // variable of type Instace
    Typeface typeface;

    // private constructor restricted to this class itself
    private FontUtility()
    {

    }

    // static method to create instance of Singleton class
    public static FontUtility getInstance()
    {
        if (fontInstance == null)
            fontInstance = new FontUtility();

        return fontInstance;
    }

    public Typeface latoRegularFont(Context context)
    {
        typeface = ResourcesCompat.getFont(context, R.font.comfortaa_regular);
        return typeface;
    }
}
