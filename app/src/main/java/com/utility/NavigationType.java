package com.utility;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class NavigationType
{
    public static final int SCAN_AND_PAY = 0;
    public static final int EATS = 1;
    public static final int LOCAL = 2;
    public static final int MALL = 3;

    public NavigationType(@NavType int navType)
    {
        System.out.println("navType :" + navType);
    }

    @IntDef({SCAN_AND_PAY, EATS, LOCAL, MALL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface NavType
    {

    }

    @NavType
    public abstract int getNavigationMode();

    public abstract void setNavigationMode(@NavType int mode);
}
