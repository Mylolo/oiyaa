package com.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.TypedValue;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

import java.util.Calendar;
import java.util.TimeZone;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

public class AppUtils
{
    public static String APP_PREFERENCES = "App_Preferences";

    public static String BUNDLE_TOOLBAR_TITLE="Bundle_Toolbar_Title";
    public static String BUNDLE_NAVIGATION_TYPE="Bundle_Navigation_Type";
    public static String BUNDLE_ACCOUNT_ID="Bundle_AccountId";
    public static String BUNDLE_USER_ID="Bundle_UserId";
    public static String BUNDLE_BUSINESS_ID="Bundle_BusinessId";
    public static String BUNDLE_TABLE_BOOKING_STATUS="Bundle_Table_Booking_Status";
    public static String BUNDLE_TABLE_BOOKING_SHOW_CANCEL="Bundle_Table_Booking_Show_Cancel";

    public static String BUNDLE_BUSINESS_DETAIL="Bundle_Business_Detail";
    public static String BUNDLE_BUSINESS_DETAIL_INFO="Bundle_Business_Detail_Info";
    public static String BUNDLE_BUSINESS_LIST="Bundle_Business_List";
    public static String BUNDLE_BUSINESS_VIEW_ALL="Bundle_Business_View_All";
    public static String BUNDLE_FOOD_MENU="Bundle_Food_Menu";
    public static String BUNDLE_CART_LIST="Bundle_Cart_List";
    public static String BUNDLE_CHECK_OUT="Bundle_Check_Out";
    public static String BUNDLE_PAY_360_RESULT="Bundle_Pay_360_Result";

    public static String BUNDLE_OIYAA_BONUS ="Bundle_Oiyaa_Bonus";
    public static String BUNDLE_BONUS_TOKENS="Bundle_Bonus_Tokens";
    public static String BUNDLE_NEW_TOKEN_BALANCE="Bundle_New_Token_Balance";
    public static String BUNDLE_REVIEW_TOKENS="Bundle_ReviewTokens";

    public static String BUNDLE_DIALOG_MODEL="Bundle_Dialog_Model";

    public static String BUNDLE_ORDER_DETAIL="Bundle_Order_Detail";

    public static String BUNDLE_SEARCH_STRING="SearchString";
    public static String BUNDLE_SEARCH_ACTION_MODE="SearchActionMode";

    public static String BUNDLE_IS_FROM_LOGIN_PAGE="Bundle_Is_From_Login_Page";

    public static String BUNDLE_FULL_NAME="Bundle_FullName";
    public static String BUNDLE_MOBILE="Bundle_Mobile";
    public static String BUNDLE_EMAIL="Bundle_Email";
    public static String BUNDLE_QR_CODE_RESULT="Bundle_QRCodeResult";
    public static String BUNDLE_REGISTER_REQUEST="Bundle_RegisterRequest";
    public static String BUNDLE_NOTIFICATION_ACTION="Bundle_NotificationAction";
    public static String BUNDLE_IS_FROM_NOTIFICATION_PAGE="Bundle_Is_From_Notification_Page";
    public static String BUNDLE_TABLE_BOOKING_LIST_STATUS="Bundle_TableBookingListStatus";
    public static String BUNDLE_REQUEST_APPOINTMENT_LIST_STATUS="Bundle_RequestAppointmentListStatus";
    public static String BUNDLE_NOTIFICATION_DATA="Bundle_NotificationData";
    public static String BUNDLE_REVIEW_TYPE="Bundle_Review_Type";

    public static int PASSION_FILLED=1;
    public static int PASSION_NOT_FILLED=0;

    public static final int BMT_TRACK_AND_TRACE=0;
    public static final int BMT_EATS = 1;
    public static final int BMT_LOCAL = 2;
    public static final int BMT_MALL = 3;

    public static final int TOP_RATED=1;
    public static final int POPULAR=2;
    public static final int NEARBY=3;

    public static final int CUISINES=1;
    public static final int CATEGORY=2;

    public static final String TEXT_SEARCH_MODE="TextSearchMode";
    public static final String FILTER_SEARCH_MODE="FilterSearchMode";

    public static final int OPTION_SEARCH = 1;
    public static final int OPTION_LOCATION = 2;
    public static final int OPTION_FILTER = 3;

    public static final String RETAILER_TYPE_EATS = "LoLo Eats";
    public static final String RETAILER_TYPE_LOCAL = "LoLo Local";

    public static final String RETAILER_TYPE_EATS_OD = "Lv Eats";
    public static final String RETAILER_TYPE_LOCAL_OD = "Lv Local";

    public static final String OPTION_DELIVERY = "Delivery";
    public static final String OPTION_COLLECTION = "Collection";

    public static final String BOOKING_TYPE_NONE = "0";

    public static final String BOOKING_TYPE_BOOK_A_TABLE = "1";
    public static final String BOOKING_TYPE_REQUEST_AN_APPOINTMENT = "2";
    public static final String BOOKING_TYPE_REQUEST_A_CALLBACK = "3";

    public static final String ORDER_ONLINE = "4";
    public static final String ORDER_AT_TABLE = "5";
    public static final String ORDER_AT_STORE = "6";

    public static final String B_START_LOCATION_SERVICE = "B_STAR_LOCATION_SERVICE";
    public static final String PLACE_ORDER_AT_TABLE_EATS = "1";
    public static final String PLACE_ORDER_AT_TABLE_LOCAL = "2";

    public static final String DRIVER_TYPE_NO_DRIVER = "0";
    public static final String DRIVER_TYPE_POOL_DRIVER = "1";
    public static final String DRIVER_TYPE_OWN_DRIVER = "2";

    public static final String REQUEST_TYPE_REQUEST_APPOINTMENT="0";
    public static final String REQUEST_TYPE_TABLE_BOOKING="1";

    public static String BUNDLE_SCAN_ERROR_MESSAGE="Bundle_ErrorMessage";

    public static int APP_UPDATE_REQUEST = 150;

    public final static int NOTIFICATION_ORDER_PLACED = 1;
    public final static int NOTIFICATION_TABLE_BOOKED = 2;
    public final static int NOTIFICATION_REQUEST_APPOINTMENT = 3;
    public final static int NOTIFICATION_REVIEWED_BUSINESS = 4;

    public final static int REVIEW_TYPE_MY = 1;
    public final static int REVIEW_TYPE_ALL = 2;

    public static final int VIEW_TYPE_BUSINESS=1;
    public static final int VIEW_TYPE_FILTERS=2;

    public static int getThemeAccentColor(Context context)
    {
        int colorAttr;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            colorAttr = android.R.attr.colorAccent;
        }
        else
        {
            //Get colorAccent defined for AppCompat
            colorAttr = context.getResources().getIdentifier("colorAccent", "attr", context.getPackageName());
        }
        TypedValue outValue = new TypedValue();
        context.getTheme().resolveAttribute(colorAttr, outValue, true);
        return outValue.data;
    }

    public static Bitmap generateQRCode(String data)
    {
        Bitmap retVal;

        try
        {
            int size = 420;

            BitMatrix bitMatrix = new MultiFormatWriter().encode(data, BarcodeFormat.QR_CODE, size, size, null);

            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();

            int[] pixels = new int[width * height];

            for (int i = 0; i < height; i++)
            {
                int offset = i * width;

                for (int j = 0; j < width; j++)
                {
                    pixels[offset + j] = bitMatrix.get(j, i) ? BLACK : WHITE;
                }
            }

            retVal = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            retVal.setPixels(pixels, 0, size, 0, 0, width, height);
        }
        catch (Exception e)
        {
            return null;
        }


        return retVal;
    }

    public static Calendar getClearedUtc()
    {
        Calendar utc = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        utc.clear();
        return utc;
    }
}
