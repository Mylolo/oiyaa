package com.utility;

import android.app.Activity;

public class UserTableManager {

    public static void savePreviousUserID(Activity activity, String userID) {
        PreferenceUtility.saveStringInPreference(activity, "PreviousID", userID);
    }

    public static void saveEmailAndPassword(Activity activity, String emailID, String password) {
        PreferenceUtility.saveStringInPreference(activity, "EmailID", emailID);
        PreferenceUtility.saveStringInPreference(activity, "Password", password);
    }

    public static String getPreviousUserID(Activity activity) {
        return PreferenceUtility.getStringFromPreference(activity, "PreviousID");
    }

}
