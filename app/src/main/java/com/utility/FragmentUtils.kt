package com.utility

import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.localvalu.R
import com.localvalu.base.Resource

fun Fragment.handleAPIError(failure: Resource.Failure, retry: (()->Unit)?=null)
{
    when
    {
        failure.isNetworkError->
        {
            requireView().snackbar(requireContext().getString(R.string.network_unavailable))
        }
    }
}

fun Fragment.showMessage(message: String, retry: (()->Unit)?=null)
{
    requireView().snackbar(message)
}

fun View.snackbar(message: String, action: (()-> Unit)?=null)
{
    val snackbar = Snackbar.make(this,message, Snackbar.LENGTH_INDEFINITE)
    action?.let {
        snackbar.setAction(this.context.getString(R.string.lbl_retry))
        {
            it
        }
    }
    snackbar.show()
}
