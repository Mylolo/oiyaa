package com.utility;

public class Constants {
    public static final String TAG = "MyLoLo";
    public static final String LOADING_MESSAGE = "Loading";

    /**
     * Testing Id
     */
   /* public static final String RAZORPAY_KEY_ID ="rzp_test_a9WAr0zQvHKy9w" ;
    public static final String RAZORPAY_KEY_SECRET = "KvqNqtUZ8HthAKmDVXcyJvrn";*/

    public static final String NOTICE = "We are upgrading our platform.  Will be back soon with new updated platform. We are requesting you to avoid transaction during the up-gradation.";

    public static final String EAT_LOCATION_SCREEN = "EatLocationScreen";

    public static final String DIRECTORY_LOCATION_SCREEN = "DirectoryLocationScreen";

    public static final String EATS_DASHBOARD_FRAGMENT = "EatsDashboardFragment";

    public static final String DIRECTORY_DASHBOARD_FRAGMENT = "DirectoryDashboardFragment";

    /**
     * Status 3 -- Waiting for confirm
     * Status 2 -- Declined
     * Status 1 -- Delivered
     * Status 0 -- Accept
     * Status 5 -- Cancelled
     * Status 20 -- Print
     */
    public static final String PENDING = "3";
    public static final String DECLINED = "2";
    public static final String DELIVERED= "1";
    public static final String ACCEPT = "0";
    public static final String BOOKING_CANCELLED = "5";
    public static final String ORDER_CANCELLED = "6";
    public static final String PRINT = "20";

}
