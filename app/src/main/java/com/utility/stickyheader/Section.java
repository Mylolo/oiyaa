package com.utility.stickyheader;

import com.localvalu.user.dto.PassionDetails;

public interface Section
{
    boolean isHeader();
    PassionDetails passionDetails();
    int sectionPosition();
}
