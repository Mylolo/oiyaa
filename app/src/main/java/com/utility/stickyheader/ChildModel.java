package com.utility.stickyheader;

import com.localvalu.user.dto.PassionDetails;

public class ChildModel implements Section
{
    PassionDetails passionDetails;
    private int section;


    public ChildModel(int section) {
        this.section = section;
    }

    public void setChild(PassionDetails passionDetails) {
        this.passionDetails = passionDetails;
    }

    @Override
    public boolean isHeader() {
        return false;
    }

    @Override
    public PassionDetails passionDetails() {
        return passionDetails;
    }

    @Override
    public int sectionPosition() {
        return section;
    }
}
