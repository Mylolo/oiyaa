package com.utility.stickyheader;

import com.localvalu.user.dto.PassionDetails;

public class HeaderModel implements Section
{
    PassionDetails passionDetails;
    private int section;

    public HeaderModel(int section)
    {
        this.section = section;
    }

    public void setHeader(PassionDetails passionDetails)
    {
        this.passionDetails = passionDetails;
    }

    @Override
    public boolean isHeader()
    {
        return true;
    }

    @Override
    public PassionDetails passionDetails()
    {
        return null;
    }


    @Override
    public int sectionPosition()
    {
        return section;
    }
}
