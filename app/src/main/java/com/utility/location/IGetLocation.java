package com.utility.location;

import android.location.Location;

public interface IGetLocation {

    void getLocation(Location location);

}
