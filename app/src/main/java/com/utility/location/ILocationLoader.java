package com.utility.location;

import android.app.Activity;

public interface ILocationLoader {

    void onCurrentLocation(Activity activity);

    void onReceivedCurrentLocation(GeoPoint geoPoint);
}
