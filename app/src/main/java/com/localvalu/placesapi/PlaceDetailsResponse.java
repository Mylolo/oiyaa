package com.localvalu.placesapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaceDetailsResponse
{
    @SerializedName("result")
    @Expose
    private PlaceDetails placeDetails;

    @SerializedName("status")
    @Expose
    private String status;

    public PlaceDetails getPlaceDetails()
    {
        return placeDetails;
    }

    public void setPlaceDetails(PlaceDetails placeDetails)
    {
        this.placeDetails = placeDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
