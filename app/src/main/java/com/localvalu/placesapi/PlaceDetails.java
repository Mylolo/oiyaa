package com.localvalu.placesapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlaceDetails
{
    @SerializedName("address_components")
    @Expose
    private List<AddressComponents> addressComponents;


    public List<AddressComponents> getAddressComponents()
    {
        return addressComponents;
    }

    public void setAddressComponents(List<AddressComponents> addressComponents)
    {
        this.addressComponents = addressComponents;
    }

}
