package com.localvalu.extensions

import androidx.annotation.VisibleForTesting
import java.text.SimpleDateFormat
import java.util.*

fun Date.convertDateToStringInDisplayFormat(): String
{
    val format = SimpleDateFormat("dd/MM/yyyy")
    return format.format(this)
}

fun Date.convertDateToStringInServerFormat(): String
{
    val format = SimpleDateFormat("yyyy-MM-dd")
    return format.format(this)
}

fun Date.convertDateTimeToStringInDisplayFormat(): String
{
    val format = SimpleDateFormat("dd/MM/yyyy hh:mm a")
    return format.format(this)
}

fun Date.convertDateTimeToStringInServerFormat(): String
{
    val format = SimpleDateFormat("yyyy-MM-dd HH:mm")
    return format.format(this)
}

fun Date.getTodayDate():Date
{
    val calendar:Calendar = Calendar.getInstance()
    return calendar.time
}

@VisibleForTesting
fun Date.getDifferenceInMinutes(startTime:Date):Long
{
    val diff: Long = this.time - startTime.time
    val seconds = diff / 1000
    val minutes = seconds / 60
    val hours = minutes / 60
    val days = hours / 24
    return minutes;
}
