package com.localvalu.extensions

import android.view.View
import androidx.viewpager2.widget.ViewPager2

// This function can sit in an Helper file, so it can be shared across your project.
fun updatePagerHeightForChild(view: View, pager: ViewPager2) {
    view.post {
        val wMeasureSpec =
            View.MeasureSpec.makeMeasureSpec(view.width, View.MeasureSpec.EXACTLY)
        val hMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        view.measure(wMeasureSpec, hMeasureSpec)

        if (pager.layoutParams.height != view.measuredHeight) {
            pager.layoutParams = (pager.layoutParams)
                .also { lp ->
                    // applying Fragment Root View Height to
                    // the pager LayoutParams, so they match
                    lp.height = view.measuredHeight
                }
        }
    }
}