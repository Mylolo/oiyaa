package com.localvalu.forgetpassword.di

import android.content.Context
import com.localvalu.base.RemoteDataSource
import com.localvalu.forgetpassword.api.ForgetPasswordApi
import com.localvalu.forgetpassword.repository.ForgetPasswordRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class ForgetPasswordModule
{
    @Provides
    fun providesForgetPasswordApi(remoteDataSource: RemoteDataSource,
                            @ApplicationContext context: Context): ForgetPasswordApi
    {
        return remoteDataSource.buildApi(ForgetPasswordApi::class.java,context)
    }

    @Provides
    fun providesForgetPasswordRepository(forgetPasswordApi: ForgetPasswordApi):ForgetPasswordRepository
    {
        return ForgetPasswordRepository(forgetPasswordApi)
    }
}