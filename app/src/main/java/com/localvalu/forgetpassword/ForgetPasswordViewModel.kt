package com.localvalu.forgetpassword

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.base.Resource
import com.localvalu.forgetpassword.repository.ForgetPasswordRepository
import com.localvalu.user.dto.forgetpassword.ForgetPasswordRequest
import com.localvalu.user.dto.forgetpassword.ForgetPasswordResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ForgetPasswordViewModel @Inject constructor(private val repository:ForgetPasswordRepository) : ViewModel()
{
    private val _forgetPasswordResponse: MutableLiveData<Resource<ForgetPasswordResponse>> = MutableLiveData()
    val forgetPasswordResponse: LiveData<Resource<ForgetPasswordResponse>>
        get() = _forgetPasswordResponse

    fun forgetPassword(forgetPasswordRequest: ForgetPasswordRequest) = viewModelScope.launch {
        _forgetPasswordResponse.value = Resource.Loading
        _forgetPasswordResponse.value = repository.forgetPassword(forgetPasswordRequest)
    }

}