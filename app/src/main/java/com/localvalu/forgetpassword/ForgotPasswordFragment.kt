package com.localvalu.forgetpassword

import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import com.localvalu.R
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import android.graphics.PorterDuff
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.localvalu.base.Resource
import com.localvalu.databinding.FragmentForgetPasswordBinding
import com.localvalu.user.dto.forgetpassword.ForgetPasswordRequest
import com.localvalu.user.repository.ForgetPasswordRepository
import com.utility.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgotPasswordFragment : Fragment()
{
    //For API Call
    private lateinit var binding: FragmentForgetPasswordBinding
    private var colorBlack = 0
    private var colorGray = 0
    private var colorWhite = 0
    private val viewModel: ForgetPasswordViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        colorBlack = ContextCompat.getColor(requireContext(), R.color.colorBlack)
        colorGray = ContextCompat.getColor(requireContext(), R.color.colorGray)
        colorWhite = ContextCompat.getColor(requireContext(), R.color.colorWhite)
        setProperties()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentForgetPasswordBinding.inflate(inflater,container,false)
        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties();
    }

    private fun setProperties()
    {
        val drawableBack = ResourcesCompat.getDrawable(resources, R.drawable.ic_app_arrow, null)
        drawableBack!!.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP)
        binding.imgBack.setImageDrawable(drawableBack)
        binding.imgBack.setOnClickListener(onClickListener)
        binding.btnSubmit.setOnClickListener(onClickListener)
        binding.btnLogin.setOnClickListener(onClickListener)
        observeSubscribers()
    }

    private fun observeSubscribers()
    {
        viewModel.forgetPasswordResponse.observe(viewLifecycleOwner, Observer {
            when(it)
            {
                is Resource.Loading ->
                {
                    showProgress()
                }
                is Resource.Success ->
                {
                    showContents()
                    if (it.value.forgetPassword.errorDetails.errorCode == 0L)
                    {
                        showMessage(it.value.forgetPassword.forgetPasswordResult.message)
                    }
                    else
                    {
                        showMessage(it.value.forgetPassword.errorDetails.errorMessage)
                    }
                }
                is Resource.Failure ->
                {
                    handleAPIError(it)
                    {
                        forgetPassword()
                    }
                }
            }
        })
    }

    private fun showProgress()
    {
        binding.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding.layoutError.layoutRoot.visibility = View.GONE
        binding.cnlMain.visibility = View.GONE
    }

    private fun showError(message:String,retry: Boolean)
    {
        binding.layoutProgress.layoutRoot.visibility = View.GONE
        binding.layoutError.layoutRoot.visibility = View.VISIBLE
        binding.cnlMain.visibility = View.GONE
    }

    private fun showContents()
    {
        binding.layoutProgress.layoutRoot.visibility = View.GONE
        binding.layoutError.layoutRoot.visibility = View.GONE
        binding.cnlMain.visibility = View.VISIBLE
    }

    var onClickListener = View.OnClickListener { view -> onButtonClick(view) }

    fun onButtonClick(view: View)
    {
        when (view.id)
        {
            R.id.imgBack -> onDestroy()
            R.id.btnSubmit -> if (isValid)
            {
                forgetPassword()
            }
            R.id.btnLogin ->
            {
                //Redirect to login page
            }
        }
    }

    private val isValid: Boolean
        private get()
        {
            hideKeyBoard()
            if (binding!!.etEmail.text.toString().isEmpty())
            {
                binding!!.etEmail.error = "Enter Your Email ID"
                binding!!.etEmail.requestFocus()
                return false
            }
            else if (!Patterns.EMAIL_ADDRESS.matcher(binding!!.etEmail.text.toString()).matches())
            {
                binding!!.etEmail.error = "Enter A Valid Email ID"
                binding!!.etEmail.requestFocus()
                return false
            }
            /*else if (binding.etMobileNumber.getText().toString().isEmpty())
            {
                binding.etMobileNumber.setError("Enter Mobile Number");
                binding.etMobileNumber.requestFocus();
                return false;
            }
            else if (!ValidationUtils.isValidMobileNo(binding.etMobileNumber.getText().toString().trim()))
            {
                binding.etMobileNumber.setError("Enter Valid Mobile Number");
                binding.etMobileNumber.requestFocus();
                return false;
            }*/return true
        }

    private val forgetPasswordRequest: ForgetPasswordRequest
        get()
        {
            val forgetPasswordRequest = ForgetPasswordRequest()
            forgetPasswordRequest.email = binding!!.etEmail.text.toString()
            return forgetPasswordRequest
        }

    private fun forgetPassword()
    {
        viewModel.forgetPassword(forgetPasswordRequest)
    }

    private fun hideKeyBoard()
    {
        val imm = requireActivity().getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding!!.etEmail.windowToken, 0)
    }

    companion object
    {
        private const val TAG = "ForgotPasswordFragment"
    }

}