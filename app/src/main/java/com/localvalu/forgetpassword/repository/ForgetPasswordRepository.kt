package com.localvalu.forgetpassword.repository

import com.localvalu.base.BaseRepository
import com.localvalu.forgetpassword.api.ForgetPasswordApi
import com.localvalu.user.dto.forgetpassword.ForgetPasswordRequest

class ForgetPasswordRepository (private val api: ForgetPasswordApi): BaseRepository(api)
{
    suspend fun forgetPassword(forgetPasswordRequest: ForgetPasswordRequest) = safeApiCall {
        api.forgetPassword(forgetPasswordRequest)
    }
}