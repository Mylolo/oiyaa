package com.localvalu.forgetpassword.api

import com.localvalu.base.BaseApi
import com.localvalu.common.model.SearchRequest
import com.localvalu.common.model.SearchResponse
import com.localvalu.registration.model.UserExistRequest
import com.localvalu.user.dto.PassionListRequest
import com.localvalu.user.dto.forgetpassword.ForgetPasswordRequest
import com.localvalu.user.dto.forgetpassword.ForgetPasswordResponse
import com.localvalu.user.wrapper.PassionListResultWrapper
import com.localvalu.user.wrapper.RegisterExistsCustomerResultWrapper
import retrofit2.http.Body
import retrofit2.http.POST

interface ForgetPasswordApi : BaseApi
{
    @POST("Local/forgetPassword")
    suspend fun forgetPassword(@Body forgetPasswordRequest: ForgetPasswordRequest): ForgetPasswordResponse
}