package com.localvalu.eats.myorder.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MyOrders implements Serializable
{
    @SerializedName("OrderId")
    @Expose
    private String orderId;

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("businessId")
    @Expose
    private String businessId;

    @SerializedName("orderTotal")
    @Expose
    private String orderTotal;

    @SerializedName("deliveryRate")
    @Expose
    private String deliveryRate;

    @SerializedName("tax")
    @Expose
    private String tax;

    @SerializedName("logo")
    @Expose
    private String logo;

    @SerializedName("reviewComments")
    @Expose
    private String reviewComments;

    @SerializedName("rate")
    @Expose
    private String rate;

    @SerializedName("reviewId")
    @Expose
    private String reviewId;

    @SerializedName("DiscountLtAmount")
    @Expose
    private String discountLtAmount;

    @SerializedName("PayableVatAmount")
    @Expose
    private String payableVatAmount;

    @SerializedName("Placedatetime")
    @Expose
    private String placedatetime;

    @SerializedName("is_order_placed")
    @Expose
    private String isOrderPlaced;

    /**
     * We use it for getting order status
     */
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("is_order_confirmed_by_rest")
    @Expose
    private String isOrderConfirmedByRest;

    @SerializedName("special_ins")
    @Expose
    private String specialInstructions;

    @SerializedName("allergy_ins")
    @Expose
    private String allergyInstructions;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("isActive")
    @Expose
    private String isActive;

    @SerializedName("orderType")
    @Expose
    private String orderType;

    @SerializedName("tradingname")
    @Expose
    private String tradingName;

    @SerializedName("RetailerType")
    @Expose
    private String retailerType;

    @SerializedName("orderType1")
    @Expose
    private String orderType1;

    @SerializedName("orderTypeLocal")
    @Expose
    private String orderTypeLocal;

    @SerializedName("orderTypeLocal1")
    @Expose
    private String orderTypeLocal1;

    @SerializedName("storeid")
    @Expose
    private String storeid;

    public String getOrderId()
    {
        return orderId;
    }

    public void setOrderId(String orderId)
    {
        this.orderId = orderId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getBusinessId()
    {
        return businessId;
    }

    public void setBusinessId(String businessId)
    {
        this.businessId = businessId;
    }

    public String getOrderTotal()
    {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal)
    {
        this.orderTotal = orderTotal;
    }

    public String getDeliveryRate()
    {
        return deliveryRate;
    }

    public void setDeliveryRate(String deliveryRate)
    {
        this.deliveryRate = deliveryRate;
    }

    public String getTax()
    {
        return tax;
    }

    public void setTax(String tax)
    {
        this.tax = tax;
    }

    public String getLogo()
    {
        return logo;
    }

    public void setLogo(String logo)
    {
        this.logo = logo;
    }

    public String getReviewComments()
    {
        return reviewComments;
    }

    public void setReviewComments(String reviewComments)
    {
        this.reviewComments = reviewComments;
    }

    public String getRate()
    {
        return rate;
    }

    public void setRate(String rate)
    {
        this.rate = rate;
    }

    public String getReviewId()
    {
        return reviewId;
    }

    public void setReviewId(String reviewId)
    {
        this.reviewId = reviewId;
    }

    public String getDiscountLtAmount()
    {
        return discountLtAmount;
    }

    public void setDiscountLtAmount(String discountLtAmount)
    {
        this.discountLtAmount = discountLtAmount;
    }

    public String getPayableVatAmount()
    {
        return payableVatAmount;
    }

    public void setPayableVatAmount(String payableVatAmount)
    {
        this.payableVatAmount = payableVatAmount;
    }

    public String getPlacedatetime()
    {
        return placedatetime;
    }

    public void setPlacedatetime(String placedatetime)
    {
        this.placedatetime = placedatetime;
    }

    public String getIsOrderPlaced()
    {
        return isOrderPlaced;
    }

    public void setIsOrderPlaced(String isOrderPlaced)
    {
        this.isOrderPlaced = isOrderPlaced;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getIsOrderConfirmedByRest()
    {
        return isOrderConfirmedByRest;
    }

    public void setIsOrderConfirmedByRest(String isOrderConfirmedByRest)
    {
        this.isOrderConfirmedByRest = isOrderConfirmedByRest;
    }

    public String getSpecialInstructions()
    {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions)
    {
        this.specialInstructions = specialInstructions;
    }

    public String getAllergyInstructions()
    {
        return allergyInstructions;
    }

    public void setAllergyInstructions(String allergyInstructions)
    {
        this.allergyInstructions = allergyInstructions;
    }

    public String getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(String createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getIsActive()
    {
        return isActive;
    }

    public void setIsActive(String isActive)
    {
        this.isActive = isActive;
    }

    public String getOrderType()
    {
        return orderType;
    }

    public void setOrderType(String orderType)
    {
        this.orderType = orderType;
    }

    public String getTradingName()
    {
        return tradingName;
    }

    public void setTradingName(String tradingName)
    {
        this.tradingName = tradingName;
    }

    public String getRetailerType()
    {
        return retailerType;
    }

    public void setRetailerType(String retailerType)
    {
        this.retailerType = retailerType;
    }

    public String getOrderType1()
    {
        return orderType1;
    }

    public void setOrderType1(String orderType1)
    {
        this.orderType1 = orderType1;
    }

    public String getOrderTypeLocal()
    {
        return orderTypeLocal;
    }

    public void setOrderTypeLocal(String orderTypeLocal)
    {
        this.orderTypeLocal = orderTypeLocal;
    }

    public String getOrderTypeLocal1()
    {
        return orderTypeLocal1;
    }

    public void setOrderTypeLocal1(String orderTypeLocal1)
    {
        this.orderTypeLocal1 = orderTypeLocal1;
    }

    public String getStoreid()
    {
        return storeid;
    }

    public void setStoreid(String storeid)
    {
        this.storeid = storeid;
    }
}
