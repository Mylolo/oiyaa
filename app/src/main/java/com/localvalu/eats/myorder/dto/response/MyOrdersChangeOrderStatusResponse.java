package com.localvalu.eats.myorder.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyOrdersChangeOrderStatusResponse
{
    @SerializedName("UserDashboardByRequestForDetailsResult")
    @Expose
    private MyOrdersChangeOrderStatusResult myOrdersChangeOrderStatusResult;

    public MyOrdersChangeOrderStatusResult getMyOrdersChangeOrderStatusResult()
    {
        return myOrdersChangeOrderStatusResult;
    }

    public void setMyOrdersChangeOrderStatusResult(MyOrdersChangeOrderStatusResult myOrdersChangeOrderStatusResult)
    {
        this.myOrdersChangeOrderStatusResult = myOrdersChangeOrderStatusResult;
    }
}
