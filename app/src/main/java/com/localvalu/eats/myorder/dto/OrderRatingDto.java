package com.localvalu.eats.myorder.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OrderRatingDto implements Serializable {

    @SerializedName("product_rate")
    @Expose
    public String product_rate;
    @SerializedName("service_rate")
    @Expose
    public String service_rate;
}
