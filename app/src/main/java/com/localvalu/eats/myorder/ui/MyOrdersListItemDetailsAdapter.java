package com.localvalu.eats.myorder.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localvalu.R;
import com.localvalu.eats.myorder.dto.response.MyOrderSingleDetails;

import java.util.ArrayList;

public class MyOrdersListItemDetailsAdapter extends ListAdapter<MyOrderSingleDetails,MyOrdersListItemDetailsAdapter.ViewHolder>
{
    public static final String TAG = MyOrdersListItemDetailsAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<MyOrderSingleDetails> data;
    private String strCurrencySymbol;
    private String strCurrencyLetter;

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.context = recyclerView.getContext();
        strCurrencyLetter=context.getString(R.string.lbl_currency_letter_euro);
        strCurrencySymbol=context.getString(R.string.lbl_currency_symbol_euro);
    }

    public MyOrdersListItemDetailsAdapter()
    {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<MyOrderSingleDetails> DIFF_CALLBACK = new DiffUtil.ItemCallback<MyOrderSingleDetails>()
    {
        @Override
        public boolean areItemsTheSame(@NonNull MyOrderSingleDetails oldItem, @NonNull MyOrderSingleDetails newItem)
        {
            return oldItem.getItemId() == newItem.getItemId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull MyOrderSingleDetails oldItem, @NonNull MyOrderSingleDetails newItem)
        {
            return oldItem.getItemId().equals(newItem.getItemId());
        }
    };

    @NonNull
    @Override
    public MyOrdersListItemDetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_item_ordered_items_eat, viewGroup, false);
        return new MyOrdersListItemDetailsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyOrdersListItemDetailsAdapter.ViewHolder viewHolder, final int position)
    {
        try
        {
            MyOrderSingleDetails myOrderSingleDetails = getItem(position);
            StringBuilder strTitle = new StringBuilder();
            strTitle.append(myOrderSingleDetails.getItemName());
            if(myOrderSingleDetails.getSizeName()!=null)
            {
                if(!myOrderSingleDetails.getSizeName().equals("Default"))
                {
                    strTitle.append("(").append(myOrderSingleDetails.getSizeName()).append(")");
                }
            }
            viewHolder.tv_item_name.setText(strTitle.toString());

            double singleItemPrice = (Double.parseDouble(myOrderSingleDetails.getSingleQuantityPrice()));
            String strSingleItemPrice = String.format("%,.2f",singleItemPrice);
            int qty = Integer.parseInt(myOrderSingleDetails.getQuantity());
            StringBuilder strQtySinglePrice = new StringBuilder();
            strQtySinglePrice.append(strCurrencySymbol).append(strSingleItemPrice).append(" x ").append(qty);
            viewHolder.tv_quantity.setText(strQtySinglePrice.toString());
            double itemTotalPrice = (Double.parseDouble(myOrderSingleDetails.getItemTotalPrice()));
            viewHolder.tv_price.setText(strCurrencySymbol + String.format("%,.2f",itemTotalPrice));
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        AppCompatTextView tv_item_name = itemView.findViewById(R.id.tv_item_name);
        AppCompatTextView tv_quantity = itemView.findViewById(R.id.tv_quantity);
        AppCompatTextView tv_price = itemView.findViewById(R.id.tv_price);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }

}
