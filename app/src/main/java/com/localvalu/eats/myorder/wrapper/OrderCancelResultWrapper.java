package com.localvalu.eats.myorder.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.myorder.dto.OrderCancelResult;

import java.io.Serializable;

public class OrderCancelResultWrapper implements Serializable {

    @SerializedName("Order")
    @Expose
    public OrderCancelResult orderCancelResult;
}
