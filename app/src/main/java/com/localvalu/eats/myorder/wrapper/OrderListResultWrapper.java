package com.localvalu.eats.myorder.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.myorder.dto.OrderListResult;

import java.io.Serializable;

public class OrderListResultWrapper implements Serializable {

    @SerializedName("Orders")
    @Expose
    public OrderListResult orderListResult;
}
