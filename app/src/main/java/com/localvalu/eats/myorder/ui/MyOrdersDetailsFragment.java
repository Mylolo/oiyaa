package com.localvalu.eats.myorder.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentMyOrdersDetailsEatBinding;
import com.localvalu.eats.myorder.dto.request.MyOrdersChangeOrderStatusRequest;
import com.localvalu.eats.myorder.dto.request.MyOrdersSingleOrderDetailsRequest;
import com.localvalu.eats.myorder.dto.response.MyOrderSingleDetails;
import com.localvalu.eats.myorder.dto.response.MyOrderSingleDetailsResponse;
import com.localvalu.eats.myorder.dto.response.MyOrders;
import com.localvalu.eats.myorder.dto.response.MyOrdersChangeOrderStatusResponse;
import com.localvalu.eats.myorder.dto.response.MyOrdersChangeOrderStatusResult;
import com.localvalu.eats.myorder.dto.response.MyOrdersSingleDetailsResult;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.squareup.picasso.Picasso;
import com.utility.AlertDialogCallBack;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.Constants;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.utility.AppUtils.RETAILER_TYPE_EATS_OD;

public class MyOrdersDetailsFragment extends BaseFragment
{
    private static final String TAG = "MyOrdersDetailsFragment";


    private int colorTheme;

    private FragmentMyOrdersDetailsEatBinding binding;

    private GridLayoutManager gridLayoutManager;
    private MyOrdersListItemDetailsAdapter myOrdersListItemDetailsAdapter;
    private LinearLayoutManager layoutManager;
    private MyOrders myOrders;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private int rightPadding;
    private HostActivityListener hostActivityListener;
    private int navigationType;

    public static MyOrdersDetailsFragment newInstance(MyOrders myOrders,int navigationType)
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppUtils.BUNDLE_ORDER_DETAIL,myOrders);
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,navigationType);
        MyOrdersDetailsFragment myOrdersDetailsFragment = new MyOrdersDetailsFragment();
        myOrdersDetailsFragment.setArguments(bundle);
        return myOrdersDetailsFragment;
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");
        readFromBundle();

        strCurrencySymbol=getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter=getString(R.string.lbl_currency_letter_euro);

        buildObjectForHandlingAPI();

        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        rightPadding = (int) getResources().getDimension(R.dimen._5sdp);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentMyOrdersDetailsEatBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
        callMyOrdersAPI();
    }

    private void setProperties()
    {
        layoutManager = new LinearLayoutManager(requireContext());
        binding.rvOrderedItemsMyOrders.setLayoutManager(layoutManager);
        myOrdersListItemDetailsAdapter=new MyOrdersListItemDetailsAdapter();
        binding.rvOrderedItemsMyOrders.setAdapter(myOrdersListItemDetailsAdapter);

        binding.btnItemCancel.setOnClickListener(_OnClickListener);

        binding.tvTableId.setVisibility(View.GONE);
        binding.tvTextTableId.setVisibility(View.GONE);

        binding.tvDescription.setVisibility(View.GONE);
        binding.tvTextDescription.setVisibility(View.GONE);

        binding.tvDeliveredAt.setVisibility(View.GONE);
        binding.tvLblDeliveredAt.setVisibility(View.GONE);
    }

    private void setUpActionBar()
    {
        binding.toolbarLayout.tvTitle.setTextColor(colorTheme);
        binding.toolbarLayout.tvTitle.setText(getString(R.string.lbl_my_orders));
        binding.toolbarLayout.toolbar.getNavigationIcon().setColorFilter(colorTheme, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });
    }

    private void readFromBundle()
    {
        if(getArguments()!=null)
        {
            myOrders = (MyOrders) getArguments().getSerializable(AppUtils.BUNDLE_ORDER_DETAIL);
            navigationType = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
            switch (navigationType)
            {
                case AppUtils.BMT_EATS:
                    colorTheme = ContextCompat.getColor(requireContext(), R.color.colorEats);
                    break;
                case AppUtils.BMT_LOCAL:
                    colorTheme = ContextCompat.getColor(requireContext(),R.color.colorLocal);
                    break;

            }
        }
    }

    private MyOrdersSingleOrderDetailsRequest getMyOrdersSingleOrderedDetailsPayload(String strOrderId)
    {
        MyOrdersSingleOrderDetailsRequest myOrdersSingleOrderDetailsRequest = new MyOrdersSingleOrderDetailsRequest();
        myOrdersSingleOrderDetailsRequest.setAccountId(userBasicInfo.accountId);
        myOrdersSingleOrderDetailsRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        myOrdersSingleOrderDetailsRequest.setRequestFor("SingleOrderDetails");
        myOrdersSingleOrderDetailsRequest.setOrderId(strOrderId);
        return myOrdersSingleOrderDetailsRequest;
    }

    private void callMyOrdersAPI()
    {
        mProgressDialog.show();

        Call<MyOrderSingleDetailsResponse> dtoCall = apiInterface.
                getOrderDetails(getMyOrdersSingleOrderedDetailsPayload(myOrders.getOrderId()));
        dtoCall.enqueue(new Callback<MyOrderSingleDetailsResponse>()
        {
            @Override
            public void onResponse(Call<MyOrderSingleDetailsResponse> call, Response<MyOrderSingleDetailsResponse> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    MyOrderSingleDetailsResponse myOrderSingleDetailsResponse = response.body();

                    if(myOrderSingleDetailsResponse!=null)
                    {
                        MyOrdersSingleDetailsResult myOrdersSingleDetailsResult = myOrderSingleDetailsResponse.getMyOrdersSingleDetailsResult();

                        if(myOrdersSingleDetailsResult!=null)
                        {
                            if(myOrdersSingleDetailsResult.errorDetails.errorCode == 0)
                            {
                                ArrayList<MyOrderSingleDetails> myOrderSingleDetails= myOrdersSingleDetailsResult.getMyOrderSingleDetails();

                                if(myOrderSingleDetails.size()>0)
                                {
                                    setViewData(myOrderSingleDetails);
                                }
                                else
                                {
                                    binding.rvOrderedItemsMyOrders.setVisibility(View.GONE);
                                }
                            }
                            else
                            {
                                DialogUtility.showMessageWithOk(myOrdersSingleDetailsResult.errorDetails.errorMessage,getActivity());
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MyOrderSingleDetailsResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.btn_item_cancel:
                     showCancelItemDialog();
                     break;
            }
        }
    };

    public void setViewData(ArrayList<MyOrderSingleDetails> myOrderSingleDetailsList)
    {
        StringBuilder strNumberOfItems = new StringBuilder();
        strNumberOfItems.append(getString(R.string.lbl_no_of_items_colon)).append(myOrderSingleDetailsList.size());
        binding.tvNoOfItem.setText(strNumberOfItems.toString());
        updateAdapter(myOrderSingleDetailsList);
        MyOrderSingleDetails myOrderSingleDetails = myOrderSingleDetailsList.get(0);
        setHeaderData(myOrderSingleDetails);
        setPaymentData(myOrderSingleDetails);
        showOrderAtTableDataOnly(myOrderSingleDetails);
    }

    private void setHeaderData(MyOrderSingleDetails myOrderSingleDetails)
    {
        StringBuilder strOrderId = new StringBuilder();
        strOrderId.append(getString(R.string.lbl_order_id_colon)).append(" ").append(myOrderSingleDetails.getOrderId());
        binding.tvOrderId.setText(strOrderId.toString());

        if(myOrderSingleDetails.getStatus()!=null) setStatus(myOrderSingleDetails.getStatus());
        if (!myOrders.getLogo().equals(""))
        {
            binding.ivPict.setVisibility(View.VISIBLE);

            Picasso.with(requireContext()).load(myOrders.getLogo())
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo ).into(binding.ivPict);

            binding.ivPict.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
        else
        {
            Picasso.with(requireContext()).load(R.drawable.app_logo)
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo ).into(binding.ivPict);
            binding.ivPict.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }
        binding.tvTradingName.setText(myOrders.getTradingName());
        StringBuilder strOrdererdAt = new StringBuilder();
        strOrdererdAt.append("Ordered At : ").append(DateUtility.getFormattedDateTime(myOrders.getPlacedatetime()));
        binding.tvOrderDate.setText(strOrdererdAt.toString());
        StringBuilder strTotalAmount = new StringBuilder();
        strTotalAmount.append(strCurrencySymbol).append(String.format("%,.2f", (Float.parseFloat(myOrders.getOrderTotal()))));
        binding.tvTotalAmount.setText(strTotalAmount.toString());
        binding.tvDeliveredAt.setText(DateUtility.getFormattedDateTime(myOrderSingleDetails.getPlacedatetime()));
        binding.tvSpecialInstructions.setText(myOrderSingleDetails.getSpecialInstructions());
        binding.tvAllergyInstructions.setText(myOrderSingleDetails.getAllergyInstructions());
        if(myOrderSingleDetails.getOrderType().trim().equals("PayByCard") || myOrderSingleDetails.getOrderType().trim().equals("Pay by Card"))
        {
            binding.tvPaidBy.setText(getString(R.string.lbl_pay_by_card));
        }
        else
        {
            binding.tvPaidBy.setText(getString(R.string.lbl_pay_by_cash));
        }
    }

    private void setPaymentData(MyOrderSingleDetails myOrderSingleDetails)
    {
        double subTotal = (Double.parseDouble(myOrderSingleDetails.getOrderTotal()));
        String strOrderTotal = String.format("%,.2f",subTotal);
        StringBuilder strSubTotal = new StringBuilder();
        strSubTotal.append(strCurrencySymbol).append(strOrderTotal);
        binding.tvSubTotal.setText(strSubTotal.toString());

        double discountAmount = (Double.parseDouble(myOrderSingleDetails.getDiscountLtAmount()));
        String strDiscountAmount = String.format("%,.2f",discountAmount);
        StringBuilder strTokenApply = new StringBuilder();
        strTokenApply.append("-").append(strCurrencyLetter).append(strDiscountAmount);
        binding.tvTokenApplied.setText(strTokenApply.toString());

        String strDeliveryRate = "0.0";
        if(myOrderSingleDetails.getDeliveryRate()!=null)
        {
            if(myOrderSingleDetails.getDeliveryRate().equals("")) strDeliveryRate = "0.0";
            else strDeliveryRate = myOrderSingleDetails.getDeliveryRate();
            double deliveryCharges = (Double.parseDouble(strDeliveryRate));
            String strDeliveryCharges = String.format("%,.2f",deliveryCharges);
            StringBuilder strDeliveryChargesTwo= new StringBuilder();
            strDeliveryChargesTwo.append("+").append(strCurrencySymbol).append(strDeliveryCharges);
            binding.tvDeliveryCharges.setText(strDeliveryChargesTwo.toString());
        }

        double balanceToPay = subTotal - discountAmount;
        String strBalanceToPay = String.format("%,.2f",balanceToPay);
        StringBuilder strBalanceAmount = new StringBuilder();
        strBalanceAmount.append(strCurrencySymbol).append(strBalanceToPay);
        binding.tvBalanceToPay.setText(strBalanceAmount.toString());

        double finalOrderTotal = (Double.parseDouble(myOrderSingleDetails.getPayableVatAmount()));
        String strFinalOrderTotal = String.format("%,.2f",finalOrderTotal);
        StringBuilder strFinalPay= new StringBuilder();
        strFinalPay.append(strCurrencySymbol).append(strFinalOrderTotal);
        binding.tvFinalTotalPay.setText(strFinalPay.toString());
    }

    private void showOrderAtTableDataOnly(MyOrderSingleDetails myOrderSingleDetails)
    {
        switch (navigationType)
        {
            case AppUtils.BMT_EATS:
                 binding.tvLblAllergyInstructions.setVisibility(View.VISIBLE);
                 binding.tvAllergyInstructions.setVisibility(View.VISIBLE);
                 break;
            case AppUtils.BMT_LOCAL:
                 switch (myOrders.getRetailerType())
                 {
                     case RETAILER_TYPE_EATS_OD:
                          binding.tvLblAllergyInstructions.setVisibility(View.VISIBLE);
                          binding.tvAllergyInstructions.setVisibility(View.VISIBLE);
                          break;
                     case AppUtils.RETAILER_TYPE_LOCAL_OD:
                          binding.tvLblAllergyInstructions.setVisibility(View.GONE);
                          binding.tvAllergyInstructions.setVisibility(View.GONE);
                          break;
                 }

                 break;
        }
        if((!myOrderSingleDetails.getTableId().equals("0"))||(!myOrderSingleDetails.getStoreid().equals("0")))
        {
            binding.tvDeliveryCharges.setVisibility(View.GONE);
            binding.tvLblDeliveryCharges.setVisibility(View.GONE);

            binding.tvBalanceToPay.setVisibility(View.GONE);
            binding.tvLblBalanceToPay.setVisibility(View.GONE);

            int retailerType = 0;
            if(myOrderSingleDetails.getTableId().equals("0"))
            {
                retailerType = AppUtils.BMT_LOCAL;
            }
            else if(myOrderSingleDetails.getStoreid().equals("0"))
            {
                retailerType = AppUtils.BMT_EATS;
            }
            switch (retailerType)
            {
                case AppUtils.BMT_EATS:
                     binding.tvTextTableId.setText(getString(R.string.lbl_table_id));
                     binding.tvTableId.setText(myOrderSingleDetails.getTableId());
                     binding.tvDescription.setText(myOrderSingleDetails.getDescription());
                     break;
                case AppUtils.BMT_LOCAL:
                     binding.tvTextTableId.setText(getString(R.string.lbl_store_id));
                     binding.tvTableId.setText(myOrderSingleDetails.getStoreid());
                     binding.tvDescription.setText(myOrderSingleDetails.getDescription());
                     break;

            }
            /*if(!myOrderSingleDetails.getTableId().equals("0"))
            {
                binding.tvTableId.setText(getString(R.string.lbl_table_id));
                binding.tvTableId.setText(myOrderSingleDetails.getTableId());
                binding.tvDescription.setText(myOrderSingleDetails.getDescription());
            }
            else if(!myOrderSingleDetails.getTableId().equals("0"))
            {
                binding.tvTableId.setText(getString(R.string.lbl_store_id));
                binding.tvTableId.setText(myOrderSingleDetails.getStoreid());
                binding.tvDescription.setText(myOrderSingleDetails.getDescription());
            }*/

            binding.tvTableId.setVisibility(View.VISIBLE);
            binding.tvTextTableId.setVisibility(View.VISIBLE);

            binding.tvDescription.setVisibility(View.VISIBLE);
            binding.tvTextDescription.setVisibility(View.VISIBLE);
        }
    }

    private void setStatus(String status)
    {
        switch (status)
        {
            case Constants.PENDING:
                 binding.tvStatus.setText(getString(R.string.lbl_waiting_for_confirm));
                 binding.tvStatus.setTextColor(ContextCompat.getColor(requireContext(),R.color.colorBlue));
                 hideAndCancelButton(true);
                 break;
            case Constants.DECLINED:
                 binding.tvStatus.setText(getString(R.string.lbl_declined));
                 binding.tvStatus.setTextColor(ContextCompat.getColor(requireContext(),R.color.colorRed));
                 hideAndCancelButton(false);
                 break;
            case Constants.ORDER_CANCELLED:
                 binding.tvStatus.setText(getString(R.string.lbl_cancelled));
                 binding.tvStatus.setTextColor(ContextCompat.getColor(requireContext(),R.color.colorRed));
                 hideAndCancelButton(false);
                break;
            case Constants.DELIVERED:
                 binding.tvStatus.setText(getString(R.string.lbl_delivered));
                 binding.tvStatus.setTextColor(ContextCompat.getColor(requireContext(),R.color.colorGreen));
                 hideAndCancelButton(false);
                 break;
            case Constants.ACCEPT:
                 binding.tvStatus.setText(getString(R.string.lbl_accepted));
                 binding.tvStatus.setTextColor(ContextCompat.getColor(requireContext(),R.color.colorBlue));
                 hideAndCancelButton(false);
                 break;
        }
    }

    private void hideAndCancelButton(boolean showCancel)
    {
        if(showCancel)
        {
            binding.btnItemCancel.setVisibility(View.VISIBLE);
            binding.tvStatus.setPadding(0,0,0,0);
        }
        else
        {
            binding.btnItemCancel.setVisibility(View.GONE);
            binding.tvStatus.setPadding(0,0,rightPadding,0);
        }
    }

    public void updateAdapter(ArrayList<MyOrderSingleDetails> myOrderSingleDetailsList)
    {
        myOrdersListItemDetailsAdapter.submitList(myOrderSingleDetailsList);
    }

    private void callAPIForCancel(String orderId)
    {
        mProgressDialog.show();

        Call<MyOrdersChangeOrderStatusResponse> dtoCall = apiInterface.
                changeOrderStatus(getChangeOrderStatusPayload(orderId));
        dtoCall.enqueue(new Callback<MyOrdersChangeOrderStatusResponse>()
        {
            @Override
            public void onResponse(Call<MyOrdersChangeOrderStatusResponse> call, Response<MyOrdersChangeOrderStatusResponse> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    MyOrdersChangeOrderStatusResponse myOrdersChangeOrderStatusResponse = response.body();

                    if(myOrdersChangeOrderStatusResponse!=null)
                    {
                        MyOrdersChangeOrderStatusResult myOrdersChangeOrderStatusResult = myOrdersChangeOrderStatusResponse.getMyOrdersChangeOrderStatusResult();

                        if(myOrdersChangeOrderStatusResult!=null)
                        {
                            if(myOrdersChangeOrderStatusResult.errorDetails.errorCode == 0)
                            {
                                //
                                Toast.makeText(getActivity(),myOrdersChangeOrderStatusResult.msg,Toast.LENGTH_SHORT).show();
                                callMyOrdersAPI();
                                updateInDetailsFragment();
                            }
                            else
                            {
                                DialogUtility.showMessageWithOk(myOrdersChangeOrderStatusResult.errorDetails.errorMessage,getActivity());
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MyOrdersChangeOrderStatusResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void updateInDetailsFragment()
    {
        MyOrdersListFragment myOrdersListFragment = (MyOrdersListFragment)
                requireFragmentManager().findFragmentByTag(MyOrdersListFragment.class.getName());

        if(myOrdersListFragment!=null)
        {
            myOrdersListFragment.updateOrderStatusFromOutside();
        }
    }

    private MyOrdersChangeOrderStatusRequest getChangeOrderStatusPayload(String orderId)
    {
        MyOrdersChangeOrderStatusRequest request = new MyOrdersChangeOrderStatusRequest();
        request.setAccountId(userBasicInfo.accountId);
        request.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        request.setRequestFor("ChangeCancelStatusOrderDetails");
        request.setOrderId(orderId);
        return request;
    }

    private void showCancelItemDialog()
    {
        DialogUtility.showMessageWithYesOrNoCallback(getString(R.string.lbl_confirm_cancel_msg_order), getActivity(), new AlertDialogCallBack()
        {

            @Override
            public void onSubmit()
            {
                callAPIForCancel(myOrders.getOrderId());
            }

            @Override
            public void onCancel()
            {

            }
        });
    }
}
