package com.localvalu.eats.myorder.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MyOrdersResponse implements Serializable
{
    @SerializedName("UserDashboardByRequestForDetailsResult")
    @Expose
    private MyOrdersResult myOrdersResult;

    public MyOrdersResult getMyOrdersResult()
    {
        return myOrdersResult;
    }

    public void setMyOrdersResult(MyOrdersResult myOrdersResult)
    {
        this.myOrdersResult = myOrdersResult;
    }
}
