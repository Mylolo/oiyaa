package com.localvalu.eats.myorder.dto.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MyOrdersRequest implements Serializable
{
    @SerializedName("Token")
    @Expose
    private String token;

    @SerializedName("AccountId")
    @Expose
    private String accountId;

    @SerializedName("RequestFor")
    @Expose
    private String requestFor;

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getRequestFor()
    {
        return requestFor;
    }

    public void setRequestFor(String requestFor)
    {
        this.requestFor = requestFor;
    }
}
