package com.localvalu.eats.myorder.dto.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MyOrdersOrderStatus implements Serializable
{
    @SerializedName("Status")
    private String status;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
