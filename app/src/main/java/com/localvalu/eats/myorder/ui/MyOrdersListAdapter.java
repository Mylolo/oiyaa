package com.localvalu.eats.myorder.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.google.android.material.button.MaterialButton;
import com.localvalu.R;
import com.localvalu.eats.myorder.dto.response.MyOrderSingleDetails;
import com.localvalu.eats.myorder.dto.response.MyOrders;
import com.squareup.picasso.Picasso;
import com.utility.AppUtils;
import com.utility.Constants;
import com.utility.DateUtility;

import java.util.ArrayList;

public class MyOrdersListAdapter extends ListAdapter<MyOrders,MyOrdersListAdapter.ViewHolder>
{
    public static final String TAG = MyOrdersListAdapter.class.getSimpleName();
    private Context context;
    private OnItemClickListener onClickListener;
    private int rightPadding;
    private String strCurrencySymbol;
    private String strCurrencyLetter;

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.context = recyclerView.getContext();
        strCurrencyLetter=context.getString(R.string.lbl_currency_letter_euro);
        strCurrencySymbol=context.getString(R.string.lbl_currency_symbol_euro);
    }

    public MyOrdersListAdapter()
    {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<MyOrders> DIFF_CALLBACK = new DiffUtil.ItemCallback<MyOrders>()
    {
        @Override
        public boolean areItemsTheSame(@NonNull MyOrders oldItem, @NonNull MyOrders newItem)
        {
            return oldItem.getOrderId() == newItem.getOrderId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull MyOrders oldItem, @NonNull MyOrders newItem)
        {
            return oldItem.getOrderId().equals(newItem.getOrderId());
        }
    };

    @NonNull
    @Override
    public MyOrdersListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_item_order_list_eat, viewGroup, false);
        return new MyOrdersListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyOrdersListAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") int position)
    {
        MyOrders myOrders = getItem(viewHolder.getAdapterPosition());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemClick(position);
            }
        });
        viewHolder.btnItemCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemCancel(position);
            }
        });

        if(myOrders.getLogo()!=null && (!myOrders.getLogo().isEmpty()))
        {
            Picasso.with(context).load(myOrders.getLogo()).into(viewHolder.iv_pict);
        }
        if (!myOrders.getLogo().equals(""))
        {
            viewHolder.iv_pict.setVisibility(View.VISIBLE);

            Picasso.with(context).load(myOrders.getLogo())
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo ).into(viewHolder.iv_pict);
            viewHolder.iv_pict.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
        else
        {
            Picasso.with(context).load(R.drawable.app_logo)
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo ).into(viewHolder.iv_pict);
            viewHolder.iv_pict.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }
        viewHolder.tv_trading_name.setText(myOrders.getTradingName());

        StringBuilder strOrderId = new StringBuilder();
        strOrderId.append(context.getString(R.string.lbl_order_id_colon)).append(" ").append(myOrders.getOrderId());
        viewHolder.tv_order_id.setText(strOrderId.toString());

        StringBuilder strOrdererdAt = new StringBuilder();
        strOrdererdAt.append("Ordered At : ").append(DateUtility.getFormattedDateTime(myOrders.getPlacedatetime()));

        viewHolder.tv_order_date.setText(strOrdererdAt.toString());

        if(myOrders.getRate()!=null)
        {
            if(!myOrders.getRate().isEmpty())
            {
                viewHolder.rb_reviewRating.setRating(Float.parseFloat(myOrders.getRate()));
            }
        }

        setStatus(viewHolder,myOrders.getStatus());

        StringBuilder strTotalAmount = new StringBuilder();
        strTotalAmount.append(strCurrencySymbol).append(String.format("%,.2f", (Float.parseFloat(myOrders.getOrderTotal()))));
        viewHolder.tv_total_amount.setText(strTotalAmount.toString());
        viewHolder.tv_status.setPadding(0,0,0,0);
    }

    private void setStatus(ViewHolder viewHolder,String status)
    {
        switch (status)
        {
            case Constants.PENDING:
                 viewHolder.tv_status.setText(context.getString(R.string.lbl_waiting_for_confirm));
                 viewHolder.tv_status.setTextColor(ContextCompat.getColor(context,R.color.colorBlue));
                 hideAndCancelButton(viewHolder,true);
                 break;
            case Constants.DECLINED:
                 viewHolder.tv_status.setText(context.getString(R.string.lbl_declined));
                 viewHolder.tv_status.setTextColor(ContextCompat.getColor(context,R.color.colorRed));
                 hideAndCancelButton(viewHolder,false);
                 break;
            case Constants.ORDER_CANCELLED:
                 viewHolder.tv_status.setText(context.getString(R.string.lbl_cancelled));
                 viewHolder.tv_status.setTextColor(ContextCompat.getColor(context,R.color.colorRed));
                 hideAndCancelButton(viewHolder,false);
                 break;
            case Constants.DELIVERED:
                 viewHolder.tv_status.setText(context.getString(R.string.lbl_delivered));
                 viewHolder.tv_status.setTextColor(ContextCompat.getColor(context,R.color.colorGreen));
                 hideAndCancelButton(viewHolder,false);
                 break;
            case Constants.ACCEPT:
                 viewHolder.tv_status.setText(context.getString(R.string.lbl_accepted));
                 viewHolder.tv_status.setTextColor(ContextCompat.getColor(context,R.color.colorBlue));
                 hideAndCancelButton(viewHolder,false);
                 break;
        }
    }

    private void hideAndCancelButton(ViewHolder viewHolder,boolean showCancel)
    {
        if(showCancel)
        {
            viewHolder.btnItemCancel.setVisibility(View.VISIBLE);
            viewHolder.tv_status.setPadding(0,0,0,0);
        }
        else
        {   viewHolder.btnItemCancel.setVisibility(View.GONE);
            viewHolder.tv_status.setPadding(0,0,rightPadding,0);
        }
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemGiveReview(int position);

        void onItemCancel(int position);

        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {

        RatingBar rb_reviewRating = itemView.findViewById(R.id.rb_reviewRating);
        ImageView iv_pict = itemView.findViewById(R.id.iv_pict);

        AppCompatTextView tv_order_id = itemView.findViewById(R.id.tv_order_id);
        AppCompatTextView tv_lbl_status = itemView.findViewById(R.id.tv_category);
        AppCompatTextView tv_status = itemView.findViewById(R.id.tv_status);
        MaterialButton btnItemCancel = itemView.findViewById(R.id.btnItemCancel);
        AppCompatTextView tv_trading_name = itemView.findViewById(R.id.tv_trading_name);
        AppCompatTextView tv_order_date = itemView.findViewById(R.id.tv_order_date);
        AppCompatTextView tv_lbl_total_amount = itemView.findViewById(R.id.tv_lbl_total_amount);
        AppCompatTextView tv_total_amount = itemView.findViewById(R.id.tv_total_amount);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }

    public void updateCancelItem(final int position)
    {
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                MyOrders myOrders = getItem(position);
                myOrders.setStatus(Constants.DECLINED);
                getCurrentList().remove(position);
                getCurrentList().add(position,myOrders);
                submitList(getCurrentList());
            }
        },200);
    }
}
