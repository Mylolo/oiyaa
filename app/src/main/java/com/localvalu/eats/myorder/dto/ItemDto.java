package com.localvalu.eats.myorder.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ItemDto implements Serializable {

    @SerializedName("item_name")
    @Expose
    public String itemName;
    @SerializedName("quantity")
    @Expose
    public String quantity;
    @SerializedName("single_quantity_price")
    @Expose
    public String itemSinglePrice;
    @SerializedName("item_total_price")
    @Expose
    public String itemTotalPrice;
    @SerializedName("food_desc")
    @Expose
    public String foodDesc;
    @SerializedName("food_type")
    @Expose
    public String foodType;
    @SerializedName("food_pic")
    @Expose
    public String foodPic;
    @SerializedName("category_name")
    @Expose
    public String categoryName;
    @SerializedName("category_path")
    @Expose
    public String categoryPath;
}
