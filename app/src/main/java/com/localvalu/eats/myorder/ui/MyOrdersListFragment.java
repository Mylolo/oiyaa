package com.localvalu.eats.myorder.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.MainActivity;
import com.localvalu.base.HostActivityListener;
import com.localvalu.eats.myorder.dto.request.MyOrdersChangeOrderStatusRequest;
import com.localvalu.eats.myorder.dto.request.MyOrdersRequest;
import com.localvalu.eats.myorder.dto.response.MyOrders;
import com.localvalu.eats.myorder.dto.response.MyOrdersChangeOrderStatusResponse;
import com.localvalu.eats.myorder.dto.response.MyOrdersChangeOrderStatusResult;
import com.localvalu.eats.myorder.dto.response.MyOrdersResponse;
import com.localvalu.eats.myorder.dto.response.MyOrdersResult;
import com.localvalu.common.review.ReviewsAddFragment;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AlertDialogCallBack;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.Constants;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyOrdersListFragment extends BaseFragment
{
    private static final String TAG = "MyOrdersListFragment";

    //Toolbar
    private Toolbar toolbar;
    private TextView tvTitle;
    private int colorTheme;

    private RecyclerView rv_lists;
    private TextView tv_error;
    private GridLayoutManager gridLayoutManager;
    private MyOrdersListAdapter myOrdersListAdapter;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private HostActivityListener hostActivityListener;
    private ArrayList<MyOrders> myOrdersList = new ArrayList<>();
    private int navigationType;
    private int selectedPosition =-1;

    // TODO: Rename and change types and number of parameters
    public static MyOrdersListFragment newInstance(int navigationType)
    {
        Bundle bundle = new Bundle();
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,navigationType);
        MyOrdersListFragment myOrdersListFragment = new MyOrdersListFragment();
        myOrdersListFragment.setArguments(bundle);
        return myOrdersListFragment;
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");

        if(getArguments()!=null)
        {
            navigationType = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);

            switch (navigationType)
            {
                case AppUtils.BMT_EATS:
                     colorTheme = ContextCompat.getColor(requireContext(), R.color.colorEats);
                     break;
                case AppUtils.BMT_LOCAL:
                     colorTheme = ContextCompat.getColor(requireContext(),R.color.colorLocal);
                     break;

            }

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_my_orders_list_eat, container, false);
        buildObjectForHandlingAPI();
        initView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setAdapter();
        tv_error.setVisibility(View.GONE);
        callMyOrdersAPI();
    }

    private void initView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        rv_lists = view.findViewById(R.id.rv_lists);
        tv_error = view.findViewById(R.id.tv_error);
    }

    private void setUpActionBar()
    {
        toolbar = view.findViewById(R.id.toolbar);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setTextColor(colorTheme);
        tvTitle.setText(getString(R.string.lbl_my_orders));
        toolbar.getNavigationIcon().setColorFilter(colorTheme, PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private MyOrdersRequest getMyOrderPayload()
    {
        MyOrdersRequest myOrdersRequest = new MyOrdersRequest();
        myOrdersRequest.setAccountId(userBasicInfo.accountId);
        myOrdersRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        switch (navigationType)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
            case AppUtils.BMT_EATS:
                 myOrdersRequest.setRequestFor("MyOrders");
                 break;
            case AppUtils.BMT_LOCAL:
                 myOrdersRequest.setRequestFor("MyOrdersLocal");
                 break;
        }
        return myOrdersRequest;
    }

    private void callMyOrdersAPI()
    {
        mProgressDialog.show();

        Call<MyOrdersResponse> dtoCall = apiInterface.getMyOrders(getMyOrderPayload());
        dtoCall.enqueue(new Callback<MyOrdersResponse>()
        {
            @Override
            public void onResponse(Call<MyOrdersResponse> call, Response<MyOrdersResponse> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    MyOrdersResponse myOrdersResponse = response.body();

                    if(myOrdersResponse!=null)
                    {
                        MyOrdersResult myOrdersResult = myOrdersResponse.getMyOrdersResult();

                        if(myOrdersResult!=null)
                        {
                            if(myOrdersResult.errorDetails.errorCode == 0)
                            {
                                ArrayList<MyOrders> myOrdersList = myOrdersResult.getMyOrders();

                                if(myOrdersList.size()>0)
                                {
                                    updateAdapter(myOrdersList);
                                    rv_lists.setVisibility(View.VISIBLE);
                                    tv_error.setVisibility(View.GONE);
                                }
                                else
                                {
                                    rv_lists.setVisibility(View.GONE);
                                    tv_error.setVisibility(View.VISIBLE);
                                }
                            }
                            else
                            {
                                DialogUtility.showMessageWithOk(myOrdersResult.errorDetails.errorMessage,getActivity());
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MyOrdersResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void setAdapter()
    {
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        rv_lists.setLayoutManager(gridLayoutManager);
        myOrdersListAdapter = new MyOrdersListAdapter();
        myOrdersListAdapter.setOnClickListener(new MyOrdersListAdapter.OnItemClickListener()
        {
            @Override
            public void onItemGiveReview(final int position)
            {
                selectedPosition=position;
                MyOrders myOrderDetail = myOrdersList.get(position);
                ReviewsAddFragment reviewsAddFragment = ReviewsAddFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("section_type", "order");
                bundle.putString("retailer_id", myOrderDetail.getBusinessId());
                bundle.putString("order_id", myOrderDetail.getOrderId());
                bundle.putString("businessImage", myOrderDetail.getLogo());
                bundle.putString("businessName", myOrderDetail.getTradingName());
                bundle.putString("businessType", myOrderDetail.getRetailerType());
                bundle.putString("businessAddress", myOrderDetail.getTradingName());
                reviewsAddFragment.setArguments(bundle);
                ((MainActivity) getActivity()).addFragment(reviewsAddFragment, true, ReviewsAddFragment.class.getName(), true);
            }

            @Override
            public void onItemCancel(final int position)
            {
                selectedPosition=position;
                DialogUtility.showMessageWithYesOrNoCallback(getString(R.string.lbl_confirm_cancel_msg_order), getActivity(), new AlertDialogCallBack()
                {

                    @Override
                    public void onSubmit()
                    {
                        MyOrders myOrderDetail = myOrdersList.get(position);
                        callAPIForCancel(myOrderDetail.getOrderId(),position);
                    }

                    @Override
                    public void onCancel()
                    {

                    }
                });
            }

            @Override
            public void onItemClick(int position)
            {
                selectedPosition=position;
                MyOrders myOrderDetail = myOrdersList.get(position);
                MyOrdersDetailsFragment orderDetailFragment = MyOrdersDetailsFragment.newInstance(myOrderDetail,navigationType);
                hostActivityListener.updateFragment(orderDetailFragment, true, MyOrdersDetailsFragment.class.getName(), true);
            }
        });
        rv_lists.setAdapter(myOrdersListAdapter);
    }

    private void updateAdapter(final ArrayList<MyOrders> detail)
    {
        myOrdersList = detail;
        myOrdersListAdapter.submitList(detail);
    }

    private MyOrdersChangeOrderStatusRequest getChangeOrderStatusPayload(String orderId)
    {
        MyOrdersChangeOrderStatusRequest request = new MyOrdersChangeOrderStatusRequest();
        request.setAccountId(userBasicInfo.accountId);
        request.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        request.setRequestFor("ChangeCancelStatusOrderDetails");
        request.setOrderId(orderId);
        return request;
    }

    private void callAPIForCancel(String orderId, final int position)
    {
        mProgressDialog.show();

        Call<MyOrdersChangeOrderStatusResponse> dtoCall = apiInterface.
                changeOrderStatus(getChangeOrderStatusPayload(orderId));
        dtoCall.enqueue(new Callback<MyOrdersChangeOrderStatusResponse>()
        {
            @Override
            public void onResponse(Call<MyOrdersChangeOrderStatusResponse> call, Response<MyOrdersChangeOrderStatusResponse> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    MyOrdersChangeOrderStatusResponse myOrdersChangeOrderStatusResponse = response.body();

                    if(myOrdersChangeOrderStatusResponse!=null)
                    {
                        MyOrdersChangeOrderStatusResult myOrdersChangeOrderStatusResult = myOrdersChangeOrderStatusResponse.getMyOrdersChangeOrderStatusResult();

                        if(myOrdersChangeOrderStatusResult!=null)
                        {
                            if(myOrdersChangeOrderStatusResult.errorDetails.errorCode == 0)
                            {
                                //
                                updateOrderStatus(position);
                                Toast.makeText(getActivity(),myOrdersChangeOrderStatusResult.msg,Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                DialogUtility.showMessageWithOk(myOrdersChangeOrderStatusResult.errorDetails.errorMessage,getActivity());
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MyOrdersChangeOrderStatusResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void updateOrderStatus(int position)
    {
        MyOrders myOrders = myOrdersList.get(position);
        myOrders.setStatus(Constants.ORDER_CANCELLED);
        myOrdersList.remove(position);
        myOrdersList.add(position,myOrders);
        myOrdersListAdapter.submitList(myOrdersList);
        myOrdersListAdapter.notifyDataSetChanged();
    }

    public void updateOrderStatusFromOutside()
    {
        if(selectedPosition!=-1)
        {
            updateOrderStatus(selectedPosition);
        }

    }

}
