package com.localvalu.eats.myorder.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MyOrderSingleDetails implements Serializable
{
    @SerializedName("OrderDetailsId")
    @Expose
    private String OrderDetailsId;

    @SerializedName("item_id")
    @Expose
    private String itemId;

    @SerializedName("item_name")
    @Expose
    private String itemName;

    @SerializedName("quantity")
    @Expose
    private String quantity;

    @SerializedName("single_quantity_price")
    @Expose
    private String singleQuantityPrice;

    @SerializedName("item_total_price")
    @Expose
    private String itemTotalPrice;

    @SerializedName("order_id")
    @Expose
    private String orderId;

    @SerializedName("DiscountLtAmount")
    @Expose
    private String discountLtAmount;

    @SerializedName("PayableVatAmount")
    @Expose
    private String payableVatAmount;

    @SerializedName("OrderId")
    @Expose
    private String OrderId;

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("businessId")
    @Expose
    private String businessId;

    @SerializedName("orderTotal")
    @Expose
    private String orderTotal;

    @SerializedName("sizeName")
    @Expose
    private String sizeName;

    @SerializedName("size_id")
    @Expose
    private String sizeId;

    @SerializedName("deliveryRate")
    @Expose
    private String deliveryRate;

    @SerializedName("tax")
    @Expose
    private String tax;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("isActive")
    @Expose
    private String isActive;

    @SerializedName("Placedatetime")
    @Expose
    private String placedatetime;

    @SerializedName("is_order_placed")
    @Expose
    private String isOrderPlaced;

    /**
     * Used to get order status
     */
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("is_order_confirmed_by_rest")
    @Expose
    private String isOrderConfirmedByRest;

    @SerializedName("special_ins")
    @Expose
    private String specialInstructions;

    @SerializedName("allergy_ins")
    @Expose
    private String allergyInstructions;

    @SerializedName("orderType")
    @Expose
    private String orderType;

    @SerializedName("tableid")
    @Expose
    private String tableId;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("orderType1")
    @Expose
    private String orderType1;

    @SerializedName("orderTypeLocal")
    @Expose
    private String orderTypeLocal;

    @SerializedName("orderTypeLocal1")
    @Expose
    private String orderTypeLocal1;

    @SerializedName("storeid")
    @Expose
    private String storeid;


    public String getOrderDetailsId()
    {
        return OrderDetailsId;
    }

    public void setOrderDetailsId(String orderDetailsId)
    {
        OrderDetailsId = orderDetailsId;
    }

    public String getItemId()
    {
        return itemId;
    }

    public void setItemId(String itemId)
    {
        this.itemId = itemId;
    }

    public String getItemName()
    {
        return itemName;
    }

    public void setItemName(String itemName)
    {
        this.itemName = itemName;
    }

    public String getQuantity()
    {
        return quantity;
    }

    public void setQuantity(String quantity)
    {
        this.quantity = quantity;
    }

    public String getSingleQuantityPrice()
    {
        return singleQuantityPrice;
    }

    public void setSingleQuantityPrice(String singleQuantityPrice)
    {
        this.singleQuantityPrice = singleQuantityPrice;
    }

    public String getItemTotalPrice()
    {
        return itemTotalPrice;
    }

    public void setItemTotalPrice(String itemTotalPrice)
    {
        this.itemTotalPrice = itemTotalPrice;
    }

    public String getOrderId()
    {
        return orderId;
    }

    public void setOrderId(String orderId)
    {
        this.orderId = orderId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getBusinessId()
    {
        return businessId;
    }

    public void setBusinessId(String businessId)
    {
        this.businessId = businessId;
    }

    public String getOrderTotal()
    {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal)
    {
        this.orderTotal = orderTotal;
    }

    public String getSizeName()
    {
        return sizeName;
    }

    public void setSizeName(String sizeName)
    {
        this.sizeName = sizeName;
    }

    public String getSizeId()
    {
        return sizeId;
    }

    public void setSizeId(String sizeId)
    {
        this.sizeId = sizeId;
    }

    public String getDeliveryRate()
    {
        return deliveryRate;
    }

    public void setDeliveryRate(String deliveryRate)
    {
        this.deliveryRate = deliveryRate;
    }

    public String getTax()
    {
        return tax;
    }

    public void setTax(String tax)
    {
        this.tax = tax;
    }

    public String getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(String createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getIsActive()
    {
        return isActive;
    }

    public void setIsActive(String isActive)
    {
        this.isActive = isActive;
    }

    public String getPlacedatetime()
    {
        return placedatetime;
    }

    public void setPlacedatetime(String placedatetime)
    {
        this.placedatetime = placedatetime;
    }

    public String getIsOrderPlaced()
    {
        return isOrderPlaced;
    }

    public void setIsOrderPlaced(String isOrderPlaced)
    {
        this.isOrderPlaced = isOrderPlaced;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getIsOrderConfirmedByRest()
    {
        return isOrderConfirmedByRest;
    }

    public void setIsOrderConfirmedByRest(String isOrderConfirmedByRest)
    {
        this.isOrderConfirmedByRest = isOrderConfirmedByRest;
    }

    public String getSpecialInstructions()
    {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions)
    {
        this.specialInstructions = specialInstructions;
    }

    public String getAllergyInstructions()
    {
        return allergyInstructions;
    }

    public void setAllergyInstructions(String allergyInstructions)
    {
        this.allergyInstructions = allergyInstructions;
    }

    public String getOrderType()
    {
        return orderType;
    }

    public void setOrderType(String orderType)
    {
        this.orderType = orderType;
    }

    public String getDiscountLtAmount()
    {
        return discountLtAmount;
    }

    public void setDiscountLtAmount(String discountLtAmount)
    {
        this.discountLtAmount = discountLtAmount;
    }

    public String getPayableVatAmount()
    {
        return payableVatAmount;
    }

    public void setPayableVatAmount(String payableVatAmount)
    {
        this.payableVatAmount = payableVatAmount;
    }

    public String getTableId()
    {
        return tableId;
    }

    public void setTableId(String tableId)
    {
        this.tableId = tableId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getOrderType1()
    {
        return orderType1;
    }

    public void setOrderType1(String orderType1)
    {
        this.orderType1 = orderType1;
    }

    public String getOrderTypeLocal()
    {
        return orderTypeLocal;
    }

    public void setOrderTypeLocal(String orderTypeLocal)
    {
        this.orderTypeLocal = orderTypeLocal;
    }

    public String getOrderTypeLocal1()
    {
        return orderTypeLocal1;
    }

    public void setOrderTypeLocal1(String orderTypeLocal1)
    {
        this.orderTypeLocal1 = orderTypeLocal1;
    }

    public String getStoreid()
    {
        return storeid;
    }

    public void setStoreid(String storeid)
    {
        this.storeid = storeid;
    }

}
