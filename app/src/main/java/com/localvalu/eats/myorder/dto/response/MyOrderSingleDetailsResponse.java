package com.localvalu.eats.myorder.dto.response;

import com.google.gson.annotations.SerializedName;

public class MyOrderSingleDetailsResponse
{
    @SerializedName("UserDashboardByRequestForDetailsResult")
    private MyOrdersSingleDetailsResult myOrdersSingleDetailsResult;

    public MyOrdersSingleDetailsResult getMyOrdersSingleDetailsResult()
    {
        return myOrdersSingleDetailsResult;
    }

    public void setMyOrdersSingleDetailsResult(MyOrdersSingleDetailsResult myOrdersSingleDetailsResult)
    {
        this.myOrdersSingleDetailsResult = myOrdersSingleDetailsResult;
    }

}
