package com.localvalu.eats.myorder.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;
import java.util.ArrayList;

public class MyOrdersSingleDetailsResult implements Serializable
{
    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("msg")
    @Expose
    public String msg;

    @SerializedName("SingleOrderDetailsResult")
    @Expose
    private ArrayList<MyOrderSingleDetails> myOrderSingleDetails;

    public ErrorDetailsDto getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetailsDto errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public ArrayList<MyOrderSingleDetails> getMyOrderSingleDetails()
    {
        return myOrderSingleDetails;
    }

    public void setMyOrderSingleDetails(ArrayList<MyOrderSingleDetails> myOrderSingleDetails)
    {
        this.myOrderSingleDetails = myOrderSingleDetails;
    }
}
