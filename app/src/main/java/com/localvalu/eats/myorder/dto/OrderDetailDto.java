package com.localvalu.eats.myorder.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderDetailDto implements Serializable {

    @SerializedName("business_id")
    @Expose
    public String businessId;
    @SerializedName("order_id")
    @Expose
    public String orderId;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("RetailerId")
    @Expose
    public String retailerId;
    @SerializedName("orderTotal")
    @Expose
    public String orderTotal;
    @SerializedName("totalLolaltyPointRedeemed")
    @Expose
    public String totalLolaltyPointRedeemed;
    @SerializedName("deliveryRate")
    @Expose
    public String deliveryRate;
    @SerializedName("tax")
    @Expose
    public String tax;
    @SerializedName("datetime")
    @Expose
    public String dateTime;
    @SerializedName("customer_name")
    @Expose
    public String customerName;
    @SerializedName("customer_mobile")
    @Expose
    public String customerMobile;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("business_name")
    @Expose
    public String businessName;

    @SerializedName("business_logo")
    @Expose
    public String businessLogo;
    @SerializedName("business_address")
    @Expose
    public String businessAddress;
    @SerializedName("business_contact_name")
    @Expose
    public String businessContactName;
    @SerializedName("is_order_confirmed_by_rest")
    @Expose
    public String is_order_confirmed_by_rest;
    @SerializedName("business_contact_phone")
    @Expose
    public String businessContactPhone;
    @SerializedName("items")
    @Expose
    public ArrayList<ItemDto> items;
    @SerializedName("OverAllBusinessRate")
    @Expose
    public RatingDto rating;
    @SerializedName("customer_review")
    @Expose
    public OrderRatingDto customerRating;
}
