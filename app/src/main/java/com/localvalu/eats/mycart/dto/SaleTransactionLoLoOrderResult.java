package com.localvalu.eats.mycart.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;

public class SaleTransactionLoLoOrderResult implements Serializable {

    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("Token_Applied")
    @Expose
    public String tokenApplied;
    @SerializedName("Bonus_Tokens")
    @Expose
    public String bonusTokens;
    @SerializedName("Balance_ToPay")
    @Expose
    public String balanceToPay;
    @SerializedName("Total")
    @Expose
    public String total;
    @SerializedName("NewToken_Balance")
    @Expose
    public String newTokenBalance;
    @SerializedName("Tokens_earned")
    @Expose
    public String tokensEarned;
    @SerializedName("TokenClosingBalance")
    @Expose
    public int tokenClosingBalance;
    @SerializedName("LisamoBonus")
    @Expose
    public float lisamoBonus;
    @SerializedName("AppWalletCredit")
    @Expose
    public float appWalletCredit;
    @SerializedName("TokenRedeemedValue")
    @Expose
    public float tokenRedeemedValue;
    @SerializedName("DeviceType")
    @Expose
    public String deviceType;
    @SerializedName("machineaddress")
    @Expose
    public String machineAddress;
    @SerializedName("PrintUrl")
    @Expose
    public String printUrl;
    @SerializedName("message")
    @Expose
    public String message;
}
