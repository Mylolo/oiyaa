package com.localvalu.eats.mycart;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.appbar.AppBarLayout;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.MainActivity;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentEatsCheckoutTableBinding;
import com.localvalu.eats.detail.wrapper.OrderNowResultResponse;
import com.localvalu.eats.mycart.adapter.CheckoutItemListAdapter;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.eats.mycart.dto.CheckoutDto;
import com.localvalu.eats.payment.ChoosePaymentFragmentTable;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.squareup.picasso.Picasso;
import com.utility.AppUtils;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckOutTableFragment extends BaseFragment
{
    private static final String TAG = CheckOutTableFragment.class.getSimpleName();

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 12345;

    private FragmentEatsCheckoutTableBinding binding;
    private int colorEats;
    private CheckoutItemListAdapter checkoutItemListAdapter;
    private LinearLayoutManager linearLayoutManager;
    private String strOrderTotal, businessId, strDeliveryRate, tax, delivery, special_ins, allergy_ins;
    private boolean locationEnabled = false;

    //For API Call
    public ProgressDialog mProgressDialog;
    public ApiInterface apiInterface;
    private UserBasicInfo userBasicInfo;
    private ArrayList<CartDetailDto> cartDetails;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private String currentDate;
    private FusedLocationProviderClient fusedLocationClient;
    private Boolean mLocationPermissionsGranted = false;
    private Double latitude, longitude;
    private boolean isApiCalled = false;
    private CheckoutDto checkoutDto;
    private HostActivityListener hostActivityListener;

    public static CheckOutTableFragment newInstance(CheckoutDto checkoutDto)
    {
        CheckOutTableFragment checkOutFragment = new CheckOutTableFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppUtils.BUNDLE_CHECK_OUT,checkoutDto);
        checkOutFragment.setArguments(bundle);
        return checkOutFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        buildObjectForHandlingAPI();
        readFromBundle();
        getLocationPermission();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date cal = (Date) Calendar.getInstance().getTime();
        currentDate = formatter.format(cal);
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
    }

    public void setGPS()
    {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            getLocationPermission();
            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>()
                {
                    @Override
                    public void onSuccess(Location location)
                    {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null)
                        {
                            // Logic to handle location object
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                            Log.d(TAG, "onSuccess: latitude-" + latitude);
                            Log.d(TAG, "onSuccess: longitude-" + longitude);
                            locationEnabled = true;
                            if (isAdded() && locationEnabled && (!isApiCalled))
                            {
                                callAPI();
                            }
                        }
                        else
                        {
                            Log.d(TAG, "onSuccess: location - " + null);
                            locationEnabled = false;
                        }
                    }
                });

    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = FragmentEatsCheckoutTableBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
    }

    private void readFromBundle()
    {
        checkoutDto = (CheckoutDto) getArguments().getSerializable(AppUtils.BUNDLE_CHECK_OUT);
        cartDetails = checkoutDto.cartList;
        strOrderTotal = checkoutDto.orderTotal;
        businessId = checkoutDto.businessDetailInfo.business_id;
        strDeliveryRate = checkoutDto.deliveryRate;
        tax = checkoutDto.tax;
        delivery = checkoutDto.delivery;
        special_ins = checkoutDto.specialInstructions;
        allergy_ins = checkoutDto.allergyInstructions;

        System.out.println("businessId " + businessId);
    }

    private void getLocationPermission()
    {
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                mLocationPermissionsGranted = true;
                setGPS();
            }
            else
            {
                ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSION_REQUEST_CODE);
            }
        }
        else
        {
            ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionsGranted = false;

        switch (requestCode)
        {
            case LOCATION_PERMISSION_REQUEST_CODE:
            {
                if (grantResults.length > 0)
                {
                    for (int i = 0; i < grantResults.length; i++)
                    {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                        {
                            mLocationPermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionsGranted = true;
                    setGPS();
                    //initialize our map
                }
            }
            break;

        }
    }

    private void setProperties()
    {
        linearLayoutManager = new LinearLayoutManager(requireContext());
        binding.rvCartItems.setLayoutManager(linearLayoutManager);
        setAdapter(cartDetails);
        binding.btnContinue.setOnClickListener(_OnClickListener);
    }

    /**
     * Set Basic Image
     * @param
     */
    public void setBasicInfo()
    {
        binding.toolbarLayout.appBarEatDetails.setVisibility(View.VISIBLE);
        binding.tvName.setText(checkoutDto.businessDetailInfo.tradingname);

        if (!checkoutDto.businessDetailInfo.currentPromotionValue.equals(""))
        {
            binding.toolbarLayout.tvCurrentPromotion.setText(checkoutDto.businessDetailInfo.currentPromotionValue + "% Off");
        }
        else
        {
            binding.toolbarLayout.tvCurrentPromotion.setText("0% Off");
        }
        binding.toolbarLayout.cnlInner.transitionToStart();
        binding.toolbarLayout.cnlInner.transitionToEnd();
        if (!checkoutDto.businessDetailInfo.businessCoverImage.equals(""))
        {
            binding.toolbarLayout.ivCoverImage.setVisibility(View.VISIBLE);

            Picasso.with(getActivity()).load(checkoutDto.businessDetailInfo.businessCoverImage)
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo ).into(binding.toolbarLayout.ivCoverImage);
            binding.toolbarLayout.ivCoverImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }
        else
        {
            Picasso.with(getActivity()).load(R.drawable.app_logo)
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo ).into(binding.toolbarLayout.ivCoverImage);
            binding.toolbarLayout.ivCoverImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }

    }

    private void setUpActionBar()
    {
        int collapsedColor = ContextCompat.getColor(requireContext(), R.color.colorEats);
        int expandedColor = ContextCompat.getColor(requireContext(), R.color.colorWhite);
        binding.toolbarLayout.toolbarEats.getNavigationIcon().setColorFilter(expandedColor, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.btnActionContactMe.setVisibility(View.GONE);
        binding.toolbarLayout.toolbarEats.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });
        binding.toolbarLayout.appBarEatDetails.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener()
        {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset)
            {
                float percentage = ((float)Math.abs(verticalOffset)/appBarLayout.getTotalScrollRange());
                binding.toolbarLayout.tvCurrentPromotion.setScaleX(1-percentage);
                binding.toolbarLayout.tvCurrentPromotion.setScaleY(1-percentage);
                binding.toolbarLayout.cnlInner.setAlpha(0.9f-percentage);
                if(percentage==0)
                {
                    binding.toolbarLayout.btnActionContactMe.setTextColor(expandedColor);
                    binding.toolbarLayout.btnActionContactMe.getIcon().setColorFilter(expandedColor, PorterDuff.Mode.SRC_ATOP);
                    binding.toolbarLayout.toolbarEats.getNavigationIcon().setColorFilter(expandedColor, PorterDuff.Mode.SRC_ATOP);
                    binding.toolbarLayout.viewSeparator.setVisibility(View.GONE);
                }
                else if(percentage==1)
                {
                    binding.toolbarLayout.btnActionContactMe.setTextColor(collapsedColor);
                    binding.toolbarLayout.btnActionContactMe.getIcon().setColorFilter(collapsedColor, PorterDuff.Mode.SRC_ATOP);
                    binding.toolbarLayout.toolbarEats.getNavigationIcon().setColorFilter(collapsedColor, PorterDuff.Mode.SRC_ATOP);
                    binding.toolbarLayout.viewSeparator.setVisibility(View.VISIBLE);
                }
            }
        });
        binding.toolbarLayout.appBarEatDetails.setVisibility(View.GONE);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    private void setAdapter(ArrayList<CartDetailDto> cartDetails)
    {
        checkoutItemListAdapter = new CheckoutItemListAdapter(getActivity(), cartDetails);
        checkoutItemListAdapter.setOnClickListener(new CheckoutItemListAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClickMinus(int position)
            {

            }

            @Override
            public void onItemClickPlus(int position)
            {

            }
        });
        binding.rvCartItems.setAdapter(checkoutItemListAdapter);
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    private void callAPI()
    {
        isApiCalled = true;
        showProgress();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId",checkoutDto.businessDetailInfo.business_id);
            objMain.put("requestFor", "PaymentOrderdetails");
            objMain.put("ordertype", delivery);
            objMain.put("Distance", "12");
            objMain.put("latitude",Double.toString(latitude));
            objMain.put("longitude",Double.toString(longitude));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<OrderNowResultResponse> dtoCall = null;

        if(checkoutDto.isOrderFromEats)
        {
            if(checkoutDto.isOrderFromTableOrStore)
            {
                dtoCall = apiInterface.addToCartTable(body);
            }
            else
            {
                dtoCall = apiInterface.orderAtStoreLocal(body);
            }
        }
        else
        {
            dtoCall = apiInterface.orderNowTable(body);
        }

        dtoCall.enqueue(new Callback<OrderNowResultResponse>()
        {
            @Override
            public void onResponse(Call<OrderNowResultResponse> call, Response<OrderNowResultResponse> response)
            {
                try
                {
                    if (response.body().paymentOrderdetails.errorDetails.errorCode == 0)
                    {
                        showContents();
                        setBasicInfo();

                        float orderTotal = Float.parseFloat(response.body().paymentOrderdetails.paymentOrderDto.paymentDto.orderTotal);
                        float tokenApplied = Float.parseFloat(response.body().paymentOrderdetails.paymentOrderDto.paymentDto.discountAmount);
                        float deliveryRate = Float.parseFloat(response.body().paymentOrderdetails.paymentOrderDto.paymentDto.deliveryRate);
                        float payableAmount = Float.parseFloat(response.body().paymentOrderdetails.paymentOrderDto.paymentDto.payableAmount);
                        float orderAmount = payableAmount-deliveryRate;
                        binding.tvTotalPrice.setText(strCurrencySymbol + String.format("%,.2f", (orderTotal)));
                        binding.tvTokenApplied.setText(strCurrencyLetter + String.format("%,.2f", (tokenApplied)));
                        binding.tvPayableAmount.setText(strCurrencySymbol + String.format("%,.2f", payableAmount));
                        strDeliveryRate = String.format("%,.2f", (0.00f));
                        checkoutDto.deliveryRate = strDeliveryRate;
                        checkoutDto.payableAmount = String.format("%,.2f", payableAmount);
                        checkoutDto.orderAmount = String.format("%,.2f", (orderAmount));
                    }
                    else
                    {
                        showError(response.body().paymentOrderdetails.errorDetails.errorMessage,false);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<OrderNowResultResponse> call, Throwable t)
            {
                Log.d(TAG, t.getMessage());
                if(t instanceof IOException) showError(getString(R.string.network_unavailable),true);
                else showError(getString(R.string.server_alert),false);
            }
        });
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                 hostActivityListener.onBackButtonPressed();
                 break;
            case R.id.btnContinue:
                 String businessDiscount = checkoutDto.businessDetailInfo.currentPromotionValue;
                 checkoutDto.totalLoyaltyPointsRedeemed = String.format("%,.2f", (Float.parseFloat(strOrderTotal) * Float.parseFloat((businessDiscount.equals("") ? "0" : businessDiscount)) / 100));
                 ChoosePaymentFragmentTable choosePaymentFragmentTable = ChoosePaymentFragmentTable.newInstance(checkoutDto);
                 hostActivityListener.updateFragment(choosePaymentFragmentTable, true, ChoosePaymentFragmentTable.class.getName(), true);
                 break;
            case R.id.btnRetry:
                 callAPI();
                 break;
        }
    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.nestedScrollView.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showContents()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.nestedScrollView.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage,boolean retry)
    {
        binding.layoutProgress. progressBar.setVisibility(View.GONE);
        binding.nestedScrollView.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if(retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

    @Override
    public void locationEnabled(double latitude, double longitude)
    {
        super.locationEnabled(latitude, longitude);
        if (!locationEnabled)
        {
            getLocationPermission();
        }
    }

}
