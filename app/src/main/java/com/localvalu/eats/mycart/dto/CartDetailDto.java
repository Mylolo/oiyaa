package com.localvalu.eats.mycart.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CartDetailDto implements Serializable {

    @SerializedName("FoodItemId")
    @Expose
    public String foodItemId;

    @SerializedName("foodMenuId")
    @Expose
    public String foodMenuId;

    @SerializedName("menuName")
    @Expose
    public String menuName;

    @SerializedName("purpose")
    @Expose
    public String purpose;

    @SerializedName("BusinessId")
    @Expose
    public String businessId;

    @SerializedName("itemName")
    @Expose
    public String itemName;

    @SerializedName("foodDesc")
    @Expose
    public String foodDesc;

    @SerializedName("user_id")
    @Expose
    public String user_id;

    @SerializedName("quantity")
    @Expose
    public String quantity;

    @SerializedName("rate")
    @Expose
    public String rate;

    @SerializedName("SizeName")
    @Expose
    public String sizeName;

    @SerializedName("Size_id")
    @Expose
    public String sizeId;
}
