package com.localvalu.eats.mycart.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.mycart.dto.CartChangeQuantityResult;

import java.io.Serializable;

public class CartChangeQuantityResultWrapper implements Serializable {

    @SerializedName("Cart")
    @Expose
    public CartChangeQuantityResult cartChangeQuantityResult;
}
