package com.localvalu.eats.mycart.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RedeemableCartAmountDto implements Serializable {

    @SerializedName("redeemable_cart_amount_percent")
    @Expose
    public String redeemableCartAmountPercent;
    @SerializedName("redeemable_cart_amount_fixed")
    @Expose
    public String redeemableCartAmountFixed;
}
