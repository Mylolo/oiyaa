package com.localvalu.eats.mycart.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;
import java.util.ArrayList;

public class CartDetailResult implements Serializable {

    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("CartDetails")
    @Expose
    public ArrayList<CartDetailDto> cartDetails;
    @SerializedName("orderTotal")
    @Expose
    public String orderTotal;
    @SerializedName("businessId")
    @Expose
    public String businessId;
    @SerializedName("business_name")
    @Expose
    public String businessName;
    @SerializedName("totalLolaltyPoint")
    @Expose
    public String totalLolaltyPoint;
    @SerializedName("redeemableCartAmount")
    @Expose
    public RedeemableCartAmountDto redeemableCartAmount;
    @SerializedName("deliveryRate")
    @Expose
    public String deliveryRate;
    @SerializedName("tax")
    @Expose
    public String tax;
}
