package com.localvalu.eats.mycart.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.eats.mycart.dto.CartDetailDto;

import java.util.ArrayList;

public class CartItemListAdapter extends RecyclerView.Adapter<CartItemListAdapter.ViewHolder>
{
    public static final String TAG = CartItemListAdapter.class.getSimpleName();
    private ArrayList<CartDetailDto> cartDetails;
    private OnItemClickListener onClickListener;

    public CartItemListAdapter(Activity activity, ArrayList<CartDetailDto> cartDetails)
    {
        this.cartDetails = cartDetails;
    }

    @NonNull
    @Override
    public CartItemListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cart_item_list, viewGroup, false);
        return new CartItemListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CartItemListAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i)
    {
        final CartDetailDto cartDetailDto = cartDetails.get(i);
        StringBuilder strTitle = new StringBuilder();
        strTitle.append(cartDetails.get(i).itemName);
        if(!cartDetailDto.sizeName.equals(""))
        {
            strTitle.append("(").append(cartDetailDto.sizeName).append(")");
        }
        viewHolder.tvItemTitle.setText(strTitle.toString());
        viewHolder.tvNoOfItems.setText(cartDetailDto.quantity);
        viewHolder.tvTotalPrice.setText("£" + cartDetailDto.rate);
        viewHolder.tvMinus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemClickMinus(i);
            }
        });
        viewHolder.tvPlus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemClickPlus(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return cartDetails.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemClickMinus(int position);

        void onItemClickPlus(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvItemTitle = itemView.findViewById(R.id.tvItemTitle);
        TextView tvNoOfItems = itemView.findViewById(R.id.tvNoOfItems);
        TextView tvMinus = itemView.findViewById(R.id.tvMinus);
        TextView tvPlus = itemView.findViewById(R.id.tvPlus);
        TextView tvTotalPrice = itemView.findViewById(R.id.tvTotalPrice);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }
}
