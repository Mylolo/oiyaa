package com.localvalu.eats.mycart.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.mycart.dto.SaleTransactionLoLoOrderResult;

import java.io.Serializable;

public class SaleTransactionLoLoOrderResultWrapper implements Serializable {

    @SerializedName("SaleTransactionLoLoOrderResult")
    @Expose
    public SaleTransactionLoLoOrderResult saleTransactionLoLoOrderResult;
}
