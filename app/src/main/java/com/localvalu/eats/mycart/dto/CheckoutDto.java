package com.localvalu.eats.mycart.dto;

import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;

import java.io.Serializable;
import java.util.ArrayList;

public class CheckoutDto implements Serializable
{
    public BusinessDetailInfo businessDetailInfo;
    public ArrayList<CartDetailDto> cartList;
    public String orderTotal;
    public String orderAmount;
    public String payableAmount;
    public String totalLoyaltyPointsRedeemed;
    public String deliveryRate;
    public String tax;
    public String delivery;
    public String specialInstructions;
    public String allergyInstructions;
    public String tableId;
    public String tableOrderDescription;
    public boolean isOrderFromTableOrStore;
    public boolean isOrderFromEats;
}
