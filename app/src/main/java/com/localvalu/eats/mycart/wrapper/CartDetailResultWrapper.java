package com.localvalu.eats.mycart.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.mycart.dto.CartDetailResult;

import java.io.Serializable;

public class CartDetailResultWrapper implements Serializable {

    @SerializedName("Cart")
    @Expose
    public CartDetailResult cartDetailResult;
}
