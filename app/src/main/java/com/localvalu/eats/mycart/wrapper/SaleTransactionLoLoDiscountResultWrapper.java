package com.localvalu.eats.mycart.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.mycart.dto.SaleTransactionLoLoDiscountResult;
import com.localvalu.eats.mycart.dto.SaleTransactionLoLoOrderResult;

import java.io.Serializable;

public class SaleTransactionLoLoDiscountResultWrapper implements Serializable {

    @SerializedName("SaleTransactionLoLoDiscountResult")
    @Expose
    public SaleTransactionLoLoDiscountResult saleTransactionLoLoDiscountResult;
}
