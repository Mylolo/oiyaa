package com.localvalu.eats.mycart.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;

public class SaleTransactionLoLoDiscountResult implements Serializable {

    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("NewTokenBalance")
    @Expose
    public String newTokenBalance;
}
