package com.localvalu.eats.repository;
import com.localvalu.eats.listing.model.filter.request.CuisinesFilter;
import com.localvalu.eats.listing.model.filter.CuisinesFilterResponse;
import com.requestHandler.ApiClient;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CuisinesFilterRepository
{
    CuisinesFilterApi cuisinesFilterApi;

    public CuisinesFilterRepository()
    {
        cuisinesFilterApi = ApiClient.getApiClientNew().create(CuisinesFilterApi.class);
    }

    public Single<CuisinesFilterResponse> cuisinesFilter(CuisinesFilter cuisinesFilter)
    {
        return cuisinesFilterApi.cuisinesFilter(cuisinesFilter)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

}