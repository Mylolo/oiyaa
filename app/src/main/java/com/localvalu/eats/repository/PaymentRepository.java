package com.localvalu.eats.repository;

import com.localvalu.eats.payment.dto.Pay360Request;
import com.localvalu.eats.payment.dto.Pay360Response;
import com.requestHandler.ApiClient;
import com.requestHandler.payment.PaymentApi;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PaymentRepository
{
    PaymentApi paymentApi;

    public PaymentRepository()
    {
        paymentApi = ApiClient.getApiClientNew().create(PaymentApi.class);
    }

    public Single<Pay360Response> startPayment(Pay360Request pay360Request)
    {
        return paymentApi.startPayment(pay360Request)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

}
