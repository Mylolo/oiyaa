package com.localvalu.eats.repository;

import com.localvalu.eats.listing.model.filter.request.CuisinesFilter;
import com.localvalu.eats.listing.model.filter.CuisinesFilterResponse;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface CuisinesFilterApi
{
    @POST("Websitesearch/cuisinesFilter")
    Single<CuisinesFilterResponse> cuisinesFilter(@Body CuisinesFilter cuisinesFilter);
}