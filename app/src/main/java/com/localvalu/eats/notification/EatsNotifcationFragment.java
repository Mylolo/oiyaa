package com.localvalu.eats.notification;

import android.app.ProgressDialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.MainActivity;
import com.localvalu.directory.notification.dto.NotificationDetailDto;
import com.localvalu.directory.notification.wrapper.NotificationListResultWrapper;
import com.localvalu.eats.notification.adapter.EatsNotificationAdapter;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EatsNotifcationFragment extends BaseFragment
{
    private static final String TAG = "EatsNotifcationFragment";

    private RecyclerView rv_lists1, rv_lists2;
    private GridLayoutManager gridLayoutManager1, gridLayoutManager2;
    private EatsNotificationAdapter eatsNotificationAdapter;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;

    // TODO: Rename and change types and number of parameters
    public static EatsNotifcationFragment newInstance()
    {
        return new EatsNotifcationFragment();
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_eats_notification, container, false);
        buildObjectForHandlingAPI();
        setView();
        return view;
    }

    private void setView()
    {
        //((MainActivity) Objects.requireNonNull(getActivity())).setOnlyTitleMenuBarHeader("eats", "");
        ((MainActivity) Objects.requireNonNull(getActivity())).setHeaderWithBackIcon(getString(R.string.lbl_notifications), AppUtils.BMT_EATS);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        rv_lists1 = view.findViewById(R.id.rv_lists1);
        rv_lists2 = view.findViewById(R.id.rv_lists2);
        gridLayoutManager1 = new GridLayoutManager(getActivity(), 1);
        rv_lists1.setLayoutManager(gridLayoutManager1);
        gridLayoutManager2 = new GridLayoutManager(getActivity(), 1);
        rv_lists2.setLayoutManager(gridLayoutManager2);
        fetchAPI();
    }

    private void fetchAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("AccountID", userBasicInfo.accountId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<NotificationListResultWrapper> dtoCall = apiInterface.getNotificationListsResult(body);
        dtoCall.enqueue(new Callback<NotificationListResultWrapper>()
        {
            @Override
            public void onResponse(Call<NotificationListResultWrapper> call, Response<NotificationListResultWrapper> response)
            {
                mProgressDialog.dismiss();
                try
                {
                    if (response.body().notificationListResult.errorDetails.errorCode == 0)
                    {
                        setAdapter1(response.body().notificationListResult.list.pushLists);
                        setAdapter2(response.body().notificationListResult.list.emailLists);
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().notificationListResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<NotificationListResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void setAdapter1(final ArrayList<NotificationDetailDto> notificationListDto)
    {
        eatsNotificationAdapter = new EatsNotificationAdapter(getActivity(), notificationListDto);
        eatsNotificationAdapter.setOnClickListener(new EatsNotificationAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClick(int position)
            {
                callAPI(notificationListDto.get(position).id, notificationListDto.get(position).type);
            }
        });
        rv_lists1.setAdapter(eatsNotificationAdapter);
    }

    private void setAdapter2(final ArrayList<NotificationDetailDto> notificationListDto)
    {
        eatsNotificationAdapter = new EatsNotificationAdapter(getActivity(), notificationListDto);
        eatsNotificationAdapter.setOnClickListener(new EatsNotificationAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClick(int position)
            {
                callAPI(notificationListDto.get(position).id, notificationListDto.get(position).type);
            }
        });
        rv_lists2.setAdapter(eatsNotificationAdapter);
    }

    private void callAPI(String id, String type)
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("AccountID", userBasicInfo.accountId);
            objMain.put("notify_id", id);
            objMain.put("notify_type", type);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<NotificationListResultWrapper> dtoCall = apiInterface.setNotificationListsResult(body);
        dtoCall.enqueue(new Callback<NotificationListResultWrapper>()
        {
            @Override
            public void onResponse(Call<NotificationListResultWrapper> call, Response<NotificationListResultWrapper> response)
            {
                mProgressDialog.dismiss();
                if (response.body().notificationListResult.errorDetails.errorCode == 0)
                {
                    setAdapter1(response.body().notificationListResult.list.pushLists);
                    setAdapter2(response.body().notificationListResult.list.emailLists);
                }
                else
                {
                    DialogUtility.showMessageWithOk(response.body().notificationListResult.errorDetails.errorMessage, getActivity());
                }
            }

            @Override
            public void onFailure(Call<NotificationListResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
        }
    }

}
