package com.localvalu.eats.webview;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.localvalu.R;

public class EatsWebviewActivity extends AppCompatActivity
{
    public static final String TAG="EatsWebviewActivity";

    private WebView webView;
    ImageView close;
    private TextView title;
    String getTitle = "", setURL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eats_webview);
        if (getIntent() != null)
        {
            getTitle = getIntent().getExtras().getString("getTitle");
            setURL = getIntent().getExtras().getString("setURL");
        }
        init();
    }

    private void init()
    {
        close = (ImageView) findViewById(R.id.close);
        webView = (WebView) findViewById(R.id.webview);
        title = (TextView) findViewById(R.id.title);
        WebSettings webSettings = webView.getSettings();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(new FAQWebViewClient());
        webView.setDownloadListener(_DownloadListener);
        webView.loadUrl(setURL);
        title.setText(getTitle);
        close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
    }

    public class FAQWebViewClient extends WebViewClient
    {
        public FAQWebViewClient()
        {
            // do nothing
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request)
        {
            final Uri uri = request.getUrl();
            StringBuilder strUrl = new StringBuilder();
            strUrl.append(uri.getScheme()).append("/").append(uri.getHost()).append("/").append(uri.getEncodedPath());
            view.loadUrl(strUrl.toString());
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
        }
    }



    DownloadListener _DownloadListener = new DownloadListener()
    {
        @Override
        public void onDownloadStart(String url, String userAgent,
                                    String contentDisposition, String mimetype,
                                    long contentLength)
        {
            /*String fileName="";
            DownloadManager.Request request = new DownloadManager.Request(
                    Uri.parse(url));
            if(!url.isEmpty())
            {
                fileName = url.substring(url.lastIndexOf('/') + 1);
            }
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); //Notify client once download is completed!
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "fileName");
            DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
            dm.enqueue(request);
            Toast.makeText(getApplicationContext(), "Downloading File", //To notify the Client that the file is being downloaded
                    Toast.LENGTH_LONG).show();*/
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
    };

    @Override
    public void onBackPressed()
    {
        if (webView.canGoBack())
        {
            webView.goBack();
        } else super.onBackPressed();
    }
}
