package com.localvalu.eats.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.eats.mycart.dto.CartDetailDto;

import java.io.Serializable;
import java.util.ArrayList;

public class PaymentOrderDto implements Serializable {

    @SerializedName("BusinessDetails")
    @Expose
    public ArrayList<BusinessDetailInfo> businessDetails;

    @SerializedName("PaymentDetails")
    @Expose
    public PaymentDTO paymentDto;
}
