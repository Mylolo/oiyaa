package com.localvalu.eats.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FoodItemSizeDto implements Serializable
{
    @SerializedName("Size_id")
    @Expose
    public String sizeId;

    @SerializedName("SizeName")
    @Expose
    public String sizeName;

    @SerializedName("FoodItemId")
    @Expose
    public String foodItemId;

    @SerializedName("mainPrice")
    @Expose
    public String mainPrice;

    public int groupPosition;
}
