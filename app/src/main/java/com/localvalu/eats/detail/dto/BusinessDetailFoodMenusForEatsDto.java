package com.localvalu.eats.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class BusinessDetailFoodMenusForEatsDto implements Serializable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("biz_user_id")
    @Expose
    public String userId;
    @SerializedName("menu_item_name")
    @Expose
    public String menuName;
    @SerializedName("purpose")
    @Expose
    public String purpose;
    @SerializedName("totalItems")
    @Expose
    public int totalItems;
    @SerializedName("items")
    @Expose
    public ArrayList<BusinessDetailFoodItemsForEatsDto> items;
}
