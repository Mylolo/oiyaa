package com.localvalu.eats.detail;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentContactMeBinding;
import com.localvalu.directory.detail.wrapper.SaveRequestSlotResultWrapper;
import com.localvalu.user.ValidationUtils;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.Calendar;
import java.util.Date;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactMeFragment extends BaseFragment
{
    String TAG = "ContactMeFragment";

    public View view;
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private FragmentContactMeBinding binding;
    private int colorEats;
    private HostActivityListener hostActivityListener;
    private String businessID;
    private Date contactMePreferredDate;
    private int originalMode;

    public static ContactMeFragment newInstance(String paramBusinessId)
    {
        Bundle bundle = new Bundle();
        bundle.putString(AppUtils.BUNDLE_BUSINESS_ID,paramBusinessId);
        ContactMeFragment contactMeFragment = new ContactMeFragment();
        contactMeFragment.setArguments(bundle);
        return contactMeFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        buildObjectForHandlingAPI();
        readFromBundle();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentContactMeBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        binding.layoutContactMe.btnContactMeSubmit.setOnClickListener(_OnClickListener);
        binding.layoutContactMe.etPreferDate.setOnClickListener(_OnClickListener);
        binding.layoutContactMe.etPreferTime.setOnClickListener(_OnClickListener);
    }

    private void setUpActionBar()
    {
        binding.toolbarLayout.tvTitle.setTextColor(colorEats);
        binding.toolbarLayout.toolbar.getNavigationIcon().setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });
    }

    private void readFromBundle()
    {
        try
        {
            businessID = getArguments().getString(AppUtils.BUNDLE_BUSINESS_ID);
        }
        catch (Exception e)
        {
            businessID = "";
        }
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.btnContactMeSubmit:
                     if(areAllInputsValid())
                     {
                         callAPIForGetQuote();
                     }
                     break;
                case R.id.etPreferDate:
                     if(contactMePreferredDate==null) contactMePreferredDate = new Date();
                     showContactMeDatePicker(binding.layoutContactMe.etPreferDate);
                     break;
                case R.id.etPreferTime:
                     if(contactMePreferredDate==null) contactMePreferredDate = new Date();
                     showContactMeTimePicker(binding.layoutContactMe.etPreferTime);
                     break;
            }
        }
    };

    /**
     * Call for get the quote from server
     * Contact Me Api.
     */
    private void callAPIForGetQuote()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId", businessID);
            objMain.put("requestFor", "quotebook");
            objMain.put("fullName", binding.layoutContactMe.etFullName.getText().toString());
            objMain.put("mobile", binding.layoutContactMe.etMobileNumber.getText().toString());
            objMain.put("email", binding.layoutContactMe.etEmailId.getText().toString());
            String strPreferDate = DateUtility.getServerFormatDateByDate(contactMePreferredDate);
            String strPreferTime = DateUtility.get24HoursTimeFormat(contactMePreferredDate);
            objMain.put("PreferredDate",strPreferDate);
            objMain.put("PreferredTime", strPreferTime);
            objMain.put("comment", binding.layoutContactMe.etComment.getText().toString());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<SaveRequestSlotResultWrapper> dtoCall = apiInterface.requestCallback(body);
        dtoCall.enqueue(new Callback<SaveRequestSlotResultWrapper>()
        {
            @Override
            public void onResponse(Call<SaveRequestSlotResultWrapper> call, Response<SaveRequestSlotResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().requestResponseResult.errorDetails.errorCode == 0)
                    {
                        binding.layoutContactMe.etFullName.setText("");
                        binding.layoutContactMe.etMobileNumber.setText("");
                        binding.layoutContactMe.etEmailId.setText("");
                        binding.layoutContactMe.etPreferDate.setText("");
                        binding.layoutContactMe.etPreferTime.setText("");
                        binding.layoutContactMe.etComment.setText("");

                        Toast.makeText(getActivity(), "Your callback DashboardRequest saved successfully.", Toast.LENGTH_LONG).show();
                        CommonUtilities.alertSound(getActivity(), R.raw.coin_drops);
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().requestResponseResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<SaveRequestSlotResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                {
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                }
                else
                {
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                }
            }
        });
    }

    public boolean areAllInputsValid()
    {
        if(isValidUserName() && isValidPhoneNumber() && isValidEmail() && isValidPreferDate() && isValidPreferTime() && isValidComment())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isValidUserName()
    {
        if(binding.layoutContactMe.etFullName.getText().toString().isEmpty())
        {
            binding.layoutContactMe.etFullName.setError(getString(R.string.lbl_name_empty));
            binding.layoutContactMe.etFullName.requestFocus();
            return false;
        }
        else
        {
            binding.layoutContactMe.etFullName.setError(null);
            return true;
        }
    }

    public boolean isValidPhoneNumber()
    {
        if(binding.layoutContactMe.etMobileNumber.getText().toString().isEmpty())
        {
            binding.layoutContactMe.etMobileNumber.setError(getString(R.string.lbl_mobile_number_empty));
            binding.layoutContactMe.etMobileNumber.requestFocus();
            return false;
        }
        else if(!ValidationUtils.isValidMobileNo(binding.layoutContactMe.etMobileNumber.getText().toString().trim()))
        {
            binding.layoutContactMe.etMobileNumber.setError(getString(R.string.lbl_invalid_mobile_number));
            binding.layoutContactMe.etMobileNumber.requestFocus();
            return false;
        }
        else
        {
            binding.layoutContactMe.etMobileNumber.setError(null);
            return true;
        }
    }

    public boolean isValidEmail()
    {
        if(binding.layoutContactMe.etEmailId.getText().toString().isEmpty())
        {
            binding.layoutContactMe.etMobileNumber.setError(getString(R.string.lbl_mobile_number_empty));
            binding.layoutContactMe.etMobileNumber.requestFocus();
            return false;
        }
        else if(!Patterns.EMAIL_ADDRESS.matcher(binding.layoutContactMe.etEmailId.getText().toString().trim()).matches())
        {
            binding.layoutContactMe.etMobileNumber.setError(getString(R.string.msg_enter_valid_email_id));
            binding.layoutContactMe.etMobileNumber.requestFocus();
            return false;
        }
        else
        {
            binding.layoutContactMe.etMobileNumber.setError(null);
            return true;
        }
    }

    public boolean isValidPreferDate()
    {
        if(binding.layoutContactMe.etPreferDate.getText().toString().isEmpty())
        {
            binding.layoutContactMe.etPreferDate.setError(getString(R.string.lbl_prefer_date_empty));
            binding.layoutContactMe.etPreferDate.requestFocus();
            return false;
        }
        else
        {
            binding.layoutContactMe.etPreferDate.setError(null);
            return true;
        }
    }

    public boolean isValidPreferTime()
    {
        if(binding.layoutContactMe.etPreferTime.getText().toString().isEmpty())
        {
            binding.layoutContactMe.etPreferTime.setError(getString(R.string.lbl_prefer_time_empty));
            binding.layoutContactMe.etPreferTime.requestFocus();
            return false;
        }
        else
        {
            binding.layoutContactMe.etPreferTime.setError(null);
            return true;
        }
    }

    public boolean isValidComment()
    {
        if(binding.layoutContactMe.etComment.getText().toString().isEmpty())
        {
            binding.layoutContactMe.etComment.setError(getString(R.string.lbl_comment_empty));
            binding.layoutContactMe.etComment.requestFocus();
            return false;
        }
        else
        {
            binding.layoutContactMe.etComment.setError(null);
            return true;
        }
    }

    private void showContactMeDatePicker(TextView textView)
    {
        final Calendar cldr = Calendar.getInstance();
        cldr.setTime(contactMePreferredDate);
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        // date picker dialog
        DatePickerDialog picker = new DatePickerDialog(requireContext(),
                R.style.AppTheme, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                String month;
                String day;

                if ((monthOfYear + 1) < 10)
                {
                    month = "0" + String.valueOf(monthOfYear + 1);
                }
                else
                {
                    month = String.valueOf(monthOfYear + 1);
                }

                if (dayOfMonth < 10)
                {
                    day = "0" + String.valueOf(dayOfMonth);
                }
                else
                {
                    day = String.valueOf(dayOfMonth);
                }
                System.out.println(year + "-" + month + "-" + dayOfMonth);
                textView.setText(dayOfMonth + "-" + month + "-" + year);
                Calendar calendar = Calendar.getInstance();
                calendar.set(year,monthOfYear,dayOfMonth);
                contactMePreferredDate = calendar.getTime();
            }
        }, year, month, day);
        picker.getDatePicker().setMinDate(new Date().getTime());
        picker.show();
    }

    private void showContactMeTimePicker(TextView textView)
    {
        final Calendar cldr = Calendar.getInstance();
        cldr.setTime(contactMePreferredDate);
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        int hour = cldr.get(Calendar.HOUR);
        int minute = cldr.get(Calendar.MINUTE);
        int second = cldr.get(Calendar.SECOND);

        TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),
                R.style.AppTheme,_OnTimeSetListener,hour,minute,false);
        timePickerDialog.show();
    }

    TimePickerDialog.OnTimeSetListener _OnTimeSetListener = new TimePickerDialog.OnTimeSetListener()
    {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute)
        {
            final Calendar cldr = Calendar.getInstance();
            cldr.setTime(contactMePreferredDate);
            cldr.set(Calendar.HOUR_OF_DAY,hourOfDay);
            cldr.set(Calendar.MINUTE,minute);
            contactMePreferredDate = cldr.getTime();
            binding.layoutContactMe.etPreferTime.setText(DateUtility.get12HoursTimeFormat(contactMePreferredDate));
        }
    };

}
