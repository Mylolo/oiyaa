package com.localvalu.eats.detail.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.R;
import com.localvalu.eats.detail.dto.FoodItemSizeDto;
import com.localvalu.eats.detail.dto.FoodMenuDto;
import com.localvalu.eats.detail.listener.FoodItemClickListener;

import java.util.ArrayList;
import java.util.List;

public class FoodItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private static final String TAG = "FoodItemAdapter";
    private List<FoodMenuDto> items = new ArrayList<FoodMenuDto>();
    private Context context;
    private FoodItemClickListener foodItemClickListener;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private int lastCheckedPosition = -1;


    public FoodItemAdapter(Context context,List<FoodMenuDto> items)
    {
        this.context = context;
        this.items = items;
        strCurrencySymbol = context.getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = context.getString(R.string.lbl_currency_letter_euro);
    }

    public void setFoodItemClickListener(FoodItemClickListener foodItemClickListener)
    {
        this.foodItemClickListener = foodItemClickListener;
    }

    @Override
    public int getItemViewType(int position)
    {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        if (items.get(position).isHeader) return 1;
        else return 2;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        switch (viewType)
        {
            case 1:
                 View menuHeaderView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_group_food_menu, parent, false);
                 return new MenuHeaderViewHolder(menuHeaderView);
            case 2:
                 View menuItemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_food_menu, parent, false);
                 return new MenuItemViewHolder(menuItemView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position)
    {
        FoodMenuDto foodMenuDto = items.get(position);
        if(holder instanceof MenuHeaderViewHolder)
        {
            MenuHeaderViewHolder menuHeaderViewHolder = (MenuHeaderViewHolder) holder;
            menuHeaderViewHolder.lblListHeader.setText(foodMenuDto.menuName);
        }
        else if(holder instanceof MenuItemViewHolder)
        {
            MenuItemViewHolder menuItemViewHolder = (MenuItemViewHolder) holder;
            //if(foodMenuDto.sheetOpen)menuItemViewHolder.view_select.setVisibility(View.VISIBLE);
            //else menuItemViewHolder.view_select.setVisibility(View.GONE);
            menuItemViewHolder.tv_food_details.setText(foodMenuDto.foodDesc);
            StringBuilder foodNameWithSize = new StringBuilder();
            foodNameWithSize.append(foodMenuDto.itemName);
            if(foodMenuDto.foodItemSize!=null)
            {
                if(foodMenuDto.foodItemSize.size()>0)
                {
                    if(foodMenuDto.foodItemSize.size()>1)
                    {
                        for(FoodItemSizeDto item:foodMenuDto.foodItemSize)
                        {
                            item.groupPosition= position;
                        }
                        FoodSizeAdapter foodSizeAdapter = new FoodSizeAdapter(context,foodMenuDto.foodItemSize);
                        menuItemViewHolder.rvSize.setLayoutManager(new LinearLayoutManager(context));
                        foodSizeAdapter.setFoodItemClickListener(new FoodItemClickListener()
                        {
                            @Override
                            public void onFoodItemClicked(FoodItemSizeDto foodItemSizeDto)
                            {
                                if(foodItemClickListener!=null)
                                    foodItemClickListener.onFoodItemClicked(foodItemSizeDto);
                            }
                        });
                        menuItemViewHolder.rvSize.setAdapter(foodSizeAdapter);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                        {
                            Drawable drawable = new ColorDrawable();
                            menuItemViewHolder.itemView.setForeground(drawable);
                        }
                    }
                    else
                    {
                        menuItemViewHolder.tv_food_price.setText(strCurrencySymbol + foodMenuDto.foodItemSize.get(0).mainPrice);
                        menuItemViewHolder.itemView.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                foodMenuDto.foodItemSize.get(0).groupPosition=position;
                                if(foodItemClickListener!=null)
                                    foodItemClickListener.onFoodItemClicked(foodMenuDto.foodItemSize.get(0));
                            }
                        });
                    }
                    String sizeName = foodMenuDto.foodItemSize.get(0).sizeName;
                    if(!sizeName.trim().equalsIgnoreCase(""))
                    {
                        foodNameWithSize.append(" (").append(sizeName).append(")");
                    }
                }
            }
            menuItemViewHolder.tv_food_name.setText(foodNameWithSize.toString());
        }
    }

    @Override
    public int getItemCount()
    {
        return items.size();
    }

    public class MenuHeaderViewHolder extends RecyclerView.ViewHolder
    {
        TextView lblListHeader;

        public MenuHeaderViewHolder(@NonNull View itemView)
        {
            super(itemView);
            lblListHeader = itemView.findViewById(R.id.lblListHeader);
        }
    }

    public class MenuItemViewHolder extends RecyclerView.ViewHolder
    {
        View view_select;
        RecyclerView rvSize;
        TextView tv_food_name,tv_food_price,tv_food_details;

        public MenuItemViewHolder(@NonNull View itemView)
        {
            super(itemView);
            view_select = itemView.findViewById(R.id.view_select);
            tv_food_name = itemView.findViewById(R.id.tv_food_name);
            tv_food_price = itemView.findViewById(R.id.tv_food_price);
            tv_food_details = itemView.findViewById(R.id.tv_food_details);
            rvSize = itemView.findViewById(R.id.rvSize);
        }
    }

    public List<FoodMenuDto> getItems()
    {
        return items;
    }

}
