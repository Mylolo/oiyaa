package com.localvalu.eats.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.detail.dto.BusinessDetailServiceDto;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.eats.mycart.dto.CartDetailResult;
import com.localvalu.eats.mycart.wrapper.CartChangeQuantityResultWrapper;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderNowDto implements Serializable {

    @SerializedName("BusinessDetails")
    @Expose
    public ArrayList<BusinessDetailInfo> businessDetails;

    @SerializedName("FoodItemsDetails")
    @Expose
    public ArrayList<BusinessDetailFoodItemsForEatsDto> foodMenus;

    @SerializedName("CartDetails")
    @Expose
    public ArrayList<CartDetailDto> cartDetails;

    @SerializedName("TaxPercentage")
    @Expose
    public String taxPercentage;

    @SerializedName("DeliveryCharge")
    @Expose
    public DeliveryChargeDto deliveryCharge;

    @SerializedName("Deliveryfee")
    @Expose
    public DeliveryChargeDto deliveryFee;
}
