package com.localvalu.eats.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;
import java.util.List;

public class FoodMenuDto implements Serializable {

    @SerializedName("FoodItemId")
    @Expose
    public String foodItemId;

    @SerializedName("itemName")
    @Expose
    public String itemName;

    @SerializedName("foodDesc")
    @Expose
    public String foodDesc;

    @SerializedName("foodMenuId")
    @Expose
    public String foodMenuId;

    @SerializedName("foodPic")
    @Expose
    public String foodPic;

    @SerializedName("foodType")
    @Expose
    public String foodType;

    @SerializedName("FoodItemSize")
    @Expose
    public List<FoodItemSizeDto> foodItemSize;

    @SerializedName("mainPrice")
    @Expose
    public String mainPrice;

    public boolean isHeader;

    public String menuName;

    public boolean sheetOpen;
}
