package com.localvalu.eats.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BusinessDetailFoodItemsForEatsDto implements Serializable {

    @SerializedName("foodMenuId")
    @Expose
    public String foodMenuId;

    @SerializedName("menuName")
    @Expose
    public String menuName;

    @SerializedName("purpose")
    @Expose
    public String purpose;

    @SerializedName("BusinessId")
    @Expose
    public String businessId;

    @SerializedName("FoodItems")
    @Expose
    public List<FoodMenuDto> items;

    public boolean isHeader;
}
