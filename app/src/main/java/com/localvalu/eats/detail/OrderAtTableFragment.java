package com.localvalu.eats.detail;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentOrderAtTableBinding;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.eats.detail.adapter.EatsTimeSliderViewPagerAdapter;
import com.localvalu.eats.detail.adapter.FoodItemAdapter;
import com.localvalu.eats.detail.dto.BusinessDetailFoodItemsForEatsDto;
import com.localvalu.eats.detail.dto.FoodItemSizeDto;
import com.localvalu.eats.detail.dto.FoodMenuDto;
import com.localvalu.eats.detail.listener.AddToCartClickListener;
import com.localvalu.eats.detail.listener.FoodItemClickListener;
import com.localvalu.eats.detail.wrapper.OrderNowResultResponse;
import com.localvalu.eats.mycart.CartFragment;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderAtTableFragment extends BaseFragment implements AddToCartClickListener
{
    private static final String TAG = "OrderAtTableFragment";


    private int colorEats;
    private BottomSheetBehavior sheetBehavior;
    private FragmentOrderAtTableBinding binding;

    private FoodItemAdapter foodItemAdapter;
    private int quantity, totQty = 0, quantityToServer;
    private Float totPrice = 0.0f;
    private String timeSlotRequest = "";
    private String dateSlotRequest = "";
    private String currentDate;
    private EatsTimeSliderViewPagerAdapter eatsTimeSliderViewPagerAdapter;
    private String timeArrayList[] = new String[]{
            "12:00 PM", "12:30 PM", "01:00 PM", "01:30 PM", "02:00 PM", "02:30 PM",
            "03:00 PM", "03:30 PM", "04:00 PM", "04:30 PM", "05:00 PM", "05:30 PM",
            "06:00 PM", "06:30 PM", "07:00 PM", "07:30 PM", "08:00 PM", "08:30 PM",
            "09:00 PM", "09:30 PM", "10:00 PM", "10:30 PM", "11:00 PM", "11:30 PM",
            "12:00 AM", "12:30 AM", "01:00 AM", "01:30 AM", "02:00 AM", "02:30 AM",
            "03:00 AM", "03:30 AM", "04:00 AM", "04:30 AM", "05:00 AM", "05:30 AM",
            "06:00 AM", "06:30 AM", "07:00 AM", "07:30 AM", "08:00 AM", "08:30 AM",
            "09:00 AM", "09:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"};
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private String strAddToBasket;
    private boolean locationEnabled = false;
    private LatLng myLocation, retailerLocation;
    private Call<OrderNowResultResponse> dtoCall;
    private ArrayList<CartDetailDto> tempCartList;
    private String price;
    private static final int MINUS_CLICKED = 1;
    private static final int PLUS_CLICKED = 2;
    private int qtyButtonClicked;
    private boolean itemExist;
    private boolean firstTimeCartLoad = true;
    private HostActivityListener hostActivityListener;
    private Typeface typeface;
    private String businessId;
    private static int BOTTOM_SHEET_DIALOG_REQUEST=11;
    private BusinessDetailInfo businessDetailInfo;

    public static OrderAtTableFragment newInstance(BusinessDetailInfo businessDetailInfo)
    {
        OrderAtTableFragment orderAtTableFragment = new OrderAtTableFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppUtils.BUNDLE_BUSINESS_DETAIL,businessDetailInfo);
        orderAtTableFragment.setArguments(bundle);
        return orderAtTableFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");

        buildObjectForHandlingAPI();

        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        typeface = ResourcesCompat.getFont(requireContext(), R.font.comfortaa_regular);

        readFromBundle();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date cal = (Date) Calendar.getInstance().getTime();
        currentDate = formatter.format(cal);
        dateSlotRequest = currentDate;
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        strAddToBasket = getString(R.string.lbl_add_to_basket);
    }

    private void readFromBundle()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        businessDetailInfo = (BusinessDetailInfo) getArguments().getSerializable(AppUtils.BUNDLE_BUSINESS_DETAIL);
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentOrderAtTableBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: ");
        setUpActionBar();
        setProperties();
        fetchOrderNowAPI(businessDetailInfo.business_id);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        binding.getRoot().requestLayout();
    }

    private void setUpActionBar()
    {
        int themeColor = ContextCompat.getColor(requireContext(), R.color.colorEats);
        binding.toolbarLayout.btnActionContactMe.setVisibility(View.GONE);
        binding.toolbarLayout.tvTitle.setText(getString(R.string.lbl_order_at_table));
        binding.toolbarLayout.toolbarEats.getNavigationIcon().setColorFilter(themeColor, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbarEats.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });
    }

    private void initView()
    {
        initBottomSheetView();
        binding.rvFoodItems.setLayoutManager(new LinearLayoutManager(requireContext()));
        callMyCartApi();
    }

    private void initBottomSheetView()
    {
        binding.layoutBottomView.clBottomView.setVisibility(View.GONE);
        binding.layoutBottomSheet.clBottomSheet.setVisibility(View.GONE);
        /*nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener()
        {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY)
            {
                if (scrollY < oldScrollY)
                {
                    if(!clBottomView.isShown()) showBottom();
                }
                else
                {
                    if(clBottomView.isShown()) hideBottom();
                }
            }
        });*/
        setPopUpMenuBottomSheet();
    }

    private void setProperties()
    {
        initView();
    }

    private void showBottom()
    {
        binding.layoutBottomView.clBottomView.setVisibility(View.VISIBLE);

    }

    private void hideBottom()
    {
        binding.layoutBottomView.clBottomView.setVisibility(View.GONE);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    private void fetchOrderNowAPI(String businessId)
    {
        showProgress();

        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("BusinessId", businessId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("requestFor", "GetOrderdetails");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        dtoCall = apiInterface.orderNow(body);
        dtoCall.enqueue(new Callback<OrderNowResultResponse>()
        {
            @Override
            public void onResponse(Call<OrderNowResultResponse> call, Response<OrderNowResultResponse> response)
            {
                try
                {
                    if (response.body().businessDetailsResult.errorDetails.errorCode == 0)
                    {
                        showContents();
                        prepareListData(response.body().businessDetailsResult.orderNowDto.foodMenus);
                    }
                    else
                    {
                        showError(response.body().businessDetailsResult.errorDetails.errorMessage, false);
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<OrderNowResultResponse> call, Throwable t)
            {
                Log.d(TAG, t.getMessage());
                if (isAdded())
                {
                    if (t instanceof IOException)
                        showError(getString(R.string.network_unavailable), true);
                    else showError(getString(R.string.server_alert), false);
                }
            }
        });
    }

    //For Listing Order Now List
    public void setPopUpMenuBottomSheet()
    {
        /**BottomSheet Setup*/
        sheetBehavior = BottomSheetBehavior.from(binding.layoutBottomSheet.clBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
        {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState)
            {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        if(!binding.layoutBottomSheet.clBottomSheet.isShown()) showBottom();
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        if(binding.layoutBottomSheet.clBottomSheet.isShown()) hideBottom();
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset)
            {

            }
        });
    }

    private void prepareListData(ArrayList<BusinessDetailFoodItemsForEatsDto> foodMenus)
    {
        setOrderList(foodMenus);
    }

    private void setOrderList(ArrayList<BusinessDetailFoodItemsForEatsDto> foodMenus)
    {
        ArrayList<FoodMenuDto> tempFoodItems = new ArrayList<FoodMenuDto>();

        if (foodMenus != null)
        {
            if (foodMenus.size() > 0)
            {
                for (BusinessDetailFoodItemsForEatsDto item : foodMenus)
                {
                    FoodMenuDto foodMenuDto = new FoodMenuDto();
                    foodMenuDto.menuName = item.menuName;
                    foodMenuDto.isHeader = true;
                    tempFoodItems.add(foodMenuDto);
                    tempFoodItems.addAll(item.items);
                }

                foodItemAdapter = new FoodItemAdapter(requireContext(), tempFoodItems);
                foodItemAdapter.setFoodItemClickListener(new FoodItemClickListener()
                {
                    @Override
                    public void onFoodItemClicked(FoodItemSizeDto foodItemSizeDto)
                    {
                        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED)
                        {
                            if (foodItemAdapter != null)
                            {
                                List<FoodMenuDto> items = foodItemAdapter.getItems();
                                FoodMenuDto foodMenuDto = items.get(foodItemSizeDto.groupPosition);
                                //foodMenuDto.sheetOpen=true;
                                //foodItemAdapter.notifyItemChanged(foodItemSizeDto.groupPosition,foodMenuDto);
                                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                                setUpOrderTableDataBottomSheet(foodMenuDto, foodItemSizeDto);
                            }
                        }
                        else
                        {
                            /*List<FoodMenuDto> items = foodItemAdapter.getItems();
                            int i=0;
                            for(FoodMenuDto item:items)
                            {
                                FoodMenuDto foodMenuDto = item;
                                foodMenuDto.sheetOpen=false;
                                foodItemAdapter.notifyItemChanged(i,foodMenuDto);
                            }*/
                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        }
                    }
                });
                binding.rvFoodItems.setAdapter(foodItemAdapter);
                binding.layoutBottomView.clBottomView.setVisibility(View.VISIBLE);
            }
            else
            {
                showError(getString(R.string.lbl_no_food_items),false);
            }
        }
    }

    private void setUpOrderTableDataBottomSheet(FoodMenuDto foodMenuDto, FoodItemSizeDto foodItemSizeDto)
    {
        OrderBottomSheetFragment orderBottomSheetFragment = OrderBottomSheetFragment.newInstance(foodMenuDto,foodItemSizeDto,tempCartList);
        orderBottomSheetFragment.setTargetFragment(this,BOTTOM_SHEET_DIALOG_REQUEST);
        orderBottomSheetFragment.show(getChildFragmentManager(),orderBottomSheetFragment.getClass().getName());
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {

            case R.id.btnCheckOut:
                 totQty = 0;
                 CartFragment cartFragment = CartFragment.newInstance(businessDetailInfo);
                 hostActivityListener.updateFragment(cartFragment,true,cartFragment.getClass().getName(),true);
                 break;
            case R.id.btnAddToCart:
                 sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                 break;
            case R.id.btnRetry:
                 fetchOrderNowAPI(businessDetailInfo.business_id);
                 break;

        }
    }

    private void callMyCartApi()
    {
        if (!firstTimeCartLoad) mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId", businessDetailInfo.business_id);
            objMain.put("requestFor", "MyCartDetails");
            objMain.put("Distance", "12");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<OrderNowResultResponse> dtoCall = apiInterface.orderNowTable(body);
        dtoCall.enqueue(new Callback<OrderNowResultResponse>()
        {
            @Override
            public void onResponse(Call<OrderNowResultResponse> call, Response<OrderNowResultResponse> response)
            {
                if (!firstTimeCartLoad) mProgressDialog.dismiss();
                firstTimeCartLoad = false;
                if (response.body().businessDetailsResult.errorDetails.errorCode == 0)
                {
                    ArrayList<CartDetailDto> cartList = response.body().businessDetailsResult.orderNowDto.cartDetails;
                    setCartDetails(cartList);
                }
                else
                {
                    binding.layoutBottomView.clBottomView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<OrderNowResultResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void callAPIForAddToCart(String strFoodItemId, String quantity, String sizeId)
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId", businessDetailInfo.business_id);
            objMain.put("requestFor", "AddItemsToCart");
            objMain.put("global_unique_id", "1");
            objMain.put("itemId", strFoodItemId);
            objMain.put("sizeId", sizeId);
            objMain.put("quantity", quantity);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<OrderNowResultResponse> dtoCall = apiInterface.orderNow(body);
        dtoCall.enqueue(new Callback<OrderNowResultResponse>()
        {
            @Override
            public void onResponse(Call<OrderNowResultResponse> call, Response<OrderNowResultResponse> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().addToCartResult.errorDetails.errorCode == 0)
                    {
                        Toast.makeText(getActivity(), getString(R.string.msg_add_to_cart_success), Toast.LENGTH_LONG).show();
                        callMyCartApi();
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().addToCartResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<OrderNowResultResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                {
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                }
                else
                {
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                }
            }
        });
    }

    public void setCartDetails(ArrayList<CartDetailDto> cartList)
    {
        try
        {
            tempCartList = cartList;

            if (cartList != null)
            {
                if (cartList.size() > 0)
                {
                    StringBuilder strItems = new StringBuilder();
                    strItems.append(Integer.toString(cartList.size())).append(" Items");
                    binding.layoutBottomView.tvItemsInCart.setText(strItems.toString());

                    String strPrice = "";
                    double price = 0;

                    for (CartDetailDto cartDetailDto : cartList)
                    {
                        price = price + Double.parseDouble(cartDetailDto.rate);
                    }
                    binding.layoutBottomView.tvPriceInCart.setText(strCurrencySymbol + String.format("%,.2f", price));

                }
                else
                {
                    binding.layoutBottomView.clBottomView.setVisibility(View.GONE);
                }

            }
            else
            {
                binding.layoutBottomView.clBottomView.setVisibility(View.GONE);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        locationEnabled = false;
    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.rvFoodItems.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showContents()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvFoodItems.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage, boolean retry)
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvFoodItems.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if (retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if (dtoCall != null)
        {
            dtoCall.cancel();
        }
    }

    @Override
    public void onAddToCartClicked(String strFoodItemId, String strQty, String strFoodItemSizeId)
    {
        callAPIForAddToCart(strFoodItemId,strQty,strFoodItemSizeId);
    }
}
