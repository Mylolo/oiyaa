package com.localvalu.eats.detail.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.R;
import com.localvalu.eats.detail.dto.BusinessDetailFoodItemsForEatsDto;
import com.localvalu.eats.detail.dto.FoodMenuDto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FoodMenuAdapter extends BaseExpandableListAdapter
{

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<FoodMenuDto>> _listDataChild;
    private OnItemClickListener onItemClickListener;
    private String strCurrencySymbol;
    private String strCurrencyLetter;

    public FoodMenuAdapter(Context context, List<String> listDataHeader,
                           HashMap<String, List<FoodMenuDto>> listChildData)
    {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        strCurrencySymbol = context.getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = context.getString(R.string.lbl_currency_letter_euro);
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon)
    {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition)
    {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent)
    {

        final FoodMenuDto foodList = (FoodMenuDto) getChild(groupPosition, childPosition);

        if (convertView == null)
        {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_food_menu, null);
        }

        View view_select = convertView.findViewById(R.id.view_select);
        TextView tv_food_name = convertView.findViewById(R.id.tv_food_name);
        TextView tv_food_price = convertView.findViewById(R.id.tv_food_price);
        TextView tv_food_details = convertView.findViewById(R.id.tv_food_details);
        RecyclerView rvSize = convertView.findViewById(R.id.rvSize);
        rvSize.setLayoutManager(new LinearLayoutManager(_context));

        tv_food_name.setText(foodList.itemName);
        tv_food_name.setCompoundDrawablePadding(0);

        if (foodList.sheetOpen == true)
        {
            view_select.setVisibility(View.VISIBLE);
            view_select.setBackgroundResource(R.color.colorEats);
        }
        else
        {
            view_select.setVisibility(View.VISIBLE);
            view_select.setBackgroundResource(R.color.colorGray);
        }
        try
        {
            if (foodList.foodType.equals("veg"))
            {
                tv_food_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.order_detail_veg, 0, 0, 0);

            }
            else
            {
                tv_food_name.setCompoundDrawablesWithIntrinsicBounds(R.drawable.order_detail_nonveg, 0, 0, 0);
            }
        }
        catch (Exception e)
        {

        }
         /*API Name changed from order/OrderNow to order/OrderNow
        on May 21 2020
        tv_food_price.setText(strCurrencySymbol + foodList.mainPrice); */
        if(foodList.foodItemSize!=null)
        {
            if(foodList.foodItemSize.size()>0)
            {
                if(foodList.foodItemSize.size()>1)
                {
                    /*FoodSizeAdapter foodSizeAdapter = new FoodSizeAdapter(_context,foodList.foodItemSize,groupPosition,childPosition);
                    foodSizeAdapter.setOnClickListener(new OnItemClickListener()
                    {
                        @Override
                        public void onItemClick(int grup_position, int child_position, int sizePosition)
                        {
                            if(onItemClickListener!=null)
                            {
                                onItemClickListener.onItemClick(grup_position,child_position,sizePosition);
                            }
                        }

                    });
                    rvSize.setAdapter(foodSizeAdapter);*/
                }
                else
                {
                    if(foodList.foodItemSize.get(0).mainPrice!=null)
                    {
                        tv_food_price.setText(strCurrencySymbol + foodList.foodItemSize.get(0).mainPrice);
                    }
                    ConstraintLayout cnlFoodItem = convertView.findViewById(R.id.cnlFoodItem);
                    cnlFoodItem.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            onItemClickListener.onItemClick(groupPosition, childPosition,0);
                        }
                    });
                }
            }
        }
        tv_food_details.setText(foodList.foodDesc);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition)
    {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition)
    {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount()
    {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition)
    {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent)
    {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null)
        {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_group_food_menu, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setText(headerTitle + " (" + this._listDataChild.get(headerTitle).size() + ")");

        return convertView;
    }

    @Override
    public boolean hasStableIds()
    {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition)
    {
        return true;
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onItemClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemClick(int grup_position, int child_position, int sizePosition);

    }
}