package com.localvalu.eats.detail.adapter;

import android.app.Activity;
import android.graphics.Color;

import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.localvalu.R;

public class EatsTimeSliderViewPagerAdapter extends PagerAdapter
{
    public int selectedPos = -1;
    private Activity activity;
    private String timeArrayList[];
    private OnPagerItemClickListener onPagerItemClickListener;

    public EatsTimeSliderViewPagerAdapter(Activity activity, String[] timeArrayList, boolean flag)
    {
        this.activity = activity;
        this.timeArrayList = timeArrayList;
    }

    @Override
    public int getCount()
    {
        return timeArrayList.length;
    }

    public int getItemPosition(Object object)
    {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position)
    {
        String time = timeArrayList[position];
        System.out.println("time " + time);
        View imageLayout = activity.getLayoutInflater().inflate(R.layout.eats_time_viewpager_slider, container, false);
        TextView tv_time = (TextView) imageLayout.findViewById(R.id.tv_time);

        tv_time.setText(time);

        System.out.println("selectedPos " + selectedPos);

        if (selectedPos == position)
        {
            tv_time.setBackgroundColor(ContextCompat.getColor(activity.getApplicationContext(),R.color.colorEats));
            tv_time.setTextColor(ContextCompat.getColor(activity.getApplicationContext(),R.color.colorWhite));
        }
        else
        {
            tv_time.setBackgroundColor(ContextCompat.getColor(activity.getApplicationContext(),R.color.colorLightGray));
            tv_time.setTextColor(ContextCompat.getColor(activity.getApplicationContext(),R.color.colorBlack));
        }

        tv_time.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                selectedPos = position;
                System.out.println("selectedPos ...." + selectedPos);
                onPagerItemClickListener.onPagerItemClick(position);

            }
        });

        container.addView(imageLayout, 0);
        return imageLayout;
    }

    public void setOnPagerItemClickListener(OnPagerItemClickListener onPagerItemClickListener)
    {
        this.onPagerItemClickListener = onPagerItemClickListener;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object)
    {
        return view == object;
    }

    @Override
    public float getPageWidth(int position)
    {
        return Float.parseFloat("0.35");
    }

    public interface OnPagerItemClickListener
    {
        void onPagerItemClick(int position);
    }
}
