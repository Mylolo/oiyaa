package com.localvalu.eats.detail.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.eats.detail.dto.BusinessDetailServiceForEatsDto;

import java.util.ArrayList;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class EatsServiceListAdapter extends RecyclerView.Adapter<EatsServiceListAdapter.ViewHolder> {
    public static final String TAG = EatsServiceListAdapter.class.getSimpleName();
    private Activity activity;
    private OnItemClickListener onClickListener;
    private ArrayList<BusinessDetailServiceForEatsDto> serviceList;

    public EatsServiceListAdapter(Activity activity, ArrayList<BusinessDetailServiceForEatsDto> serviceList) {
        this.activity = activity;
        this.serviceList = serviceList;
    }

    @NonNull
    @Override
    public EatsServiceListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_eats_service_list, viewGroup, false);
        return new EatsServiceListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EatsServiceListAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.tv_service_content.setText("■ " + serviceList.get(i).serviceName);
        viewHolder.tv_service_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onItemClick(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

//    public class ViewHolder extends RecyclerView.ViewHolder {
//        public ViewHolder(@NonNull View itemView) {
//            super(itemView);
//        }
//    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_service_content = itemView.findViewById(R.id.tv_service_content);

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
