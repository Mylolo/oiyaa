package com.localvalu.eats.detail.listener;

import com.localvalu.eats.detail.dto.FoodItemSizeDto;

public interface AddToCartClickListener
{
    void onAddToCartClicked(String strFoodItemId,String strQty,String strFoodItemSizeId);
}
