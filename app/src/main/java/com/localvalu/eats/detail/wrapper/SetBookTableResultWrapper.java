package com.localvalu.eats.detail.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.detail.dto.RequestBookTableResult;

import java.io.Serializable;

public class SetBookTableResultWrapper implements Serializable {

    @SerializedName("RequestTableBooking")
    @Expose
    public RequestBookTableResult requestBookTableResult;
}
