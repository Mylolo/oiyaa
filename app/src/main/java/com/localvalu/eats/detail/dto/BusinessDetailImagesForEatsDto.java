package com.localvalu.eats.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessDetailImagesForEatsDto implements Serializable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("business_id")
    @Expose
    public String businessId;
    @SerializedName("main_image")
    @Expose
    public String mainImage;
    @SerializedName("thumb_image")
    @Expose
    public String thumbImage;
}
