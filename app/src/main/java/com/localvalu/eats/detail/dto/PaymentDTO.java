package com.localvalu.eats.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentDTO implements Serializable {

    @SerializedName("CurrentPromotionValue")
    @Expose
    public String currentPromotionValue;

    @SerializedName("DiscountAmount")
    @Expose
    public String discountAmount;

    @SerializedName("orderTotal")
    @Expose
    public String orderTotal;

    @SerializedName("payableAmount")
    @Expose
    public String payableAmount;

    @SerializedName("deliveryRate")
    @Expose
    public String deliveryRate;

    @SerializedName("taxPayableAmount")
    @Expose
    public String taxPayableAmount;
}
