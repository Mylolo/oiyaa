package com.localvalu.eats.detail.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.eats.detail.dto.BusinessDetailDiscountForEatsDto;

import java.util.ArrayList;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class EatsDiscountListAdapter extends RecyclerView.Adapter<EatsDiscountListAdapter.ViewHolder> {
    public static final String TAG = EatsDiscountListAdapter.class.getSimpleName();
    private Activity activity;
    private OnItemClickListener onClickListener;
    private ArrayList<BusinessDetailDiscountForEatsDto> discountList;

    public EatsDiscountListAdapter(Activity activity, ArrayList<BusinessDetailDiscountForEatsDto> discountList) {
        this.activity = activity;
        this.discountList = discountList;
    }

    @NonNull
    @Override
    public EatsDiscountListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_eats_discount_list, viewGroup, false);
        return new EatsDiscountListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EatsDiscountListAdapter.ViewHolder viewHolder, final int i) {
        if (i % 2 == 0) {
            viewHolder.ll_main.setBackgroundColor(activity.getResources().getColor(R.color.colorEats));
        } else {
            viewHolder.ll_main.setBackgroundColor(activity.getResources().getColor(R.color.colorGray));
        }
        viewHolder.tv_heading.setText(discountList.get(i).fromTime + " - " + discountList.get(i).toTime + " Hrs");
        viewHolder.tv_disc_mon.setText(discountList.get(i).mon + "%");
        viewHolder.tv_disc_tue.setText(discountList.get(i).tue + "%");
        viewHolder.tv_disc_wed.setText(discountList.get(i).wed + "%");
        viewHolder.tv_disc_thu.setText(discountList.get(i).thu + "%");
        viewHolder.tv_disc_fri.setText(discountList.get(i).fri + "%");
        viewHolder.tv_disc_sat.setText(discountList.get(i).sat + "%");
        viewHolder.tv_disc_sun.setText(discountList.get(i).sun + "%");
        viewHolder.ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onItemClick(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return discountList.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

//    public class ViewHolder extends RecyclerView.ViewHolder {
//        public ViewHolder(@NonNull View itemView) {
//            super(itemView);
//        }
//    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout ll_main = itemView.findViewById(R.id.ll_main);
        TextView tv_heading = itemView.findViewById(R.id.tv_heading);
        TextView tv_disc_mon = itemView.findViewById(R.id.tv_disc_mon);
        TextView tv_disc_tue = itemView.findViewById(R.id.tv_disc_tue);
        TextView tv_disc_wed = itemView.findViewById(R.id.tv_disc_wed);
        TextView tv_disc_thu = itemView.findViewById(R.id.tv_disc_thu);
        TextView tv_disc_fri = itemView.findViewById(R.id.tv_disc_fri);
        TextView tv_disc_sat = itemView.findViewById(R.id.tv_disc_sat);
        TextView tv_disc_sun = itemView.findViewById(R.id.tv_disc_sun);

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
