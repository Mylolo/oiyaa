package com.localvalu.eats.detail.listener;

import com.localvalu.eats.detail.dto.FoodItem;
import com.localvalu.eats.detail.dto.FoodItemSizeDto;
import com.localvalu.eats.detail.dto.FoodMenuDto;

public interface FoodItemClickListener
{
    void onFoodItemClicked(FoodItemSizeDto foodItemSizeDto);
}
