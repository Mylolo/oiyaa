package com.localvalu.eats.detail.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.detail.dto.AddToCartResult;
import com.localvalu.eats.detail.dto.OrderNowResult;
import com.localvalu.eats.detail.dto.PaymentOrderDetailResult;

import java.io.Serializable;

public class OrderNowResultResponse implements Serializable
{

    @SerializedName("OrderNowResults")
    @Expose
    public OrderNowResult businessDetailsResult;

    @SerializedName("AddToCartResults")
    @Expose
    public AddToCartResult addToCartResult;

    @SerializedName("PaymentOrderdetails")
    @Expose
    public PaymentOrderDetailResult paymentOrderdetails;
}
