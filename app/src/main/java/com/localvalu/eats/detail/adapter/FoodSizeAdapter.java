package com.localvalu.eats.detail.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.R;
import com.localvalu.eats.detail.dto.FoodItemSizeDto;
import com.localvalu.eats.detail.dto.FoodMenuDto;
import com.localvalu.eats.detail.listener.FoodItemClickListener;
import java.util.List;

public class FoodSizeAdapter extends RecyclerView.Adapter<FoodSizeAdapter.ViewHolder>
{
    public static final String TAG = FoodSizeAdapter.class.getSimpleName();
    private Context context;
    private List<FoodItemSizeDto> data;
    private int rightPadding;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private FoodItemClickListener foodItemClickListener;
    private static AppCompatRadioButton lastChecked = null;
    private int lastCheckedPosition = -1;

    public FoodSizeAdapter(Context context,List<FoodItemSizeDto> data)
    {
        this.context = context;
        this.data = data;
        rightPadding = (int)context.getResources().getDimension(R.dimen._5sdp);
        strCurrencySymbol=context.getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter=context.getString(R.string.lbl_currency_letter_euro);
    }

    public void setFoodItemClickListener(FoodItemClickListener foodItemClickListener)
    {
        this.foodItemClickListener = foodItemClickListener;
    }

    @NonNull
    @Override
    public FoodSizeAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        Log.d(TAG, "onCreateViewHolder: ");
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_food_size, viewGroup, false);
        return new FoodSizeAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodSizeAdapter.ViewHolder viewHolder, final int i)
    {
        Log.d(TAG, "onCreateViewHolder: ");
        try
        {
            /** Useful for single recycler view */
            //viewHolder.radioButtonFoodSize.setChecked(i == lastCheckedPosition);
            FoodItemSizeDto foodItemSizeDto = data.get(i);
            viewHolder.tvSizeName.setText(foodItemSizeDto.sizeName);
            float price = Float.parseFloat(foodItemSizeDto.mainPrice);
            StringBuilder strPrice = new StringBuilder();
            strPrice.append(strCurrencySymbol).append(String.format("%.2f",price));
            viewHolder.tvSizePrice.setText(strPrice.toString());
            viewHolder.itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    /** Useful for single recycler view */
                    /**int copyOfLastCheckedPosition = lastCheckedPosition;
                    lastCheckedPosition = i;
                    notifyItemChanged(copyOfLastCheckedPosition);
                    notifyItemChanged(lastCheckedPosition);
                    viewHolder.radioButtonFoodSize.setChecked(true);*/

                    if(foodItemClickListener!=null)
                        foodItemClickListener.onFoodItemClicked(foodItemSizeDto);
                }
            });
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public int getItemCount()
    {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        AppCompatTextView tvSizeName,tvSizePrice;
        AppCompatRadioButton radioButtonFoodSize;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            tvSizeName = itemView.findViewById(R.id.tvSizeName);
            tvSizePrice = itemView.findViewById(R.id.tvSizePrice);
            radioButtonFoodSize = itemView.findViewById(R.id.radioButtonFoodSize);
            /** Useful for single recyclerview parent*/
            /** radioButtonFoodSize.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {

                    int copyOfLastCheckedPosition = lastCheckedPosition;
                    lastCheckedPosition = getAdapterPosition();
                    notifyItemChanged(copyOfLastCheckedPosition);
                    notifyItemChanged(lastCheckedPosition);
                    itemView.performClick();
                }
            });*/
        }
    }

}
