package com.localvalu.eats.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryChargeDto
{
    @SerializedName("Retailorout")
    @Expose
    public String retailorOut;

    @SerializedName("RetailorName")
    @Expose
    public String retailorName;

    @SerializedName("Kilometer")
    @Expose
    public String kilometer;

    @SerializedName("Cost")
    @Expose
    public String cost;

    @SerializedName("deliverid")
    @Expose
    public String deliverId;

    @SerializedName("MinOrderval")
    @Expose
    public String minOrderValue;

    @SerializedName("Postcode")
    @Expose
    public String postCode;

    @SerializedName("Category")
    @Expose
    public String category;

    @SerializedName("errorCode")
    @Expose
    public String errorCode;

    @SerializedName("errorMessage")
    @Expose
    public String errorMessage;
}
