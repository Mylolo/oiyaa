package com.localvalu.eats.detail.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.detail.dto.AddToCartResult;

import java.io.Serializable;

public class SetAddToCartResultWrapper implements Serializable {

    @SerializedName("Cart")
    @Expose
    public AddToCartResult addToCartResult;
}
