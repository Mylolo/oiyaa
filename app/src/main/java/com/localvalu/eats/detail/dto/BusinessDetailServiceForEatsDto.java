package com.localvalu.eats.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessDetailServiceForEatsDto implements Serializable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("service_name")
    @Expose
    public String serviceName;
}
