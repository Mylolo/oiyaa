package com.localvalu.eats.listing.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.listing.dto.BusinessListDto;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;
import java.util.ArrayList;

public class BusinessListResult implements Serializable
{
    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("LoloWebSiteResult")
    @Expose
    public ArrayList<BusinessListDto> businessList;

    @SerializedName("msg")
    @Expose
    public String msg;
}
