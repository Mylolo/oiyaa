package com.localvalu.eats.listing.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.base.Resource
import com.localvalu.common.model.cuisinescategory.CuisinesCategoryListResponse
import com.localvalu.home.model.CuisinesCategoryRequest
import com.localvalu.home.repository.CuisinesCategoryListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CuisinesCategoryViewModel @Inject constructor(private val repository:CuisinesCategoryListRepository) : ViewModel()
{
    private val _cuisinesCategoryList: MutableLiveData<Resource<CuisinesCategoryListResponse>> = MutableLiveData()
    val cuisinesCategoryList: LiveData<Resource<CuisinesCategoryListResponse>>
        get() = _cuisinesCategoryList

    fun getCuisinesCategoryList(cuisinesCategoryRequest: CuisinesCategoryRequest) = viewModelScope.launch {
        _cuisinesCategoryList.value = Resource.Loading
        _cuisinesCategoryList.value = repository.cuisinesCategoryList(cuisinesCategoryRequest)
    }

}