package com.localvalu.eats.listing;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.common.model.cuisinescategory.CuisineCategoryData;
import com.localvalu.common.model.cuisinescategory.CuisinesCategoryListResponse;
import com.localvalu.databinding.FragmentEatsSearchFilterBinding;
import com.localvalu.eats.listing.adapter.EatsBusinessCategoryAdapter;
import com.localvalu.eats.listing.model.BusinessViewAll;
import com.localvalu.eats.listing.model.filter.CuisinesFilterResponse;
import com.localvalu.eats.listing.model.filter.request.CuisinesFilter;
import com.localvalu.eats.repository.CuisinesFilterRepository;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.GridSpacingItemDecoration;
import com.utility.PreferenceUtility;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class EatsFilterSearchFragment extends BaseFragment
{
    private static final String TAG = "EatsFilterSearchFragmen";

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 12345;


    private int colorEats,colorBlack;


    private GridLayoutManager gridLayoutManager;
    private EatsBusinessCategoryAdapter eatsBusinessCategoryAdapter;
    private Boolean mLocationPermissionsGranted = false;
    private Double latitude, longitude;
    private ArrayList<CuisineCategoryData> cuisinesCategoryListResult;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private CuisinesFilterRepository cuisinesFilterRepository;
    private List<String> selectedCategories=new ArrayList<>();
    private int verticalItemSpace;
    private HostActivityListener hostActivityListener;
    private FragmentEatsSearchFilterBinding binding;
    private StringBuilder strFoodTypeIds;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    // TODO: Rename and change types and number of parameters
    public static EatsFilterSearchFragment newInstance()
    {
        return new EatsFilterSearchFragment();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        colorBlack = ContextCompat.getColor(requireContext(), R.color.colorBlack);
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        cuisinesFilterRepository = new CuisinesFilterRepository();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentEatsSearchFilterBinding.inflate(inflater, container, false);
        buildObjectForHandlingAPI();
        getLocationPermission();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
        fetchCuisinesList();
    }

    private void getLocationPermission()
    {
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                mLocationPermissionsGranted = true;
            }
            else
            {
                ActivityCompat.requestPermissions(getActivity(),
                        permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        }
        else
        {
            ActivityCompat.requestPermissions(getActivity(),
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionsGranted = false;

        switch (requestCode)
        {
            case LOCATION_PERMISSION_REQUEST_CODE:
            {
                if (grantResults.length > 0)
                {
                    for (int i = 0; i < grantResults.length; i++)
                    {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                        {
                            mLocationPermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionsGranted = true;
                    //initialize our map
                }
            }
        }
    }

    private void setUpActionBar()
    {
        binding.tabLayout.tvTitle.setTextColor(colorEats);
        binding.tabLayout.tvTitle.setText(getString(R.string.lbl_your_cuisines));
        binding.tabLayout.toolbar.getNavigationIcon().setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP);
        binding.tabLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });
    }

    private void setProperties()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        verticalItemSpace = (int) getResources().getDimension(R.dimen._15sdp);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        binding.rvCategories.setLayoutManager(gridLayoutManager);
        binding.rvCategories.setHasFixedSize(true);
        binding.rvCategories.setItemViewCacheSize(20);
        binding.rvCategories.setDrawingCacheEnabled(true);
        binding.rvCategories.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        binding.tvError.setVisibility(View.GONE);
        binding.btnNext.setOnClickListener(_OnClickListener);
        int spanCount = 3; // 3 columns
        boolean includeEdge = false;
        binding.rvCategories.addItemDecoration(new GridSpacingItemDecoration(2,verticalItemSpace,false));
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    private void fetchCuisinesList()
    {
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("type",AppUtils.CUISINES);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<CuisinesCategoryListResponse> dtoCall = apiInterface.cuisinesCategoryList(body);
        dtoCall.enqueue(new Callback<CuisinesCategoryListResponse>()
        {
            @Override
            public void onResponse(Call<CuisinesCategoryListResponse> call, Response<CuisinesCategoryListResponse> response)
            {
                try
                {
                    if (response.body().getCuisinesCategoryListResult().getErrorDetails().getErrorCode() == 0)
                    {
                        setAdapter(response.body().getCuisinesCategoryListResult().getCuisinesCategoryListResults());
                    }
                    else
                    {

                    }
                }
                catch (Exception e)
                {
                }
            }

            @Override
            public void onFailure(Call<CuisinesCategoryListResponse> call, Throwable t)
            {

            }
        });
    }

    private void setAdapter(ArrayList<CuisineCategoryData> cuisinesCategoryListResult)
    {
        this.cuisinesCategoryListResult = cuisinesCategoryListResult;
        eatsBusinessCategoryAdapter = new EatsBusinessCategoryAdapter(requireContext(), cuisinesCategoryListResult);
        eatsBusinessCategoryAdapter.setOnClickListener(new EatsBusinessCategoryAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClick(int position)
            {

            }
        });
        binding.rvCategories.setAdapter(eatsBusinessCategoryAdapter);
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
            case R.id.btn_next:
                filterSearch();
                break;
        }
    }

    public boolean anyItemSelected()
    {
        if (cuisinesCategoryListResult == null)
        {
            cuisinesCategoryListResult = new ArrayList<>();
        }

        boolean isAnyItemSelected = false;

        strFoodTypeIds = new StringBuilder();

        for (CuisineCategoryData cuisineCategoryData : cuisinesCategoryListResult)
        {
            if (cuisineCategoryData.checkStatus == true)
            {
                strFoodTypeIds.append(cuisineCategoryData.getId());
                strFoodTypeIds.append(",");
                selectedCategories.add(cuisineCategoryData.getName());
                isAnyItemSelected = true;
            }
        }

        if(strFoodTypeIds.length()>0)
        {
            Character character = strFoodTypeIds.charAt(strFoodTypeIds.length()-1);
            if(character.toString().equals(","))
            {
                strFoodTypeIds.deleteCharAt(strFoodTypeIds.length()-1);
            }
        }

        if (!isAnyItemSelected)
        {
            Toast.makeText(getActivity(), "Select filter", Toast.LENGTH_SHORT).show();
            return false;
        }

        else return true;

    }

    public CuisinesFilter getCuisinesFilterApiPayload()
    {
        CuisinesFilter cuisinesFilter = new CuisinesFilter();
        cuisinesFilter.setType(Integer.toString(AppUtils.BMT_EATS));
        cuisinesFilter.setItem(strFoodTypeIds.toString());
        cuisinesFilter.setRate(Float.toString(binding.rbReviewRating.getRating()));
        return cuisinesFilter;
    }

    public void filterSearch()
    {
        if(anyItemSelected())
        {
            mProgressDialog.show();
            cuisinesFilterRepository.cuisinesFilter(getCuisinesFilterApiPayload()).subscribe(new SingleObserver<CuisinesFilterResponse>()
            {
                @Override
                public void onSubscribe(@NotNull Disposable d)
                {
                    compositeDisposable.add(d);
                }

                @Override
                public void onSuccess(@NotNull CuisinesFilterResponse cuisinesFilterResponse)
                {
                    mProgressDialog.dismiss();

                    try
                    {
                        if (cuisinesFilterResponse.getCuisinesFilter().getErrorDetails().getErrorCode() == 0)
                        {
                            BusinessViewAll businessViewAll = new BusinessViewAll();
                            businessViewAll.setHeader(getString(R.string.lbl_back));
                            businessViewAll.setFromAllFilter(true);
                            EatsListingFragment eatsListingFragment = EatsListingFragment.newInstance(cuisinesFilterResponse.getCuisinesFilter().getCuisinesFilterResults(),businessViewAll);
                            hostActivityListener.updateFragment(eatsListingFragment, true, EatsListingFragment.class.getName(), true);
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(cuisinesFilterResponse.getCuisinesFilter().getErrorDetails().getErrorMessage(), getActivity());
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(@NotNull Throwable e)
                {
                    mProgressDialog.dismiss();
                    Log.d(TAG, e.getMessage());

                    if (!CommonUtilities.checkConnectivity(getContext()))
                        DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                    else
                        DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                }
            });

        }
    }

    @Override
    public void onDestroy()
    {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
