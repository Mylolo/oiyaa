package com.localvalu.eats.listing.model.filter.request;

public class CuisinesFilter
{
    private String token = "/3+YFd5QZdSK9EKsB8+TlA==";
    private String type;
    private String item;
    private String Rate;

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getItem()
    {
        return item;
    }

    public void setItem(String item)
    {
        this.item = item;
    }

    public String getRate()
    {
        return Rate;
    }

    public void setRate(String rate)
    {
        Rate = rate;
    }
}
