package com.localvalu.eats.listing.adapter;

import static com.utility.AppUtils.VIEW_TYPE_BUSINESS;
import static com.utility.AppUtils.VIEW_TYPE_FILTERS;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.localvalu.R;
import com.localvalu.common.CategoryAdapter;
import com.localvalu.common.listeners.CategoryClicksListener;
import com.localvalu.common.model.cuisinescategory.CuisineCategoryData;
import com.localvalu.directory.listing.dto.BusinessListDto;
import com.localvalu.directory.listing.dto.HomeListParent;
import com.utility.AppUtils;
import com.utility.GridSpacingItemDecoration;

import java.util.ArrayList;

public class EatsHomeParentListAdapter extends ListAdapter<HomeListParent,
        EatsHomeParentListAdapter.EatsHomeListParentViewHolder> implements EatsHomeChildClickListener, CategoryClicksListener
{
    private static final String TAG = "EatsHomeParentListAdapter";

    private ArrayList<HomeListParent> data;
    private Context context;
    private EatsHomeParentClickListener parentClickListener;
    private EatsHomeChildClickListener childClickListener;
    private CategoryClicksListener categoryClicksListener;
    private int horizontalSpace;

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.context = recyclerView.getContext();
        horizontalSpace = (int) context.getResources().getDimension(R.dimen._10sdp);
    }

    public EatsHomeParentListAdapter()
    {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<HomeListParent> DIFF_CALLBACK = new DiffUtil.ItemCallback<HomeListParent>()
    {
        @Override
        public boolean areItemsTheSame(@NonNull HomeListParent oldItem, @NonNull HomeListParent newItem)
        {
            return oldItem.headerId == newItem.headerId;
        }

        @Override
        public boolean areContentsTheSame(@NonNull HomeListParent oldItem, @NonNull HomeListParent newItem)
        {
            return Integer.toString(oldItem.headerId).equals( Integer.toString(newItem.headerId));
        }
    };

    @SuppressLint("LongLogTag")
    @Override
    public int getItemViewType(int position)
    {

        if(position<getItemCount())
        {

            Log.d(TAG,"getItemViewType->size-> " + getItemCount() + ", pos->" + position);

            switch (getItem(position).viewType)
            {
                case VIEW_TYPE_BUSINESS:
                    return VIEW_TYPE_BUSINESS;
                case VIEW_TYPE_FILTERS:
                    return VIEW_TYPE_FILTERS;
                default:
                    return -1;
            }
        }
        return -1;
    }

    public void setParentClickListener(EatsHomeParentClickListener parentClickListener)
    {
        this.parentClickListener = parentClickListener;
    }

    public void setChildClickListener(EatsHomeChildClickListener childClickListener)
    {
        this.childClickListener = childClickListener;
    }

    public void setCategoryClicksListener(CategoryClicksListener categoryClicksListener)
    {
        this.categoryClicksListener = categoryClicksListener;
    }

    @NonNull
    @Override
    public EatsHomeParentListAdapter.EatsHomeListParentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        switch (getItemViewType(position))
        {
            case VIEW_TYPE_BUSINESS:
            case VIEW_TYPE_FILTERS:
                 View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_item_home_list_parent, viewGroup, false);
                 return new EatsHomeParentListAdapter.EatsHomeListParentViewHolder(view);
        }
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_item_home_list_parent, viewGroup, false);
        return new EatsHomeParentListAdapter.EatsHomeListParentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EatsHomeParentListAdapter.EatsHomeListParentViewHolder viewHolder, final int position)
    {
        //if(position<getItemCount())
        //{
            final HomeListParent homeListParent = getItem(position);
            viewHolder.tvHeading.setVisibility(View.GONE);
            viewHolder.btnViewAll.setVisibility(View.GONE);
            /*viewHolder.tvHeading.setText(homeListParent.strHeader);
            viewHolder.btnViewAll.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    if(parentClickListener!=null)
                    {
                        parentClickListener.onViewAllButtonClicked(homeListParent);
                    }
                }
            });*/

            switch (getItemViewType(position))
            {
                case VIEW_TYPE_BUSINESS:
                    if(homeListParent.businessList!=null)
                    {
                        if(homeListParent.businessList.size()>0)
                        {
                            EatsHomeChildListAdapter eatsHomeChildListAdapter = new EatsHomeChildListAdapter(getItemWidth());
                            viewHolder.rvHomeListChild.setLayoutManager(new GridLayoutManager(context,2));
                            viewHolder.rvHomeListChild.setAdapter(eatsHomeChildListAdapter);
                            viewHolder.rvHomeListChild.addItemDecoration(new GridSpacingItemDecoration(2,horizontalSpace,false));
                            eatsHomeChildListAdapter.setChildClickListener(this);
                            eatsHomeChildListAdapter.submitList(homeListParent.businessList);
                            viewHolder.tvEmptyListDashboard.setVisibility(View.GONE);
                            viewHolder.cnlDashboardItem.setVisibility(View.VISIBLE);
                            viewHolder.rvHomeListChild.invalidate();
                        }
                        else
                        {
                            viewHolder.tvEmptyListDashboard.setVisibility(View.VISIBLE);
                            viewHolder.cnlDashboardItem.setVisibility(View.GONE);

                            switch (homeListParent.headerId)
                            {
                                case 2:
                                    viewHolder.tvEmptyListDashboard.setText(context.getString(R.string.lbl_no_nearby_restaurants_found));
                                    break;
                                case 3:
                                    viewHolder.tvEmptyListDashboard.setText(context.getString(R.string.lbl_no_top_rated_restaurants_found));
                                    break;
                                case 4:
                                    viewHolder.tvEmptyListDashboard.setText(context.getString(R.string.lbl_no_popular_business_found));
                                    break;
                            }
                        }
                    }
                    else
                    {
                        viewHolder.tvEmptyListDashboard.setVisibility(View.VISIBLE);
                        viewHolder.tvEmptyListDashboard.setTextColor(ContextCompat.getColor(context,R.color.colorLocal));
                        viewHolder.cnlDashboardItem.setVisibility(View.GONE);

                        switch (homeListParent.headerId)
                        {
                            case 2:
                                viewHolder.tvEmptyListDashboard.setText(context.getString(R.string.lbl_no_nearby_restaurants_found));
                                break;
                            case 3:
                                viewHolder.tvEmptyListDashboard.setText(context.getString(R.string.lbl_no_top_rated_restaurants_found));
                                break;
                            case 4:
                                viewHolder.tvEmptyListDashboard.setText(context.getString(R.string.lbl_no_popular_restaurants_found));
                                break;
                        }
                    }
                    break;
                case VIEW_TYPE_FILTERS:
                    if(homeListParent.cuisinesCategoryDataArrayList!=null)
                    {
                        if(homeListParent.cuisinesCategoryDataArrayList.size()>0)
                        {
                            CategoryAdapter categoryAdapter = new CategoryAdapter(homeListParent.cuisinesCategoryDataArrayList,AppUtils.BMT_EATS);
                            viewHolder.rvHomeListChild.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL, false));
                            viewHolder.rvHomeListChild.setAdapter(categoryAdapter);
                            categoryAdapter.setOnCategoryClicksListener(this);
                            viewHolder.tvEmptyListDashboard.setVisibility(View.GONE);
                            viewHolder.cnlDashboardItem.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            viewHolder.tvEmptyListDashboard.setVisibility(View.VISIBLE);
                            viewHolder.cnlDashboardItem.setVisibility(View.GONE);

                            switch (homeListParent.headerId)
                            {
                                case 1:
                                    viewHolder.tvEmptyListDashboard.setText(context.getString(R.string.lbl_no_cuisines_found));
                                    break;
                            }
                        }
                    }
                    else
                    {
                        viewHolder.tvEmptyListDashboard.setVisibility(View.VISIBLE);
                        viewHolder.tvEmptyListDashboard.setTextColor(ContextCompat.getColor(context,R.color.colorLocal));
                        viewHolder.cnlDashboardItem.setVisibility(View.GONE);

                        switch (homeListParent.headerId)
                        {
                            case 1:
                                viewHolder.tvEmptyListDashboard.setText(context.getString(R.string.lbl_no_cuisines_found));
                                break;
                        }
                    }
                    break;
            }
       // }
    }

    public void removeItem(int position)
    {
        data.remove(position);
        notifyItemRemoved(position);
    }

    class EatsHomeListParentViewHolder extends RecyclerView.ViewHolder
    {
        public AppCompatTextView tvHeading,tvEmptyListDashboard;
        public MaterialButton btnViewAll;
        public RecyclerView rvHomeListChild;
        public ConstraintLayout cnlDashboardItem;

        public EatsHomeListParentViewHolder(@NonNull View itemView)
        {
            super(itemView);
            tvHeading = itemView.findViewById(R.id.tvHeading);
            tvEmptyListDashboard = itemView.findViewById(R.id.tvEmptyListDashboard);
            btnViewAll = itemView.findViewById(R.id.btnViewAll);
            rvHomeListChild = itemView.findViewById(R.id.rvHomeListChild);
            cnlDashboardItem  = itemView.findViewById(R.id.cnlDashboardItem);
        }
    }

    @Override
    public void onItemClicked(BusinessListDto businessListDto, int childPosition)
    {
        if(childClickListener!=null)
        {
            childClickListener.onItemClicked(businessListDto,childPosition);
        }
    }

    private int getItemWidth()
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        displayMetrics = context.getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        return (int) (width/2.25);
    }

    @Override
    public void onCategoryClicked(CuisineCategoryData cuisineCategoryData, int navigationType)
    {
        if(categoryClicksListener !=null)
        {
            categoryClicksListener.onCategoryClicked(cuisineCategoryData,navigationType);
        }
    }

}
