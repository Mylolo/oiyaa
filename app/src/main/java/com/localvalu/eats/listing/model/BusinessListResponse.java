package com.localvalu.eats.listing.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;
import java.util.ArrayList;

public class BusinessListResponse implements Serializable
{
    @SerializedName("LoloLocalCompleteResult")
    @Expose
    public BusinessListResult businessListResult;

}
