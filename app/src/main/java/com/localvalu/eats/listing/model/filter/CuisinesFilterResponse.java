package com.localvalu.eats.listing.model.filter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CuisinesFilterResponse
{
    @SerializedName("cuisinesFilter")
    @Expose
    private CuisinesFilter cuisinesFilter;

    public CuisinesFilter getCuisinesFilter()
    {
        return cuisinesFilter;
    }

    public void setCuisinesFilter(CuisinesFilter cuisinesFilter)
    {
        this.cuisinesFilter = cuisinesFilter;
    }
}