package com.localvalu.eats.listing.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.R;
import com.localvalu.directory.listing.dto.BusinessListDto;
import com.localvalu.tablebooking.ui.TableBookingListAdapter;
import com.squareup.picasso.Picasso;
import com.utility.AppUtils;

import java.util.ArrayList;

public class EatsHomeChildListAdapter extends ListAdapter<BusinessListDto, EatsHomeChildListAdapter.EatsHomeListParentViewHolder>
{
    private ArrayList<BusinessListDto> data;
    private Context context;
    private EatsHomeChildClickListener childClickListener;
    private int itemWidth;
    private int colorBlack;

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.context = recyclerView.getContext();
        colorBlack = context.getResources().getColor(R.color.colorBlack);
    }

    public EatsHomeChildListAdapter(int itemWidth)
    {
        super(DIFF_CALLBACK);
        this.itemWidth = itemWidth;
    }

    private static final DiffUtil.ItemCallback<BusinessListDto> DIFF_CALLBACK = new DiffUtil.ItemCallback<BusinessListDto>()
    {
        @Override
        public boolean areItemsTheSame(@NonNull BusinessListDto oldItem, @NonNull BusinessListDto newItem)
        {
            return oldItem.businessId == newItem.businessId;
        }

        @Override
        public boolean areContentsTheSame(@NonNull BusinessListDto oldItem, @NonNull BusinessListDto newItem)
        {
            return oldItem.businessId.equals(newItem.businessId);
        }
    };

    public void setChildClickListener(EatsHomeChildClickListener childClickListener)
    {
        this.childClickListener = childClickListener;
    }

    @NonNull
    @Override
    public EatsHomeChildListAdapter.EatsHomeListParentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_item_home_list_child, viewGroup, false);
        return new EatsHomeChildListAdapter.EatsHomeListParentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EatsHomeChildListAdapter.EatsHomeListParentViewHolder viewHolder, @SuppressLint("RecyclerView") final int position)
    {
        final BusinessListDto businessListDto = getItem(viewHolder.getAdapterPosition());
        viewHolder.ivOrderOnline.getDrawable().setColorFilter(colorBlack, PorterDuff.Mode.SRC_ATOP);
        viewHolder.ivOrderAtTable.getDrawable().setColorFilter(colorBlack, PorterDuff.Mode.SRC_ATOP);
        viewHolder.ivBusinessLogo.setImageDrawable(context.getResources().getDrawable(R.drawable.app_logo));
        viewHolder.cvEatsHomeChildListRoot.setLayoutParams(getCardViewLayoutParams(viewHolder.cvEatsHomeChildListRoot));

        String rateBusiness = "0.0";
        if (!businessListDto.currentPromotionValue.equals(""))
        {
            rateBusiness = businessListDto.currentPromotionValue;
        }

        String rating = String.format("%.1f", Float.valueOf(rateBusiness));
        if (!rating.equals("0.0"))
        {
            viewHolder.tvPercentage.setVisibility(View.VISIBLE);
            viewHolder.tvPercentage.setText(businessListDto.currentPromotionValue + "%");
        }
        else
        {
            viewHolder.tvPercentage.setVisibility(View.GONE);
        }
        viewHolder.tvPercentage.setBackground(ContextCompat.getDrawable(context,R.drawable.top_left_rounded_background_local));
        viewHolder.tvBusinessName.setText(businessListDto.tradingname);
        viewHolder.tvAddress.setText(businessListDto.address1 + "," + businessListDto.country);
        viewHolder.tvDeliveryTime.setText("Delivery Time - 35 Mins");

        String reviewsCount = businessListDto.overAllreviewCount;
        StringBuilder totalReviews = new StringBuilder();
        totalReviews.append(reviewsCount).append(" ").append(context.getString(R.string.lbl_reviews));
        viewHolder.tvReviews.setText(totalReviews.toString());
        viewHolder.tvReviews.setVisibility(View.GONE);

        boolean orderOnlineAvailable = isOrderNow(businessListDto.bookingTypeId);
        boolean orderAtTableAvailable = isOrderAtTable(businessListDto.bookingTypeId);
        if(orderOnlineAvailable) viewHolder.ivOrderOnline.setVisibility(View.VISIBLE);
        else viewHolder.ivOrderOnline.setVisibility(View.INVISIBLE);
        if(orderAtTableAvailable) viewHolder.ivOrderAtTable.setVisibility(View.VISIBLE);
        else viewHolder.ivOrderAtTable.setVisibility(View.INVISIBLE);

        if (!businessListDto.getLogo("eats").trim().equals(""))
        {
            Picasso.with(context).load(businessListDto.getLogo("eats"))
                    .placeholder(R.drawable.progress_animation).error(R.drawable.app_logo).into(viewHolder.ivBusinessLogo);
            viewHolder.ivBusinessLogo.setScaleType(ImageView.ScaleType.CENTER_CROP);
            viewHolder.ivBusinessLogo.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));

        }
        else
        {
            viewHolder.ivBusinessLogo.setImageResource(R.drawable.app_logo);
            viewHolder.ivBusinessLogo.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            viewHolder.ivBusinessLogo.setBackgroundColor(context.getResources().getColor(R.color.colorEats));
        }


        viewHolder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(childClickListener!=null)
                {
                    childClickListener.onItemClicked(businessListDto,position);
                }
            }
        });
    }

    public void removeItem(int position)
    {
        data.remove(position);
        notifyItemRemoved(position);
    }

    class EatsHomeListParentViewHolder extends RecyclerView.ViewHolder
    {
        public AppCompatImageView ivBusinessLogo;
        public AppCompatTextView tvPercentage,tvBusinessName,tvReviews,tvDeliveryTime,tvAvailableOptions,tvAddress;
        public CardView cvEatsHomeChildListRoot;
        public AppCompatImageView ivOrderOnline,ivOrderAtTable,ivOrderAtStore;

        public EatsHomeListParentViewHolder(@NonNull View itemView)
        {
            super(itemView);
            cvEatsHomeChildListRoot = itemView.findViewById(R.id.cvEatsHomeChildListRoot);
            ivBusinessLogo = itemView.findViewById(R.id.ivBusinessLogo);
            tvPercentage = itemView.findViewById(R.id.tvPercentage);
            tvBusinessName = itemView.findViewById(R.id.tvBusinessName);
            tvReviews = itemView.findViewById(R.id.tvReviews);
            tvDeliveryTime = itemView.findViewById(R.id.tvDeliveryTime);
            tvAvailableOptions = itemView.findViewById(R.id.tvAvailableOptions);
            tvAddress = itemView.findViewById(R.id.tvAddress);
            ivOrderOnline = itemView.findViewById(R.id.ivOrderOnline);
            ivOrderAtTable = itemView.findViewById(R.id.ivOrderAtTable);
            ivOrderAtStore = itemView.findViewById(R.id.ivOrderAtStore);
            ivOrderOnline.setVisibility(View.GONE);
            ivOrderAtTable.setVisibility(View.GONE);
            ivOrderAtStore.setVisibility(View.GONE);
        }
    }

    private boolean isOrderNow(String bookingTypeId)
    {
        return bookingTypeId.contains(AppUtils.ORDER_ONLINE);
    }

    private boolean isOrderAtTable(String bookingTypeId)
    {
        return bookingTypeId.contains(AppUtils.ORDER_AT_TABLE);
    }

    private RecyclerView.LayoutParams getCardViewLayoutParams(CardView cardView)
    {
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) cardView.getLayoutParams();
        layoutParams.width = itemWidth;
        return layoutParams;
    }

    private void setAvailableOptionsText(AppCompatTextView textView, BusinessListDto businessListDto)
    {
        StringBuilder strOrderOnlineAvailableOptions = new StringBuilder();
        boolean orderOnlineAvailable = isOrderNow(businessListDto.bookingTypeId);
        boolean orderAtTableAvailable = isOrderAtTable(businessListDto.bookingTypeId);
        if(orderOnlineAvailable)
        {
            strOrderOnlineAvailableOptions.append(context.getString(R.string.lbl_order_online)).append(" : ").append(context.getString(R.string.lbl_available)).append('\n');
        }
        else
        {
            strOrderOnlineAvailableOptions.append(context.getString(R.string.lbl_order_online)).append(" : ").append(context.getString(R.string.lbl_unavailable)).append('\n');
        }
        if(orderAtTableAvailable)
        {
            strOrderOnlineAvailableOptions.append(context.getString(R.string.lbl_order_at_table)).append(" : ").append(context.getString(R.string.lbl_available));
        }
        else
        {
            strOrderOnlineAvailableOptions.append(context.getString(R.string.lbl_order_at_table)).append(" : ").append(context.getString(R.string.lbl_unavailable));
        }
        textView.setText(strOrderOnlineAvailableOptions.toString());
    }

}
