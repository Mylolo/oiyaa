package com.localvalu.eats.listing.model.filter;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.model.ErrorDetails;
import com.localvalu.directory.listing.dto.BusinessListDto;

import java.util.ArrayList;

public class CuisinesFilter
{
    @SerializedName("ErrorDetails")
    @Expose
    private ErrorDetails errorDetails;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName(value = "cuisinesFilterResults",alternate = {"EatsNearBySearchResults"})
    @Expose
    private ArrayList<BusinessListDto> cuisinesFilterResults;

    public ErrorDetails getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public ArrayList<BusinessListDto> getCuisinesFilterResults()
    {
        return cuisinesFilterResults;
    }

    public void setCuisinesFilterResults(ArrayList<BusinessListDto> cuisinesFilterResults)
    {
        this.cuisinesFilterResults = cuisinesFilterResults;
    }
}
