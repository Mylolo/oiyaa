package com.localvalu.eats.listing.adapter;

import com.localvalu.directory.listing.dto.BusinessListDto;

import java.util.ArrayList;

public interface EatsHomeChildClickListener
{
    void onItemClicked(BusinessListDto businessListDto, int childPosition);
}
