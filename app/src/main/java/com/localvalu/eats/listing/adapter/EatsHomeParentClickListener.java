package com.localvalu.eats.listing.adapter;

import com.localvalu.directory.listing.dto.BusinessListDto;
import com.localvalu.directory.listing.dto.HomeListParent;

import java.util.ArrayList;

public interface EatsHomeParentClickListener
{
    void onViewAllButtonClicked(HomeListParent homeListParent);
}
