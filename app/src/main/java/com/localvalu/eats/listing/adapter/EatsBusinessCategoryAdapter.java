package com.localvalu.eats.listing.adapter;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.common.model.cuisinescategory.CuisineCategoryData;

import java.util.ArrayList;

public class EatsBusinessCategoryAdapter extends RecyclerView.Adapter<EatsBusinessCategoryAdapter.ViewHolder>
{
    public static final String TAG = EatsBusinessCategoryAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<CuisineCategoryData> cuisineCategoryDataArrayList;
    private OnItemClickListener onClickListener;

    public EatsBusinessCategoryAdapter(Context context, ArrayList<CuisineCategoryData> cuisineCategoryDataArrayList)
    {
        this.context = context;
        this.cuisineCategoryDataArrayList = cuisineCategoryDataArrayList;
    }

    @NonNull
    @Override
    public EatsBusinessCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_eats_business_category, viewGroup, false);
        return new EatsBusinessCategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final EatsBusinessCategoryAdapter.ViewHolder viewHolder, final int i)
    {
        //in some cases, it will prevent unwanted situations
        viewHolder.img_check_pic.setOnCheckedChangeListener(null);
        /*if (!sectorsList.get(i).image.trim().equals(""))
        {
            Picasso.with(context).load(sectorsList.get(i).image).placeholder(R.drawable.progress_animation)
                    .error(R.color.colorWhite).into(viewHolder.img_category);
        } else
        {
            viewHolder.img_category.setImageDrawable(ContextCompat.getDrawable(context,R.color.colorWhite));
        }*/
        viewHolder.tv_category.setText(cuisineCategoryDataArrayList.get(i).getName());
        if (cuisineCategoryDataArrayList.get(i).checkStatus == true)
        {
            viewHolder.img_check_pic.setChecked(true);
        } else
        {
            viewHolder.img_check_pic.setChecked(false);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

            }
        });
        viewHolder.img_check_pic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                buttonView.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        updateStatus(i, isChecked);
                    }
                }, 100);

            }
        });
    }

    @Override
    public int getItemCount()
    {
        return cuisineCategoryDataArrayList.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView img_category = itemView.findViewById(R.id.img_category);
        CheckBox img_check_pic = itemView.findViewById(R.id.img_check_pic);
        TextView tv_category = itemView.findViewById(R.id.tv_category);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }

    private void updateStatus(int position, boolean isChecked)
    {
        CuisineCategoryData cuisineCategoryDataTemp = cuisineCategoryDataArrayList.get(position);

        CuisineCategoryData newCuisineCategoryData = cuisineCategoryDataTemp;

        newCuisineCategoryData.checkStatus = isChecked;

        cuisineCategoryDataArrayList.remove(position);

        cuisineCategoryDataArrayList.add(position, newCuisineCategoryData);

        notifyDataSetChanged();
    }

}
