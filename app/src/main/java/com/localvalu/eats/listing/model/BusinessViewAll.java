package com.localvalu.eats.listing.model;

import com.localvalu.directory.listing.dto.BusinessListDto;

import java.io.Serializable;
import java.util.ArrayList;

public class BusinessViewAll implements Serializable
{
    private int viewResultType;
    private double latitude;
    private double longitude;
    private String header;
    private boolean fromAllFilter;
    public ArrayList<BusinessListDto> businessList;

    public int getViewResultType()
    {
        return viewResultType;
    }

    public void setViewResultType(int viewResultType)
    {
        this.viewResultType = viewResultType;
    }

    public double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }

    public String getHeader()
    {
        return header;
    }

    public void setHeader(String header)
    {
        this.header = header;
    }

    public boolean isFromAllFilter()
    {
        return fromAllFilter;
    }

    public void setFromAllFilter(boolean fromAllFilter)
    {
        this.fromAllFilter = fromAllFilter;
    }

    public ArrayList<BusinessListDto> getBusinessList()
    {
        return businessList;
    }

    public void setBusinessList(ArrayList<BusinessListDto> businessList)
    {
        this.businessList = businessList;
    }
}
