package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoucherPurchase
{
    @SerializedName("URL")
    @Expose
    public String url;

    @SerializedName("z2")
    @Expose
    public String z2;

    @SerializedName("z3")
    @Expose
    public String z3;

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getZ2()
    {
        return z2;
    }

    public void setZ2(String z2)
    {
        this.z2 = z2;
    }

    public String getZ3()
    {
        return z3;
    }

    public void setZ3(String z3)
    {
        this.z3 = z3;
    }
}
