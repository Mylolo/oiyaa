package com.localvalu.eats.payment.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.payment.dto.PlaceOrderResult;

import java.io.Serializable;

public class PlaceOrderResultWrapper implements Serializable {

    @SerializedName("Order")
    @Expose
    public PlaceOrderResult placeOrderResult;
}
