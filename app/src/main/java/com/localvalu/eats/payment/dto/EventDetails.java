
package com.localvalu.eats.payment.dto;


import com.google.gson.annotations.Expose;

import java.io.Serializable;


@SuppressWarnings("unused")
public class EventDetails implements Serializable
{

    @Expose
    private Double amount;
    @Expose
    private String description;
    @Expose
    private Integer eventId;
    @Expose
    private String type;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getEventId() {
        return eventId;
    }

    public void setEventId(Integer eventId) {
        this.eventId = eventId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
