package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnlinePaymentResponseOne
{
    @SerializedName("placeOrderOneResult")
    @Expose
    public PlaceOrderOneResult placeOrderOneResult;

    public PlaceOrderOneResult getPlaceOrderOneResult()
    {
        return placeOrderOneResult;
    }

    public void setPlaceOrderOneResult(PlaceOrderOneResult placeOrderOneResult)
    {
        this.placeOrderOneResult = placeOrderOneResult;
    }

}
