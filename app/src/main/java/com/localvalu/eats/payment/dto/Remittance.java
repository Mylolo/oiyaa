
package com.localvalu.eats.payment.dto;


import com.google.gson.annotations.Expose;

import java.io.Serializable;


@SuppressWarnings("unused")
public class Remittance implements Serializable
{

    @Expose
    private String merchantId;
    @Expose
    private String paymentAccountId;
    @Expose
    private String remittanceGroup;

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getPaymentAccountId() {
        return paymentAccountId;
    }

    public void setPaymentAccountId(String paymentAccountId) {
        this.paymentAccountId = paymentAccountId;
    }

    public String getRemittanceGroup() {
        return remittanceGroup;
    }

    public void setRemittanceGroup(String remittanceGroup) {
        this.remittanceGroup = remittanceGroup;
    }

}
