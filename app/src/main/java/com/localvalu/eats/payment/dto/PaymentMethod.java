
package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;

import java.io.Serializable;


@SuppressWarnings("unused")
public class PaymentMethod implements Serializable
{

    @Expose
    private BillingAddress billingAddress;
    @Expose
    private String billingEmail;
    @Expose
    private Gateway gateway;
    @Expose
    private String methodId;
    @Expose
    private String provider;
    @Expose
    private ProviderResponse providerResponse;
    @Expose
    private String saveMethod;

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public Gateway getGateway() {
        return gateway;
    }

    public void setGateway(Gateway gateway) {
        this.gateway = gateway;
    }

    public String getMethodId() {
        return methodId;
    }

    public void setMethodId(String methodId) {
        this.methodId = methodId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public ProviderResponse getProviderResponse() {
        return providerResponse;
    }

    public void setProviderResponse(ProviderResponse providerResponse) {
        this.providerResponse = providerResponse;
    }

    public String getSaveMethod() {
        return saveMethod;
    }

    public void setSaveMethod(String saveMethod) {
        this.saveMethod = saveMethod;
    }

}
