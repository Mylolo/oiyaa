
package com.localvalu.eats.payment.dto;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;


@SuppressWarnings("unused")
public class PAY360Result implements Serializable
{

    @Expose
    private String apiVersion;
    @Expose
    private EventDetails eventDetails;
    @Expose
    private List<MerchantInstruction> merchantInstructions;
    @Expose
    private String ownerId;
    @Expose
    private PaymentMethod paymentMethod;
    @Expose
    private String processedBy;
    @Expose
    private String processedByIsvId;
    @Expose
    private String processedFor;
    @Expose
    private String processedForIsvId;
    @Expose
    private Processing processing;
    @Expose
    private List<Purchase> purchases;
    @Expose
    private Remittance remittance;
    @Expose
    private Transaction transaction;

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public EventDetails getEventDetails() {
        return eventDetails;
    }

    public void setEventDetails(EventDetails eventDetails) {
        this.eventDetails = eventDetails;
    }

    public List<MerchantInstruction> getMerchantInstructions() {
        return merchantInstructions;
    }

    public void setMerchantInstructions(List<MerchantInstruction> merchantInstructions) {
        this.merchantInstructions = merchantInstructions;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getProcessedBy() {
        return processedBy;
    }

    public void setProcessedBy(String processedBy) {
        this.processedBy = processedBy;
    }

    public String getProcessedByIsvId() {
        return processedByIsvId;
    }

    public void setProcessedByIsvId(String processedByIsvId) {
        this.processedByIsvId = processedByIsvId;
    }

    public String getProcessedFor() {
        return processedFor;
    }

    public void setProcessedFor(String processedFor) {
        this.processedFor = processedFor;
    }

    public String getProcessedForIsvId() {
        return processedForIsvId;
    }

    public void setProcessedForIsvId(String processedForIsvId) {
        this.processedForIsvId = processedForIsvId;
    }

    public Processing getProcessing() {
        return processing;
    }

    public void setProcessing(Processing processing) {
        this.processing = processing;
    }

    public List<Purchase> getPurchases() {
        return purchases;
    }

    public void setPurchases(List<Purchase> purchases) {
        this.purchases = purchases;
    }

    public Remittance getRemittance() {
        return remittance;
    }

    public void setRemittance(Remittance remittance) {
        this.remittance = remittance;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

}
