package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CredoraxHPPRequest
{
    @SerializedName("URL")
    @Expose
    public String url;

    @SerializedName("z2")
    @Expose
    public String z2;

    @SerializedName("z3")
    @Expose
    public String z3;
}
