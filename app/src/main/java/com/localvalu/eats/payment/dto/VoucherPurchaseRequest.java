package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VoucherPurchaseRequest implements Serializable
{
    @SerializedName("Token")
    @Expose
    private String token;

    @SerializedName("AccountId")
    @Expose
    private String accountId;

    @SerializedName("MerchantId")
    @Expose
    private String merchantId;

    @SerializedName("SuccessUrl")
    @Expose
    private String successUrl;

    @SerializedName("FailureUrl")
    @Expose
    private String failureUrl;

    @SerializedName("BackUrl")
    @Expose
    private String backUrl;

    @SerializedName("payable Amount")
    @Expose
    private String payableAmount;

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getMerchantId()
    {
        return merchantId;
    }

    public void setMerchantId(String merchantId)
    {
        this.merchantId = merchantId;
    }

    public String getSuccessUrl()
    {
        return successUrl;
    }

    public void setSuccessUrl(String successUrl)
    {
        this.successUrl = successUrl;
    }

    public String getFailureUrl()
    {
        return failureUrl;
    }

    public void setFailureUrl(String failureUrl)
    {
        this.failureUrl = failureUrl;
    }

    public String getBackUrl()
    {
        return backUrl;
    }

    public void setBackUrl(String backUrl)
    {
        this.backUrl = backUrl;
    }

    public String getPayableAmount()
    {
        return payableAmount;
    }

    public void setPayableAmount(String payableAmount)
    {
        this.payableAmount = payableAmount;
    }
}
