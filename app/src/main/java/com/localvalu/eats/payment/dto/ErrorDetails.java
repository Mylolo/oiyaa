
package com.localvalu.eats.payment.dto;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


@SuppressWarnings("unused")
public class ErrorDetails implements Serializable
{

    @SerializedName("ErrorCode")
    private Long errorCode;
    @SerializedName("ErrorMessage")
    private String errorMessage;

    public Long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Long errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
