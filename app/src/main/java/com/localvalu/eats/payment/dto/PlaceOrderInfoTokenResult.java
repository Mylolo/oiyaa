package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.mycart.dto.SaleTransactionLoLoOrderResult;

import java.io.Serializable;

public class PlaceOrderInfoTokenResult implements Serializable {

    @SerializedName("SaleTransactionLoLoEatResult")
    @Expose
    public SaleTransactionLoLoOrderResult saleTransactionLoLoOrderResult;
}
