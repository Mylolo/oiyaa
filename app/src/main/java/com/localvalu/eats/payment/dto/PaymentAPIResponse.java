package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PaymentAPIResponse {
    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetails errorDetails;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("msg")
    @Expose
    public String message;
    @SerializedName(value = "placeOrderOne", alternate = {"placeOrderSecond"})
    @Expose
    public PlaceOrderResultResponse placeOrderResultResponse;

    public ErrorDetails getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PlaceOrderResultResponse getPlaceOrderResultResponse() {
        return placeOrderResultResponse;
    }

    public void setPlaceOrderResultResponse(PlaceOrderResultResponse placeOrderResultResponse) {
        this.placeOrderResultResponse = placeOrderResultResponse;
    }
}
