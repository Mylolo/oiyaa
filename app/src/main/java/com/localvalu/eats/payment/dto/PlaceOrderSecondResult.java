package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

public class PlaceOrderSecondResult
{
    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("msg")
    @Expose
    public String message;

    @SerializedName("placeOrderSecond")
    @Expose
    public PlaceOrderSecond placeOrderSecond;

    public ErrorDetailsDto getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetailsDto errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public PlaceOrderSecond getPlaceOrderSecond()
    {
        return placeOrderSecond;
    }

    public void setPlaceOrderSecond(PlaceOrderSecond placeOrderSecond)
    {
        this.placeOrderSecond = placeOrderSecond;
    }
}
