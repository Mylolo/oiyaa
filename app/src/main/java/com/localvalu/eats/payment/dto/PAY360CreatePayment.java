
package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class PAY360CreatePayment implements Serializable
{

    @SerializedName("ErrorDetails")
    private ErrorDetails errorDetails;
    @Expose
    private String msg;
    @SerializedName("PAY360Result")
    private PAY360Result pAY360Result;
    @Expose
    private String status;

    public ErrorDetails getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public PAY360Result getPAY360Result() {
        return pAY360Result;
    }

    public void setPAY360Result(PAY360Result pAY360Result) {
        this.pAY360Result = pAY360Result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
