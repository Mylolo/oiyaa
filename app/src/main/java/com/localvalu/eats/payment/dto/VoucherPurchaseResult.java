package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

public class VoucherPurchaseResult
{
    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("msg")
    @Expose
    public String message;

    @SerializedName("VoucherPurchaseOnlinePay")
    @Expose
    public VoucherPurchase voucherPurchase;

    public ErrorDetailsDto getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetailsDto errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public VoucherPurchase getVoucherPurchase()
    {
        return voucherPurchase;
    }

    public void setVoucherPurchase(VoucherPurchase voucherPurchase)
    {
        this.voucherPurchase = voucherPurchase;
    }

}
