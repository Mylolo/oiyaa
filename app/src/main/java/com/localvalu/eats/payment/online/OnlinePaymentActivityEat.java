package com.localvalu.eats.payment.online;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.localvalu.BuildConfig;
import com.localvalu.R;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.eats.mycart.dto.CheckoutDto;
import com.localvalu.eats.payment.EatsSuccessPopupForOrderSubmit;
import com.localvalu.eats.payment.dto.MerchantInstruction;
import com.localvalu.eats.payment.dto.OnlinePaymentResponseTwo;
import com.localvalu.eats.payment.dto.PAY360Result;
import com.localvalu.eats.payment.dto.PlaceOrderSecond;
import com.localvalu.eats.payment.dto.PlaceOrderSecondResult;
import com.localvalu.eats.payment.dto.VoucherPurchase;
import com.localvalu.eats.payment.dto.VoucherPurchaseRequest;
import com.localvalu.eats.payment.dto.PaymentIntegrationEat;
import com.localvalu.eats.payment.dto.VoucherPurchaseResponse;
import com.localvalu.eats.payment.dto.VoucherPurchaseResult;
import com.localvalu.eats.repository.PaymentRepository;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AlertDialogCallBack;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnlinePaymentActivityEat extends AppCompatActivity
{

    WebView webview;
    public ApiInterface apiInterface;
    private UserBasicInfo userBasicInfo;
    Context context;
    private ArrayList<CartDetailDto> cartDetails;
    private CheckoutDto checkoutDto;
    private String businessId, special_ins, allergy_ins, delivery;
    String TAG = "OnlinePaymentActivity";
    private ProgressDialog mProgressDialog;
    Activity activity;
    private boolean canGoBack = false;

    private PaymentRepository paymentRepository;
    private PAY360Result pay360Result;
    private String strEatsSuccessUrl;
    private String strEatsFailureUrl;
    private String strEatsBackUrl;
    private String pay360RequestId="0";


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_payment_eat);
        context = this;
        activity = this;
        paymentRepository = new PaymentRepository();
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(this, PreferenceUtility.APP_PREFERENCE_NAME);
        readFromBundle();
        setView();
    }

    private void setView()
    {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(context, PreferenceUtility.APP_PREFERENCE_NAME);
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        webview = findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(webViewClient);
        webview.setWebChromeClient(webChromeClient);
        fetchRedirectUrl();
        //callPaymentApi();
    }

    private void readFromBundle()
    {
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"readFromBundle");

        }
        Bundle bundle = getIntent().getBundleExtra("data");
        checkoutDto = (CheckoutDto) bundle.getSerializable(AppUtils.BUNDLE_CHECK_OUT);
        pay360Result = (PAY360Result) bundle.getSerializable(AppUtils.BUNDLE_PAY_360_RESULT);
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"readFromBundle-isOrderFromTable"+ checkoutDto.isOrderFromTableOrStore);
        }
    }

    /**
     * Fetch the redirect url and load it in web view.
     */
    private void fetchRedirectUrl()
    {
        if(pay360Result!=null)
        {
            if(pay360Result.getMerchantInstructions()!=null)
            {
                if(pay360Result.getMerchantInstructions().size()>0)
                {
                    MerchantInstruction merchantInstruction = pay360Result.getMerchantInstructions().get(0);

                    if(merchantInstruction!=null)
                    {
                        if(merchantInstruction.getParameters()!=null)
                        {
                            if(merchantInstruction.getParameters().getRedirectUrl()!=null)
                            {
                                if(!merchantInstruction.getParameters().getRedirectUrl().equals(""))
                                {
                                    pay360RequestId = merchantInstruction.getParameters().getRequestId();
                                    fetchAllStatusUrl();
                                    String url=merchantInstruction.getParameters().getRedirectUrl();
                                    url = url.replace("\\", "");
                                    webview.loadUrl(url);
                                }
                                else
                                {
                                    showCannotDoPayment();
                                }
                            }
                            else
                            {
                                showCannotDoPayment();
                            }
                        }
                        else
                        {
                            showCannotDoPayment();
                        }
                    }
                    else
                    {
                        showCannotDoPayment();
                    }
                }
                else
                {
                    showCannotDoPayment();
                }
            }
            else
            {
                showCannotDoPayment();
            }
        }
        else
        {
            showCannotDoPayment();
        }
    }

    private void fetchAllStatusUrl()
    {
        if(pay360Result.getPaymentMethod()!=null)
        {
            if(pay360Result.getPaymentMethod().getGateway()!=null)
            {
                if(pay360Result.getPaymentMethod().getGateway().getReturnUrl()!=null)
                {
                    if(!pay360Result.getPaymentMethod().getGateway().getReturnUrl().equals(""))
                    {
                       strEatsSuccessUrl = pay360Result.getPaymentMethod().getGateway().getReturnUrl();
                    }
                }
                if(pay360Result.getPaymentMethod().getGateway().getCancelUrl()!=null)
                {
                    if(!pay360Result.getPaymentMethod().getGateway().getCancelUrl().equals(""))
                    {
                        strEatsBackUrl = pay360Result.getPaymentMethod().getGateway().getCancelUrl();
                    }
                }
                if(pay360Result.getPaymentMethod().getGateway().getErrorUrl()!=null)
                {
                    if(!pay360Result.getPaymentMethod().getGateway().getErrorUrl().equals(""))
                    {
                        strEatsFailureUrl = pay360Result.getPaymentMethod().getGateway().getErrorUrl();
                    }
                }
            }
        }
    }

    private void showCannotDoPayment()
    {
        DialogUtility.showMessageOkWithCallback(getString(R.string.msg_cannot_make_payment), activity, new AlertDialogCallBack()
        {
            @Override
            public void onSubmit()
            {
                finish();
            }

            @Override
            public void onCancel()
            {
                finish();
            }
        });
    }

    public PaymentIntegrationEat getPaymentIntegration(String url)
    {
        PaymentIntegrationEat paymentIntegrationApi = new PaymentIntegrationEat();
        paymentIntegrationApi.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        paymentIntegrationApi.setAccountId(userBasicInfo.accountId);
        paymentIntegrationApi.setBusinessId(checkoutDto.businessDetailInfo.business_id);
        paymentIntegrationApi.setUserId(userBasicInfo.userId);
        paymentIntegrationApi.setSpecialIns(checkoutDto.specialInstructions);
        paymentIntegrationApi.setAllergyIns(checkoutDto.allergyInstructions);
        paymentIntegrationApi.setDistance("12");
        paymentIntegrationApi.setPaymentType("PAY-PAY360");
        paymentIntegrationApi.setOrderType(delivery);
        paymentIntegrationApi.setPlatforms("android");
        paymentIntegrationApi.setPay360RequestId(pay360RequestId);
        if(checkoutDto.isOrderFromTableOrStore)
        {
            String retailerTypeId = checkoutDto.businessDetailInfo.retailerTypeId;
            switch (Integer.parseInt(retailerTypeId))
            {
                case AppUtils.BMT_EATS:
                     paymentIntegrationApi.setTableId(checkoutDto.tableId);
                     paymentIntegrationApi.setPlaceOrder(AppUtils.PLACE_ORDER_AT_TABLE_EATS);
                     break;
                default:
                     paymentIntegrationApi.setStoreId(checkoutDto.tableId);
                     paymentIntegrationApi.setPlaceOrder(AppUtils.PLACE_ORDER_AT_TABLE_LOCAL);
                     break;
            }
            paymentIntegrationApi.setDescription(checkoutDto.tableOrderDescription);
        }
        return paymentIntegrationApi;
    }

    /**
     * First step - CallPaymentApi is the first api we need to call placeOrderOneApi.
     */
    /*private void callPaymentApi()
    {
        mProgressDialog.show();

        Call<OnlinePaymentResponseOne> getPaymentIntegration = apiInterface.startOnlinePayment(getPaymentIntegration(""));
        getPaymentIntegration.enqueue(new Callback<OnlinePaymentResponseOne>()
        {
            @Override
            public void onResponse(Call<OnlinePaymentResponseOne> call, Response<OnlinePaymentResponseOne> response)
            {
                try
                {
                    mProgressDialog.dismiss();

                    try
                    {
                        OnlinePaymentResponseOne onlinePaymentResponseOne = response.body();

                        PlaceOrderOneResult placeOrderOneResult = onlinePaymentResponseOne.getPlaceOrderOneResult();


                        if (placeOrderOneResult.errorDetails.errorCode == 0)
                        {
                            //load webview url here
                            PlaceOrderOne placeOrderOne = placeOrderOneResult.placeOrderOne;
                            if (placeOrderOne != null)
                            {
                                CredoraxHPPRequestResult credoraxHPPRequestResult = placeOrderOne.credoraxHPPRequestResult;
                                CredoraxHPPRequest credoraxHPPRequest = credoraxHPPRequestResult.credoraxHPPRequest;
                                String url=credoraxHPPRequest.url;
                                url = url.replace("\\", "");
                                webview.loadUrl(url);
                            }

                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(placeOrderOneResult.errorDetails.errorMessage, OnlinePaymentActivityEat.this);
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    mProgressDialog.dismiss();
                    Log.e(TAG, " : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<OnlinePaymentResponseOne> call, Throwable t)
            {
                mProgressDialog.dismiss();
                if (!CommonUtilities.checkConnectivity(OnlinePaymentActivityEat.this))
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), OnlinePaymentActivityEat.this);
                }
                else
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), OnlinePaymentActivityEat.this);
                }
            }
        });
    }*/

    WebViewClient webViewClient = new WebViewClient()
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            super.onPageStarted(view, url, favicon);
            Log.d(TAG, "onPageStarted: " + url);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            Log.d(TAG, "shouldOverrideUrlLoading: url-" + url);
            if(!strEatsSuccessUrl.equals("") && url.equals(strEatsSuccessUrl))
            {
                callOnlinePaymentSuccessAPI(url);
            }
            else if(!strEatsBackUrl.equals("")  && url.equals(strEatsBackUrl))
            {
                showErrorDialog();
            }
            else if(!strEatsFailureUrl.equals("")  && url.equals(strEatsFailureUrl))
            {
                DialogUtility.showMessageWithOk(getString(R.string.msg_payment_transaction_failure), OnlinePaymentActivityEat.this);
            }
            else
            {
                webview.loadUrl(url);
            }

            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request)
        {

            final Uri uri = request.getUrl();
            String url = uri.toString();
            Log.d(TAG, "shouldOverrideUrlLoading: url-" + url);
            if(!strEatsSuccessUrl.equals("") && url.equals(strEatsSuccessUrl))
            {
                callOnlinePaymentSuccessAPI(url);
            }
            else if(!strEatsBackUrl.equals("")  && url.equals(strEatsBackUrl))
            {
                showErrorDialog();
            }
            else if(!strEatsFailureUrl.equals("")  && url.equals(strEatsFailureUrl))
            {
                DialogUtility.showMessageWithOk(getString(R.string.msg_payment_transaction_failure), OnlinePaymentActivityEat.this);
            }
            else
            {
                webview.loadUrl(url);
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            super.onPageFinished(view, url);
            Log.d(TAG, "onPageFinished: " + url);
            mProgressDialog.dismiss();
        }
    };

    WebChromeClient webChromeClient = new WebChromeClient()
    {
        @Override
        public void onProgressChanged(WebView view, int progress)
        {
            mProgressDialog.show();

            if (progress == 100)
            {
                mProgressDialog.dismiss();
            }
        }
    };

    public void callOnlinePayMallVoucherHppRequest()
    {
        mProgressDialog.show();
        Call<VoucherPurchaseResponse> voucherPurchaseResponseCall = apiInterface.
                requestForVoucherPurchase(getVoucherPurchasePayload());
        voucherPurchaseResponseCall.enqueue(new Callback<VoucherPurchaseResponse>()
        {
            @Override
            public void onResponse(Call<VoucherPurchaseResponse> call, Response<VoucherPurchaseResponse> response)
            {
                try
                {
                    mProgressDialog.dismiss();
                    try
                    {
                        VoucherPurchaseResponse voucherPurchaseResponse = response.body();
                        VoucherPurchaseResult voucherPurchaseResult = voucherPurchaseResponse.getVoucherPurchaseResult();
                        if (voucherPurchaseResult.errorDetails.errorCode == 0)
                        {
                            //load webview url here
                            VoucherPurchase voucherPurchase = voucherPurchaseResult.getVoucherPurchase();
                            if (voucherPurchase != null)
                            {
                                webview.loadUrl(voucherPurchase.getUrl());
                            }

                            /*Bundle bundle = new Bundle();
                            bundle.putString("lisamoBonus", "" + placeOrderResultResponse.getLisamoBonus());
                            bundle.putString("bonusTokens", "" + placeOrderResultResponse.getBonusTokens());
                            bundle.putString("newTokenBalance", "" + placeOrderResultResponse.getNewTokenBalance());
                            ActivityController.startNextActivity(OnlinePaymentActivityEat.this, EatsSuccessPopupForOrderSubmit.class, bundle, false);*/
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(voucherPurchaseResult.errorDetails.errorMessage, OnlinePaymentActivityEat.this);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                catch (Exception e)
                {
                    mProgressDialog.dismiss();
                    Log.e(TAG, " : " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<VoucherPurchaseResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                if (!CommonUtilities.checkConnectivity(OnlinePaymentActivityEat.this))
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), OnlinePaymentActivityEat.this);
                }
                else
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), OnlinePaymentActivityEat.this);
                }
            }
        });

    }

    public VoucherPurchaseRequest getVoucherPurchasePayload()
    {
        VoucherPurchaseRequest voucherPurchaseRequest = new VoucherPurchaseRequest();
        voucherPurchaseRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        voucherPurchaseRequest.setAccountId(userBasicInfo.accountId);
        voucherPurchaseRequest.setMerchantId(businessId);
        voucherPurchaseRequest.setSuccessUrl("https://www.oiyaa.com/lolo-mall/voucher/GetMallVoucherAmount/" + businessId);
        voucherPurchaseRequest.setFailureUrl("https://www.oiyaa.com/lolo-mall/voucher/GetMallVoucherAmount/" + businessId);
        voucherPurchaseRequest.setBackUrl("https://www.oiyaa.com/lolo-mall/voucher/GetMallVoucherAmount/" + businessId);
        return voucherPurchaseRequest;
    }

    public void callOnlinePaymentSuccessAPI(String url)
    {
        mProgressDialog.show();
        Call<OnlinePaymentResponseTwo> onlinePaymentResponseTwoCall = null;
        if(checkoutDto.isOrderFromTableOrStore)
        {
            if(checkoutDto.isOrderFromEats)
            {
                onlinePaymentResponseTwoCall = apiInterface.finishOnlinePaymentTable(getPaymentIntegration(url));
            }
            else
            {
                onlinePaymentResponseTwoCall = apiInterface.finishOnlinePaymentLocalStore(getPaymentIntegration(url));
            }
        }
        else
        {
            if(checkoutDto.isOrderFromEats)
            {
                onlinePaymentResponseTwoCall = apiInterface.finishOnlinePayment(getPaymentIntegration(url));
            }
            else
            {
                onlinePaymentResponseTwoCall = apiInterface.finishOnlinePaymentLocal(getPaymentIntegration(url));
            }

        }
        onlinePaymentResponseTwoCall.enqueue(new Callback<OnlinePaymentResponseTwo>()
        {
            @Override
            public void onResponse(Call<OnlinePaymentResponseTwo> call, Response<OnlinePaymentResponseTwo> response)
            {
                try
                {
                    mProgressDialog.dismiss();
                    try
                    {
                        OnlinePaymentResponseTwo onlinePaymentResponseTwo = response.body();
                        PlaceOrderSecondResult placeOrderSecondResult = onlinePaymentResponseTwo.getPlaceOrderSecondResult();
                        if (placeOrderSecondResult.errorDetails.errorCode == 0)
                        {
                            //load webview url here
                            PlaceOrderSecond placeOrderSecond = placeOrderSecondResult.getPlaceOrderSecond();

                            if (placeOrderSecond != null)
                            {
                                Bundle bundle = new Bundle();
                                bundle.putString(AppUtils.BUNDLE_OIYAA_BONUS, "" + placeOrderSecond.getLvBonus());
                                bundle.putString(AppUtils.BUNDLE_BONUS_TOKENS, "" + placeOrderSecond.getBonusTokens());
                                bundle.putString(AppUtils.BUNDLE_NEW_TOKEN_BALANCE, "" + placeOrderSecond.getNewTokenBalance());
                                int navigationType = AppUtils.BMT_EATS;
                                if(!checkoutDto.isOrderFromEats)
                                {
                                    navigationType = AppUtils.BMT_LOCAL;
                                }
                                bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, navigationType);
                                ActivityController.startNextActivity(OnlinePaymentActivityEat.this, EatsSuccessPopupForOrderSubmit.class, bundle, false);
                            }
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(placeOrderSecondResult.errorDetails.errorMessage, OnlinePaymentActivityEat.this);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                catch (Exception e)
                {
                    mProgressDialog.dismiss();
                    Log.e(TAG, " : " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<OnlinePaymentResponseTwo> call, Throwable t)
            {
                mProgressDialog.dismiss();
                if (!CommonUtilities.checkConnectivity(OnlinePaymentActivityEat.this))
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), OnlinePaymentActivityEat.this);
                }
                else
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), OnlinePaymentActivityEat.this);
                }
            }
        });

    }

    public static String getIpAddress(Context context)
    {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext()
                .getSystemService(WIFI_SERVICE);

        String ipAddress = intToInetAddress(wifiManager.getDhcpInfo().ipAddress).toString();

        ipAddress = ipAddress.substring(1);

        return ipAddress;
    }

    public static InetAddress intToInetAddress(int hostAddress)
    {
        byte[] addressBytes = {(byte) (0xff & hostAddress),
                (byte) (0xff & (hostAddress >> 8)),
                (byte) (0xff & (hostAddress >> 16)),
                (byte) (0xff & (hostAddress >> 24))};

        try
        {
            return InetAddress.getByAddress(addressBytes);
        }
        catch (UnknownHostException e)
        {
            throw new AssertionError();
        }
    }

    private void showErrorDialog()
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(getString(R.string.lbl_kindly_verify_details));
        dialog.setTitle(R.string.dialog_title);
        dialog.setNeutralButton(this.getResources().getString(R.string.okay), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                OnlinePaymentActivityEat.this.finish();
            }
        });
        dialog.show();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        if (!canGoBack)
        {

        }
        else
        {

        }
    }

}
