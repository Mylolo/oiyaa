
package com.localvalu.eats.payment.dto;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@SuppressWarnings("unused")
public class Pay360Response implements Serializable
{

    @SerializedName("PAY360CreatePayment")
    private PAY360CreatePayment pay360CreatePayment;

    public PAY360CreatePayment getPAY360CreatePayment() {
        return pay360CreatePayment;
    }

    public void setPAY360CreatePayment(PAY360CreatePayment pay360CreatePayment) {
        this.pay360CreatePayment = pay360CreatePayment;
    }

}
