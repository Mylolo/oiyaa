
package com.localvalu.eats.payment.dto;


import com.google.gson.annotations.Expose;

import java.io.Serializable;


@SuppressWarnings("unused")
public class Gateway implements Serializable
{

    @Expose
    private String cancelUrl;
    @Expose
    private String errorUrl;
    @Expose
    private String returnUrl;

    public String getCancelUrl() {
        return cancelUrl;
    }

    public void setCancelUrl(String cancelUrl) {
        this.cancelUrl = cancelUrl;
    }

    public String getErrorUrl() {
        return errorUrl;
    }

    public void setErrorUrl(String errorUrl) {
        this.errorUrl = errorUrl;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

}
