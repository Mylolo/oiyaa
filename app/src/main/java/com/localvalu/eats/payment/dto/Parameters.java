
package com.localvalu.eats.payment.dto;


import com.google.gson.annotations.Expose;

import java.io.Serializable;


@SuppressWarnings("unused")
public class Parameters implements Serializable
{

    @Expose
    private String redirectUrl;
    @Expose
    private String requestId;
    @Expose
    private String scpReference;
    @Expose
    private String status;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getScpReference() {
        return scpReference;
    }

    public void setScpReference(String scpReference) {
        this.scpReference = scpReference;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
