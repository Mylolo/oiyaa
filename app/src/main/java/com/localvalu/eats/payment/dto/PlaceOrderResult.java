package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;

public class PlaceOrderResult implements Serializable {

    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("msg")
    @Expose
    public String message;
    @SerializedName("PlaceOrder")
    @Expose
    public PlaceOrderInfoTokenResult placeOrderInfo;
}
