package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentIntegrationApi {

    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("AccountId")
    @Expose
    private String accountId;
    @SerializedName("BusinessId")
    @Expose
    private String businessId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("paymentType")
    @Expose
    private String paymentType;
    @SerializedName("special_ins")
    @Expose
    private String specialIns;
    @SerializedName("allergy_ins")
    @Expose
    private String allergyIns;
    @SerializedName("platforms")
    @Expose
    private String platforms;
    @SerializedName("Distance")
    @Expose
    private String distance;
    @SerializedName("OrderType")
    @Expose
    private String orderType;
    @SerializedName("SuccessUrl")
    @Expose
    private String successUrl;
    @SerializedName("FailureUrl")
    @Expose
    private String failureUrl;
    @SerializedName("BackUrl")
    @Expose
    private String backUrl;
    @SerializedName("IpAddress")
    @Expose
    private String ipAddress;
    @SerializedName("Tableid")
    @Expose
    private String tableId;
    @SerializedName("Storeid")
    @Expose
    private String storeId;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("placeOrder")
    @Expose
    private String placeOrder;

    public PaymentIntegrationApi() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getSpecialIns() {
        return specialIns;
    }

    public void setSpecialIns(String specialIns) {
        this.specialIns = specialIns;
    }

    public String getAllergyIns() {
        return allergyIns;
    }

    public void setAllergyIns(String allergyIns) {
        this.allergyIns = allergyIns;
    }

    public String getPlatforms()
    {
        return platforms;
    }

    public void setPlatforms(String platforms)
    {
        this.platforms = platforms;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getSuccessUrl() {
        return successUrl;
    }

    public void setSuccessUrl(String successUrl) {
        this.successUrl = successUrl;
    }

    public String getFailureUrl() {
        return failureUrl;
    }

    public void setFailureUrl(String failureUrl) {
        this.failureUrl = failureUrl;
    }

    public String getBackUrl() {
        return backUrl;
    }

    public void setBackUrl(String backUrl) {
        this.backUrl = backUrl;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getTableId()
    {
        return tableId;
    }

    public void setTableId(String tableId)
    {
        this.tableId = tableId;
    }

    public String getStoreId()
    {
        return storeId;
    }

    public void setStoreId(String storeId)
    {
        this.storeId = storeId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getPlaceOrder()
    {
        return placeOrder;
    }

    public void setPlaceOrder(String placeOrder)
    {
        this.placeOrder = placeOrder;
    }
}
