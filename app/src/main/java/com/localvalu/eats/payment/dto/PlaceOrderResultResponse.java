package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaceOrderResultResponse {

    @SerializedName("NewTokenBalance")
    @Expose
    private String newTokenBalance;
    @SerializedName("BonusTokens")
    @Expose
    private String bonusTokens;
    @SerializedName("LvBonus")
    @Expose
    private String lvBonus;
    @SerializedName("ShowModalPopUp")
    @Expose
    private Integer showModalPopUp;
    @SerializedName("CredroxtransId")
    @Expose
    private String credroxTransId;
    @SerializedName("URL")
    @Expose
    private String url;

    public String getNewTokenBalance()
    {
        return newTokenBalance;
    }

    public void setNewTokenBalance(String newTokenBalance)
    {
        this.newTokenBalance = newTokenBalance;
    }

    public String getBonusTokens()
    {
        return bonusTokens;
    }

    public void setBonusTokens(String bonusTokens)
    {
        this.bonusTokens = bonusTokens;
    }

    public String getLvBonus()
    {
        return lvBonus;
    }

    public void setLvBonus(String lvBonus)
    {
        this.lvBonus = lvBonus;
    }

    public Integer getShowModalPopUp()
    {
        return showModalPopUp;
    }

    public void setShowModalPopUp(Integer showModalPopUp)
    {
        this.showModalPopUp = showModalPopUp;
    }

    public String getCredroxTransId() {
        return credroxTransId;
    }

    public void setCredroxTransId(String credroxTransId) {
        this.credroxTransId = credroxTransId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
