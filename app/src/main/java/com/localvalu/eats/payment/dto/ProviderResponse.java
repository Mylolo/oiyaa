
package com.localvalu.eats.payment.dto;


import com.google.gson.annotations.Expose;

import java.io.Serializable;


@SuppressWarnings("unused")
public class ProviderResponse implements Serializable
{

    @Expose
    private String captured;
    @Expose
    private String result;
    @Expose
    private String settlementMode;
    @Expose
    private String status;

    public String getCaptured() {
        return captured;
    }

    public void setCaptured(String captured) {
        this.captured = captured;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getSettlementMode() {
        return settlementMode;
    }

    public void setSettlementMode(String settlementMode) {
        this.settlementMode = settlementMode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
