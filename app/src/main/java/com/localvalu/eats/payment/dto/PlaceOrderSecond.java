package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaceOrderSecond
{
    @SerializedName("NewTokenBalance")
    @Expose
    private String newTokenBalance;
    @SerializedName("BonusTokens")
    @Expose
    private String bonusTokens;
    @SerializedName("LvBonus")
    @Expose
    private String lvBonus;
    @SerializedName("CredroxtransId")
    @Expose
    private String credroxtransId;
    @SerializedName("ShowModalPopUp")
    @Expose
    private Integer showModalPopUp;

    public String getNewTokenBalance()
    {
        return newTokenBalance;
    }

    public void setNewTokenBalance(String newTokenBalance)
    {
        this.newTokenBalance = newTokenBalance;
    }

    public String getBonusTokens()
    {
        return bonusTokens;
    }

    public void setBonusTokens(String bonusTokens)
    {
        this.bonusTokens = bonusTokens;
    }

    public String getLvBonus()
    {
        return lvBonus;
    }

    public void setLvBonus(String lvBonus)
    {
        this.lvBonus = lvBonus;
    }

    public String getCredroxtransId()
    {
        return credroxtransId;
    }

    public void setCredroxtransId(String credroxtransId)
    {
        this.credroxtransId = credroxtransId;
    }

    public Integer getShowModalPopUp()
    {
        return showModalPopUp;
    }

    public void setShowModalPopUp(Integer showModalPopUp)
    {
        this.showModalPopUp = showModalPopUp;
    }
}
