package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaceOrderOne
{
    @SerializedName("CredoraxHPPRequestResult")
    @Expose
    public CredoraxHPPRequestResult credoraxHPPRequestResult;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("msg")
    @Expose
    public String message;

}
