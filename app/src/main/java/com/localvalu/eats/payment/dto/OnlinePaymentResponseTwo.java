package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnlinePaymentResponseTwo
{
    @SerializedName("placeOrderSecondResult")
    @Expose
    public PlaceOrderSecondResult placeOrderSecondResult;

    public PlaceOrderSecondResult getPlaceOrderSecondResult()
    {
        return placeOrderSecondResult;
    }

    public void setPlaceOrderSecondResult(PlaceOrderSecondResult placeOrderSecondResult)
    {
        this.placeOrderSecondResult = placeOrderSecondResult;
    }

}
