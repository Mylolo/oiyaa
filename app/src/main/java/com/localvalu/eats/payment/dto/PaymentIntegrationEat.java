package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentIntegrationEat
{

    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("AccountId")
    @Expose
    private String accountId;
    @SerializedName("BusinessId")
    @Expose
    private String businessId;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("paymentType")
    @Expose
    private String paymentType;
    @SerializedName("special_ins")
    @Expose
    private String specialIns;
    @SerializedName("allergy_ins")
    @Expose
    private String allergyIns;
    @SerializedName("platforms")
    @Expose
    private String platforms;
    @SerializedName("Distance")
    @Expose
    private String distance;
    @SerializedName("OrderType")
    @Expose
    private String orderType;
    @SerializedName("SuccessUrl")
    @Expose
    private String successUrl;
    @SerializedName("FailureUrl")
    @Expose
    private String failureUrl;
    @SerializedName("BackUrl")
    @Expose
    private String backUrl;

    @SerializedName("Tableid")
    @Expose
    private String tableId;

    @SerializedName("Storeid")
    @Expose
    private String storeId;

    @SerializedName("Description")
    @Expose
    private String description;

    @SerializedName("placeOrder")
    @Expose
    private String placeOrder;

    @SerializedName("K")
    @Expose
    private String K;

    @SerializedName("M")
    @Expose
    private String M;
    @SerializedName("O")
    @Expose
    private String O;
    @SerializedName("T")
    @Expose
    private String T;
    @SerializedName("V")
    @Expose
    private String V;
    @SerializedName("a1")
    @Expose
    private String a1;
    @SerializedName("a2")
    @Expose
    private String a2;
    @SerializedName("a4")
    @Expose
    private String a4;
    @SerializedName("a6")
    @Expose
    private String a6;
    @SerializedName("a7")
    @Expose
    private String a7;
    @SerializedName("a9")
    @Expose
    private String a9;
    @SerializedName("b1")
    @Expose
    private String b1;
    @SerializedName("b2")
    @Expose
    private String b2;
    @SerializedName("b3")
    @Expose
    private String b3;
    @SerializedName("b4")
    @Expose
    private String b4;
    @SerializedName("c1")
    @Expose
    private String c1;
    @SerializedName("g1")
    @Expose
    private String g1;
    @SerializedName("z1")
    @Expose
    private String z1;
    @SerializedName("z13")
    @Expose
    private String z13;
    @SerializedName("z14")
    @Expose
    private String z14;
    @SerializedName("z2")
    @Expose
    private String z2;
    @SerializedName("z3")
    @Expose
    private String z3;
    @SerializedName("z33")
    @Expose
    private String z33;
    @SerializedName("z34")
    @Expose
    private String z34;
    @SerializedName("z39")
    @Expose
    private String z39;
    @SerializedName("z4")
    @Expose
    private String z4;
    @SerializedName("z41")
    @Expose
    private String z41;
    @SerializedName("z6")
    @Expose
    private String z6;
    @SerializedName("pay360_requestId")
    @Expose
    private String pay360RequestId="0";

    public PaymentIntegrationEat() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getSpecialIns() {
        return specialIns;
    }

    public void setSpecialIns(String specialIns) {
        this.specialIns = specialIns;
    }

    public String getAllergyIns() {
        return allergyIns;
    }

    public void setAllergyIns(String allergyIns) {
        this.allergyIns = allergyIns;
    }

    public String getDistance() {
        return distance;
    }

    public String getPlatforms()
    {
        return platforms;
    }

    public void setPlatforms(String platforms)
    {
        this.platforms = platforms;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getSuccessUrl()
    {
        return successUrl;
    }

    public void setSuccessUrl(String successUrl)
    {
        this.successUrl = successUrl;
    }

    public String getFailureUrl()
    {
        return failureUrl;
    }

    public void setFailureUrl(String failureUrl)
    {
        this.failureUrl = failureUrl;
    }

    public String getBackUrl()
    {
        return backUrl;
    }

    public void setBackUrl(String backUrl)
    {
        this.backUrl = backUrl;
    }

    public String getTableId()
    {
        return tableId;
    }

    public void setTableId(String tableId)
    {
        this.tableId = tableId;
    }

    public String getStoreId()
    {
        return storeId;
    }

    public void setStoreId(String storeId)
    {
        this.storeId = storeId;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getK()
    {
        return K;
    }

    public void setK(String k)
    {
        K = k;
    }

    public String getM()
    {
        return M;
    }

    public void setM(String m)
    {
        M = m;
    }

    public String getO()
    {
        return O;
    }

    public void setO(String o)
    {
        O = o;
    }

    public String getT()
    {
        return T;
    }

    public void setT(String t)
    {
        T = t;
    }

    public String getV()
    {
        return V;
    }

    public void setV(String v)
    {
        V = v;
    }

    public String getA1()
    {
        return a1;
    }

    public void setA1(String a1)
    {
        this.a1 = a1;
    }

    public String getA2()
    {
        return a2;
    }

    public void setA2(String a2)
    {
        this.a2 = a2;
    }

    public String getA4()
    {
        return a4;
    }

    public void setA4(String a4)
    {
        this.a4 = a4;
    }

    public String getA6()
    {
        return a6;
    }

    public void setA6(String a6)
    {
        this.a6 = a6;
    }

    public String getA7()
    {
        return a7;
    }

    public void setA7(String a7)
    {
        this.a7 = a7;
    }

    public String getA9()
    {
        return a9;
    }

    public void setA9(String a9)
    {
        this.a9 = a9;
    }

    public String getB1()
    {
        return b1;
    }

    public void setB1(String b1)
    {
        this.b1 = b1;
    }

    public String getB2()
    {
        return b2;
    }

    public void setB2(String b2)
    {
        this.b2 = b2;
    }

    public String getB3()
    {
        return b3;
    }

    public void setB3(String b3)
    {
        this.b3 = b3;
    }

    public String getB4()
    {
        return b4;
    }

    public void setB4(String b4)
    {
        this.b4 = b4;
    }

    public String getC1()
    {
        return c1;
    }

    public void setC1(String c1)
    {
        this.c1 = c1;
    }

    public String getG1()
    {
        return g1;
    }

    public void setG1(String g1)
    {
        this.g1 = g1;
    }

    public String getZ1()
    {
        return z1;
    }

    public void setZ1(String z1)
    {
        this.z1 = z1;
    }

    public String getZ13()
    {
        return z13;
    }

    public void setZ13(String z13)
    {
        this.z13 = z13;
    }

    public String getZ14()
    {
        return z14;
    }

    public void setZ14(String z14)
    {
        this.z14 = z14;
    }

    public String getZ2()
    {
        return z2;
    }

    public void setZ2(String z2)
    {
        this.z2 = z2;
    }

    public String getZ3()
    {
        return z3;
    }

    public void setZ3(String z3)
    {
        this.z3 = z3;
    }

    public String getZ33()
    {
        return z33;
    }

    public void setZ33(String z33)
    {
        this.z33 = z33;
    }

    public String getZ34()
    {
        return z34;
    }

    public void setZ34(String z34)
    {
        this.z34 = z34;
    }

    public String getZ39()
    {
        return z39;
    }

    public void setZ39(String z39)
    {
        this.z39 = z39;
    }

    public String getZ4()
    {
        return z4;
    }

    public void setZ4(String z4)
    {
        this.z4 = z4;
    }

    public String getZ41()
    {
        return z41;
    }

    public void setZ41(String z41)
    {
        this.z41 = z41;
    }

    public String getZ6()
    {
        return z6;
    }

    public void setZ6(String z6)
    {
        this.z6 = z6;
    }

    public String getPlaceOrder()
    {
        return placeOrder;
    }

    public void setPlaceOrder(String placeOrder)
    {
        this.placeOrder = placeOrder;
    }

    public String getPay360RequestId()
    {
        return pay360RequestId;
    }

    public void setPay360RequestId(String pay360RequestId)
    {
        this.pay360RequestId = pay360RequestId;
    }
}
