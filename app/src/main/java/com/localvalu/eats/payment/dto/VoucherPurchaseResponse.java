package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoucherPurchaseResponse
{
    @SerializedName("VoucherPurchaseOnlinePayResult")
    @Expose
    public VoucherPurchaseResult voucherPurchaseResult;

    public VoucherPurchaseResult getVoucherPurchaseResult()
    {
        return voucherPurchaseResult;
    }

    public void setVoucherPurchaseResult(VoucherPurchaseResult voucherPurchaseResult)
    {
        this.voucherPurchaseResult = voucherPurchaseResult;
    }
}
