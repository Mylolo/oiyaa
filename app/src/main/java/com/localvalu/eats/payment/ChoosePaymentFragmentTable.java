package com.localvalu.eats.payment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.localvalu.BuildConfig;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentChoosePaymentTableBinding;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.eats.mycart.dto.CheckoutDto;
import com.localvalu.eats.payment.dto.OnlinePaymentWrapper;
import com.localvalu.eats.payment.dto.Pay360Request;
import com.localvalu.eats.payment.dto.Pay360Response;
import com.localvalu.eats.payment.dto.PaymentAPIResponse;
import com.localvalu.eats.payment.dto.PaymentIntegrationApi;
import com.localvalu.eats.payment.dto.PlaceOrderResultResponse;
import com.localvalu.eats.payment.online.OnlinePaymentActivityEat;
import com.localvalu.eats.repository.PaymentRepository;
import com.localvalu.user.dto.UserBasicInfo;
import com.localvalu.user.repository.UserAddressRepository;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChoosePaymentFragmentTable extends BaseFragment
{
    String TAG = "ChoosePaymentFragmentTable";

    private static final int REQUEST_CODE_PAYMENT = 786;
    public View view;
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private ArrayList<CartDetailDto> cartDetails;
    private String orderTotal, businessRetailerID, businessId, businessID, businessImage, businessName, businessType, businessAddress, totalLolaltyPointRedeemed,
            deliveryRate, tax, delivery, special_ins, allergy_ins;
    private FragmentChoosePaymentTableBinding binding;
    private String payment_type = "2";
    private UserAddressRepository userAddressRepository;
    private int colorEats;
    private CheckoutDto checkoutDto;
    private HostActivityListener hostActivityListener;

    private PaymentRepository paymentRepository;
    private String strEatsSuccessUrl;
    private String strEatsFailureUrl;
    private String strEatsBackUrl;

    public static ChoosePaymentFragmentTable newInstance(CheckoutDto checkoutDto)
    {
        ChoosePaymentFragmentTable choosePaymentFragment = new ChoosePaymentFragmentTable();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppUtils.BUNDLE_CHECK_OUT,checkoutDto);
        choosePaymentFragment.setArguments(bundle);
        return choosePaymentFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        userAddressRepository = new UserAddressRepository();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        readFromBundle();
        buildObjectForHandlingAPI();
        paymentRepository = new PaymentRepository();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentChoosePaymentTableBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
        binding.btnPlaceOrder.setOnClickListener(_OnClickListener);
    }

    private void setUpActionBar()
    {
        binding.toolbarLayout.toolbar.getNavigationIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private void readFromBundle()
    {
        Log.d(TAG,"readFromBundle");
        checkoutDto = (CheckoutDto) getArguments().getSerializable(AppUtils.BUNDLE_CHECK_OUT);
        Log.d(TAG,"readFromBundle - isOrderFromTable" + checkoutDto.isOrderFromTableOrStore);
        cartDetails = checkoutDto.cartList;
        orderTotal = checkoutDto.orderTotal;
        businessId = checkoutDto.businessDetailInfo.business_id;
        businessRetailerID = checkoutDto.businessDetailInfo.retailerId;
        //For review
        businessID = checkoutDto.businessDetailInfo.retailerId;
        businessImage = checkoutDto.businessDetailInfo.businessCoverImage;
        businessName = checkoutDto.businessDetailInfo.tradingname;
        businessType = checkoutDto.businessDetailInfo.businessType;
        businessAddress = getAddress(checkoutDto.businessDetailInfo);

        totalLolaltyPointRedeemed = checkoutDto.totalLoyaltyPointsRedeemed;
        deliveryRate = checkoutDto.deliveryRate;
        tax = checkoutDto.tax;
        delivery = checkoutDto.delivery;
        special_ins = checkoutDto.specialInstructions;
        allergy_ins = checkoutDto.allergyInstructions;

        System.out.println("cartDetails : " + cartDetails.toString());
        System.out.println("businessId : " + businessId);
    }

    private void setProperties()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        /*if(checkoutDto.businessDetailInfo!=null)
        {
            BusinessDetailInfo businessDetailInfo = checkoutDto.businessDetailInfo;
            if(businessDetailInfo.pay360Confirm.equals("0"))
            {
                payment_type="2";
                binding.radioButtonOnlinePayment.setVisibility(View.GONE);
            }
            else
            {
                payment_type="1";
                binding.radioButtonOnlinePayment.setVisibility(View.VISIBLE);
                binding.radioButtonOnlinePayment.setChecked(true);
            }
            switch (businessDetailInfo.DriverType)
            {
                case AppUtils.DRIVER_TYPE_NO_DRIVER:
                     break;
                case AppUtils.DRIVER_TYPE_OWN_DRIVER:
                     break;
                case AppUtils.DRIVER_TYPE_POOL_DRIVER:
                     break;
            }
        }*/
        binding.radioGroupPayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checked)
            {
                switch (checked)
                {
                    case R.id.radioButtonOnlinePayment:
                         payment_type = "1";
                         break;
                    case R.id.radioButtonPayAtRestaurant:
                         payment_type = "2";
                         break;
                }
            }
        });
        binding.etName.setText(userBasicInfo.name);
        binding.radioButtonOnlinePayment.setVisibility(View.GONE);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btnPlaceOrder:
                 if (validate())
                 {
                     if (payment_type.equalsIgnoreCase("1"))
                     {
                         createPayment();
                     }
                     else if(payment_type.equalsIgnoreCase("2"))
                     {
                         callPayAtRestaurant("CashOnDelivery");
                     }
                 }
                 break;
        }
    }

    private void createPayment()
    {
        mProgressDialog.show();
        Pay360Request pay360Request = getPay360Request();
        if(pay360Request!=null)
        {
            paymentRepository.startPayment(pay360Request).subscribe(new SingleObserver<Pay360Response>()
            {
                @Override
                public void onSubscribe(@NonNull Disposable d)
                {

                }

                @Override
                public void onSuccess(@NonNull Pay360Response pay360Response)
                {
                    mProgressDialog.dismiss();
                    if(pay360Response!=null)
                    {
                        if(pay360Response.getPAY360CreatePayment()!=null)
                        {
                            if(pay360Response.getPAY360CreatePayment().getErrorDetails().getErrorCode()==0)
                            {
                                try
                                {
                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable(AppUtils.BUNDLE_PAY_360_RESULT,
                                            pay360Response.getPAY360CreatePayment().getPAY360Result());
                                    bundle.putSerializable(AppUtils.BUNDLE_CHECK_OUT,
                                            checkoutDto);
                                    Intent intent = new Intent(getActivity(), OnlinePaymentActivityEat.class);
                                    intent.putExtra("data", bundle);
                                    requireActivity().startActivity(intent);
                                }
                                catch (Exception ex)
                                {
                                    ex.printStackTrace();
                                }

                            }
                            else
                            {
                                DialogUtility.showMessageWithOk(pay360Response.getPAY360CreatePayment().getErrorDetails().getErrorMessage(), getActivity());
                            }
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                        }
                    }
                }

                @Override
                public void onError(@NonNull Throwable e)
                {
                    Log.d(TAG,e.getMessage());
                    e.printStackTrace();
                    mProgressDialog.dismiss();
                    if (!CommonUtilities.checkConnectivity(getContext()))
                        DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                    else
                        DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                }
            });
        }
        else
        {
            mProgressDialog.dismiss();
        }
    }

    private Pay360Request getPay360Request()
    {
        Pay360Request pay360Request = new Pay360Request();
        pay360Request.setCALine1(userBasicInfo.address);
        pay360Request.setCALine2("");
        pay360Request.setCEmailAddress(userBasicInfo.email);
        pay360Request.setReturnUrl("https://www.oiyaa.com/");
        pay360Request.setCancelUrl("https://www.oiyaa.com/");
        pay360Request.setDeclineUrl("https://www.oiyaa.com/");
        pay360Request.setErrorUrl("https://www.oiyaa.com/");
        pay360Request.setCAPostcode(userBasicInfo.homePostcode);
        pay360Request.setCCustomerId(userBasicInfo.userId);
        if (BuildConfig.DEBUG)
        {
            pay360Request.setTDescription("Test");

        }
        else
        {
            pay360Request.setTDescription("Purchase");
        }
        pay360Request.setOrderAmount(checkoutDto.orderAmount);
        pay360Request.setDeliveryFee(checkoutDto.deliveryRate);
        pay360Request.setTotalAmount(checkoutDto.payableAmount);
        if(checkoutDto.businessDetailInfo != null)
        {
            BusinessDetailInfo businessDetailInfo = checkoutDto.businessDetailInfo;
            switch (businessDetailInfo.DriverType)
            {
                case AppUtils.DRIVER_TYPE_NO_DRIVER:
                    if(BuildConfig.DEBUG)
                    {
                        pay360Request.setOrderMerchantId("11004101");
                        pay360Request.setDeliveryMerchantId("11004101");
                    }
                    else
                    {
                        pay360Request.setOrderMerchantId(businessDetailInfo.pay360midID);
                        pay360Request.setDeliveryMerchantId(businessDetailInfo.retailerId);
                    }
                    break;
                default:
                    if(BuildConfig.DEBUG)
                    {
                        pay360Request.setOrderMerchantId("11004101");
                        pay360Request.setDeliveryMerchantId("11004101");
                    }
                    else
                    {
                        pay360Request.setOrderMerchantId(businessDetailInfo.pay360midID);
                        pay360Request.setDeliveryMerchantId(businessDetailInfo.pay360midID);
                    }
                    break;
            }
        }
        return pay360Request;
    }

    private boolean validate()
    {
        Log.d(TAG, "validate: delivery-" + delivery);
        if (payment_type.equalsIgnoreCase(""))
        {
            Toast.makeText(getContext(), getString(R.string.lbl_select_payment_method), Toast.LENGTH_LONG).show();
            return false;
        }
        else
        {
            return true;
        }
    }

    private String getAddress(BusinessDetailInfo businessDetailInfo)
    {
        StringBuilder strAddress = new StringBuilder();
        if (businessDetailInfo.address1.isEmpty())
        {
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        else
        {
            strAddress.append(businessDetailInfo.address1.trim());
            strAddress.append(" ");
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.city.isEmpty())
        {
            strAddress.append(businessDetailInfo.city.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.country.isEmpty())
        {
            strAddress.append(businessDetailInfo.country.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.postcode.isEmpty())
        {
            strAddress.append(businessDetailInfo.postcode.trim());
        }
        return strAddress.toString();
    }

    private void callPayAtRestaurant(String paymentType)
    {
        Log.d(TAG,"callPayAtRestaurant");
        mProgressDialog.show();

        PaymentIntegrationApi paymentIntegrationApi = new PaymentIntegrationApi();
        paymentIntegrationApi.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        paymentIntegrationApi.setAccountId(userBasicInfo.accountId);
        paymentIntegrationApi.setBusinessId(businessId);
        paymentIntegrationApi.setUserId(userBasicInfo.userId);
        paymentIntegrationApi.setPaymentType(paymentType);
        paymentIntegrationApi.setSpecialIns(special_ins);
        paymentIntegrationApi.setAllergyIns(allergy_ins);
        paymentIntegrationApi.setDistance("1");
        paymentIntegrationApi.setOrderType("");
        paymentIntegrationApi.setPlatforms("android");
        paymentIntegrationApi.setIpAddress(getIpAddress(getContext()));
        paymentIntegrationApi.setDescription(checkoutDto.tableOrderDescription);
        paymentIntegrationApi.setPlaceOrder(AppUtils.PLACE_ORDER_AT_TABLE_EATS);
        String retailerTypeId = checkoutDto.businessDetailInfo.retailerTypeId;
        Call<OnlinePaymentWrapper> getPaymentIntegration;
        switch (Integer.parseInt(retailerTypeId))
        {
            case AppUtils.BMT_EATS:
                 paymentIntegrationApi.setTableId(checkoutDto.tableId);
                 getPaymentIntegration= apiInterface.orderAtTableManualPayment(paymentIntegrationApi);
                 break;
            default:
                 paymentIntegrationApi.setStoreId(checkoutDto.tableId);
                 getPaymentIntegration= apiInterface.orderAtStoreLocalCOD(paymentIntegrationApi);
                 break;
        }


        getPaymentIntegration.enqueue(new Callback<OnlinePaymentWrapper>()
        {
            @Override
            public void onResponse(Call<OnlinePaymentWrapper> call, Response<OnlinePaymentWrapper> response)
            {
                try
                {
                    mProgressDialog.dismiss();

                    try
                    {
                        OnlinePaymentWrapper onlinePaymentWrapper = response.body();

                        PaymentAPIResponse paymentAPIResponse = onlinePaymentWrapper.getPaymentAPIResponse();
                        PlaceOrderResultResponse placeOrderResultResponse = paymentAPIResponse.getPlaceOrderResultResponse();

                        if (paymentAPIResponse.errorDetails.getErrorCode() == 0)
                        {
                            Bundle bundle = new Bundle();
                            bundle.putString(AppUtils.BUNDLE_OIYAA_BONUS, "" + placeOrderResultResponse.getLvBonus());
                            bundle.putString(AppUtils.BUNDLE_BONUS_TOKENS, "" + placeOrderResultResponse.getBonusTokens());
                            bundle.putString(AppUtils.BUNDLE_NEW_TOKEN_BALANCE, "" + placeOrderResultResponse.getNewTokenBalance());
                            //For review
                            bundle.putString("businessId", businessID);
                            bundle.putString("businessImage", businessImage);
                            bundle.putString("businessName", businessName);
                            bundle.putString("businessType", businessType);
                            bundle.putString("businessAddress", businessAddress);
                            ActivityController.startNextActivity(getActivity(), EatsSuccessPopupForOrderSubmit.class, bundle, false);
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(paymentAPIResponse.errorDetails.getErrorMessage(), getActivity());
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
                catch (Exception e)
                {
                    mProgressDialog.dismiss();
                    Log.e(TAG, " : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<OnlinePaymentWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.e(TAG, " : " + t.getMessage());
            }
        });
    }

    public static String getIpAddress(Context context)
    {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext()
                .getSystemService(context.WIFI_SERVICE);

        String ipAddress = intToInetAddress(wifiManager.getDhcpInfo().ipAddress).toString();

        ipAddress = ipAddress.substring(1);

        return ipAddress;
    }

    public static InetAddress intToInetAddress(int hostAddress)
    {
        byte[] addressBytes = {(byte) (0xff & hostAddress),
                (byte) (0xff & (hostAddress >> 8)),
                (byte) (0xff & (hostAddress >> 16)),
                (byte) (0xff & (hostAddress >> 24))};

        try
        {
            return InetAddress.getByAddress(addressBytes);
        }
        catch (UnknownHostException e)
        {
            throw new AssertionError();
        }
    }

    private void showProgress()
    {
        binding.progressLayout.progressBar.setVisibility(View.VISIBLE);
        binding.nestedScrollView.setVisibility(View.GONE);
        binding.errorLayout.layoutRoot.setVisibility(View.GONE);
    }

    private void showContents()
    {
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        binding.nestedScrollView.setVisibility(View.VISIBLE);
        binding.errorLayout.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage,boolean retry)
    {
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        binding.nestedScrollView.setVisibility(View.GONE);
        binding.errorLayout.layoutRoot.setVisibility(View.VISIBLE);
        binding.errorLayout.tvError.setText(strMessage);
        if(retry) binding.errorLayout.btnRetry.setVisibility(View.VISIBLE);
        else binding.errorLayout.btnRetry.setVisibility(View.GONE);
    }

}
