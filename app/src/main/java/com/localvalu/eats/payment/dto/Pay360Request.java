
package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Pay360Request {

    @SerializedName("CA_line1")
    private String cALine1;
    @SerializedName("CA_line2")
    private String cALine2;
    @SerializedName("CA_postcode")
    private String cAPostcode;
    @SerializedName("C_customerId")
    private String cCustomerId;
    @SerializedName("C_emailAddress")
    private String cEmailAddress;
    @Expose
    private String cancelUrl;
    @Expose
    private String cardHolderName="ECOMM";
    @Expose
    private String commerceType="ECOMM";
    @Expose
    private String declineUrl;
    @SerializedName("DeliveryFee")
    private String deliveryFee;
    @SerializedName("Delivery_merchantId")
    private String deliveryMerchantId;
    @Expose
    private String errorUrl;
    @Expose
    private String methodId="GATEWAY";
    @SerializedName("OrderAmount")
    private String orderAmount;
    @SerializedName("Order_merchantId")
    private String orderMerchantId;
    @Expose
    private String provider="SBS";
    @Expose
    private String returnUrl;
    @SerializedName("T_currency")
    private String tCurrency="GBP";
    @SerializedName("T_description")
    private String tDescription;
    @SerializedName("T_merchantReference")
    private String tMerchantReference;
    @SerializedName("T_type")
    private String tType="PAYMENT";
    @SerializedName("TotalAmount")
    private String totalAmount;

    public String getCALine1() {
        return cALine1;
    }

    public void setCALine1(String cALine1) {
        this.cALine1 = cALine1;
    }

    public String getCALine2() {
        return cALine2;
    }

    public void setCALine2(String cALine2) {
        this.cALine2 = cALine2;
    }

    public String getCAPostcode() {
        return cAPostcode;
    }

    public void setCAPostcode(String cAPostcode) {
        this.cAPostcode = cAPostcode;
    }

    public String getCCustomerId() {
        return cCustomerId;
    }

    public void setCCustomerId(String cCustomerId) {
        this.cCustomerId = cCustomerId;
    }

    public String getCEmailAddress() {
        return cEmailAddress;
    }

    public void setCEmailAddress(String cEmailAddress) {
        this.cEmailAddress = cEmailAddress;
    }

    public String getCancelUrl() {
        return cancelUrl;
    }

    public void setCancelUrl(String cancelUrl) {
        this.cancelUrl = cancelUrl;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getCommerceType() {
        return commerceType;
    }

    public void setCommerceType(String commerceType) {
        this.commerceType = commerceType;
    }

    public String getDeclineUrl() {
        return declineUrl;
    }

    public void setDeclineUrl(String declineUrl) {
        this.declineUrl = declineUrl;
    }

    public String getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(String deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public String getDeliveryMerchantId() {
        return deliveryMerchantId;
    }

    public void setDeliveryMerchantId(String deliveryMerchantId) {
        this.deliveryMerchantId = deliveryMerchantId;
    }

    public String getErrorUrl() {
        return errorUrl;
    }

    public void setErrorUrl(String errorUrl) {
        this.errorUrl = errorUrl;
    }

    public String getMethodId() {
        return methodId;
    }

    public void setMethodId(String methodId) {
        this.methodId = methodId;
    }

    public String getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    public String getOrderMerchantId() {
        return orderMerchantId;
    }

    public void setOrderMerchantId(String orderMerchantId) {
        this.orderMerchantId = orderMerchantId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getTCurrency() {
        return tCurrency;
    }

    public void setTCurrency(String tCurrency) {
        this.tCurrency = tCurrency;
    }

    public String getTDescription() {
        return tDescription;
    }

    public void setTDescription(String tDescription) {
        this.tDescription = tDescription;
    }

    public String getTMerchantReference() {
        return tMerchantReference;
    }

    public void setTMerchantReference(String tMerchantReference) {
        this.tMerchantReference = tMerchantReference;
    }

    public String getTType() {
        return tType;
    }

    public void setTType(String tType) {
        this.tType = tType;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

}
