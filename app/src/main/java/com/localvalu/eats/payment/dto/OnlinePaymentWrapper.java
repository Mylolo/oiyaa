package com.localvalu.eats.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnlinePaymentWrapper {
    @SerializedName(value = "placeOrderOneResult", alternate = {"placeOrderSecondResult"})
    @Expose
    public PaymentAPIResponse paymentAPIResponse;

    public PaymentAPIResponse getPaymentAPIResponse() {
        return paymentAPIResponse;
    }

    public void setPaymentAPIResponse(PaymentAPIResponse paymentAPIResponse) {
        this.paymentAPIResponse = paymentAPIResponse;
    }
}
