package com.localvalu.eats.transfertokens.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.transfertokens.dto.CustCurrentTokenResult;

import java.io.Serializable;

public class CustCurrentTokensResultWrapper implements Serializable {

    @SerializedName("CustCurrentTokensLTResult")
    @Expose
    public CustCurrentTokenResult custCurrentTokensLTResult;
}
