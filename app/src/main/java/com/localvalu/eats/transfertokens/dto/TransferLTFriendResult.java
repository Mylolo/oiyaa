package com.localvalu.eats.transfertokens.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;

public class TransferLTFriendResult implements Serializable {

    @SerializedName("NewBalance")
    @Expose
    public String newBalance;
}
