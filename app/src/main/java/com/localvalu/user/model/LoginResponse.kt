package com.localvalu.user.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.localvalu.common.model.ErrorDetails


data class LoginResponse(
    @SerializedName("custLoginResult") val loginResult: LoginResult
)