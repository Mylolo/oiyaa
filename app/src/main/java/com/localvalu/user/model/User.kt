package com.localvalu.user.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User(
    @PrimaryKey val user_id: String,
    val Address: String,
    val city: String,
    val country: String,
    val profile_pic: String,
    val AccountId: String,
    val Name: String,
    val MobileNo: String,
    val Email: String,
    val HomePostcode: String,
    val WorkPostcode: String,
    val AgeGroup: String,
    val Gender: String,
    val QRCode: String,
    val Passions: String,
    val FCMIdOutput:String
)