package com.localvalu.user.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.localvalu.common.model.ErrorDetails


data class LoginResult(
    @SerializedName("ErrorDetails") val errorDetails: ErrorDetails,
    @SerializedName("status") val status:String,
    @SerializedName("msg") val msg:String,
    @SerializedName("LoginDetails") val user: User
)