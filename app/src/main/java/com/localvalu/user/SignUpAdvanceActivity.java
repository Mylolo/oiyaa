package com.localvalu.user;

import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.R;
import com.localvalu.databinding.ActivitySignUpAdvanceBinding;
import com.localvalu.user.adapter.PassionListAdapter;
import com.localvalu.user.dto.PassionCategory;
import com.localvalu.user.dto.PassionDetails;
import com.localvalu.user.dto.PassionListRequest;
import com.localvalu.user.dto.RegisterDataDto;
import com.localvalu.user.wrapper.PassionListResultWrapper;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.CommonUtility;
import com.utility.Config;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpAdvanceActivity extends AppCompatActivity
{
    private static final String TAG = "SignUpAdvanceActivity";

    public ApiInterface apiInterface;
    private PassionListAdapter passionListAdapter;
    private List<PassionDetails> passionDetailsList;
    private RegisterDataDto registerDataDto;
    private LinearLayoutManager layoutManager;
    private String strPassions;
    private ActivitySignUpAdvanceBinding binding;
    private int colorWhite;
    private int colorBlack;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeLogin);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_sign_up_advance);
        colorBlack = ContextCompat.getColor(getApplicationContext(),R.color.colorBlack);
        colorWhite = ContextCompat.getColor(getApplicationContext(),R.color.colorWhite);
        setProperties();
        readFromBundle();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        getPassionList();

    }

    private void readFromBundle()
    {
        if (getIntent().getExtras() != null)
        {
            registerDataDto = (RegisterDataDto) getIntent().getExtras().getSerializable("registerDataDto");
            System.out.println("registerDataDto " + registerDataDto.toString());
        }
    }

    private void setProperties()
    {
        Drawable drawableBack = ResourcesCompat.getDrawable(getResources(),R.drawable.ic_app_arrow,null);
        drawableBack.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP);
        binding.imgBack.setImageDrawable(drawableBack);

        binding.imgBack.setOnClickListener(_OnClickListener);
        binding.layoutError.btnRetry.setOnClickListener(_OnClickListener);
        binding.btnContinueSignUpAdvance.setOnClickListener(_OnClickListener);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.rvPassionList.setLayoutManager(layoutManager);
        setPassionsText();
    }

    private void setPassionsText()
    {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        SpannableString str1= new SpannableString(getString(R.string.lbl_sign_up_advance_header));
        str1.setSpan(new ForegroundColorSpan(colorBlack), 0, str1.length(), 0);
        builder.append(str1).append(" ");

        SpannableString str2= new SpannableString(getString(R.string.lbl_sign_up_advance_header_a_white));
        str2.setSpan(new ForegroundColorSpan(colorWhite), 0, str2.length(), 0);
        builder.append(str2).append(" ");

        SpannableString str3= new SpannableString(getString(R.string.lbl_sign_up_advance_header_a_black));
        str3.setSpan(new ForegroundColorSpan(colorBlack), 0, str3.length(), 0);
        builder.append(str3).append(" ");

        binding.tvPassionMessageOne.setText(builder, TextView.BufferType.SPANNABLE);
        binding.tvPassionMessageTwo.setText(getString(R.string.lbl_sign_up_advance_header_b_black));
        binding.tvPassionMessageThree.setText(getString(R.string.lbl_sign_up_advance_header_c_black));
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                 finish();
                 break;
            case R.id.btnContinueSignUpAdvance:
                 if (isValid())
                 {
                     registerDataDto.passionFilled = AppUtils.PASSION_FILLED;
                     registerDataDto.passionDetails = strPassions;
                     Bundle bundle = new Bundle();
                     bundle.putSerializable("registerDataDto", registerDataDto);
                     ActivityController.startNextActivity(this, SignUpFinalActivity.class, bundle, false);
                  }
                break;
            case R.id.cl_main:
                 CommonUtility.hideKeyboard(this);
                 break;
            case R.id.ll_main:
                 CommonUtility.hideKeyboard(this);
                 break;
        }
    }

    private void setData(List<PassionCategory> passionCategoryList)
    {
        passionDetailsList  = new ArrayList<PassionDetails>();

        for(int i=0;i<passionCategoryList.size();i++)
        {
            PassionCategory category = passionCategoryList.get(i);

            PassionDetails passionDetailsHeader = new PassionDetails();
            passionDetailsHeader.type = PassionListAdapter.HEADER_VIEW_HOLDER;
            passionDetailsHeader.passion= category.passionCategory;
            passionDetailsList.add(passionDetailsHeader);

            for(int j=0;j<category.passionDetailsList.size();j++)
            {
                PassionDetails tempPassionDetails = category.passionDetailsList.get(j);
                PassionDetails passionDetailsItem = new PassionDetails();
                passionDetailsItem.type = PassionListAdapter.ITEM_VIEW_HOLDER;
                passionDetailsItem.passion= tempPassionDetails.passion;
                passionDetailsItem.checked=false;
                passionDetailsList.add(passionDetailsItem);
            }
        }
        if(passionDetailsList.size()>0)
        {
            showList();
            passionListAdapter = new PassionListAdapter(passionDetailsList);
            binding.rvPassionList.setAdapter(passionListAdapter);
        }

    }

    private void getPassionList()
    {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        showProgress();

        PassionListRequest passionListRequest = new PassionListRequest();

        Call<PassionListResultWrapper> dtoCall = apiInterface.getPassionList(passionListRequest);
        dtoCall.enqueue(new Callback<PassionListResultWrapper>()
        {
            @Override
            public void onResponse(Call<PassionListResultWrapper> call, Response<PassionListResultWrapper> response)
            {
                try
                {
                    if (response.code() != 400)
                    {
                        if (response.body().passionsListResult.errorDetails.errorCode == 0)
                        {
                            setData(response.body().passionsListResult.passionCategoryList);
                        }
                        else
                        {
                            showError(response.body().passionsListResult.errorDetails.errorMessage,false);
                        }
                    }
                    else
                    {
                        showError(getString(R.string.server_alert),false);
                    }
                }
                catch (Exception e)
                {
                    showError(getString(R.string.server_alert),false);
                }
            }

            @Override
            public void onFailure(Call<PassionListResultWrapper> call, Throwable t)
            {
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(SignUpAdvanceActivity.this))
                    showError(getString(R.string.network_unavailable),true);
                else
                    showError(getString(R.string.server_alert),true);
            }
        });

    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.rvPassionList.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showList()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvPassionList.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage,boolean retry)
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvPassionList.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if(retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

    private void getValueFromCheckBox(CheckBox cb)
    {
        if (cb.isChecked())
        {
            if (registerDataDto.passionDetails.trim().isEmpty())
            {
                registerDataDto.passionDetails = cb.getTag().toString();
            }
            else
            {
                registerDataDto.passionDetails = registerDataDto.passionDetails + "," + cb.getTag().toString();
            }
        }
    }

    public static String pad(int data)
    {
        return data >= 10 ? String.valueOf(data) : ("0" + data);
    }

    private boolean isValid()
    {
        StringBuilder strPassionStringBuilder = new StringBuilder();
        boolean valid=false;
        for(int i=0;i<passionDetailsList.size();i++)
        {
            PassionDetails passionDetails = passionDetailsList.get(i);
            if(passionDetails.checked)
            {
                valid=true;
                strPassionStringBuilder.append(passionDetails.passion);
                strPassionStringBuilder.append(",");
            }
        }
        if(valid)
        {
            strPassionStringBuilder.deleteCharAt(strPassionStringBuilder.length()-1);
            strPassions = strPassionStringBuilder.toString();
            return true;
        }
        else return false;
    }

}
