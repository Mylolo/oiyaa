package com.localvalu.user

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.localvalu.R
import com.localvalu.databinding.FragmentLoginBinding
import com.localvalu.user.model.LoginRequest
import com.localvalu.user.viewmodel.UserViewModel
import com.utility.*
import dagger.hilt.android.AndroidEntryPoint
import java.lang.StringBuilder

@AndroidEntryPoint
class LoginFragment: Fragment()
{
    private lateinit var binding: FragmentLoginBinding
    private var colorWhite = 0
    private var colorBlack = 0
    private val viewModel: UserViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        colorBlack = ContextCompat.getColor(requireContext(), R.color.colorBlack)
        colorWhite = ContextCompat.getColor(requireContext(), R.color.colorWhite)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentLoginBinding.inflate(inflater,container,false)
        return binding.clRoot
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
        observeSubscribers()
    }

    private fun observeSubscribers()
    {
        viewModel.user.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            showContents()
        })
        viewModel.loading.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if(it){
                showProgress()
            }
        })
        viewModel.failure.observe(viewLifecycleOwner, Observer {
            handleAPIError(it)
            {
                login()
            }
        })
    }

    private fun setProperties()
    {
        val type = ResourcesCompat.getFont(requireContext(), R.font.comfortaa_regular)
        binding!!.etPassword.inputType =
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        binding!!.etPassword.typeface = type
        setSupportingText()
        binding!!.cnlRegister.setOnClickListener(onClickListener)
        binding!!.tvForgotPassword.setOnClickListener(onClickListener)
        binding!!.tvLoginTermsAndConditions.setOnClickListener(onClickListener)
        binding!!.tvLoginPrivacyPolicy.setOnClickListener(onClickListener)
        binding!!.btnLogin.setOnClickListener(onClickListener)
    }

    private fun setSupportingText()
    {
        val builder = SpannableStringBuilder()
        val str1 = SpannableString(getString(R.string.lbl_login_bottom_text_one))
        str1.setSpan(ForegroundColorSpan(colorBlack), 0, str1.length, 0)
        builder.append(str1).append(" ")
        val str2 = SpannableString(getString(R.string.lbl_login_bottom_text_one_a))
        str2.setSpan(ForegroundColorSpan(colorWhite), 0, str2.length, 0)
        builder.append(str2).append(" ")
        binding!!.tvLoginTermsAndConditions.setText(builder, TextView.BufferType.SPANNABLE)
        val builder2 = SpannableStringBuilder()
        val str4 = SpannableString(getString(R.string.lbl_login_bottom_text_two))
        str4.setSpan(ForegroundColorSpan(colorBlack), 0, str4.length, 0)
        builder2.append(str4).append(" ")
        val str5 = SpannableString(getString(R.string.lbl_login_bottom_text_two_a))
        str5.setSpan(ForegroundColorSpan(colorWhite), 0, str5.length, 0)
        builder2.append(str5).append(" ")
        binding!!.tvLoginPrivacyPolicy.setText(builder2, TextView.BufferType.SPANNABLE)
    }

    private fun showProgress()
    {
        binding.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding.layoutError.layoutRoot.visibility = View.GONE
        binding.cnlMain.visibility = View.GONE
    }

    private fun showError(message:String,retry: Boolean)
    {
        binding.layoutProgress.layoutRoot.visibility = View.GONE
        binding.layoutError.layoutRoot.visibility = View.VISIBLE
        binding.cnlMain.visibility = View.GONE
    }

    private fun showContents()
    {
        binding.layoutProgress.layoutRoot.visibility = View.GONE
        binding.layoutError.layoutRoot.visibility = View.GONE
        binding.cnlMain.visibility = View.VISIBLE
    }

    var onClickListener =
        View.OnClickListener { view -> onButtonClick(view) }

    fun onButtonClick(view: View)
    {
        when (view.id)
        {
            R.id.cnlRegister->
            {
                //Redirect to register screen
            }
            R.id.btnLogin-> if (isValid)
            {
                login()
            }
            R.id.tvForgotPassword->
            {
                //Redirect to forget password screen
            }
            R.id.tvLoginTermsAndConditions ->
            {
                val strTACUri = StringBuilder()
                strTACUri.append("https://www.oiyaa.com//terms_conditions/WebsiteTermsandConditions.pdf")
                showTermsAndConditionsOrPrivacyPolicy(
                    strTACUri.toString(),
                    getString(R.string.lbl_open_terms_and_conditions_file)
                )
            }
            R.id.tvLoginPrivacyPolicy->
            {
                val strUri = StringBuilder()
                strUri.append("https://www.oiyaa.com//terms_conditions/AppPivacyPolicy.pdf")
                showTermsAndConditionsOrPrivacyPolicy(
                    strUri.toString(),
                    getString(R.string.lbl_open_privacy_policy_file)
                )
            }
        }
    }

    private fun getLoginRequest(): LoginRequest
    {
        val loginRequest = LoginRequest("",
            binding.etEmail.text.toString().trim(),
            binding.etPassword.text.toString().trim(),"")
        return loginRequest;
    }

    private fun login()
    {
        if (isValid)
        {
            viewModel.getUserInfo(getLoginRequest())
        }
    }

    private val isValid: Boolean
        private get()
        {
            if (!Patterns.EMAIL_ADDRESS.matcher(binding!!.etEmail.text.toString()).matches())
            {
                binding!!.etEmail.error = "Enter A Valid Email ID"
                binding!!.etEmail.requestFocus()
                return false
            }
            else if (binding!!.etPassword.text.toString().isEmpty())
            {
                binding!!.etPassword.error = "Enter Password"
                binding!!.etPassword.requestFocus()
                return false
            }
            else if (binding!!.etPassword.text.toString().length < 6)
            {
                binding!!.etPassword.error = "Password should be at least 6 digit."
                binding!!.etPassword.requestFocus()
                return false
            }
            return true
        }

    private fun gotoTermsAndConditionsPage()
    {
        /*Bundle bundle = new Bundle();
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_EATS);
        bundle.putBoolean(AppUtils.BUNDLE_IS_FROM_LOGIN_PAGE,true);
        ActivityController.startNextActivity(this, TermsAndConditionActivity.class, bundle, false);*/
    }

    private fun showTermsAndConditionsOrPrivacyPolicy(strUri: String, title: String)
    {
        val browserIntent = Intent(Intent.ACTION_VIEW)
        browserIntent.setDataAndType(Uri.parse(strUri), "application/pdf")
        val chooser = Intent.createChooser(browserIntent, title)
        chooser.flags = Intent.FLAG_ACTIVITY_NEW_TASK // optional
        startActivity(chooser)
    }

    private fun showTPrivacyPolicy(strUri: String, title: String)
    {
        val browserIntent = Intent(Intent.ACTION_VIEW)
        browserIntent.setDataAndType(Uri.parse(strUri), "application/pdf")
        val chooser = Intent.createChooser(
            browserIntent,
            getString(R.string.lbl_open_terms_and_conditions_file)
        )
        chooser.flags = Intent.FLAG_ACTIVITY_NEW_TASK // optional
        startActivity(chooser)
    }

}