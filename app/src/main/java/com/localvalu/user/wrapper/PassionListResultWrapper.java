package com.localvalu.user.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.PassionListResult;
import com.localvalu.user.dto.RegisterExistsCustomerResult;

import java.io.Serializable;

public class PassionListResultWrapper implements Serializable {

    @SerializedName("PassionsListResult")
    @Expose
    public PassionListResult passionsListResult;
}
