package com.localvalu.user.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.MyAccountResult;

import java.io.Serializable;

public class MyAccountResultWrapper implements Serializable {

    @SerializedName("UserDashboardDetailsResult")
    @Expose
    public MyAccountResult myAccountResult;
}
