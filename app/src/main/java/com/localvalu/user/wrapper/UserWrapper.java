package com.localvalu.user.wrapper;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.CustLoginResultDto;

public class UserWrapper implements Serializable {

    @SerializedName("custLoginResult")
    @Expose
    public CustLoginResultDto custLoginResult;
}
