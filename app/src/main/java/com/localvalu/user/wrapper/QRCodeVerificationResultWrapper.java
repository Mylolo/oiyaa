package com.localvalu.user.wrapper;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.QRCodeVerificationResult;

public class QRCodeVerificationResultWrapper implements Serializable {

    @SerializedName("QRCodeVerificationResult")
    @Expose
    public QRCodeVerificationResult qRCodeVerificationResult;

}
