package com.localvalu.user.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.RegisterCustomerResult;

import java.io.Serializable;

public class RegisterCustomerResultWrapper implements Serializable {

    @SerializedName("UserRegistrationResult")
    @Expose
    public RegisterCustomerResult registerCustomerResult;
}
