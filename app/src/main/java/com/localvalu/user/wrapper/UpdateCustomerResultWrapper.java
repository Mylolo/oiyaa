package com.localvalu.user.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.UpdateCustomerResult;

import java.io.Serializable;

public class UpdateCustomerResultWrapper implements Serializable {

    @SerializedName("UserDetailsUpdateResult")
    @Expose
    public UpdateCustomerResult updateCustomerDetails;
}
