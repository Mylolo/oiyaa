package com.localvalu.user;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.localvalu.BuildConfig;
import com.localvalu.R;
import com.localvalu.base.SplashScreen;
import com.localvalu.common.model.appversion.VersionCheck;
import com.localvalu.common.model.appversion.VersionCheckRequest;
import com.localvalu.common.model.appversion.VersionCheckResponse;
import com.localvalu.common.model.appversion.VersionCheckResult;
import com.localvalu.common.repositories.VersionCheckRepository;
import com.localvalu.databinding.ActivitySplashInfoBinding;
import com.localvalu.common.mytransaction.wrapper.UserDetailResultWrapper;
import com.localvalu.eats.payment.online.OnlinePaymentActivityEatTable;
import com.localvalu.user.dashboard.ui.DashboardActivity;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AlertDialogCallBack;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;
import static com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE;

public class SplashInfoActivity extends AppCompatActivity
{
    private static final String TAG = SplashInfoActivity.class.getSimpleName();
    //For API Call
    public ProgressDialog mProgressDialog;
    public ApiInterface apiInterface;
    private UserBasicInfo userBasicInfo;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private ActivitySplashInfoBinding binding;
    private int colorWhite;
    private int colorBlack;
    private VersionCheckRepository repository;
    private CompositeDisposable compositeDisposable= new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeLogin);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash_info);
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        colorBlack = ContextCompat.getColor(getApplicationContext(),R.color.colorBlack);
        colorWhite = ContextCompat.getColor(getApplicationContext(),R.color.colorWhite);
        bouldObjectForHandlingAPI();
        repository = VersionCheckRepository.getInstance();
        initView();
        callAPI();
        versionCheck();
    }

    private VersionCheckRequest getVersionCheckRequest()
    {
        VersionCheckRequest versionCheckRequest = new VersionCheckRequest();
        versionCheckRequest.setAccountId(userBasicInfo.accountId);
        return versionCheckRequest;
    }

    private void versionCheck()
    {
        repository.checkVersion(getVersionCheckRequest()).subscribe(new SingleObserver<VersionCheckResponse>()
        {
            @Override
            public void onSubscribe(@NonNull Disposable d)
            {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(@NonNull VersionCheckResponse versionCheckResponse)
            {
                if(versionCheckResponse!=null)
                {
                    if(versionCheckResponse.getVersionCheckResult()!=null)
                    {
                        VersionCheckResult versionCheckResult = versionCheckResponse.getVersionCheckResult();
                        if(versionCheckResult.getVersionCheck()!=null)
                        {
                            VersionCheck versionCheck = versionCheckResult.getVersionCheck();
                            if(versionCheck.getErrorDetails().getErrorCode() == 0)
                            {
                                Log.d(TAG,"onSuccess - version code " + versionCheck.getVersioncode());
                                int versionCode = Integer.parseInt(versionCheck.getVersioncode());
                                int existVersionCode = BuildConfig.VERSION_CODE;
                                if(versionCode>existVersionCode)
                                {
                                    showMandateUpdate();
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable e)
            {
                if(BuildConfig.DEBUG)
                {
                    e.printStackTrace();
                }
                //DialogUtility.showMessageWithOk(getString(R.string.server_alert),SplashInfoActivity.this);
            }
        });
    }

    private void showMandateUpdate()
    {
        DialogUtility.showMessageWithOkWithCallback(getString(R.string.dialog_title),
                getString(R.string.lbl_update_available),
                SplashInfoActivity.this,new AlertDialogCallBack()
                {
                    @Override
                    public void onSubmit()
                    {
                        redirectToPlayStore();
                    }

                    @Override
                    public void onCancel()
                    {

                    }
                },false);
    }

    private void redirectToPlayStore()
    {
        final String appPackageName = getPackageName(); // package name of the app
        try
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        }
        catch (android.content.ActivityNotFoundException anfe)
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void bouldObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    private void initView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(this, PreferenceUtility.APP_PREFERENCE_NAME);
        try
        {
            setSupportingText();
            binding.btnContinue.setOnClickListener(_OnClickListener);
            if (userBasicInfo.qRCodeOutput == null)
            {
                userBasicInfo.qRCodeOutput = "0";
            }
            binding.tvCustomerCode.setText(userBasicInfo.qRCodeOutput);
            Bitmap qrCodeImage = generateQRCode(userBasicInfo.qRCodeOutput);
            binding.ivQRCode.setImageBitmap(qrCodeImage);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setSupportingText()
    {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        SpannableString str1= new SpannableString(getString(R.string.lbl_splash_info_text_one));
        str1.setSpan(new ForegroundColorSpan(colorBlack), 0, str1.length(), 0);
        builder.append(str1).append(" ");

        SpannableString str2= new SpannableString(getString(R.string.lbl_splash_info_text_one_a_white));
        str2.setSpan(new ForegroundColorSpan(colorWhite), 0, str2.length(), 0);
        builder.append(str2).append(" ");

        SpannableString str3= new SpannableString(getString(R.string.lbl_splash_info_text_one_a_black));
        str3.setSpan(new ForegroundColorSpan(colorBlack), 0, str3.length(), 0);
        builder.append(str3).append(" ");

        binding.tvSplashTextOne.setText(builder, TextView.BufferType.SPANNABLE);

        SpannableStringBuilder builder2 = new SpannableStringBuilder();

        SpannableString str4= new SpannableString(getString(R.string.lbl_splash_info_text_two));
        str4.setSpan(new ForegroundColorSpan(colorWhite), 0, str4.length(), 0);
        builder2.append(str4).append(" ");

        SpannableString str5= new SpannableString(getString(R.string.lbl_splash_info_text_two_a));
        str5.setSpan(new ForegroundColorSpan(colorBlack), 0, str5.length(), 0);
        builder2.append(str5).append(" ");

        binding.tvSplashTextTwo.setText(builder2, TextView.BufferType.SPANNABLE);
    }

    // Checks that the update is not stalled during 'onResume()'.
   // However, you should execute this check at all entry points into the app.
    @Override
    protected void onResume() {
        super.onResume();


    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            onButtonClick(view);
        }
    };

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {

            case R.id.btnContinue:
                startNextActivity();
                break;
        }
    }

    private void startNextActivity()
    {
        ActivityController.startNextActivity(this, DashboardActivity.class, true);
        //        if (userBasicInfo == null) {
        //            ActivityController.startNextActivity(this, LoginActivity.class, true);
        //        } else {
        //            ActivityController.startNextActivity(this, DashboardActivity.class, true);
        //        }
    }

    private void callAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "CustomerBalanceEnquiry");
            objMain.put("AccountId", userBasicInfo.accountId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                        binding.tvMyLoyaltyBalance.setText(String.format("%,.2f", Float.parseFloat(response.body().userDetailResult.customerBalanceEnquiryData.currentTokenBalance)));
                        binding.tvCashSavedWithOiyaa.setText(strCurrencySymbol + String.format("%,.2f", Float.parseFloat(response.body().userDetailResult.customerBalanceEnquiryData.totalTokenEarned)));
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, SplashInfoActivity.this);
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(SplashInfoActivity.this))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), SplashInfoActivity.this);
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), SplashInfoActivity.this);
            }
        });
    }

    private Bitmap generateQRCode(String data)
    {
        Bitmap retVal;

        try
        {
            int size = 420;

            BitMatrix bitMatrix = new MultiFormatWriter().encode(data, BarcodeFormat.QR_CODE, size, size, null);

            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();

            int[] pixels = new int[width * height];

            for (int i = 0; i < height; i++)
            {
                int offset = i * width;

                for (int j = 0; j < width; j++)
                {
                    pixels[offset + j] = bitMatrix.get(j, i) ? BLACK : WHITE;
                }
            }

            retVal = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            retVal.setPixels(pixels, 0, size, 0, 0, width, height);
        }
        catch (Exception e)
        {
            return null;
        }


        return retVal;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
