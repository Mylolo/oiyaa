package com.localvalu.user.api

import com.localvalu.base.BaseApi
import com.localvalu.common.model.SearchRequest
import com.localvalu.common.model.SearchResponse
import com.localvalu.registration.model.UserExistRequest
import com.localvalu.user.dto.PassionListRequest
import com.localvalu.user.model.LoginRequest
import com.localvalu.user.model.LoginResponse
import com.localvalu.user.wrapper.PassionListResultWrapper
import com.localvalu.user.wrapper.RegisterExistsCustomerResultWrapper
import com.localvalu.user.wrapper.UserWrapper
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginApi : BaseApi
{
    @POST("Auth/UserLogin")
    suspend fun login(@Body loginRequest: LoginRequest): LoginResponse
}