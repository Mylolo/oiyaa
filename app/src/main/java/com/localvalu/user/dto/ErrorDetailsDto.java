package com.localvalu.user.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ErrorDetailsDto implements Serializable {

    @SerializedName("ErrorCode")
    @Expose
    public int errorCode;

    @SerializedName("ErrorMessage")
    @Expose
    public String errorMessage;
}
