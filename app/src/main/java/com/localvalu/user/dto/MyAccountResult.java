package com.localvalu.user.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MyAccountResult implements Serializable {

    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("UserDashboardDetails")
    @Expose
    public UserBasicInfo userBasicInfo;
}
