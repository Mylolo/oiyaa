package com.localvalu.user.dto.forgetpassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgetPasswordResult
{
    @SerializedName("Message")
    @Expose
    public String message;
}
