package com.localvalu.user.dto.forgetpassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.model.ErrorDetails;

public class ForgetPasswordRequest
{
    @SerializedName("token")
    @Expose
    private String token="/3+YFd5QZdSK9EKsB8+TlA==";

    @SerializedName("email")
    @Expose
    private String email;

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}
