package com.localvalu.user.dto;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterDataDto implements Serializable
{

    @SerializedName("Token")
    @Expose
    public String token;

    @SerializedName("userName")
    @Expose
    public String name;

    @SerializedName("Mobile")
    @Expose
    public String mobile;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("QRCodeInput")
    @Expose
    public String qRCodeInput;

    @SerializedName("ReferralCode")
    @Expose
    public String referralCode;

    @SerializedName("Home_postcode")
    @Expose
    public String homePostCode;

    @SerializedName("Work_postcode")
    @Expose
    public String workPostCode;

    @SerializedName("Dateofbirth")
    @Expose
    public String dob;

    @SerializedName("Agegroup")
    @Expose
    public String ageGroup;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("Address")
    @Expose
    public String address;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("country")
    @Expose
    public String country;

    @SerializedName("Passion_filled")
    @Expose
    public int passionFilled;

    @SerializedName("passionDetails")
    @Expose
    public String passionDetails = "";

    @SerializedName("password")
    @Expose
    public String password;

    @SerializedName("SourceFrom")
    @Expose
    public String sourceFrom;
}
