package com.localvalu.user.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserBasicInfo implements Serializable {

    @SerializedName(value = "user_id", alternate = {"UserId"})
    @Expose
    public String userId;

    @SerializedName(value = "account_id", alternate = {"AccountId"})
    @Expose
    public String accountId;

    @SerializedName(value = "Name", alternate = {"UserName", "userName"})
    @Expose
    public String name;

    @SerializedName("Dateofbirth")
    @Expose
    public String dob;

    @SerializedName(value = "Mobile", alternate = {"MobileNo"})
    @Expose
    public String mobileNo;

    @SerializedName(value = "QRCode", alternate = {"QrCode"})
    @Expose
    public String qRCodeOutput;

    @SerializedName("VoucherCode")
    @Expose
    public String voucherCode;

    @SerializedName("Agegroup")
    @Expose
    public String ageGroup;

    @SerializedName(value = "gender", alternate = {"Gender"})
    @Expose
    public String gender;

    @SerializedName("Address")
    @Expose
    public String address;

    @SerializedName(value = "city", alternate = {"City"})
    @Expose
    public String city;

    @SerializedName(value = "country", alternate = {"Country"})
    @Expose
    public String country;

    @SerializedName(value = "Home_postcode", alternate = {"HomePostcode"})
    @Expose
    public String homePostcode;

    @SerializedName(value = "Work_postcode", alternate = {"WorkPostcode"})
    @Expose
    public String workPostcode;

    @SerializedName(value = "passionDetails", alternate = {"PassionDetails", "Passions"})
    @Expose
    public String passionDetails;

    @SerializedName(value = "email", alternate = {"Email"})
    @Expose
    public String email;

    @SerializedName(value = "profile_pic", alternate = {"ProfilePicture"})
    @Expose
    public String image;

    @SerializedName(value = "createdDate", alternate = {"AccountCreatedDate"})
    @Expose
    public String createdDate;

    @SerializedName("NoofTokens")
    @Expose
    public String noOfTokens;

    public String address() {
        StringBuilder retVal = new StringBuilder();

        if (address != null && !address.isEmpty()) {
            retVal.append(address);
        }

        if (city != null && !city.isEmpty()) {
            retVal.append(", ");
            retVal.append(city);
        }

        if (city != null && !city.isEmpty()) {
            retVal.append(", ");
            retVal.append(city);
        }

        return retVal.toString();
    }
}
