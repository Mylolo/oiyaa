package com.localvalu.user.dto;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QRCodeVerificationResult implements Serializable {

    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("QRCode")
    @Expose
    public String qRCode;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("Token")
    @Expose
    public String token;

}
