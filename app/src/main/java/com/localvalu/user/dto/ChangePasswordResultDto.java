package com.localvalu.user.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChangePasswordResultDto implements Serializable {

    @SerializedName("Message")
    @Expose
    public String message;

}
