package com.localvalu.user.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PassionDetails implements Serializable {

    @SerializedName("PassionId")
    @Expose
    public String passionId;

    @SerializedName("Passion")
    @Expose
    public String passion;

    public boolean checked;

    public int type;

}
