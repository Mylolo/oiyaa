package com.localvalu.user.dto.forgetpassword;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.model.ErrorDetails;

public class ForgetPassword
{
    @SerializedName("forgetPasswordResult")
    @Expose
    public ForgetPasswordResult forgetPasswordResult;

    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetails errorDetails;
}
