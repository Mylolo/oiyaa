package com.localvalu.user.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpdateCustomerResult implements Serializable {

    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("msg")
    @Expose
    public String message;

    @SerializedName("UserDetailsUpdation")
    @Expose
    public UpdateCustomerData updateCustomerData;
}
