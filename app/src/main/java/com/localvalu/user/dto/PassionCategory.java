package com.localvalu.user.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PassionCategory implements Serializable {

    @SerializedName("PassionsCategoryid")
    @Expose
    public String passionCategoryId;

    @SerializedName("PassionsCategory")
    @Expose
    public String passionCategory;

    @SerializedName("PassionsCategoryWiseList")
    @Expose
    public List<PassionDetails> passionDetailsList;

}
