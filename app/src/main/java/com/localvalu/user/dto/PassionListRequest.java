package com.localvalu.user.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PassionListRequest implements Serializable
{
    @SerializedName("Token")
    @Expose
    public String Token="/3+YFd5QZdSK9EKsB8+TlA==";

    public String getToken()
    {
        return Token;
    }

    public void setToken(String token)
    {
        Token = token;
    }
}
