package com.localvalu.user.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RegisterExistsCustomer implements Serializable {

    @SerializedName("emailExists")
    @Expose
    public String emailExists;

    @SerializedName("MobileExists")
    @Expose
    public String mobileExists;

    @SerializedName("referrFlagValid")
    @Expose
    public String referrFlagValid;
}
