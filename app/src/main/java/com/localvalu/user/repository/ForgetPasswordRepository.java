package com.localvalu.user.repository;

import com.localvalu.user.dashboard.request.HomePageRequest;
import com.localvalu.user.dashboard.response.HomePageResponse;
import com.localvalu.user.dto.forgetpassword.ForgetPassword;
import com.localvalu.user.dto.forgetpassword.ForgetPasswordRequest;
import com.localvalu.user.dto.forgetpassword.ForgetPasswordResponse;
import com.requestHandler.ApiClient;
import com.requestHandler.auth.ForgetPasswordApi;
import com.requestHandler.dashboard.DashboardApi;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ForgetPasswordRepository
{
    ForgetPasswordApi forgetPasswordApi;

    public ForgetPasswordRepository()
    {
        forgetPasswordApi = ApiClient.getApiClientNew().create(ForgetPasswordApi.class);
    }

    public Single<ForgetPasswordResponse> forgetPassword(ForgetPasswordRequest forgetPasswordRequest)
    {
        return forgetPasswordApi.forgetPassword(forgetPasswordRequest)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

}
