package com.localvalu.user.repository

import com.localvalu.base.BaseRepository
import com.localvalu.db.AppDatabase
import com.localvalu.db.networkBoundResource
import com.localvalu.user.api.LoginApi
import com.localvalu.user.model.LoginRequest
import javax.inject.Inject


class UserRepository @Inject constructor(private val api: LoginApi,private val db:AppDatabase):BaseRepository(api)
{
    private val userDao = db.userDao()

    fun getUser(loginRequest: LoginRequest) = networkBoundResource(
        query = {
            userDao.getLoggedInUser()
        },
        fetch = {
            api.login(loginRequest)
        },
        saveFetchResult = {
            userDao.deleteAllUsers()
            userDao.save(it.loginResult.user)
        }
    )
}