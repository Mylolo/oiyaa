package com.localvalu.user.repository;

import com.localvalu.user.dashboard.request.HomePageRequest;
import com.localvalu.user.dashboard.response.HomePageResponse;
import com.localvalu.user.manageaddress.request.AddressListRequest;
import com.localvalu.user.manageaddress.response.OrderNowResponse;
import com.requestHandler.ApiClient;
import com.requestHandler.dashboard.DashboardApi;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class DashboardRepository
{

    DashboardApi dashboardApi;

    public DashboardRepository()
    {
        dashboardApi = ApiClient.getApiClientNew().create(DashboardApi.class);
    }

    public Observable<HomePageResponse> getHomePageContent(HomePageRequest homePageRequest)
    {
        return dashboardApi.homePageContent(homePageRequest)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

}
