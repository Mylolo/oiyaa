package com.localvalu.user.repository;

import com.localvalu.user.manageaddress.request.AddressListRequest;
import com.localvalu.user.manageaddress.request.NewAddressRequest;
import com.localvalu.user.manageaddress.request.UpdateAddressRequest;
import com.localvalu.user.manageaddress.response.DeliveryAddressResponse;
import com.localvalu.user.manageaddress.response.OrderNowResponse;
import com.requestHandler.ApiClient;
import com.requestHandler.manageaddress.ManageAddressApi;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class UserAddressRepository
{

    ManageAddressApi manageAddressApi;

    public UserAddressRepository()
    {
        manageAddressApi = ApiClient.getApiClientNew().create(ManageAddressApi.class);
    }

    public Observable<DeliveryAddressResponse> saveAddressInServer(NewAddressRequest newAddressRequest)
    {
        return manageAddressApi.addNewAddress(newAddressRequest)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<OrderNowResponse> getAddressList(AddressListRequest addressListRequest)
    {
        return manageAddressApi.getDeliveryAddressListLocal(addressListRequest)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<DeliveryAddressResponse> setDefaultAddress(UpdateAddressRequest updateAddressRequest)
    {
        return manageAddressApi.updateAddress(updateAddressRequest)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<DeliveryAddressResponse> removeAddress(UpdateAddressRequest updateAddressRequest)
    {
        return manageAddressApi.updateAddress(updateAddressRequest)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

}
