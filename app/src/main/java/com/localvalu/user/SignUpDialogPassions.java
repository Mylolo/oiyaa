package com.localvalu.user;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;

import com.localvalu.R;
import com.localvalu.databinding.ActivitySignupDialogPassionsBinding;
import com.localvalu.user.dto.RegisterDataDto;
import com.utility.ActivityController;
import com.utility.AppUtils;

public class SignUpDialogPassions extends AppCompatActivity
{
    private static final String TAG = "SignUpAdvanceActivity";

    private RegisterDataDto registerDataDto;
    private ActivitySignupDialogPassionsBinding binding;
    private int colorWhite;
    private int colorBlack;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeLogin);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_signup_dialog_passions);
        colorBlack = ContextCompat.getColor(getApplicationContext(),R.color.colorBlack);
        colorWhite = ContextCompat.getColor(getApplicationContext(),R.color.colorWhite);
        readFromBundle();
        setProperties();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private void readFromBundle()
    {
        if (getIntent().getExtras() != null)
        {
            registerDataDto = (RegisterDataDto) getIntent().getExtras().getSerializable("registerDataDto");
            System.out.println("registerDataDto " + registerDataDto.toString());
        }
    }

    private void setProperties()
    {
        setPassionsText();
        Drawable drawableBack = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_app_arrow, null);
        drawableBack.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP);
        binding.imgBack.setImageDrawable(drawableBack);

        binding.imgBack.setOnClickListener(_OnClickListener);
        binding.btnMoreDialogPassions.setOnClickListener(_OnClickListener);
        binding.btnContinueDialogPassions.setOnClickListener(_OnClickListener);
    }

    private void setPassionsText()
    {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        SpannableString str1= new SpannableString(getString(R.string.lbl_sign_up_dialog_passions_text));
        str1.setSpan(new ForegroundColorSpan(colorBlack), 0, str1.length(), 0);
        builder.append(str1).append(" ");

        SpannableString str2= new SpannableString(getString(R.string.lbl_sign_up_dialog_passions_text_a_white));
        str2.setSpan(new ForegroundColorSpan(colorWhite), 0, str2.length(), 0);
        builder.append(str2).append(" ");

        SpannableString str3= new SpannableString(getString(R.string.lbl_sign_up_dialog_passions_text_a_black));
        str3.setSpan(new ForegroundColorSpan(colorBlack), 0, str3.length(), 0);
        builder.append(str3).append(" ");

        binding.tvPassionsTextOne.setText(builder, TextView.BufferType.SPANNABLE);

        SpannableStringBuilder builder2 = new SpannableStringBuilder();

        SpannableString str4= new SpannableString(getString(R.string.lbl_sign_up_dialog_passions_text_b_black));
        str4.setSpan(new ForegroundColorSpan(colorBlack), 0, str4.length(), 0);
        builder2.append(str4).append(" ");

        SpannableString str5= new SpannableString(getString(R.string.lbl_sign_up_dialog_passions_text_b_white));
        str5.setSpan(new ForegroundColorSpan(colorWhite), 0, str5.length(), 0);
        builder2.append(str5).append(" ");

        SpannableString str6= new SpannableString(getString(R.string.lbl_sign_up_dialog_passions_text_b_black_two));
        str6.setSpan(new ForegroundColorSpan(colorBlack), 0, str6.length(), 0);
        builder2.append(str6).append(" ");

        binding.tvPassionsTextTwo.setText(builder2, TextView.BufferType.SPANNABLE);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            onButtonClick(view);
        }
    };

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                 finish();
                 break;
            case R.id.btnContinueDialogPassions:
                 registerDataDto.passionFilled = AppUtils.PASSION_NOT_FILLED;
                 Bundle bundleSubmit = new Bundle();
                 bundleSubmit.putSerializable("registerDataDto", registerDataDto);

                 ActivityController.startNextActivity(this, SignUpFinalActivity.class, bundleSubmit, false);
                 break;
            case R.id.btnMoreDialogPassions:
                 Bundle bundleMore = new Bundle();
                 bundleMore.putSerializable("registerDataDto", registerDataDto);
                 ActivityController.startNextActivity(this, SignUpAdvanceActivity.class, bundleMore, false);
                 break;
        }
    }

}
