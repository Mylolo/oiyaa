package com.localvalu.user.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.localvalu.user.model.User
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao
{
    @Insert(onConflict = REPLACE)
    fun save(user: User)

    @Query("SELECT *FROM User")
    fun getLoggedInUser(): Flow<User>

    @Query("DELETE FROM user")
    fun deleteAllUsers()
}