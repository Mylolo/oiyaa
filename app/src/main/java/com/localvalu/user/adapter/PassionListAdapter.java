package com.localvalu.user.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.R;
import com.localvalu.eats.detail.adapter.FoodItemAdapter;
import com.localvalu.user.dto.PassionDetails;
import com.utility.StickyAdapter;
import com.utility.stickyheader.Section;
import com.utility.stickyheader.StickHeaderItemDecoration;

import java.util.List;

public class PassionListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private List<PassionDetails> items;
    private Context context;
    public final static int HEADER_VIEW_HOLDER=1;
    public final static int ITEM_VIEW_HOLDER=2;


    public PassionListAdapter(List<PassionDetails> items)
    {
        this.items = items;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.context = recyclerView.getContext();
    }

    @Override
    public int getItemViewType(int position)
    {
        if(items.get(position).type==HEADER_VIEW_HOLDER) return HEADER_VIEW_HOLDER;
        else return ITEM_VIEW_HOLDER;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        switch (viewType)
        {
            case HEADER_VIEW_HOLDER:
                 View headerView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_signup_passion_header, parent, false);
                 return new HeaderViewHolder(headerView);
            case ITEM_VIEW_HOLDER:
                 View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_signup_passion_item, parent, false);
                 return new ItemViewHolder(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
    {
        if(holder!=null)
        {
            if(holder instanceof HeaderViewHolder)
            {
                HeaderViewHolder headerViewHolder = (HeaderViewHolder) holder;
                headerViewHolder.tvHeader.setText(items.get(position).passion);
            }
            else  if(holder instanceof ItemViewHolder)
            {
                ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                itemViewHolder.cbPassion.setText(items.get(position).passion);
                itemViewHolder.cbPassion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b)
                    {
                        compoundButton.postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                items.get(position).checked = b;
                            }
                        },100);
                    }
                });
                itemViewHolder.cbPassion.setChecked(items.get(position).checked);
            }
        }
    }

    @Override
    public int getItemCount()
    {
        return items.size();
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvHeader = itemView.findViewById(R.id.tvHeader);

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder
    {
        CheckBox cbPassion = itemView.findViewById(R.id.cbPassion);

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }



}
