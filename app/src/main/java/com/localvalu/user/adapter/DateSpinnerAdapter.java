package com.localvalu.user.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.localvalu.R;

public class DateSpinnerAdapter extends ArrayAdapter<String>
{
    LayoutInflater inflater;

    String[] items;


    public DateSpinnerAdapter(@NonNull Context context, int resource, @NonNull String[] objects)
    {
        super(context, resource, objects);
        inflater=LayoutInflater.from(context);
        items = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        View rowview = inflater.inflate(R.layout.view_item_spinner,null,true);

        AppCompatTextView tvData = rowview.findViewById(R.id.tv_data);

        tvData.setText(items[position]);

        return rowview;
    }

    @NonNull
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        View rowview = inflater.inflate(R.layout.view_item_spinner_dropdown,null,true);

        AppCompatTextView tvData = rowview.findViewById(R.id.tv_data);

        tvData.setText(items[position]);

        return rowview;
    }


}
