package com.localvalu.user;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.localvalu.R;
import com.localvalu.databinding.ActivitySignUpBinding;
import com.localvalu.user.adapter.DateSpinnerAdapter;
import com.localvalu.user.dto.RegisterDataDto;
import com.localvalu.user.wrapper.RegisterExistsCustomerResultWrapper;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.CommonUtility;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.customView.RadioGridGroup;
import com.utility.validators.RangeValidator;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity
{
    private static final String TAG = SignUpActivity.class.getSimpleName();
    public ProgressDialog mProgressDialog;
    public ApiInterface apiInterface;
    private String full_name, mobile, email, qrCodeResult, selectedGender, selectedAgeGroup;
    private String referralCode;
    private RegisterDataDto registerDataDto;
    private ActivitySignUpBinding binding;
    private final int CUSTOMIZED_REQUEST_CODE = 0x0000ffff;
    private int colorBlack, colorGray, colorWhite;
    private String strDOB;
    private Date dobDate;
    private Calendar startTime = Calendar.getInstance();
    private Calendar endTime = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeLogin);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        colorBlack = ContextCompat.getColor(getApplicationContext(), R.color.colorBlack);
        colorGray = ContextCompat.getColor(getApplicationContext(), R.color.colorGray);
        colorWhite = ContextCompat.getColor(getApplicationContext(), R.color.colorWhite);
        startTime.setTimeZone(TimeZone.getTimeZone("UK"));
        startTime.set(1922,0, 1);
        endTime.set(2002, 11, 31);
        setProperties();
        readFromBundle();
        loadSpinners();
        bouldObjectForHandlingAPI();
    }

    private void readFromBundle()
    {
        full_name = getIntent().getStringExtra(AppUtils.BUNDLE_FULL_NAME);
        mobile = getIntent().getStringExtra(AppUtils.BUNDLE_MOBILE);
        email = getIntent().getStringExtra(AppUtils.BUNDLE_EMAIL);
        qrCodeResult = getIntent().getStringExtra(AppUtils.BUNDLE_QR_CODE_RESULT);
        full_name = capitalizeWord(full_name);
        setTextAndFocus(binding.etFullName,full_name);
        setTextAndFocus(binding.etMobileNumber,mobile);
        setTextAndFocus(binding.etEmail,email);
        setTextAndFocus(binding.etReferralCode,qrCodeResult);
    }

    private void setTextAndFocus(TextInputEditText textInputEditText,String text)
    {
        if(text!=null)
        {
            if(!text.equals(""))
            {
                textInputEditText.setText(text);
                textInputEditText.requestFocus();
            }
        }
    }

    public static String capitalizeWord(String str)
    {
        String capitalizeWord = "";
        if(str!=null)
        {
            if (!str.equals(""))
            {
                String words[] = str.split("\\s");

                for (String w : words)
                {
                    String first = w.substring(0, 1);
                    String afterfirst = w.substring(1);
                    capitalizeWord += first.toUpperCase() + afterfirst + " ";
                }
            }
        }
        return capitalizeWord.trim();
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            onButtonClick(view);
        }
    };

    private void setProperties()
    {
        setFilters(binding.etFullName, "[a-zA-Z ]+");
        setFilters(binding.etHomePostCode, "[0-9a-zA-Z ]+");
        setFilters(binding.etWorkPostCode, "[0-9a-zA-Z ]+");

        dobDate = new Date();

        Drawable drawableBack = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_app_arrow, null);
        drawableBack.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP);
        binding.imgBack.setImageDrawable(drawableBack);

        binding.imgBack.setOnClickListener(_OnClickListener);
        binding.btnSignUpSubmit.setOnClickListener(_OnClickListener);
        binding.etDOB.setOnClickListener(_OnClickListener);
        binding.tvGenderError.setVisibility(View.GONE);
        binding.cnlMain.setOnClickListener(_OnClickListener);
        binding.tvTextSelectDOB.setVisibility(View.GONE);

        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.menu_scanpay, null);
        drawable.setColorFilter(colorGray, PorterDuff.Mode.SRC_ATOP);
        binding.tilReferralCode.setEndIconDrawable(drawable);
        binding.tilReferralCode.setEndIconOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startScan();
            }
        });
        binding.radGroupAge.setOnCheckedChangeListener(new RadioGridGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioButton radioButton, boolean checked)
            {
                if (checked)
                {
                    binding.tvAgeError.setVisibility(View.GONE);
                    switch (radioButton.getId())
                    {
                        case R.id.radioButton16To24:
                            selectedAgeGroup = binding.radioButton16To24.getText().toString();
                            break;
                        case R.id.radioButton25To34:
                            selectedAgeGroup = binding.radioButton25To34.getText().toString();
                            break;
                        case R.id.radioButton35To44:
                            selectedGender = binding.radioButton35To44.getText().toString();
                            break;
                        case R.id.radioButton45To54:
                            selectedGender = binding.radioButton45To54.getText().toString();
                            break;
                        case R.id.radioButton55To64:
                            selectedGender = binding.radioButton55To64.getText().toString();
                            break;
                        case R.id.radioButton65Plus:
                            selectedGender = binding.radioButton65Plus.getText().toString();
                            break;
                    }
                }
            }
        });
        binding.radGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int radioButtonId)
            {
                binding.tvGenderError.setVisibility(View.GONE);
                switch (radioButtonId)
                {
                    case R.id.radioButtonMale:
                        selectedGender = binding.radioButtonMale.getText().toString();
                        break;
                    case R.id.radioButtonFemale:
                        selectedGender = binding.radioButtonFemale.getText().toString();
                        break;
                    case R.id.radioButtonOther:
                        selectedGender = "Other";
                        break;
                }
            }
        });
        /*binding.radGroupGender.setOnCheckedChangeListener(new RadioGridGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioButton radioButton, boolean checked)
            {
                binding.tvGenderError.setVisibility(View.GONE);

                switch (radioButton.getId())
                {
                    case R.id.radioButtonMale:
                        selectedGender = binding.radioButtonMale.getText().toString();
                        break;
                    case R.id.radioButtonFemale:
                        selectedGender = binding.radioButtonFemale.getText().toString();
                        break;
                    case R.id.radioButtonOther:
                        selectedGender = "Other";
                        break;
                }
            }
        });*/
        registerDataDto = new RegisterDataDto();
        binding.etFullName.requestFocus();
    }

    private void showDOBPicker()
    {
        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        builder.setCalendarConstraints(dobDateLimitRange().build());
        MaterialDatePicker picker = builder.build();

        picker.show(getSupportFragmentManager(), picker.toString());
        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener()
        {
            @Override
            public void onPositiveButtonClick(Object selection)
            {
                long milliseconds = (long) selection;
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTimeInMillis(milliseconds);
                dobDate = selectedDate.getTime();
                strDOB = convertToStringInServerFormat(selectedDate.getTime());
                binding.etDOB.setText(convertDateToStringInUKFormat(selectedDate.getTime()));
                binding.tvTextSelectDOB.setVisibility(View.GONE);
            }
        });
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btnSignUpSubmit:
                if (isValid())
                {
                    binding.tvGenderError.setVisibility(View.GONE);
                    callUserExists();
                }
                break;
            case R.id.imgBack:
                finish();
                break;
            case R.id.cnlMain:
                CommonUtility.hideKeyboard(this);
                break;
            case R.id.etDOB:
                showDOBPicker();
                break;
        }
    }

    private boolean isValid()
    {
        if (binding.etFullName.getText().toString().trim().isEmpty())
        {
            binding.etFullName.setError(getString(R.string.msg_enter_your_full_name));
            binding.etFullName.requestFocus();
            return false;
        }
        else if (binding.etMobileNumber.getText().toString().trim().isEmpty())
        {
            binding.etMobileNumber.setError(getString(R.string.msg_enter_your_mobile_no));
            binding.etMobileNumber.requestFocus();
            return false;
        }
        else if (!ValidationUtils.isValidMobileNo(binding.etMobileNumber.getText().toString().trim()))
        {
            binding.etMobileNumber.setError(getString(R.string.msg_enter_valid_mobile_no));
            binding.etMobileNumber.requestFocus();
            return false;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(binding.etEmail.getText().toString().trim()).matches())
        {
            binding.etEmail.setError(getString(R.string.msg_enter_valid_email_id));
            binding.etEmail.requestFocus();
            return false;
        }

        else if (!binding.etReferralCode.getText().toString().trim().equals("")&&
                !binding.etReferralCode.getText().toString().trim().contains("IDL"))
        {
            binding.etHomePostCode.setError(getString(R.string.msg_valid_referral_code));
            binding.etHomePostCode.requestFocus();
            return false;
        }
        else if (binding.etHomePostCode.getText().toString().trim().equals(""))
        {
            binding.etHomePostCode.setError(getString(R.string.msg_enter_your_home_post_code));
            binding.etHomePostCode.requestFocus();
            return false;
        }
        else if (!ValidationUtils.isValidPostCode(binding.etHomePostCode.getText().toString().trim().toUpperCase()))
        {
            binding.etHomePostCode.setError(getString(R.string.msg_enter_valid_post_code));
            binding.etHomePostCode.requestFocus();

            return false;
        }
        /*else if (spinnerDay.getSelectedItemPosition() == 0
                || binding.spinnerMonths.getSelectedItemPosition() == 0 || spinnerYear.getSelectedItemPosition() == 0)

        {
            tvDOBError.setText(getString(R.string.msg_select_dob));
            tvDOBError.setVisibility(View.VISIBLE);
            return false;
        }
        /*else if (radGroupAge.getCheckedRadioButtonId() == -1)
        {
            tvAgeError.setText(getString(R.string.msg_select_age_group));
            tvAgeError.setVisibility(View.VISIBLE);
            return false;
        }*/
        else if (binding.radGroupGender.getCheckedRadioButtonId() == -1)
        {
            binding.tvGenderError.setText(getString(R.string.msg_select_gender));
            binding.tvGenderError.setVisibility(View.VISIBLE);
            return false;
        }
        /* else if (binding.etAddress.getText().toString().equals(""))
        {
            binding.etAddress.setError(getString(R.string.msg_enter_address));
            binding.etAddress.requestFocus();
            return false;
        }
        else if (binding.etCity.getText().toString().equals(""))
        {
            binding.etCity.setError(getString(R.string.msg_enter_city));
            binding.etCity.requestFocus();
            return false;
        }*/
        if (!binding.etWorkPostCode.getText().toString().trim().equals(""))
        {
            if (!ValidationUtils.isValidPostCode(binding.etWorkPostCode.getText().toString().trim().toUpperCase()))
            {
                binding.etWorkPostCode.setError(getString(R.string.msg_enter_valid_post_code));
                binding.etWorkPostCode.requestFocus();
                return false;
            }
        }
        if (binding.etDOB.getText().toString().trim().equals(""))
        {
            binding.etDOB.setError(getString(R.string.msg_select_dob));
            binding.etDOB.requestFocus();
            binding.tvTextSelectDOB.setVisibility(View.VISIBLE);
            return false;
        }
        return true;
    }

    private void setFilters(TextInputEditText textInputEditText, String regEx)
    {
        textInputEditText.setFilters(new InputFilter[]{
                new InputFilter()
                {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd)
                    {
                        // TODO Auto-generated method stub
                        if (cs.equals(""))
                        { // for backspace
                            return cs;
                        }
                        if (cs.toString().matches(regEx))
                        {
                            return cs;
                        }
                        return "";
                    }
                }
        });
    }

    private void bouldObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    private void callUserExists()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("email", binding.etEmail.getText().toString().trim());
            objMain.put("Mobile", binding.etMobileNumber.getText().toString().trim());
            referralCode = binding.etReferralCode.getText().toString().trim();
            if (referralCode.equals("IDL"))
            {
                referralCode = "";
            }
            objMain.put("ReferralCode", referralCode);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<RegisterExistsCustomerResultWrapper> dtoCall = apiInterface.userSignUpValidate(body);
        dtoCall.enqueue(new Callback<RegisterExistsCustomerResultWrapper>()
        {
            @Override
            public void onResponse(Call<RegisterExistsCustomerResultWrapper> call, Response<RegisterExistsCustomerResultWrapper> response)
            {
                mProgressDialog.dismiss();
                try
                {
                    if (response.body().registerCustomerResult.errorDetails.errorCode == 0)
                    {
                        String emailExists = response.body().registerCustomerResult.registerExistsCustomer.emailExists;
                        String mobileExists = response.body().registerCustomerResult.registerExistsCustomer.mobileExists;
                        String referrFlagValid = response.body().registerCustomerResult.registerExistsCustomer.referrFlagValid;

                        if (emailExists.equals("false") && mobileExists.equals("false"))
                        {
                            //Check for referal code
                            if (binding.etReferralCode.getText().length() > 0)
                            {
                                if (referrFlagValid.equals("false"))
                                {
                                    DialogUtility.showMessageWithOk("Invalid referral code", SignUpActivity.this);
                                    return;
                                }

                            }
                            selectedAgeGroup = getAgeGroup(DateUtility.ageCalculator(dobDate));
                            registerDataDto.referralCode = "";
                            registerDataDto.token = "/3+YFd5QZdSK9EKsB8+TlA==";
                            registerDataDto.name = binding.etFullName.getText().toString().trim();
                            registerDataDto.mobile = binding.etMobileNumber.getText().toString().trim();
                            registerDataDto.email = binding.etEmail.getText().toString().trim();
                            registerDataDto.qRCodeInput = referralCode;
                            registerDataDto.homePostCode = binding.etHomePostCode.getText().toString().trim();
                            registerDataDto.workPostCode = binding.etWorkPostCode.getText().toString().trim();
                            registerDataDto.address = binding.etAddress.getText().toString().trim();
                            registerDataDto.city = binding.etCity.getText().toString().trim();
                            registerDataDto.ageGroup = selectedAgeGroup;
                            registerDataDto.gender = selectedGender;
                            registerDataDto.dob = strDOB;
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("registerDataDto", registerDataDto);

                            ActivityController.startNextActivity(SignUpActivity.this, SignUpDialogPassions.class, bundle, false);

                        }
                        else if (emailExists.equals("true"))
                        {
                            DialogUtility.showMessageWithOk("Email Already Exists", SignUpActivity.this);
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk("Mobile Number Already Exists", SignUpActivity.this);
                        }
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().registerCustomerResult.errorDetails.errorMessage, SignUpActivity.this);
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<RegisterExistsCustomerResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(SignUpActivity.this))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), SignUpActivity.this);
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), SignUpActivity.this);
            }
        });
    }

    private void startScan()
    {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setOrientationLocked(false);
        integrator.setCaptureActivity(SmallCaptureActivity.class);
        integrator.setRequestCode(CUSTOMIZED_REQUEST_CODE);
        integrator.initiateScan();
    }

    public void loadSpinners()
    {
        Log.d(TAG, "loadSpinners: ");
        String[] monthArrayOne = getResources().getStringArray(R.array.month_arrays);
        String[] monthArrayTwo = new String[monthArrayOne.length + 1];
        monthArrayTwo[0] = getString(R.string.lbl_mm);

        for (int i = 1; i <= monthArrayOne.length; i++)
        {
            monthArrayTwo[i] = monthArrayOne[i - 1];
        }

        DateSpinnerAdapter monthAdapter = new DateSpinnerAdapter(this,
                R.layout.view_item_spinner, monthArrayTwo);
        binding.spinnerMonths.setAdapter(monthAdapter);
        binding.spinnerMonths.setOnItemSelectedListener(_OnItemSelectedListener);

        String[] days = new String[32];
        days[0] = getString(R.string.lbl_dd);
        for (int i = 1; i <= 31; i++)
        {
            days[i] = Integer.toString(i);
        }

        DateSpinnerAdapter daysAdapter = new DateSpinnerAdapter(this, R.layout.view_item_spinner, days);
        binding.spinnerDays.setAdapter(daysAdapter);
        binding.spinnerDays.setOnItemSelectedListener(_OnItemSelectedListener);

        String[] years = new String[66];
        years[0] = getString(R.string.lbl_yyy);
        int i = 1;
        for (int j = 2003; j > 1939; j--)
        {
            years[i] = Integer.toString(j);
            i++;
        }

        DateSpinnerAdapter yearAdapter = new DateSpinnerAdapter(this, R.layout.view_item_spinner, years);
        binding.spinnerYear.setAdapter(yearAdapter);

        binding.spinnerDays.setSelection(-1);
        binding.spinnerMonths.setSelection(-1);
        binding.spinnerYear.setSelection(-1);

        binding.spinnerDays.setPrompt(getString(R.string.lbl_dd));
        binding.spinnerMonths.setPrompt(getString(R.string.lbl_mm));
        binding.spinnerYear.setPrompt(getString(R.string.lbl_yyy));

        binding.spinnerYear.setOnItemSelectedListener(_OnItemSelectedListener);

    }

    /*
      Limit selectable Date range
    */
    private CalendarConstraints.Builder dobDateLimitRange()
    {
        CalendarConstraints.Builder constraintsBuilderRange = new CalendarConstraints.Builder();

        constraintsBuilderRange.setStart(startTime.getTimeInMillis());
        constraintsBuilderRange.setEnd(endTime.getTimeInMillis());
        constraintsBuilderRange.setValidator(new RangeValidator(startTime.getTimeInMillis(), endTime.getTimeInMillis()));

        return constraintsBuilderRange;
    }

    AdapterView.OnItemSelectedListener _OnItemSelectedListener = new AdapterView.OnItemSelectedListener()
    {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
        {
            if (position != 0)
            {
                if (binding.spinnerDays.getSelectedItemPosition() != 0 && binding.spinnerMonths.getSelectedItemPosition() != 0
                        && binding.spinnerYear.getSelectedItemPosition() != 0)
                {
                    Calendar selectedDate = Calendar.getInstance();
                    SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
                    int day = Integer.parseInt(binding.spinnerDays.getSelectedItem().toString());
                    int MMM = binding.spinnerMonths.getSelectedItemPosition();
                    int year = Integer.parseInt(binding.spinnerYear.getSelectedItem().toString());
                    selectedDate.set(year, MMM, day);
                    binding.tvDOBError.setVisibility(View.GONE);
                    registerDataDto.dob = year + "-" + pad(MMM) + "-" + pad(day);
                    //Log.d(TAG, "onItemSelected: dob-" + registerDataDto.dob);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent)
        {

        }
    };

    public static String pad(int data)
    {
        return data >= 10 ? String.valueOf(data) : ("0" + data);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode != CUSTOMIZED_REQUEST_CODE && requestCode != IntentIntegrator.REQUEST_CODE)
        {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }
        switch (requestCode)
        {
            case CUSTOMIZED_REQUEST_CODE:
            {
                //Toast.makeText(this, "REQUEST_CODE = " + requestCode, Toast.LENGTH_LONG).show();
                break;
            }
            default:
                break;
        }

        IntentResult result = IntentIntegrator.parseActivityResult(resultCode, data);

        if (result.getContents() == null)
        {
            Log.d("MainActivity", "Cancelled scan");
            Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
        }
        else
        {
            Log.d("MainActivity", "Scanned" + result.getContents());
            binding.etReferralCode.setText(result.getContents());
        }
    }

    private String getAgeGroup(int age)
    {
        if (age >= 16 && age <= 24) return getString(R.string.lbl_16_to_24);
        else if (age >= 25 && age <= 34) return getString(R.string.lbl_25_to_34);
        else if (age >= 35 && age <= 44) return getString(R.string.lbl_35_to_44);
        else if (age >= 44 && age <= 54) return getString(R.string.lbl_45_to_54);
        else if (age >= 55 && age <= 64) return getString(R.string.lbl_55_to_64);
        else if (age >= 65) return getString(R.string.lbl_65_plus);
        return "";
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev)
    {
        View v = getCurrentFocus();

        if (v != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit."))
        {
            int[] sourceCoordinates = new int[2];
            v.getLocationOnScreen(sourceCoordinates);
            float x = ev.getRawX() + v.getLeft() - sourceCoordinates[0];
            float y = ev.getRawY() + v.getTop() - sourceCoordinates[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
            {
                hideKeyboard(this);
            }

        }
        return super.dispatchTouchEvent(ev);
    }

    private void hideKeyboard(Activity activity)
    {
        if (activity != null && activity.getWindow() != null)
        {
            activity.getWindow().getDecorView();
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
            {
                imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
            }
        }
    }

    private String convertDateToStringInUKFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    private String convertDateStringInUKFormat(String strDate)
    {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date date = inputFormat.parse(strDate);
            return convertDateToStringInUKFormat(date);
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private Date getDateFromString(String strDate)
    {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date date = inputFormat.parse(strDate);
            return date;
        }
        catch (Exception ex)
        {
            return new Date();
        }
    }

    private String convertToStringInServerFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

}
