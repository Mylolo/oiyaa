package com.localvalu.user.manageaddress.ui;

import com.localvalu.user.manageaddress.response.DeliveryAddress;
import com.localvalu.user.manageaddress.response.DeliveryAddressDetails;

public interface AddressIndexSelection
{
    void onItemClicked(int type, DeliveryAddressDetails deliveryAddressDetails);
}
