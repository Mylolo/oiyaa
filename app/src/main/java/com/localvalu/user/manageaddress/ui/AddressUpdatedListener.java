package com.localvalu.user.manageaddress.ui;

import com.localvalu.user.manageaddress.request.NewAddressRequest;
import com.localvalu.user.manageaddress.response.DeliveryAddressDetails;

public interface AddressUpdatedListener
{
    void onAddressSelected(DeliveryAddressDetails deliveryAddressDetails);
    void onAddressAdded(DeliveryAddressDetails deliveryAddressDetails);
    void onNoAddressFound();
}
