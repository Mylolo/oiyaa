package com.localvalu.user.manageaddress.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderNowResults
{
    @SerializedName("ErrorDetails")
    @Expose
    private ErrorDetails errorDetails;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("msg")
    @Expose
    private String message;

    @SerializedName("OrderNowResult")
    @Expose
    private OrderNow orderNow;

    public ErrorDetails getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public OrderNow getOrderNow()
    {
        return orderNow;
    }

    public void setOrderNow(OrderNow orderNow)
    {
        this.orderNow = orderNow;
    }
}
