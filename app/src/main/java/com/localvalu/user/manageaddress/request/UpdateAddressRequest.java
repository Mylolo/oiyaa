package com.localvalu.user.manageaddress.request;

public class UpdateAddressRequest
{
    private String Token;
    private String AccountId;
    private String RequestFor;
    private String OA_Id;

    public String getToken()
    {
        return Token;
    }

    public void setToken(String token)
    {
        Token = token;
    }

    public String getAccountId()
    {
        return AccountId;
    }

    public void setAccountId(String accountId)
    {
        AccountId = accountId;
    }

    public String getRequestFor()
    {
        return RequestFor;
    }

    public void setRequestFor(String requestFor)
    {
        RequestFor = requestFor;
    }

    public String getOA_Id()
    {
        return OA_Id;
    }

    public void setOA_Id(String OA_Id)
    {
        this.OA_Id = OA_Id;
    }

}
