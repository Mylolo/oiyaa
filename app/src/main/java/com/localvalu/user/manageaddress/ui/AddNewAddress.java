package com.localvalu.user.manageaddress.ui;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.localvalu.R;
import com.localvalu.databinding.FragmentAddNewAddressBinding;
import com.localvalu.user.ValidationUtils;
import com.localvalu.user.manageaddress.request.NewAddressRequest;
import com.localvalu.user.manageaddress.response.DeliveryAddress;
import com.localvalu.user.manageaddress.response.DeliveryAddressDetails;
import com.localvalu.user.manageaddress.response.DeliveryAddressResponse;
import com.localvalu.user.manageaddress.response.DeliveryAddressResults;
import com.localvalu.user.repository.UserAddressRepository;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.NavigationType;
import com.utility.AppUtils;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static com.utility.NavigationType.EATS;
import static com.utility.NavigationType.LOCAL;
import static com.utility.NavigationType.MALL;


public class AddNewAddress extends DialogFragment
{

    private FragmentAddNewAddressBinding binding;
    private UserAddressRepository userAddressRepository;
    private String strAccountId,strUserId;
    private ProgressDialog mProgressDialog;
    private AddressUpdatedListener newAddressAddedListener;
    private int navigationType;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        newAddressAddedListener = (AddressUpdatedListener) getTargetFragment();
        userAddressRepository = new UserAddressRepository();
        strAccountId =bundle.getString(AppUtils.BUNDLE_ACCOUNT_ID);
        strUserId = bundle.getString(AppUtils.BUNDLE_USER_ID);
        navigationType = bundle.getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
        initProgressDialog();
        switch (navigationType)
        {
            case EATS:
                 setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme);
                 break;
            case LOCAL:
                 setStyle(DialogFragment.STYLE_NORMAL, R.style.AppThemeLocal);
                 break;
            case MALL:
                 setStyle(DialogFragment.STYLE_NORMAL, R.style.AppThemeMall);
                 break;
        }
    }

    public void initProgressDialog()
    {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    public void setProperties()
    {
        binding.etReceiverName.addTextChangedListener(new TextChangeListener(binding.etReceiverName,binding.tlReceiverName));
        binding.etCompany.addTextChangedListener(new TextChangeListener(binding.etCompany,binding.tlCompany));
        binding.etAddressOne.addTextChangedListener(new TextChangeListener(binding.etAddressOne,binding.tlAddressOne));
        binding.etAddressOne.addTextChangedListener(new TextChangeListener(binding.etAddressOne,binding.tlAddressTwo));
        binding.etCity.addTextChangedListener(new TextChangeListener(binding.etCity,binding.tlCity));
        binding.etState.addTextChangedListener(new TextChangeListener(binding.etState,binding.tlState));
        binding.etCounty.addTextChangedListener(new TextChangeListener(binding.etCounty,binding.tlCounty));
        binding.etCountry.addTextChangedListener(new TextChangeListener(binding.etCountry,binding.tlCountry));
        binding.etLandmark.addTextChangedListener(new TextChangeListener(binding.etLandmark,binding.tlLandmark));
        binding.etPostcode.addTextChangedListener(new TextChangeListener(binding.etPostcode,binding.tlPostalCode));
        binding.etPhoneNumber.addTextChangedListener(new TextChangeListener(binding.etPhoneNumber,binding.tlPhoneNumber));

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = FragmentAddNewAddressBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        binding.toolbarDialog.toolbarDetails.setBackgroundColor(AppUtils.getThemeAccentColor(view.getContext()));
        binding.toolbarDialog.textViewTitle.setText(getString(R.string.lbl_add_new_address));
        binding.toolbarDialog.ivAction.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                dismiss();
            }
        });
        binding.btnSubmit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                saveAddressInsServer();
            }
        });
        setProperties();
    }

    public void saveAddressInsServer()
    {
        if(areAllInputsValid())
        {
            mProgressDialog.show();
            userAddressRepository.saveAddressInServer(getNewAddressRequest())

                    .subscribe(new Observer<DeliveryAddressResponse>()
            {
                @Override
                public void onSubscribe(Disposable d)
                {

                }

                @Override
                public void onNext(DeliveryAddressResponse value)
                {
                    if(mProgressDialog!=null)mProgressDialog.dismiss();
                    if(value!=null)
                    {
                        DeliveryAddressResults results =  value.getDeliveryAddressResults();

                        if(results!=null)
                        {
                            if(results.getErrorDetails().getErrorCode()==0)
                            {
                                DeliveryAddress deliveryAddress = results.getDeliveryAddress();

                                if(deliveryAddress!=null)
                                {
                                    Toast.makeText(getActivity(),results.getDeliveryAddress().getAddressResult(),Toast.LENGTH_SHORT).show();
                                    DeliveryAddressDetails deliveryAddressDetails = new DeliveryAddressDetails();
                                    deliveryAddressDetails.setName(binding.etReceiverName.getText().toString());
                                    deliveryAddressDetails.setAddressOne(binding.etAddressOne.getText().toString());
                                    //deliveryAddressDetails.setAddresstwo(binding.etAddressTwo.getText().toString());
                                    deliveryAddressDetails.setPhoneNo(binding.etPhoneNumber.getText().toString());
                                    deliveryAddressDetails.setPostalcode(binding.etPostcode.getText().toString());
                                    newAddressAddedListener.onAddressAdded(deliveryAddressDetails);
                                    dismiss();
                                }
                            }
                            else
                            {
                                DialogUtility.showMessageWithOk(results.getErrorDetails().getErrorMessage(),getActivity());
                            }

                        }
                    }
                }

                @Override
                public void onError(Throwable e)
                {
                    if(mProgressDialog!=null)mProgressDialog.dismiss();

                    if (!CommonUtilities.checkConnectivity(getContext()))
                        DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                    else
                        DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                }

                @Override
                public void onComplete()
                {
                    if(mProgressDialog!=null)mProgressDialog.dismiss();
                }
            });
        }
    }

    public NewAddressRequest getNewAddressRequest()
    {
        NewAddressRequest newAddressRequest = new NewAddressRequest();
        newAddressRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        newAddressRequest.setAccountId(strAccountId);
        newAddressRequest.setUserId(strUserId);
        newAddressRequest.setRequestFor("InsertDeliveryAddress");
        newAddressRequest.setUserName(binding.etReceiverName.getText().toString());
        newAddressRequest.setCompany(binding.etCompany.getText().toString());
        newAddressRequest.setAddressOne(binding.etAddressOne.getText().toString());
        newAddressRequest.setAddresstwo(binding.etAddressTwo.getText().toString());
        newAddressRequest.setCity(binding.etCity.getText().toString());
        //newAddressRequest.setCounty("C");
        //newAddressRequest.setCountry("C");
        //newAddressRequest.setState("C");
        newAddressRequest.setLandmark(binding.etLandmark.getText().toString());
        newAddressRequest.setPostalcode(binding.etPostcode.getText().toString());
        newAddressRequest.setPhoneNo(binding.etPhoneNumber.getText().toString());
        newAddressRequest.setSelectedAddr("1");
        return newAddressRequest;
    }

    public static AddNewAddress newInstance(String title,int navigationType,String accountId,String userId)
    {
        AddNewAddress frag = new AddNewAddress();
        Bundle args = new Bundle();
        args.putString(AppUtils.BUNDLE_TOOLBAR_TITLE, title);
        args.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,navigationType);
        args.putString(AppUtils.BUNDLE_ACCOUNT_ID,accountId);
        args.putString(AppUtils.BUNDLE_USER_ID,userId);
        frag.setArguments(args);
        return frag;
    }

    class TextChangeListener implements TextWatcher
    {
        int rowIndex;
        TextInputEditText editText;
        TextInputLayout til;

        public TextChangeListener(TextInputEditText editText,TextInputLayout til)
        {
            this.editText = editText;
            this.til = til;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after)
        {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count)
        {

        }

        @Override
        public void afterTextChanged(Editable s)
        {
            switch (editText.getId())
            {
                case R.id.et_receiver_name:
                     isValidUserName();break;
                case R.id.et_company:
                     isValidCompany();break;
                case R.id.et_address_one:
                     isValidAddress1();break;
                case R.id.et_address_two:
                     isValidAddress2();break;
                case R.id.et_city:
                     isValidCity();break;
                case R.id.et_state:
                     isValidState();break;
                case R.id.et_county:
                     isValidCounty();break;
                case R.id.et_country:
                     isValidCountry(); break;
                case R.id.et_landmark:
                    isValidLandmark(); break;
                case R.id.et_postcode:
                     isValidPostalCode(); break;
                case R.id.et_phone_number:
                     isValidPhoneNumber(); break;
            }
        }
    }

    public boolean areAllInputsValid()
    {
        if(isValidUserName() && isValidAddress1() && isValidPostalCode() && isValidPhoneNumber() )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isValidUserName()
    {
        if(binding.etReceiverName.getText().toString().isEmpty())
        {
            binding.etReceiverName.setError(getString(R.string.lbl_name_empty));
            binding.etReceiverName.requestFocus();
            return false;
        }
        else
        {
            binding.tlReceiverName.setError(null);
            return true;
        }
    }

    public boolean isValidCompany()
    {
        if(binding.etCompany.getText().toString().isEmpty())
        {
            binding.etCompany.setError(getString(R.string.lbl_company_empty));
            return false;
        }
        else
        {
            binding.etCompany.setError(null);
            return true;
        }
    }

    public boolean isValidAddress1()
    {
        if(binding.etAddressOne.getText().toString().isEmpty())
        {
            binding.etAddressOne.setError(getString(R.string.lbl_address_one_empty));
            binding.etAddressOne.requestFocus();
            return false;
        }
        else
        {
            binding.etAddressOne.setError(null);
            return true;
        }
    }

    public boolean isValidAddress2()
    {
        if(binding.etAddressTwo.getText().toString().isEmpty())
        {
            binding.etAddressTwo.setError(getString(R.string.lbl_address_two_empty));
            return false;
        }
        else
        {
            binding.etAddressTwo.setError(null);
            return true;
        }
    }

    public boolean isValidCity()
    {
        if(binding.etCity.getText().toString().isEmpty())
        {
            binding.etCity.setError(getString(R.string.lbl_city_empty));
            return false;
        }
        else
        {
            binding.etCity.setError(null);
            return true;
        }
    }

    public boolean isValidState()
    {
        if(binding.etState.getText().toString().isEmpty())
        {
            binding.etState.setError(getString(R.string.lbl_state_empty));
            return false;
        }
        else
        {
            binding.etState.setError(null);
            return true;
        }
    }

    public boolean isValidCounty()
    {
        if(binding.etCounty.getText().toString().isEmpty())
        {
            binding.etCounty.setError(getString(R.string.lbl_county_empty));
            return false;
        }
        else
        {
            binding.etCounty.setError(null);
            return true;
        }
    }

    public boolean isValidCountry()
    {
        if(binding.etCountry.getText().toString().isEmpty())
        {
            binding.etCountry.setError(getString(R.string.lbl_country_empty));
            return false;
        }
        else
        {
            binding.etCountry.setError(null);
            return true;
        }
    }

    public boolean isValidLandmark()
    {
        if(binding.etLandmark.getText().toString().isEmpty())
        {
            binding.etLandmark.setError(getString(R.string.lbl_landmark_empty));
            return false;
        }
        else
        {
            binding.etLandmark.setError(null);
            return true;
        }
    }

    public boolean isValidPostalCode()
    {
        if(binding.etPostcode.getText().toString().isEmpty())
        {
            binding.etPostcode.setError(getString(R.string.lbl_postal_code_empty));
            binding.etPostcode.requestFocus();
            return false;
        }
        if(!ValidationUtils.isValidPostCode(binding.etPostcode.getText().toString()))
        {
            binding.etPostcode.setError(getString(R.string.lbl_invalid_post_code));
            binding.etPostcode.requestFocus();
            return false;
        }
        else
        {
            binding.etPostcode.setError(null);
            return true;
        }
    }

    public boolean isValidPhoneNumber()
    {
        if(binding.etPhoneNumber.getText().toString().isEmpty())
        {
            binding.etPhoneNumber.setError(getString(R.string.lbl_phone_number_empty));
            binding.etPhoneNumber.requestFocus();
            return false;
        }
        else if(!ValidationUtils.isValidMobileNo(binding.etPhoneNumber.getText().toString().trim()))
        {
            binding.etPhoneNumber.setError(getString(R.string.lbl_invalid_phone_number));
            binding.etPhoneNumber.requestFocus();
            return false;
        }
        else
        {
            binding.etPhoneNumber.setError(null);
            return true;
        }
    }

}
