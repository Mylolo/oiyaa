package com.localvalu.user.manageaddress.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryAddressResponse
{
    @SerializedName("DeliveryAddressResults")
    @Expose
    private DeliveryAddressResults deliveryAddressResults;

    public DeliveryAddressResults getDeliveryAddressResults()
    {
        return deliveryAddressResults;
    }

    public void setDeliveryAddressResults(DeliveryAddressResults deliveryAddressResults)
    {
        this.deliveryAddressResults = deliveryAddressResults;
    }
}
