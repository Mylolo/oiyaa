package com.localvalu.user.manageaddress.request;

public class NewAddressRequest
{
    private String Token;
    private String AccountId;
    private String RequestFor;
    private String UserId;
    private String UserName;
    private String company;
    private String AddressOne;
    private String Addresstwo;
    private String City;
    private String County;
    private String Country;
    private String State;
    private String Landmark;
    private String Postalcode;
    private String PhoneNo;
    private String selectedAddr;

    public String getToken()
    {
        return Token;
    }

    public void setToken(String token)
    {
        Token = token;
    }

    public String getAccountId()
    {
        return AccountId;
    }

    public void setAccountId(String accountId)
    {
        AccountId = accountId;
    }

    public String getRequestFor()
    {
        return RequestFor;
    }

    public void setRequestFor(String requestFor)
    {
        RequestFor = requestFor;
    }

    public String getUserId()
    {
        return UserId;
    }

    public void setUserId(String userId)
    {
        UserId = userId;
    }

    public String getUserName()
    {
        return UserName;
    }

    public void setUserName(String userName)
    {
        UserName = userName;
    }

    public String getCompany()
    {
        return company;
    }

    public void setCompany(String company)
    {
        this.company = company;
    }

    public String getAddressOne()
    {
        return AddressOne;
    }

    public void setAddressOne(String addressOne)
    {
        AddressOne = addressOne;
    }

    public String getAddresstwo()
    {
        return Addresstwo;
    }

    public void setAddresstwo(String addresstwo)
    {
        Addresstwo = addresstwo;
    }

    public String getCity()
    {
        return City;
    }

    public void setCity(String city)
    {
        City = city;
    }

    public String getCounty()
    {
        return County;
    }

    public void setCounty(String county)
    {
        County = county;
    }

    public String getCountry()
    {
        return Country;
    }

    public void setCountry(String country)
    {
        Country = country;
    }

    public String getState()
    {
        return State;
    }

    public void setState(String state)
    {
        State = state;
    }

    public String getLandmark()
    {
        return Landmark;
    }

    public void setLandmark(String landmark)
    {
        Landmark = landmark;
    }

    public String getPostalcode()
    {
        return Postalcode;
    }

    public void setPostalcode(String postalcode)
    {
        Postalcode = postalcode;
    }

    public String getPhoneNo()
    {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo)
    {
        PhoneNo = phoneNo;
    }

    public String getSelectedAddr()
    {
        return selectedAddr;
    }

    public void setSelectedAddr(String selectedAddr)
    {
        this.selectedAddr = selectedAddr;
    }
}
