package com.localvalu.user.manageaddress.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderNowResponse
{
    @SerializedName("OrderNowResults")
    @Expose
    private OrderNowResults orderNowResults;

    public OrderNowResults getOrderNowResults()
    {
        return orderNowResults;
    }

    public void setOrderNowResults(OrderNowResults orderNowResults)
    {
        this.orderNowResults = orderNowResults;
    }
}
