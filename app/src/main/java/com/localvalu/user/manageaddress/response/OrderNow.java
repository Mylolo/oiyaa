package com.localvalu.user.manageaddress.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderNow
{
    @SerializedName("DeliveryAddressDetails")
    private List<DeliveryAddressDetails> deliveryAddressList;

    public List<DeliveryAddressDetails> getDeliveryAddressList()
    {
        return deliveryAddressList;
    }

    public void setDeliveryAddressList(List<DeliveryAddressDetails> deliveryAddressList)
    {
        this.deliveryAddressList = deliveryAddressList;
    }
}
