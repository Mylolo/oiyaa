package com.localvalu.user.manageaddress.response;

import com.google.gson.annotations.SerializedName;

public class ErrorDetails
{
    @SerializedName("ErrorCode")
    private int ErrorCode;
    @SerializedName("ErrorMessage")
    private String ErrorMessage;

    public int getErrorCode()
    {
        return ErrorCode;
    }

    public void setErrorCode(int errorCode)
    {
        ErrorCode = errorCode;
    }

    public String getErrorMessage()
    {
        return ErrorMessage;
    }

    public void setErrorMessage(String errorMessage)
    {
        ErrorMessage = errorMessage;
    }
}
