package com.localvalu.user.manageaddress.ui;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.localvalu.R;
import com.localvalu.databinding.FragmentAddressSelectionBinding;
import com.localvalu.eats.payment.ChoosePaymentFragment;
import com.localvalu.user.manageaddress.request.AddressListRequest;
import com.localvalu.user.manageaddress.request.UpdateAddressRequest;
import com.localvalu.user.manageaddress.response.DeliveryAddress;
import com.localvalu.user.manageaddress.response.DeliveryAddressDetails;
import com.localvalu.user.manageaddress.response.DeliveryAddressResponse;
import com.localvalu.user.manageaddress.response.DeliveryAddressResults;
import com.localvalu.user.manageaddress.response.OrderNow;
import com.localvalu.user.manageaddress.response.OrderNowResponse;
import com.localvalu.user.manageaddress.response.OrderNowResults;
import com.localvalu.user.repository.UserAddressRepository;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.NavigationType;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public class AddressSelection extends DialogFragment implements AddressIndexSelection,AddressUpdatedListener
{
    private static final String TAG = "AddressSelection";

    private FragmentAddressSelectionBinding binding;
    private UserAddressRepository userAddressRepository;
    private String strAccountId,strUserId,strBusinessId;
    private AddressUpdatedListener addressUpdatedListener;
    private LinearLayoutManager layoutManager;
    private AddressListAdapter addressListAdapter;
    private DeliveryAddressDetails deliveryAddressDetails;
    private int navigationType;
    private boolean addressListNotEmpty;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        addressUpdatedListener = (AddressUpdatedListener) getTargetFragment();
        userAddressRepository = new UserAddressRepository();
        strAccountId =bundle.getString(AppUtils.BUNDLE_ACCOUNT_ID);
        strUserId = bundle.getString(AppUtils.BUNDLE_USER_ID);
        strBusinessId = bundle.getString(AppUtils.BUNDLE_BUSINESS_ID);
        navigationType = bundle.getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);

        switch (navigationType)
        {
            case AppUtils.BMT_EATS:
                 setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme);
                 break;
            case AppUtils.BMT_LOCAL:
                 setStyle(DialogFragment.STYLE_NORMAL, R.style.AppThemeLocal);
                 break;
            case AppUtils.BMT_MALL:
                 setStyle(DialogFragment.STYLE_NORMAL, R.style.AppThemeMall);
                 break;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = FragmentAddressSelectionBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        binding.toolbarDialog.toolbarDetails.setBackgroundColor(AppUtils.getThemeAccentColor(view.getContext()));
        binding.toolbarDialog.textViewTitle.setText(getString(R.string.lbl_select_address));
        binding.toolbarDialog.ivAction.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                performNoAddressFoundTask();
                dismiss();
            }
        });
        setProperties();
    }

    public void setProperties()
    {
        binding.progressBar.setVisibility(View.GONE);
        binding.btnDeliverHere.setVisibility(View.GONE);
        layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.btnAddNewAddress.setOnClickListener(_OnClickListener);
        binding.btnDeliverHere.setOnClickListener(_OnClickListener);
        getAddressListFromServer();
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.btn_deliver_here:
                     setDefaultAddress();
                     break;
                case R.id.btnAddNewAddress:
                     gotoNewAddAddress();
                     break;
            }
        }
    };

    public void getAddressListFromServer()
    {
        binding.progressBar.setVisibility(View.VISIBLE);
        userAddressRepository.getAddressList(getRequestPayload()).subscribe(new Observer<OrderNowResponse>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {

            }

            @Override
            public void onNext(OrderNowResponse orderNowResponse)
            {
                binding.progressBar.setVisibility(View.GONE);

                if(orderNowResponse.getOrderNowResults()!=null)
                {
                    OrderNowResults results = orderNowResponse.getOrderNowResults();

                    if(results!=null)
                    {
                        if(results.getErrorDetails().getErrorCode()==0)
                        {
                            OrderNow orderNow = results.getOrderNow();

                            if(orderNow.getDeliveryAddressList()!=null)
                            {
                                List<DeliveryAddressDetails> list = orderNow.getDeliveryAddressList();
                                Log.d(TAG, "onNext: "+list.size());
                                setAdapter(list);
                            }
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(results.getMessage(),getActivity());
                        }
                    }
                }
            }

            @Override
            public void onError(Throwable e)
            {
                binding.progressBar.setVisibility(View.GONE);
                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }

            @Override
            public void onComplete()
            {
                binding.progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void gotoNewAddAddress()
    {
        AddNewAddress addNewAddress = AddNewAddress.newInstance(getString(R.string.lbl_add_new_address),
                navigationType,strAccountId,strUserId);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        addNewAddress.setTargetFragment(this,55);
        addNewAddress.show(fragmentManager,addNewAddress.getClass().getName());
    }

    public void setAdapter(List<DeliveryAddressDetails> list)
    {
        if(list.size()>0)
        {
            showList();
            getAndSetDefaultAddress(list);
        }
        else
            hideList();
        addressListAdapter = new AddressListAdapter(getActivity(),list);
        addressListAdapter.setAddressIndexSelection(this);
        binding.recyclerView.setAdapter(addressListAdapter);
    }

    private void getAndSetDefaultAddress( List<DeliveryAddressDetails> list)
    {
        boolean defaultAddressFound = false;

        for (DeliveryAddressDetails deliveryAddress : list)
        {
            if (deliveryAddress.getSelectedAddr().equals("1"))
            {
                deliveryAddressDetails = deliveryAddress;
            }
        }
        if (!defaultAddressFound)
        {
            deliveryAddressDetails = list.get(0);
        }
    }

    public AddressListRequest getRequestPayload()
    {
        AddressListRequest addressListRequest = new AddressListRequest();
        addressListRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        addressListRequest.setAccountId(strAccountId);
        addressListRequest.setUserId(strUserId);
        addressListRequest.setRequestFor("MyBasketDetails");
        addressListRequest.setBusinessId(strBusinessId);
        addressListRequest.setDistance("12");
        return addressListRequest;
    }

    public static AddressSelection newInstance(String title, int navigationType,
                                               String accountId, String userId,String businessId)
    {
        AddressSelection frag = new AddressSelection();
        Bundle args = new Bundle();
        args.putString(AppUtils.BUNDLE_TOOLBAR_TITLE, title);
        args.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,navigationType);
        args.putString(AppUtils.BUNDLE_ACCOUNT_ID,accountId);
        args.putString(AppUtils.BUNDLE_USER_ID,userId);
        args.putString(AppUtils.BUNDLE_BUSINESS_ID,businessId);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public void onItemClicked(int type,DeliveryAddressDetails deliveryAddressDetails)
    {
        this.deliveryAddressDetails=deliveryAddressDetails;

        switch (type)
        {
            case 0:

                 break;
            case 1:
                 //removal code
                 removeAddress();
                 break;
        }
    }

    public UpdateAddressRequest getDefaultAddressRequestPayload()
    {
        UpdateAddressRequest updateAddressRequest = new UpdateAddressRequest();
        updateAddressRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        updateAddressRequest.setAccountId(strAccountId);
        updateAddressRequest.setOA_Id(deliveryAddressDetails.getOAId());
        updateAddressRequest.setRequestFor("SelectedDeliveryAddress");
        return updateAddressRequest;
    }

    public void setDefaultAddress()
    {
        binding.progressBar.setVisibility(View.VISIBLE);
        userAddressRepository.setDefaultAddress(getDefaultAddressRequestPayload())
                             .subscribe(new Observer<DeliveryAddressResponse>()
                             {
                                 @Override
                                 public void onSubscribe(Disposable d)
                                 {

                                 }

                                 @Override
                                 public void onNext(DeliveryAddressResponse deliveryAddressResponse)
                                 {
                                     binding.progressBar.setVisibility(View.GONE);
                                     if(deliveryAddressResponse!=null)
                                     {
                                         DeliveryAddressResults results = deliveryAddressResponse.getDeliveryAddressResults();

                                         if(results!=null)
                                         {
                                             if(results.getErrorDetails().getErrorCode()==0)
                                             {
                                                 DeliveryAddress deliveryAddress = results.getDeliveryAddress();

                                                 Toast.makeText(getContext(),deliveryAddress.getAddressResult(),Toast.LENGTH_SHORT).show();

                                                 addressUpdatedListener.onAddressAdded(deliveryAddressDetails);
                                                 dismiss();
                                             }
                                             else
                                             {
                                                 DialogUtility.showMessageWithOk(results.getErrorDetails().getErrorMessage(),getActivity());
                                             }
                                         }

                                     }
                                 }

                                 @Override
                                 public void onError(Throwable e)
                                 {
                                     binding.progressBar.setVisibility(View.GONE);
                                     if (!CommonUtilities.checkConnectivity(getContext()))
                                         DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                                     else
                                         DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                                 }

                                 @Override
                                 public void onComplete()
                                 {
                                     binding.progressBar.setVisibility(View.GONE);
                                 }
                             });
    }

    public UpdateAddressRequest getRemovalAddressRequestPayload()
    {
        UpdateAddressRequest updateAddressRequest = new UpdateAddressRequest();
        updateAddressRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        updateAddressRequest.setAccountId(strAccountId);
        updateAddressRequest.setOA_Id(deliveryAddressDetails.getOAId());
        updateAddressRequest.setRequestFor("RemoveDeliveryAddress");
        return updateAddressRequest;
    }

    public void removeAddress()
    {
        binding.progressBar.setVisibility(View.VISIBLE);
        userAddressRepository.removeAddress(getRemovalAddressRequestPayload())
                .subscribe(new Observer<DeliveryAddressResponse>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {

                    }

                    @Override
                    public void onNext(DeliveryAddressResponse deliveryAddressResponse)
                    {
                        binding.progressBar.setVisibility(View.GONE);
                        if(deliveryAddressResponse!=null)
                        {
                            DeliveryAddressResults results = deliveryAddressResponse.getDeliveryAddressResults();

                            if(results!=null)
                            {
                                if(results.getErrorDetails().getErrorCode()==0)
                                {
                                    DeliveryAddress deliveryAddress = results.getDeliveryAddress();

                                    Toast.makeText(getContext(),deliveryAddress.getAddressResult(),Toast.LENGTH_SHORT).show();

                                    addressListAdapter.removeItem(deliveryAddressDetails);

                                    if(addressListAdapter.getItemCount()>0) showList();
                                    else
                                    {
                                        hideList();
                                    }
                                }
                                else
                                {
                                    DialogUtility.showMessageWithOk(results.getErrorDetails().getErrorMessage(),getActivity());
                                }
                            }

                        }
                    }

                    @Override
                    public void onError(Throwable e)
                    {
                        binding.progressBar.setVisibility(View.GONE);
                        if (!CommonUtilities.checkConnectivity(getContext()))
                            DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                        else
                            DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                    }

                    @Override
                    public void onComplete()
                    {
                        binding.progressBar.setVisibility(View.GONE);
                    }
                });
    }

    public void showList()
    {
        binding.recyclerView.setVisibility(View.VISIBLE);
        binding.tvNoAddressFound.setVisibility(View.GONE);
        binding.btnDeliverHere.setVisibility(View.VISIBLE);
        addressListNotEmpty=true;
    }

    public void hideList()
    {
        binding.recyclerView.setVisibility(View.GONE);
        binding.tvNoAddressFound.setVisibility(View.VISIBLE);
        binding.btnDeliverHere.setVisibility(View.GONE);
        addressListNotEmpty=false;
    }

    private void updateUIInCheckoutScreen()
    {
    }

    @Override
    public void onNoAddressFound()
    {

    }

    @Override
    public void onAddressAdded(DeliveryAddressDetails deliveryAddressDetails)
    {
        addressUpdatedListener.onAddressAdded(deliveryAddressDetails);
        dismiss();
    }

    @Override
    public void onAddressSelected(DeliveryAddressDetails deliveryAddressDetails)
    {

    }

    @Override
    public void onDestroy()
    {
        performNoAddressFoundTask();
        super.onDestroy();
    }

    private void performNoAddressFoundTask()
    {
        if(!addressListNotEmpty)
        {
            if(addressUpdatedListener!=null)
            {
                addressUpdatedListener.onNoAddressFound();
            }
        }
    }

}
