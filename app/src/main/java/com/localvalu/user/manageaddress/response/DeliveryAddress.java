package com.localvalu.user.manageaddress.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryAddress
{

    @SerializedName("AddressResult")
    @Expose
    private String AddressResult;

    public String getAddressResult()
    {
        return AddressResult;
    }

    public void setAddressResult(String addressResult)
    {
        AddressResult = addressResult;
    }

}
