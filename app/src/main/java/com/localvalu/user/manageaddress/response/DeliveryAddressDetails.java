package com.localvalu.user.manageaddress.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeliveryAddressDetails
{
    @SerializedName("OA_Id")
    @Expose
    private String OAId;

    @SerializedName("User_id")
    @Expose
    private String userId;

    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("company")
    @Expose
    private String company;

    @SerializedName("AddressOne")
    @Expose
    private String AddressOne;

    @SerializedName("Addresstwo")
    @Expose
    private String Addresstwo;

    @SerializedName("City")
    @Expose
    private String City;

    @SerializedName("County")
    @Expose
    private String County;

    @SerializedName("State")
    @Expose
    private String State;

    @SerializedName("Country")
    @Expose
    private String Country;

    @SerializedName("Landmark")
    @Expose
    private String Landmark;

    @SerializedName("Postalcode")
    @Expose
    private String Postalcode;

    @SerializedName("PhoneNo")
    @Expose
    private String PhoneNo;

    @SerializedName("selectedAddr")
    @Expose
    private String selectedAddr;

    @SerializedName("isactive")
    @Expose
    private String isactive;

    @SerializedName("updateddate")
    @Expose
    private String updateddate;

    @SerializedName("createddate")
    @Expose
    private String createddate;

    public String getOAId()
    {
        return OAId;
    }

    public void setOAId(String OAId)
    {
        this.OAId = OAId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCompany()
    {
        return company;
    }

    public void setCompany(String company)
    {
        this.company = company;
    }

    public String getAddressOne()
    {
        return AddressOne;
    }

    public void setAddressOne(String addressOne)
    {
        AddressOne = addressOne;
    }

    public String getAddresstwo()
    {
        return Addresstwo;
    }

    public void setAddresstwo(String addresstwo)
    {
        Addresstwo = addresstwo;
    }

    public String getCity()
    {
        return City;
    }

    public void setCity(String city)
    {
        City = city;
    }

    public String getCounty()
    {
        return County;
    }

    public void setCounty(String county)
    {
        County = county;
    }

    public String getState()
    {
        return State;
    }

    public void setState(String state)
    {
        State = state;
    }

    public String getCountry()
    {
        return Country;
    }

    public void setCountry(String country)
    {
        Country = country;
    }

    public String getLandmark()
    {
        return Landmark;
    }

    public void setLandmark(String landmark)
    {
        Landmark = landmark;
    }

    public String getPostalcode()
    {
        return Postalcode;
    }

    public void setPostalcode(String postalcode)
    {
        Postalcode = postalcode;
    }

    public String getPhoneNo()
    {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo)
    {
        PhoneNo = phoneNo;
    }

    public String getSelectedAddr()
    {
        return selectedAddr;
    }

    public void setSelectedAddr(String selectedAddr)
    {
        this.selectedAddr = selectedAddr;
    }

    public String getIsactive()
    {
        return isactive;
    }

    public void setIsactive(String isactive)
    {
        this.isactive = isactive;
    }

    public String getUpdateddate()
    {
        return updateddate;
    }

    public void setUpdateddate(String updateddate)
    {
        this.updateddate = updateddate;
    }

    public String getCreateddate()
    {
        return createddate;
    }

    public void setCreateddate(String createddate)
    {
        this.createddate = createddate;
    }
}
