package com.localvalu.user.manageaddress.ui;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localvalu.R;
import com.localvalu.databinding.ViewItemAddressListBigBinding;
import com.localvalu.user.manageaddress.response.DeliveryAddressDetails;

import java.util.List;

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.MyViewHolder>
{

    private List<DeliveryAddressDetails> items;
    private Context context;
    private AddressIndexSelection addressIndexSelection;
    private int lastSelectedPosition = -1;

    public AddressListAdapter(Context context, List<DeliveryAddressDetails> items)
    {
        this.context=context;
        this.items = items;
    }

    public void setAddressIndexSelection(AddressIndexSelection addressIndexSelection)
    {
        this.addressIndexSelection = addressIndexSelection;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
       // View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_item_order_list_eat, viewGroup, false);
       // return new AddressListAdapter.MyViewHolder(view.);
        ViewItemAddressListBigBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.view_item_address_list_big,viewGroup,false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int i)
    {
        final DeliveryAddressDetails deliveryAddressDetails = items.get(i);
        myViewHolder.binding.tvAddressOne.setText(deliveryAddressDetails.getAddressOne());
        //myViewHolder.binding.tvAddressTwo.setText(deliveryAddressDetails.getAddresstwo());
        myViewHolder.binding.tvPhoneNumber.setText(deliveryAddressDetails.getPhoneNo());
        myViewHolder.binding.tvName.setText(deliveryAddressDetails.getName());
        boolean checked =false;
        if(deliveryAddressDetails.getSelectedAddr().equals("1"))
        {
            checked=true;
            lastSelectedPosition=i;
        }
        else checked=false;
        myViewHolder.binding.radioAddressSelect.setChecked(lastSelectedPosition == i);
        myViewHolder.binding.radioAddressSelect.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                lastSelectedPosition = i;
                addressIndexSelection.onItemClicked(0,deliveryAddressDetails);
                notifyDataSetChanged();
            }
        });
        myViewHolder.binding.btnRemove.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addressIndexSelection.onItemClicked(1,deliveryAddressDetails);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return items.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        ViewItemAddressListBigBinding binding;

        public MyViewHolder(@NonNull ViewItemAddressListBigBinding binding)
        {
            super(binding.getRoot());
            this.binding=binding;
        }

    }

    public void removeItem(DeliveryAddressDetails deliveryAddressDetails)
    {
        items.remove(deliveryAddressDetails);
        notifyDataSetChanged();
    }



}
