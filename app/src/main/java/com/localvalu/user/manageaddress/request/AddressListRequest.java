package com.localvalu.user.manageaddress.request;

public class AddressListRequest
{
    private String Token;
    private String AccountId;
    private String userId;
    private String BusinessId;
    private String requestFor;
    private String Distance;

    public String getToken()
    {
        return Token;
    }

    public void setToken(String token)
    {
        Token = token;
    }

    public String getAccountId()
    {
        return AccountId;
    }

    public void setAccountId(String accountId)
    {
        AccountId = accountId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getBusinessId()
    {
        return BusinessId;
    }

    public void setBusinessId(String businessId)
    {
        BusinessId = businessId;
    }

    public String getRequestFor()
    {
        return requestFor;
    }

    public void setRequestFor(String requestFor)
    {
        this.requestFor = requestFor;
    }

    public String getDistance()
    {
        return Distance;
    }

    public void setDistance(String distance)
    {
        Distance = distance;
    }
}
