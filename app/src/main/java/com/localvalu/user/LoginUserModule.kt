package com.localvalu.user

import android.content.Context
import com.localvalu.base.LocalDataSource
import com.localvalu.base.RemoteDataSource
import com.localvalu.db.AppDatabase
import com.localvalu.registration.api.RegisterUserApi
import com.localvalu.registration.repository.RegisterRepository
import com.localvalu.user.api.LoginApi
import com.localvalu.user.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class LoginUserModule
{
    @Provides
    fun providesLoginUserApi(remoteDataSource: RemoteDataSource,
                             @ApplicationContext context: Context): LoginApi
    {
        return remoteDataSource.buildApi(LoginApi::class.java,context)
    }

    @Provides
    fun providesUserRepository(loginUserApi:LoginApi,appDatabase: AppDatabase): UserRepository
    {
        return UserRepository(loginUserApi,appDatabase)
    }

    @Provides
    fun providesAppDatabase(localDataSource: LocalDataSource,
                               @ApplicationContext context: Context): AppDatabase
    {
        return LocalDataSource().buildLocalSource(context)
    }

}