package com.localvalu.user;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtils
{
    public static boolean isValidMobileNo(String mobileNumber)
    {
        String regexMobileNo="^(\\+44\\s?7\\d{3}|\\(?07\\d{3}\\)?)\\s?\\d{3}\\s?\\d{3}$";

        Pattern p = Pattern.compile(regexMobileNo);

        // Pattern class contains matcher() method
        // to find matching between given number
        // and regular expression
        Matcher m = p.matcher(mobileNumber);
        return (m.find() && m.group().equals(mobileNumber));
    }

    public static boolean isValidPostCode(String postCode)
    {
        postCode = postCode.trim();
        String regexPostCode = "^(([A-Z][A-HJ-Y]?\\d[A-Z\\d]?|ASCN|STHL|TDCU|BBND|[BFS]IQQ|PCRN|TKCA) ?\\d[A-Z]{2}|BFPO ?\\d{1,4}|(KY\\d|MSR|VG|AI)[ -]?\\d{4}|[A-Z]{2} ?\\d{2}|GE ?CX|GIR ?0A{2}|SAN ?TA1)$";
        Pattern p = Pattern.compile(regexPostCode);
        // Pattern class contains matcher() method
        // to find matching between given number
        // and regular expression
        Matcher m = p.matcher(postCode);
        return (m.find() && m.group().equals(postCode));
    }

}
