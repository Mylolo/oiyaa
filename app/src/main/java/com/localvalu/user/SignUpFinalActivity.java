package com.localvalu.user;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;

import android.text.InputType;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.localvalu.R;
import com.localvalu.databinding.ActivitySignUpFinalBinding;
import com.localvalu.user.dto.RegisterDataDto;
import com.localvalu.user.dto.UserBasicInfo;
import com.localvalu.user.wrapper.RegisterCustomerResultWrapper;
import com.localvalu.user.wrapper.UserWrapper;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.CommonUtility;
import com.utility.Config;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpFinalActivity extends AppCompatActivity
{
    private static final String TAG = SignUpFinalActivity.class.getSimpleName();
    //For API Call
    public ProgressDialog mProgressDialog;
    public ApiInterface apiInterface;
    private RegisterDataDto registerDataDto;
    private ActivitySignUpFinalBinding binding;
    private int colorBlack,colorWhite;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeLogin);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up_final);
        colorBlack = getResources().getColor(R.color.colorBlack);
        colorWhite = getResources().getColor(R.color.colorWhite);
        bouldObjectForHandlingAPI();
        setProperties();
        readFromBundle();
    }

    private void readFromBundle()
    {
        if (getIntent().getExtras() != null)
        {
            registerDataDto = (RegisterDataDto) getIntent().getExtras().getSerializable("registerDataDto");
            System.out.println(registerDataDto.toString());
        }
    }

    private void bouldObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    private void setProperties()
    {
        Typeface type = ResourcesCompat.getFont(getApplicationContext(), R.font.comfortaa_regular);
        binding.etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        binding.etPassword.setTypeface(type);
        binding.etConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        binding.etConfirmPassword.setTypeface(type);

        Drawable drawable = binding.imgBack.getDrawable();
        if (drawable != null)
        {
            drawable.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP);
            binding.imgBack.setImageDrawable(drawable);
        }
        setPasswordTextInstructions();
        binding.btnCreateAccount.setOnClickListener(_OnClickListener);
        binding.cnlMain.setOnClickListener(_OnClickListener);
        binding.imgBack.setOnClickListener(_OnClickListener);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            onButtonClick(view);
        }
    };

    private void setPasswordTextInstructions()
    {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        SpannableString str1= new SpannableString(getString(R.string.lbl_password_one));
        str1.setSpan(new ForegroundColorSpan(colorBlack), 0, str1.length(), 0);
        builder.append(str1).append(" ");

        SpannableString str2= new SpannableString(getString(R.string.lbl_password_one_a_white));
        str2.setSpan(new ForegroundColorSpan(colorWhite), 0, str2.length(), 0);
        builder.append(str2).append(" ");

        binding.tvPasswordTextOne.setText(builder, TextView.BufferType.SPANNABLE);
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                 finish();
                 break;
            case R.id.btnCreateAccount:
                 if (isValid())
                 {
                    callAPI();
                 }
                 break;
            case R.id.linearLayout_login:
                 ActivityController.startNextActivity(this, LoginActivity.class, false);
                 finish();
                 break;
            case R.id.cnlMain:
                 CommonUtility.hideKeyboard(this);
                 break;
        }
    }

    private boolean isValid()
    {
        if (binding.etPassword.getText().toString().isEmpty())
        {
            binding.etPassword.requestFocus();
            binding.etPassword.setError(getString(R.string.enter_pass));
            return false;
        }
        else if (!checkRegex(binding.etPassword.getText().toString().trim()))
        {
            binding.etPassword.requestFocus();
            binding.etPassword.setError(getString(R.string.greater_pass));
            return false;
        }
        else if (binding.etConfirmPassword.getText().toString().isEmpty())
        {
            binding.etConfirmPassword.requestFocus();
            binding.etConfirmPassword.setError(getString(R.string.enter_pass));
        }
        else if (!checkRegex(binding.etConfirmPassword.getText().toString().trim()))
        {
            binding.etConfirmPassword.requestFocus();
            binding.etConfirmPassword.setError(getString(R.string.greater_pass));
            return false;
        }
        else if (!binding.etConfirmPassword.getText().toString().equals(binding.etPassword.getText().toString()))
        {
            binding.etConfirmPassword.requestFocus();
            binding.etConfirmPassword.setError(getString(R.string.pass_not_matched));
            return false;
        }
        return true;
    }

    private boolean checkRegex(String password)
    {
        // Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
        final String PASSWORD_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[~`#@$!%*?^&])[A-Za-z\\d~`#@$!%*?&]{8,}$";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password.trim());
        return matcher.matches();
    }

    private void callAPI()
    {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("userName", registerDataDto.name);
            objMain.put("Mobile", registerDataDto.mobile);
            objMain.put("email", registerDataDto.email);
            objMain.put("ReferralCode", registerDataDto.qRCodeInput);
            objMain.put("password", binding.etPassword.getText().toString());
            objMain.put("Dateofbirth", registerDataDto.dob);
            objMain.put("Agegroup", registerDataDto.ageGroup);
            objMain.put("gender", registerDataDto.gender);
            objMain.put("Address", registerDataDto.address);
            objMain.put("city", registerDataDto.city);
            objMain.put("country", "United Kingdom");
            objMain.put("Home_postcode", registerDataDto.homePostCode);
            objMain.put("Work_postcode", registerDataDto.workPostCode);
            objMain.put("Passion_filled", registerDataDto.passionFilled);
            objMain.put("passionDetails", registerDataDto.passionDetails);
            objMain.put("SourceFrom", "Android");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<RegisterCustomerResultWrapper> dtoCall = apiInterface.userSignUpAPI(body);
        dtoCall.enqueue(new Callback<RegisterCustomerResultWrapper>()
        {
            @Override
            public void onResponse(Call<RegisterCustomerResultWrapper> call, Response<RegisterCustomerResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.code() != 400)
                    {
                        if (response.body().registerCustomerResult.errorDetails.errorCode == 0)
                        {
                            callLoginAPI(response.body().registerCustomerResult.userBasicInfo.noOfTokens);
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(response.body().registerCustomerResult.errorDetails.errorMessage, SignUpFinalActivity.this);
                        }
                    }
                    else
                    {
                        Toast.makeText(SignUpFinalActivity.this, "You have entered wrong data.", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<RegisterCustomerResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(SignUpFinalActivity.this))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), SignUpFinalActivity.this);
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), SignUpFinalActivity.this);
            }
        });

    }

    private void callLoginAPI(final String strNumberOfTokens)
    {
        //API
        Call<UserWrapper> call;
        mProgressDialog.show();

        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("sToken", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("sEmail", registerDataDto.email.trim());
            objMain.put("sPassword", binding.etPassword.getText().toString().trim());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        call = apiInterface.loginAPI(body);
        call.enqueue(new Callback<UserWrapper>()
        {
            @Override
            public void onResponse(Call<UserWrapper> call, Response<UserWrapper> response)
            {
                try
                {
                    if (response.body().custLoginResult.errorDetails.errorCode == 0)
                    {
                        UserBasicInfo userBasicInfo = response.body().custLoginResult.userBasicInfos;

                        PreferenceUtility.saveObjectInAppPreference(SignUpFinalActivity.this, userBasicInfo, PreferenceUtility.APP_PREFERENCE_NAME);
                        Bundle bundle = new Bundle();
                        bundle.putString(AppUtils.BUNDLE_NEW_TOKEN_BALANCE, strNumberOfTokens);
                        ActivityController.startNextActivity(SignUpFinalActivity.this, SignUpCongratulationsActivity.class, bundle, true);
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().custLoginResult.errorDetails.errorMessage, SignUpFinalActivity.this);
                    }
                }
                catch (Exception e)
                {

                }
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<UserWrapper> call, Throwable t)
            {
                if (!CommonUtilities.checkConnectivity(SignUpFinalActivity.this))
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), SignUpFinalActivity.this);
                }
                else
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), SignUpFinalActivity.this);
                }
                mProgressDialog.dismiss();
            }
        });
    }
}
