package com.localvalu.user;

//import static androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG;
//import static androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
//import androidx.biometric.BiometricManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;

import android.text.InputType;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.localvalu.BuildConfig;
import com.localvalu.R;
import com.localvalu.common.dialog.DialogCallBack;
import com.localvalu.common.dialog.DialogModel;
import com.localvalu.common.dialog.SingleButtonDialog;
import com.localvalu.databinding.ActivityLoginBinding;
import com.localvalu.user.dashboard.ui.DashboardActivity;
import com.localvalu.user.dto.UserBasicInfo;
import com.localvalu.user.wrapper.UserWrapper;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.CommonUtility;
import com.utility.Config;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements DialogCallBack
{
    private static final String TAG =LoginActivity.class.getName();
    //For API Call
    public ProgressDialog mProgressDialog;
    public ApiInterface apiInterface;
    private ActivityLoginBinding binding;
    private int colorWhite;
    private int colorBlack;
    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    //private BiometricManager biometricManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeLogin);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        colorBlack = ContextCompat.getColor(getApplicationContext(),R.color.colorBlack);
        colorWhite = ContextCompat.getColor(getApplicationContext(),R.color.colorWhite);
        initView();
        initGoogleSignIn();
        setSavedEmailAndPassword();
        buildObjectForHandlingAPI();
    }

    private void initGoogleSignIn()
    {
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_client_id))
                .requestEmail()
                .requestProfile()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    private void initView()
    {
        Typeface type = ResourcesCompat.getFont(getApplicationContext(), R.font.comfortaa_regular);
        binding.etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        binding.etPassword.setTypeface(type);
        setSupportingText();
        binding.cnlRegister.setOnClickListener(_OnClickListener);
        binding.tvForgotPassword.setOnClickListener(_OnClickListener);
        binding.tvLoginTermsAndConditions.setOnClickListener(_OnClickListener);
        binding.tvLoginPrivacyPolicy.setOnClickListener(_OnClickListener);
        binding.btnLogin.setOnClickListener(_OnClickListener);
        binding.tvBiometricLogin.setOnClickListener(_OnClickListener);
    }

    private void setSupportingText()
    {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        SpannableString str1= new SpannableString(getString(R.string.lbl_login_bottom_text_one));
        str1.setSpan(new ForegroundColorSpan(colorBlack), 0, str1.length(), 0);
        builder.append(str1).append(" ");

        SpannableString str2= new SpannableString(getString(R.string.lbl_login_bottom_text_one_a));
        str2.setSpan(new ForegroundColorSpan(colorWhite), 0, str2.length(), 0);
        builder.append(str2).append(" ");

        binding.tvLoginTermsAndConditions.setText(builder, TextView.BufferType.SPANNABLE);

        SpannableStringBuilder builder2 = new SpannableStringBuilder();

        SpannableString str4= new SpannableString(getString(R.string.lbl_login_bottom_text_two));
        str4.setSpan(new ForegroundColorSpan(colorBlack), 0, str4.length(), 0);
        builder2.append(str4).append(" ");

        SpannableString str5= new SpannableString(getString(R.string.lbl_login_bottom_text_two_a));
        str5.setSpan(new ForegroundColorSpan(colorWhite), 0, str5.length(), 0);
        builder2.append(str5).append(" ");

        binding.tvLoginPrivacyPolicy.setText(builder2, TextView.BufferType.SPANNABLE);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        /*if(mAuth!=null)
        {
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if(currentUser!=null)
            {
                updateUI(currentUser);
            }
        }*/
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            onButtonClick(view);
        }
    };

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {

            case R.id.cnlRegister:
                 ActivityController.startNextActivity(this, SignUpActivity.class, false);
                 break;
            case R.id.btnLogin:
                 if (isValid())
                 {
                    callAPI();
                    //     Bundle bundle = new Bundle();
                    //    ActivityController.startNextActivity(this, DashboardActivity.class, bundle, true);
                 }
                 break;
            case R.id.cl_main:
                 CommonUtility.hideKeyboard(this);
                 break;
            case R.id.tvForgotPassword:
                 ActivityController.startNextActivity(this, ForgotPasswordActivity.class, false);
                 break;
            case R.id.tvLoginTermsAndConditions:
                 StringBuilder strTACUri = new StringBuilder();
                 strTACUri.append("https://www.oiyaa.com//terms_conditions/WebsiteTermsandConditions.pdf");
                 showTermsAndConditionsOrPrivacyPolicy(strTACUri.toString(),getString(R.string.lbl_open_terms_and_conditions_file));
                 break;
            case R.id.tvLoginPrivacyPolicy:
                 StringBuilder strUri = new StringBuilder();
                 strUri.append("https://www.oiyaa.com//terms_conditions/App%20Privacy%20Policy.pdf");
                 showTermsAndConditionsOrPrivacyPolicy(strUri.toString(),getString(R.string.lbl_open_privacy_policy_file));
                 break;
            case R.id.btnGoogleSignIn:
                 signIn();
                 break;
            case R.id.tvBiometricLogin:
                 proceedWithBiometricLogin();
                 break;
        }
    }

    private void callAPI()
    {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        //API
        Call<UserWrapper> call;
        mProgressDialog.show();

        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("sToken", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("sEmail", binding.etEmail.getText().toString().trim());
            objMain.put("sPassword", binding.etPassword.getText().toString().trim());
            objMain.put("fcmId", regId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        call = apiInterface.loginAPI(body);
        call.enqueue(new Callback<UserWrapper>()
        {
            @Override
            public void onResponse(Call<UserWrapper> call, Response<UserWrapper> response)
            {
                try
                {
                    if (response.body().custLoginResult.errorDetails.errorCode == 0)
                    {
                        UserBasicInfo userBasicInfo = response.body().custLoginResult.userBasicInfos;

                        PreferenceUtility.saveObjectInAppPreference(LoginActivity.this, userBasicInfo, PreferenceUtility.APP_PREFERENCE_NAME);
                        ActivityController.startNextActivity(LoginActivity.this, DashboardActivity.class, true);
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().custLoginResult.errorDetails.errorMessage, LoginActivity.this);
                    }
                }
                catch (Exception e)
                {

                }
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<UserWrapper> call, Throwable t)
            {
                if (!CommonUtilities.checkConnectivity(LoginActivity.this))
                {
                    //showDialog(getResources().getString(R.string.network_unavailable));
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), LoginActivity.this);
                }
                else
                {
                    //showDialog(getResources().getString(R.string.server_alert));
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), LoginActivity.this);
                }
                mProgressDialog.dismiss();
            }
        });
    }

    private void signIn()
    {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        signInLauncher.launch(signInIntent);
    }

    ActivityResultLauncher<Intent> signInLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>()
    {
        @Override
        public void onActivityResult(ActivityResult result)
        {
            Intent resultIntent = result.getData();
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(resultIntent);
            try
            {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.d(TAG, "firebaseAuthWithGoogle:" + account.getId());
                firebaseAuthWithGoogle(account.getIdToken());
            }
            catch (ApiException e)
            {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
            }

        }
    });

    private void firebaseAuthWithGoogle(String idToken)
    {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        if (task.isSuccessful())
                        {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        }
                        else
                        {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            updateUI(null);
                        }

                    }
                });

    }

    private void updateUI(FirebaseUser firebaseUser)
    {
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"firebaseUserID - "+firebaseUser.getUid());
            Log.d(TAG,"firebaseUserDisplayName - "+firebaseUser.getDisplayName());
            Log.d(TAG,"firebaseUserEmail - "+firebaseUser.getEmail());
            if(firebaseUser.getPhoneNumber()!=null)
                Log.d(TAG, "firebaseUserPhoneNumber -" + firebaseUser.getPhoneNumber());
            else
                Log.d(TAG,"firebaseUserPhoneNumber - Not Available");
        }
        Intent intent = new Intent(getApplicationContext(),SignUpActivity.class);
        if(firebaseUser!=null)
        {
            if(firebaseUser.getDisplayName()!=null)
            {
                if(!firebaseUser.getDisplayName().equals(""))
                {
                    intent.putExtra(AppUtils.BUNDLE_FULL_NAME,firebaseUser.getDisplayName());
                }
            }
            if(firebaseUser.getEmail()!=null)
            {
                if(!firebaseUser.getEmail().equals(""))
                {
                    intent.putExtra(AppUtils.BUNDLE_EMAIL,firebaseUser.getEmail());
                }
            }
            if(firebaseUser.getPhoneNumber()!=null)
            {
                if(!firebaseUser.getPhoneNumber().equals(""))
                {
                    intent.putExtra(AppUtils.BUNDLE_MOBILE,firebaseUser.getPhoneNumber());
                }
            }
            startActivity(intent);
            FirebaseAuth.getInstance().signOut();
        }
    }

    private void setSavedEmailAndPassword()
    {
        String emailID = PreferenceUtility.getStringFromPreference(this, "EmailID");
        String password = PreferenceUtility.getStringFromPreference(this, "Password");
        binding.etEmail.setText(emailID);
        binding.etPassword.setText(password);
    }

    private boolean isValid()
    {
        if (!Patterns.EMAIL_ADDRESS.matcher(binding.etEmail.getText().toString()).matches())
        {
            binding.etEmail.setError("Enter A Valid Email ID");
            binding.etEmail.requestFocus();
            return false;
        }
        else if (binding.etPassword.getText().toString().isEmpty())
        {
            binding.etPassword.setError("Enter Password");
            binding.etPassword.requestFocus();
            return false;
        }
        else if (binding.etPassword.getText().toString().length() < 6)
        {
            binding.etPassword.setError("Password should be at least 6 digit.");
            binding.etPassword.requestFocus();
            return false;
        }
        return true;
    }

    private void gotoTermsAndConditionsPage()
    {
        /*Bundle bundle = new Bundle();
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_EATS);
        bundle.putBoolean(AppUtils.BUNDLE_IS_FROM_LOGIN_PAGE,true);
        ActivityController.startNextActivity(this, TermsAndConditionActivity.class, bundle, false);*/
    }

    private void showTermsAndConditionsOrPrivacyPolicy(String strUri,String title)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        browserIntent.setDataAndType(Uri.parse(strUri), "application/pdf");

        Intent chooser = Intent.createChooser(browserIntent, title);
        chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional

        startActivity(chooser);
    }

    private void showTPrivacyPolicy(String strUri,String title)
    {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
        browserIntent.setDataAndType(Uri.parse(strUri), "application/pdf");

        Intent chooser = Intent.createChooser(browserIntent, getString(R.string.lbl_open_terms_and_conditions_file));
        chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // optional

        startActivity(chooser);
    }

    private void proceedWithBiometricLogin()
    {
        /*biometricManager = BiometricManager.from(this);
        switch (biometricManager.canAuthenticate(BIOMETRIC_STRONG | DEVICE_CREDENTIAL)) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                Log.d("MY_APP_TAG", "App can authenticate using biometrics.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                Log.e("MY_APP_TAG", "No biometric features available on this device.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                Log.e("MY_APP_TAG", "Biometric features are currently unavailable.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                // Prompts the user to create credentials that your app accepts.
                final Intent enrollIntent = new Intent(Settings.ACTION_BIOMETRIC_ENROLL);
                enrollIntent.putExtra(Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED,
                        BIOMETRIC_STRONG | DEVICE_CREDENTIAL);
                bioMetricLauncher.launch(enrollIntent);
                break;
        }*/
    }

    ActivityResultLauncher<Intent> bioMetricLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>()
    {
        @Override
        public void onActivityResult(ActivityResult result)
        {
            Intent resultIntent = result.getData();
            if(result.getResultCode()==RESULT_OK)
            {
                Log.d(TAG,"Biometric is enabled in settings");
            }
        }
    });

    @Override
    public void onPointerCaptureChanged(boolean hasCapture)
    {

    }

    private void showDialog(String strMessage)
    {
        DialogModel dialogModel = new DialogModel();
        dialogModel.setTitle(getString(R.string.dialog_title));
        dialogModel.setMessage(strMessage);
        dialogModel.setFromActivity(true);
        SingleButtonDialog singleButtonDialog = SingleButtonDialog.newInstance(dialogModel);
        singleButtonDialog.show(getSupportFragmentManager(), SingleButtonDialog.class.getName());
    }

    @Override
    public void onNeutralButtonClicked()
    {

    }

    @Override
    public void onPositiveButtonClicked()
    {

    }

    @Override
    public void onNegativeButtonClicked()
    {

    }


}
