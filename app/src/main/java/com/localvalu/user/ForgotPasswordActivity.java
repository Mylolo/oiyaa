package com.localvalu.user;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;

import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.localvalu.R;
import com.localvalu.databinding.ActivityForgotPasswordBinding;
import com.localvalu.user.dto.forgetpassword.ForgetPasswordRequest;
import com.localvalu.user.dto.forgetpassword.ForgetPasswordResponse;
import com.localvalu.user.repository.ForgetPasswordRepository;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AlertDialogCallBack;
import com.utility.CommonUtilities;
import com.utility.CommonUtility;
import com.utility.DialogUtility;

import org.jetbrains.annotations.NotNull;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class ForgotPasswordActivity extends AppCompatActivity
{
    private static final String TAG = "ForgotPasswordActivity";

    //For API Call
    public ProgressDialog mProgressDialog;
    public ApiInterface apiInterface;
    private ActivityForgotPasswordBinding binding;
    private int colorBlack,colorGray,colorWhite;
    private ForgetPasswordRepository forgetPasswordRepository;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeLogin);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_forgot_password);
        colorBlack = ContextCompat.getColor(getApplicationContext(),R.color.colorBlack);
        colorGray  = ContextCompat.getColor(getApplicationContext(),R.color.colorGray);
        colorWhite = ContextCompat.getColor(getApplicationContext(),R.color.colorWhite);
        setProperties();
        bouldObjectForHandlingAPI();
    }

    private void setProperties()
    {
        Drawable drawableBack = ResourcesCompat.getDrawable(getResources(),R.drawable.ic_app_arrow,null);
        drawableBack.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP);
        binding.imgBack.setImageDrawable(drawableBack);
        binding.imgBack.setOnClickListener(_OnClickListener);
        binding.btnSubmit.setOnClickListener(_OnClickListener);
        binding.btnLogin.setOnClickListener(_OnClickListener);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            onButtonClick(view);
        }
    };

    private void bouldObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        forgetPasswordRepository = new ForgetPasswordRepository();
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                 finish();
                 break;
            case R.id.btnSubmit:
                 if (isValid())
                 {
                     forgetPassword();
                 }
                 break;
            case R.id.btnLogin:
                 ActivityController.startNextActivity(this, LoginActivity.class, false);
                 finish();
                 break;
            case R.id.cnlMain:
                 CommonUtility.hideKeyboard(this);
                 break;
        }
    }

    private boolean isValid()
    {
        hideKeyBoard();
        if (binding.etEmail.getText().toString().isEmpty())
        {
            binding.etEmail.setError("Enter Your Email ID");
            binding.etEmail.requestFocus();
            return false;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(binding.etEmail.getText().toString()).matches())
        {
            binding.etEmail.setError("Enter A Valid Email ID");
            binding.etEmail.requestFocus();
            return false;
        }
        /*else if (binding.etMobileNumber.getText().toString().isEmpty())
        {
            binding.etMobileNumber.setError("Enter Mobile Number");
            binding.etMobileNumber.requestFocus();
            return false;
        }
        else if (!ValidationUtils.isValidMobileNo(binding.etMobileNumber.getText().toString().trim()))
        {
            binding.etMobileNumber.setError("Enter Valid Mobile Number");
            binding.etMobileNumber.requestFocus();
            return false;
        }*/
        return true;
    }

    public ForgetPasswordRequest getForgetPasswordPayloadApi()
    {
        ForgetPasswordRequest forgetPasswordRequest = new ForgetPasswordRequest();
        forgetPasswordRequest.setEmail(binding.etEmail.getText().toString());
        return forgetPasswordRequest;
    }

    private void forgetPassword()
    {
        //API
        mProgressDialog.show();
        forgetPasswordRepository.forgetPassword(getForgetPasswordPayloadApi()).subscribe(new SingleObserver<ForgetPasswordResponse>()
        {
            @Override
            public void onSubscribe(@NotNull Disposable d)
            {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(@NotNull ForgetPasswordResponse forgetPasswordResponse)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (forgetPasswordResponse.forgetPassword.errorDetails.getErrorCode() == 0)
                    {
                        DialogUtility.showMessageWithOkWithCallback(getString(R.string.dialog_title),
                                forgetPasswordResponse.forgetPassword.forgetPasswordResult.message,
                                ForgotPasswordActivity.this,new AlertDialogCallBack()
                                {
                                    @Override
                                    public void onSubmit()
                                    {
                                        onBackPressed();
                                    }

                                    @Override
                                    public void onCancel()
                                    {

                                    }
                                },true);
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(forgetPasswordResponse.forgetPassword.errorDetails.getErrorMessage(), ForgotPasswordActivity.this);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(@NotNull Throwable e)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, e.getMessage());

                if (!CommonUtilities.checkConnectivity(getApplicationContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable),ForgotPasswordActivity.this);
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), ForgotPasswordActivity.this);
            }
        });
    }

    private void hideKeyBoard()
    {
        InputMethodManager imm =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(binding.etEmail.getWindowToken(), 0);
    }


}
