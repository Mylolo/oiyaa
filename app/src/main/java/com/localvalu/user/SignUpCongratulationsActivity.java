package com.localvalu.user;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.localvalu.R;
import com.localvalu.databinding.ActivitySignUpCongratulationsBinding;
import com.localvalu.user.dashboard.ui.DashboardActivity;
import com.localvalu.user.dto.UserBasicInfo;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.CommonUtility;
import com.utility.PreferenceUtility;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

public class SignUpCongratulationsActivity extends AppCompatActivity
{
    //For API Call
    public ProgressDialog mProgressDialog;
    private String strNumberOfTokens = "0";
    private ActivitySignUpCongratulationsBinding binding;
    private int colorWhite;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeLogin);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up_congratulations);
        colorWhite = ContextCompat.getColor(getApplicationContext(),R.color.colorWhite);
        readFromBundle();
        setProperties();
        bouldObjectForHandlingAPI();
    }

    private void bouldObjectForHandlingAPI()
    {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    private void readFromBundle()
    {
        if (getIntent().getExtras() != null)
        {
            strNumberOfTokens = getIntent().getExtras().getString(AppUtils.BUNDLE_NEW_TOKEN_BALANCE, "0");
        }
    }

    private void setProperties()
    {
        Drawable drawableBack = ResourcesCompat.getDrawable(getResources(),R.drawable.ic_app_arrow,null);
        drawableBack.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP);
        binding.imgBack.setImageDrawable(drawableBack);

        binding.imgBack.setOnClickListener(_OnClickListener);
        binding.cnlMain.setOnClickListener(_OnClickListener);
        binding.btnGotoHome.setOnClickListener(_OnClickListener);
        UserBasicInfo userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(this, PreferenceUtility.APP_PREFERENCE_NAME);
        binding.tvReferralCode.setText(userBasicInfo.qRCodeOutput);
        StringBuilder strTokenBalance = new StringBuilder();
        strTokenBalance.append(getString(R.string.lbl_congratulations_msg_one)).append(" ")
                .append(getString(R.string.lbl_currency_letter_euro)).append(" ")
                .append(strNumberOfTokens).append(" ")
                .append(getString(R.string.lbl_congratulations_msg_two));
        binding.tvCongratsOne.setText(strTokenBalance.toString());
        try
        {
            binding.ivQRImage.setImageBitmap(encodeAsBitmap(userBasicInfo.qRCodeOutput));
        }
        catch (WriterException e)
        {
            e.printStackTrace();
        }
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            onButtonClick(view);
        }
    };

    Bitmap encodeAsBitmap(String str) throws WriterException
    {
        BitMatrix result;
        try
        {
            result = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, 120, 120, null);
        }
        catch (IllegalArgumentException iae)
        {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++)
        {
            int offset = y * w;
            for (int x = 0; x < w; x++)
            {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 120, 0, 0, w, h);
        return bitmap;
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                 finish();
                 break;
            case R.id.btnGotoHome:
                 ActivityController.startNextActivity(this, SplashInfoActivity.class, false);
                 break;
            case R.id.linearLayout_login:
                 ActivityController.startNextActivity(this, LoginActivity.class, false);
                 finish();
                 break;
            case R.id.cnlMain:
                 CommonUtility.hideKeyboard(this);
                 break;
        }
    }

}
