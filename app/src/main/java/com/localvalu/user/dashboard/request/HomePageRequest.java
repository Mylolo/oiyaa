package com.localvalu.user.dashboard.request;

public class HomePageRequest
{
    private String Token;

    public String getToken()
    {
        return Token;
    }

    public void setToken(String token)
    {
        Token = token;
    }
}
