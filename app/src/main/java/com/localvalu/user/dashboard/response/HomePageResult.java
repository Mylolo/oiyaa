package com.localvalu.user.dashboard.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomePageResult
{
    @SerializedName("ErrorDetails")
    @Expose
    private ErrorDetails errorDetails;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("msg")
    @Expose
    private String message;

    @SerializedName("HomepageDataList")
    @Expose
    private List<HomePageDataList> homePageDataLists;

    public ErrorDetails getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public List<HomePageDataList> getHomePageDataLists()
    {
        return homePageDataLists;
    }

    public void setHomePageDataLists(List<HomePageDataList> homePageDataLists)
    {
        this.homePageDataLists = homePageDataLists;
    }

}
