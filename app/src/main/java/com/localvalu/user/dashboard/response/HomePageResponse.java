package com.localvalu.user.dashboard.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomePageResponse
{
    @SerializedName("HomepageResult")
    @Expose
    private HomePageResult homePageResult;

    public HomePageResult getHomePageResult()
    {
        return homePageResult;
    }

    public void setHomePageResult(HomePageResult homePageResult)
    {
        this.homePageResult = homePageResult;
    }
}
