package com.localvalu.user.dashboard.ui;

import android.app.ProgressDialog;
import androidx.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import com.localvalu.R;
import com.localvalu.base.MainActivity;
import com.localvalu.databinding.ActivityDashboardBinding;
import com.localvalu.user.TermsAndConditionActivity;
import com.localvalu.user.dashboard.request.HomePageRequest;
import com.localvalu.user.dashboard.response.HomePageDataList;
import com.localvalu.user.dashboard.response.HomePageResponse;
import com.localvalu.user.dashboard.response.HomePageResult;
import com.localvalu.user.repository.DashboardRepository;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.DialogUtility;
import com.utility.MarshMallowPermission;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class DashboardActivity extends AppCompatActivity
{

    //For API Call
    public ProgressDialog mProgressDialog;

    private ActivityDashboardBinding binding;

    private DashboardRepository dashboardRepository;

    private List<HomePageDataList> homePageDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_dashboard);
        initProgressDialog();
        dashboardRepository = new DashboardRepository();
        //fetchHomeContent();
        binding.constraintLayoutEatsDashboard.setOnClickListener(_OnClickListener);
        binding.constraintLayoutLocalDashboard.setOnClickListener(_OnClickListener);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            onButtonClick(view);
        }
    };

    private void fetchHomeContent()
    {
        mProgressDialog.show();
        dashboardRepository.getHomePageContent(getHomePageRequestPayload())
                .subscribe(new Observer<HomePageResponse>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {

                    }

                    @Override
                    public void onNext(HomePageResponse homePageResponse)
                    {
                        if(mProgressDialog!=null) mProgressDialog.dismiss();
                        if(homePageResponse!=null)
                        {
                            HomePageResult result = homePageResponse.getHomePageResult();

                            if(result!=null)
                            {
                                if(result.getErrorDetails().getErrorCode()==0)
                                {
                                    homePageDataList = result.getHomePageDataLists();

                                    if(homePageDataList.size()>0)
                                    {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                                        {

                                        }
                                        else
                                        {

                                        }

                                    }
                                }
                                else
                                {
                                    DialogUtility.showMessageWithOk(result.getErrorDetails().getErrorMessage(),
                                            DashboardActivity.this);
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e)
                    {
                        if(mProgressDialog!=null) mProgressDialog.dismiss();
                        DialogUtility.showMessageWithOk(getString(R.string.server_alert),
                                DashboardActivity.this);
                    }

                    @Override
                    public void onComplete()
                    {
                        if(mProgressDialog!=null) mProgressDialog.dismiss();
                    }
                });
    }

    public HomePageRequest getHomePageRequestPayload()
    {
        HomePageRequest homePageRequest=new HomePageRequest();
        homePageRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        return homePageRequest;
    }

    private void initProgressDialog()
    {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    public void onButtonClick(View view)
    {
        boolean termsAndConditionViewed = true;//PreferenceUtility.getBooleanFromPreference(this, PreferenceUtility.TERMS_AND_CONDITION);

        Bundle bundle = new Bundle();
        bundle.putBoolean(AppUtils.BUNDLE_IS_FROM_LOGIN_PAGE,false);
        switch (view.getId())
        {
            case R.id.imgBack:
                 //onBackPressed();
                 break;
            case R.id.constraintLayoutLocalDashboard:
                 bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_LOCAL);
                 ActivityController.startNextActivity(this, termsAndConditionViewed ? MainActivity.class : TermsAndConditionActivity.class, bundle, false);
                 break;
            case R.id.constraintLayoutEatsDashboard:
                 bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_EATS);
                 ActivityController.startNextActivity(this, termsAndConditionViewed ? MainActivity.class : TermsAndConditionActivity.class, bundle, false);
                 break;
        }
    }
}
