package com.localvalu.user.dashboard.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomePageDataList
{
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("page_key")
    @Expose
    private String page_key;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getPage_key()
    {
        return page_key;
    }

    public void setPage_key(String page_key)
    {
        this.page_key = page_key;
    }
}
