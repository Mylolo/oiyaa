package com.localvalu.user.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.base.Resource
import com.localvalu.common.model.SearchRequest
import com.localvalu.common.model.SearchResponse
import com.localvalu.home.repository.SearchListRepository
import com.localvalu.registration.model.UserExistRequest
import com.localvalu.registration.repository.PassionListRepository
import com.localvalu.registration.repository.UserExistRepository
import com.localvalu.user.dto.PassionListRequest
import com.localvalu.user.model.LoginRequest
import com.localvalu.user.model.LoginResponse
import com.localvalu.user.model.User
import com.localvalu.user.repository.UserRepository
import com.localvalu.user.wrapper.PassionListResultWrapper
import com.localvalu.user.wrapper.RegisterExistsCustomerResultWrapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(private val repository:UserRepository) : ViewModel()
{
    val user:MutableLiveData<User> = MutableLiveData()

    val loading:MutableLiveData<Boolean> = MutableLiveData()

    val failure:MutableLiveData<Resource.Failure> = MutableLiveData()

    fun getUserInfo(loginRequest: LoginRequest)= viewModelScope.launch {
        repository.getUser(loginRequest).collect{
            when(it)
            {
                is Resource.Loading ->
                {
                    loading.value = true
                }
                is Resource.Success ->
                {
                    user.value = it.value
                }
                is Resource.Failure ->
                {
                    failure.value = it
                }
            }
        }
    }

}