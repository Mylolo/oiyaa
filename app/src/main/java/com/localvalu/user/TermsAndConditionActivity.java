package com.localvalu.user;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;
import androidx.databinding.DataBindingUtil;

import android.view.View;

import com.localvalu.R;
import com.localvalu.base.MainActivity;
import com.localvalu.databinding.ActivityTermsAndConditionBinding;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.PreferenceUtility;

public class TermsAndConditionActivity extends AppCompatActivity
{
    private int navigationType;
    private boolean isFromLogin;
    private ActivityTermsAndConditionBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppThemeLogin);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_terms_and_condition);
        readFromBundle();
        setProperties();
    }

    private void readFromBundle()
    {
        try
        {
            navigationType = getIntent().getExtras().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
            isFromLogin  = getIntent().getExtras().getBoolean(AppUtils.BUNDLE_IS_FROM_LOGIN_PAGE);
        }
        catch (Exception e)
        {
            navigationType = 0;
        }
    }

    private void setProperties()
    {
        binding.tvTermsAndConditions.setText(HtmlCompat.fromHtml(getString(R.string.terms_and_condition),HtmlCompat.FROM_HTML_MODE_LEGACY));
        if(isFromLogin)binding.btnActionTermsAndConditions.setText(getString(R.string.lbl_done));
        else binding.btnActionTermsAndConditions.setText(getString(R.string.lbl_continue));
        binding.btnActionTermsAndConditions.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                PreferenceUtility.saveBooleanInPreference(TermsAndConditionActivity.this, PreferenceUtility.TERMS_AND_CONDITION, true);

                if(isFromLogin)
                {
                    finish();
                    return;
                }
                else
                {
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, navigationType);

                    boolean isClearActivityStack = (navigationType==AppUtils.BMT_MALL) ? false : true;
                    ActivityController.startNextActivity(TermsAndConditionActivity.this, MainActivity.class, bundle, isClearActivityStack);
                }
            }
        });
    }

}
