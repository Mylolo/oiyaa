package com.localvalu.locationservices

import android.app.Service;
import android.content.Context
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.utility.AppUtils


private val TAG = LocationUpdatesService::class.java.simpleName



/**
 * The desired interval for location updates. Inexact. Updates may be more or less frequent.
 */
private const val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000

/**
 * The fastest rate for active location updates. Updates will never be more frequent
 * than this value.
 */
private const val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2

/**
 * Used to check whether the bound activity has really gone away and not unbound as part of an
 * orientation change. We create a foreground service notification only if the former takes
 * place.
 */
private var mChangingConfiguration = false

/**
 * Contains parameters used by [com.google.android.gms.location.FusedLocationProviderApi].
 */
private var mLocationRequest: LocationRequest? = null

/**
 * Provides access to the Fused Location Provider API.
 */
private var mFusedLocationClient: FusedLocationProviderClient? = null

/**
 * Callback for changes in location.
 */
private var mLocationCallback: LocationCallback? = null

private var mServiceHandler: Handler? = null

/**
 * The current location.
 */
private var mLocation: Location? = null


class LocationUpdatesService(val context: Context)
{
    init
    {
        Log.d(TAG,"onCreate")
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
        mLocationCallback = object : LocationCallback()
        {
            override fun onLocationResult(locationResult: LocationResult)
            {
                super.onLocationResult(locationResult)
                onNewLocation(locationResult.lastLocation)
            }
        }
        createLocationRequest()
    }

    /**
     * Makes a request for location updates. Note that in this sample we merely log the
     * [SecurityException].
     */
    fun requestLocationUpdates()
    {
        Log.i(TAG, "Requesting location updates")
        try
        {
            mFusedLocationClient!!.requestLocationUpdates(
                mLocationRequest,
                mLocationCallback, Looper.myLooper()
            )
        }
        catch (unlikely: SecurityException)
        {
            Log.e(TAG, "Lost location permission. Could not request updates. $unlikely")
        }
    }

    /**
     * Removes location updates. Note that in this sample we merely log the
     * [SecurityException].
     */
    fun removeLocationUpdates()
    {
        Log.i(TAG, "Removing location updates")
        try
        {
            mFusedLocationClient!!.removeLocationUpdates(mLocationCallback)
        }
        catch (unlikely: SecurityException)
        {
            Log.e(TAG, "Lost location permission. Could not remove updates. $unlikely")
        }
    }

    private fun getLastLocation()
    {
        try
        {
            mFusedLocationClient!!.lastLocation
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null)
                    {
                        mLocation = task.result
                    }
                    else
                    {
                        Log.w(TAG, "Failed to get location.")
                    }
                }
        }
        catch (unlikely: SecurityException)
        {
            Log.e(TAG, "Lost location permission.$unlikely")
        }
    }

    private fun onNewLocation(location: Location)
    {
        Log.i(TAG, "New location: $location")
        mLocation = location

        // Notify anyone listening for broadcasts about the new location.
        LocationRepository.locationUpdate.value=(location)
    }

    /**
     * Sets the location request parameters.
     */
    private fun createLocationRequest()
    {
        mLocationRequest = LocationRequest()
        mLocationRequest?.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest?.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    companion object {
        @Volatile private var mInstance: LocationUpdatesService? = null

         fun get(context: Context): LocationUpdatesService =
            mInstance ?: synchronized(this) {
                val newInstance = mInstance ?: LocationUpdatesService(context).also { mInstance = it }
                newInstance
            }
    }
}
