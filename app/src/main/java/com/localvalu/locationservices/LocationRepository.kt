package com.localvalu.locationservices

import android.content.Context
import android.content.Intent
import android.location.Location
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.LocationServices
import com.utility.AppUtils

object LocationRepository
{
    val locationUpdate: MutableLiveData<Location> = MutableLiveData()

    fun startService(context: Context)
    {
        LocationUpdatesService.get(context).requestLocationUpdates()
    }

    fun stopService(context: Context)
    {
        LocationUpdatesService.get(context).requestLocationUpdates()
    }
}