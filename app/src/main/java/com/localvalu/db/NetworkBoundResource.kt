package com.localvalu.db

import com.localvalu.base.Resource
import kotlinx.coroutines.flow.*
import retrofit2.HttpException

inline fun <ResultType, RequestType> networkBoundResource(
    crossinline query: () -> Flow<ResultType>,
    crossinline fetch: suspend () -> RequestType,
    crossinline saveFetchResult: suspend (RequestType) -> Unit,
    crossinline shouldFetch: (ResultType) -> Boolean = { true }
) = flow {
    val data = query().first()

    val flow = if (shouldFetch(data))
    {
        emit(Resource.Loading)

        try
        {
            saveFetchResult(fetch())
            query().map { Resource.Success(it) }
        }
        catch (throwable: Throwable)
        {
            when (throwable)
            {
                is HttpException ->
                {
                    query().map {
                        Resource.Failure(
                            false,
                            throwable.code(),
                            throwable.response()?.errorBody()
                        )
                    }
                }
                else             ->
                {
                    query().map { Resource.Failure(true, null, null) }
                }
            }

        }
    }
    else
    {
        query().map { Resource.Success(it) }
    }

    emitAll(flow)
}



