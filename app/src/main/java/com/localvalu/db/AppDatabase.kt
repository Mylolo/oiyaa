package com.localvalu.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.localvalu.user.db.UserDao
import com.localvalu.user.model.User

@Database(entities = [User::class],version = 1)
abstract class AppDatabase : RoomDatabase()
{
    abstract fun userDao(): UserDao
}