package com.localvalu.directory.listing.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessListDto implements Serializable
{
    @SerializedName("business_id")
    @Expose
    public String businessId;

    @SerializedName("firstname")
    @Expose
    public String firstname;

    @SerializedName("lastname")
    @Expose
    public String lastname;

    @SerializedName("retailerId")
    @Expose
    public String retailerId;

    @SerializedName("tradingname")
    @Expose
    public String tradingname;

    @SerializedName("BusinessType")
    @Expose
    public String businessType;

    @SerializedName("RetailerType")
    @Expose
    public String retailerType;

    @SerializedName("logo")
    @Expose
    public String logo;

    @SerializedName("Website")
    @Expose
    public String website;

    @SerializedName("contact_person_name")
    @Expose
    public String contact_person_name;

    @SerializedName("contact_person_Email")
    @Expose
    public String contact_person_Email;

    @SerializedName("contact_person_phone")
    @Expose
    public String contact_person_phone;

    @SerializedName("address1")
    @Expose
    public String address1;

    @SerializedName("address2")
    @Expose
    public String address2;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("country")
    @Expose
    public String country;

    @SerializedName("postcode")
    @Expose
    public String postcode;

    @SerializedName("about")
    @Expose
    public String about;

    @SerializedName("timings")
    @Expose
    public String timings;

    @SerializedName("SplitTimings")
    @Expose
    public String splitTimings;

    @SerializedName("categories")
    @Expose
    public String categories;

    @SerializedName("typeoffoods")
    @Expose
    public String typeoffoods;

    @SerializedName("minOrderVall")
    @Expose
    public String minOrderVall;

    @SerializedName("lat")
    @Expose
    public String lat;

    @SerializedName("lng")
    @Expose
    public String lng;

    @SerializedName("CurrentPromotionValue")
    @Expose
    public String currentPromotionValue;

    @SerializedName("Is_Active")
    @Expose
    public String isActive;

    @SerializedName("OverAllreviewCount")
    @Expose
    public String overAllreviewCount;

    @SerializedName("OverAllreviewRate")
    @Expose
    public String overAllreviewRate;

    @SerializedName("OverAllProductRate")
    @Expose
    public String overAllProductRate;

    @SerializedName("OverAllServiceRate")
    @Expose
    public String overAllServiceRate;

    @SerializedName("BookingTypeId")
    @Expose
    public String bookingTypeId;

    public String getLogo(String type)
    {
        String retVal = logo;

        /*if (logo.equals(""))
        {
            if (type.toLowerCase().equals("eats"))
            {
                retVal = "https://www.localvalu.com/lolo-eats/assets/images/no-image.png";
            }
            else if (type.toLowerCase().equals("local"))
            {
                retVal = "https://www.localvalu.com/assets/images/no-image.png";
            }
        }*/

        return retVal;
    }
}
