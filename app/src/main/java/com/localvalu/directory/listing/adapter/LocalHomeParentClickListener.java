package com.localvalu.directory.listing.adapter;

import com.localvalu.directory.listing.dto.BusinessListDto;

import java.util.ArrayList;

public interface LocalHomeParentClickListener
{
    void onViewAllButtonClicked(ArrayList<BusinessListDto> items, int parentPosition);
}
