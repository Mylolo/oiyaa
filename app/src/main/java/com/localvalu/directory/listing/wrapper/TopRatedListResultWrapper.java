package com.localvalu.directory.listing.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.listing.dto.BusinessListResult;

import java.io.Serializable;

public class TopRatedListResultWrapper implements Serializable {

    @SerializedName("CompleteSearchResult")
    @Expose
    public BusinessListResult businessListResult;
}
