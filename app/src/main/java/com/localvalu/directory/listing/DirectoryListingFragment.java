package com.localvalu.directory.listing;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentDirectoryListingBinding;
import com.localvalu.directory.detail.DirectoryDetailFragment;
import com.localvalu.directory.listing.adapter.DirectoryBusinessListAdapter;
import com.localvalu.directory.listing.dto.BusinessListDto;
import com.localvalu.directory.listing.wrapper.DashboardListResultWrapper;
import com.localvalu.eats.listing.model.BusinessViewAll;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.PreferenceUtility;
import com.utility.VerticalSpacingItemDecoration;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DirectoryListingFragment extends BaseFragment
{
    private static final String TAG = "DirectoryListingFragment";

    private FragmentDirectoryListingBinding binding;
    private int colorEats;

    private UserBasicInfo userBasicInfo;
    private GridLayoutManager gridLayoutManager;
    private DirectoryBusinessListAdapter directoryBusinessListAdapter;
    private ArrayList<BusinessListDto> businessList = new ArrayList<>();
    private BusinessViewAll businessViewAll;
    private int verticalItemSpace;
    private HostActivityListener hostActivityListener;
    private ApiInterface apiInterface;

    public static DirectoryListingFragment newInstance(ArrayList<BusinessListDto> businessList, BusinessViewAll businessViewAll)
    {
        DirectoryListingFragment directoryListingFragment = new DirectoryListingFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppUtils.BUNDLE_BUSINESS_LIST,businessList);
        bundle.putSerializable(AppUtils.BUNDLE_BUSINESS_VIEW_ALL,businessViewAll);
        directoryListingFragment.setArguments(bundle);
        return directoryListingFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        verticalItemSpace = (int) getResources().getDimension(R.dimen._10sdp);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        readFromBundle();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentDirectoryListingBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
    }

    private void readFromBundle()
    {
        businessList = (ArrayList<BusinessListDto>) getArguments().getSerializable(AppUtils.BUNDLE_BUSINESS_LIST);
        businessViewAll = (BusinessViewAll) getArguments().getSerializable(AppUtils.BUNDLE_BUSINESS_VIEW_ALL);
    }

    private void setProperties()
    {
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        binding.rvList.setLayoutManager(gridLayoutManager);
        binding.rvList.addItemDecoration(new VerticalSpacingItemDecoration(verticalItemSpace));
        binding.layoutError.btnRetry.setOnClickListener(_OnClickListener);
        directoryBusinessListAdapter = new DirectoryBusinessListAdapter();
        directoryBusinessListAdapter.setOnClickListener(new DirectoryBusinessListAdapter.OnItemClickListener()
        {
            @Override
            public void onItemCall(final int position,final BusinessListDto businessListDto)
            {
                Log.d(TAG, "Phone : " + businessListDto.contact_person_phone);
                if (!businessListDto.contact_person_phone.equals(""))
                {
                    Intent callIntent = new Intent(Intent.ACTION_DIAL);
                    callIntent.setData(Uri.parse("tel:" +businessListDto.contact_person_phone));
                    startActivity(callIntent);
                }
            }

            @Override
            public void onItemClick(final int position,final BusinessListDto businessListDto)
            {
                Bundle bundle = new Bundle();
                bundle.putString("businessID", businessListDto.businessId);
                DirectoryDetailFragment directoryDetailFragment = DirectoryDetailFragment.newInstance();
                directoryDetailFragment.setArguments(bundle);
                hostActivityListener.updateFragment(directoryDetailFragment, true, DirectoryDetailFragment.class.getName(), true);
            }
        });
        binding.rvList.setAdapter(directoryBusinessListAdapter);
        if(businessList==null)
        {
            if(businessViewAll!=null)
            {
                if(!businessViewAll.isFromAllFilter())
                {
                    binding.tabLayout.tvTitle.setText(businessViewAll.getHeader());
                    fetchAllBusiness();
                }
            }
            else
            {
                showError(getString(R.string.lbl_no_business_found),false);
            }
        }
        else
        {
            setListView();
        }
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.btnRetry:
                    fetchAllBusiness();
                    break;
            }
        }
    };

    private void setListView()
    {
        if (businessList.size() > 0)
        {
            updateAdapter(businessList);
            showList();
        }
        else
        {
            showError(getString(R.string.lbl_no_business_found),false);
        }
    }

    private void setUpActionBar()
    {
        binding.tabLayout.tvTitle.setTextColor(colorEats);
        binding.tabLayout.toolbar.getNavigationIcon().setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP);
        binding.tabLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });
        if(businessList==null && businessViewAll!=null)
        {
            binding.tabLayout.tvTitle.setText(businessViewAll.getHeader());
        }
        else
        {
            binding.tabLayout.tvTitle.setText(getString(R.string.lbl_back));
        }
    }

    private void fetchAllBusiness()
    {
        showProgress();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("type",AppUtils.BMT_LOCAL);
            objMain.put("requestfor",businessViewAll.getViewResultType());
            if(businessViewAll.getViewResultType()==AppUtils.NEARBY)
            {
                objMain.put("Latitude", businessViewAll.getLatitude());
                objMain.put("Longitude", businessViewAll.getLongitude());
            }
            //objMain.put("Latitude", businessViewAll.getLatitude());
            //objMain.put("Longitude", businessViewAll.getLongitude());
            //objMain.put("Latitude", 51.5125299);
            //objMain.put("Longitude", -0.1269216);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<DashboardListResultWrapper> dtoCall = apiInterface.dashboardListResult(body);

        dtoCall.enqueue(new Callback<DashboardListResultWrapper>()
        {
            @Override
            public void onResponse(Call<DashboardListResultWrapper> call, Response<DashboardListResultWrapper> response)
            {
                try
                {
                    if (response.body().getWebsiteComplete.errorDetails.errorCode == 0)
                    {
                        updateAdapter(response.body().getWebsiteComplete.businessListDto);
                        showList();
                    }
                    else
                    {
                        showError(response.body().getWebsiteComplete.errorDetails.errorMessage,false);
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<DashboardListResultWrapper> call, Throwable t)
            {
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(requireContext()))
                    showError(getString(R.string.network_unavailable),true);
                else
                    showError(getString(R.string.server_alert),true);
            }
        });
    }

    private void updateAdapter(final ArrayList<BusinessListDto> businessList)
    {
        directoryBusinessListAdapter.submitList(businessList);
    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.rvList.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showList()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvList.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage, boolean retry)
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvList.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if (retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

}
