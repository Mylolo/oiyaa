package com.localvalu.directory.listing.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;
import java.util.ArrayList;

public class BusinessListResult implements Serializable {

    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("BusinessList")
    @Expose
    public ArrayList<BusinessListDto> businessList;
}
