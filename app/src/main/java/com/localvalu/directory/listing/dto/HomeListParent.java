package com.localvalu.directory.listing.dto;

import com.localvalu.common.model.cuisinescategory.CuisineCategoryData;

import java.util.ArrayList;

public class HomeListParent
{
    public int headerId;

    public String strHeader;

    public ArrayList<BusinessListDto> businessList;

    public ArrayList<CuisineCategoryData> cuisinesCategoryDataArrayList;

    public int viewType;

    public int viewTypeResult;

}
