package com.localvalu.directory.listing.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CompleteSearchResultDto
{

    @SerializedName("LocalResults")
    @Expose
    public ArrayList<BusinessListDto> businessList;

    @SerializedName("PopularResults")
    @Expose
    public ArrayList<BusinessListDto> popularList;

    @SerializedName("NearByresults")
    @Expose
    public ArrayList<BusinessListDto> nearbyList;

    @SerializedName("TopRatedResults")
    @Expose
    public ArrayList<BusinessListDto> topRatedList;
}
