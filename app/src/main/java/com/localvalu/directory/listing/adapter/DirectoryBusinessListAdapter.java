package com.localvalu.directory.listing.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.directory.listing.dto.BusinessListDto;
import com.localvalu.eats.listing.adapter.EatsBusinessListAdapter;
import com.squareup.picasso.Picasso;
import com.utility.AppUtils;

import java.util.ArrayList;

public class DirectoryBusinessListAdapter extends ListAdapter<BusinessListDto, DirectoryBusinessListAdapter.ViewHolder>
{
    public static final String TAG = DirectoryBusinessListAdapter.class.getSimpleName();
    private Context context;
    private OnItemClickListener onClickListener;
    private ArrayList<BusinessListDto> businessList;
    private int colorBlack;

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.context = recyclerView.getContext();
        colorBlack = context.getResources().getColor(R.color.colorBlack);
        colorBlack = context.getResources().getColor(R.color.colorBlack);
    }

    public DirectoryBusinessListAdapter()
    {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<BusinessListDto> DIFF_CALLBACK = new DiffUtil.ItemCallback<BusinessListDto>()
    {
        @Override
        public boolean areItemsTheSame(@NonNull BusinessListDto oldItem, @NonNull BusinessListDto newItem)
        {
            return oldItem.businessId == newItem.businessId;
        }

        @Override
        public boolean areContentsTheSame(@NonNull BusinessListDto oldItem, @NonNull BusinessListDto newItem)
        {
            return oldItem.businessId.equals(newItem.businessId);
        }
    };

    @NonNull
    @Override
    public DirectoryBusinessListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_directory_business_list, viewGroup, false);
        return new DirectoryBusinessListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DirectoryBusinessListAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int position)
    {
        BusinessListDto businessListDto = getItem(position);
        viewHolder.ivOrderOnline.getDrawable().setColorFilter(colorBlack, PorterDuff.Mode.SRC_ATOP);
        viewHolder.ivOrderAtTable.getDrawable().setColorFilter(colorBlack, PorterDuff.Mode.SRC_ATOP);
        viewHolder.ivOrderAtStore.getDrawable().setColorFilter(colorBlack, PorterDuff.Mode.SRC_ATOP);

        if (!businessListDto.getLogo("local").trim().equals(""))
        {
            Picasso.with(context).load(businessListDto.getLogo("local"))
                    .placeholder(R.drawable.progress_animation)
                    .fit()
                    .centerCrop()
                    .error(R.drawable.app_logo).into(viewHolder.ivCoverImage);
            viewHolder.ivCoverImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
            viewHolder.ivCoverImage.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
        }

        else
        {
            viewHolder.ivCoverImage.setImageResource(R.drawable.app_logo);
            viewHolder.ivCoverImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            viewHolder.ivCoverImage.setBackgroundColor(context.getResources().getColor(R.color.colorEats));
        }


        viewHolder.tvName.setText(businessListDto.tradingname);
        viewHolder.tvDesc.setText(getAddress(businessListDto));
        viewHolder.tvDeliveryTime.setText("Delivery Time - 35 Mins");
        viewHolder.tvCurrentPromotion.setText(businessListDto.currentPromotionValue + "% OFF");

        if(businessListDto.bookingTypeId!=null)
        {
            boolean orderOnlineAvailable = isOrderNow(businessListDto.bookingTypeId);
            boolean orderAtTableAvailable = isOrderAtTable(businessListDto.bookingTypeId);
            boolean orderAtStoreAvailable = isOrderAtStore(businessListDto.bookingTypeId);
            if(orderOnlineAvailable) viewHolder.ivOrderOnline.setVisibility(View.VISIBLE);
            else viewHolder.ivOrderOnline.setVisibility(View.INVISIBLE);
            if(orderAtTableAvailable) viewHolder.ivOrderAtTable.setVisibility(View.VISIBLE);
            else viewHolder.ivOrderAtTable.setVisibility(View.INVISIBLE);
            if(orderAtStoreAvailable) viewHolder.ivOrderAtStore.setVisibility(View.VISIBLE);
            else viewHolder.ivOrderAtStore.setVisibility(View.INVISIBLE);
        }

        viewHolder.rlHeader.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemClick(position,businessListDto);
            }
        });
        viewHolder.ivCall.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemCall(position,businessListDto);
            }
        });
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemCall(int position,BusinessListDto businessListDto);

        void onItemClick(int position,BusinessListDto businessListDto);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        RelativeLayout rlHeader;
        ImageView ivCall,ivCoverImage;
        TextView tvName,tvDesc,tvCurrentPromotion,tvDeliveryTime;
        TextView tvAvailableOptions;
        AppCompatImageView ivOrderOnline,ivOrderAtTable,ivOrderAtStore;

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            rlHeader = itemView.findViewById(R.id.rlHeader);
            ivCall = itemView.findViewById(R.id.ivCall);
            ivCoverImage = itemView.findViewById(R.id.ivCoverImage);
            tvName = itemView.findViewById(R.id.tvName);
            tvDesc = itemView.findViewById(R.id.tvDesc);
            tvCurrentPromotion = itemView.findViewById(R.id.tvCurrentPromotion);
            tvDeliveryTime = itemView.findViewById(R.id.tvDeliveryTime);
            tvAvailableOptions = itemView.findViewById(R.id.tvAvailableOptions);
            ivOrderOnline = itemView.findViewById(R.id.ivOrderOnline);
            ivOrderAtTable = itemView.findViewById(R.id.ivOrderAtTable);
            ivOrderAtStore = itemView.findViewById(R.id.ivOrderAtStore);
            ivOrderOnline.setVisibility(View.GONE);
            ivOrderAtTable.setVisibility(View.GONE);
            ivOrderAtStore.setVisibility(View.GONE);
        }
    }

    private boolean isOrderNow(String bookingTypeId)
    {
        return bookingTypeId.contains(AppUtils.ORDER_ONLINE);
    }

    private boolean isOrderAtTable(String bookingTypeId)
    {
        return bookingTypeId.contains(AppUtils.ORDER_AT_TABLE);
    }

    private boolean isOrderAtStore(String bookingTypeId)
    {
        return bookingTypeId.contains(AppUtils.ORDER_AT_STORE);
    }

    private void setAvailableOptionsText(TextView textView,BusinessListDto businessListDto)
    {
        StringBuilder strOrderOnlineAvailableOptions = new StringBuilder();
        boolean orderOnlineAvailable = isOrderNow(businessListDto.bookingTypeId);
        boolean orderAtTableAvailable = isOrderAtTable(businessListDto.bookingTypeId);
        if(orderOnlineAvailable)
        {
            strOrderOnlineAvailableOptions.append(context.getString(R.string.lbl_order_online)).append(" : ").append(context.getString(R.string.lbl_available)).append(" | ");
        }
        else
        {
            strOrderOnlineAvailableOptions.append(context.getString(R.string.lbl_order_online)).append(" : ").append(context.getString(R.string.lbl_unavailable)).append(" | ");
        }
        if(orderAtTableAvailable)
        {
            strOrderOnlineAvailableOptions.append(context.getString(R.string.lbl_order_at_table)).append(" : ").append(context.getString(R.string.lbl_available));
        }
        else
        {
            strOrderOnlineAvailableOptions.append(context.getString(R.string.lbl_order_at_table)).append(" : ").append(context.getString(R.string.lbl_unavailable));
        }
        textView.setText(strOrderOnlineAvailableOptions.toString());
    }

    private String getAddress(BusinessListDto businessListDto)
    {
        StringBuilder strAddress = new StringBuilder();
        if (businessListDto.address1.isEmpty())
        {
            strAddress.append(businessListDto.address2.trim());
            strAddress.append(" ");
        }
        else
        {
            strAddress.append(businessListDto.address1.trim());
            strAddress.append(" ");
            if(!businessListDto.address2.trim().isEmpty())
            {
                strAddress.append(businessListDto.address2.trim());
                strAddress.append(" ");
            }
        }
        if (!businessListDto.city.isEmpty())
        {
            strAddress.append(businessListDto.city.trim());
            strAddress.append(" ");
        }
        if (!businessListDto.postcode.isEmpty())
        {
            strAddress.append(businessListDto.postcode.trim());
        }
        return strAddress.toString();
    }

}
