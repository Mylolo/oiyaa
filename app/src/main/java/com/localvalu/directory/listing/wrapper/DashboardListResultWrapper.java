package com.localvalu.directory.listing.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.listing.dto.CompleteSearchResult;
import com.localvalu.directory.listing.dto.GetWebsiteComplete;

import java.io.Serializable;

public class DashboardListResultWrapper implements Serializable {
    @SerializedName("GetWebsiteComplete")
    @Expose
    public GetWebsiteComplete getWebsiteComplete;
}
