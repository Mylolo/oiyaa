package com.localvalu.directory.listing.adapter;

import com.localvalu.directory.listing.dto.BusinessListDto;

public interface LocalHomeChildClickListener
{
    void onItemClicked(BusinessListDto businessListDto, int childPosition);
}
