package com.localvalu.directory.listing.adapter;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.common.model.cuisinescategory.CuisineCategoryData;

import java.util.ArrayList;


public class DirectoryBusinessCategoryAdapter extends RecyclerView.Adapter<DirectoryBusinessCategoryAdapter.ViewHolder>
{
    public static final String TAG = DirectoryBusinessCategoryAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<CuisineCategoryData> cuisinesCategoryListResult;
    private OnItemClickListener onClickListener;

    public DirectoryBusinessCategoryAdapter(Context context, ArrayList<CuisineCategoryData> cuisinesCategoryListResult)
    {
        this.context = context;
        this.cuisinesCategoryListResult = cuisinesCategoryListResult;
    }

    @NonNull
    @Override
    public DirectoryBusinessCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_directory_business_category, viewGroup, false);
        return new DirectoryBusinessCategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DirectoryBusinessCategoryAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") int i)
    {
        //in some cases, it will prevent unwanted situations
        viewHolder.img_check_pic.setOnCheckedChangeListener(null);
        /*if (!sectorsList.get(i).image.trim().equals(""))
        {
            Picasso.with(context)
                    .load(sectorsList.get(i).image)
                    .transform(ImageTransformation.getTransformation(viewHolder.img_category))
                    .placeholder(R.drawable.progress_animation)
            .error(R.color.colorWhite).into(viewHolder.img_category);
        }
        else
        {
            viewHolder.img_category.setImageDrawable(ContextCompat.getDrawable(context,R.color.colorWhite));
        }*/
        viewHolder.tv_category.setText(cuisinesCategoryListResult.get(i).getName());
        if (cuisinesCategoryListResult.get(i).checkStatus == true)
        {
            viewHolder.img_check_pic.setChecked(true);
        }
        else
        {
            viewHolder.img_check_pic.setChecked(false);
        }
        viewHolder.img_check_pic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                buttonView.postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        updateStatus(i, isChecked);
                    }
                }, 100);

            }
        });
    }

    @Override
    public int getItemCount()
    {
        return cuisinesCategoryListResult.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ImageView img_category = itemView.findViewById(R.id.img_category);
        AppCompatCheckBox img_check_pic = itemView.findViewById(R.id.img_check_pic);
        TextView tv_category = itemView.findViewById(R.id.tv_category);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }

    private void updateStatus(int position, boolean isChecked)
    {
        CuisineCategoryData cuisineCategoryData = cuisinesCategoryListResult.get(position);

        CuisineCategoryData newCuisineCategoryData = cuisineCategoryData;

        newCuisineCategoryData.checkStatus = isChecked;

        cuisinesCategoryListResult.remove(position);

        cuisinesCategoryListResult.add(position, newCuisineCategoryData);

        notifyDataSetChanged();
    }
}