package com.localvalu.directory.webview;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.localvalu.R;

public class DirectoryWebviewActivity extends AppCompatActivity {

    private WebView webView;
    ImageView close;
    private TextView title;
    String getTitle = "", setURL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory_webview);
        if (getIntent() != null) {
            getTitle = getIntent().getExtras().getString("getTitle");
            setURL = getIntent().getExtras().getString("setURL");
        }
        init();
    }

    private void init() {
        close = (ImageView) findViewById(R.id.close);
        webView = (WebView) findViewById(R.id.webview);
        title = (TextView) findViewById(R.id.title);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(setURL);
        webView.setWebViewClient(new WebViewClient());
        title.setText(getTitle);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else super.onBackPressed();
    }
}
