package com.localvalu.directory.detail;

import static com.localvalu.extensions.WidgetExtensionsKt.updatePagerHeightForChild;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.MainActivity;
import com.localvalu.base.HostActivityListener;
import com.localvalu.common.BusinessInfoFragment;
import com.localvalu.common.BusinessInfoFragmentEmbedded;
import com.localvalu.common.ContactMeActivity;
import com.localvalu.common.ContactMeFragmentEmbedded;
import com.localvalu.common.RequestSlotFragment;
import com.localvalu.common.TableBookFragment;
import com.localvalu.common.review.ReviewsAddFragment;
import com.localvalu.common.review.ReviewsListFragment;
import com.localvalu.databinding.FragmentDirectoryDetailBinding;
import com.localvalu.directory.mycart.LocalCartFragment;
import com.localvalu.directory.mycart.LocalCartFragmentStore;
import com.localvalu.directory.scanpay.dto.BusinessDetail;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.directory.scanpay.dto.BusinessDetailResult;
import com.localvalu.directory.scanpay.wrapper.BusinessDetailResultWrapper;
import com.localvalu.eats.detail.OrderNowFragment;
import com.localvalu.eats.detail.OrderTableFragment;
import com.localvalu.eats.detail.adapter.DynamicFragmentAdapter;
import com.localvalu.eats.detail.adapter.FoodItemAdapter;
import com.localvalu.eats.detail.wrapper.OrderNowResultResponse;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.requestappointment.dto.RequestAppointment;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.squareup.picasso.Picasso;
import com.utility.AppUtils;
import com.utility.PreferenceUtility;
import com.utility.customView.ZoomOutPageTransformer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DirectoryDetailFragment extends BaseFragment
{
    private static final String TAG = "DirectoryDetailFragment";

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;

    private static final int MY_MAKE_CALL_PERMISSION_REQUEST_CODE = 123;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 12345;


    private int colorLocal;
    private FragmentDirectoryDetailBinding binding;

    private FoodItemAdapter foodItemAdapter;
    private int quantity, totQty = 0, quantityToServer;
    private Float totPrice = 0.0f;
    private String businessID, businessImage, businessName, businessDiscount, businessRetailerID, businessType, businessAddress;
    private String timeSlotRequest = "";
    private String dateSlotRequest = "";
    private String currentDate;
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private String strAddToBasket;
    private boolean locationEnabled = false;
    Call<OrderNowResultResponse> dtoCall;
    private HostActivityListener hostActivityListener;
    private Typeface typeface;
    private BusinessDetailInfo businessDetailInfo;
    private BusinessDetail businessDetail;


    public static DirectoryDetailFragment newInstance()
    {
        return new DirectoryDetailFragment();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");

        buildObjectForHandlingAPI();

        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        colorLocal = ContextCompat.getColor(requireContext(), R.color.colorLocal);
        typeface = ResourcesCompat.getFont(requireContext(), R.font.comfortaa_regular);

        readFromBundle();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date cal = (Date) Calendar.getInstance().getTime();
        currentDate = formatter.format(cal);
        dateSlotRequest = currentDate;
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        strAddToBasket = getString(R.string.lbl_add_to_basket);
    }

    private void readFromBundle()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        businessID = getArguments().getString("businessID");
        //businessID = "60";

    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentDirectoryDetailBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: ");
        setUpActionBar();
        setProperties();
        fetchBusinessInfo(businessID);
    }

    private void setUpActionBar()
    {
        int collapsedColor = ContextCompat.getColor(requireContext(), R.color.colorEats);
        int expandedColor = ContextCompat.getColor(requireContext(), R.color.colorWhite);
        binding.toolbarLayout.toolbarEats.getNavigationIcon().setColorFilter(expandedColor, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbarEats.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });
        binding.toolbarLayout.appBarEatDetails.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener()
        {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset)
            {
                float percentage = ((float) Math.abs(verticalOffset) / appBarLayout.getTotalScrollRange());
                binding.toolbarLayout.tvCurrentPromotion.setScaleX(1 - percentage);
                binding.toolbarLayout.tvCurrentPromotion.setScaleY(1 - percentage);
                binding.toolbarLayout.cnlInner.setAlpha(0.9f - percentage);
                if (percentage == 0)
                {
                    binding.toolbarLayout.btnActionContactMe.setTextColor(expandedColor);
                    binding.toolbarLayout.btnActionContactMe.getIcon().setColorFilter(expandedColor, PorterDuff.Mode.SRC_ATOP);
                    binding.toolbarLayout.toolbarEats.getNavigationIcon().setColorFilter(expandedColor, PorterDuff.Mode.SRC_ATOP);
                    binding.toolbarLayout.viewSeparator.setVisibility(View.GONE);
                }
                else if (percentage == 1)
                {
                    binding.toolbarLayout.btnActionContactMe.setTextColor(collapsedColor);
                    binding.toolbarLayout.btnActionContactMe.getIcon().setColorFilter(collapsedColor, PorterDuff.Mode.SRC_ATOP);
                    binding.toolbarLayout.toolbarEats.getNavigationIcon().setColorFilter(collapsedColor, PorterDuff.Mode.SRC_ATOP);
                    binding.toolbarLayout.viewSeparator.setVisibility(View.VISIBLE);
                }
            }
        });
        binding.toolbarLayout.appBarEatDetails.setVisibility(View.GONE);
    }

    private void setProperties()
    {
        binding.toolbarLayout.btnActionContactMe.setOnClickListener(_OnClickListener);
        binding.toolbarLayout.btnActionContactMe.setTypeface(typeface);
        binding.layoutError.btnRetry.setOnClickListener(_OnClickListener);
        binding.btnMoreInfo.setTypeface(typeface);
        binding.btnMoreInfo.setOnClickListener(_OnClickListener);
        binding.cnlRatingReview.setOnClickListener(_OnClickListener);
        binding.tabLayout.addOnTabSelectedListener(_OnTabSelectedListener);
        binding.layoutBottomView.clBottomView.setVisibility(View.GONE);
        binding.layoutBottomView.btnCheckOut.setOnClickListener(_OnClickListener);
    }

    TabLayout.OnTabSelectedListener _OnTabSelectedListener = new TabLayout.OnTabSelectedListener()
    {
        @Override
        public void onTabSelected(TabLayout.Tab tab)
        {
            setContent();
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab)
        {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab)
        {

        }
    };

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    private void setContent()
    {
        /*switch (binding.tabLayout.getSelectedTabPosition())
        {
            case 0:
                binding.flDetailContainerOne.setVisibility(View.VISIBLE);
                binding.flDetailContainerTwo.setVisibility(View.GONE);
                binding.flDetailContainerThree.setVisibility(View.GONE);
                updateCartInfoFromServer();
                break;
            case 1:
                binding.flDetailContainerOne.setVisibility(View.GONE);
                binding.flDetailContainerTwo.setVisibility(View.VISIBLE);
                binding.flDetailContainerThree.setVisibility(View.GONE);
                updateCartInfoFromServer();
                if(canHideBottom())
                {
                    hideBottom();
                }
                break;
            case 2:
                binding.flDetailContainerOne.setVisibility(View.GONE);
                binding.flDetailContainerTwo.setVisibility(View.GONE);
                binding.flDetailContainerThree.setVisibility(View.VISIBLE);
                hideBottom();
                break;
        }*/
    }

    private void updateCartInfoFromServer(Fragment fragment)
    {
        if (fragment != null)
        {
            if (fragment instanceof LocalOrderNowFragment)
            {
                LocalOrderNowFragment localOrderNowFragment = (LocalOrderNowFragment) fragment;

                if (localOrderNowFragment != null) localOrderNowFragment.callMyCartApi();
            }
            else if (fragment instanceof LocalOrderAtStoreFragment)
            {
                LocalOrderAtStoreFragment localOrderAtStoreFragment = (LocalOrderAtStoreFragment) fragment;

                if (localOrderAtStoreFragment != null) localOrderAtStoreFragment.callMyCartApi();
            }
        }
    }

    /**
     * Get the business information from server
     *
     * @param businessId
     */
    private void fetchBusinessInfo(String businessId)
    {
        showProgress();

        JSONObject objMain = new JSONObject();

        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("BusinessId", businessId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<BusinessDetailResultWrapper> dtoCall = apiInterface.businessDetailFrom(body);
        dtoCall.enqueue(new Callback<BusinessDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<BusinessDetailResultWrapper> call, Response<BusinessDetailResultWrapper> response)
            {
                //mProgressDialog.dismiss();

                try
                {
                    if (response.isSuccessful())
                    {
                        BusinessDetailResultWrapper businessDetailResultWrapper = response.body();

                        if (businessDetailResultWrapper != null)
                        {
                            BusinessDetailResult businessDetailResult = businessDetailResultWrapper.businessDetailResult;

                            if (businessDetailResult.errorDetails.errorCode == 0)
                            {
                                businessDetail = businessDetailResult.businessDetails;

                                if (businessDetail != null)
                                {
                                    businessDetailInfo = businessDetail.businessDetails.get(0);

                                    if (businessDetailInfo != null)
                                    {
                                        showContents();
                                        setRatingInformation();
                                        setBasicInfo();
                                        //setFragmentsInContainer();
                                        setFragmentsInPager();
                                    }
                                }
                            }

                            else
                            {
                                showError(businessDetailResult.errorDetails.errorMessage, false);
                            }
                        }
                    }
                    else
                    {
                        showError(getString(R.string.server_alert), false);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<BusinessDetailResultWrapper> call, Throwable t)
            {
                //mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());
                if (isAdded())
                {
                    if (t instanceof IOException)
                        showError(getString(R.string.network_unavailable), true);
                    else showError(getString(R.string.server_alert), false);
                }
            }
        });

    }

    /**
     * Set Basic Image
     *
     * @param
     */
    public void setBasicInfo()
    {
        binding.toolbarLayout.appBarEatDetails.setVisibility(View.VISIBLE);
        binding.tvName.setText(businessDetailInfo.tradingname);
        binding.tvDeliveryTime.setText("Delivery Time - 35 Mins");
        if (!businessDetailInfo.currentPromotionValue.equals(""))
        {
            binding.toolbarLayout.tvCurrentPromotion.setText(businessDetailInfo.currentPromotionValue + "% Off");
        }
        else
        {
            binding.toolbarLayout.tvCurrentPromotion.setText("0% OFF");
        }
        binding.toolbarLayout.cnlInner.transitionToStart();
        binding.toolbarLayout.cnlInner.transitionToEnd();
        if (!businessDetailInfo.businessCoverImage.equals(""))
        {
            binding.toolbarLayout.ivCoverImage.setVisibility(View.VISIBLE);

            Picasso.with(getActivity()).load(businessDetailInfo.businessCoverImage)
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo).into(binding.toolbarLayout.ivCoverImage);
            binding.toolbarLayout.ivCoverImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
        else
        {
            Picasso.with(getActivity()).load(R.drawable.app_logo)
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo).into(binding.toolbarLayout.ivCoverImage);
            binding.toolbarLayout.ivCoverImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }
        binding.tvAddress.setText(getAddress());
    }

    private void setRatingInformation()
    {
        if (businessDetailInfo.overAllProductRate.equals(""))
        {
            businessDetailInfo.overAllProductRate = "0.0";
        }
        if (businessDetailInfo.overAllServiceRate.equals(""))
        {
            businessDetailInfo.overAllServiceRate = "0.0";
        }
        if (businessDetailInfo.overAllreviewRate.equals("0"))
        {
            businessDetailInfo.overAllreviewRate = "";
        }
        if (businessDetailInfo.overAllreviewCount.equals("0"))
        {
            businessDetailInfo.overAllreviewCount = "";
        }

        binding.ratingBarProduct.setRating(Float.parseFloat(businessDetailInfo.overAllProductRate));


        String strReviewCount = businessDetailInfo.overAllreviewCount;
        StringBuilder strOverAllReviewCount = new StringBuilder();
        strOverAllReviewCount.append(getString(R.string.lbl_reviews));
        if (strReviewCount.equals(""))
        {
            strOverAllReviewCount.append(" ").append(0);
            binding.tvRatingCount.setText(strOverAllReviewCount.toString());
        }
        else
        {
            strOverAllReviewCount.append(" ").append(strReviewCount);
            binding.tvRatingCount.setText(strOverAllReviewCount.toString());
        }

        StringBuilder strProductRate = new StringBuilder();
        if (strProductRate.equals(""))
        {
            strProductRate.append(" ").append(0);
            binding.tvRatingCount.setText(strProductRate.toString());
        }
        else
        {
            strProductRate.append(Float.parseFloat(businessDetailInfo.overAllProductRate));
            binding.tvRatingCount.setText(strProductRate.toString());
        }

        StringBuilder strServiceRate = new StringBuilder();
        if (strServiceRate.equals(""))
        {
            strServiceRate.append(" ").append(0);
            binding.tvRatingCount.setText(strServiceRate.toString());
        }
        else
        {
            strServiceRate.append(Float.parseFloat(businessDetailInfo.overAllServiceRate));
            binding.tvRatingCount.setText(strServiceRate.toString());
        }
    }

    private void setFragmentsInPager()
    {
        ArrayList<String> bookingTypes = getBookingTypes();
        if (bookingTypes != null)
        {
            if (bookingTypes.size() > 0)
            {
                int numberOfTabs = bookingTypes.size();
                ArrayList<Fragment> fragments = new ArrayList<>();

                for (int i = 0; i < numberOfTabs; i++)
                {
                    switch (bookingTypes.get(i))
                    {
                        case AppUtils.ORDER_ONLINE:
                            LocalOrderNowFragment orderNowFragment = LocalOrderNowFragment.newInstance(businessDetailInfo);
                            fragments.add(orderNowFragment);
                            break;
                        case AppUtils.ORDER_AT_TABLE:
                        case AppUtils.ORDER_AT_STORE:
                            LocalOrderAtStoreFragment orderTableFragment = LocalOrderAtStoreFragment.newInstance(businessDetailInfo);
                            fragments.add(orderTableFragment);
                            break;
                        case AppUtils.BOOKING_TYPE_BOOK_A_TABLE:
                            TableBookFragment tableBooKFragment = TableBookFragment.newInstance(businessDetailInfo.business_id, AppUtils.BMT_EATS);
                            fragments.add(tableBooKFragment);
                            break;
                        case AppUtils.BOOKING_TYPE_REQUEST_A_CALLBACK:
                            ContactMeFragmentEmbedded contactMeFragmentEmbedded = ContactMeFragmentEmbedded.newInstance(businessDetailInfo.business_id);
                            fragments.add(contactMeFragmentEmbedded);
                            break;
                        case AppUtils.BOOKING_TYPE_REQUEST_AN_APPOINTMENT:
                            RequestSlotFragment requestSlotFragment = RequestSlotFragment.newInstance(businessDetailInfo.business_id, AppUtils.BMT_LOCAL);
                            fragments.add(requestSlotFragment);
                            break;
                    }
                }
                ArrayList<Fragment> fragmentsNew = new ArrayList<>();
                boolean orderOnline = bookingTypes.contains(AppUtils.ORDER_ONLINE);
                boolean orderAtTable = bookingTypes.contains(AppUtils.ORDER_AT_TABLE);
                boolean orderAtStore = bookingTypes.contains(AppUtils.ORDER_AT_STORE);
                boolean bookATable = bookingTypes.contains(AppUtils.BOOKING_TYPE_BOOK_A_TABLE);
                boolean requestCallback = bookingTypes.contains(AppUtils.BOOKING_TYPE_REQUEST_A_CALLBACK);
                boolean requestAppointment = bookingTypes.contains(AppUtils.BOOKING_TYPE_REQUEST_AN_APPOINTMENT);

                if (orderOnline)
                {
                    Fragment fragment = getOrderOnlineFragment(fragments);
                    if (fragment != null)
                    {
                        fragmentsNew.add(fragment);
                        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(getString(R.string.lbl_order_online)));
                    }
                }
                if (orderAtTable || orderAtStore)
                {
                    Fragment fragment = getOrderAtTableFragment(fragments);
                    if (fragment != null)
                    {
                        fragmentsNew.add(fragment);
                        if (orderAtTable)
                            binding.tabLayout.addTab(binding.tabLayout.newTab().setText(getString(R.string.lbl_order_at_table)));
                        else
                            binding.tabLayout.addTab(binding.tabLayout.newTab().setText(getString(R.string.lbl_order_at_store)));
                    }
                }
                if (bookATable)
                {
                    Fragment fragment = getTableBookFragment(fragments);
                    if (fragment != null)
                    {
                        fragmentsNew.add(fragment);
                        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(getString(R.string.lbl_book_a_table)));
                    }
                }
                if (requestCallback)
                {
                    Fragment fragment = getRequestCallBackFragment(fragments);
                    if (fragment != null)
                    {
                        fragmentsNew.add(fragment);
                        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(getString(R.string.lbl_request_a_callback)));
                    }
                }
                if (requestAppointment)
                {
                    Fragment fragment = getRequestSlotFragment(fragments);
                    if (fragment != null)
                    {
                        fragmentsNew.add(fragment);
                        binding.tabLayout.addTab(binding.tabLayout.newTab().setText(getString(R.string.lbl_request_appointment)));
                    }
                }

                binding.pager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback()
                {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
                    {
                        super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                    }

                    @Override
                    public void onPageSelected(int position)
                    {
                        super.onPageSelected(position);
                        // Because the fragment might or might not be created yet,
                        // we need to check for the size of the fragmentManager
                        // before accessing it.
                        if (getChildFragmentManager().getFragments().size() > position)
                        {
                            Fragment fragment = getChildFragmentManager().getFragments().get(position);
                            if(fragment.getView()!=null)
                            {
                                fragment.getView().postDelayed(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        updatePagerHeightForChild(fragment.getView(), binding.pager);
                                    }
                                },500);
                            }
                        }
                        // setCurrentItem as the tab position
                        DynamicFragmentAdapter adapter = (DynamicFragmentAdapter) binding.pager.getAdapter();
                        ArrayList<Fragment> fragmentArrayList = adapter.getFragments();
                        Fragment fragment = fragmentArrayList.get(position);

                        if (fragment instanceof LocalOrderNowFragment || fragment instanceof LocalOrderAtStoreFragment)
                        {
                            showBottom();
                            if(position==0)updateCartInfoFromServer(fragment);
                        }
                        else hideBottom();
                    }

                    @Override
                    public void onPageScrollStateChanged(int state)
                    {
                        super.onPageScrollStateChanged(state);
                    }
                });
                binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
                {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab)
                    {
                        if(userBasicInfo!=null)
                        {
                            binding.pager.setCurrentItem(tab.getPosition());
                        }

                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab)
                    {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab)
                    {

                    }
                });

                DynamicFragmentAdapter dynamicFragmentAdapter = new DynamicFragmentAdapter(getChildFragmentManager(), getLifecycle());
                dynamicFragmentAdapter.setFragments(fragmentsNew);
                binding.pager.setAdapter(dynamicFragmentAdapter);
            }
        }
    }

    private Fragment getOrderOnlineFragment(ArrayList<Fragment> fragments)
    {
        for (Fragment tempFragment : fragments)
        {
            if (tempFragment instanceof LocalOrderNowFragment)
            {
                return tempFragment;
            }
        }
        return null;
    }

    private Fragment getOrderAtTableFragment(ArrayList<Fragment> fragments)
    {
        for (Fragment tempFragment : fragments)
        {
            if (tempFragment instanceof LocalOrderAtStoreFragment)
            {
                return tempFragment;
            }
        }
        return null;
    }

    private Fragment getTableBookFragment(ArrayList<Fragment> fragments)
    {
        for (Fragment tempFragment : fragments)
        {
            if (tempFragment instanceof TableBookFragment)
            {
                return tempFragment;
            }
        }
        return null;
    }

    private Fragment getRequestSlotFragment(ArrayList<Fragment> fragments)
    {
        for (Fragment tempFragment : fragments)
        {
            if (tempFragment instanceof RequestSlotFragment)
            {
                return tempFragment;
            }
        }
        return null;
    }

    private Fragment getRequestCallBackFragment(ArrayList<Fragment> fragments)
    {
        for (Fragment tempFragment : fragments)
        {
            if (tempFragment instanceof ContactMeFragmentEmbedded)
            {
                return tempFragment;
            }
        }
        return null;
    }

    private void setFragmentsInContainer()
    {
        boolean bookTableOrRequestSlotAdded = false;
        int count = 0;

        /**
         * Set first tab fragment
         */
        if (isOrderNow())
        {
            LocalOrderNowFragment localOrderNowFragment = LocalOrderNowFragment.newInstance(businessDetailInfo);
            //getChildFragmentManager().beginTransaction().add(R.id.flDetailContainerOne,localOrderNowFragment,localOrderNowFragment.getClass().getName()).commit();
            count++;
        }
        else
        {
            BusinessInfoFragmentEmbedded businessInfoFragmentEmbedded = BusinessInfoFragmentEmbedded.newInstance(businessDetail, AppUtils.BMT_LOCAL);
            //getChildFragmentManager().beginTransaction().add(R.id.flDetailContainerOne,businessInfoFragmentEmbedded,businessInfoFragmentEmbedded.getClass().getName()).commit();
            binding.tabLayout.getTabAt(0).setText(getString(R.string.lbl_info));
            count++;
        }

        /**
         * Set second tab fragment
         */
        boolean orderAtTable = isOrderAtTable();
        boolean orderAtStore = isOrderAtStore();
        boolean secondTabAdded = false;
        if (orderAtTable || orderAtStore)
        {
            if (orderAtTable)
                binding.tabLayout.getTabAt(1).setText(getString(R.string.lbl_order_at_table));
            if (orderAtStore)
                binding.tabLayout.getTabAt(1).setText(getString(R.string.lbl_order_at_store));
            LocalOrderAtStoreFragment localOrderAtStoreFragment = LocalOrderAtStoreFragment.newInstance(businessDetailInfo);
            //getChildFragmentManager().beginTransaction().add(R.id.flDetailContainerTwo,localOrderAtStoreFragment,localOrderAtStoreFragment.getClass().getName()).commit();
            count++;
            secondTabAdded = true;
        }

        if (getBookingTypeIdSize() > 1)
        {
            if (!secondTabAdded)
            {
                ArrayList<Integer> ids = getRemainingBookingIds();

                if (ids != null)
                {
                    Log.d(TAG, "ids Size -> " + Integer.toString(ids.size()));
                    Log.d(TAG, "ids Size -> " + ids);
                    switch (ids.size())
                    {
                        case 2:
                            //setBookTableOrRequestSlot(Integer.toString(ids.get(0)),R.id.flDetailContainerTwo,1);
                            count++;
                            //setBookTableOrRequestSlot(Integer.toString(ids.get(1)),R.id.flDetailContainerThree,2);
                            count++;
                            break;
                        case 1:
                            //setBookTableOrRequestSlot(Integer.toString(ids.get(0)),R.id.flDetailContainerTwo,1);
                            count++;
                            break;
                    }
                    if (ids.size() == 1)
                    {
                        binding.tabLayout.removeTabAt(2);
                    }
                }
                else
                {
                    if (businessDetailInfo.bookingTypeId != null)
                    {
                        //setBookTableOrRequestSlot(businessDetailInfo.bookingTypeId,R.id.flDetailContainerTwo,1);
                        binding.tabLayout.removeTabAt(2);
                    }
                }
            }
            else
            {
                ArrayList<Integer> ids = getRemainingBookingIds();

                if (ids != null)
                {
                    Log.d(TAG, "ids Size -> " + Integer.toString(ids.size()));
                    Log.d(TAG, "ids Size -> " + ids);
                    switch (ids.size())
                    {
                        case 2:
                            String idOne = Integer.toString(ids.get(0));
                            if (idOne.equals(AppUtils.BOOKING_TYPE_BOOK_A_TABLE))
                            {
                                //setBookTableOrRequestSlot(idOne,R.id.flDetailContainerThree,2);
                            }
                            else if (idOne.equals(AppUtils.BOOKING_TYPE_REQUEST_AN_APPOINTMENT))
                            {
                                //setBookTableOrRequestSlot(idOne,R.id.flDetailContainerThree,2);
                            }
                            else if (idOne.equals(AppUtils.BOOKING_TYPE_REQUEST_A_CALLBACK))
                            {
                                //setBookTableOrRequestSlot(idOne,R.id.flDetailContainerThree,2);
                            }
                            count++;
                            String idTwo = Integer.toString(ids.get(1));
                            if (idOne.equals(AppUtils.BOOKING_TYPE_REQUEST_A_CALLBACK))
                            {

                            }
                            break;
                        case 1:
                            String id = Integer.toString(ids.get(0));
                            if (id.equals(AppUtils.BOOKING_TYPE_BOOK_A_TABLE))
                            {
                                //setBookTableOrRequestSlot(id,R.id.flDetailContainerThree,2);
                            }
                            else if (id.equals(AppUtils.BOOKING_TYPE_REQUEST_AN_APPOINTMENT))
                            {
                                //setBookTableOrRequestSlot(id,R.id.flDetailContainerThree,2);
                            }
                            else if (id.equals(AppUtils.BOOKING_TYPE_REQUEST_A_CALLBACK))
                            {
                                //setBookTableOrRequestSlot(id,R.id.flDetailContainerThree,2);
                            }
                            count++;
                            break;
                    }
                }
                else
                {
                    if (businessDetailInfo.bookingTypeId != null)
                    {
                        //setBookTableOrRequestSlot(businessDetailInfo.bookingTypeId,R.id.flDetailContainerTwo,1);
                        binding.tabLayout.removeTabAt(2);
                    }
                }

            }
        }
        else
        {
            binding.tabLayout.removeTabAt(2);
            binding.tabLayout.removeTabAt(1);
        }
        setContent();
    }

    private void setBookTableOrRequestSlot(String bookingTypeId, int container, int tabIndex)
    {
        /**
         * Set second third fragment
         */
        if (bookingTypeId != null)
        {
            switch (bookingTypeId)
            {
                case AppUtils.BOOKING_TYPE_REQUEST_AN_APPOINTMENT:
                    binding.tabLayout.getTabAt(tabIndex).setText(getString(R.string.lbl_request_appointment));
                    RequestSlotFragment requestSlotFragment = RequestSlotFragment.newInstance(businessDetailInfo.business_id, AppUtils.BMT_LOCAL);
                    getChildFragmentManager().beginTransaction().add(container, requestSlotFragment, requestSlotFragment.getClass().getName()).commit();
                    break;
                case AppUtils.BOOKING_TYPE_REQUEST_A_CALLBACK:
                    binding.tabLayout.getTabAt(tabIndex).setText(getString(R.string.lbl_contact_me));
                    ContactMeFragmentEmbedded contactMeFragmentEmbedded = ContactMeFragmentEmbedded.newInstance(businessDetailInfo.business_id);
                    getChildFragmentManager().beginTransaction().add(container, contactMeFragmentEmbedded, contactMeFragmentEmbedded.getClass().getName()).commit();
                    binding.toolbarLayout.btnActionContactMe.setVisibility(View.GONE);
                    break;
                case AppUtils.BOOKING_TYPE_BOOK_A_TABLE:
                    binding.tabLayout.getTabAt(tabIndex).setText(getString(R.string.lbl_book_a_table));
                    TableBookFragment tableBooKFragment = TableBookFragment.newInstance(businessDetailInfo.business_id, AppUtils.BMT_LOCAL);
                    getChildFragmentManager().beginTransaction().add(container, tableBooKFragment, tableBooKFragment.getClass().getName()).commit();
                    break;
            }
        }
    }

    private ArrayList<String> getBookingTypes()
    {
        ArrayList<String> bookingTypesList = new ArrayList<>();

        if (businessDetailInfo.bookingTypeId != null)
        {
            if (!businessDetailInfo.bookingTypeId.isEmpty())
            {
                String strBookingTypeIds = businessDetailInfo.bookingTypeId;
                if (strBookingTypeIds.contains(","))
                {
                    String[] bookingTypeIds = strBookingTypeIds.split(",");
                    return new ArrayList<>(Arrays.asList(bookingTypeIds));
                }
                else
                {
                    bookingTypesList.add(strBookingTypeIds);
                    return bookingTypesList;
                }
            }
        }
        return bookingTypesList;
    }

    private int getBookingTypeIdSize()
    {
        if (businessDetailInfo.bookingTypeId != null)
        {
            if (!businessDetailInfo.bookingTypeId.isEmpty())
            {
                if (businessDetailInfo.bookingTypeId.contains(","))
                {
                    String[] bookingTypeIds = businessDetailInfo.bookingTypeId.split(",");
                    return bookingTypeIds.length;
                }
            }
        }
        return 0;
    }

    private boolean canHideBottom()
    {
        if (isOrderNow() || isOrderAtTable() || isOrderAtStore())
        {
            return true;
        }
        return false;
    }

    private ArrayList<Integer> getRemainingBookingIds()
    {
        if (businessDetailInfo.bookingTypeId != null)
        {
            if (!businessDetailInfo.bookingTypeId.isEmpty())
            {
                if (businessDetailInfo.bookingTypeId.contains(","))
                {
                    String[] bookingTypeIds = businessDetailInfo.bookingTypeId.split(",");

                    ArrayList<Integer> ids = new ArrayList<>();

                    for (String id : bookingTypeIds)
                    {
                        if (!containsOrderTexts(id))
                        {
                            ids.add(Integer.parseInt(id));
                        }
                    }
                    return ids;
                }
            }
        }
        return null;
    }

    private boolean containsOrderTexts(String id)
    {
        if (id.contains(AppUtils.ORDER_ONLINE) || id.contains(AppUtils.ORDER_AT_TABLE)
                || (id.contains(AppUtils.ORDER_AT_STORE)))
        {
            return true;
        }
        return false;
    }

    private boolean isOrderNow()
    {
        return businessDetailInfo.bookingTypeId.contains(AppUtils.ORDER_ONLINE);
    }

    private boolean isOrderAtTable()
    {
        return businessDetailInfo.bookingTypeId.contains(AppUtils.ORDER_AT_TABLE);
    }

    private boolean isOrderAtStore()
    {
        return businessDetailInfo.bookingTypeId.contains(AppUtils.ORDER_AT_STORE);
    }

    private boolean isBookTable()
    {
        return businessDetailInfo.bookingTypeId.contains(AppUtils.BOOKING_TYPE_BOOK_A_TABLE);
    }

    private void showBottom()
    {
        binding.layoutBottomView.clBottomView.setVisibility(View.VISIBLE);

    }

    private void hideBottom()
    {
        binding.layoutBottomView.clBottomView.setVisibility(View.GONE);
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.cnlRatingReview:
                gotoReviewsFragment();
                break;
            case R.id.btnAddReview:
                goToAddReviewFragment();
                break;
            case R.id.btnCheckOut:
                gotoCheckOutFragment();
                break;
            case R.id.btnAddToCart:
                break;
            case R.id.btnActionContactMe:
                gotoContactMeFragment();
                break;
            case R.id.btnMoreInfo:
                gotoInfoFragment();
                break;
            case R.id.btnRetry:
                fetchBusinessInfo(businessID);
                break;

        }
    }

    private void gotoContactMeFragment()
    {
        //ContactMeFragment contactMeFragment = ContactMeFragment.newInstance(businessID);
        //hostActivityListener.updateFragment(contactMeFragment,false,ContactMeFragment.class.getName(),true);

        Intent intent = new Intent(requireActivity(), ContactMeActivity.class);
        intent.putExtra(AppUtils.BUNDLE_BUSINESS_ID, businessID);
        intent.putExtra(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_LOCAL);
        startActivity(intent);
    }

    private void gotoInfoFragment()
    {
        BusinessInfoFragment businessInfoFragment = BusinessInfoFragment.newInstance(businessDetail, AppUtils.BMT_LOCAL);
        hostActivityListener.updateFragment(businessInfoFragment, true, BusinessInfoFragment.class.getName(), true);
    }

    /**
     * Move to the reviews fragment
     */
    public void gotoReviewsFragment()
    {
        final Bundle bundle = new Bundle();
        System.out.println("businessID :: " + businessID);
        ReviewsListFragment directoryReviewsListFragment = ReviewsListFragment.newInstance();
        bundle.putString(AppUtils.BUNDLE_BUSINESS_ID, businessDetailInfo.business_id);
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_LOCAL);
        directoryReviewsListFragment.setArguments(bundle);
        hostActivityListener.updateFragment(directoryReviewsListFragment, true, ReviewsListFragment.class.getName(), true);
    }

    /**
     * Move to the add review fragment
     */
    public void goToAddReviewFragment()
    {
        final Bundle bundle = new Bundle();
        System.out.println("businessID :: " + businessID);
        ReviewsAddFragment directoryReviewsAddFragment = ReviewsAddFragment.newInstance();

        bundle.putString("section_type", "info");
        bundle.putString("order_id", "");
        bundle.putString("businessID", businessDetailInfo.business_id);
        bundle.putString("businessImage", businessDetailInfo.businessCoverImage);
        bundle.putString("businessName", businessDetailInfo.tradingname);
        bundle.putString("businessType", businessDetailInfo.businessType);
        bundle.putString("businessAddress", getAddress());

        directoryReviewsAddFragment.setArguments(bundle);
        ((MainActivity) getActivity()).addFragment(directoryReviewsAddFragment, true, ReviewsAddFragment.class.getName(), true);
    }

    private void gotoCheckOutFragment()
    {
        totQty = 0;

        DynamicFragmentAdapter adapter = (DynamicFragmentAdapter) binding.pager.getAdapter();
        ArrayList<Fragment> fragmentArrayList = adapter.getFragments();
        Fragment fragment = fragmentArrayList.get(binding.tabLayout.getSelectedTabPosition());
        if (fragment instanceof LocalOrderNowFragment)
        {
            LocalCartFragment localCartFragment = LocalCartFragment.newInstance(businessDetailInfo);
            hostActivityListener.updateFragment(localCartFragment, true, localCartFragment.getClass().getName(), true);
        }
        else
        {
            LocalCartFragmentStore localCartFragmentStore = LocalCartFragmentStore.newInstance(businessDetailInfo);
            hostActivityListener.updateFragment(localCartFragmentStore, true, localCartFragmentStore.getClass().getName(), true);
        }
    }

    private String getAddress()
    {
        StringBuilder strAddress = new StringBuilder();
        if (businessDetailInfo.address1.isEmpty())
        {
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        else
        {
            strAddress.append(businessDetailInfo.address1.trim());
            strAddress.append(" ");
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.city.isEmpty())
        {
            strAddress.append(businessDetailInfo.city.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.country.isEmpty())
        {
            strAddress.append(businessDetailInfo.country.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.postcode.isEmpty())
        {
            strAddress.append(businessDetailInfo.postcode.trim());
        }
        return strAddress.toString();
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        locationEnabled = false;
    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.nestedScrollView.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showContents()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.nestedScrollView.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    public void setBottomViewContent(String strItems, String price, boolean visible, boolean enableCheckout)
    {
        binding.layoutBottomView.tvItemsInCart.setText(strItems.toString());
        binding.layoutBottomView.tvPriceInCart.setText(price);
        if (visible) binding.layoutBottomView.clBottomView.setVisibility(View.VISIBLE);
        else binding.layoutBottomView.clBottomView.setVisibility(View.GONE);
        binding.layoutBottomView.btnCheckOut.setEnabled(enableCheckout);
    }

    public void updateCartFragment(ArrayList<CartDetailDto> cartList)
    {
        DynamicFragmentAdapter adapter = (DynamicFragmentAdapter) binding.pager.getAdapter();

        for (Fragment fragment : adapter.getFragments())
        {
            if (fragment != null)
            {
                if (fragment instanceof LocalOrderNowFragment)
                {
                    LocalOrderNowFragment localOrderNowFragment = (LocalOrderNowFragment) fragment;

                    if (localOrderNowFragment != null)
                        localOrderNowFragment.setCartDetails(cartList);
                }
                else if (fragment instanceof LocalOrderAtStoreFragment)
                {
                    LocalOrderAtStoreFragment localOrderAtStoreFragment = (LocalOrderAtStoreFragment) fragment;

                    if (localOrderAtStoreFragment != null)
                        localOrderAtStoreFragment.setCartDetails(cartList);
                }
            }

        }
    }

    private void showError(String strMessage, boolean retry)
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.nestedScrollView.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if (retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

    public void setDeliveryFee(String deliveryFee)
    {
        StringBuilder strDeliveryFee = new StringBuilder();
        strDeliveryFee.append(getString(R.string.lbl_delivery_fee)).append(" ")
                .append(getString(R.string.lbl_currency_symbol_euro)).append(" ")
                .append(deliveryFee);
        binding.tvDeliveryFee.setText(strDeliveryFee);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if (dtoCall != null)
        {
            dtoCall.cancel();
        }
    }

}
