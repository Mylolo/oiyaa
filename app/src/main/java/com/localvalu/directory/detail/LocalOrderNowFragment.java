package com.localvalu.directory.detail;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentOrderNowBinding;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.eats.detail.EatsDetailFragment;
import com.localvalu.eats.detail.adapter.FoodItemAdapter;
import com.localvalu.eats.detail.dto.BusinessDetailFoodItemsForEatsDto;
import com.localvalu.eats.detail.dto.FoodItemSizeDto;
import com.localvalu.eats.detail.dto.FoodMenuDto;
import com.localvalu.eats.detail.dto.OrderNowDto;
import com.localvalu.eats.detail.listener.AddToCartClickListener;
import com.localvalu.eats.detail.listener.FoodItemClickListener;
import com.localvalu.eats.detail.wrapper.OrderNowResultResponse;
import com.localvalu.eats.mycart.CartFragment;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocalOrderNowFragment extends BaseFragment implements AddToCartClickListener
{
    private static final String TAG = "LocalOrderNowFragment";


    private int colorEats;
    private BottomSheetBehavior sheetBehavior;
    private FragmentOrderNowBinding binding;

    private FoodItemAdapter foodItemAdapter;
    private int quantity, totQty = 0, quantityToServer;
    private Float totPrice = 0.0f;
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private String strAddToBasket;
    private boolean locationEnabled = false;
    private Call<OrderNowResultResponse> dtoCall;
    private ArrayList<CartDetailDto> tempCartList;
    private String price;
    private static final int MINUS_CLICKED = 1;
    private static final int PLUS_CLICKED = 2;
    private int qtyButtonClicked;
    private boolean itemExist;
    private boolean firstTimeCartLoad = true;
    private HostActivityListener hostActivityListener;
    private Typeface typeface;
    private static int BOTTOM_SHEET_DIALOG_REQUEST=11;
    private BusinessDetailInfo businessDetailInfo;
    private ArrayList<BusinessDetailFoodItemsForEatsDto> foodMenus;
    private LocalOrderBottomSheetFragment localOrderBottomSheetFragment;

    public static LocalOrderNowFragment newInstance(BusinessDetailInfo businessDetailInfo)
    {
        LocalOrderNowFragment orderNowFragment = new LocalOrderNowFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppUtils.BUNDLE_BUSINESS_DETAIL,businessDetailInfo);
        orderNowFragment.setArguments(bundle);
        return orderNowFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");

        buildObjectForHandlingAPI();

        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        typeface = ResourcesCompat.getFont(requireContext(), R.font.comfortaa_regular);

        readFromBundle();

        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        strAddToBasket = getString(R.string.lbl_add_to_basket);
    }

    private void readFromBundle()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        businessDetailInfo = (BusinessDetailInfo) getArguments().getSerializable(AppUtils.BUNDLE_BUSINESS_DETAIL);
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentOrderNowBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: ");
        setProperties();
        fetchOrderOnlineApi(businessDetailInfo.business_id);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        binding.getRoot().requestLayout();
    }

    private void initView()
    {
        initBottomSheetView();
        binding.rvFoodItems.setLayoutManager(new LinearLayoutManager(requireContext()));
    }

    private void initBottomSheetView()
    {
        binding.layoutBottomSheet.clBottomSheet.setVisibility(View.GONE);
        /*nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener()
        {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY)
            {
                if (scrollY < oldScrollY)
                {
                    if(!clBottomView.isShown()) showBottom();
                }
                else
                {
                    if(clBottomView.isShown()) hideBottom();
                }
            }
        });*/
        setPopUpMenuBottomSheet();
    }

    private void setProperties()
    {
        initView();
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    private void fetchOrderOnlineApi(String businessId)
    {
        showProgress();

        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("BusinessId", businessId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("requestFor", "MyCartDetails");
            objMain.put("Distance", "12");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        dtoCall = apiInterface.orderOnlineLocal(body);
        dtoCall.enqueue(new Callback<OrderNowResultResponse>()
        {
            @Override
            public void onResponse(Call<OrderNowResultResponse> call, Response<OrderNowResultResponse> response)
            {
                try
                {
                    if (response.body().businessDetailsResult.errorDetails.errorCode == 0)
                    {
                        showContents();
                        setOrderList(response.body().businessDetailsResult.orderNowDto.foodMenus);
                        setCartDetails(response.body().businessDetailsResult.orderNowDto.cartDetails);
                        setDeliveryFee(response.body());
                    }
                    else
                    {
                        showError(response.body().businessDetailsResult.errorDetails.errorMessage, false);
                        setDeliveryFee(null);
                    }
                }

                catch (Exception e)
                {
                    e.printStackTrace();
                    setDeliveryFee(null);
                }
            }

            @Override
            public void onFailure(Call<OrderNowResultResponse> call, Throwable t)
            {
                Log.d(TAG, t.getMessage());
                if (isAdded())
                {
                    if (t instanceof IOException)
                        showError(getString(R.string.network_unavailable), true);
                    else showError(getString(R.string.server_alert), false);
                }
            }
        });
    }

    //For Listing Order Now List
    public void setPopUpMenuBottomSheet()
    {
        /**BottomSheet Setup*/
        sheetBehavior = BottomSheetBehavior.from(binding.layoutBottomSheet.clBottomSheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
        {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState)
            {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        //if(!binding.layoutBottomSheet.clBottomSheet.isShown()) showBottom();
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        //if(binding.layoutBottomSheet.clBottomSheet.isShown()) hideBottom();
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset)
            {

            }
        });
    }

    private void setOrderList(ArrayList<BusinessDetailFoodItemsForEatsDto> foodMenus)
    {
        ArrayList<FoodMenuDto> tempFoodItems = new ArrayList<FoodMenuDto>();

        if (foodMenus != null)
        {
            if (foodMenus.size() > 0)
            {
                for (BusinessDetailFoodItemsForEatsDto item : foodMenus)
                {
                    FoodMenuDto foodMenuDto = new FoodMenuDto();
                    foodMenuDto.menuName = item.menuName;
                    foodMenuDto.isHeader = true;
                    tempFoodItems.add(foodMenuDto);
                    tempFoodItems.addAll(item.items);
                }

                foodItemAdapter = new FoodItemAdapter(requireContext(), tempFoodItems);
                foodItemAdapter.setFoodItemClickListener(new FoodItemClickListener()
                {
                    @Override
                    public void onFoodItemClicked(FoodItemSizeDto foodItemSizeDto)
                    {
                        if (foodItemAdapter != null)
                        {
                            List<FoodMenuDto> items = foodItemAdapter.getItems();
                            FoodMenuDto foodMenuDto = items.get(foodItemSizeDto.groupPosition);
                            setUpOrderTableDataBottomSheet(foodMenuDto, foodItemSizeDto);
                        }
                    }
                });
                binding.rvFoodItems.setAdapter(foodItemAdapter);
            }
            else
            {
                showError(getString(R.string.lbl_no_food_items),false);
            }
        }
    }

    private void setUpOrderTableDataBottomSheet(FoodMenuDto foodMenuDto, FoodItemSizeDto foodItemSizeDto)
    {
        if(localOrderBottomSheetFragment!=null) localOrderBottomSheetFragment.dismiss();
        if(tempCartList!=null)
        {
            localOrderBottomSheetFragment = LocalOrderBottomSheetFragment.newInstance(foodMenuDto,foodItemSizeDto,tempCartList);
            localOrderBottomSheetFragment.setTargetFragment(this,BOTTOM_SHEET_DIALOG_REQUEST);
            localOrderBottomSheetFragment.show(getParentFragment().getChildFragmentManager(),localOrderBottomSheetFragment.getClass().getName());
        }
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {

            case R.id.btnCheckOut:
                 totQty = 0;
                 CartFragment cartFragment = CartFragment.newInstance(businessDetailInfo);
                 hostActivityListener.updateFragment(cartFragment, true, CartFragment.class.getName(), true);
                 break;
            case R.id.btnAddToCart:
                 sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                 break;
            case R.id.btnRetry:
                 fetchOrderOnlineApi(businessDetailInfo.business_id);
                 break;
        }
    }

    public void callMyCartApi()
    {
        //if (!firstTimeCartLoad) mProgressDialog.show();
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId", businessDetailInfo.business_id);
            objMain.put("requestFor", "MyCartDetails");
            objMain.put("Distance", "12");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<OrderNowResultResponse> dtoCall = apiInterface.orderOnlineLocal(body);
        dtoCall.enqueue(new Callback<OrderNowResultResponse>()
        {
            @Override
            public void onResponse(Call<OrderNowResultResponse> call, Response<OrderNowResultResponse> response)
            {
                //if (!firstTimeCartLoad) mProgressDialog.dismiss();
                //firstTimeCartLoad = false;
                mProgressDialog.dismiss();
                if (response.body().businessDetailsResult.errorDetails.errorCode == 0)
                {
                    ArrayList<CartDetailDto> cartList = response.body().businessDetailsResult.orderNowDto.cartDetails;
                    setCartDetails(cartList);
                    setDeliveryFee(response.body());
                }
                else
                {
                    setBottomView("","",false);
                    setDeliveryFee(null);
                }
            }

            @Override
            public void onFailure(Call<OrderNowResultResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void callAPIForAddToCart(String strFoodItemId, String quantity, String sizeId)
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId", businessDetailInfo.business_id);
            objMain.put("requestFor", "AddItemsToCart");
            objMain.put("global_unique_id", "1");
            objMain.put("itemId", strFoodItemId);
            objMain.put("sizeId", sizeId);
            objMain.put("quantity", quantity);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<OrderNowResultResponse> dtoCall = apiInterface.orderOnlineLocal(body);
        dtoCall.enqueue(new Callback<OrderNowResultResponse>()
        {
            @Override
            public void onResponse(Call<OrderNowResultResponse> call, Response<OrderNowResultResponse> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().addToCartResult.errorDetails.errorCode == 0)
                    {
                        Toast.makeText(getActivity(), getString(R.string.msg_add_to_cart_success), Toast.LENGTH_LONG).show();
                        callMyCartApi();
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().addToCartResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<OrderNowResultResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                {
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                }
                else
                {
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                }
            }
        });
    }

    public void setCartDetails(ArrayList<CartDetailDto> cartList)
    {
        try
        {
            tempCartList = cartList;

            if (cartList != null)
            {
                if (cartList.size() > 0)
                {
                    StringBuilder strItems = new StringBuilder();
                    strItems.append(Integer.toString(cartList.size())).append(" Items");

                    double price = 0;

                    for (CartDetailDto cartDetailDto : cartList)
                    {
                        price = price + Double.parseDouble(cartDetailDto.rate);
                    }

                    StringBuilder strPrice = new StringBuilder();
                    strPrice.append(strCurrencySymbol).append(String.format("%,.2f", price));

                    setBottomView(strItems.toString(),strPrice.toString(),true);
                }
                else
                {
                    setBottomView("","",false);
                }
            }
            else
            {
                setBottomView("","",false);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private void setBottomView(String strItems,String strPrice,boolean visible)
    {
        Fragment parentFragment = getParentFragment();

        if(parentFragment instanceof DirectoryDetailFragment)
        {
            DirectoryDetailFragment directoryDetailFragment = (DirectoryDetailFragment) parentFragment;
            if(directoryDetailFragment!=null)
                if(visible) directoryDetailFragment.setBottomViewContent(strItems,strPrice,true,true);
                else
                {
                    double price = 0;
                    StringBuilder strNoOfItems = new StringBuilder();
                    strNoOfItems.append(0).append(" Items");
                    StringBuilder strTotPrice = new StringBuilder();
                    strTotPrice.append(strCurrencySymbol).append(String.format("%,.2f", price));
                    directoryDetailFragment.setBottomViewContent(strNoOfItems.toString(),strTotPrice.toString(),true,false);
                }
        }
    }

    private void setDeliveryFee(OrderNowResultResponse orderNowResultResponse)
    {
        Fragment parentFragment = getParentFragment();

        if(parentFragment instanceof DirectoryDetailFragment)
        {
            DirectoryDetailFragment directoryDetailFragment = (DirectoryDetailFragment) parentFragment;

            if(directoryDetailFragment!=null)
            {
                if(orderNowResultResponse != null)
                {
                    if(orderNowResultResponse.businessDetailsResult.orderNowDto!=null)
                    {
                        OrderNowDto orderNowDto = orderNowResultResponse.businessDetailsResult.orderNowDto;

                        if(orderNowDto.deliveryCharge!=null)
                        {
                            if(orderNowDto.deliveryCharge.cost!=null)
                            {
                                if(!orderNowDto.deliveryCharge.cost.isEmpty())
                                {
                                    float deliveryCost = Float.parseFloat(orderNowDto.deliveryCharge.cost);
                                    DecimalFormat df = new DecimalFormat("0.00");
                                    df.setMaximumFractionDigits(2);
                                    directoryDetailFragment.setDeliveryFee(df.format(deliveryCost));
                                }
                                else
                                {
                                    directoryDetailFragment.setDeliveryFee("0.00");
                                }
                            }
                            else
                            {
                                directoryDetailFragment.setDeliveryFee("0.00");
                            }
                        }
                        else
                        {
                            directoryDetailFragment.setDeliveryFee("0.00");
                        }
                    }
                    else
                    {
                        directoryDetailFragment.setDeliveryFee("0.00");
                    }
                }
                else
                {
                    directoryDetailFragment.setDeliveryFee("0.00");
                }
            }
        }
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.rvFoodItems.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showContents()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvFoodItems.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage, boolean retry)
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvFoodItems.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if (retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        if (dtoCall != null)
        {
            dtoCall.cancel();
        }
    }

    @Override
    public void onAddToCartClicked(String strFoodItemId, String strQty, String strFoodItemSizeId)
    {
        callAPIForAddToCart(strFoodItemId,strQty,strFoodItemSizeId);
    }
}
