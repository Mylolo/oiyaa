package com.localvalu.directory.detail.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.detail.dto.RequestResponseResult;

import java.io.Serializable;

public class SaveRequestSlotResultWrapper implements Serializable {

    @SerializedName("requestSlotResults")
    @Expose
    public RequestResponseResult requestResponseResult;
}
