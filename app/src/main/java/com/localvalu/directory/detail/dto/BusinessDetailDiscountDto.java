package com.localvalu.directory.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessDetailDiscountDto implements Serializable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("business_id")
    @Expose
    public String businessId;
    @SerializedName("discount")
    @Expose
    public String discount;
    @SerializedName("discount_type")
    @Expose
    public String discountType;
    @SerializedName("from_time")
    @Expose
    public String fromTime;
    @SerializedName("to_time")
    @Expose
    public String toTime;
    @SerializedName("from_date")
    @Expose
    public String fromDate;
    @SerializedName("to_date")
    @Expose
    public String toDate;
    @SerializedName("Mon")
    @Expose
    public String mon;
    @SerializedName("Tue")
    @Expose
    public String tue;
    @SerializedName("Wed")
    @Expose
    public String wed;
    @SerializedName("Thu")
    @Expose
    public String thu;
    @SerializedName("Fri")
    @Expose
    public String fri;
    @SerializedName("Sat")
    @Expose
    public String sat;
    @SerializedName("Sun")
    @Expose
    public String sun;
}
