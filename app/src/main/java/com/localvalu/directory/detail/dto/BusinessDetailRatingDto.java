package com.localvalu.directory.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessDetailRatingDto implements Serializable {

    @SerializedName("product_rate")
    @Expose
    public String product_rate;
    @SerializedName("service_rate")
    @Expose
    public String service_rate;
    @SerializedName("count_rate")
    @Expose
    public String count_product_rate;
    @SerializedName("count_service_rate")
    @Expose
    public String count_service_rate;
}
