package com.localvalu.directory.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class BusinessDetailDto implements Serializable {

    @SerializedName("business_name")
    @Expose
    public String businessName;
    @SerializedName("FixedBusinessDiscount")
    @Expose
    public String discountName;
    @SerializedName("logo")
    @Expose
    public String logo;
    @SerializedName("website")
    @Expose
    public String website;
    @SerializedName("contact_name")
    @Expose
    public String contactName;
    @SerializedName("contact_phone")
    @Expose
    public String contactPhone;
    @SerializedName("about")
    @Expose
    public String about;
    @SerializedName("timing")
    @Expose
    public String timing;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("business_created_date")
    @Expose
    public String businessCreatedDate;
    @SerializedName("business_modified_date")
    @Expose
    public String businessModifiedDate;
    @SerializedName("min_order_val")
    @Expose
    public String minOrderVal;
    @SerializedName("categories")
    @Expose
    public String categories;
    @SerializedName("category_name")
    @Expose
    public String categoryName;
    @SerializedName("BusinessImage")
    @Expose
    public String businessImage;
    @SerializedName("BusinessImageThumbnail")
    @Expose
    public String businessImageThumbnail;
    @SerializedName("type_name")
    @Expose
    public String typeName;
    @SerializedName("rate")
    @Expose
    public String rate;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("Address2")
    @Expose
    public String address2;
    @SerializedName("City")
    @Expose
    public String city;
    @SerializedName("County")
    @Expose
    public String county;
    @SerializedName("PostCode")
    @Expose
    public String postCode;
    @SerializedName("Country")
    @Expose
    public String country;
    @SerializedName("lat")
    @Expose
    public String latitude;
    @SerializedName("lng")
    @Expose
    public String lngitude;
    @SerializedName("isFeatured")
    @Expose
    public String feature;
    @SerializedName("LatestReview")
    @Expose
    public BusinessDetailReviewDto latestReview;
    @SerializedName("Services")
    @Expose
    public ArrayList<BusinessDetailServiceDto> services;
    @SerializedName("discounts")
    @Expose
    public ArrayList<BusinessDetailDiscountDto> discounts;
    @SerializedName("RatingTypes")
    @Expose
    public BusinessDetailRatingDto rating;
    @SerializedName("BusinessImages")
    @Expose
    public ArrayList<BusinessDetailImagesDto> images;
}
