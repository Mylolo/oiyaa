package com.localvalu.directory.detail.adapter;

import android.app.Activity;

import androidx.viewpager.widget.PagerAdapter;

import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.localvalu.R;
import com.localvalu.directory.detail.dto.BusinessDetailImagesDto;
import com.localvalu.directory.scanpay.dto.BusinessImages;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class DirectoryImageSliderViewPagerAdapter extends PagerAdapter
{
    public int selectedPos = -1;
    private Activity activity;
    private List<BusinessImages> imageArrayList;
    private OnPagerItemClickListener onPagerItemClickListener;
    private int deviceWidth;

    public DirectoryImageSliderViewPagerAdapter(Activity activity, List<BusinessImages> imageArrayList, boolean flag)
    {
        this.activity = activity;
        this.imageArrayList = imageArrayList;
        setDeviceWidth();
    }

    public void setDeviceWidth()
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        deviceWidth = width;
    }

    @Override
    public int getCount()
    {
        return imageArrayList.size();
    }

    public int getItemPosition(Object object)
    {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position)
    {
        String img = imageArrayList.get(position).imagePath;
        System.out.println("img " + img);
        System.out.println("img " + img);
        View imageLayout = activity.getLayoutInflater().inflate(R.layout.directory_image_viewpager_slider, container, false);
        final ImageView iv_img = imageLayout.findViewById(R.id.iv_img);

        if (!img.trim().equals(""))
            Picasso.with(activity)
                    .load(img)
                    .resize(deviceWidth - 40, deviceWidth / 2)
                    .centerCrop()
                    .into(iv_img);


        else
            iv_img.setImageResource(R.drawable.directory_listing_sample);

        iv_img.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                selectedPos = position;
                System.out.println("selectedPos ...." + selectedPos);
                onPagerItemClickListener.onPagerItemClick(position);
            }
        });

        container.addView(imageLayout, 0);
        return imageLayout;
    }

    public void setOnPagerItemClickListener(OnPagerItemClickListener onPagerItemClickListener)
    {
        this.onPagerItemClickListener = onPagerItemClickListener;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object)
    {
        return view == object;
    }

    @Override
    public float getPageWidth(int position)
    {
        return Float.parseFloat("1.00");
    }

    public interface OnPagerItemClickListener
    {
        void onPagerItemClick(int position);
    }

}
