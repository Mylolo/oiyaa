package com.localvalu.directory.detail.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.directory.detail.dto.BusinessDetailServiceDto;

import java.util.ArrayList;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class DirectoryServiceListAdapter extends RecyclerView.Adapter<DirectoryServiceListAdapter.ViewHolder> {
    public static final String TAG = DirectoryServiceListAdapter.class.getSimpleName();
    private Activity activity;
    private OnItemClickListener onClickListener;
    private ArrayList<BusinessDetailServiceDto> serviceList;

    public DirectoryServiceListAdapter(Activity activity, ArrayList<BusinessDetailServiceDto> serviceList) {
        this.activity = activity;
        this.serviceList = serviceList;
    }

    @NonNull
    @Override
    public DirectoryServiceListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_directory_service_list, viewGroup, false);
        return new DirectoryServiceListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DirectoryServiceListAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.tv_service_content.setText("■ " + serviceList.get(i).serviceName);
        viewHolder.tv_service_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onItemClick(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

//    public class ViewHolder extends RecyclerView.ViewHolder {
//        public ViewHolder(@NonNull View itemView) {
//            super(itemView);
//        }
//    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_service_content = itemView.findViewById(R.id.tv_service_content);

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
