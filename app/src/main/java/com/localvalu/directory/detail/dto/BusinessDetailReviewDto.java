package com.localvalu.directory.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessDetailReviewDto implements Serializable {

    @SerializedName("rate_given_by")
    @Expose
    public String rateGivenBy;
    @SerializedName("image")
    @Expose
    public String userImage;
    @SerializedName("product_rate")
    @Expose
    public String productRate;
    @SerializedName("service_rate")
    @Expose
    public String serviceRate;
    @SerializedName("review")
    @Expose
    public String review;
    @SerializedName("DaysAgo")
    @Expose
    public String daysAgo;
}
