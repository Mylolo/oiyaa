package com.localvalu.directory.detail.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.detail.dto.RequestResponseResult;

import java.io.Serializable;

public class SaveRequestQuoteResultWrapper implements Serializable {

    @SerializedName("RequestAQuote")
    @Expose
    public RequestResponseResult requestResponseResult;
}
