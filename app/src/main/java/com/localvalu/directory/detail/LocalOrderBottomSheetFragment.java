package com.localvalu.directory.detail;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.localvalu.R;
import com.localvalu.databinding.FragmentBottomSheetBinding;
import com.localvalu.databinding.FragmentBottomSheetLocalBinding;
import com.localvalu.eats.detail.dto.FoodItemSizeDto;
import com.localvalu.eats.detail.dto.FoodMenuDto;
import com.localvalu.eats.detail.listener.AddToCartClickListener;
import com.localvalu.eats.mycart.dto.CartDetailDto;

import java.util.ArrayList;

public class LocalOrderBottomSheetFragment extends BottomSheetDialogFragment
{
    private static final String TAG = "LocalOrderBottomSheetFragment";

    private FragmentBottomSheetLocalBinding binding;
    private BottomSheetBehavior sheetBehavior;
    private static final String BUNDLE_FOOD_MENU = "Bundle_Food_Menu";
    private static final String BUNDLE_FOOD_ITEM_SIZE = "Bundle_Food_Item_Size";
    private static final String BUNDLE_CART_LIST= "Bundle_Cart_List";
    private FoodMenuDto foodMenuDto;
    private FoodItemSizeDto foodItemSizeDto;
    private ArrayList<CartDetailDto> cartList;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private String strAddToBasket;
    private static final int MINUS_CLICKED = 1;
    private static final int PLUS_CLICKED = 2;
    private int qtyButtonClicked;
    private String price;
    private int quantity = 1;
    private int quantityToServer = 1;
    private boolean itemExist;
    private String strFoodItemId;
    private String strFoodItemSizeId;
    private AddToCartClickListener addToCartClickListener;

    public static LocalOrderBottomSheetFragment newInstance(FoodMenuDto foodMenuDto, FoodItemSizeDto foodItemSizeDto, ArrayList<CartDetailDto> cartList)
    {
        LocalOrderBottomSheetFragment localOrderBottomSheetFragment = new LocalOrderBottomSheetFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BUNDLE_FOOD_MENU,foodMenuDto);
        bundle.putSerializable(BUNDLE_FOOD_ITEM_SIZE,foodItemSizeDto);
        bundle.putSerializable(BUNDLE_CART_LIST,cartList);
        localOrderBottomSheetFragment.setArguments(bundle);
        return localOrderBottomSheetFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        readFromBundle();
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        strAddToBasket = getString(R.string.lbl_add_to_basket);
        addToCartClickListener = (AddToCartClickListener) getTargetFragment();
    }

    private void readFromBundle()
    {
        if(getArguments()!=null)
        {
            foodMenuDto = (FoodMenuDto) getArguments().getSerializable(BUNDLE_FOOD_MENU);
            foodItemSizeDto = (FoodItemSizeDto) getArguments().getSerializable(BUNDLE_FOOD_ITEM_SIZE);
            cartList = (ArrayList<CartDetailDto>) getArguments().getSerializable(BUNDLE_CART_LIST);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        binding = FragmentBottomSheetLocalBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        binding.btnAddToCart.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(requireContext(),R.color.colorLocal)));

        initialize();

        //Check for items existence and load the quantity
        for (CartDetailDto cartDetailDto : cartList)
        {
            if (strFoodItemSizeId.equals(cartDetailDto.sizeId))
            {
                /** Api has been changed on 21/05/2020-Code commented
                 * Clien requirement neeta mam...changed on 10/06/2020-code uncommented**/
                itemExist = true;
                quantity = Integer.parseInt(cartDetailDto.quantity);
                quantityToServer = 0;
            }
        }
        binding.tvNoOfItems.setText("" + quantity);
        binding.btnAddToCart.setText(strAddToBasket + String.format("%,.2f", (Float.parseFloat(price) * quantity)));
        //When we click minus button.
        binding.tvMinus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (quantity > 1)
                {
                    quantity--;
                    binding.tvNoOfItems.setText("" + quantity);
                    binding.btnAddToCart.setText(strAddToBasket + String.format("%,.2f", (Float.parseFloat(price) * quantity)));
                    quantityToServer--;
                    qtyButtonClicked = MINUS_CLICKED;
                }
            }
        });
        //When we click plus button.
        binding.tvPlus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                quantity++;
                binding.tvNoOfItems.setText("" + quantity);
                binding.btnAddToCart.setText(strAddToBasket + String.format("%,.2f", (Float.parseFloat(price) * quantity)));
                quantityToServer++;
                qtyButtonClicked = PLUS_CLICKED;
            }
        });

        binding.btnAddToCart.setOnClickListener(_OnClickListener);
    }

    private void initialize()
    {
        final StringBuilder strFoodItemName = new StringBuilder();
        strFoodItemName.append(foodMenuDto.itemName).append("(").append(foodItemSizeDto.sizeName).append(")");
        final String desc = foodMenuDto.foodDesc;
        price = foodItemSizeDto.mainPrice;
        strFoodItemId = foodMenuDto.foodItemId;
        strFoodItemSizeId = foodItemSizeDto.sizeId;
        binding.tvItemTitle.setText(strFoodItemName.toString());
        binding.tvItemDesc.setText(desc);
        binding.tvItemPrice.setText(strCurrencySymbol + price);
        quantity=1;
        quantityToServer = 1;
        itemExist = false;
        qtyButtonClicked = 0;

        //Check for items existence and load the quantity
        for(CartDetailDto cartDetailDto:cartList)
        {
            if(strFoodItemSizeId.equals(cartDetailDto.sizeId))
            {
                /** Api has been changed on 21/05/2020-Code commented
                 * Clien requirement neeta mam...changed on 10/06/2020-code uncommented**/
                itemExist=true;
                quantity = Integer.parseInt(cartDetailDto.quantity);
                quantityToServer=0;
            }
        }
        binding.tvNoOfItems.setText("" + quantity);
        binding.btnAddToCart.setText(strAddToBasket + String.format("%,.2f", (Float.parseFloat(price) * quantity)));
        binding.tvPlus.setOnClickListener(_OnClickListener);
        binding.tvMinus.setOnClickListener(_OnClickListener);
        binding.btnAddToCart.setOnClickListener(_OnClickListener);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.tvPlus:
                     onPlusButtonClicked();
                     break;
                case R.id.tvMinus:
                     onMinusButtonClicked();
                     break;
                case R.id.btnAddToCart:
                     onAddToCartButtonClicked();
                     break;
            }
        }
    };

    private void onPlusButtonClicked()
    {
        if (quantity > 1)
        {
            quantity--;
            binding.tvNoOfItems.setText("" + quantity);
            binding.btnAddToCart.setText(strAddToBasket + String.format("%,.2f", (Float.parseFloat(price) * quantity)));
            quantityToServer--;
            qtyButtonClicked=MINUS_CLICKED;
        }
    }

    private void  onMinusButtonClicked()
    {
        if (quantity > 1)
        {
            quantity--;
            binding.tvNoOfItems.setText("" + quantity);
            binding.btnAddToCart.setText(strAddToBasket + String.format("%,.2f", (Float.parseFloat(price) * quantity)));
            quantityToServer--;
            qtyButtonClicked=MINUS_CLICKED;
        }
    }

    private void onAddToCartButtonClicked()
    {
        StringBuilder strQty = new StringBuilder();
        switch (qtyButtonClicked)
        {
            case MINUS_CLICKED:
                 if(itemExist)
                 {
                    strQty.append(quantityToServer);
                 }
                 else
                 {
                    strQty.append(quantityToServer);
                 }
                 if(addToCartClickListener!=null)
                 {
                     addToCartClickListener.onAddToCartClicked(strFoodItemId,strQty.toString(),strFoodItemSizeId);
                     dismiss();
                 }
                 break;
            case PLUS_CLICKED:
                 strQty.append(quantityToServer);
                 if(addToCartClickListener!=null)
                 {
                     addToCartClickListener.onAddToCartClicked(strFoodItemId, strQty.toString(),strFoodItemSizeId);
                     dismiss();
                 }
                 break;
            default:
                 if(!itemExist)
                 {
                     strQty.append(quantityToServer);
                     if(addToCartClickListener!=null)
                     {
                         addToCartClickListener.onAddToCartClicked(strFoodItemId, strQty.toString(),strFoodItemSizeId);
                         dismiss();
                     }
                 }
                 break;
        }
    }

}
