package com.localvalu.directory.detail.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.detail.dto.BusinessDetailsResult;

import java.io.Serializable;

public class BusinessDetailResultWrapper implements Serializable {

    @SerializedName("BusinessDetailsResult")
    @Expose
    public BusinessDetailsResult businessDetailsResult;
}
