package com.localvalu.directory.detail.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessDetailServiceDto implements Serializable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("service_name")
    @Expose
    public String serviceName;
}
