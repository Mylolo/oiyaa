package com.localvalu.directory.mycart;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.material.appbar.AppBarLayout;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentCartBinding;
import com.localvalu.directory.detail.DirectoryDetailFragment;
import com.localvalu.directory.mycart.adapter.LocalCartItemListAdapter;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.eats.detail.wrapper.OrderNowResultResponse;
import com.localvalu.eats.mycart.CheckOutFragment;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.eats.mycart.dto.CheckoutDto;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.squareup.picasso.Picasso;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.utility.AppUtils.BMT_LOCAL;
import static com.utility.AppUtils.OPTION_COLLECTION;
import static com.utility.AppUtils.OPTION_DELIVERY;


public class LocalCartFragment extends BaseFragment
{
    private static final String TAG = "LocalCartFragment";

    private FragmentCartBinding binding;
    private GridLayoutManager gridLayoutManager;
    private LocalCartItemListAdapter localCartItemListAdapter;

    private int colorEats,colorLocal;
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private String currentDate;

    private ArrayList<CartDetailDto> cartDetailsFinal;
    private String taxPercentage;
    private String deliveryCharge;
    private Float price = 0.0f;
    private String delivery = "";
    private HostActivityListener hostActivityListener;
    private BusinessDetailInfo businessDetailInfo;

    // TODO: Rename and change types and number of parameters
    public static LocalCartFragment newInstance(BusinessDetailInfo businessDetailInfo)
    {
        LocalCartFragment localCartFragment = new LocalCartFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppUtils.BUNDLE_BUSINESS_DETAIL,businessDetailInfo);
        localCartFragment.setArguments(bundle);
        return localCartFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        colorLocal = ContextCompat.getColor(requireContext(), R.color.colorLocal);
        buildObjectForHandlingAPI();
        readFromBundle();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date cal = (Date) Calendar.getInstance().getTime();
        currentDate = formatter.format(cal);
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentCartBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
        fetchAPI();
    }

    private void setProperties()
    {
        binding.layoutError.btnRetry.setOnClickListener(_OnClickListener);
        binding.btnContinue.setOnClickListener(_OnClickListener);
        binding.btnContinue.setOnClickListener(_OnClickListener);
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        binding.rvCartItems.setLayoutManager(gridLayoutManager);
        setBasicInfo();
        showContents();
    }

    private void setUpActionBar()
    {
        int collapsedColor = ContextCompat.getColor(requireContext(), R.color.colorEats);
        int expandedColor = ContextCompat.getColor(requireContext(), R.color.colorWhite);
        binding.toolbarLayout.toolbarEats.getNavigationIcon().setColorFilter(expandedColor, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.btnActionContactMe.setVisibility(View.GONE);
        binding.toolbarLayout.toolbarEats.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });
        binding.toolbarLayout.appBarEatDetails.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener()
        {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset)
            {
                float percentage = ((float)Math.abs(verticalOffset)/appBarLayout.getTotalScrollRange());
                binding.toolbarLayout.tvCurrentPromotion.setScaleX(1-percentage);
                binding.toolbarLayout.tvCurrentPromotion.setScaleY(1-percentage);
                binding.toolbarLayout.cnlInner.setAlpha(0.9f-percentage);
                if(percentage==0)
                {
                    binding.toolbarLayout.btnActionContactMe.setTextColor(expandedColor);
                    binding.toolbarLayout.btnActionContactMe.getIcon().setColorFilter(expandedColor, PorterDuff.Mode.SRC_ATOP);
                    binding.toolbarLayout.toolbarEats.getNavigationIcon().setColorFilter(expandedColor, PorterDuff.Mode.SRC_ATOP);
                    binding.toolbarLayout.viewSeparator.setVisibility(View.GONE);
                }
                else if(percentage==1)
                {
                    binding.toolbarLayout.btnActionContactMe.setTextColor(collapsedColor);
                    binding.toolbarLayout.btnActionContactMe.getIcon().setColorFilter(collapsedColor, PorterDuff.Mode.SRC_ATOP);
                    binding.toolbarLayout.toolbarEats.getNavigationIcon().setColorFilter(collapsedColor, PorterDuff.Mode.SRC_ATOP);
                    binding.toolbarLayout.viewSeparator.setVisibility(View.VISIBLE);
                }
            }
        });
        binding.toolbarLayout.appBarEatDetails.setVisibility(View.GONE);
    }

    private void readFromBundle()
    {
        try
        {
            if(getArguments()!=null)
            {
                businessDetailInfo = (BusinessDetailInfo) getArguments().getSerializable(AppUtils.BUNDLE_BUSINESS_DETAIL);
            }

        }
        catch (Exception e)
        {

        }
    }

    public void setBasicInfo()
    {
        binding.toolbarLayout.appBarEatDetails.setVisibility(View.VISIBLE);
        binding.tvName.setText(businessDetailInfo.tradingname);
        if (!businessDetailInfo.currentPromotionValue.equals(""))
        {
            binding.toolbarLayout.tvCurrentPromotion.setText(businessDetailInfo.currentPromotionValue + "% Off");
        }
        else
        {
            binding.toolbarLayout.tvCurrentPromotion.setText("0% Off");
        }
        binding.toolbarLayout.cnlInner.transitionToStart();
        binding.toolbarLayout.cnlInner.transitionToEnd();
        if (!businessDetailInfo.businessCoverImage.equals(""))
        {
            binding.toolbarLayout.ivCoverImage.setVisibility(View.VISIBLE);

            Picasso.with(getActivity()).load(businessDetailInfo.businessCoverImage)
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo ).into(binding.toolbarLayout.ivCoverImage);
            binding.toolbarLayout.ivCoverImage.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }
        else
        {
            Picasso.with(getActivity()).load(R.drawable.app_logo)
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo ).into(binding.toolbarLayout.ivCoverImage);
            binding.toolbarLayout.ivCoverImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }
        if(businessDetailInfo.retailerTypeId!=null)
        {
            if(!businessDetailInfo.retailerTypeId.equals(""))
            {
                switch (Integer.parseInt(businessDetailInfo.retailerTypeId))
                {
                    case AppUtils.BMT_EATS:
                         binding.tvTextAllergyInstructions.setVisibility(View.VISIBLE);
                         binding.etAllergyInstructions.setVisibility(View.VISIBLE);
                         break;
                    case AppUtils.BMT_LOCAL:
                         binding.tvTextAllergyInstructions.setVisibility(View.GONE);
                         binding.etAllergyInstructions.setVisibility(View.GONE);
                         binding.etSpecialInstructions.setHint(getString(R.string.lbl_add_a_note));
                         break;
                }

            }
        }
    }

    private void fetchAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId", businessDetailInfo.business_id);
            objMain.put("requestFor", "MyCartDetails");
            objMain.put("Distance", "12");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<OrderNowResultResponse> dtoCall = apiInterface.orderOnlineLocal(body);
        dtoCall.enqueue(new Callback<OrderNowResultResponse>()
        {
            @Override
            public void onResponse(Call<OrderNowResultResponse> call, Response<OrderNowResultResponse> response)
            {
                mProgressDialog.dismiss();
                if (response.body().businessDetailsResult.errorDetails.errorCode == 0)
                {
                    if (response.body().businessDetailsResult.orderNowDto.cartDetails.size() == 0)
                    {
                        binding.btnContinue.setVisibility(View.GONE);
                    }
                    else
                    {
                        binding.btnContinue.setVisibility(View.VISIBLE);
                    }
                    cartDetailsFinal = response.body().businessDetailsResult.orderNowDto.cartDetails;
                    taxPercentage = response.body().businessDetailsResult.orderNowDto.taxPercentage;
                    deliveryCharge = response.body().businessDetailsResult.orderNowDto.deliveryCharge.cost;
                    price = 0.0f;
                    for (CartDetailDto cartDetailsFinalEach : cartDetailsFinal)
                    {
                        String strRate = cartDetailsFinalEach.rate.trim();
                        if(strRate.equals("")) strRate="0.0";
                        try
                        {
                            price = price + Float.parseFloat(cartDetailsFinalEach.rate);
                        }
                        catch (Exception ex)
                        {
                            ex.printStackTrace();
                        }
                    }
                    binding.tvTotalPrice.setText("£" + String.format("%,.2f", price));
                    setAdapter(response.body().businessDetailsResult.orderNowDto.cartDetails);
                    updateInOrderPage(cartDetailsFinal);
                }
                else
                {
                    DialogUtility.showMessageWithOk(response.body().businessDetailsResult.errorDetails.errorMessage, getActivity());
                }
            }

            @Override
            public void onFailure(Call<OrderNowResultResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void setAdapter(final ArrayList<CartDetailDto> cartDetails)
    {
        localCartItemListAdapter = new LocalCartItemListAdapter(getActivity(), cartDetails);
        localCartItemListAdapter.setOnClickListener(new LocalCartItemListAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClickMinus(int position)
            {
                if (!cartDetails.get(position).quantity.equals("0"))
                {
                    /** Api has been changed on 21/05/2020*/
                    callAPIForAddToCart(cartDetails.get(position).foodItemId,cartDetails.get(position).sizeId,false);
                    /*if (!cartDetails.get(position).quantity.equals("0"))
                    {
                        callAPIForAddToCart(cartDetails.get(position).foodItemId,true);
                    }*/
                }
            }

            @Override
            public void onItemClickPlus(int position)
            {
                /** Api has been changed on 21/05/2020*/
                callAPIForAddToCart(cartDetails.get(position).foodItemId,cartDetails.get(position).sizeId,true);
                /*if (!cartDetails.get(position).quantity.equals("0"))
                {
                     callAPIForAddToCart(cartDetails.get(position).foodItemId,true);
                }*/
            }
        });
        binding.rvCartItems.setAdapter(localCartItemListAdapter);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    /**
     * We should pass exact Quantity pass if we want to replace quantity instead of sending -1 or 1 value.
     * Api change on 21/05/2020
     * @param id
     * @param add
     */
    private void callAPIForAddToCart(String id,String sizeId,boolean add)
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            String quantity="";
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId", businessDetailInfo.business_id);
            objMain.put("requestFor", "AddItemsToCart");
            objMain.put("global_unique_id", "1");
            objMain.put("itemId", id);
            if(add)quantity = "1";
            else quantity = "-1";
            objMain.put("quantity", quantity);
            objMain.put("sizeId", sizeId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<OrderNowResultResponse> dtoCall = apiInterface.orderOnlineLocal(body);
        dtoCall.enqueue(new Callback<OrderNowResultResponse>()
        {
            @Override
            public void onResponse(Call<OrderNowResultResponse> call, Response<OrderNowResultResponse> response)
            {
                mProgressDialog.dismiss();
                if (response.body().addToCartResult.errorDetails.errorCode == 0)
                {
                    Toast.makeText(getActivity(), "Cart updated successfully.", Toast.LENGTH_LONG).show();
                    fetchAPI();
                }
                else
                {
                    DialogUtility.showMessageWithOk(response.body().addToCartResult.errorDetails.errorMessage, getActivity());
                }
            }

            @Override
            public void onFailure(Call<OrderNowResultResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void updateInOrderPage(List<CartDetailDto> cartList)
    {
        DirectoryDetailFragment directoryDetailFragment = (DirectoryDetailFragment) getActivity().getSupportFragmentManager()
                .findFragmentByTag(DirectoryDetailFragment.class.getName());
        if(directoryDetailFragment!=null)
        {
            directoryDetailFragment.updateCartFragment((ArrayList<CartDetailDto>) cartList);
        }

    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                 getActivity().onBackPressed();
                 break;
            case R.id.btnContinue:
                 switch (binding.radioGroupDelivery.getCheckedRadioButtonId())
                 {
                     case R.id.radioDelivery:
                          delivery = OPTION_DELIVERY;
                          break;
                     case R.id.radioCollection:
                          delivery = OPTION_COLLECTION;
                          break;
                 }
                 gotoCheckOutFragment();
                 break;
            case R.id.btnRetry:
                 fetchAPI();
                 break;
        }
    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.nestedScrollView.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showContents()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.nestedScrollView.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage,boolean retry)
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.nestedScrollView.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if(retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

    private void gotoCheckOutFragment()
    {
        CheckoutDto checkoutDto = new CheckoutDto();
        checkoutDto.businessDetailInfo = businessDetailInfo;
        checkoutDto.cartList = cartDetailsFinal;
        checkoutDto.orderTotal = String.valueOf(price);
        checkoutDto.delivery = delivery;
        checkoutDto.deliveryRate = deliveryCharge;
        checkoutDto.tax = taxPercentage;
        checkoutDto.specialInstructions = binding.etSpecialInstructions.getText().toString();
        checkoutDto.allergyInstructions = binding.etAllergyInstructions.getText().toString();
        checkoutDto.isOrderFromTableOrStore = false;
        checkoutDto.isOrderFromEats=false;
        //LocalCheckOutFragment localCheckOutFragment = LocalCheckOutFragment.newInstance(checkoutDto);
        //hostActivityListener.updateFragment(localCheckOutFragment, true, LocalCheckOutFragment.class.getName(), true);
        CheckOutFragment checkOutFragment = CheckOutFragment.newInstance(checkoutDto);
        hostActivityListener.updateFragment(checkOutFragment, true, CheckOutFragment.class.getName(), true);
    }


}
