package com.localvalu.directory.mycart.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.R;
import com.localvalu.eats.mycart.dto.CartDetailDto;

import java.util.ArrayList;

public class LocalCheckoutItemListAdapter extends RecyclerView.Adapter<LocalCheckoutItemListAdapter.ViewHolder>
{
    public static final String TAG = LocalCheckoutItemListAdapter.class.getSimpleName();
    private ArrayList<CartDetailDto> cartDetails;
    private OnItemClickListener onClickListener;

    public LocalCheckoutItemListAdapter(Activity activity, ArrayList<CartDetailDto> cartDetails)
    {
        this.cartDetails = cartDetails;
    }

    @NonNull
    @Override
    public LocalCheckoutItemListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_checkout_item_list, viewGroup, false);
        return new LocalCheckoutItemListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LocalCheckoutItemListAdapter.ViewHolder viewHolder, final int i)
    {
        final CartDetailDto cartDetailDto = cartDetails.get(i);
        StringBuilder strTitle = new StringBuilder();
        strTitle.append(cartDetails.get(i).itemName);
        if(!cartDetailDto.sizeName.equals(""))
        {
            strTitle.append("(").append(cartDetailDto.sizeName).append(")");
        }
        viewHolder.tvItemTitle.setText(strTitle.toString());
        viewHolder.tvNoOfItems.setText(cartDetails.get(i).quantity);
        viewHolder.tvTotalPrice.setText("£" + cartDetails.get(i).rate);
    }

    @Override
    public int getItemCount()
    {
        return cartDetails.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemClickMinus(int position);

        void onItemClickPlus(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvItemTitle = itemView.findViewById(R.id.tvItemTitle);
        TextView tvNoOfItems = itemView.findViewById(R.id.tvNoOfItems);
        TextView tvTotalPrice = itemView.findViewById(R.id.tvTotalPrice);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }
}
