package com.localvalu.directory.mycart.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.R;
import com.localvalu.eats.mycart.dto.CartDetailDto;

import java.util.ArrayList;

public class LocalCartItemListAdapter extends RecyclerView.Adapter<LocalCartItemListAdapter.ViewHolder>
{
    public static final String TAG = LocalCartItemListAdapter.class.getSimpleName();
    private ArrayList<CartDetailDto> cartDetails;
    private OnItemClickListener onClickListener;
    private int colorLocal;
    private Context context;

    public LocalCartItemListAdapter(Activity activity, ArrayList<CartDetailDto> cartDetails)
    {
        this.cartDetails = cartDetails;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
        colorLocal = ContextCompat.getColor(context, R.color.colorLocal);
    }

    @NonNull
    @Override
    public LocalCartItemListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cart_item_list, viewGroup, false);
        return new LocalCartItemListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LocalCartItemListAdapter.ViewHolder viewHolder, final int i)
    {
        final CartDetailDto cartDetailDto = cartDetails.get(i);
        StringBuilder strTitle = new StringBuilder();
        strTitle.append(cartDetails.get(i).itemName);
        if (!cartDetailDto.sizeName.equals(""))
        {
            strTitle.append("(").append(cartDetailDto.sizeName).append(")");
        }
        viewHolder.tvItemTitle.setText(strTitle.toString());
        viewHolder.tvNoOfItems.setText(cartDetailDto.quantity);
        viewHolder.tvTotalPrice.setText("£" + cartDetailDto.rate);
        viewHolder.tvMinus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemClickMinus(i);
            }
        });
        viewHolder.tvPlus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemClickPlus(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return cartDetails.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemClickMinus(int position);

        void onItemClickPlus(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvItemTitle = itemView.findViewById(R.id.tvItemTitle);
        TextView tvNoOfItems = itemView.findViewById(R.id.tvNoOfItems);
        TextView tvMinus = itemView.findViewById(R.id.tvMinus);
        TextView tvPlus = itemView.findViewById(R.id.tvPlus);
        TextView tvTotalPrice = itemView.findViewById(R.id.tvTotalPrice);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
            Drawable drawableMinus = tvMinus.getBackground();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            {
                drawableMinus.setColorFilter(new BlendModeColorFilter(colorLocal, BlendMode.SRC_ATOP));
            }
            else
            {
                drawableMinus.setColorFilter(colorLocal, PorterDuff.Mode.SRC_ATOP);
            }
            tvMinus.setBackground(drawableMinus);

            Drawable drawablePlus = tvPlus.getBackground();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            {
                drawablePlus.setColorFilter(new BlendModeColorFilter(colorLocal, BlendMode.SRC_ATOP));
            }
            else
            {
                drawablePlus.setColorFilter(colorLocal, PorterDuff.Mode.SRC_ATOP);
            }
            tvPlus.setBackground(drawablePlus);
        }
    }
}
