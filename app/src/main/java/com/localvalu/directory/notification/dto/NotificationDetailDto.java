package com.localvalu.directory.notification.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationDetailDto implements Serializable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("notification")
    @Expose
    public String notification;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("set_for")
    @Expose
    public String set_for;
    @SerializedName("user_saved")
    @Expose
    public String user_saved;
}
