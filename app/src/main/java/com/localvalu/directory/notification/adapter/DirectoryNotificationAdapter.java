package com.localvalu.directory.notification.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.directory.notification.dto.NotificationDetailDto;

import java.util.ArrayList;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class DirectoryNotificationAdapter extends RecyclerView.Adapter<DirectoryNotificationAdapter.ViewHolder> {
    public static final String TAG = DirectoryNotificationAdapter.class.getSimpleName();
    private Activity activity;
    private ArrayList<NotificationDetailDto> detail;
    private OnItemClickListener onClickListener;

    public DirectoryNotificationAdapter(Activity activity, ArrayList<NotificationDetailDto> detail) {
        this.activity = activity;
        this.detail = detail;
        System.out.println("detail.size() : " + detail.size());
    }

    @NonNull
    @Override
    public DirectoryNotificationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_notification_local_list, viewGroup, false);
        return new DirectoryNotificationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DirectoryNotificationAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.tv_name.setText(detail.get(i).notification);
        if (detail.get(i).user_saved.equals("yes")) {
            viewHolder.cb_settings.setChecked(true);
        } else {
            viewHolder.cb_settings.setChecked(false);
        }

        viewHolder.cb_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onItemClick(i);
//                if(detail.get(i).user_saved.equals("yes")) {
//                    viewHolder.cb_settings.setChecked(false);
//                } else {
//                    viewHolder.cb_settings.setChecked(true);
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return detail.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name = itemView.findViewById(R.id.tv_name);
        CheckBox cb_settings = itemView.findViewById(R.id.cb_settings);

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
