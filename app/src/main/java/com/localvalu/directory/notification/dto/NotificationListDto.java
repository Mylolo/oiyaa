package com.localvalu.directory.notification.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class NotificationListDto implements Serializable {

    @SerializedName("pushNotifications")
    @Expose
    public ArrayList<NotificationDetailDto> pushLists;
    @SerializedName("emailNotifications")
    @Expose
    public ArrayList<NotificationDetailDto> emailLists;
}
