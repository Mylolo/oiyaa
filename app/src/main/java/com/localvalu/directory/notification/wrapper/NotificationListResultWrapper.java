package com.localvalu.directory.notification.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.notification.dto.NotificationListResult;

import java.io.Serializable;

public class NotificationListResultWrapper implements Serializable {

    @SerializedName("NotificationAllList")
    @Expose
    public NotificationListResult notificationListResult;
}
