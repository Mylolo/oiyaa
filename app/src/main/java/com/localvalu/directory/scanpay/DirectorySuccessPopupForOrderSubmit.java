package com.localvalu.directory.scanpay;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.base.MainActivity;
import com.utility.ActivityController;
import com.utility.AppUtils;

import java.util.Random;

@SuppressLint("NewApi")
public class DirectorySuccessPopupForOrderSubmit extends Activity
{

    private MediaPlayer player;
    private ImageView iv_animation;
    private AnimationDrawable frameAnimation;
    private Button btn_next, btn_collect;
    private LinearLayout ll_collect, ll_collecting;
    private TextView tv_token, tv_old_token, tv_new_token;
    final int min = 1;
    final int max = 9;
    final int[] sounds = new int[10];
    private int navigationType;

    private String tokensEarned, bonusTokens, newTokenBalance;
    private String strCurrencySymbol;
    private String strCurrencyLetter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        getWindow().getDecorView().findViewById(android.R.id.content).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setFinishOnTouchOutside(false);
        setContentView(R.layout.directory_popup_success_for_order_submit);
        readFromBundle();
        initSounds();
        init();
    }

    private void init()
    {
        ll_collect = findViewById(R.id.ll_collect);
        ll_collecting = findViewById(R.id.ll_collecting);
        btn_next = findViewById(R.id.btn_next);

        iv_animation = findViewById(R.id.iv_animation);
        iv_animation.setBackgroundResource(R.drawable.coin_animation);
        frameAnimation = (AnimationDrawable) iv_animation.getBackground();
        player = MediaPlayer.create(this, sounds[getRandomSound()]);
        tv_token = findViewById(R.id.tv_token);

        tv_new_token = findViewById(R.id.tv_new_token);
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        setData();
    }

    public void initSounds()
    {
        sounds[0] = R.raw.coin_drops;
        sounds[1] = R.raw.sockitto;
        sounds[2] = R.raw.sportsevent;
        sounds[3] = R.raw.urcrazee;
        sounds[4] = R.raw.wow1;
        sounds[5] = R.raw.wow2;
        sounds[6] = R.raw.coin_drops;
        sounds[7] = R.raw.yeah;
        sounds[8] = R.raw.yeah1;
        sounds[9] = R.raw.startofhorserace;
    }

    private void readFromBundle()
    {
        tokensEarned = getIntent().getExtras().getString("tokensEarned");
        bonusTokens = getIntent().getExtras().getString("bonusTokens");
        newTokenBalance = getIntent().getExtras().getString("newTokenBalance");
        navigationType = getIntent().getExtras().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);

        tokensEarned = tokensEarned.replace(",", "");
        bonusTokens = bonusTokens.replace(",", "");
        newTokenBalance = newTokenBalance.replace(",", "");
    }

    private void setData()
    {
        double dTtokensEarned = Double.parseDouble(tokensEarned);
        dTtokensEarned = (dTtokensEarned * 100.0) / 100.0;

        double dBonusTokens = Double.parseDouble(bonusTokens);
        dBonusTokens = (dBonusTokens * 100.0) / 100.0;

        double dNewTokenBalance = Double.parseDouble(newTokenBalance);
        dNewTokenBalance = (dNewTokenBalance * 100.0) / 100.0;

        double dOldToken = dNewTokenBalance - dTtokensEarned;
        dOldToken = (dOldToken * 100.0) / 100.0;

        tv_token.setText(strCurrencySymbol + dTtokensEarned);
        tv_old_token.setText(strCurrencyLetter + dOldToken);
        tv_new_token.setText(strCurrencyLetter + dNewTokenBalance);

    }

    public int getRandomSound()
    {
        return new Random().nextInt((max - min) + 1) + min;
    }

    public void onButtonClick(View v)
    {
        try
        {
            Bundle bundle = new Bundle();
            switch (v.getId())
            {
                case R.id.iv_close:
                    frameAnimation.stop();
                    player.release();
                    btn_next.setVisibility(View.VISIBLE);
                    btn_collect.setVisibility(View.GONE);
                    ll_collecting.setVisibility(View.VISIBLE);
                    ll_collect.setVisibility(View.GONE);
                    bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, navigationType);
                    ActivityController.startNextActivity(this, MainActivity.class, bundle, true);
                    finish();
                    break;
                case R.id.btn_nextall:
                    Intent intent = getIntent();
                    bundle.putString("review", "yes");
                    intent.putExtras(bundle);
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                case R.id.btn_next:
                    frameAnimation.start();
                    player.start();
                    player.setLooping(false);
                    btn_next.setVisibility(View.GONE);
                    btn_collect.setVisibility(View.VISIBLE);
                    break;

            }
        }
        catch (Exception e)
        {

        }
    }

}