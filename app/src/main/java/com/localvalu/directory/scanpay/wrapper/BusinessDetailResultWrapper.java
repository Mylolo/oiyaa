package com.localvalu.directory.scanpay.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.scanpay.dto.BusinessDetailResult;

import java.io.Serializable;

public class BusinessDetailResultWrapper implements Serializable {
    @SerializedName("BusinessInfoResults")
    @Expose
    public BusinessDetailResult businessDetailResult;
}
