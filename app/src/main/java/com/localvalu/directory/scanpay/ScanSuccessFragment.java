package com.localvalu.directory.scanpay;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.MainActivity;
import com.localvalu.base.HostActivityListener;
import com.localvalu.common.review.ReviewsAddFragment;
import com.localvalu.directory.scanpay.wrapper.BusinessDetailResultWrapper;
import com.localvalu.directory.scanpay.wrapper.StarPrinterScanQRCodeResultWrapper;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class ScanSuccessFragment extends BaseFragment
{

    private static final String TAG = "ScanSuccessFragment";

    //Toolbar
    private Toolbar toolbar;
    private TextView tvTitle;
    private int colorTheme;
    private int colorThemeBackground;


    private String qrCode = "";

    private String tokensEarned, bonusTokens, newTokenBalance, retailerID;
    private String businessId, businessImage, businessName, businessType, businessAddress;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;

    private NestedScrollView nestedScrollView;
    private TextView tv_token_reedemed, tv_tokens_earned, tv_token_balance, tv_balance_pay, tv_token_content, tv_token_balance_actual;
    private AppCompatImageView ivSuccess;
    private ProgressBar progressBar;
    private int navigationType;
    private HostActivityListener hostActivityListener;

    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private static int CLAIM_REQUEST = 100;

    // TODO: Rename and change types and number of parameters
    public static ScanSuccessFragment newInstance()
    {
        return new ScanSuccessFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_directory_scanpay_success, container, false);
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        buildObjectForHandlingAPI();
        readFromBundle();
        initView();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener =(HostActivityListener) context;
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
        fetchForPrinterAPI();
    }

    private void initView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        nestedScrollView = view.findViewById(R.id.nestedScrollView);
        tv_token_reedemed = view.findViewById(R.id.tv_token_reedemed);
        tv_tokens_earned = view.findViewById(R.id.tv_tokens_earned);
        tv_token_balance = view.findViewById(R.id.tv_token_balance);
        tv_balance_pay = view.findViewById(R.id.tv_balance_pay);
        tv_token_content = view.findViewById(R.id.tv_token_content);
        tv_token_balance_actual = view.findViewById(R.id.tv_token_balance_actual);
        ivSuccess = view.findViewById(R.id.ivSuccess);
        progressBar = view.findViewById(R.id.progressBar);
    }

    private void setUpActionBar()
    {
        toolbar = view.findViewById(R.id.toolbar);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setTextColor(colorTheme);
        toolbar.getNavigationIcon().setColorFilter(colorTheme, PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private void readFromBundle()
    {
        try
        {
            qrCode = "" + getArguments().getString("qrCode");
            navigationType = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
            switch (navigationType)
            {
                case AppUtils.BMT_TRACK_AND_TRACE:
                case AppUtils.BMT_EATS:
                    colorTheme = ContextCompat.getColor(requireContext(), R.color.colorEats);
                    colorThemeBackground = ContextCompat.getColor(requireContext(), R.color.colorEatsLightBackground);
                    break;
                case AppUtils.BMT_LOCAL:
                    colorTheme = ContextCompat.getColor(requireContext(), R.color.colorEats);
                    colorThemeBackground = ContextCompat.getColor(requireContext(), R.color.colorLocalLightBackground);
                    break;
                case AppUtils.BMT_MALL:
                    colorTheme = ContextCompat.getColor(requireContext(), R.color.colorMall);
                    colorThemeBackground = ContextCompat.getColor(requireContext(), R.color.colorMallLightBackground);
                    break;

            }
        }
        catch (Exception e)
        {

        }
    }

    private void setProperties()
    {
        nestedScrollView.setVisibility(View.GONE);
        view.setBackgroundColor(colorThemeBackground);
        Drawable drawable = ivSuccess.getDrawable();
        if(drawable!=null)
        {
            drawable.setColorFilter(colorTheme, PorterDuff.Mode.SRC_ATOP);
            ivSuccess.setImageDrawable(drawable);
        }
    }

    private void fetchForPrinterAPI()
    {
        //mProgressDialog.show();
        showProgress();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("Scancode", qrCode);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        System.out.println("Request ::  " + objMain.toString());

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<StarPrinterScanQRCodeResultWrapper> dtoCall = apiInterface.scanPay(body);
        dtoCall.enqueue(new Callback<StarPrinterScanQRCodeResultWrapper>()
        {
            @Override
            public void onResponse(Call<StarPrinterScanQRCodeResultWrapper> call, Response<StarPrinterScanQRCodeResultWrapper> response)
            {
                //mProgressDialog.dismiss();
                progressBar.setVisibility(View.GONE);

                try
                {
                    if (response.isSuccessful())
                    {
                        StarPrinterScanQRCodeResultWrapper starPrinterScanQRCodeResultWrapper = response.body();
                        System.out.println("response ::  " + starPrinterScanQRCodeResultWrapper.toString());
                        if (response.body().scanQRCodeResult.errorDetails.errorCode == 0)
                        {
                            showContent(response);
                        }
                        else
                        {
                            showErrorFragment((response.body().scanQRCodeResult.errorDetails.errorMessage));
                            //DialogUtility.showMessageWithOk(response.body().scanQRCodeResult.errorDetails.errorMessage, getActivity());
                        }
                    }
                    else
                    {
                        showErrorFragment(getString(R.string.network_unavailable));
                        //DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                    }
                }
                catch (Exception e)
                {
                    showErrorFragment(getString(R.string.server_alert));
                }
            }

            @Override
            public void onFailure(Call<StarPrinterScanQRCodeResultWrapper> call, Throwable t)
            {
                //mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    showErrorFragment(getString(R.string.network_unavailable));
                    //DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    showErrorFragment(getString(R.string.server_alert));
                //DialogUtility.showMessageWithOk("Connection Error, Try again", getActivity());
            }
        });
    }

    private void showProgress()
    {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void showContent(Response<StarPrinterScanQRCodeResultWrapper> response)
    {
        nestedScrollView.setVisibility(View.VISIBLE);
        businessId = response.body().scanQRCodeResult.scanQRCodeData.businessIdOutput;
        retailerID = response.body().scanQRCodeResult.scanQRCodeData.retailerIdOutput;
        tokensEarned = String.format("%,.2f", (Float.parseFloat(response.body().scanQRCodeResult.scanQRCodeData.tokensearned)));
        bonusTokens = String.format("%,.2f", (Float.parseFloat(response.body().scanQRCodeResult.scanQRCodeData.bonusTokens)));
        newTokenBalance = String.format("%,.2f", (Float.parseFloat(response.body().scanQRCodeResult.scanQRCodeData.newTokenBalance)));

        tv_token_reedemed.setText(strCurrencyLetter + String.format("%,.2f", (Float.parseFloat(response.body().scanQRCodeResult.scanQRCodeData.tokenApplied))));
        tv_tokens_earned.setText(strCurrencyLetter + String.format("%,.2f", (Float.parseFloat(response.body().scanQRCodeResult.scanQRCodeData.tokensearned))));
        tv_token_balance.setText(strCurrencyLetter + String.format("%,.2f", (Float.parseFloat(response.body().scanQRCodeResult.scanQRCodeData.newTokenBalance))));
        tv_balance_pay.setText(strCurrencySymbol + String.format("%,.2f", (Float.parseFloat(response.body().scanQRCodeResult.scanQRCodeData.balanceToPay))));
        tv_token_content.setText("Congratulations on being a Localist, you have earned "
                + strCurrencySymbol + String.format("%,.2f", (Float.parseFloat(response.body().scanQRCodeResult.scanQRCodeData.tokensearned)))
                + " Loyalty Anywhere Tokens.");
        tv_token_balance_actual.setText(strCurrencyLetter + String.format("%,.2f", (Float.parseFloat(response.body().scanQRCodeResult.scanQRCodeData.newTokenBalance))));
        callAPI(businessId);
    }

    private void callAPI(String businessId)
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("BusinessId", businessId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<BusinessDetailResultWrapper> dtoCall = apiInterface.businessDetailFrom(body);
        dtoCall.enqueue(new Callback<BusinessDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<BusinessDetailResultWrapper> call, Response<BusinessDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.isSuccessful())
                    {
                        if (response.body().businessDetailResult.errorDetails.errorCode == 0)
                        {
                            ScanSuccessFragment.this.businessId = response.body().businessDetailResult.businessDetails.businessDetails.get(0).business_id;
                            businessName = response.body().businessDetailResult.businessDetails.businessDetails.get(0).tradingname;
                            businessImage = response.body().businessDetailResult.businessDetails.businessDetails.get(0).logo;
                            businessType = response.body().businessDetailResult.businessDetails.businessDetails.get(0).businessType;
                            businessAddress = response.body().businessDetailResult.businessDetails.businessDetails.get(0).address1;
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(response.body().businessDetailResult.errorDetails.errorMessage, getActivity());
                            getActivity().onBackPressed();
                        }
                    }
                    else
                    {
                        mProgressDialog.dismiss();
                        // error case
                        switch (response.code())
                        {
                            case 404:
                                Toast.makeText(getActivity(), getString(R.string.server_alert), Toast.LENGTH_SHORT).show();
                                getActivity().onBackPressed();
                                break;
                            case 500:
                                Toast.makeText(getActivity(), getString(R.string.server_alert), Toast.LENGTH_SHORT).show();
                                getActivity().onBackPressed();
                                break;
                            default:
                                Toast.makeText(getActivity(), getString(R.string.server_alert), Toast.LENGTH_SHORT).show();
                                getActivity().onBackPressed();
                                break;
                        }
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<BusinessDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    public void onButtonClick(View view)
    {
        Bundle bundle = new Bundle();
        switch (view.getId())
        {
            case R.id.imgBack:
                 getActivity().onBackPressed();
                 break;
            case R.id.btnReviewNowAndEarnMore:
                 ReviewsAddFragment reviewsAddFragment = ReviewsAddFragment.newInstance();
                 bundle.putString("section_type", "scan");
                 bundle.putString("businessId", businessId);
                 bundle.putString("order_id", "");
                 bundle.putString("businessImage", businessImage);
                 bundle.putString("businessName", businessName);
                 bundle.putString("businessType", businessType);
                 bundle.putString("businessAddress", businessAddress);
                 bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, navigationType);
                 reviewsAddFragment.setArguments(bundle);
                 hostActivityListener.updateFragment(reviewsAddFragment, true, ReviewsAddFragment.class.getName(), true);
                 break;
            case R.id.btnClaim:
                 Intent intent = new Intent(getActivity(), DirectorySuccessPopupForOrderSubmit.class);
                 intent.putExtra("tokensEarned", "" + tokensEarned);
                 intent.putExtra("bonusTokens", "" + bonusTokens);
                 intent.putExtra("newTokenBalance", "" + newTokenBalance);
                 bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, navigationType);
                 startActivityForResult(intent, CLAIM_REQUEST);
                 break;
        }
    }

    public void onActivityResult(final int requestCode, int resultCode, Intent data)
    {
        try
        {
            int unmaskedRequestCode = CLAIM_REQUEST & 0x0000ffff;

            if (unmaskedRequestCode == CLAIM_REQUEST)
            {
                if (resultCode == RESULT_OK)
                {
                    mProgressDialog.dismiss();

                    Bundle bundle = new Bundle();

                    if (data != null)
                    {
                        if (data.getExtras().getString("review").equals("yes"))
                        {
                            ScanReviewFragment scanReviewFragment = ScanReviewFragment.newInstance();
                            bundle.putString("section_type", "scan");
                            bundle.putString("businessId", businessId);
                            bundle.putString("order_id", "");
                            bundle.putString("businessImage", businessImage);
                            bundle.putString("businessName", businessName);
                            bundle.putString("businessType", businessType);
                            bundle.putString("businessAddress", businessAddress);
                            scanReviewFragment.setArguments(bundle);
                            ((MainActivity) getActivity()).addFragment(scanReviewFragment, true, ScanReviewFragment.class.getName(), true);
                        }
                    }
                }

                else
                {
                    getActivity().onBackPressed();
                    Log.w("DialogScanPay", "Warning: activity result not ok");
                }
            }

        }
        catch (Throwable e)
        {
            e.printStackTrace();
        }
    }

    private void showErrorFragment(String message)
    {
        hostActivityListener.onBackButtonPressed();
        ScanErrorFragment scanErrorFragment = ScanErrorFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, navigationType);
        bundle.putString(AppUtils.BUNDLE_SCAN_ERROR_MESSAGE, message);
        scanErrorFragment.setArguments(bundle);
        hostActivityListener.updateFragment(scanErrorFragment, true, ScanErrorFragment.class.getName(),true);
    }

}
