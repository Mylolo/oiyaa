package com.localvalu.directory.scanpay.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RetailerPromotion implements Serializable {

    @SerializedName("PromotionName")
    @Expose
    public String promotionName;

    @SerializedName("Startdate")
    @Expose
    public String startdate;

    @SerializedName("Enddate")
    @Expose
    public String enddate;

    @SerializedName("MONMorning")
    @Expose
    public String MONMorning;

    @SerializedName("MONAfternoon")
    @Expose
    public String MONAfternoon;

    @SerializedName("MONEvening")
    @Expose
    public String MONEvening;

    @SerializedName("MONFullday")
    @Expose
    public String MONFullday;

    @SerializedName("TUEMorning")
    @Expose
    public String TUEMorning;

    @SerializedName("TUEAfternoon")
    @Expose
    public String TUEAfternoon;

    @SerializedName("TUEEvening")
    @Expose
    public String TUEEvening;

    @SerializedName("TUEFullday")
    @Expose
    public String TUEFullday;

    @SerializedName("WEDMorning")
    @Expose
    public String WEDMorning;

    @SerializedName("WEDAfternoon")
    @Expose
    public String WEDAfternoon;

    @SerializedName("WEDEvening")
    @Expose
    public String WEDEvening;

    @SerializedName("WEDFullday")
    @Expose
    public String WEDFullday;

    @SerializedName("THUMorning")
    @Expose
    public String THUMorning;

    @SerializedName("THUAfternoon")
    @Expose
    public String THUAfternoon;

    @SerializedName("THUEvening")
    @Expose
    public String THUEvening;

    @SerializedName("THUFullday")
    @Expose
    public String THUFullday;

    @SerializedName("FRIMorning")
    @Expose
    public String FRIMorning;

    @SerializedName("FRIAfternoon")
    @Expose
    public String FRIAfternoon;

    @SerializedName("FRIEvening")
    @Expose
    public String FRIEvening;

    @SerializedName("FRIFullday")
    @Expose
    public String FRIFullday;

    @SerializedName("SATMorning")
    @Expose
    public String SATMorning;

    @SerializedName("SATAfternoon")
    @Expose
    public String SATAfternoon;

    @SerializedName("SATEvening")
    @Expose
    public String SATEvening;

    @SerializedName("SATFullday")
    @Expose
    public String SATFullday;

    @SerializedName("SUNMorning")
    @Expose
    public String SUNMorning;

    @SerializedName("SUNAfternoon")
    @Expose
    public String SUNAfternoon;

    @SerializedName("SUNEvening")
    @Expose
    public String SUNEvening;

    @SerializedName("SUNFullday")
    @Expose
    public String SUNFullday;

    @SerializedName("PromotionType")
    @Expose
    public String PromotionType;
}
