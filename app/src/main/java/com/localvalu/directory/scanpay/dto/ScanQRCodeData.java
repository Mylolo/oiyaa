package com.localvalu.directory.scanpay.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;

public class ScanQRCodeData implements Serializable {

    @SerializedName("TokenApplied")
    @Expose
    public String tokenApplied;

    @SerializedName("BonusTokens")
    @Expose
    public String bonusTokens;

    @SerializedName("BalanceToPay")
    @Expose
    public String balanceToPay;

    @SerializedName("Total")
    @Expose
    public String total;

    @SerializedName("Tokensearned")
    @Expose
    public String tokensearned;

    @SerializedName("NewTokenBalance")
    @Expose
    public String newTokenBalance;

    @SerializedName("RetailerIdOutput")
    @Expose
    public String retailerIdOutput;

    @SerializedName("BusinessIdOutput")
    @Expose
    public String businessIdOutput;
}
