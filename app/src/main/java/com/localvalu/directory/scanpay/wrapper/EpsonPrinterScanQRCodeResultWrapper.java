package com.localvalu.directory.scanpay.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.scanpay.dto.ScanQRCodeResult;

import java.io.Serializable;

public class EpsonPrinterScanQRCodeResultWrapper implements Serializable {

    @SerializedName("SaleTransactionScanQRCode_EpsonResult")
    @Expose
    public ScanQRCodeResult scanQRCodeResult;
}
