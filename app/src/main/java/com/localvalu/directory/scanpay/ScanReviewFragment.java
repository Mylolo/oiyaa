package com.localvalu.directory.scanpay;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.MainActivity;
import com.localvalu.base.HostActivityListener;
import com.localvalu.common.mytransaction.wrapper.UserDetailResultWrapper;
import com.localvalu.common.review.ReviewSuccessFragment;
import com.localvalu.common.review.wrapper.ReviewPostResultWrapper;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.squareup.picasso.Picasso;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ScanReviewFragment extends BaseFragment
{
    private static final String TAG = "ScanReviewFragment";

    //Toolbar
    private Toolbar toolbar;
    private TextView tvTitle;
    private int colorTheme;
    private int colorThemeBackground;

    private CircleImageView iv_img, iv_user_image;
    private TextView tv_name, tv_type, tv_address;
    private RatingBar rb_reviewRating1, rb_reviewRating2;
    private TextView tv_cnt;
    private EditText et_comment;
    private Button btn_next;
    private int navigationType;

    private String section_type, businessId, order_id, businessImage, businessName, businessType, businessAddress;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private HostActivityListener hostActivityListener;

    // TODO: Rename and change types and number of parameters
    public static ScanReviewFragment newInstance()
    {
        return new ScanReviewFragment();
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_scan_review_add, container, false);
        buildObjectForHandlingAPI();
        readFromBundle();
        initView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setProperties();
        setUpActionBar();
    }

    private void readFromBundle()
    {
        section_type = getArguments().getString("section_type");
        businessId = getArguments().getString("businessId");
        order_id = getArguments().getString("order_id");
        businessImage = getArguments().getString("businessImage");
        businessName = getArguments().getString("businessName");
        businessType = getArguments().getString("businessType");
        businessAddress = getArguments().getString("businessAddress");
        navigationType = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);

        System.out.println("retailer_id :: " + businessId);

        switch (navigationType)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
            case AppUtils.BMT_EATS:
                colorTheme = ContextCompat.getColor(requireContext(), R.color.colorEats);
                colorThemeBackground = ContextCompat.getColor(requireContext(), R.color.colorEatsLightBackground);
                break;
            case AppUtils.BMT_LOCAL:
                colorTheme = ContextCompat.getColor(requireContext(), R.color.colorLocal);
                colorThemeBackground = ContextCompat.getColor(requireContext(), R.color.colorLocalLightBackground);
                break;
            case AppUtils.BMT_MALL:
                colorTheme = ContextCompat.getColor(requireContext(), R.color.colorMall);
                colorThemeBackground = ContextCompat.getColor(requireContext(), R.color.colorMallLightBackground);
                break;

        }
    }

    private void initView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        iv_user_image = view.findViewById(R.id.iv_user_image);
        iv_img = view.findViewById(R.id.iv_img);
        tv_name = view.findViewById(R.id.tv_name);
        tv_type = view.findViewById(R.id.tv_type);
        tv_address = view.findViewById(R.id.tv_address);
        btn_next = view.findViewById(R.id.btn_next);
        tv_cnt = view.findViewById(R.id.tv_cnt);
        et_comment = view.findViewById(R.id.et_comment);
        rb_reviewRating1 = view.findViewById(R.id.rb_reviewRating1);
        rb_reviewRating2 = view.findViewById(R.id.rb_reviewRating2);

        if (userBasicInfo.image != null)
        {
            if (!userBasicInfo.image.trim().equals(""))
            {
                Picasso.with(getActivity()).load(userBasicInfo.image).placeholder(R.drawable.progress_animation)
                        .error(R.drawable.noimg_profile).into(iv_user_image);
            }
            else
            {
                iv_user_image.setImageResource(R.drawable.noimg_profile);
            }
        }
        else
        {
            iv_user_image.setImageResource(R.drawable.noimg_profile);
        }

        if (!businessImage.trim().equals(""))
        {
            Picasso.with(getActivity()).load(businessImage).placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo ).into(iv_img);
            iv_img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }

        else
        {
            iv_img.setImageResource(R.drawable.app_logo );
            iv_img.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }

        tv_name.setText(businessName);
        tv_type.setText(businessType);
        tv_address.setText(businessAddress);

        btn_next.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isValidForReviewPost())
                {
                    callAPIForReviewPost();
                }
            }
        });
        et_comment.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                // TODO Auto-generated method stub
                int length = et_comment.length();
                String convert = String.valueOf(length);
                tv_cnt.setText("[" + convert + "/140]");
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                // TODO Auto-generated method stub
            }
        });
    }

    private void setProperties()
    {
       view.setBackgroundColor(colorThemeBackground);
    }

    private void setUpActionBar()
    {
        toolbar = view.findViewById(R.id.toolbar);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setTextColor(colorTheme);
        toolbar.getNavigationIcon().setColorFilter(colorTheme, PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private boolean isValidForReviewPost()
    {
        if (rb_reviewRating1.getRating() == 0)
        {
            Toast.makeText(getActivity(), "Please give rating of product.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (rb_reviewRating2.getRating() == 0)
        {
            Toast.makeText(getActivity(), "Please give rating of service.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (et_comment.getText().toString().isEmpty())
        {
            Toast.makeText(getActivity(), "Please write your review.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (et_comment.getText().toString().length() > 140)
        {
            Toast.makeText(getActivity(), "Review must be up to 140 characters long.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!CommonUtilities.checkConnectivity(getActivity()))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.network_unavailable), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void callAPIForReviewPost()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("ReviewId", "");
            objMain.put("BusinessId", businessId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("order_id", order_id);
            objMain.put("reviewComments", et_comment.getText().toString());
            objMain.put("sectionType", section_type);
            objMain.put("productRate", rb_reviewRating1.getRating());
            objMain.put("serviceRate", rb_reviewRating2.getRating());
            objMain.put("requestFor", "PostReview");

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<ReviewPostResultWrapper> dtoCall = apiInterface.review(body);
        dtoCall.enqueue(new Callback<ReviewPostResultWrapper>()
        {
            @Override
            public void onResponse(Call<ReviewPostResultWrapper> call, Response<ReviewPostResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().reviewPostResult.errorDetails.errorCode == 0)
                    {
                        callAPI();
                        Toast.makeText(getActivity(), "Your review posted successfully.", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().reviewPostResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<ReviewPostResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk("No Transaction for this retailer", getActivity());
//                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void callAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "CustomerBalanceEnquiry");
            objMain.put("AccountId", userBasicInfo.accountId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString("currentToken", "" + response.body().userDetailResult.customerBalanceEnquiryData.currentTokenBalance);
                        ActivityController.startNextActivity(getActivity(), ReviewSuccessFragment.class, bundle, false);
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                Bundle bundle = new Bundle();
                bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,navigationType);
                ActivityController.startNextActivity(getActivity(), MainActivity.class, bundle, true);
                break;
        }
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }

}
