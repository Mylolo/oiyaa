package com.localvalu.directory.scanpay.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessDetailInfo implements Serializable
{

    @SerializedName("business_id")
    @Expose
    public String business_id;

    @SerializedName("firstname")
    @Expose
    public String firstname;

    @SerializedName("lastname")
    @Expose
    public String lastname;

    @SerializedName("retailerId")
    @Expose
    public String retailerId;

    @SerializedName("tradingname")
    @Expose
    public String tradingname;

    @SerializedName("BusinessType")
    @Expose
    public String businessType;

    @SerializedName("RetailerTypeID")
    @Expose
    public String retailerTypeId;

    @SerializedName("logo")
    @Expose
    public String logo;

    @SerializedName("Website")
    @Expose
    public String website;

    @SerializedName("contact_person_name")
    @Expose
    public String contact_person_name;

    @SerializedName("contact_person_Email")
    @Expose
    public String contact_person_Email;

    @SerializedName("contact_person_phone")
    @Expose
    public String contact_person_phone;

    @SerializedName("address1")
    @Expose
    public String address1;

    @SerializedName("address2")
    @Expose
    public String address2;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("country")
    @Expose
    public String country;

    @SerializedName("postcode")
    @Expose
    public String postcode;

    @SerializedName("about")
    @Expose
    public String about;

    @SerializedName("timings")
    @Expose
    public String timings;

    @SerializedName("SplitTimings")
    @Expose
    public String splitTimings;

    @SerializedName("categories")
    @Expose
    public String categories;

    @SerializedName("typeoffoods")
    @Expose
    public String typeoffoods;

    @SerializedName("minOrderVall")
    @Expose
    public String minOrderVall;

    @SerializedName("lat")
    @Expose
    public String lat;

    @SerializedName("lng")
    @Expose
    public String lng;

    @SerializedName("CurrentPromotionValue")
    @Expose
    public String currentPromotionValue;

    @SerializedName("OverAllreviewCount")
    @Expose
    public String overAllreviewCount;

    @SerializedName("OverAllreviewRate")
    @Expose
    public String overAllreviewRate;

    @SerializedName("OverAllProductRate")
    @Expose
    public String overAllProductRate;

    @SerializedName("OverAllServiceRate")
    @Expose
    public String overAllServiceRate;

    @SerializedName("HygieneImageUrl")
    @Expose
    public String hygieneImageUrl;

    @SerializedName("BusinessCoverImage")
    @Expose
    public String businessCoverImage;

    @SerializedName("BookingTypeId")
    @Expose
    public String bookingTypeId;

    @SerializedName("optomanymidConfirm")
    @Expose
    public String optomanymidConfirm;

    @SerializedName("DriverType")
    @Expose
    public String DriverType;

    @SerializedName("pay360Confirm")
    @Expose
    public String pay360Confirm;

    @SerializedName("pay360midID")
    @Expose
    public String pay360midID;

    public String sectionType;

    public String orderId;

}
