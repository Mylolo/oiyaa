package com.localvalu.directory.scanpay.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BusinessImages implements Serializable {

    @SerializedName("imageId")
    @Expose
    public String imageId;

    @SerializedName("businessId")
    @Expose
    public String businessId;

    @SerializedName("imagePath")
    @Expose
    public String imagePath;

    @SerializedName("createdDate")
    @Expose
    public String createdDate;

    @SerializedName("isActive")
    @Expose
    public String isActive;

    @SerializedName("updatedDate")
    @Expose
    public String updatedDate;


}
