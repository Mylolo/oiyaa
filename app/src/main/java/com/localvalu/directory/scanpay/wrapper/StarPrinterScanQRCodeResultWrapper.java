package com.localvalu.directory.scanpay.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.scanpay.dto.ScanQRCodeResult;

import java.io.Serializable;

public class StarPrinterScanQRCodeResultWrapper implements Serializable {

    @SerializedName("SaleTransactionScanQRCodeAPIResult")
    @Expose
    public ScanQRCodeResult scanQRCodeResult;
}
