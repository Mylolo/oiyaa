package com.localvalu.directory.scanpay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.journeyapps.barcodescanner.DefaultDecoderFactory;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.utility.AppUtils;

import java.util.Arrays;
import java.util.List;

public class ScanPayFragment extends BaseFragment
{
    private static final String TAG = "DirectoryReviewDetailFr";

    //Toolbar
    private Toolbar toolbar;
    private TextView tvTitle;
    private int colorTheme;

    private DecoratedBarcodeView decoratedBarcodeView;
    private String lastText;
    private BeepManager beepManager;
    private int navigationType;
    private HostActivityListener hostActivityListener;

    // TODO: Rename and change types and number of parameters
    public static ScanPayFragment newInstance()
    {
        return new ScanPayFragment();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_directory_scanpay, container, false);
        beepManager = new BeepManager(getActivity());
        readFromBundle();
        initView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
    }

    private void initView()
    {
        decoratedBarcodeView = view.findViewById(R.id.preview);
        decoratedBarcodeView.setStatusText("Scan Your QR Code");
        decoratedBarcodeView.getBarcodeView().setDecoderFactory(new DefaultDecoderFactory(Arrays.asList(BarcodeFormat.QR_CODE)));
        decoratedBarcodeView.initializeFromIntent(getActivity().getIntent());
        decoratedBarcodeView.decodeContinuous(new BarcodeCallback()
        {
            @Override
            public void barcodeResult(BarcodeResult result)
            {
                String text = result.getText();

                if (text == null || text.equals(lastText))
                    return;

                lastText = text;
                beepManager.playBeepSoundAndVibrate();

                if (lastText.length() > 0)
                {
                    System.out.println("QR_CODE : " + lastText);

                    Bundle bundle = new Bundle();
                    ScanSuccessFragment scanSuccessFragment = ScanSuccessFragment.newInstance();
                    bundle.putString("qrCode", lastText);
                    bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,navigationType);
                    scanSuccessFragment.setArguments(bundle);
                    hostActivityListener.updateFragment(scanSuccessFragment, true, ScanSuccessFragment.class.getName(),true);

                }
            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints)
            {

            }
        });
    }

    private void readFromBundle()
    {
        navigationType = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);

        switch (navigationType)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
            case AppUtils.BMT_EATS:
            case AppUtils.BMT_LOCAL:
                colorTheme = ContextCompat.getColor(requireContext(), R.color.colorEats);
                 break;
            case AppUtils.BMT_MALL:
                 colorTheme = ContextCompat.getColor(requireContext(), R.color.colorMall);
                 break;

        }
    }

    private void setUpActionBar()
    {
        toolbar = view.findViewById(R.id.toolbar);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setTextColor(colorTheme);
        toolbar.getNavigationIcon().setColorFilter(colorTheme, PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private void setProperties()
    {
        switch (navigationType)
        {
            case AppUtils.BMT_LOCAL:
                view.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorLocalLightBackground));
                break;
            case AppUtils.BMT_MALL:
                view.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorMallLightBackground));
                break;
            case AppUtils.BMT_EATS:
            default:
                view.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorEatsLightBackground));
                break;
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

        decoratedBarcodeView.resume();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        decoratedBarcodeView.pause();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
            case R.id.ll_scan:
//                Bundle bundle = new Bundle();
//                ScanSuccessFragment scanSuccessFragment = ScanSuccessFragment.newInstance();
//                bundle.putString("section_type", "info");
//                ((MainActivity) getActivity()).addFragment(scanSuccessFragment, true, ScanSuccessFragment.class.getName(), true);
                break;
        }
    }

}
