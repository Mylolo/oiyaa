package com.localvalu.directory.scanpay;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentScanpayErrorBinding;
import com.utility.AppUtils;

public class ScanErrorFragment extends BaseFragment
{

    private static final String TAG = "ScanErrorFragment";
    private FragmentScanpayErrorBinding binding;
    private int colorTheme;
    private int colorThemeBackground;

    private String strErrorMessage;
    private int navigationType;
    private HostActivityListener hostActivityListener;

    // TODO: Rename and change types and number of parameters
    public static ScanErrorFragment newInstance()
    {
        return new ScanErrorFragment();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentScanpayErrorBinding.inflate(inflater,container,false);
        readFromBundle();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
    }

    private void setUpActionBar()
    {
        binding.toolbarLayout.tvTitle.setTextColor(colorTheme);
        binding.toolbarLayout.toolbar.getNavigationIcon().setColorFilter(colorTheme, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });
    }

    private void setProperties()
    {
        binding.layoutError.btnRetry.setText(getString(R.string.lbl_ok));
        binding.layoutError.btnRetry.setOnClickListener(_OnClickListener);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strErrorMessage);
    }

    private void readFromBundle()
    {
        try
        {
            strErrorMessage = "" + getArguments().getString(AppUtils.BUNDLE_SCAN_ERROR_MESSAGE);
            navigationType = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
            switch (navigationType)
            {
                case AppUtils.BMT_TRACK_AND_TRACE:
                case AppUtils.BMT_EATS:
                    colorTheme = ContextCompat.getColor(requireContext(), R.color.colorEats);
                    colorThemeBackground = ContextCompat.getColor(requireContext(), R.color.colorEatsLightBackground);
                    break;
                case AppUtils.BMT_LOCAL:
                    colorTheme = ContextCompat.getColor(requireContext(), R.color.colorLocal);
                    colorThemeBackground = ContextCompat.getColor(requireContext(), R.color.colorLocalLightBackground);
                    break;
                case AppUtils.BMT_MALL:
                    colorTheme = ContextCompat.getColor(requireContext(), R.color.colorMall);
                    colorThemeBackground = ContextCompat.getColor(requireContext(), R.color.colorMallLightBackground);
                    break;

            }
        }
        catch (Exception e)
        {

        }
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.btnRetry:
                    hostActivityListener.onBackButtonPressed();
                    break;
            }
        }
    };

}
