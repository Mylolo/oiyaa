package com.localvalu.directory.scanpay.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.review.dto.ReviewData;

import java.io.Serializable;
import java.util.List;

public class BusinessDetail implements Serializable {

    @SerializedName("BusinessInfo")
    @Expose
    public List<BusinessDetailInfo> businessDetails;

    @SerializedName("BusinessImages")
    @Expose
    public List<BusinessImages> businessImages;

    @SerializedName("RetailerPromotionList")
    @Expose
    public List<RetailerPromotion> retailerPromotions;

    @SerializedName("ProductReviews")
    @Expose
    public List<ReviewData> productReviews;

    @SerializedName("ServiceReviews")
    @Expose
    public List<ReviewData> serviceReviews;
}
