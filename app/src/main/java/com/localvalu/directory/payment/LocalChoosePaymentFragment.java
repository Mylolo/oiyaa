package com.localvalu.directory.payment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.localvalu.BuildConfig;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentChoosePaymentBinding;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.eats.mycart.dto.CheckoutDto;
import com.localvalu.eats.payment.dto.OnlinePaymentWrapper;
import com.localvalu.eats.payment.dto.Pay360Request;
import com.localvalu.eats.payment.dto.Pay360Response;
import com.localvalu.eats.payment.dto.PaymentAPIResponse;
import com.localvalu.eats.payment.dto.PaymentIntegrationApi;
import com.localvalu.eats.payment.dto.PlaceOrderResultResponse;
import com.localvalu.eats.payment.online.OnlinePaymentActivityEat;
import com.localvalu.eats.repository.PaymentRepository;
import com.localvalu.user.dto.UserBasicInfo;
import com.localvalu.user.manageaddress.request.AddressListRequest;
import com.localvalu.user.manageaddress.response.DeliveryAddressDetails;
import com.localvalu.user.manageaddress.response.OrderNow;
import com.localvalu.user.manageaddress.response.OrderNowResponse;
import com.localvalu.user.manageaddress.response.OrderNowResults;
import com.localvalu.user.manageaddress.ui.AddNewAddress;
import com.localvalu.user.manageaddress.ui.AddressSelection;
import com.localvalu.user.manageaddress.ui.AddressUpdatedListener;
import com.localvalu.user.repository.UserAddressRepository;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocalChoosePaymentFragment extends BaseFragment implements AddressUpdatedListener
{
    String TAG = LocalChoosePaymentFragment.class.getName();

    private static final int REQUEST_CODE_PAYMENT = 786;
    public View view;
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private ArrayList<CartDetailDto> cartDetails;
    private String orderTotal, businessRetailerID, businessId, businessID, businessImage, businessName, businessType, businessAddress, totalLolaltyPointRedeemed,
            deliveryRate, tax, delivery, special_ins, allergy_ins;
    private FragmentChoosePaymentBinding binding;
    private String payment_type = "2";
    private UserAddressRepository userAddressRepository;
    private boolean addressAdded = false;
    private boolean firstTime = false;
    private int colorLocal;
    private CheckoutDto checkoutDto;
    private HostActivityListener hostActivityListener;

    private PaymentRepository paymentRepository;
    private String strEatsSuccessUrl;
    private String strEatsFailureUrl;
    private String strEatsBackUrl;


    public static LocalChoosePaymentFragment newInstance(CheckoutDto checkoutDto)
    {
        LocalChoosePaymentFragment choosePaymentFragment = new LocalChoosePaymentFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppUtils.BUNDLE_CHECK_OUT,checkoutDto);
        choosePaymentFragment.setArguments(bundle);
        return choosePaymentFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        userAddressRepository = new UserAddressRepository();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        colorLocal = ContextCompat.getColor(requireContext(), R.color.colorLocal);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        readFromBundle();
        buildObjectForHandlingAPI();
        paymentRepository = new PaymentRepository();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentChoosePaymentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
        binding.btnPlaceOrder.setOnClickListener(_OnClickListener);
        binding.btnAddNewAddress.setOnClickListener(_OnClickListener);
    }

    private void setUpActionBar()
    {
        binding.toolbarLayout.toolbar.getNavigationIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private void readFromBundle()
    {
        checkoutDto = (CheckoutDto) getArguments().getSerializable(AppUtils.BUNDLE_CHECK_OUT);
        cartDetails = checkoutDto.cartList;
        orderTotal = checkoutDto.orderTotal;
        businessId = checkoutDto.businessDetailInfo.business_id;
        businessRetailerID = checkoutDto.businessDetailInfo.retailerId;
        //For review
        businessID = checkoutDto.businessDetailInfo.retailerId;
        businessImage =checkoutDto.businessDetailInfo.businessCoverImage;
        businessName =checkoutDto.businessDetailInfo.tradingname;
        businessType = checkoutDto.businessDetailInfo.businessType;
        businessAddress = getAddress(checkoutDto.businessDetailInfo);

        totalLolaltyPointRedeemed =checkoutDto.totalLoyaltyPointsRedeemed;
        deliveryRate = checkoutDto.deliveryRate;
        tax = checkoutDto.tax;
        delivery = checkoutDto.delivery;
        special_ins = checkoutDto.specialInstructions;
        allergy_ins = checkoutDto.allergyInstructions;

        System.out.println("cartDetails : " + cartDetails.toString());
        System.out.println("businessId : " + businessId);
    }

    private void setProperties()
    {
        switch (delivery.trim())
        {
            case AppUtils.OPTION_DELIVERY:
                getAddressListFromServer();
                showDeliveryUI();
                break;
            case AppUtils.OPTION_COLLECTION:
                showCollectionUI();
                break;
        }

        if(checkoutDto.businessDetailInfo!=null)
        {
            BusinessDetailInfo businessDetailInfo = checkoutDto.businessDetailInfo;
            if(businessDetailInfo.pay360midID.equals(""))
            {
                payment_type="2";
                binding.radioButtonOnlinePayment.setVisibility(View.GONE);
            }
            else
            {
                payment_type="1";
                binding.radioButtonOnlinePayment.setVisibility(View.VISIBLE);
                binding.radioButtonOnlinePayment.setChecked(true);
            }
            switch (businessDetailInfo.DriverType)
            {
                case AppUtils.DRIVER_TYPE_NO_DRIVER:
                    break;
                case AppUtils.DRIVER_TYPE_OWN_DRIVER:
                    break;
                case AppUtils.DRIVER_TYPE_POOL_DRIVER:
                    break;
            }
        }

        binding.radioGroupPayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checked)
            {
                switch (checked)
                {
                    case R.id.radioButtonOnlinePayment:
                        payment_type = "1";
                        break;
                    case R.id.radioButtonCashOnDelivery:
                        payment_type = "2";
                        break;
                    case R.id.radioButtonPayOnCollection:
                        payment_type = "3";
                        break;
                }
            }
        });
        binding.errorLayout.btnRetry.setOnClickListener(_OnClickListener);
    }

    private void showDeliveryUI()
    {
        binding.radioButtonPayOnCollection.setVisibility(View.GONE);
        binding.radioButtonCashOnDelivery.setChecked(true);
        binding.cnlName.setVisibility(View.VISIBLE);
        binding.cnlAddress.setVisibility(View.VISIBLE);
        binding.cnlPostCode.setVisibility(View.VISIBLE);
        binding.cnlMobile.setVisibility(View.VISIBLE);
        binding.cnlDeliveryDetails.setVisibility(View.VISIBLE);
        binding.btnAddNewAddress.setVisibility(View.VISIBLE);
        binding.etName.setText(userBasicInfo.name);
    }

    private void showCollectionUI()
    {
        binding.radioButtonCashOnDelivery.setVisibility(View.GONE);
        binding.radioButtonPayOnCollection.setChecked(true);
        binding.cnlName.setVisibility(View.VISIBLE);
        binding.cnlAddress.setVisibility(View.GONE);
        binding.cnlPostCode.setVisibility(View.GONE);
        binding.cnlMobile.setVisibility(View.GONE);
        binding.tvDelivery.setText(getString(R.string.lbl_collection));
        binding.btnAddNewAddress.setVisibility(View.GONE);
        binding.etName.setText(userBasicInfo.name);
    }

    public void setAddress(DeliveryAddressDetails deliveryAddressDetails)
    {
        binding.etName.setText(deliveryAddressDetails.getName());
        binding.etAddress.setText(deliveryAddressDetails.getAddressOne());
        binding.etPostCode.setText(deliveryAddressDetails.getPostalcode());
        binding.etMobileNumber.setText(deliveryAddressDetails.getPhoneNo());

        binding.cnlAddress.setVisibility(View.VISIBLE);
        binding.cnlPostCode.setVisibility(View.VISIBLE);
        binding.cnlMobile.setVisibility(View.VISIBLE);
        binding.cnlName.setVisibility(View.VISIBLE);
        binding.btnAddNewAddress.setVisibility(View.VISIBLE);
        binding.cnlDeliveryDetails.setVisibility(View.VISIBLE);

        binding.btnAddNewAddress.setText(getString(R.string.lbl_change_or_add_new_address));
        addressAdded = true;
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                 getActivity().onBackPressed();
                 break;
            case R.id.btnAddNewAddress:
                 if (firstTime) gotoAddNewAddress();
                 else gotoAddressSelection();
                 break;
            case R.id.btnPlaceOrder:
                 if (validate())
                 {
                     if (payment_type.equalsIgnoreCase("1"))
                     {
                         /*Bundle bundle = new Bundle();
                         bundle.putString("businessId", businessId);
                         bundle.putString("special_ins", special_ins);
                         bundle.putString("allergy_ins", allergy_ins);
                         bundle.putString("delivery", delivery);
                         Intent intent = new Intent(getActivity(), OnlinePaymentActivityLocal.class);
                         intent.putExtra("data", bundle);
                         getActivity().startActivity(intent);*/
                         //callOnlinePayment();
                         createPayment();
                     }
                     else if(payment_type.equalsIgnoreCase("2"))
                     {
                         callCOD("CashOnDelivery");
                     }
                     else if(payment_type.equalsIgnoreCase("3"))
                     {
                         callCOD("PayOnCollection");
                     }
                 }
                 break;
            case R.id.btnRetry:
                 getAddressListFromServer();
                 break;
        }
    }

    private void createPayment()
    {
        Pay360Request pay360Request = getPay360Request();
        if(pay360Request!=null)
        {
            paymentRepository.startPayment(pay360Request).subscribe(new SingleObserver<Pay360Response>()
            {
                @Override
                public void onSubscribe(@NonNull Disposable d)
                {

                }

                @Override
                public void onSuccess(@NonNull Pay360Response pay360Response)
                {
                    if(pay360Response!=null)
                    {
                        if(pay360Response.getPAY360CreatePayment()!=null)
                        {
                            if(pay360Response.getPAY360CreatePayment().getErrorDetails().getErrorCode()==0)
                            {
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(AppUtils.BUNDLE_PAY_360_RESULT,
                                        pay360Response.getPAY360CreatePayment().getPAY360Result());
                                Intent intent = new Intent(getActivity(), OnlinePaymentActivityEat.class);
                                intent.putExtra("data", bundle);
                                getActivity().startActivity(intent);

                            }
                            else
                            {
                                DialogUtility.showMessageWithOk(pay360Response.getPAY360CreatePayment().getErrorDetails().getErrorMessage(), getActivity());
                            }
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                        }
                    }
                }

                @Override
                public void onError(@NonNull Throwable e)
                {
                    if (!CommonUtilities.checkConnectivity(getContext()))
                        DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                    else
                        DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                }
            });
        }
    }

    private Pay360Request getPay360Request()
    {
        Pay360Request pay360Request = new Pay360Request();
        pay360Request.setCALine1(binding.etAddress.getText().toString());
        pay360Request.setCALine2("");
        pay360Request.setCEmailAddress(userBasicInfo.email);
        pay360Request.setReturnUrl("https://www.oiyaa.com/");
        pay360Request.setCancelUrl("https://www.oiyaa.com/");
        pay360Request.setDeclineUrl("https://www.oiyaa.com/");
        pay360Request.setErrorUrl("https://www.oiyaa.com/");
        pay360Request.setCAPostcode(binding.etPostCode.getText().toString());
        pay360Request.setCCustomerId(userBasicInfo.userId);
        if (BuildConfig.DEBUG)
        {
            pay360Request.setTDescription("Test");
        }
        else
        {
            pay360Request.setTDescription("Purchase");
        }
        pay360Request.setOrderAmount(checkoutDto.orderTotal);
        pay360Request.setDeliveryFee(checkoutDto.deliveryRate);
        pay360Request.setTotalAmount(checkoutDto.orderTotal);
        if(checkoutDto.businessDetailInfo != null)
        {
            BusinessDetailInfo businessDetailInfo = checkoutDto.businessDetailInfo;
            switch (businessDetailInfo.DriverType)
            {
                case AppUtils.DRIVER_TYPE_NO_DRIVER:
                    pay360Request.setOrderMerchantId(businessDetailInfo.pay360midID);
                    pay360Request.setDeliveryMerchantId(businessDetailInfo.retailerId);
                    break;
                default:
                    pay360Request.setOrderMerchantId(businessDetailInfo.pay360midID);
                    pay360Request.setDeliveryMerchantId(businessDetailInfo.pay360midID);
                    break;
            }
        }
        return pay360Request;
    }

    private boolean validate()
    {
        Log.d(TAG, "validate: delivery-" + delivery);
        if (payment_type.equalsIgnoreCase(""))
        {
            Toast.makeText(getContext(), getString(R.string.lbl_select_payment_method), Toast.LENGTH_LONG).show();
            return false;
        }
        else if (delivery.equals("Delivery") && !addressAdded)
        {
            if (firstTime)
                Toast.makeText(getContext(), getString(R.string.lbl_add_delivery_address), Toast.LENGTH_LONG).show();
            return false;
        }
        else
        {
            return true;
        }
    }

    private String getAddress(BusinessDetailInfo businessDetailInfo)
    {
        StringBuilder strAddress = new StringBuilder();
        if (businessDetailInfo.address1.isEmpty())
        {
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        else
        {
            strAddress.append(businessDetailInfo.address1.trim());
            strAddress.append(" ");
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.city.isEmpty())
        {
            strAddress.append(businessDetailInfo.city.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.country.isEmpty())
        {
            strAddress.append(businessDetailInfo.country.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.postcode.isEmpty())
        {
            strAddress.append(businessDetailInfo.postcode.trim());
        }
        return strAddress.toString();
    }

    public void gotoAddressSelection()
    {
        AddressSelection addressSelection = AddressSelection.newInstance(getString(R.string.lbl_select_address)
                , AppUtils.BMT_LOCAL, userBasicInfo.accountId, userBasicInfo.userId, businessId);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        addressSelection.setTargetFragment(this, 45);
        addressSelection.show(fragmentManager, addressSelection.getClass().getName());
    }

    public void gotoAddNewAddress()
    {
        AddNewAddress addNewAddress = AddNewAddress.newInstance(getString(R.string.lbl_select_address),AppUtils.BMT_LOCAL, userBasicInfo.accountId, userBasicInfo.userId);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        addNewAddress.setTargetFragment(this, 55);
        addNewAddress.show(fragmentManager, addNewAddress.getClass().getName());
    }

    private void callCOD(String paymentType)
    {
        mProgressDialog.show();

        PaymentIntegrationApi paymentIntegrationApi = new PaymentIntegrationApi();
        paymentIntegrationApi.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        paymentIntegrationApi.setAccountId(userBasicInfo.accountId);
        paymentIntegrationApi.setBusinessId(businessId);
        paymentIntegrationApi.setUserId(userBasicInfo.userId);
        paymentIntegrationApi.setPaymentType(paymentType);
        paymentIntegrationApi.setSpecialIns(special_ins);
        paymentIntegrationApi.setAllergyIns(allergy_ins);
        paymentIntegrationApi.setDistance("1");
        paymentIntegrationApi.setOrderType(delivery);
        paymentIntegrationApi.setPlatforms("android");
        paymentIntegrationApi.setIpAddress(getIpAddress(getContext()));

        Call<OnlinePaymentWrapper> getPaymentIntegration = apiInterface.orderOnlineLocalCOD(paymentIntegrationApi);
        getPaymentIntegration.enqueue(new Callback<OnlinePaymentWrapper>()
        {
            @Override
            public void onResponse(Call<OnlinePaymentWrapper> call, Response<OnlinePaymentWrapper> response)
            {
                try
                {
                    mProgressDialog.dismiss();

                    try
                    {
                        OnlinePaymentWrapper onlinePaymentWrapper = response.body();

                        PaymentAPIResponse paymentAPIResponse = onlinePaymentWrapper.getPaymentAPIResponse();
                        PlaceOrderResultResponse placeOrderResultResponse = paymentAPIResponse.getPlaceOrderResultResponse();

                        if (paymentAPIResponse.errorDetails.getErrorCode() == 0)
                        {
                            Bundle bundle = new Bundle();
                            bundle.putString(AppUtils.BUNDLE_OIYAA_BONUS, "" + placeOrderResultResponse.getLvBonus());
                            bundle.putString(AppUtils.BUNDLE_BONUS_TOKENS, "" + placeOrderResultResponse.getBonusTokens());
                            bundle.putString(AppUtils.BUNDLE_NEW_TOKEN_BALANCE, "" + placeOrderResultResponse.getNewTokenBalance());
                            //For review
                            bundle.putString("businessId", businessID);
                            bundle.putString("businessImage", businessImage);
                            bundle.putString("businessName", businessName);
                            bundle.putString("businessType", businessType);
                            bundle.putString("businessAddress", businessAddress);
                            ActivityController.startNextActivity(getActivity(), LocalSuccessPopupForOrderSubmit.class, bundle, false);
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(paymentAPIResponse.errorDetails.getErrorMessage(), getActivity());
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
                catch (Exception e)
                {
                    mProgressDialog.dismiss();
                    Log.e(TAG, " : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<OnlinePaymentWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.e(TAG, " : " + t.getMessage());
            }
        });
    }

    public static String getIpAddress(Context context)
    {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext()
                .getSystemService(context.WIFI_SERVICE);

        String ipAddress = intToInetAddress(wifiManager.getDhcpInfo().ipAddress).toString();

        ipAddress = ipAddress.substring(1);

        return ipAddress;
    }

    public static InetAddress intToInetAddress(int hostAddress)
    {
        byte[] addressBytes = {(byte) (0xff & hostAddress),
                (byte) (0xff & (hostAddress >> 8)),
                (byte) (0xff & (hostAddress >> 16)),
                (byte) (0xff & (hostAddress >> 24))};

        try
        {
            return InetAddress.getByAddress(addressBytes);
        }
        catch (UnknownHostException e)
        {
            throw new AssertionError();
        }
    }

    public AddressListRequest getRequestPayload()
    {
        AddressListRequest addressListRequest = new AddressListRequest();
        addressListRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        addressListRequest.setAccountId(userBasicInfo.accountId);
        addressListRequest.setUserId(userBasicInfo.userId);
        addressListRequest.setRequestFor("MyBasketDetails");
        addressListRequest.setBusinessId(businessId);
        addressListRequest.setDistance("12");
        return addressListRequest;
    }

    public void getAddressListFromServer()
    {
        //mProgressDialog.show();

        showProgress();

        userAddressRepository.getAddressList(getRequestPayload()).subscribe(new Observer<OrderNowResponse>()
        {
            @Override
            public void onSubscribe(Disposable d)
            {

            }

            @Override
            public void onNext(OrderNowResponse orderNowResponse)
            {
                if (orderNowResponse.getOrderNowResults() != null)
                {
                    OrderNowResults results = orderNowResponse.getOrderNowResults();
                    verifyDeliveryAddress(results);
                }
            }

            @Override
            public void onError(Throwable e)
            {
                //if (mProgressDialog != null) mProgressDialog.dismiss();
                if(e instanceof IOException) showError(getString(R.string.network_unavailable),true);
                else showError(getString(R.string.server_alert),false);
            }

            @Override
            public void onComplete()
            {

            }
        });
    }

    public void verifyDeliveryAddress(OrderNowResults results)
    {
        if (results != null)
        {
            if (results.getErrorDetails().getErrorCode() == 0)
            {
                OrderNow orderNow = results.getOrderNow();

                if (orderNow.getDeliveryAddressList() != null)
                {
                    List<DeliveryAddressDetails> list = orderNow.getDeliveryAddressList();
                    Log.d(TAG, "onNext: " + list.size());

                    showContents();

                    if (list.size() > 0)
                    {
                        boolean defaultAddressFound = false;

                        for (DeliveryAddressDetails deliveryAddress : list)
                        {
                            if (deliveryAddress.getSelectedAddr().equals("1"))
                            {
                                onAddressAdded(deliveryAddress);
                                addressAdded = true;
                                defaultAddressFound = true;
                            }
                        }
                        if (!defaultAddressFound)
                        {
                            //Set the first address as default address
                            onAddressAdded(list.get(0));
                            addressAdded = true;
                        }
                    }
                    else
                    {
                        //Hide all the labels, show the button as "Add New Address"
                        showNewAddressContent();
                    }
                }
            }

            else showError(results.getErrorDetails().getErrorMessage(),false);
        }
    }

    public void showNewAddressContent()
    {
        binding.cnlDeliveryDetails.setVisibility(View.GONE);
        binding.btnAddNewAddress.setText(getString(R.string.lbl_add_new_address));
        addressAdded = false;
        firstTime = true;
    }

    @Override
    public void onAddressSelected(DeliveryAddressDetails deliveryAddressDetails)
    {
        setAddress(deliveryAddressDetails);
    }

    @Override
    public void onAddressAdded(DeliveryAddressDetails deliveryAddressDetails)
    {
        firstTime = false;
        setAddress(deliveryAddressDetails);
    }

    @Override
    public void onNoAddressFound()
    {
        showNewAddressContent();
    }

    private void showProgress()
    {
        binding.progressLayout.layoutRoot.setVisibility(View.VISIBLE);
        binding.nestedScrollView.setVisibility(View.GONE);
        binding.errorLayout.layoutRoot.setVisibility(View.GONE);
    }

    private void showContents()
    {
        binding.progressLayout.layoutRoot.setVisibility(View.GONE);
        binding.nestedScrollView.setVisibility(View.VISIBLE);
        binding.errorLayout.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage,boolean retry)
    {
        binding.progressLayout.layoutRoot.setVisibility(View.GONE);
        binding.nestedScrollView.setVisibility(View.GONE);
        binding.errorLayout.layoutRoot.setVisibility(View.VISIBLE);
        binding.errorLayout.tvError.setText(strMessage);
        if(retry) binding.errorLayout.btnRetry.setVisibility(View.VISIBLE);
        else binding.errorLayout.btnRetry.setVisibility(View.GONE);
    }

}
