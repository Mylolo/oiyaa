package com.localvalu.directory.payment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentChoosePaymentTableBinding;
import com.localvalu.directory.payment.online.OnlinePaymentActivityLocalStore;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.eats.mycart.dto.CheckoutDto;
import com.localvalu.eats.payment.dto.OnlinePaymentWrapper;
import com.localvalu.eats.payment.dto.PaymentAPIResponse;
import com.localvalu.eats.payment.dto.PaymentIntegrationApi;
import com.localvalu.eats.payment.dto.PlaceOrderResultResponse;
import com.localvalu.user.dto.UserBasicInfo;
import com.localvalu.user.repository.UserAddressRepository;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocalChoosePaymentFragmentStore extends BaseFragment
{
    String TAG = LocalChoosePaymentFragmentStore.class.getName();

    private static final int REQUEST_CODE_PAYMENT = 786;
    public View view;
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private ArrayList<CartDetailDto> cartDetails;
    private String orderTotal, businessRetailerID, businessId, businessID, businessImage, businessName, businessType, businessAddress, totalLolaltyPointRedeemed,
            deliveryRate, tax, delivery, tableId,description, special_ins, allergy_ins;
    private FragmentChoosePaymentTableBinding binding;
    private String payment_type = "2";
    private UserAddressRepository userAddressRepository;
    private boolean addressAdded = false;
    private boolean firstTime = false;
    private int colorLocal;
    private CheckoutDto checkoutDto;
    private HostActivityListener hostActivityListener;

    public static LocalChoosePaymentFragmentStore newInstance(CheckoutDto checkoutDto)
    {
        LocalChoosePaymentFragmentStore choosePaymentFragment = new LocalChoosePaymentFragmentStore();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppUtils.BUNDLE_CHECK_OUT,checkoutDto);
        choosePaymentFragment.setArguments(bundle);
        return choosePaymentFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        userAddressRepository = new UserAddressRepository();

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        colorLocal = ContextCompat.getColor(requireContext(), R.color.colorLocal);
        readFromBundle();
        buildObjectForHandlingAPI();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentChoosePaymentTableBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
    }

    private void setUpActionBar()
    {
        binding.toolbarLayout.toolbar.getNavigationIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private void readFromBundle()
    {
        checkoutDto = (CheckoutDto) getArguments().getSerializable(AppUtils.BUNDLE_CHECK_OUT);
        cartDetails = checkoutDto.cartList;
        orderTotal = checkoutDto.orderTotal;
        businessId = checkoutDto.businessDetailInfo.business_id;
        businessRetailerID = checkoutDto.businessDetailInfo.retailerId;
        //For review
        businessID = checkoutDto.businessDetailInfo.retailerId;
        businessImage =checkoutDto.businessDetailInfo.businessCoverImage;
        businessName =checkoutDto.businessDetailInfo.tradingname;
        businessType = checkoutDto.businessDetailInfo.businessType;
        businessAddress = getAddress(checkoutDto.businessDetailInfo);

        totalLolaltyPointRedeemed =checkoutDto.totalLoyaltyPointsRedeemed;
        deliveryRate = checkoutDto.deliveryRate;
        tax = checkoutDto.tax;
        delivery = checkoutDto.delivery;
        tableId = checkoutDto.tableId;
        description = checkoutDto.tableOrderDescription;
        special_ins = checkoutDto.specialInstructions;
        allergy_ins = checkoutDto.allergyInstructions;

        System.out.println("cartDetails : " + cartDetails.toString());
        System.out.println("businessId : " + businessId);
    }

    private void setProperties()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        binding.btnPlaceOrder.setOnClickListener(_OnClickListener);
        binding.radioButtonOnlinePayment.setVisibility(View.GONE);
        binding.radioButtonPayAtRestaurant.setChecked(true);
        if(checkoutDto!=null)
        {
            if(checkoutDto.businessDetailInfo!=null)
            {
                if(checkoutDto.businessDetailInfo.retailerTypeId!=null)
                {
                    String retailerTypeId = checkoutDto.businessDetailInfo.retailerTypeId;

                    switch (Integer.parseInt(retailerTypeId))
                    {
                        case AppUtils.BMT_EATS:
                             binding.radioButtonPayAtRestaurant.setText(getString(R.string.lbl_pay_at_table));
                             break;
                        default:
                             binding.radioButtonPayAtRestaurant.setText(getString(R.string.lbl_pay_at_store));
                             break;
                    }
                }
            }
        }

        binding.radioGroupPayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checked)
            {
                switch (checked)
                {
                    case R.id.radioButtonOnlinePayment:
                        payment_type = "1";
                        break;
                    case R.id.radioButtonPayAtRestaurant:
                        payment_type = "2";
                        break;
                }
            }
        });
        binding.errorLayout.btnRetry.setOnClickListener(_OnClickListener);
        binding.etName.setText(userBasicInfo.name);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btnPlaceOrder:
                if (validate())
                {
                    if (payment_type.equalsIgnoreCase("1"))
                    {
                        //OnlinePayment Here
                        Intent intent = new Intent(requireActivity(), OnlinePaymentActivityLocalStore.class);
                        intent.putExtra(AppUtils.BUNDLE_CHECK_OUT,checkoutDto);
                        startActivity(intent);

                    }
                    else if(payment_type.equalsIgnoreCase("2"))
                    {
                        callPayAtStore("CashOnDelivery");
                    }
                }
                break;
        }
    }

    private boolean validate()
    {
        Log.d(TAG, "validate: delivery-" + delivery);
        if (payment_type.equalsIgnoreCase(""))
        {
            Toast.makeText(getContext(), getString(R.string.lbl_select_payment_method), Toast.LENGTH_LONG).show();
            return false;
        }
        else if (delivery.equals("Delivery") && !addressAdded)
        {
            if (firstTime)
                Toast.makeText(getContext(), getString(R.string.lbl_add_delivery_address), Toast.LENGTH_LONG).show();
            return false;
        }
        else
        {
            return true;
        }
    }

    private String getAddress(BusinessDetailInfo businessDetailInfo)
    {
        StringBuilder strAddress = new StringBuilder();
        if (businessDetailInfo.address1.isEmpty())
        {
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        else
        {
            strAddress.append(businessDetailInfo.address1.trim());
            strAddress.append(" ");
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.city.isEmpty())
        {
            strAddress.append(businessDetailInfo.city.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.country.isEmpty())
        {
            strAddress.append(businessDetailInfo.country.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.postcode.isEmpty())
        {
            strAddress.append(businessDetailInfo.postcode.trim());
        }
        return strAddress.toString();
    }

    private void callPayAtStore(String paymentType)
    {
        mProgressDialog.show();

        Call<OnlinePaymentWrapper> getPaymentIntegration;

        PaymentIntegrationApi paymentIntegrationApi = new PaymentIntegrationApi();
        paymentIntegrationApi.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        paymentIntegrationApi.setAccountId(userBasicInfo.accountId);
        paymentIntegrationApi.setBusinessId(businessId);
        paymentIntegrationApi.setUserId(userBasicInfo.userId);
        paymentIntegrationApi.setPaymentType(paymentType);
        paymentIntegrationApi.setSpecialIns(special_ins);
        paymentIntegrationApi.setAllergyIns(allergy_ins);
        paymentIntegrationApi.setDistance("1");
        paymentIntegrationApi.setOrderType(delivery);
        paymentIntegrationApi.setPlatforms("android");
        String retailerTypeId = checkoutDto.businessDetailInfo.retailerTypeId;
        switch (Integer.parseInt(retailerTypeId))
        {
            case AppUtils.BMT_EATS:
                 paymentIntegrationApi.setTableId(tableId);
                 break;
            default:
                 paymentIntegrationApi.setStoreId(tableId);
                 break;
        }
        paymentIntegrationApi.setPlaceOrder(AppUtils.PLACE_ORDER_AT_TABLE_LOCAL);
        paymentIntegrationApi.setDescription(description);
        paymentIntegrationApi.setIpAddress(getIpAddress(getContext()));

        switch (Integer.parseInt(retailerTypeId))
        {
            case AppUtils.BMT_EATS:
                 getPaymentIntegration = apiInterface.orderAtTableManualPayment(paymentIntegrationApi);
                 break;
            default:
                 getPaymentIntegration = apiInterface.orderAtStoreLocalCOD(paymentIntegrationApi);
                 break;
        }
        getPaymentIntegration.enqueue(new Callback<OnlinePaymentWrapper>()
        {
            @Override
            public void onResponse(Call<OnlinePaymentWrapper> call, Response<OnlinePaymentWrapper> response)
            {
                try
                {
                    mProgressDialog.dismiss();

                    try
                    {
                        OnlinePaymentWrapper onlinePaymentWrapper = response.body();

                        PaymentAPIResponse paymentAPIResponse = onlinePaymentWrapper.getPaymentAPIResponse();
                        PlaceOrderResultResponse placeOrderResultResponse = paymentAPIResponse.getPlaceOrderResultResponse();

                        if (paymentAPIResponse.errorDetails.getErrorCode() == 0)
                        {
                            Bundle bundle = new Bundle();
                            bundle.putString(AppUtils.BUNDLE_OIYAA_BONUS, "" + placeOrderResultResponse.getLvBonus());
                            bundle.putString(AppUtils.BUNDLE_BONUS_TOKENS, "" + placeOrderResultResponse.getBonusTokens());
                            bundle.putString(AppUtils.BUNDLE_NEW_TOKEN_BALANCE, "" + placeOrderResultResponse.getNewTokenBalance());
                            //For review
                            bundle.putString("businessId", businessID);
                            bundle.putString("businessImage", businessImage);
                            bundle.putString("businessName", businessName);
                            bundle.putString("businessType", businessType);
                            bundle.putString("businessAddress", businessAddress);
                            ActivityController.startNextActivity(getActivity(), LocalSuccessPopupForOrderSubmit.class, bundle, false);
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(paymentAPIResponse.errorDetails.getErrorMessage(), getActivity());
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
                catch (Exception e)
                {
                    mProgressDialog.dismiss();
                    Log.e(TAG, " : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<OnlinePaymentWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.e(TAG, " : " + t.getMessage());
            }
        });
    }

    public static String getIpAddress(Context context)
    {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext()
                .getSystemService(context.WIFI_SERVICE);

        String ipAddress = intToInetAddress(wifiManager.getDhcpInfo().ipAddress).toString();

        ipAddress = ipAddress.substring(1);

        return ipAddress;
    }

    public static InetAddress intToInetAddress(int hostAddress)
    {
        byte[] addressBytes = {(byte) (0xff & hostAddress),
                (byte) (0xff & (hostAddress >> 8)),
                (byte) (0xff & (hostAddress >> 16)),
                (byte) (0xff & (hostAddress >> 24))};

        try
        {
            return InetAddress.getByAddress(addressBytes);
        }
        catch (UnknownHostException e)
        {
            throw new AssertionError();
        }
    }

    private void showProgress()
    {
        binding.progressLayout.progressBar.setVisibility(View.VISIBLE);
        binding.nestedScrollView.setVisibility(View.GONE);
        binding.errorLayout.layoutRoot.setVisibility(View.GONE);
    }

    private void showContents()
    {
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        binding.nestedScrollView.setVisibility(View.VISIBLE);
        binding.errorLayout.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage,boolean retry)
    {
        binding.progressLayout.progressBar.setVisibility(View.GONE);
        binding.nestedScrollView.setVisibility(View.GONE);
        binding.errorLayout.layoutRoot.setVisibility(View.VISIBLE);
        binding.errorLayout.tvError.setText(strMessage);
        if(retry) binding.errorLayout.btnRetry.setVisibility(View.VISIBLE);
        else binding.errorLayout.btnRetry.setVisibility(View.GONE);
    }

}
