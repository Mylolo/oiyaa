package com.localvalu.directory.payment;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.databinding.DataBindingUtil;

import com.localvalu.R;
import com.localvalu.base.MainActivity;
import com.localvalu.databinding.ActivityPopupSuccessForOrderSubmitBinding;
import com.localvalu.common.review.ReviewsAddFragment;
import com.utility.ActivityController;
import com.utility.AppUtils;

import java.util.Random;

public class LocalSuccessPopupForOrderSubmit extends Activity
{
    private ActivityPopupSuccessForOrderSubmitBinding binding;
    private MediaPlayer player;
    private AnimationDrawable frameAnimation;
    private String lisamoBonus, bonusTokens, newTokenBalance;
    private String businessID, businessImage, businessName, businessType, businessAddress;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    final int min = 1;
    final int max = 9;
    final int[] sounds = new int[10];

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        getWindow().getDecorView().findViewById(android.R.id.content).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setFinishOnTouchOutside(false);
        setTheme(R.style.AppThemeLocal);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_popup_success_for_order_submit);
        initSounds();
        init();
        readFromBundle();

    }

    public void initSounds()
    {
        sounds[0] = R.raw.coin_drops;
        sounds[1] = R.raw.sockitto;
        sounds[2] = R.raw.sportsevent;
        sounds[3] = R.raw.urcrazee;
        sounds[4] = R.raw.wow1;
        sounds[5] = R.raw.wow2;
        sounds[6] = R.raw.coin_drops;
        sounds[7] = R.raw.yeah;
        sounds[8] = R.raw.yeah1;
        sounds[9] = R.raw.startofhorserace;
    }

    private void init()
    {

        player = MediaPlayer.create(this, sounds[getRandomSound()]);
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
    }

    private void readFromBundle()
    {
        try
        {
            lisamoBonus = getIntent().getExtras().getString(AppUtils.BUNDLE_OIYAA_BONUS);
            bonusTokens = getIntent().getExtras().getString(AppUtils.BUNDLE_BONUS_TOKENS);
            newTokenBalance = getIntent().getExtras().getString(AppUtils.BUNDLE_NEW_TOKEN_BALANCE);

            //For review page
            businessID = getIntent().getExtras().getString("businessId");
            businessImage = getIntent().getExtras().getString("businessImage");
            businessName = getIntent().getExtras().getString("businessName");
            businessType = getIntent().getExtras().getString("businessType");
            businessAddress = getIntent().getExtras().getString("businessAddress");


            if (lisamoBonus.equals("null"))
                lisamoBonus = "0";


            if (bonusTokens.equals("null"))
                bonusTokens = "0";


            if (newTokenBalance.equals("null"))
                newTokenBalance = "0";

            binding.tvRewardsToken.setText(strCurrencyLetter + String.format("%,.2f", (Float.parseFloat(bonusTokens))));
            binding.tvNewLoyaltyBalance.setText(strCurrencyLetter + String.format("%,.2f", (Float.parseFloat(newTokenBalance))));
            binding.tvExtraTokens.setText(strCurrencyLetter + String.format("%,.2f", (Float.parseFloat(lisamoBonus))));

        }
        catch (Exception e)
        {

        }
    }

    public void setView()
    {

    }

    public int getRandomSound()
    {
        return new Random().nextInt((max - min) + 1) + min;
    }

    public void onButtonClick(View v)
    {
        Bundle bundle = new Bundle();
        switch (v.getId())
        {
            case R.id.iv_close:
            case R.id.btnCollect:
                 player.release();
                 bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_LOCAL);
                 ActivityController.startNextActivity(this, MainActivity.class, bundle, true);
                 break;
        }
    }

    public void gotoReviewPage()
    {
        ReviewsAddFragment reviewsAddFragment = ReviewsAddFragment.newInstance();
        Bundle bundle = new Bundle();
        bundle.putString("section_type", "info");
        bundle.putString("retailer_id", businessID);
        bundle.putString("order_id", "");
        bundle.putString("businessImage", businessImage);
        bundle.putString("businessName", businessName);
        bundle.putString("businessType", businessType);
        bundle.putString("businessAddress", businessAddress);
        //eatsReviewsAddFragment.setArguments(bundle);
        //((MainActivity) activity).addFragment(eatsReviewsAddFragment, true, EatsReviewsAddFragment.class.getName(), true);
    }


}