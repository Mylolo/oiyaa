package com.localvalu.directory.payment.online;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import com.localvalu.BuildConfig;
import com.localvalu.R;
import com.localvalu.directory.payment.LocalSuccessPopupForOrderSubmit;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.eats.payment.dto.CredoraxHPPRequest;
import com.localvalu.eats.payment.dto.CredoraxHPPRequestResult;
import com.localvalu.eats.payment.dto.OnlinePaymentResponseOne;
import com.localvalu.eats.payment.dto.OnlinePaymentResponseTwo;
import com.localvalu.eats.payment.dto.PaymentIntegrationEat;
import com.localvalu.eats.payment.dto.PlaceOrderOne;
import com.localvalu.eats.payment.dto.PlaceOrderOneResult;
import com.localvalu.eats.payment.dto.PlaceOrderSecond;
import com.localvalu.eats.payment.dto.PlaceOrderSecondResult;
import com.localvalu.eats.payment.dto.VoucherPurchase;
import com.localvalu.eats.payment.dto.VoucherPurchaseRequest;
import com.localvalu.eats.payment.dto.VoucherPurchaseResponse;
import com.localvalu.eats.payment.dto.VoucherPurchaseResult;
import com.localvalu.eats.payment.online.OnlinePaymentActivityEat;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnlinePaymentActivityLocal extends AppCompatActivity
{

    WebView webview;
    public ApiInterface apiInterface;
    private UserBasicInfo userBasicInfo;
    Context context;
    private ArrayList<CartDetailDto> cartDetails;
    private String businessId, special_ins, allergy_ins, delivery;
    String TAG = OnlinePaymentActivityEat.class.getName();
    private ProgressDialog mProgressDialog;
    Activity activity;
    private boolean canGoBack = false;

    private String strEatsSuccessUrl;
    private String strEatsFailureUrl;
    private String strEatsBackUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_payment_eat);
        context = this;
        activity = this;
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(this, PreferenceUtility.APP_PREFERENCE_NAME);
        readFromBundle();
        setView();
    }

    private void setView()
    {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(context, PreferenceUtility.APP_PREFERENCE_NAME);
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        webview = findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(webViewClient);
        webview.setWebChromeClient(webChromeClient);
        callPaymentApi();
    }

    private void readFromBundle()
    {
        Bundle bundle = getIntent().getBundleExtra("data");

        businessId = bundle.getString("businessId");
        special_ins = bundle.getString("special_ins");
        allergy_ins = bundle.getString("allergy_ins");
        delivery = bundle.getString("delivery");

        strEatsSuccessUrl = BuildConfig.BASE_URL_CREDORAX + "credorax/Success.php";
        strEatsFailureUrl = BuildConfig.BASE_URL_CREDORAX  +  "credorax/Error.php";
        strEatsBackUrl =  BuildConfig.BASE_URL_CREDORAX  +  "credorax/Back.php";
    }

    public PaymentIntegrationEat getPaymentIntegration(boolean isFirst,String url)
    {
        PaymentIntegrationEat paymentIntegrationEat = new PaymentIntegrationEat();
        paymentIntegrationEat.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        paymentIntegrationEat.setAccountId(userBasicInfo.accountId);
        paymentIntegrationEat.setBusinessId(businessId);
        paymentIntegrationEat.setUserId(userBasicInfo.userId);
        paymentIntegrationEat.setSpecialIns(special_ins);
        paymentIntegrationEat.setAllergyIns(allergy_ins);
        paymentIntegrationEat.setDistance("12");
        paymentIntegrationEat.setPaymentType("PayByCard");
        paymentIntegrationEat.setOrderType(delivery);
        paymentIntegrationEat.setPlatforms("android");
        if(!isFirst)
        {
            try
            {
                Uri uri = Uri.parse(url);
                paymentIntegrationEat.setK(uri.getQueryParameter("K"));
                paymentIntegrationEat.setM(uri.getQueryParameter("M"));
                paymentIntegrationEat.setO(uri.getQueryParameter("O"));
                paymentIntegrationEat.setT(uri.getQueryParameter("T"));
                paymentIntegrationEat.setV(uri.getQueryParameter("V"));
                paymentIntegrationEat.setA1(uri.getQueryParameter("a1"));
                paymentIntegrationEat.setA2(uri.getQueryParameter("a2"));
                paymentIntegrationEat.setA4(uri.getQueryParameter("a4"));
                paymentIntegrationEat.setA6(uri.getQueryParameter("a6"));
                paymentIntegrationEat.setA7(uri.getQueryParameter("a7"));
                paymentIntegrationEat.setA9(uri.getQueryParameter("a9"));
                paymentIntegrationEat.setB1(uri.getQueryParameter("b1"));
                paymentIntegrationEat.setB2(uri.getQueryParameter("b2"));
                paymentIntegrationEat.setB3(uri.getQueryParameter("b3"));
                paymentIntegrationEat.setB4(uri.getQueryParameter("b4"));
                paymentIntegrationEat.setC1(uri.getQueryParameter("c1"));
                paymentIntegrationEat.setG1(uri.getQueryParameter("g1"));
                paymentIntegrationEat.setZ1(uri.getQueryParameter("z1"));
                paymentIntegrationEat.setZ13(uri.getQueryParameter("z13"));
                paymentIntegrationEat.setZ14(uri.getQueryParameter("z14"));
                paymentIntegrationEat.setZ2(uri.getQueryParameter("z2"));
                paymentIntegrationEat.setZ3(uri.getQueryParameter("z3"));
                paymentIntegrationEat.setZ33(uri.getQueryParameter("z33"));
                paymentIntegrationEat.setZ34(uri.getQueryParameter("z34"));
                paymentIntegrationEat.setZ39(uri.getQueryParameter("z39"));
                paymentIntegrationEat.setZ4(uri.getQueryParameter("z4"));
                paymentIntegrationEat.setZ41(uri.getQueryParameter("z41"));
                paymentIntegrationEat.setZ6(uri.getQueryParameter("z6"));
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        if (isFirst)
        {
            paymentIntegrationEat.setPaymentType("PayByCard");
            /*paymentIntegrationEat.setSuccessUrl("https://www.localvalu.com/credorax/Success.php");
            paymentIntegrationEat.setFailureUrl("https://www.localvalu.com/credorax/Error.php");
            paymentIntegrationEat.setBackUrl("https://www.localvalu.com/credorax/Back.php");*/

            paymentIntegrationEat.setSuccessUrl(strEatsSuccessUrl);
            paymentIntegrationEat.setFailureUrl(strEatsFailureUrl);
            paymentIntegrationEat.setBackUrl(strEatsBackUrl);

        }
        return paymentIntegrationEat;
    }

    /**
     * First step - CallPaymentApi is the first api we need to call placeOrderOneApi.
     */
    private void callPaymentApi()
    {
        mProgressDialog.show();

        Call<OnlinePaymentResponseOne> getPaymentIntegration = apiInterface.startOnlinePaymentLocal(getPaymentIntegration(true,""));
        getPaymentIntegration.enqueue(new Callback<OnlinePaymentResponseOne>()
        {
            @Override
            public void onResponse(Call<OnlinePaymentResponseOne> call, Response<OnlinePaymentResponseOne> response)
            {
                try
                {
                    mProgressDialog.dismiss();

                    try
                    {
                        OnlinePaymentResponseOne onlinePaymentResponseOne = response.body();

                        PlaceOrderOneResult placeOrderOneResult = onlinePaymentResponseOne.getPlaceOrderOneResult();


                        if (placeOrderOneResult.errorDetails.errorCode == 0)
                        {
                            //load webview url here
                            PlaceOrderOne placeOrderOne = placeOrderOneResult.placeOrderOne;
                            if (placeOrderOne != null)
                            {
                                CredoraxHPPRequestResult credoraxHPPRequestResult = placeOrderOne.credoraxHPPRequestResult;
                                CredoraxHPPRequest credoraxHPPRequest = credoraxHPPRequestResult.credoraxHPPRequest;
                                String url=credoraxHPPRequest.url;
                                url = url.replace("\\", "");
                                webview.loadUrl(url);
                            }

                            /*Bundle bundle = new Bundle();
                            bundle.putString("lisamoBonus", "" + placeOrderResultResponse.getLisamoBonus());
                            bundle.putString("bonusTokens", "" + placeOrderResultResponse.getBonusTokens());
                            bundle.putString("newTokenBalance", "" + placeOrderResultResponse.getNewTokenBalance());
                            ActivityController.startNextActivity(OnlinePaymentActivityEat.this, EatsSuccessPopupForOrderSubmit.class, bundle, false);*/
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(placeOrderOneResult.errorDetails.errorMessage, OnlinePaymentActivityLocal.this);
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                catch (Exception e)
                {
                    mProgressDialog.dismiss();
                    Log.e(TAG, " : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<OnlinePaymentResponseOne> call, Throwable t)
            {
                mProgressDialog.dismiss();
                if (!CommonUtilities.checkConnectivity(OnlinePaymentActivityLocal.this))
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), OnlinePaymentActivityLocal.this);
                }
                else
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), OnlinePaymentActivityLocal.this);
                }
            }
        });
    }

    WebViewClient webViewClient = new WebViewClient()
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            super.onPageStarted(view, url, favicon);
            Log.d(TAG, "onPageStarted: " + url);

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            Log.d(TAG, "shouldOverrideUrlLoading: url-" + url);
            if(url.contains(strEatsSuccessUrl+"?")) //Checks second api url
            {
                callOnlinePaymentSuccessAPI(url);
                /*String strMessage= "Payment Success";
                try
                {
                    Uri uri = Uri.parse(url);
                    strMessage = uri.getQueryParameter("z3");
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                callOnlinePaymentSuccessAPI();
                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                Bundle bundle = new Bundle();
                bundle.putString("lolotype", "eats");
                ActivityController.startNextActivity(OnlinePaymentActivityEat.this, MainActivity.class, null, true);*/
            }
            else if (url.contains(strEatsFailureUrl)) //Checks second api url
            {
                showErrorDialog();
                //callOnlinePaymentSuccessAPI(url);
                /*String strMessage= "Payment Success";
                try
                {
                    Uri uri = Uri.parse(url);
                    strMessage = uri.getQueryParameter("z3");
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                callOnlinePaymentSuccessAPI();
                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                Bundle bundle = new Bundle();
                bundle.putString("lolotype", "eats");
                ActivityController.startNextActivity(OnlinePaymentActivityEat.this, MainActivity.class, null, true);*/
            }
            else
            {
                webview.loadUrl(url);
                DialogUtility.showMessageWithOk("Payment Transaction Failure", OnlinePaymentActivityLocal.this);
            }
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request)
        {

            final Uri uri = request.getUrl();
            String url = uri.toString();
            if(url.toString().contains(strEatsSuccessUrl+"?")) //Checks second api url
            {
                callOnlinePaymentSuccessAPI(url.toString());
                /*String strMessage= "Payment Success";
                try
                {
                    Uri uri = Uri.parse(url);
                    strMessage = uri.getQueryParameter("z3");
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                callOnlinePaymentSuccessAPI();
                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                Bundle bundle = new Bundle();
                bundle.putString("lolotype", "eats");
                ActivityController.startNextActivity(OnlinePaymentActivityEat.this, MainActivity.class, null, true);*/
            }
            else if (url.toString().contains(strEatsFailureUrl)) //Checks second api url
            {
                showErrorDialog();
                //callOnlinePaymentSuccessAPI(url.toString());
                /*String strMessage= "Payment Success";
                try
                {
                    Uri uri = Uri.parse(url);
                    strMessage = uri.getQueryParameter("z3");
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                callOnlinePaymentSuccessAPI();
                Toast.makeText(context, strMessage, Toast.LENGTH_SHORT).show();
                Bundle bundle = new Bundle();
                bundle.putString("lolotype", "eats");
                ActivityController.startNextActivity(OnlinePaymentActivityEat.this, MainActivity.class, null, true);*/
            }
            else
            {
                webview.loadUrl(url.toString());
                DialogUtility.showMessageWithOk("Payment Transaction Failure", OnlinePaymentActivityLocal.this);
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            super.onPageFinished(view, url);
            Log.d(TAG, "onPageFinished: " + url);
            mProgressDialog.dismiss();
        }
    };

    WebChromeClient webChromeClient = new WebChromeClient()
    {
        @Override
        public void onProgressChanged(WebView view, int progress)
        {
            mProgressDialog.show();

            if (progress == 100)
            {
                mProgressDialog.dismiss();
            }
        }
    };

    public void callOnlinePayMallVoucherHppRequest()
    {
        mProgressDialog.show();
        Call<VoucherPurchaseResponse> voucherPurchaseResponseCall = apiInterface.
                requestForVoucherPurchase(getVoucherPurchasePayload());
        voucherPurchaseResponseCall.enqueue(new Callback<VoucherPurchaseResponse>()
        {
            @Override
            public void onResponse(Call<VoucherPurchaseResponse> call, Response<VoucherPurchaseResponse> response)
            {
                try
                {
                    mProgressDialog.dismiss();
                    try
                    {
                        VoucherPurchaseResponse voucherPurchaseResponse = response.body();
                        VoucherPurchaseResult voucherPurchaseResult = voucherPurchaseResponse.getVoucherPurchaseResult();
                        if (voucherPurchaseResult.errorDetails.errorCode == 0)
                        {
                            //load webview url here
                            VoucherPurchase voucherPurchase = voucherPurchaseResult.getVoucherPurchase();
                            if (voucherPurchase != null)
                            {
                                webview.loadUrl(voucherPurchase.getUrl());
                            }

                            /*Bundle bundle = new Bundle();
                            bundle.putString("lisamoBonus", "" + placeOrderResultResponse.getLisamoBonus());
                            bundle.putString("bonusTokens", "" + placeOrderResultResponse.getBonusTokens());
                            bundle.putString("newTokenBalance", "" + placeOrderResultResponse.getNewTokenBalance());
                            ActivityController.startNextActivity(OnlinePaymentActivityEat.this, EatsSuccessPopupForOrderSubmit.class, bundle, false);*/
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(voucherPurchaseResult.errorDetails.errorMessage, OnlinePaymentActivityLocal.this);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                catch (Exception e)
                {
                    mProgressDialog.dismiss();
                    Log.e(TAG, " : " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<VoucherPurchaseResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                if (!CommonUtilities.checkConnectivity(OnlinePaymentActivityLocal.this))
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), OnlinePaymentActivityLocal.this);
                }
                else
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), OnlinePaymentActivityLocal.this);
                }
            }
        });

    }

    public VoucherPurchaseRequest getVoucherPurchasePayload()
    {
        VoucherPurchaseRequest voucherPurchaseRequest = new VoucherPurchaseRequest();
        voucherPurchaseRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        voucherPurchaseRequest.setAccountId(userBasicInfo.accountId);
        voucherPurchaseRequest.setMerchantId(businessId);
        voucherPurchaseRequest.setSuccessUrl("https://www.localvalu.com/lolo-mall/voucher/GetMallVoucherAmount/" + businessId);
        voucherPurchaseRequest.setFailureUrl("https://www.localvalu.com/lolo-mall/voucher/GetMallVoucherAmount/" + businessId);
        voucherPurchaseRequest.setBackUrl("https://www.localvalu.com/lolo-mall/voucher/GetMallVoucherAmount/" + businessId);
        return voucherPurchaseRequest;
    }

    public void callOnlinePaymentSuccessAPI(String url)
    {
        mProgressDialog.show();
        Call<OnlinePaymentResponseTwo> onlinePaymentResponseTwoCall = apiInterface.
                finishOnlinePaymentLocal(getPaymentIntegration(false,url));
        onlinePaymentResponseTwoCall.enqueue(new Callback<OnlinePaymentResponseTwo>()
        {
            @Override
            public void onResponse(Call<OnlinePaymentResponseTwo> call, Response<OnlinePaymentResponseTwo> response)
            {
                try
                {
                    mProgressDialog.dismiss();
                    try
                    {
                        OnlinePaymentResponseTwo onlinePaymentResponseTwo = response.body();
                        PlaceOrderSecondResult placeOrderSecondResult = onlinePaymentResponseTwo.getPlaceOrderSecondResult();
                        if (placeOrderSecondResult.errorDetails.errorCode == 0)
                        {
                            //load webview url here
                            PlaceOrderSecond placeOrderSecond = placeOrderSecondResult.getPlaceOrderSecond();

                            if (placeOrderSecond != null)
                            {
                                Bundle bundle = new Bundle();
                                bundle.putString(AppUtils.BUNDLE_OIYAA_BONUS, "" + placeOrderSecond.getLvBonus());
                                bundle.putString(AppUtils.BUNDLE_BONUS_TOKENS, "" + placeOrderSecond.getBonusTokens());
                                bundle.putString(AppUtils.BUNDLE_NEW_TOKEN_BALANCE, "" + placeOrderSecond.getNewTokenBalance());
                                ActivityController.startNextActivity(OnlinePaymentActivityLocal.this, LocalSuccessPopupForOrderSubmit.class, bundle, false);
                            }
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(placeOrderSecondResult.errorDetails.errorMessage, OnlinePaymentActivityLocal.this);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                catch (Exception e)
                {
                    mProgressDialog.dismiss();
                    Log.e(TAG, " : " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<OnlinePaymentResponseTwo> call, Throwable t)
            {
                mProgressDialog.dismiss();
                if (!CommonUtilities.checkConnectivity(OnlinePaymentActivityLocal.this))
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), OnlinePaymentActivityLocal.this);
                }
                else
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), OnlinePaymentActivityLocal.this);
                }
            }
        });

    }

    public static String getIpAddress(Context context)
    {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext()
                .getSystemService(WIFI_SERVICE);

        String ipAddress = intToInetAddress(wifiManager.getDhcpInfo().ipAddress).toString();

        ipAddress = ipAddress.substring(1);

        return ipAddress;
    }

    public static InetAddress intToInetAddress(int hostAddress)
    {
        byte[] addressBytes = {(byte) (0xff & hostAddress),
                (byte) (0xff & (hostAddress >> 8)),
                (byte) (0xff & (hostAddress >> 16)),
                (byte) (0xff & (hostAddress >> 24))};

        try
        {
            return InetAddress.getByAddress(addressBytes);
        }
        catch (UnknownHostException e)
        {
            throw new AssertionError();
        }
    }

    private void showErrorDialog()
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(getString(R.string.lbl_kindly_verify_details));
        dialog.setTitle(R.string.dialog_title);
        dialog.setNeutralButton(this.getResources().getString(R.string.okay), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                OnlinePaymentActivityLocal.this.finish();
            }
        });
        dialog.show();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();

        if (!canGoBack)
        {

        }
        else
        {

        }
    }

}
