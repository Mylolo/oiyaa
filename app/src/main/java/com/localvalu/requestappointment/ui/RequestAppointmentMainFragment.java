package com.localvalu.requestappointment.ui;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.requestappointment.dto.RequestAppointment;
import com.localvalu.tablebooking.dto.TableBooking;
import com.localvalu.tablebooking.ui.TableBookingListFragment;
import com.utility.AppUtils;

import java.util.ArrayList;
import java.util.List;


public class RequestAppointmentMainFragment extends BaseFragment
{

    //Toolbar
    private Toolbar toolbar;
    private TextView tvTitle;
    private int colorTheme;
    private HostActivityListener hostActivityListener;
    private int navigationType;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private int receivedStatus;


    public static RequestAppointmentMainFragment newInstance()
    {
        RequestAppointmentMainFragment fragment = new RequestAppointmentMainFragment();
        return fragment;
    }

    private void readFromBundle()
    {
        if (getArguments() != null)
        {
            navigationType = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
            receivedStatus = getArguments().getInt(AppUtils.BUNDLE_REQUEST_APPOINTMENT_LIST_STATUS);
        }
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        readFromBundle();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_table_bookings, container, false);
        initView();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
    }

    private void setProperties()
    {
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        /*tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener()
        {
            @Override
            public void onTabSelected(TabLayout.Tab tab)
            {
                Fragment fragment = adapter.getItem(tab.getPosition());

                if(fragment instanceof TableBookingListFragment)
                {
                    TableBookingListFragment tableBookingListFragment = (TableBookingListFragment) fragment;
                    if(tableBookingListFragment!=null)
                    {
                        tableBookingListFragment.invokeTableBookingListApi();
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab)
            {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab)
            {

            }
        });
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        tabLayout.selectTab(tab);*/
    }

    private void initView()
    {
        tabLayout = view.findViewById(R.id.tab_pager_heading);
        viewPager = view.findViewById(R.id.viewPager);
    }

    private void setUpActionBar()
    {
        switch (navigationType)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
            case AppUtils.BMT_EATS:
            case AppUtils.BMT_LOCAL:
                colorTheme = ContextCompat.getColor(requireContext(), R.color.colorEats);
                break;
            case AppUtils.BMT_MALL:
                colorTheme = ContextCompat.getColor(requireContext(), R.color.colorMall);
                break;
        }
        toolbar = view.findViewById(R.id.toolbar);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setTextColor(colorTheme);
        tvTitle.setText(getString(R.string.lbl_my_appointments));
        toolbar.getNavigationIcon().setColorFilter(colorTheme, PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private void setupViewPager(ViewPager viewPager)
    {
        adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(RequestAppointmentListFragment.newInstance(false,0), getString(R.string.lbl_accepted_small));
        adapter.addFragment(RequestAppointmentListFragment.newInstance(false,5), getString(R.string.lbl_cancelled_or_declined));
        adapter.addFragment(RequestAppointmentListFragment.newInstance(true,3), getString(R.string.lbl_pending));
        viewPager.setAdapter(adapter);
        switch (receivedStatus)
        {
            case 0:
                 viewPager.setCurrentItem(0);
                 break;
            case 3:
                 viewPager.setCurrentItem(2);
                 break;
            case 5:
                 viewPager.setCurrentItem(1);
                 break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager)
        {
            super(manager);
        }

        @Override
        public Fragment getItem(int position)
        {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount()
        {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title)
        {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return mFragmentTitleList.get(position);
        }
    }

    public void addToCancelledTab(RequestAppointment requestAppointment)
    {
        if(adapter!=null)
        {
            RequestAppointmentListFragment fragment = (RequestAppointmentListFragment) adapter.getItem(1);

            if(fragment!=null)
            {
                fragment.addItem(requestAppointment);
            }
        }

    }

    @Override
    public void onDestroyView()
    {
        tabLayout.addOnTabSelectedListener(null);
        super.onDestroyView();
    }
}
