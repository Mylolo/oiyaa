package com.localvalu.requestappointment.ui;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.databinding.FragmentTableBookingListBinding;
import com.localvalu.requestappointment.dto.RequestAppointment;
import com.localvalu.requestappointment.dto.RequestAppointmentCancelRequest;
import com.localvalu.requestappointment.dto.RequestAppointmentCancelResponse;
import com.localvalu.requestappointment.dto.RequestAppointmentCancelResult;
import com.localvalu.requestappointment.dto.RequestAppointmentListRequest;
import com.localvalu.requestappointment.dto.RequestAppointmentListResponse;
import com.localvalu.requestappointment.dto.RequestAppointmentListResult;

import com.localvalu.tablebooking.dto.TableBooking;
import com.localvalu.tablebooking.dto.TableBookingListRequest;
import com.localvalu.tablebooking.dto.TableBookingListResponse;
import com.localvalu.tablebooking.dto.TableBookingListResult;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AlertDialogCallBack;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.Constants;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;
import com.utility.VerticalSpacingItemDecoration;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RequestAppointmentListFragment extends BaseFragment
{
    private static final String TAG = "RAListFragment";
    private FragmentTableBookingListBinding binding;
    private LinearLayoutManager layoutManager;
    private RequestAppointmentListAdapter requestAppointmentListAdapter;
    private boolean canShowCancel;
    private int status;
    private int cancelledStatus;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private int verticalSpace=10;
    private int cancelPosition;
    private List<RequestAppointment> data;

    // TODO: Rename and change types and number of parameters
    public static RequestAppointmentListFragment newInstance(boolean canShowCancel, int status)
    {
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppUtils.BUNDLE_TABLE_BOOKING_SHOW_CANCEL,canShowCancel);
        bundle.putInt(AppUtils.BUNDLE_TABLE_BOOKING_STATUS,status);
        RequestAppointmentListFragment tableBookingListFragment = new RequestAppointmentListFragment();
        tableBookingListFragment.setArguments(bundle);
        return tableBookingListFragment;
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        buildObjectForHandlingAPI();
        readFromBundle();
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        verticalSpace = (int) getResources().getDimension(R.dimen._15sdp);
        cancelledStatus = Integer.parseInt(Constants.BOOKING_CANCELLED);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentTableBookingListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setProperties();
        callRequestAppointmentListAPI();
    }

    public void invokeTableBookingListApi()
    {
        callRequestAppointmentListAPI();
    }

    private void readFromBundle()
    {
        if(getArguments()!=null)
        {
            canShowCancel = getArguments().getBoolean(AppUtils.BUNDLE_TABLE_BOOKING_SHOW_CANCEL);
            status = getArguments().getInt(AppUtils.BUNDLE_TABLE_BOOKING_STATUS);
        }
    }

    private void setProperties()
    {
        showRadioButtons();
        setAdapter();
        binding.layoutError.tvError.setText(getString(R.string.lbl_no_appointments_found));
        binding.layoutError.btnRetry.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                callRequestAppointmentListAPI();
            }
        });
        if(Constants.BOOKING_CANCELLED.equals(Integer.toString(status)))
        {
             binding.radioButtonCancelled.postDelayed(new Runnable()
             {
                 @Override
                 public void run()
                 {
                     binding.radioButtonCancelled.setChecked(true);
                 }
             },2000);
        }
    }

    private void showRadioButtons()
    {
        String strStatus = Integer.toString(status);

        switch (strStatus)
        {
            case Constants.ACCEPT:
            case Constants.PENDING:
                binding.radGroupStatus.setVisibility(View.GONE);
                break;
            case Constants.BOOKING_CANCELLED:
            case Constants.DECLINED:
                binding.radGroupStatus.setVisibility(View.VISIBLE);
                break;
        }

        binding.radGroupStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId)
            {
                switch (checkedId)
                {
                    case R.id.radioButtonDeclined:
                         cancelledStatus=2;
                         callRequestAppointmentListAPI();
                         break;
                    case R.id.radioButtonCancelled:
                         cancelledStatus=5;
                         callRequestAppointmentListAPI();
                         break;
                }
            }
        });

        Typeface typeface = ResourcesCompat.getFont(requireContext(),R.font.comfortaa_regular);
        binding.radioButtonCancelled.setTypeface(typeface);
        binding.radioButtonDeclined.setTypeface(typeface);
    }

    private void setAdapter()
    {
        layoutManager = new LinearLayoutManager(requireContext());
        binding.rvTableBooking.setLayoutManager(layoutManager);
        requestAppointmentListAdapter = new RequestAppointmentListAdapter(canShowCancel);
        requestAppointmentListAdapter.setOnClickListener(new RequestAppointmentListAdapter.OnItemClickListener()
        {

            @Override
            public void onItemCancel(final RequestAppointment requestAppointment, final int position)
            {
                cancelPosition = position;
                DialogUtility.showMessageWithYesOrNoCallback(getString(R.string.lbl_confirm_cancel_msg_request_appointment), getActivity(),
                        new AlertDialogCallBack()
                        {

                            @Override
                            public void onSubmit()
                            {
                                callRequestAppointmentCancelAPI(requestAppointment.getSlotId());
                            }

                            @Override
                            public void onCancel()
                            {

                            }
                        });
            }

        });
        binding.rvTableBooking.setAdapter(requestAppointmentListAdapter);
        binding.rvTableBooking.addItemDecoration(new VerticalSpacingItemDecoration(verticalSpace));
    }

    private RequestAppointmentListRequest getRequestAppointmentListRequest()
    {
        int tempStatus = status;

        if(Constants.BOOKING_CANCELLED.equals(Integer.toString(status)))
        {
            tempStatus = cancelledStatus;
        }
        RequestAppointmentListRequest requestAppointmentListRequest = new RequestAppointmentListRequest();
        requestAppointmentListRequest.setUserId(userBasicInfo.userId);
        requestAppointmentListRequest.setStatus(tempStatus);
        //requestAppointmentListRequest.setRequestFor(AppUtils.REQUEST_TYPE_REQUEST_APPOINTMENT);
        return requestAppointmentListRequest;
    }

    private RequestAppointmentCancelRequest getRequestAppointmentCancelRequest(String slotId)
    {
        RequestAppointmentCancelRequest requestAppointmentCancelRequest = new RequestAppointmentCancelRequest();
        requestAppointmentCancelRequest.setUserId(userBasicInfo.userId);
        requestAppointmentCancelRequest.setSlotId(slotId);
        return requestAppointmentCancelRequest;
    }

    private void callRequestAppointmentListAPI()
    {
        showProgress();

        Call<RequestAppointmentListResponse> dtoCall = apiInterface.getRequestAppointmentList(getRequestAppointmentListRequest());
        dtoCall.enqueue(new Callback<RequestAppointmentListResponse>()
        {
            @Override
            public void onResponse(Call<RequestAppointmentListResponse> call, Response<RequestAppointmentListResponse> response)
            {
                //mProgressDialog.dismiss();

                try
                {
                    RequestAppointmentListResponse requestAppointmentListResponse = response.body();

                    if(requestAppointmentListResponse!=null)
                    {
                        RequestAppointmentListResult requestAppointmentListResult = requestAppointmentListResponse.getRequestAppointmentListResult();

                        if(requestAppointmentListResult!=null)
                        {
                            if(requestAppointmentListResult.errorDetails.errorCode == 0)
                            {
                                ArrayList<RequestAppointment> requestAppointmentList = requestAppointmentListResult.getRequestAppointmentList();

                                if(requestAppointmentList.size()>0)
                                {
                                    setAdapter(requestAppointmentList);
                                    showList();
                                }
                                else
                                {
                                    showError(getString(R.string.lbl_no_appointments_found),false);
                                }
                            }
                            else
                            {
                                showError(requestAppointmentListResult.errorDetails.errorMessage,false);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RequestAppointmentListResponse> call, Throwable t)
            {
                //mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    showError(getString(R.string.network_unavailable),true);
                else
                    showError(getString(R.string.server_alert),true);
            }
        });
    }

    private void callRequestAppointmentCancelAPI(String slotId)
    {
        mProgressDialog.show();

        Call<RequestAppointmentCancelResponse> dtoCall = apiInterface.cancelRequestAppointment(getRequestAppointmentCancelRequest(slotId));
        dtoCall.enqueue(new Callback<RequestAppointmentCancelResponse>()
        {
            @Override
            public void onResponse(Call<RequestAppointmentCancelResponse> call, Response<RequestAppointmentCancelResponse> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    RequestAppointmentCancelResponse requestAppointmentCancelResponse = response.body();

                    if(requestAppointmentCancelResponse!=null)
                    {
                        RequestAppointmentCancelResult requestAppointmentCancelResult = requestAppointmentCancelResponse.getRequestAppointmentCancelResult();

                        if(requestAppointmentCancelResult!=null)
                        {
                            if(requestAppointmentCancelResult.errorDetails.errorCode == 0)
                            {
                                updateInCancelTab(requestAppointmentCancelResult);
                            }
                            else
                            {
                                DialogUtility.showMessageWithOk(requestAppointmentCancelResult.errorDetails.errorMessage,getActivity());
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RequestAppointmentCancelResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void updateInCancelTab(RequestAppointmentCancelResult requestAppointmentCancelResult)
    {
        RequestAppointment requestAppointment = data.get(cancelPosition);
        Toast.makeText(requireContext(),requestAppointmentCancelResult.message,Toast.LENGTH_LONG).show();
        data.remove(cancelPosition);
        requestAppointmentListAdapter.submitList(data);
        requestAppointmentListAdapter.notifyDataSetChanged();

        RequestAppointmentMainFragment frag = ((RequestAppointmentMainFragment)this.getParentFragment());
        if(frag!=null)
        {
            frag.addToCancelledTab(requestAppointment);
        }
    }

    private void setAdapter(final ArrayList<RequestAppointment> detail)
    {
        requestAppointmentListAdapter.submitList(detail);
        data=detail;
    }

    public void addItem(RequestAppointment requestAppointment)
    {
        if(binding.radioButtonCancelled.isChecked())
        {
            ArrayList<RequestAppointment> tempData = new ArrayList<>();
            tempData.add(requestAppointment);
            tempData.addAll(data);
            requestAppointmentListAdapter.submitList(tempData);
            requestAppointmentListAdapter.notifyDataSetChanged();
        }
    }

    private void showList()
    {
        binding.rvTableBooking.setVisibility(View.VISIBLE);
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showProgress()
    {
        binding.rvTableBooking.setVisibility(View.GONE);
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String message,boolean retry)
    {
        binding.rvTableBooking.setVisibility(View.GONE);
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(message);
        if(retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

}
