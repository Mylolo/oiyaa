package com.localvalu.requestappointment.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.localvalu.R;
import com.localvalu.requestappointment.dto.RequestAppointment;
import com.localvalu.tablebooking.dto.TableBooking;
import com.utility.DateUtility;

import java.util.ArrayList;

public class RequestAppointmentListAdapter extends ListAdapter<RequestAppointment, RequestAppointmentListAdapter.RequestAppointmentListViewHolder>
{
    private ArrayList<TableBooking> data;
    private Context context;
    private RequestAppointmentListAdapter.OnItemClickListener onClickListener;
    private boolean canCancel=false;

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.context = recyclerView.getContext();
    }

    public RequestAppointmentListAdapter(boolean canCancel)
    {
        super(DIFF_CALLBACK);
        this.canCancel = canCancel;
    }

    private static final DiffUtil.ItemCallback<RequestAppointment> DIFF_CALLBACK = new DiffUtil.ItemCallback<RequestAppointment>()
    {
        @Override
        public boolean areItemsTheSame(@NonNull RequestAppointment oldItem, @NonNull RequestAppointment newItem)
        {
            return oldItem.getSlotId() == newItem.getSlotId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull RequestAppointment oldItem, @NonNull RequestAppointment newItem)
        {
            return oldItem.getSlotId().equals(newItem.getSlotId());
        }
    };

    public void setOnClickListener(RequestAppointmentListAdapter.OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public RequestAppointmentListAdapter.RequestAppointmentListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_item_request_appointment_list, viewGroup, false);
        return new RequestAppointmentListAdapter.RequestAppointmentListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestAppointmentListAdapter.RequestAppointmentListViewHolder viewHolder, @SuppressLint("RecyclerView") final int position)
    {
        RequestAppointment requestAppointment = getItem(viewHolder.getAdapterPosition());
        StringBuilder strSlotId = new StringBuilder();
        strSlotId.append(context.getString(R.string.lbl_slot_id)).append(" ").append(requestAppointment.getSlotId());
        viewHolder.tvSlotId.setText(strSlotId.toString());
        viewHolder.tvTradingName.setText(requestAppointment.getTradingName());

        StringBuilder strDateTime = new StringBuilder();
        strDateTime.append(requestAppointment.getPreferredDate()).append(" ").append(requestAppointment.getPreferredTime());
        viewHolder.tvTime.setText(DateUtility.displayFormatDateTime(strDateTime.toString()));
        if(canCancel) viewHolder.btnCancel.setVisibility(View.VISIBLE);
        else viewHolder.btnCancel.setVisibility(View.GONE);
        viewHolder.btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemCancel(getItem(position),position);
            }
        });
    }

    public void removeItem(int position)
    {
        data.remove(position);
        notifyItemRemoved(position);
    }

    class RequestAppointmentListViewHolder extends RecyclerView.ViewHolder
    {
        public AppCompatTextView tvSlotId,tvTradingName,tvTime;
        public MaterialButton btnCancel;

        public RequestAppointmentListViewHolder(@NonNull View itemView)
        {
            super(itemView);
            tvSlotId = itemView.findViewById(R.id.tvSlotId);
            tvTradingName = itemView.findViewById(R.id.tvTradingName);
            tvTime = itemView.findViewById(R.id.tvTime);
            btnCancel = itemView.findViewById(R.id.btnCancel);
        }
    }

    public interface OnItemClickListener
    {
        void onItemCancel(RequestAppointment requestAppointment,int position);
    }

}
