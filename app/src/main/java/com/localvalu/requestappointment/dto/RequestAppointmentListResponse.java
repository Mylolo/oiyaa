package com.localvalu.requestappointment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.tablebooking.dto.TableBookingListResult;

import java.io.Serializable;

public class RequestAppointmentListResponse implements Serializable
{
    @SerializedName("RequestslotResults")
    @Expose
    private RequestAppointmentListResult requestAppointmentListResult;

    public RequestAppointmentListResult getRequestAppointmentListResult()
    {
        return requestAppointmentListResult;
    }

    public void setRequestAppointmentListResult(RequestAppointmentListResult requestAppointmentListResult)
    {
        this.requestAppointmentListResult = requestAppointmentListResult;
    }
}
