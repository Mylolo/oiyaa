package com.localvalu.requestappointment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RequestAppointment implements Serializable
{
    @SerializedName("slotid")
    @Expose
    private String slotId;

    @SerializedName("Userid")
    @Expose
    private String userId;

    @SerializedName("userName")
    @Expose
    private String userName;

    @SerializedName("tradingname")
    @Expose
    private String tradingName;

    @SerializedName("PreferredDate")
    @Expose
    private String preferredDate;

    @SerializedName("PreferredTime")
    @Expose
    private String preferredTime;

    @SerializedName("NoOfGuest")
    @Expose
    private String noOfGuest;

    @SerializedName("SpecialRequest")
    @Expose
    private String specialRequest;

    @SerializedName("status")
    @Expose
    private String status;

    public String getSlotId()
    {
        return slotId;
    }

    public void setSlotId(String slotId)
    {
        this.slotId = slotId;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getTradingName()
    {
        return tradingName;
    }

    public void setTradingName(String tradingName)
    {
        this.tradingName = tradingName;
    }

    public String getPreferredDate()
    {
        return preferredDate;
    }

    public void setPreferredDate(String preferredDate)
    {
        this.preferredDate = preferredDate;
    }

    public String getPreferredTime()
    {
        return preferredTime;
    }

    public void setPreferredTime(String preferredTime)
    {
        this.preferredTime = preferredTime;
    }

    public String getNoOfGuest()
    {
        return noOfGuest;
    }

    public void setNoOfGuest(String noOfGuest)
    {
        this.noOfGuest = noOfGuest;
    }

    public String getSpecialRequest()
    {
        return specialRequest;
    }

    public void setSpecialRequest(String specialRequest)
    {
        this.specialRequest = specialRequest;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }
}
