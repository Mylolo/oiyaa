package com.localvalu.requestappointment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/* 0 - Accept
   5 - Booking cancelled
   3 - waiting for Confirm
*/
public class RequestAppointmentCancelRequest implements Serializable
{
    @SerializedName("Token")
    @Expose
    private String token="/3+YFd5QZdSK9EKsB8+TlA==";

    @SerializedName("Userid")
    @Expose
    private String userId;

    @SerializedName("slotId")
    @Expose
    private String slotId;

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getSlotId()
    {
        return slotId;
    }

    public void setSlotId(String slotId)
    {
        this.slotId = slotId;
    }
}
