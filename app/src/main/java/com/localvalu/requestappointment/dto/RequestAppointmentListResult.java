package com.localvalu.requestappointment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.tablebooking.dto.TableBooking;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;
import java.util.ArrayList;

public class RequestAppointmentListResult implements Serializable
{
    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("RequestslotResults")
    @Expose
    private ArrayList<RequestAppointment> requestAppointmentList;

    public ErrorDetailsDto getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetailsDto errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public ArrayList<RequestAppointment> getRequestAppointmentList()
    {
        return requestAppointmentList;
    }

    public void setRequestAppointmentList(ArrayList<RequestAppointment> requestAppointmentList)
    {
        this.requestAppointmentList = requestAppointmentList;
    }

}
