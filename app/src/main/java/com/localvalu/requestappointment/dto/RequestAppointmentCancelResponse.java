package com.localvalu.requestappointment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.tablebooking.dto.TableBookingCancelResult;

import java.io.Serializable;

public class RequestAppointmentCancelResponse implements Serializable
{
    @SerializedName("slotBookingCancelResults")
    @Expose
    private RequestAppointmentCancelResult requestAppointmentCancelResult;

    public RequestAppointmentCancelResult getRequestAppointmentCancelResult()
    {
        return requestAppointmentCancelResult;
    }

    public void setRequestAppointmentCancelResult(RequestAppointmentCancelResult requestAppointmentCancelResult)
    {
        this.requestAppointmentCancelResult = requestAppointmentCancelResult;
    }
}
