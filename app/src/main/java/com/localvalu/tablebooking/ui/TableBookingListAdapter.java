package com.localvalu.tablebooking.ui;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.localvalu.R;
import com.localvalu.eats.myorder.dto.response.MyOrders;
import com.localvalu.eats.myorder.ui.MyOrdersListAdapter;
import com.localvalu.tablebooking.dto.TableBooking;
import com.squareup.picasso.Picasso;
import com.utility.Constants;
import com.utility.DateUtility;

import java.util.ArrayList;

public class TableBookingListAdapter extends ListAdapter<TableBooking, TableBookingListAdapter.TableBookingListViewHolder>
{
    private ArrayList<TableBooking> data;
    private Context context;
    private TableBookingListAdapter.OnItemClickListener onClickListener;
    private boolean canCancel=false;

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        this.context = recyclerView.getContext();
    }

    public TableBookingListAdapter(boolean canCancel)
    {
        super(DIFF_CALLBACK);
        this.canCancel = canCancel;
    }

    private static final DiffUtil.ItemCallback<TableBooking> DIFF_CALLBACK = new DiffUtil.ItemCallback<TableBooking>()
    {
        @Override
        public boolean areItemsTheSame(@NonNull TableBooking oldItem, @NonNull TableBooking newItem)
        {
            return oldItem.getSlotId() == newItem.getSlotId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull TableBooking oldItem, @NonNull TableBooking newItem)
        {
            return oldItem.getSlotId().equals(newItem.getSlotId());
        }
    };

    public void setOnClickListener(TableBookingListAdapter.OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public TableBookingListAdapter.TableBookingListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_item_booking_list, viewGroup, false);
        return new TableBookingListAdapter.TableBookingListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TableBookingListAdapter.TableBookingListViewHolder viewHolder, final int position)
    {
        TableBooking tableBooking = getItem(position);
        StringBuilder strSlotId = new StringBuilder();
        strSlotId.append(context.getString(R.string.lbl_slot_id)).append(" ").append(tableBooking.getSlotId());
        viewHolder.tvSlotId.setText(strSlotId.toString());
        viewHolder.tvTradingName.setText(tableBooking.getTradingName());
        viewHolder.tvNoOfGuests.setText(tableBooking.getNoOfGuest());
        viewHolder.tvSpecial.setText(tableBooking.getSpecialRequest());

        StringBuilder strDateTime = new StringBuilder();
        strDateTime.append(tableBooking.getPreferredDate()).append(" ").append(tableBooking.getPreferredTime());
        viewHolder.tvTime.setText(DateUtility.displayFormatDateTime(strDateTime.toString()));
        if(canCancel) viewHolder.btnCancel.setVisibility(View.VISIBLE);
        else viewHolder.btnCancel.setVisibility(View.GONE);
        viewHolder.btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemCancel(getItem(position),position);
            }
        });
    }

    public void removeItem(int position)
    {
        data.remove(position);
        notifyItemRemoved(position);
    }

    class TableBookingListViewHolder extends RecyclerView.ViewHolder
    {
        public AppCompatTextView tvSlotId,tvTradingName,tvNoOfGuests,tvTime,tvSpecial;
        public MaterialButton btnCancel;

        public TableBookingListViewHolder(@NonNull View itemView)
        {
            super(itemView);
            tvSlotId = itemView.findViewById(R.id.tvSlotId);
            tvTradingName = itemView.findViewById(R.id.tvTradingName);
            tvNoOfGuests = itemView.findViewById(R.id.tvNoOfGuests);
            tvSpecial = itemView.findViewById(R.id.tvSpecial);
            tvTime = itemView.findViewById(R.id.tvTime);
            btnCancel = itemView.findViewById(R.id.btnCancel);
        }
    }

    public interface OnItemClickListener
    {
        void onItemCancel(TableBooking tableBooking,int position);
    }

}
