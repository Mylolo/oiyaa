package com.localvalu.tablebooking.ui;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.databinding.FragmentTableBookingListBinding;
import com.localvalu.tablebooking.dto.TableBooking;
import com.localvalu.tablebooking.dto.TableBookingCancelRequest;
import com.localvalu.tablebooking.dto.TableBookingCancelResponse;
import com.localvalu.tablebooking.dto.TableBookingCancelResult;
import com.localvalu.tablebooking.dto.TableBookingListRequest;
import com.localvalu.tablebooking.dto.TableBookingListResponse;
import com.localvalu.tablebooking.dto.TableBookingListResult;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AlertDialogCallBack;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.Constants;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;
import com.utility.VerticalSpacingItemDecoration;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TableBookingListFragment extends BaseFragment
{
    private static final String TAG = "TBListFragment";
    private FragmentTableBookingListBinding binding;
    private LinearLayoutManager layoutManager;
    private TableBookingListAdapter tableBookingListAdapter;
    private boolean canShowCancel;
    private int status;
    private int cancelledStatus;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private int verticalSpace=10;
    private int cancelPosition;
    private List<TableBooking> data;

    // TODO: Rename and change types and number of parameters
    public static TableBookingListFragment newInstance(boolean canShowCancel,int status)
    {
        Bundle bundle = new Bundle();
        bundle.putBoolean(AppUtils.BUNDLE_TABLE_BOOKING_SHOW_CANCEL,canShowCancel);
        bundle.putInt(AppUtils.BUNDLE_TABLE_BOOKING_STATUS,status);
        TableBookingListFragment tableBookingListFragment = new TableBookingListFragment();
        tableBookingListFragment.setArguments(bundle);
        return tableBookingListFragment;
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        buildObjectForHandlingAPI();
        readFromBundle();
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        verticalSpace = (int) getResources().getDimension(R.dimen._15sdp);
        cancelledStatus = Integer.parseInt(Constants.BOOKING_CANCELLED);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentTableBookingListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setProperties();
        callTableBookingListAPI();
    }

    public void invokeTableBookingListApi()
    {
        callTableBookingListAPI();
    }

    private void readFromBundle()
    {
        if(getArguments()!=null)
        {
            canShowCancel = getArguments().getBoolean(AppUtils.BUNDLE_TABLE_BOOKING_SHOW_CANCEL);
            status = getArguments().getInt(AppUtils.BUNDLE_TABLE_BOOKING_STATUS);
        }
    }

    private void setProperties()
    {
        showRadioButtons();
        setAdapter();
        binding.layoutError.btnRetry.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                callTableBookingListAPI();
            }
        });
        if(Constants.BOOKING_CANCELLED.equals(Integer.toString(status)))
        {
             binding.radioButtonCancelled.postDelayed(new Runnable()
             {
                 @Override
                 public void run()
                 {
                     binding.radioButtonCancelled.setChecked(true);
                 }
             },2000);
        }
    }

    private void showRadioButtons()
    {
        String strStatus = Integer.toString(status);

        switch (strStatus)
        {
            case Constants.ACCEPT:
            case Constants.PENDING:
                binding.radGroupStatus.setVisibility(View.GONE);
                break;
            case Constants.BOOKING_CANCELLED:
            case Constants.DECLINED:
                binding.radGroupStatus.setVisibility(View.VISIBLE);
                break;
        }

        binding.radGroupStatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId)
            {
                switch (checkedId)
                {
                    case R.id.radioButtonDeclined:
                         cancelledStatus=2;
                         callTableBookingListAPI();
                         break;
                    case R.id.radioButtonCancelled:
                         cancelledStatus=5;
                         callTableBookingListAPI();
                         break;
                }
            }
        });

        Typeface typeface = ResourcesCompat.getFont(requireContext(),R.font.comfortaa_regular);
        binding.radioButtonCancelled.setTypeface(typeface);
        binding.radioButtonDeclined.setTypeface(typeface);
    }

    private void setAdapter()
    {
        layoutManager = new LinearLayoutManager(requireContext());
        binding.rvTableBooking.setLayoutManager(layoutManager);
        tableBookingListAdapter = new TableBookingListAdapter(canShowCancel);
        tableBookingListAdapter.setOnClickListener(new TableBookingListAdapter.OnItemClickListener()
        {

            @Override
            public void onItemCancel(final TableBooking tableBooking,final int position)
            {
                cancelPosition = position;
                DialogUtility.showMessageWithYesOrNoCallback(getString(R.string.lbl_confirm_cancel_msg_table_booking), getActivity(),
                        new AlertDialogCallBack()
                        {

                            @Override
                            public void onSubmit()
                            {
                                callTableBookingCancelAPI(tableBooking.getSlotId());
                            }

                            @Override
                            public void onCancel()
                            {

                            }
                        });
            }

        });
        binding.rvTableBooking.setAdapter(tableBookingListAdapter);
        binding.rvTableBooking.addItemDecoration(new VerticalSpacingItemDecoration(verticalSpace));
    }

    private TableBookingListRequest getTableBookingRequest()
    {
        int tempStatus = status;

        if(Constants.BOOKING_CANCELLED.equals(Integer.toString(status)))
        {
            tempStatus = cancelledStatus;
        }
        TableBookingListRequest tableBookingListRequest = new TableBookingListRequest();
        tableBookingListRequest.setUserId(userBasicInfo.userId);
        tableBookingListRequest.setStatus(tempStatus);
        tableBookingListRequest.setRequestFor(AppUtils.REQUEST_TYPE_TABLE_BOOKING);
        return tableBookingListRequest;
    }

    private TableBookingCancelRequest getTableBookingCancelRequest(String slotId)
    {
        TableBookingCancelRequest tableBookingCancelRequest = new TableBookingCancelRequest();
        tableBookingCancelRequest.setUserId(userBasicInfo.userId);
        tableBookingCancelRequest.setSlotId(slotId);
        return tableBookingCancelRequest;
    }

    private void callTableBookingListAPI()
    {
        showProgress();

        Call<TableBookingListResponse> dtoCall = apiInterface.getTableBookingList(getTableBookingRequest());
        dtoCall.enqueue(new Callback<TableBookingListResponse>()
        {
            @Override
            public void onResponse(Call<TableBookingListResponse> call, Response<TableBookingListResponse> response)
            {
                //mProgressDialog.dismiss();

                try
                {
                    TableBookingListResponse tableBookingListResponse = response.body();

                    if(tableBookingListResponse!=null)
                    {
                        TableBookingListResult tableBookingListResult = tableBookingListResponse.getTableBookingListResult();

                        if(tableBookingListResult!=null)
                        {
                            if(tableBookingListResult.errorDetails.errorCode == 0)
                            {
                                ArrayList<TableBooking> tableBookingList = tableBookingListResult.getTableBookingList();

                                if(tableBookingList.size()>0)
                                {
                                    setAdapter(tableBookingList);
                                    showList();
                                }
                                else
                                {
                                    showError(getString(R.string.lbl_no_bookings_found),false);
                                }
                            }
                            else
                            {
                                showError(tableBookingListResult.errorDetails.errorMessage,false);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<TableBookingListResponse> call, Throwable t)
            {
                //mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    showError(getString(R.string.network_unavailable),true);
                else
                    showError(getString(R.string.server_alert),true);
            }
        });
    }

    private void callTableBookingCancelAPI(String slotId)
    {
        mProgressDialog.show();

        Call<TableBookingCancelResponse> dtoCall = apiInterface.cancelTableBooking(getTableBookingCancelRequest(slotId));
        dtoCall.enqueue(new Callback<TableBookingCancelResponse>()
        {
            @Override
            public void onResponse(Call<TableBookingCancelResponse> call, Response<TableBookingCancelResponse> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    TableBookingCancelResponse tableBookingCancelResponse = response.body();

                    if(tableBookingCancelResponse!=null)
                    {
                        TableBookingCancelResult tableBookingCancelResult = tableBookingCancelResponse.getTableBookingCancelResult();

                        if(tableBookingCancelResult!=null)
                        {
                            if(tableBookingCancelResult.errorDetails.errorCode == 0)
                            {
                                updateInCancelTab(tableBookingCancelResult);
                            }
                            else
                            {
                                DialogUtility.showMessageWithOk(tableBookingCancelResult.errorDetails.errorMessage,getActivity());
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<TableBookingCancelResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void updateInCancelTab(TableBookingCancelResult tableBookingCancelResult)
    {
        TableBooking tableBooking = data.get(cancelPosition);
        Toast.makeText(requireContext(),tableBookingCancelResult.message,Toast.LENGTH_LONG).show();
        data.remove(cancelPosition);
        tableBookingListAdapter.submitList(data);
        tableBookingListAdapter.notifyDataSetChanged();

        TableBookingFragment frag = ((TableBookingFragment)this.getParentFragment());
        if(frag!=null)
        {
            frag.addToCancelledTab(tableBooking);
        }
    }

    private void setAdapter(final ArrayList<TableBooking> detail)
    {
        tableBookingListAdapter.submitList(detail);
        data=detail;
    }

    public void addItem(TableBooking tableBooking)
    {
        if(binding.radioButtonCancelled.isChecked())
        {
            ArrayList<TableBooking> tempData = new ArrayList<>();
            tempData.add(tableBooking);
            tempData.addAll(data);
            tableBookingListAdapter.submitList(tempData);
            tableBookingListAdapter.notifyDataSetChanged();
        }
    }

    private void showList()
    {
        binding.rvTableBooking.setVisibility(View.VISIBLE);
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showProgress()
    {
        binding.rvTableBooking.setVisibility(View.GONE);
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String message,boolean retry)
    {
        binding.rvTableBooking.setVisibility(View.GONE);
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(message);
        if(retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

}
