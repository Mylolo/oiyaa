package com.localvalu.tablebooking.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.myorder.dto.response.MyOrders;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;
import java.util.ArrayList;

public class TableBookingListResult implements Serializable
{
    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("TableBookingResults")
    @Expose
    private ArrayList<TableBooking> tableBookingList;
    @SerializedName("requestfor")
    @Expose
    private String requestFor;

    public ErrorDetailsDto getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetailsDto errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public ArrayList<TableBooking> getTableBookingList()
    {
        return tableBookingList;
    }

    public void setTableBookingList(ArrayList<TableBooking> tableBookingList)
    {
        this.tableBookingList = tableBookingList;
    }

    public String getRequestFor()
    {
        return requestFor;
    }

    public void setRequestFor(String requestFor)
    {
        this.requestFor = requestFor;
    }
}
