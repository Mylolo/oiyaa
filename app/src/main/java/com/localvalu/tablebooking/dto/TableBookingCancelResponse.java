package com.localvalu.tablebooking.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TableBookingCancelResponse implements Serializable
{
    @SerializedName("TableBookingCancelResults")
    @Expose
    private TableBookingCancelResult tableBookingCancelResult;

    public TableBookingCancelResult getTableBookingCancelResult()
    {
        return tableBookingCancelResult;
    }

    public void setTableBookingCancelResult(TableBookingCancelResult tableBookingCancelResult)
    {
        this.tableBookingCancelResult = tableBookingCancelResult;
    }
}
