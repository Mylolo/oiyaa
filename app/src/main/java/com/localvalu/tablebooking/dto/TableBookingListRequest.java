package com.localvalu.tablebooking.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/* 0 - Accept
   5 - Booking cancelled
   3 - waiting for Confirm
*/
public class TableBookingListRequest implements Serializable
{
    @SerializedName("Token")
    @Expose
    private String token="/3+YFd5QZdSK9EKsB8+TlA==";

    @SerializedName("Userid")
    @Expose
    private String userId;

    @SerializedName("Status")
    @Expose
    private int status;

    @SerializedName("requestfor")
    @Expose
    private String requestFor;

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public String getRequestFor()
    {
        return requestFor;
    }

    public void setRequestFor(String requestFor)
    {
        this.requestFor = requestFor;
    }

}
