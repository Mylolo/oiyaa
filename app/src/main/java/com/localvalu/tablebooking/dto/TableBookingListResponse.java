package com.localvalu.tablebooking.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.myorder.dto.response.MyOrdersResult;

import java.io.Serializable;

public class TableBookingListResponse implements Serializable
{
    @SerializedName("TableBookingResults")
    @Expose
    private TableBookingListResult tableBookingListResult;

    public TableBookingListResult getTableBookingListResult()
    {
        return tableBookingListResult;
    }

    public void setTableBookingListResult(TableBookingListResult tableBookingListResult)
    {
        this.tableBookingListResult = tableBookingListResult;
    }
}
