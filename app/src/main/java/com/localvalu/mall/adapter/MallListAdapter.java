package com.localvalu.mall.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.localvalu.R;
import com.localvalu.base.MainActivity;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.eats.mycart.CartFragment;
import com.localvalu.mall.ConstantsMall;
import com.localvalu.mall.MallListingFragment;
import com.localvalu.mall.dto.GetMallVoucherAmountRequest;
import com.localvalu.mall.dto.GetMallVoucherAmountResult;
import com.localvalu.mall.dto.GetMallVoucherAmountResultResponse;
import com.localvalu.mall.dto.MallUserVoucherDetailsList;
import com.localvalu.mall.dto.MallVoucherInsert;
import com.localvalu.mall.dto.MallVoucherInsertUpdateResult;
import com.localvalu.mall.dto.MallVoucherInsertUpdateResultResponse;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiInterface;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MallListAdapter extends RecyclerView.Adapter<MallListAdapter.ViewHolder>
{
    public static final String TAG = MallListAdapter.class.getSimpleName();

    private LinearLayout ll_mall;
    private ImageView image;
    private Context context;
    private OnItemClickListener onClickListener;
    List<MallUserVoucherDetailsList> mallUserVoucherDetailsLists;
    private ProgressDialog mProgressDialog;
    double totalPayment = 0;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private MallListItemClickListener mallListItemClickListener;
    private UserBasicInfo userBasicInfo;
    private String merchantId;
    private ApiInterface apiInterface;

    public MallListAdapter(Context context)
    {
        mallUserVoucherDetailsLists = ConstantsMall.mallUserVoucherDetailsList;
        strCurrencySymbol=context.getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter=context.getString(R.string.lbl_currency_letter_euro);
        this.context = context;
        callGetMallVoucherAmount();
    }

    public void setMallListItemClickListener(MallListItemClickListener mallListItemClickListener)
    {
        this.mallListItemClickListener = mallListItemClickListener;
    }

    public void setMerchantId(String merchantId)
    {
        this.merchantId = merchantId;
    }

    public void setUserBasicInfo(String strUserBasicInfo)
    {
        this.userBasicInfo = userBasicInfo;
    }

    public void setApiInterface(ApiInterface apiInterface)
    {
        this.apiInterface = apiInterface;
    }

    @NonNull
    @Override
    public MallListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_mall_list, viewGroup, false);
        init();
        return new MallListAdapter.ViewHolder(view);
    }

    private void init()
    {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

    }

    private void callGetMallVoucherAmount()
    {
        GetMallVoucherAmountRequest getMallVoucherAmountRequest = new GetMallVoucherAmountRequest();
        getMallVoucherAmountRequest.setAccountId(userBasicInfo.accountId);
        getMallVoucherAmountRequest.setMallRetailerId(merchantId);
        getMallVoucherAmountRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        Call<GetMallVoucherAmountResult> getMallVoucherAmount = apiInterface.getMallVoucherAmount(getMallVoucherAmountRequest);
        getMallVoucherAmount.enqueue(new Callback<GetMallVoucherAmountResult>()
        {
            @Override
            public void onResponse(Call<GetMallVoucherAmountResult> call, Response<GetMallVoucherAmountResult> response)
            {
                try
                {
                    GetMallVoucherAmountResult getMallVoucherAmountResult = response.body();
                    GetMallVoucherAmountResultResponse getMallVoucherAmountResultResponse = getMallVoucherAmountResult.getGetMallVoucherAmountResult();
                    if (getMallVoucherAmountResultResponse.getStatus().equalsIgnoreCase("true"))
                    {
                        totalPayment = getMallVoucherAmountResultResponse.getTotalAmount();
                        //mallListingFragment.tvTotalPay.setText(strCurrencySymbol + totalPayment);
                    }
                    else
                    {
                        //DialogUtility.showMessageWithOk(getMallVoucherAmountResultResponse.getMessage(),activity);
                    }

                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<GetMallVoucherAmountResult> call, Throwable t)
            {

            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull final MallListAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i)
    {
        try
        {
            final MallUserVoucherDetailsList mallUserVoucherDetailsList = mallUserVoucherDetailsLists.get(i);
            Picasso.with(context).load(R.drawable.app_logo ).placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo ).into(viewHolder.image);
            viewHolder.denomination_value.setText(strCurrencyLetter + mallUserVoucherDetailsList.getDenominationValue());
            viewHolder.youpay.setText(strCurrencySymbol+"0");
            double getValue = mallUserVoucherDetailsList.getDenominationValue();
            double currentDiscount = mallUserVoucherDetailsList.getCurrentDiscountLT();
            final double discount = getValue / 100 * currentDiscount;
            final double discountPrice = getValue - discount;
            final long[] availableVoucher = {mallUserVoucherDetailsList.getCurrentAvailableVoucher()};
            final long[] getquantity = {mallUserVoucherDetailsList.getQuantity()};
            viewHolder.quantity.setText("" + getquantity[0]);
            viewHolder.paynow.setVisibility(View.GONE);
            viewHolder.voucher_layout.setVisibility(View.GONE);
            viewHolder.imageshown_layout.setVisibility(View.GONE);
            viewHolder.terms_button.setVisibility(View.GONE);
            viewHolder.discount.setText(mallUserVoucherDetailsList.getCurrentDiscountLT() + "%");
            viewHolder.currentdiscount_lt.setText(strCurrencyLetter + discount);
            Log.e(TAG, "discount : " + discount + " discountPrice : " + discountPrice);
            viewHolder.tvMinus.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (getquantity[0] > 0)
                    {
                        availableVoucher[0] = availableVoucher[0] + 1;
                        getquantity[0] = getquantity[0] - 1;
                        long setQuantity = getquantity[0];
                        double setprice = discountPrice * setQuantity;
                        totalPayment = totalPayment - discountPrice;
                        viewHolder.quantity.setText("" + setQuantity);
                        viewHolder.youpay.setText(strCurrencySymbol + setprice);
                        callInsertAPI(mallUserVoucherDetailsList, getquantity[0]);
                    }
                    else
                    {
                        //Toast.makeText(activity, "Sold out", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            viewHolder.tvPlus.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if (availableVoucher[0] > 0)
                    {
                        availableVoucher[0] = availableVoucher[0] - 1;
                        getquantity[0] = getquantity[0] + 1;
                        long setQuantity = getquantity[0];
                        double setprice = discountPrice * setQuantity;
                        totalPayment = totalPayment + discountPrice;
                        viewHolder.quantity.setText("" + setQuantity);
                        viewHolder.youpay.setText(strCurrencySymbol + setprice);
                        callInsertAPI(mallUserVoucherDetailsList, getquantity[0]);
                    }
                    else
                    {
                        Toast.makeText(context, "Sold out", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            viewHolder.paynow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    CartFragment cartFragment = CartFragment.newInstance(new BusinessDetailInfo());
                    ((MainActivity) context).addFragment(cartFragment, true, CartFragment.class.getName(), true);
                }
            });

        }
        catch (Exception e)
        {
            Log.e(TAG, "Exception : " + e.getMessage());
        }


        //start
        //Comment by Balasubramaniam
        /*if (i == 0) {
            viewHolder.image.setImageResource(R.drawable.sample_mall_logo1);
        } else if (i == 1) {
            viewHolder.image.setImageResource(R.drawable.sample_mall_logo2);
        } else if (i == 2) {
            viewHolder.image.setImageResource(R.drawable.sample_mall_logo3);
        } else if (i == 3) {
            viewHolder.image.setImageResource(R.drawable.sample_mall_logo4);
        } else if (i == 4) {
            viewHolder.image.setImageResource(R.drawable.sample_mall_logo5);
        } else if (i == 5) {
            viewHolder.image.setImageResource(R.drawable.sample_mall_logo6);
        } else if (i == 6) {
            viewHolder.image.setImageResource(R.drawable.sample_mall_logo7);
        } else if (i == 7) {
            viewHolder.image.setImageResource(R.drawable.sample_mall_logo8);
        } else if (i == 8) {
            viewHolder.image.setImageResource(R.drawable.sample_mall_logo9);
        }*/
        //End
        viewHolder.ll_mall.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemClick(i);
            }
        });
    }

    private void callInsertAPI(MallUserVoucherDetailsList mallUserVoucherDetailsList, long getquantity)
    {
        mProgressDialog.show();
        MallVoucherInsert mallVoucherInsert = new MallVoucherInsert();
        mallVoucherInsert.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        mallVoucherInsert.setMallRetailerId("" + mallUserVoucherDetailsList.getMallRetailerId());
        mallVoucherInsert.setUserAccountId(userBasicInfo.accountId);
        mallVoucherInsert.setVoucherDenominationId("" + mallUserVoucherDetailsList.getVoucherDemoninationId());
        mallUserVoucherDetailsList.setVoucherDemoninationId(mallUserVoucherDetailsList.getVoucherDemoninationId());
        mallVoucherInsert.setQuantity(getquantity);
        Call<MallVoucherInsertUpdateResultResponse> getMallVoucherInsert = apiInterface.getMallVoucherInsert(mallVoucherInsert);
        getMallVoucherInsert.enqueue(new Callback<MallVoucherInsertUpdateResultResponse>()
        {
            @Override
            public void onResponse(Call<MallVoucherInsertUpdateResultResponse> call, Response<MallVoucherInsertUpdateResultResponse> response)
            {
                mProgressDialog.dismiss();
                try
                {
                    MallVoucherInsertUpdateResultResponse mallVoucherInsertUpdateResultResponse = response.body();
                    MallVoucherInsertUpdateResult mallVoucherInsertUpdateResult = mallVoucherInsertUpdateResultResponse.getMallVoucherInsertUpdateResult();
                    if (mallVoucherInsertUpdateResult.getStatus().equalsIgnoreCase("true"))
                    {
                        //mallListingFragment.tvTotalPay.setText(strCurrencySymbol + totalPayment);
                        mallListItemClickListener.onItemUpdated(strCurrencySymbol+totalPayment);
                    }
                    else
                    {
                        Toast.makeText(context, "" + mallVoucherInsertUpdateResult.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "Exception : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<MallVoucherInsertUpdateResultResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return mallUserVoucherDetailsLists.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout ll_mall = itemView.findViewById(R.id.ll_mall);
        LinearLayout voucher_layout = itemView.findViewById(R.id.voucher_layout);
        RelativeLayout imageshown_layout = itemView.findViewById(R.id.imageshown_layout);
        ImageView image = itemView.findViewById(R.id.image);
        TextView denomination_value = itemView.findViewById(R.id.denomination_value);
        TextView currentdiscount_lt = itemView.findViewById(R.id.currentdiscount_lt);
        TextView youpay = itemView.findViewById(R.id.youpay);
        TextView quantity = itemView.findViewById(R.id.quantity);
        TextView discount = itemView.findViewById(R.id.totalpay);
        Button paynow = itemView.findViewById(R.id.paynow);
        Button terms_button = itemView.findViewById(R.id.terms_button);
        AppCompatTextView tvMinus = itemView.findViewById(R.id.tvMinus);
        AppCompatTextView tvPlus = itemView.findViewById(R.id.tvPlus);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }
}
