package com.localvalu.mall.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.mall.dto.MallMerchantSearchList;
import com.squareup.picasso.Picasso;

import java.util.List;


public class MallGridAdapter extends RecyclerView.Adapter<MallGridAdapter.ViewHolder>
{
    public static final String TAG = MallGridAdapter.class.getSimpleName();
    private ImageView image;
    private Activity activity;
    private OnItemClickListener onClickListener;
    List<MallMerchantSearchList> mallMerchantSearchList;

    public MallGridAdapter(Activity activity, List<MallMerchantSearchList> mallMerchantSearchList)
    {
        Log.d(TAG,"MallGridAdapter");
        this.activity = activity;
        this.mallMerchantSearchList = mallMerchantSearchList;
    }

    @NonNull
    @Override
    public MallGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        Log.d(TAG,"onCreateViewHolder");
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_mall_grid, viewGroup, false);
        return new MallGridAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MallGridAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i)
    {
        Log.d(TAG,"onBindViewHolder");
        try
        {
            MallMerchantSearchList getmallMerchantSearchList = mallMerchantSearchList.get(i);
            String businessLogo = "https://admin.localvalu.com/" + getmallMerchantSearchList.getBusinessLogo();
            Picasso.with(activity).load(businessLogo).placeholder(R.drawable.progress_animation)
                    .error(R.drawable.mall_detail_sample ).into(viewHolder.image);
            viewHolder.trading_name.setText(getmallMerchantSearchList.getTradingName());
            viewHolder.city.setText(getmallMerchantSearchList.getCity());
            viewHolder.tv_phone_number.setText(getmallMerchantSearchList.getContactPhone());
            viewHolder.address.setText(getmallMerchantSearchList.getAddress1());
            viewHolder.address2.setText(getmallMerchantSearchList.getAddress2());
            viewHolder.tv_promotion_mall_grid.setText(getmallMerchantSearchList.getPromotionPercentage()
                    .replace(".00", "") + "%");
        }
        catch (Exception e)
        {
            Log.e(TAG, "Exception : " + e.getMessage());
        }

        viewHolder.image.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemClick(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return mallMerchantSearchList.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        AppCompatImageView image = itemView.findViewById(R.id.iv_mall_grid);
        AppCompatTextView tv_promotion_mall_grid = itemView.findViewById(R.id.tv_promotion_mall_grid);
        AppCompatTextView trading_name = itemView.findViewById(R.id.trading_name);
        AppCompatTextView city = itemView.findViewById(R.id.city);
        AppCompatTextView tv_phone_number = itemView.findViewById(R.id.tv_phone_number);
        AppCompatTextView address = itemView.findViewById(R.id.address);
        AppCompatTextView address2 = itemView.findViewById(R.id.address2);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }
}
