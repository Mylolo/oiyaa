package com.localvalu.mall;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.button.MaterialButton;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.base.MainActivity;
import com.localvalu.databinding.FragmentMallListBinding;
import com.localvalu.mall.adapter.MallGridAdapter;
import com.localvalu.mall.dto.MallMerchantSearch;
import com.localvalu.mall.dto.MallMerchantSearchList;
import com.localvalu.mall.dto.MallMerchantSearchResult;
import com.localvalu.mall.dto.MallMerchantSearchResultResponse;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.PreferenceUtility;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MallDashboardFragment extends BaseFragment
{
    private MaterialButton btnActionContactMe;
    private int colorMall;

    private FragmentMallListBinding binding;
    private GridLayoutManager gridLayoutManager;
    private MallGridAdapter mallGridAdapter;
    private ApiInterface apiMallInterface;
    private String TAG = "MallDashboardFragment";
    private String merchantId = "";
    private UserBasicInfo userBasicInfo;
    private HostActivityListener hostActivityListener;


    // TODO: Rename and change types and number of parameters
    public static MallDashboardFragment newInstance()
    {
        return new MallDashboardFragment();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        apiMallInterface = ApiClient.getApiClientMall().create(ApiInterface.class);
        colorMall = ContextCompat.getColor(requireContext(),R.color.colorMall);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentMallListBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        setUpActionBar();
        setProperties();
        fetchApi();
    }

    private void setUpActionBar()
    {
        binding.toolbarLayout.tvTitle.setText(getString(R.string.title_tab_mall));
        binding.toolbarLayout.toolbar.getNavigationIcon().setColorFilter(colorMall, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onMenuClicked();
            }
        });
    }

    private void setProperties()
    {
        binding.tvName.setVisibility(View.GONE);
        binding.bottomSheet.setVisibility(View.GONE);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(requireContext(),2);
        binding.rvLists.setLayoutManager(gridLayoutManager);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.btnRetry:
                     fetchApi();
                     break;
            }
        }
    };

    private void fetchApi()
    {
        showProgress();
        MallMerchantSearch mallMerchantSearch = new MallMerchantSearch();
        mallMerchantSearch.setAccountId(userBasicInfo.accountId);
        mallMerchantSearch.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        mallMerchantSearch.setMerchantName("");
        Call<MallMerchantSearchResultResponse> getMallMerchantSearch = apiMallInterface.getMallMerchantSearch(mallMerchantSearch);
        getMallMerchantSearch.enqueue(new Callback<MallMerchantSearchResultResponse>()
        {
            @Override
            public void onResponse(Call<MallMerchantSearchResultResponse> call, Response<MallMerchantSearchResultResponse> response)
            {
                try
                {
                    MallMerchantSearchResultResponse mallMerchantSearchResultResponse = response.body();
                    final MallMerchantSearchResult mallMerchantSearchResult = mallMerchantSearchResultResponse.getMallVoucherInsertUpdateResult();

                    if (mallMerchantSearchResult.getErrorDetails().getErrorCode()==0)
                    {
                        showList();
                        List<MallMerchantSearchList> mallMerchantSearchList = mallMerchantSearchResult.getMallMerchantSearchList();
                        mallGridAdapter = new MallGridAdapter(getActivity(), mallMerchantSearchList);
                        mallGridAdapter.setOnClickListener(new MallGridAdapter.OnItemClickListener()
                        {
                            @Override
                            public void onItemClick(int position)
                            {
                                String strLMRID = Long.toString(mallMerchantSearchList.get(position).getLMRIDAutoPK());
                                Bundle bundle = new Bundle();
                                bundle.putString("LMRID",strLMRID);
                                bundle.putString("TradingName",mallMerchantSearchList.get(position).getTradingName());
                                bundle.putString("PromotionPercentage",mallMerchantSearchList.get(position).getPromotionPercentage());

                                MallListingFragment mallListingFragment = new MallListingFragment();
                                mallListingFragment.setArguments(bundle);
                                hostActivityListener.updateFragment(mallListingFragment,
                                        true,MallListingFragment.class.getName(),true);
                            }
                        });
                        binding.rvLists.setAdapter(mallGridAdapter);
                    }
                    else
                    {
                        showError(mallMerchantSearchResult.getErrorDetails().getErrorMessage(),false);
                    }
                }
                catch (Exception e)
                {
                    Log.d(TAG, " Exception : " + e.getMessage());
                    if(e instanceof IOException) showError(getString(R.string.network_unavailable),true);
                    else showError(getString(R.string.server_alert),false);
                }
            }

            @Override
            public void onFailure(Call<MallMerchantSearchResultResponse> call, Throwable t)
            {
                Log.d(TAG, " onFailure : " + t.getMessage());
                if(t instanceof IOException) showError(getString(R.string.network_unavailable),true);
                else showError(getString(R.string.server_alert),false);
            }
        });

    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.scrollView.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showList()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.scrollView.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage,boolean retry)
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.scrollView.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if(retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

}
