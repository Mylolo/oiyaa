package com.localvalu.mall;

import android.os.Bundle;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.MainActivity;
import com.localvalu.mall.adapter.MallListAdapter;
import com.utility.AppUtils;

import java.util.Objects;

public class MallBonusFragment extends BaseFragment
{

    private RecyclerView rv_lists;
    private GridLayoutManager gridLayoutManager;
    private MallListAdapter mallListAdapter;

    // TODO: Rename and change types and number of parameters
    public static MallBonusFragment newInstance()
    {
        return new MallBonusFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mall_bonus, container, false);
        initView();
        return view;
    }

    private void initView()
    {
        ((MainActivity) Objects.requireNonNull(getActivity())).setHeaderWithBackIconButton(AppUtils.BMT_MALL);
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                onBackPressed();
                break;
        }
    }

}
