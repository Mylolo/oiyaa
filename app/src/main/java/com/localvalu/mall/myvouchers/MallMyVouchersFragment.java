package com.localvalu.mall.myvouchers;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.MainActivity;
import com.localvalu.base.HostActivityListener;
import com.localvalu.mall.MallMyVoucherDetailFragment;
import com.localvalu.mall.myvouchers.adapter.MallMyVoucherListAdapter;
import com.localvalu.mall.myvouchers.dto.MallRetailersList;
import com.localvalu.mall.myvouchers.dto.MallRetailersListResponse;
import com.localvalu.mall.myvouchers.dto.MallRetailersListResult;
import com.localvalu.mall.myvouchers.dto.MallRetailersVoucherDetailsResponse;
import com.localvalu.mall.myvouchers.dto.MallRetailersVoucherDetailsResult;
import com.localvalu.mall.myvouchers.dto.MallRetailersVoucherList;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.CommonUtilities;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.DropDownViewForXML;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MallMyVouchersFragment extends BaseFragment
{
    private static final String TAG = "MallMyVouchersFragment";

    //Toolbar
    private Toolbar toolbar;
    private TextView tvTitle;
    private int colorMall;

    public View view;
    private TextView from_date, to_date;
    private ArrayList<MallRetailersList> merchantArray;
    private DropDownViewForXML dd_retailer;
    private RecyclerView rv_lists;
    private GridLayoutManager gridLayoutManager;
    private MallMyVoucherListAdapter mallMyVoucherListAdapter;
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private String retailerID = "0";
    private int selectedDayOfMonth;
    private int selectedMonth;
    private int selectedYear;

    private HostActivityListener hostActivityListener;

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientMall().create(ApiInterface.class);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    // TODO: Rename and change types and number of parameters
    public static MallMyVouchersFragment newInstance()
    {
        return new MallMyVouchersFragment();
    }

    public MallMyVouchersFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        colorMall = ContextCompat.getColor(requireContext(),R.color.colorMall);
        setTodayDateAsSelected();
    }

    public void setTodayDateAsSelected()
    {
        Calendar calendar = Calendar.getInstance();
        selectedDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        selectedMonth = calendar.get(Calendar.MONTH);
        selectedYear = calendar.get(Calendar.YEAR);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_mall_my_vouchers, container, false);
        buildObjectForHandlingAPI();
        initView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
        fetchAPI();
    }

    private void initView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        rv_lists = view.findViewById(R.id.rv_lists);
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        rv_lists.setLayoutManager(gridLayoutManager);
        from_date = view.findViewById(R.id.from_date);
        to_date = view.findViewById(R.id.to_date);
        dd_retailer = view.findViewById(R.id.dd_merchant);

        to_date.setText(DateUtility.getLocalDateTime());
        from_date.setText(DateUtility.getPastLocalDateTime());
    }

    private void setUpActionBar()
    {
        toolbar = view.findViewById(R.id.toolbar);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setTextColor(colorMall);
        toolbar.getNavigationIcon().setColorFilter(colorMall, PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private void setProperties()
    {
        dd_retailer.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                System.out.println("Id :::: " + merchantArray.get(position).getMallRetailerId());
                retailerID = String.valueOf(merchantArray.get(position).getMallRetailerId());
                callAPI(retailerID);
            }
        });

        from_date.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    DatePickerDialog fromPickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
                    {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
                        {
                            from_date.setText(year + "-" + pad(month + 1) + "-" + pad(dayOfMonth));
                        }
                    },selectedYear,selectedMonth,selectedDayOfMonth);
                    fromPickerDialog.getDatePicker().setMaxDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(to_date.getText().toString()).getTime());
                    fromPickerDialog.show();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        to_date.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    DatePickerDialog toDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
                    {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth)
                        {
                            to_date.setText(year + "-" + pad(month + 1) + "-" + pad(dayOfMonth));
                        }
                    },selectedYear,selectedMonth,selectedDayOfMonth);
                    toDatePickerDialog.getDatePicker().setMinDate(new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).parse(from_date.getText().toString()).getTime());
                    toDatePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                    toDatePickerDialog.show();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        to_date.addTextChangedListener(new TextWatcher()
        {

            public void afterTextChanged(Editable s)
            {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                callAPI(retailerID);
            }
        });

        from_date.addTextChangedListener(new TextWatcher()
        {

            public void afterTextChanged(Editable s)
            {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                callAPI(retailerID);
            }
        });
    }

    private void setAdapter(final List<MallRetailersVoucherList> mallRetailersVoucherLists)
    {
        final List<MallRetailersVoucherList> mallRetailersVoucherList = new ArrayList<>();
        for (int i = 0; i < mallRetailersVoucherLists.size(); i++)
        {
            if (mallRetailersVoucherLists.get(i).getSoldToAccountId().toString().equalsIgnoreCase(userBasicInfo.accountId))
            {
                mallRetailersVoucherList.add(mallRetailersVoucherLists.get(i));
            }
        }
        mallMyVoucherListAdapter = new MallMyVoucherListAdapter(getActivity(), mallRetailersVoucherList);
        mallMyVoucherListAdapter.setOnClickListener(new MallMyVoucherListAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClick(int position)
            {
                Bundle bundle = new Bundle();
                String id = String.valueOf(mallRetailersVoucherList.get(position).getMallRetailerId());
                bundle.putString("id", id);
                MallMyVoucherDetailFragment mallMyVoucherDetailFragment = MallMyVoucherDetailFragment.newInstance(mallRetailersVoucherList.get(position));
                mallMyVoucherDetailFragment.setArguments(bundle);
                ((MainActivity) getActivity()).addFragment(mallMyVoucherDetailFragment, true, MallMyVoucherDetailFragment.class.getName(), true);
            }
        });
        rv_lists.setAdapter(mallMyVoucherListAdapter);
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        // unbind the view to free some memory
    }

    private void fetchAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<MallRetailersListResponse> getMallRetailersList = apiInterface.getMallRetailersList(body);
        getMallRetailersList.enqueue(new Callback<MallRetailersListResponse>()
        {
            @Override
            public void onResponse(Call<MallRetailersListResponse> call, Response<MallRetailersListResponse> response)
            {
                mProgressDialog.dismiss();
                MallRetailersListResponse mallRetailersListResponse = response.body();
                MallRetailersListResult mallRetailersListResult = mallRetailersListResponse.getMallRetailersListResult();
                if (mallRetailersListResult.getErrorDetails().getErrorCode() == 0)
                {
                    if (mallRetailersListResult.getStatus().equalsIgnoreCase("true"))
                    {
                        merchantArray = mallRetailersListResult.getMallRetailersList();
                        dd_retailer.setMallRetailersListItems(merchantArray);
                        if (merchantArray != null && !merchantArray.isEmpty())
                        {
                            dd_retailer.setSelectedPosition(0);
                            retailerID = merchantArray.get(0).getMallRetailerId().toString();
                            callAPI(merchantArray.get(0).getMallRetailerId().toString());
                        }
                    }
                }
                else
                {
                    DialogUtility.showMessageWithOk(mallRetailersListResult.getErrorDetails().getErrorMessage(), getActivity());
                }
            }

            @Override
            public void onFailure(Call<MallRetailersListResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void callAPI(String retailerID)
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("MallRetailerId", retailerID);
            objMain.put("FromDate", from_date.getText().toString().trim());
            objMain.put("ToDate", to_date.getText().toString().trim());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<MallRetailersVoucherDetailsResponse> getMallRetailersVoucherDetails = apiInterface.getMallRetailersVoucherDetails(body);
        getMallRetailersVoucherDetails.enqueue(new Callback<MallRetailersVoucherDetailsResponse>()
        {
            @Override
            public void onResponse(Call<MallRetailersVoucherDetailsResponse> call, Response<MallRetailersVoucherDetailsResponse> response)
            {
                try
                {
                    mProgressDialog.dismiss();
                    MallRetailersVoucherDetailsResponse mallRetailersVoucherDetailsResponse = response.body();
                    MallRetailersVoucherDetailsResult mallRetailersVoucherDetailsResult = mallRetailersVoucherDetailsResponse.getMallRetailersVoucherDetailsResult();
                    if (mallRetailersVoucherDetailsResult.getStatus().equalsIgnoreCase("true"))
                    {
                        if (mallRetailersVoucherDetailsResult.getErrorDetails().getErrorCode() == 0)
                        {
                            setAdapter(mallRetailersVoucherDetailsResult.getMallRetailersVoucherList());
                        }
                    }
                    else
                    {
                        List<MallRetailersVoucherList> mallRetailersVoucherLists = new ArrayList<>();
                        mallMyVoucherListAdapter = new MallMyVoucherListAdapter(getActivity(), mallRetailersVoucherLists);
                        rv_lists.setAdapter(mallMyVoucherListAdapter);
                        DialogUtility.showMessageWithOk(mallRetailersVoucherDetailsResult.getErrorDetails().getErrorMessage(), getActivity());
                    }
                }
                catch (Exception e)
                {
                    Log.d(TAG, e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<MallRetailersVoucherDetailsResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());
                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private String pad(int data)
    {
        return data <= 9 ? ("0" + data) : (data + "");
    }

}
