package com.localvalu.mall.myvouchers.dto;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallRetailersListResult {

    @SerializedName("ErrorDetails")
    @Expose
    private ErrorDetails errorDetails;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("MallRetailersList")
    @Expose
    private ArrayList<MallRetailersList> mallRetailersList = null;
    @SerializedName("message")
    @Expose
    private String message;

    public ErrorDetails getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<MallRetailersList> getMallRetailersList() {
        return mallRetailersList;
    }

    public void setMallRetailersList(ArrayList<MallRetailersList> mallRetailersList) {
        this.mallRetailersList = mallRetailersList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}