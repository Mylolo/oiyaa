package com.localvalu.mall.myvouchers.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallRetailersVoucherDetailSearch {

    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("MallRetailerId")
    @Expose
    private Long mallRetailerId;
    @SerializedName("FromDate")
    @Expose
    private String fromDate;
    @SerializedName("ToDate")
    @Expose
    private String toDate;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getMallRetailerId() {
        return mallRetailerId;
    }

    public void setMallRetailerId(Long mallRetailerId) {
        this.mallRetailerId = mallRetailerId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

}