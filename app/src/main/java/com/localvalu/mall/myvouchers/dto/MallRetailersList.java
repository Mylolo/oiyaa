package com.localvalu.mall.myvouchers.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MallRetailersList  implements Serializable {

    @SerializedName("MallRetailerId")
    @Expose
    private Long mallRetailerId;
    @SerializedName("TradingName")
    @Expose
    private String tradingName;

    public Long getMallRetailerId() {
        return mallRetailerId;
    }

    public void setMallRetailerId(Long mallRetailerId) {
        this.mallRetailerId = mallRetailerId;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

}