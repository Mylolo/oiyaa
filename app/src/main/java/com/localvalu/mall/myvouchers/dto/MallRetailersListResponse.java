package com.localvalu.mall.myvouchers.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallRetailersListResponse {

    @SerializedName("MallRetailersListResult")
    @Expose
    private MallRetailersListResult mallRetailersListResult;

    public MallRetailersListResult getMallRetailersListResult() {
        return mallRetailersListResult;
    }

    public void setMallRetailersListResult(MallRetailersListResult mallRetailersListResult) {
        this.mallRetailersListResult = mallRetailersListResult;
    }
}


