package com.localvalu.mall.myvouchers.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.mall.myvouchers.dto.MallRetailersVoucherList;
import com.localvalu.user.dto.UserBasicInfo;
import com.utility.PreferenceUtility;

import java.util.List;

public class MallMyVoucherListAdapter extends RecyclerView.Adapter<MallMyVoucherListAdapter.ViewHolder>
{

    public static final String TAG = MallMyVoucherListAdapter.class.getSimpleName();
    private Activity activity;
    private OnItemClickListener onClickListener;
    List<MallRetailersVoucherList> mallRetailersVoucherLists;
    public UserBasicInfo userBasicInfo;
    private String strCurrencySymbol;
    private String strCurrencyLetter;

    public MallMyVoucherListAdapter(Activity activity, List<MallRetailersVoucherList> mallRetailersVoucherLists)
    {
        this.activity = activity;
        this.mallRetailersVoucherLists = mallRetailersVoucherLists;
        strCurrencySymbol = activity.getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = activity.getString(R.string.lbl_currency_letter_euro);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(activity, PreferenceUtility.APP_PREFERENCE_NAME);
    }

    @NonNull
    @Override
    public MallMyVoucherListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_mall_transaction_history_list, viewGroup, false);
        return new MallMyVoucherListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MallMyVoucherListAdapter.ViewHolder viewHolder, final int i)
    {
        MallRetailersVoucherList mallRetailersVoucherList = mallRetailersVoucherLists.get(i);
        viewHolder.tv_date.setText(mallRetailersVoucherList.getTradingName().trim());
        viewHolder.tv_price.setText(strCurrencySymbol + mallRetailersVoucherList.getDenominationValue());
        viewHolder.tv_view.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemClick(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return mallRetailersVoucherLists.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_date = itemView.findViewById(R.id.tv_date);
        TextView tv_price = itemView.findViewById(R.id.tv_price);
        TextView tv_view = itemView.findViewById(R.id.tv_view);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }

}
