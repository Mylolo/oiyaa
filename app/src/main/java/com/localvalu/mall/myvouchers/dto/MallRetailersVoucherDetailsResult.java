package com.localvalu.mall.myvouchers.dto;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallRetailersVoucherDetailsResult {

    @SerializedName("ErrorDetails")
    @Expose
    private ErrorDetails errorDetails;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("MallRetailersVoucherList")
    @Expose
    private List<MallRetailersVoucherList> mallRetailersVoucherList = null;
    @SerializedName("message")
    @Expose
    private String message;

    public ErrorDetails getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MallRetailersVoucherList> getMallRetailersVoucherList() {
        return mallRetailersVoucherList;
    }

    public void setMallRetailersVoucherList(List<MallRetailersVoucherList> mallRetailersVoucherList) {
        this.mallRetailersVoucherList = mallRetailersVoucherList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}