package com.localvalu.mall.myvouchers.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ErrorDetails {

    @SerializedName("ErrorCode")
    @Expose
    private Long errorCode;
    @SerializedName("ErrorMessage")
    @Expose
    private String errorMessage;

    public Long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Long errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
