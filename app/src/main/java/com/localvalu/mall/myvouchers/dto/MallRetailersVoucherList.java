package com.localvalu.mall.myvouchers.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallRetailersVoucherList {

    @SerializedName("MallRetailerId")
    @Expose
    private Long mallRetailerId;
    @SerializedName("TradingName")
    @Expose
    private String tradingName;
    @SerializedName("DenominationValue")
    @Expose
    private Long denominationValue;
    @SerializedName("VoucherStatus")
    @Expose
    private String voucherStatus;
    @SerializedName("voucherCode")
    @Expose
    private String voucherCode;
    @SerializedName("voucherExpiry_date")
    @Expose
    private String voucherExpiryDate;
    @SerializedName("SoldToAccountId")
    @Expose
    private Long soldToAccountId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;

    public Long getMallRetailerId() {
        return mallRetailerId;
    }

    public void setMallRetailerId(Long mallRetailerId) {
        this.mallRetailerId = mallRetailerId;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public Long getDenominationValue() {
        return denominationValue;
    }

    public void setDenominationValue(Long denominationValue) {
        this.denominationValue = denominationValue;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherExpiryDate() {
        return voucherExpiryDate;
    }

    public void setVoucherExpiryDate(String voucherExpiryDate) {
        this.voucherExpiryDate = voucherExpiryDate;
    }

    public Long getSoldToAccountId() {
        return soldToAccountId;
    }

    public void setSoldToAccountId(Long soldToAccountId) {
        this.soldToAccountId = soldToAccountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

}