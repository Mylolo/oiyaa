package com.localvalu.mall.myvouchers.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallRetailersVoucherDetailsResponse {

    @SerializedName("MallRetailersVoucherDetailsResult")
    @Expose
    private MallRetailersVoucherDetailsResult mallRetailersVoucherDetailsResult;

    public MallRetailersVoucherDetailsResult getMallRetailersVoucherDetailsResult() {
        return mallRetailersVoucherDetailsResult;
    }

    public void setMallRetailersVoucherDetailsResult(MallRetailersVoucherDetailsResult mallRetailersVoucherDetailsResult) {
        this.mallRetailersVoucherDetailsResult = mallRetailersVoucherDetailsResult;
    }

}
