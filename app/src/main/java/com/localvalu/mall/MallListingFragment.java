package com.localvalu.mall;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentMallListBinding;
import com.localvalu.mall.adapter.MallListAdapter;
import com.localvalu.mall.adapter.MallListItemClickListener;
import com.localvalu.mall.dto.MallMerchantSearch;
import com.localvalu.mall.dto.MallUserVoucherDetails;
import com.localvalu.mall.dto.MallUserVoucherDetailsResult;
import com.localvalu.mall.payment.online.OnlinePaymentActivityMall;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.PreferenceUtility;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MallListingFragment extends BaseFragment implements MallListItemClickListener
{
    private static final String TAG = "MallListingFragment";

    //Toolbar
    private int colorMall;

    private FragmentMallListBinding binding;
    private GridLayoutManager gridLayoutManager;
    private MallListAdapter mallListAdapter;
    private ApiInterface apiInterface;
    private String tradingName = "", promotionPercentage = "";
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private ApiInterface apiMallInterface;
    private String strLMRID,strTradingName,strPromotionPercentage;
    public ApiInterface mallApiInterface;
    public UserBasicInfo userBasicInfo;
    private HostActivityListener hostActivityListener;


    public MallListingFragment()
    {
        super();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        mallApiInterface = ApiClient.getApiClientMall().create(ApiInterface.class);
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        colorMall = ContextCompat.getColor(requireContext(), R.color.colorMall);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        readFromBundle();
    }

    private void readFromBundle()
    {
        Bundle bundle = getArguments();

        if(bundle!=null)
        {
            strLMRID = bundle.getString("LMRID","");
            strTradingName = bundle.getString("TradingName");
            strPromotionPercentage = bundle.getString("PromotionPercentage");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentMallListBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        initView();
        getDetails();
        setUpActionBar();
    }

    private void initView()
    {
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
    }

    private void setUpActionBar()
    {
        binding.toolbarLayout.tvTitle.setTextColor(colorMall);
        binding.toolbarLayout.toolbar.getNavigationIcon().setColorFilter(colorMall, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private void setAdapter()
    {
        mallListAdapter = new MallListAdapter(getActivity());
        mallListAdapter.setOnClickListener(new MallListAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClick(int position)
            {
                /*Bundle bundle = new Bundle();
                bundle.putString("merchantId", merchantId);
                MallBonusFragment mallBonusFragment = MallBonusFragment.newInstance();
                mallBonusFragment.setArguments(bundle);
                ((MainActivity) getActivity()).addFragment(mallBonusFragment, true, MallBonusFragment.class.getName(), true);
                */
            }
        });
        binding.rvLists.setAdapter(mallListAdapter);
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                 onBackPressed();
                 break;
            case R.id.paynow:
                 callPaymentApi();
                 break;
        }
    }

    private void callPaymentApi()
    {
        //Intent intent = new Intent(getActivity(), MallOnlinePaymentActivity.class);
        Intent intent = new Intent(getActivity(), OnlinePaymentActivityMall.class);
        intent.putExtra("merchantId", strLMRID);
        startActivityForResult(intent, 101);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.btnRetry:
                     break;
            }
        }
    };

    private void getDetails()
    {
        showProgress();
        MallMerchantSearch mallMerchantSearch = new MallMerchantSearch();
        mallMerchantSearch.setAccountId(userBasicInfo.accountId);
        mallMerchantSearch.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        mallMerchantSearch.setMerchantID("" + strLMRID);
        Call<MallUserVoucherDetails> getMallUserVoucherDetails = mallApiInterface.getMallUserVoucherDetails(mallMerchantSearch);
        getMallUserVoucherDetails.enqueue(new Callback<MallUserVoucherDetails>()
        {
            @Override
            public void onResponse(Call<MallUserVoucherDetails> call, Response<MallUserVoucherDetails> response)
            {
                try
                {
                    MallUserVoucherDetails mallUserVoucherDetails = response.body();
                    MallUserVoucherDetailsResult result = mallUserVoucherDetails.getMallUserVoucherDetailsResult();
                    if (result.getErrorDetails().getErrorCode() == 0)
                    {
                        showContents();
                        ConstantsMall.mallUserVoucherDetails = mallUserVoucherDetails;
                        ConstantsMall.mallUserVoucherDetailsList = result.getMallUserVoucherDetailsList();
                        binding.tvName.setText(strTradingName);
                        if(promotionPercentage.equals("")) promotionPercentage = "0.0";
                        binding.tvTotalDiscount.setText(promotionPercentage + "%");
                        binding.tvTotalPay.setText(strCurrencySymbol + "0.0");
                        if(result.getMallUserVoucherDetailsList().size()>0)
                        {
                            showList();
                            setAdapter();
                        }
                        else
                        {
                            String strEmptyMessage = result.getMessage();
                            showEmpty(strEmptyMessage);
                        }
                    }
                    else
                    {
                        showError(result.getErrorDetails().getErrorMessage(),false);
                    }
                }
                catch (Exception e)
                {
                    Log.e(TAG, "" + e.getMessage());
                    showEmpty(getString(R.string.server_alert));
                }
            }

            @Override
            public void onFailure(Call<MallUserVoucherDetails> call, Throwable t)
            {
                if(t instanceof IOException) showError(getString(R.string.network_unavailable),true);
                else showError(getString(R.string.server_alert),false);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);
        Log.e("requestCode,requestCode", "" + requestCode);
        if (requestCode == 101)
        {
            Log.e("requestCoderequestCode", "sucess: " + requestCode);
            getActivity().onBackPressed();
        }
    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.scrollView.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showContents()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.scrollView.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage,boolean retry)
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.scrollView.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if(retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

    private void showEmpty(String strMessage)
    {
        binding.tvEmptyText.setText(strMessage);
        binding.tvEmptyText.setVisibility(View.VISIBLE);
        binding.rvLists.setVisibility(View.GONE);
    }

    private void showList()
    {
        binding.tvEmptyText.setText("");
        binding.tvEmptyText.setVisibility(View.GONE);
        binding.rvLists.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemUpdated(String amount)
    {
        binding.tvTotalPay.setText(amount);
    }
}
