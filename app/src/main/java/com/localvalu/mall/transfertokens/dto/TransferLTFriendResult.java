package com.localvalu.mall.transfertokens.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TransferLTFriendResult implements Serializable {

    @SerializedName("NewBalance")
    @Expose
    public Integer newBalance;
}
