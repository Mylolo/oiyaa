package com.localvalu.mall.transfertokens;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.directory.transfertokens.DirectoryTransferTokensFragment;
import com.localvalu.common.mytransaction.wrapper.UserDetailResultWrapper;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.CommonUtilities;
import com.utility.CommonUtility;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MallTransferTokensFragment extends BaseFragment
{
    private static final String TAG = "MallTransferTokensFragm";

    //Toolbar
    private Toolbar toolbar;
    private TextView tvTitle;
    private int colorEats;

    private TextView tv_current_bal, tv_transfer_amnt, tv_bal_after_transfer;
    private EditText et_cust_unic_code;
    private TextView tvMinus, tvPlus;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;

    private String current_bal, transfer_amnt, available_bal;

    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private HostActivityListener hostActivityListener;

    // TODO: Rename and change types and number of parameters
    public static DirectoryTransferTokensFragment newInstance()
    {
        return new DirectoryTransferTokensFragment();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_mall_transfertokens, container, false);
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        buildObjectForHandlingAPI();
        initView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        fetchAPI();
    }

    private void initView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        tv_current_bal = view.findViewById(R.id.tv_current_bal);
        tv_transfer_amnt = view.findViewById(R.id.tv_transfer_amnt);
        tv_bal_after_transfer = view.findViewById(R.id.tv_bal_after_transfer);
        et_cust_unic_code = view.findViewById(R.id.et_cust_unic_code);
        et_cust_unic_code.setText("IDL");
        fetchAPI();
    }

    private void setUpActionBar()
    {
        toolbar = view.findViewById(R.id.toolbar);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setTextColor(colorEats);
        toolbar.getNavigationIcon().setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    private void fetchAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "CustomerBalanceEnquiry");
            objMain.put("AccountId", userBasicInfo.accountId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                        if (response.body().userDetailResult.status.equalsIgnoreCase("true"))
                        {
                            current_bal = String.valueOf(response.body().userDetailResult.customerBalanceEnquiryData.currentTokenBalance);
                            transfer_amnt = "1";
                            available_bal = String.valueOf(Integer.valueOf(response.body().userDetailResult.customerBalanceEnquiryData.currentTokenBalance) - 1);

                            tv_current_bal.setText(strCurrencyLetter + current_bal);
                            tv_transfer_amnt.setText("1");
                            tv_bal_after_transfer.setText(strCurrencyLetter + available_bal);
                        }
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void callAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "TransferLTFriends");
            objMain.put("AccountId", userBasicInfo.accountId.toString().trim());
            objMain.put("CurrentBalance", current_bal);
            objMain.put("TransferAmount", transfer_amnt);
            objMain.put("BalanceAfterTransfer", available_bal);
            objMain.put("LTtoAccountID", et_cust_unic_code.getText().toString().trim());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                        if (response.body().userDetailResult.status.equalsIgnoreCase("true"))
                        {
                            Toast.makeText(getActivity(), response.body().userDetailResult.msg, Toast.LENGTH_LONG).show();
                            CommonUtilities.alertSound(getActivity(), R.raw.coin_drops);

                            current_bal = String.valueOf(response.body().userDetailResult.transferLTFriendResults.newBalance);
                            transfer_amnt = "1";
                            available_bal = String.valueOf(Double.parseDouble(response.body().userDetailResult.transferLTFriendResults.newBalance )- 1);

                            tv_current_bal.setText(strCurrencyLetter + current_bal);
                            tv_transfer_amnt.setText("1");
                            tv_bal_after_transfer.setText(strCurrencyLetter + available_bal);

                            et_cust_unic_code.setText("IDL");
                        }
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk("Invalid  AccountID Or Customer Unique Code", getActivity());
//                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
            case R.id.cnst_root:
                CommonUtility.hideKeyboard(getActivity());
                break;
            case R.id.ll_root:
                CommonUtility.hideKeyboard(getActivity());
                break;
            case R.id.btn_send:
                callAPI();
                break;
            case R.id.btn_cancel:
                et_cust_unic_code.setText("IDL");
                break;
            case R.id.tvMinus:
                if (Integer.parseInt(transfer_amnt) > 1)
                {
                    transfer_amnt = String.valueOf(Integer.parseInt(transfer_amnt) - 1);
                    available_bal = String.valueOf(Integer.parseInt(current_bal) - Integer.parseInt(transfer_amnt));
                }
                tv_current_bal.setText(strCurrencyLetter + current_bal);
                tv_transfer_amnt.setText(transfer_amnt);
                tv_bal_after_transfer.setText(strCurrencyLetter + available_bal);
                break;
            case R.id.tvPlus:
                if (Integer.parseInt(transfer_amnt) < Integer.parseInt(current_bal))
                {
                    transfer_amnt = String.valueOf(Integer.parseInt(transfer_amnt) + 1);
                    available_bal = String.valueOf(Integer.parseInt(current_bal) - Integer.parseInt(transfer_amnt));
                }
                tv_current_bal.setText(strCurrencyLetter + current_bal);
                tv_transfer_amnt.setText(transfer_amnt);
                tv_bal_after_transfer.setText(strCurrencyLetter + available_bal);
                break;
        }
    }

}
