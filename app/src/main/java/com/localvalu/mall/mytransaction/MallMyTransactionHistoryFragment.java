package com.localvalu.mall.mytransaction;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.localvalu.R;
import com.localvalu.base.MainActivity;
import com.localvalu.common.mytransaction.dto.MerchantListDto;
import com.localvalu.common.mytransaction.dto.TransactionHistoryListDto;
import com.localvalu.common.mytransaction.wrapper.UserDetailResultWrapper;
import com.localvalu.mall.mytransaction.adapter.MallTransactionHistoryListAdapter;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.CommonUtilities;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.DropDownViewForXML;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MallMyTransactionHistoryFragment extends Fragment
{
    private static final String TAG = "MallMyTransactionHistoryFragment";

    public View view;
    private TextView from_date, to_date;
    private ArrayList<MerchantListDto> merchantArray;
    private DropDownViewForXML dd_merchant;
    private RecyclerView rv_lists;
    private GridLayoutManager gridLayoutManager;
    private MallTransactionHistoryListAdapter mallTransactionHistoryListAdapter;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private int selectedDayOfMonth;
    private int selectedMonth;
    private int selectedYear;
    private String fromDate, toDate;


    private String merchant = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setTodayDateAsSelected();
    }

    public void setTodayDateAsSelected()
    {
        Calendar calendar = Calendar.getInstance();
        selectedDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        selectedMonth = calendar.get(Calendar.MONTH);
        selectedYear = calendar.get(Calendar.YEAR);
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        merchantArray = new ArrayList<>();
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_mall_my_transaction_history, container, false);
        buildObjectForHandlingAPI();
        setView();
        return view;
    }

    private void setView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);

        rv_lists = view.findViewById(R.id.rv_lists);
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        rv_lists.setLayoutManager(gridLayoutManager);
        from_date = view.findViewById(R.id.from_date);
        to_date = view.findViewById(R.id.to_date);
        dd_merchant = view.findViewById(R.id.dd_merchant);

        toDate = DateUtility.getLocalDateTime();
        fromDate = DateUtility.getPastLocalDateTime();

        to_date.setText(convertDateStringInUKFormat(toDate));
        from_date.setText(convertDateStringInUKFormat(fromDate));


        dd_merchant.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                System.out.println("Id :::: " + merchantArray.get(position).merchantTradingName);
                merchant = String.valueOf(merchantArray.get(position).retailerID);
                callAPI(merchant);
            }
        });

        fetchAPI();

        from_date.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    showFromDatePicker();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        to_date.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    showToDatePicker();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        to_date.addTextChangedListener(new TextWatcher()
        {

            public void afterTextChanged(Editable s)
            {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                callAPI(merchant);
            }
        });

        from_date.addTextChangedListener(new TextWatcher()
        {

            public void afterTextChanged(Editable s)
            {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                callAPI(merchant);
            }
        });
    }

    private void showFromDatePicker()
    {
        Calendar calendarMaxDate = Calendar.getInstance();
        calendarMaxDate.setTime(getDateFromString(toDate));

        Calendar calendarDate = Calendar.getInstance();
        calendarDate.setTime(getDateFromString(fromDate));

        int day = calendarDate.get(Calendar.DAY_OF_MONTH);
        int month = calendarDate.get(Calendar.MONTH);
        final int year = calendarDate.get(Calendar.YEAR);

        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        MaterialDatePicker picker = builder.build();
        picker.show(requireFragmentManager(),picker.toString());
        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener()
        {
            @Override
            public void onPositiveButtonClick(Object selection)
            {
                long milliseconds = (long) selection;
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTimeInMillis(milliseconds);
                fromDate = convertToStringInServerFormat(selectedDate.getTime());
                from_date.setText(convertDateToStringInUKFormat(selectedDate.getTime()));
            }
        });
    }

    private void showToDatePicker()
    {
        Calendar calendarMinDate = Calendar.getInstance();
        calendarMinDate.setTime(getDateFromString(fromDate));

        Calendar calendarMaxDate = Calendar.getInstance();

        Calendar calendarDate = Calendar.getInstance();
        calendarDate.setTime(getDateFromString(toDate));

        int day = calendarDate.get(Calendar.DAY_OF_MONTH);
        int month = calendarDate.get(Calendar.MONTH);
        final int year = calendarDate.get(Calendar.YEAR);

        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        MaterialDatePicker picker = builder.build();
        picker.show(requireFragmentManager(),picker.toString());
        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener()
        {
            @Override
            public void onPositiveButtonClick(Object selection)
            {
                long milliseconds = (long) selection;
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTimeInMillis(milliseconds);
                toDate = convertToStringInServerFormat(selectedDate.getTime());
                to_date.setText(convertDateToStringInUKFormat(selectedDate.getTime()));
            }
        });
    }

    private void setAdapter(final ArrayList<TransactionHistoryListDto> transactionHistoryList)
    {
        mallTransactionHistoryListAdapter = new MallTransactionHistoryListAdapter(getActivity(), transactionHistoryList);
        mallTransactionHistoryListAdapter.setOnClickListener(new MallTransactionHistoryListAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClick(int position)
            {
                Bundle bundle = new Bundle();
                String id = String.valueOf(transactionHistoryList.get(position).sirTransID);
                bundle.putString("id", id);
                MallTransactionDetailFragment mallTransactionDetailFragment = MallTransactionDetailFragment.newInstance();
                mallTransactionDetailFragment.setArguments(bundle);
                ((MainActivity) getActivity()).addFragment(mallTransactionDetailFragment, true, MallTransactionDetailFragment.class.getName(), true);
            }
        });
        rv_lists.setAdapter(mallTransactionHistoryListAdapter);
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    private void fetchAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "MerchantList");
            objMain.put("AccountId", userBasicInfo.accountId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                        if (response.body().userDetailResult.status.equalsIgnoreCase("true"))
                        {
                            if(merchantArray!=null)
                            {
                                merchantArray.clear();
                                merchantArray.add(0, MerchantListDto.all());
                                merchantArray.addAll(response.body().userDetailResult.merchantList);
                                dd_merchant.setItems(merchantArray);

                                if (merchantArray != null && !merchantArray.isEmpty())
                                {
                                    dd_merchant.setSelectedPosition(0);
                                    merchant = merchantArray.get(0).retailerID.toString();
                                    callAPI(merchant);
                                }
                            }
                        }
                    }
                    else
                    {
//                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void callAPI(String merchant)
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "TransactionHistory");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("MerchantId", merchant);
            objMain.put("Fromdate", fromDate);
            objMain.put("Todate", toDate);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                        if (response.body().userDetailResult.status.equalsIgnoreCase("true"))
                        {
                            setAdapter(response.body().userDetailResult.transactionHistoryList);
                        }
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());
                        setAdapter(new ArrayList<TransactionHistoryListDto>());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());

                setAdapter(new ArrayList<TransactionHistoryListDto>());
            }
        });
    }

    private String pad(int data)
    {
        return data <= 9 ? ("0" + data) : (data + "");
    }

    private String convertDateToStringInUKFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/YYYY");
        return format.format(date);
    }

    private String convertDateStringInUKFormat(String strDate)
    {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date date = inputFormat.parse(strDate);
            return convertDateToStringInUKFormat(date);
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private Date getDateFromString(String strDate)
    {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date date = inputFormat.parse(strDate);
            return date;
        }
        catch (Exception ex)
        {
            return new Date();
        }
    }

    private String convertToStringInServerFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

}
