package com.localvalu.mall.mytransaction;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.localvalu.R;
import com.localvalu.common.mytransaction.TransactionDashboardFragment;
import com.localvalu.common.mytransaction.wrapper.UserDetailResultWrapper;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.squareup.picasso.Picasso;
import com.utility.CommonUtilities;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MallMyTransactionDashboardFragment extends Fragment
{
    private static final String TAG = TransactionDashboardFragment.class.getSimpleName();
    public View view;
    private TextView from_date, to_date;

    private CircleImageView iv_profile;
    private TextView tv_name, tv_profile;
    private UserBasicInfo userBasicInfo;
    private String fromDate,toDate;

    private TextView tv_transactions, tv_tokens_received, tv_tokens_earned, tv_ref_tokens_earned, tv_review_tokens_earned, tv_current_bal, tv_bonus_token_earned, tv_cash_save;

    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private String strCurrencySymbol;
    private String strCurrencyLetter;

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_mall_my_transaction_dashboard, container, false);
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        buildObjectForHandlingAPI();
        setView();
        return view;
    }

    private void setView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);

        from_date = view.findViewById(R.id.from_date);
        to_date = view.findViewById(R.id.to_date);

        iv_profile = view.findViewById(R.id.iv_profile);
        tv_name = view.findViewById(R.id.tv_name);
        tv_profile = view.findViewById(R.id.tv_profile);

        tv_transactions = view.findViewById(R.id.tv_transactions);
        tv_tokens_received = view.findViewById(R.id.tv_tokens_received);
        tv_tokens_earned = view.findViewById(R.id.tv_tokens_earned);
        tv_ref_tokens_earned = view.findViewById(R.id.tv_ref_tokens_earned);
        tv_review_tokens_earned = view.findViewById(R.id.tv_review_tokens_earned);
        tv_current_bal = view.findViewById(R.id.tv_current_bal);
        tv_bonus_token_earned = view.findViewById(R.id.tv_bonus_token);
        tv_cash_save = view.findViewById(R.id.tv_cash_save);

        if (userBasicInfo.image != null)
        {
            if (!userBasicInfo.image.trim().equals(""))
            {
                Picasso.with(getActivity()).load(userBasicInfo.image)
                        .placeholder(R.drawable.progress_animation).error(R.drawable.noimg_profile).into(iv_profile);
            }
            else
            {
                iv_profile.setImageResource(R.drawable.noimg_profile);
            }
        }
        else
        {
            iv_profile.setImageResource(R.drawable.noimg_profile);
        }
        tv_profile.setText(userBasicInfo.qRCodeOutput);
        tv_name.setText(userBasicInfo.name);

        toDate = DateUtility.getLocalDateTime();
        fromDate = DateUtility.getPastLocalDateTime();

        to_date.setText(convertDateStringInUKFormat(toDate));
        from_date.setText(convertDateStringInUKFormat(fromDate));

        callAPI();

        from_date.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    showFromDatePicker();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        to_date.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    showToDatePicker();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        to_date.addTextChangedListener(new TextWatcher()
        {

            public void afterTextChanged(Editable s)
            {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                callAPI();
            }
        });

        from_date.addTextChangedListener(new TextWatcher()
        {

            public void afterTextChanged(Editable s)
            {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                callAPI();
            }
        });

    }

    private void showFromDatePicker()
    {
        Calendar calendarMaxDate = Calendar.getInstance();
        calendarMaxDate.setTime(getDateFromString(toDate));

        Calendar calendarDate = Calendar.getInstance();
        calendarDate.setTime(getDateFromString(fromDate));

        int day = calendarDate.get(Calendar.DAY_OF_MONTH);
        int month = calendarDate.get(Calendar.MONTH);
        final int year = calendarDate.get(Calendar.YEAR);

        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        MaterialDatePicker picker = builder.build();
        picker.show(requireFragmentManager(),picker.toString());
        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener()
        {
            @Override
            public void onPositiveButtonClick(Object selection)
            {
                long milliseconds = (long) selection;
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTimeInMillis(milliseconds);
                fromDate = convertToStringInServerFormat(selectedDate.getTime());
                from_date.setText(convertDateToStringInUKFormat(selectedDate.getTime()));
            }
        });
    }

    private void showToDatePicker()
    {
        Calendar calendarMinDate = Calendar.getInstance();
        calendarMinDate.setTime(getDateFromString(fromDate));

        Calendar calendarMaxDate = Calendar.getInstance();

        Calendar calendarDate = Calendar.getInstance();
        calendarDate.setTime(getDateFromString(toDate));

        int day = calendarDate.get(Calendar.DAY_OF_MONTH);
        int month = calendarDate.get(Calendar.MONTH);
        final int year = calendarDate.get(Calendar.YEAR);

        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        MaterialDatePicker picker = builder.build();
        picker.show(requireFragmentManager(),picker.toString());
        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener()
        {
            @Override
            public void onPositiveButtonClick(Object selection)
            {
                long milliseconds = (long) selection;
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTimeInMillis(milliseconds);
                toDate = convertToStringInServerFormat(selectedDate.getTime());
                to_date.setText(convertDateToStringInUKFormat(selectedDate.getTime()));
            }
        });
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    private void callAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "TransactionDashboard");
            objMain.put("Todate", toDate);
            objMain.put("Fromdate", fromDate);
            objMain.put("AccountId", userBasicInfo.accountId);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                    /*    PhotosAdapter photosAdapter = new PhotosAdapter(response.body().photoDtos, getActivity());
                        rv_photos.setAdapter(photosAdapter);*/

                        tv_transactions.setText("" + response.body().userDetailResult.dashBoardAPIResult.totalTransactions);
                        tv_tokens_received.setText("" + response.body().userDetailResult.dashBoardAPIResult.totalRedeemed);
                        tv_tokens_earned.setText("" + response.body().userDetailResult.dashBoardAPIResult.totalEarned);
                        tv_ref_tokens_earned.setText("" + response.body().userDetailResult.dashBoardAPIResult.referralTokenearned);
                        tv_review_tokens_earned.setText("" + response.body().userDetailResult.dashBoardAPIResult.reviewTokenearned);
                        tv_current_bal.setText("" + response.body().userDetailResult.dashBoardAPIResult.currentBalanceToken);
                        tv_bonus_token_earned.setText("" + response.body().userDetailResult.dashBoardAPIResult.bonusToken);
                        tv_cash_save.setText(strCurrencySymbol + response.body().userDetailResult.dashBoardAPIResult.cashSavedOiyaa);
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());

                        tv_transactions.setText("0");
                        tv_tokens_received.setText("0");
                        tv_tokens_earned.setText("0");
                        tv_ref_tokens_earned.setText("0");
                        tv_review_tokens_earned.setText("0");
                        tv_current_bal.setText("0");
                        tv_bonus_token_earned.setText("0");
                        tv_cash_save.setText(strCurrencySymbol+"0");
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());

                tv_transactions.setText("0");
                tv_tokens_received.setText("0");
                tv_tokens_earned.setText("0");
                tv_ref_tokens_earned.setText("0");
                tv_review_tokens_earned.setText("0");
                tv_current_bal.setText("0");
                tv_bonus_token_earned.setText("0");
                tv_cash_save.setText(strCurrencySymbol+"0");
            }
        });
    }

    private String pad(int data)
    {
        return data <= 9 ? ("0" + data) : (data + "");
    }

    private String convertDateToStringInUKFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/YYYY");
        return format.format(date);
    }

    private String convertDateStringInUKFormat(String strDate)
    {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date date = inputFormat.parse(strDate);
            return convertDateToStringInUKFormat(date);
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private Date getDateFromString(String strDate)
    {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date date = inputFormat.parse(strDate);
            return date;
        }
        catch (Exception ex)
        {
            return new Date();
        }
    }

    private String convertToStringInServerFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

}
