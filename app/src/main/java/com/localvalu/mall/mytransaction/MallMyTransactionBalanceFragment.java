package com.localvalu.mall.mytransaction;


import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.common.mytransaction.dto.MerchantListDto;
import com.localvalu.common.mytransaction.wrapper.UserDetailResultWrapper;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.DropDownViewForXML;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class MallMyTransactionBalanceFragment extends Fragment
{
    private static final String TAG = "MallMyTransactionBalanc";

    public View view;
    private ArrayList<MerchantListDto> merchantArray;
    private DropDownViewForXML dd_merchant;
    private TextView tv_current_token_balance, tv_tokens_earned, tv_tokens_redeemed;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    public MallMyTransactionBalanceFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_mall_my_transaction_balance, container, false);
        buildObjectForHandlingAPI();
        setView();
        return view;

    }

    private void setView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);

        dd_merchant = (DropDownViewForXML) view.findViewById(R.id.dd_merchant);
        tv_current_token_balance = view.findViewById(R.id.tv_current_token_balance);
        tv_tokens_earned = view.findViewById(R.id.tv_tokens_earned);
        tv_tokens_redeemed = view.findViewById(R.id.tv_tokens_redeemed);

        dd_merchant.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                System.out.println("Id :::: " + merchantArray.get(position).merchantTradingName);
            }
        });
//        fetchAPI();
        callAPI();
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    private void callAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "CustomerBalanceEnquiry");
            objMain.put("AccountId", userBasicInfo.accountId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();
                if (response.body().userDetailResult.errorDetails.errorCode == 0)
                {
                    tv_current_token_balance.setText("" + response.body().userDetailResult.customerBalanceEnquiryData.currentTokenBalance);
                    tv_tokens_earned.setText("" + response.body().userDetailResult.customerBalanceEnquiryData.totalTokenRedeemed);
                    tv_tokens_redeemed.setText("" + response.body().userDetailResult.customerBalanceEnquiryData.totalTokenEarned);
                }
                else
                {
                    DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());
                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

}
