package com.localvalu.mall.payment.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnlinePayMallVoucherPurchaseResponse
{
    @SerializedName("OnlinePayMallVoucherPurchaseResult")
    @Expose
    private OnlinePayMallVoucherPurchaseResult onlinePayMallVoucherPurchaseResult;

    public OnlinePayMallVoucherPurchaseResult getOnlinePayMallVoucherPurchaseResult()
    {
        return onlinePayMallVoucherPurchaseResult;
    }

    public void setOnlinePayMallVoucherPurchaseResult(OnlinePayMallVoucherPurchaseResult onlinePayMallVoucherPurchaseResult)
    {
        this.onlinePayMallVoucherPurchaseResult = onlinePayMallVoucherPurchaseResult;
    }
}
