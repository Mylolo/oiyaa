package com.localvalu.mall.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoucherPurchaseOnlinePay {

    @SerializedName("ErrorDetails")
    @Expose
    private ErrorDetails errorDetails;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("VoucherPurchaseOnlinePayResult")
    @Expose
    private VoucherPurchaseOnlinePayResult voucherPurchaseOnlinePayResult;
    @SerializedName("msg")
    @Expose
    private String msg;

    public ErrorDetails getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public VoucherPurchaseOnlinePayResult getVoucherPurchaseOnlinePayResult() {
        return voucherPurchaseOnlinePayResult;
    }

    public void setVoucherPurchaseOnlinePayResult(VoucherPurchaseOnlinePayResult voucherPurchaseOnlinePayResult) {
        this.voucherPurchaseOnlinePayResult = voucherPurchaseOnlinePayResult;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
