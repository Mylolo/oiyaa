package com.localvalu.mall.payment.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnlinePayMallVoucherPurchase
{
    @SerializedName("Message")
    @Expose
    String message;

    @SerializedName("VoucherStatus")
    @Expose
    String voucherStatus;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getVoucherStatus()
    {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus)
    {
        this.voucherStatus = voucherStatus;
    }
}
