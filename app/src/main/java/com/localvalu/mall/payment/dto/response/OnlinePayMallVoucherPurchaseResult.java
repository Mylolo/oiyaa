package com.localvalu.mall.payment.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.payment.dto.ErrorDetails;

import java.io.Serializable;

public class OnlinePayMallVoucherPurchaseResult implements Serializable
{
    @SerializedName("ErrorDetails")
    @Expose
    private ErrorDetails errorDetails;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName("OnlinePayMallVoucherPurchase")
    @Expose
    private OnlinePayMallVoucherPurchase onlinePayMallVoucherPurchase;

    public ErrorDetails getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public OnlinePayMallVoucherPurchase getOnlinePayMallVoucherPurchase()
    {
        return onlinePayMallVoucherPurchase;
    }

    public void setOnlinePayMallVoucherPurchase(OnlinePayMallVoucherPurchase onlinePayMallVoucherPurchase)
    {
        this.onlinePayMallVoucherPurchase = onlinePayMallVoucherPurchase;
    }
}
