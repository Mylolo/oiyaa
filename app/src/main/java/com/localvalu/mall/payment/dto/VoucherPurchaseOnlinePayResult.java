package com.localvalu.mall.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoucherPurchaseOnlinePayResult {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("VoucherStatus")
    @Expose
    private String voucherStatus;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

}