package com.localvalu.mall.payment;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.localvalu.R;
import com.localvalu.base.MainActivity;
import com.utility.ActivityController;
import com.utility.AppUtils;

public class MallSuccessPopupForOrderSubmit extends Activity
{

    private int navigationType;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        getWindow().getDecorView().findViewById(android.R.id.content).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setFinishOnTouchOutside(false);
        setContentView(R.layout.activity_mall_success_popup_for_order_submit);
        init();
        readFromBundle();
    }


    private void init()
    {

    }

    private void readFromBundle()
    {
        navigationType =  getIntent().getExtras().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
    }

    public void onButtonClick(View v)
    {
        Bundle bundle = new Bundle();
        switch (v.getId())
        {
            case R.id.iv_close:
            case R.id.btn_go_to_home:
                 bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,AppUtils.BMT_MALL);
                 ActivityController.startNextActivity(this, MainActivity.class, bundle, true);
                 finish();
                 break;
        }
    }
}
