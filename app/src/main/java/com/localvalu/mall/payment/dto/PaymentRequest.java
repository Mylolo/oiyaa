package com.localvalu.mall.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentRequest {

    @SerializedName("paymentType")
    @Expose
    private String paymentType;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("AccountId")
    @Expose
    private String accountId;
    @SerializedName("CardNo")
    @Expose
    private String cardNo;
    @SerializedName("CardType")
    @Expose
    private String cardType;
    @SerializedName("ExpMonth")
    @Expose
    private String expMonth;
    @SerializedName("ExpYear")
    @Expose
    private String expYear;
    @SerializedName("CVV")
    @Expose
    private String cVV;
    @SerializedName("CardHolderName")
    @Expose
    private String cardHolderName;
    @SerializedName("CardHolderEmail")
    @Expose
    private String cardHolderEmail;
    @SerializedName("IpAddress")
    @Expose
    private String ipAddress;
    @SerializedName("MerchantId")
    @Expose
    private String merchantId;

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    public String getExpYear() {
        return expYear;
    }

    public void setExpYear(String expYear) {
        this.expYear = expYear;
    }

    public String getCVV() {
        return cVV;
    }

    public void setCVV(String cVV) {
        this.cVV = cVV;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getCardHolderEmail() {
        return cardHolderEmail;
    }

    public void setCardHolderEmail(String cardHolderEmail) {
        this.cardHolderEmail = cardHolderEmail;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

}