package com.localvalu.mall.payment.online;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.localvalu.R;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.eats.payment.dto.CredoraxHPPRequest;
import com.localvalu.eats.payment.dto.CredoraxHPPRequestResult;
import com.localvalu.eats.payment.dto.VoucherPurchase;
import com.localvalu.eats.payment.dto.VoucherPurchaseRequest;
import com.localvalu.eats.payment.dto.OnlinePaymentResponseOne;
import com.localvalu.eats.payment.dto.PaymentIntegrationEat;
import com.localvalu.eats.payment.dto.PlaceOrderOne;
import com.localvalu.eats.payment.dto.PlaceOrderOneResult;
import com.localvalu.eats.payment.dto.VoucherPurchaseResponse;
import com.localvalu.eats.payment.dto.VoucherPurchaseResult;
import com.localvalu.mall.payment.MallSuccessPopupForOrderSubmit;
import com.localvalu.mall.payment.dto.request.OnlinePayMallVoucherPurchaseRequest;
import com.localvalu.mall.payment.dto.response.OnlinePayMallVoucherPurchase;
import com.localvalu.mall.payment.dto.response.OnlinePayMallVoucherPurchaseResponse;
import com.localvalu.mall.payment.dto.response.OnlinePayMallVoucherPurchaseResult;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnlinePaymentActivityMall extends AppCompatActivity
{
    WebView webview;
    public ApiInterface apiInterface;
    private UserBasicInfo userBasicInfo;
    Context context;
    private ArrayList<CartDetailDto> cartDetails;
    private String businessId, special_ins, allergy_ins, delivery;
    String TAG = "OnlinePaymentActivityMall";
    private ProgressDialog mProgressDialog;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_payment_eat);
        context = this;
        activity = this;
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(this, PreferenceUtility.APP_PREFERENCE_NAME);
        readFromBundle();
        setView();
    }

    private void setView()
    {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(context, PreferenceUtility.APP_PREFERENCE_NAME);
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        webview = findViewById(R.id.webview);
        webview.clearCache(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(webViewClient);
        webview.setWebChromeClient(webChromeClient);
        //callPaymentApi();
        callOnlinePayMallVoucherHppRequest();
    }

    private void readFromBundle()
    {

        businessId = getIntent().getStringExtra("merchantId");
    }

    public PaymentIntegrationEat getPaymentIntegration(boolean isFirst)
    {
        PaymentIntegrationEat paymentIntegrationEat = new PaymentIntegrationEat();
        paymentIntegrationEat.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        paymentIntegrationEat.setAccountId(userBasicInfo.accountId);
        paymentIntegrationEat.setBusinessId(businessId);
        paymentIntegrationEat.setUserId(userBasicInfo.userId);
        paymentIntegrationEat.setDistance("12");
        paymentIntegrationEat.setPaymentType("PayByCard");
        paymentIntegrationEat.setOrderType(delivery);
        paymentIntegrationEat.setPlatforms("android");
        if (isFirst)
        {
            paymentIntegrationEat.setPaymentType("PayByCard");
            paymentIntegrationEat.setSuccessUrl("https://www.oiyaa.com/credorax/Success.php");
            paymentIntegrationEat.setFailureUrl("https://www.oiyaa.com/credorax/Error.php");
            paymentIntegrationEat.setBackUrl("https://www.oiyaa.com/credorax/Back.php");
        }
        return paymentIntegrationEat;
    }

    private void callPaymentApi()
    {

        mProgressDialog.show();


        Call<OnlinePaymentResponseOne> getPaymentIntegration = apiInterface.startOnlinePayment(getPaymentIntegration(true));
        getPaymentIntegration.enqueue(new Callback<OnlinePaymentResponseOne>()
        {
            @Override
            public void onResponse(Call<OnlinePaymentResponseOne> call, Response<OnlinePaymentResponseOne> response)
            {
                try
                {
                    mProgressDialog.dismiss();

                    try
                    {
                        OnlinePaymentResponseOne onlinePaymentResponseOne = response.body();

                        PlaceOrderOneResult placeOrderOneResult = onlinePaymentResponseOne.getPlaceOrderOneResult();


                        if (placeOrderOneResult.errorDetails.errorCode == 0)
                        {
                            //load webview url here
                            PlaceOrderOne placeOrderOne = placeOrderOneResult.placeOrderOne;
                            if (placeOrderOne != null)
                            {
                                CredoraxHPPRequestResult credoraxHPPRequestResult = placeOrderOne.credoraxHPPRequestResult;
                                CredoraxHPPRequest credoraxHPPRequest = credoraxHPPRequestResult.credoraxHPPRequest;
                                String url=credoraxHPPRequest.url;
                                url = url.replace("\\", "");
                                webview.loadUrl(url);
                            }

                            /*Bundle bundle = new Bundle();
                            bundle.putString("lisamoBonus", "" + placeOrderResultResponse.getLisamoBonus());
                            bundle.putString("bonusTokens", "" + placeOrderResultResponse.getBonusTokens());
                            bundle.putString("newTokenBalance", "" + placeOrderResultResponse.getNewTokenBalance());
                            ActivityController.startNextActivity(OnlinePaymentActivityMall.this, EatsSuccessPopupForOrderSubmit.class, bundle, false);*/
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(placeOrderOneResult.errorDetails.errorMessage, OnlinePaymentActivityMall.this);
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }
                catch (Exception e)
                {
                    mProgressDialog.dismiss();
                    Log.e(TAG, " : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<OnlinePaymentResponseOne> call, Throwable t)
            {
                mProgressDialog.dismiss();
                if (!CommonUtilities.checkConnectivity(OnlinePaymentActivityMall.this))
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), OnlinePaymentActivityMall.this);
                }
                else
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), OnlinePaymentActivityMall.this);
                }
            }
        });
    }

    WebViewClient webViewClient = new WebViewClient()
    {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            super.onPageStarted(view, url, favicon);
            Log.d(TAG, "onPageStarted: ");

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
            Log.d(TAG, "shouldOverrideUrlLoading: url-" + url);
            if (url.contains("https://www.oiyaa.com/credorax/Success.php"))
            {
                callOnlinePaymentSuccessAPI(url);
            }
            else if (url.contains("https://www.oiyaa.com/credorax/Error.php"))
            {
                callOnlinePaymentSuccessAPI(url);
            }
            else
            {
                webview.loadUrl(url);
                DialogUtility.showMessageWithOk("Payment Transaction Failure", OnlinePaymentActivityMall.this);
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url)
        {
            super.onPageFinished(view, url);
            Log.d(TAG, "onPageFinished: ");
            mProgressDialog.dismiss();
        }
    };

    WebChromeClient webChromeClient = new WebChromeClient()
    {
        @Override
        public void onProgressChanged(WebView view, int progress)
        {
            mProgressDialog.show();

            if (progress == 100)
            {
                mProgressDialog.dismiss();
            }
        }
    };

    public OnlinePayMallVoucherPurchaseRequest getOnlinePayMallVoucherPurchaseRequestPayload(String url)
    {
        OnlinePayMallVoucherPurchaseRequest request = new OnlinePayMallVoucherPurchaseRequest();
        request.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        request.setAccountId(userBasicInfo.accountId);
        request.setMerchantId(businessId);
        request.setPaymenttype("Online");
        request.setPlatforms("android");
        try
        {
            Uri uri = Uri.parse(url);
            request.setK(uri.getQueryParameter("K"));
            request.setM(uri.getQueryParameter("M"));
            request.setO(uri.getQueryParameter("O"));
            request.setT(uri.getQueryParameter("T"));
            request.setV(uri.getQueryParameter("V"));
            request.setA1(uri.getQueryParameter("a1"));
            request.setA2(uri.getQueryParameter("a2"));
            request.setA4(uri.getQueryParameter("a4"));
            request.setA6(uri.getQueryParameter("a6"));
            request.setA7(uri.getQueryParameter("a7"));
            request.setA9(uri.getQueryParameter("a9"));
            request.setB1(uri.getQueryParameter("b1"));
            request.setB2(uri.getQueryParameter("b2"));
            request.setB3(uri.getQueryParameter("b3"));
            request.setB4(uri.getQueryParameter("b4"));
            request.setC1(uri.getQueryParameter("c1"));
            request.setG1(uri.getQueryParameter("g1"));
            request.setZ1(uri.getQueryParameter("z1"));
            request.setZ13(uri.getQueryParameter("z13"));
            request.setZ14(uri.getQueryParameter("z14"));
            request.setZ2(uri.getQueryParameter("z2"));
            request.setZ3(uri.getQueryParameter("z3"));
            request.setZ33(uri.getQueryParameter("z33"));
            request.setZ34(uri.getQueryParameter("z34"));
            request.setZ39(uri.getQueryParameter("z39"));
            request.setZ4(uri.getQueryParameter("z4"));
            request.setZ41(uri.getQueryParameter("z41"));
            request.setZ6(uri.getQueryParameter("z6"));
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return request;
    }

    public void callOnlinePayMallVoucherHppRequest()
    {
        mProgressDialog.show();
        Call<VoucherPurchaseResponse> voucherPurchaseResponseCall = apiInterface.
                requestForVoucherPurchase(getVoucherPurchasePayload());
        voucherPurchaseResponseCall.enqueue(new Callback<VoucherPurchaseResponse>()
        {
            @Override
            public void onResponse(Call<VoucherPurchaseResponse> call, Response<VoucherPurchaseResponse> response)
            {
                try
                {
                    mProgressDialog.dismiss();
                    try
                    {
                        VoucherPurchaseResponse voucherPurchaseResponse = response.body();
                        VoucherPurchaseResult voucherPurchaseResult = voucherPurchaseResponse.getVoucherPurchaseResult();
                        if (voucherPurchaseResult.errorDetails.errorCode == 0)
                        {
                            //load webview url here
                            VoucherPurchase voucherPurchase = voucherPurchaseResult.getVoucherPurchase();
                            if (voucherPurchase != null)
                            {

                                webview.loadUrl(voucherPurchase.getUrl());
                            }

                            /*Bundle bundle = new Bundle();
                            bundle.putString("lisamoBonus", "" + placeOrderResultResponse.getLisamoBonus());
                            bundle.putString("bonusTokens", "" + placeOrderResultResponse.getBonusTokens());
                            bundle.putString("newTokenBalance", "" + placeOrderResultResponse.getNewTokenBalance());
                            ActivityController.startNextActivity(OnlinePaymentActivityMall.this, EatsSuccessPopupForOrderSubmit.class, bundle, false);*/
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(voucherPurchaseResult.errorDetails.errorMessage, OnlinePaymentActivityMall.this);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                catch (Exception e)
                {
                    mProgressDialog.dismiss();
                    Log.e(TAG, " : " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<VoucherPurchaseResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                if (!CommonUtilities.checkConnectivity(OnlinePaymentActivityMall.this))
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), OnlinePaymentActivityMall.this);
                }
                else
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), OnlinePaymentActivityMall.this);
                }
            }
        });

    }

    public VoucherPurchaseRequest getVoucherPurchasePayload()
    {
        VoucherPurchaseRequest voucherPurchaseRequest = new VoucherPurchaseRequest();
        voucherPurchaseRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        voucherPurchaseRequest.setAccountId(userBasicInfo.accountId);
        voucherPurchaseRequest.setMerchantId(businessId);
        voucherPurchaseRequest.setSuccessUrl("https://www.oiyaa.com/credorax/Success.php");
        voucherPurchaseRequest.setFailureUrl("https://www.oiyaa.com/credorax/Error.php");
        voucherPurchaseRequest.setBackUrl("https://www.oiyaa.com/credorax/Back.php");
        voucherPurchaseRequest.setPayableAmount("0");
        Log.d(TAG, "getVoucherPurchasePayload: " + voucherPurchaseRequest);
        return voucherPurchaseRequest;
    }

    public void callOnlinePaymentSuccessAPI(String url)
    {
        mProgressDialog.show();
        Call<OnlinePayMallVoucherPurchaseResponse> OnlinePayMallVoucherPurchaseResponseCall = apiInterface.
                finishOnlinePaymentMall(getOnlinePayMallVoucherPurchaseRequestPayload(url));
        OnlinePayMallVoucherPurchaseResponseCall.enqueue(new Callback<OnlinePayMallVoucherPurchaseResponse>()
        {
            @Override
            public void onResponse(Call<OnlinePayMallVoucherPurchaseResponse> call, Response<OnlinePayMallVoucherPurchaseResponse> response)
            {
                try
                {
                    mProgressDialog.dismiss();
                    try
                    {
                        OnlinePayMallVoucherPurchaseResponse onlinePayMallVoucherPurchaseResponse = response.body();
                        OnlinePayMallVoucherPurchaseResult onlinePayMallVoucherPurchaseResult = onlinePayMallVoucherPurchaseResponse.getOnlinePayMallVoucherPurchaseResult();
                        if (onlinePayMallVoucherPurchaseResult.getErrorDetails().getErrorCode() == 0)
                        {
                            //load webview url here
                            OnlinePayMallVoucherPurchase onlinePayMallVoucherPurchase = onlinePayMallVoucherPurchaseResult.getOnlinePayMallVoucherPurchase();

                            if (onlinePayMallVoucherPurchase != null)
                            {
                                Toast.makeText(OnlinePaymentActivityMall.this,
                                        onlinePayMallVoucherPurchase.getMessage(), Toast.LENGTH_LONG).show();
                                navigateToSuccessMallPage();
                            }
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(onlinePayMallVoucherPurchaseResult.getErrorDetails().getErrorMessage(), OnlinePaymentActivityMall.this);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                catch (Exception e)
                {
                    mProgressDialog.dismiss();
                    Log.e(TAG, " : " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<OnlinePayMallVoucherPurchaseResponse> call, Throwable t)
            {
                mProgressDialog.dismiss();
                if (!CommonUtilities.checkConnectivity(OnlinePaymentActivityMall.this))
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), OnlinePaymentActivityMall.this);
                }
                else
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), OnlinePaymentActivityMall.this);
                }
            }
        });

    }

    public static String getIpAddress(Context context)
    {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext()
                .getSystemService(WIFI_SERVICE);

        String ipAddress = intToInetAddress(wifiManager.getDhcpInfo().ipAddress).toString();

        ipAddress = ipAddress.substring(1);

        return ipAddress;
    }

    public static InetAddress intToInetAddress(int hostAddress)
    {
        byte[] addressBytes = {(byte) (0xff & hostAddress),
                (byte) (0xff & (hostAddress >> 8)),
                (byte) (0xff & (hostAddress >> 16)),
                (byte) (0xff & (hostAddress >> 24))};

        try
        {
            return InetAddress.getByAddress(addressBytes);
        }
        catch (UnknownHostException e)
        {
            throw new AssertionError();
        }
    }

    public void navigateToSuccessMallPage()
    {
        Bundle bundle = new Bundle();
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_MALL);
        ActivityController.startNextActivity(OnlinePaymentActivityMall.this, MallSuccessPopupForOrderSubmit.class, bundle, true);
        finish();
    }

}
