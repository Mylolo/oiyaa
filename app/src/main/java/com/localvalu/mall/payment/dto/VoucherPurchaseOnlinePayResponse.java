package com.localvalu.mall.payment.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoucherPurchaseOnlinePayResponse {

    @SerializedName("VoucherPurchaseOnlinePay")
    @Expose
    private VoucherPurchaseOnlinePay voucherPurchaseOnlinePay;

    public VoucherPurchaseOnlinePay getVoucherPurchaseOnlinePay() {
        return voucherPurchaseOnlinePay;
    }

    public void setVoucherPurchaseOnlinePay(VoucherPurchaseOnlinePay voucherPurchaseOnlinePay) {
        this.voucherPurchaseOnlinePay = voucherPurchaseOnlinePay;
    }

}
