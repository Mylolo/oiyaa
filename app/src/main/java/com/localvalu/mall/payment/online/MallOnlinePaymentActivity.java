package com.localvalu.mall.payment.online;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.localvalu.R;
import com.localvalu.base.MainActivity;
import com.localvalu.eats.mycart.dto.CartDetailDto;
import com.localvalu.mall.payment.dto.PaymentRequest;
import com.localvalu.mall.payment.dto.VoucherPurchaseOnlinePay;
import com.localvalu.mall.payment.dto.VoucherPurchaseOnlinePayResponse;
import com.localvalu.mall.payment.dto.VoucherPurchaseOnlinePayResult;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MallOnlinePaymentActivity extends AppCompatActivity {
    EditText cardHolderName, cardHolderEmail, cardNumber, expiresMonth, expiresYear, cvv;
    Button placeOrder, discard;
    public ApiInterface apiInterface;
    private UserBasicInfo userBasicInfo;
    Context context;
    private ArrayList<CartDetailDto> cartDetails;
    String TAG = "OnlinePaymentActivity";
    String merchantId = "";
    private ProgressDialog mProgressDialog;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mall_online_payment);
        context = this;
        activity = this;
        readFromBundle();
        setView();
    }

    private void setView() {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(context, PreferenceUtility.APP_PREFERENCE_NAME);
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        cardHolderName = (EditText) findViewById(R.id.card_holder_name);
        cardHolderEmail = (EditText) findViewById(R.id.card_holder_email);
        cardNumber = (EditText) findViewById(R.id.card_number);
        expiresMonth = (EditText) findViewById(R.id.expires_month);
        expiresYear = (EditText) findViewById(R.id.expires_year);
        cvv = (EditText) findViewById(R.id.cvv);

        placeOrder = (Button) findViewById(R.id.place_order);
        discard = (Button) findViewById(R.id.discard);

        placeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPaymentApi();
            }
        });

        discard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void readFromBundle() {
        //Bundle bundle = getIntent().getBundleExtra("data");
        merchantId = getIntent().getExtras().getString("merchantId");
    }

    private void callPaymentApi() {
        String cardHolderName = this.cardHolderName.getText().toString();
        if (cardHolderName == null || cardHolderName.equals("")) {
            Toast.makeText(getApplicationContext(), "Enter card holder name", Toast.LENGTH_SHORT).show();
            return;
        }

        String cardHolderEmail = this.cardHolderEmail.getText().toString();
        if (cardHolderEmail == null || cardHolderEmail.equals("")) {
            Toast.makeText(getApplicationContext(), "Enter card holder email", Toast.LENGTH_SHORT).show();
            return;
        }

        String cardNumber = this.cardNumber.getText().toString();
        if (cardNumber == null || cardNumber.equals("")) {
            Toast.makeText(getApplicationContext(), "Enter card number", Toast.LENGTH_SHORT).show();
            return;
        }

        String expiresMonth = this.expiresMonth.getText().toString();
        if (expiresMonth == null || expiresMonth.equals("")) {
            Toast.makeText(getApplicationContext(), "Enter month", Toast.LENGTH_SHORT).show();
            return;
        }

        if(Integer.parseInt(expiresMonth) > 12) {
            Toast.makeText(getApplicationContext(), "Enter valid month", Toast.LENGTH_SHORT).show();
            return;
        }

        String expiresYear = this.expiresYear.getText().toString();
        if (expiresYear == null || expiresYear.equals("")) {
            Toast.makeText(getApplicationContext(), "Enter year", Toast.LENGTH_SHORT).show();
            return;
        }

        String cvv = this.cvv.getText().toString();
        if (cvv == null || cvv.equals("")) {
            Toast.makeText(getApplicationContext(), "Enter cvv", Toast.LENGTH_SHORT).show();
            return;
        }

        mProgressDialog.show();
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setToken("/3+YFd5QZdSK9EKsB8+TlA==");
        paymentRequest.setAccountId(userBasicInfo.accountId);
        paymentRequest.setPaymentType("OnlinePay");
        paymentRequest.setCardNo(cardNumber);
        paymentRequest.setCardType("2");
        paymentRequest.setExpMonth(expiresMonth);
        paymentRequest.setExpYear(expiresYear);
        paymentRequest.setCVV(cvv);
        paymentRequest.setCardHolderName(cardHolderName);
        paymentRequest.setCardHolderEmail(cardHolderEmail);
        paymentRequest.setIpAddress(getIpAddress(context));
        paymentRequest.setMerchantId(merchantId);
        Call<VoucherPurchaseOnlinePayResponse> getOnlinePayMallVoucherPurchase = apiInterface.getOnlinePayMallVoucherPurchase(paymentRequest);
        getOnlinePayMallVoucherPurchase.enqueue(new Callback<VoucherPurchaseOnlinePayResponse>() {
            @Override
            public void onResponse(Call<VoucherPurchaseOnlinePayResponse> call, Response<VoucherPurchaseOnlinePayResponse> response) {
                mProgressDialog.dismiss();
                try {
                    VoucherPurchaseOnlinePayResponse voucherPurchaseOnlinePayResponse = response.body();
                    VoucherPurchaseOnlinePay voucherPurchaseOnlinePay = voucherPurchaseOnlinePayResponse.getVoucherPurchaseOnlinePay();
                    if (voucherPurchaseOnlinePay.getErrorDetails().getErrorCode() == 0) {
                        VoucherPurchaseOnlinePayResult voucherPurchaseOnlinePayResult = voucherPurchaseOnlinePay.getVoucherPurchaseOnlinePayResult();
                        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                        dialog.setMessage(voucherPurchaseOnlinePayResult.getMessage());
                        dialog.setTitle(R.string.app_name);
                        dialog.setNeutralButton(context.getResources().getString(R.string.okay), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Bundle bundle = new Bundle();
                                bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,AppUtils.BMT_MALL);
                                ActivityController.startNextActivity(MallOnlinePaymentActivity.this, MainActivity.class, bundle, true);
                                finish();
                            }
                        });
                        dialog.show();
                    } else {
                        DialogUtility.showMessageWithOk(voucherPurchaseOnlinePay.getErrorDetails().getErrorMessage(), MallOnlinePaymentActivity.this);

                    }
                } catch (Exception e) {
                    Log.e(TAG, "Exception : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<VoucherPurchaseOnlinePayResponse> call, Throwable t) {
                mProgressDialog.dismiss();
                if (!CommonUtilities.checkConnectivity(MallOnlinePaymentActivity.this)) {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), MallOnlinePaymentActivity.this);
                } else {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), MallOnlinePaymentActivity.this);
                }
            }
        });
    }


    public static String getIpAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext()
                .getSystemService(WIFI_SERVICE);

        String ipAddress = intToInetAddress(wifiManager.getDhcpInfo().ipAddress).toString();

        ipAddress = ipAddress.substring(1);

        return ipAddress;
    }

    public static InetAddress intToInetAddress(int hostAddress) {
        byte[] addressBytes = {(byte) (0xff & hostAddress),
                (byte) (0xff & (hostAddress >> 8)),
                (byte) (0xff & (hostAddress >> 16)),
                (byte) (0xff & (hostAddress >> 24))};

        try {
            return InetAddress.getByAddress(addressBytes);
        } catch (UnknownHostException e) {
            throw new AssertionError();
        }
    }
}
