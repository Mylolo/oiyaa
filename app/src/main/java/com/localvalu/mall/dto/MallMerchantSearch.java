package com.localvalu.mall.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallMerchantSearch {

    @SerializedName("AccountId")
    @Expose
    private String accountId;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("MerchantName")
    @Expose
    private String merchantName;
    @SerializedName("MerchantID")
    @Expose
    private String merchantID;

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

}