package com.localvalu.mall.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallMerchantSearchResultResponse {
    @SerializedName("MallMerchantSearchResult")
    @Expose
    private MallMerchantSearchResult mallVoucherInsertUpdateResult;

    public MallMerchantSearchResult getMallVoucherInsertUpdateResult() {
        return mallVoucherInsertUpdateResult;
    }

    public void setMallVoucherInsertUpdateResult(MallMerchantSearchResult mallVoucherInsertUpdateResult) {
        this.mallVoucherInsertUpdateResult = mallVoucherInsertUpdateResult;
    }
}
