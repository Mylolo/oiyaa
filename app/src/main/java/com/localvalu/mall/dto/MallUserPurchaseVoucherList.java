package com.localvalu.mall.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallUserPurchaseVoucherList {

    @SerializedName("DenominationValue")
    @Expose
    private Long denominationValue;
    @SerializedName("Quantity")
    @Expose
    private Long quantity;
    @SerializedName("DiscountLT")
    @Expose
    private Double discountLT;
    @SerializedName("CurrentlyAvailableVoucher")
    @Expose
    private Long currentlyAvailableVoucher;
    @SerializedName("TotalAvailableVoucher")
    @Expose
    private Long totalAvailableVoucher;
    @SerializedName("savingAmount")
    @Expose
    private Long savingAmount;
    @SerializedName("totalAmount")
    @Expose
    private Long totalAmount;
    @SerializedName("TradingName")
    @Expose
    private String tradingName;
    @SerializedName("VoucherDenominationId")
    @Expose
    private Long voucherDenominationId;

    public Long getDenominationValue() {
        return denominationValue;
    }

    public void setDenominationValue(Long denominationValue) {
        this.denominationValue = denominationValue;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getDiscountLT() {
        return discountLT;
    }

    public void setDiscountLT(Double discountLT) {
        this.discountLT = discountLT;
    }

    public Long getCurrentlyAvailableVoucher() {
        return currentlyAvailableVoucher;
    }

    public void setCurrentlyAvailableVoucher(Long currentlyAvailableVoucher) {
        this.currentlyAvailableVoucher = currentlyAvailableVoucher;
    }

    public Long getTotalAvailableVoucher() {
        return totalAvailableVoucher;
    }

    public void setTotalAvailableVoucher(Long totalAvailableVoucher) {
        this.totalAvailableVoucher = totalAvailableVoucher;
    }

    public Long getSavingAmount() {
        return savingAmount;
    }

    public void setSavingAmount(Long savingAmount) {
        this.savingAmount = savingAmount;
    }

    public Long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public Long getVoucherDenominationId() {
        return voucherDenominationId;
    }

    public void setVoucherDenominationId(Long voucherDenominationId) {
        this.voucherDenominationId = voucherDenominationId;
    }

}
