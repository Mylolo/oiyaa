package com.localvalu.mall.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetMallVoucherAmountRequest {

    @SerializedName("AccountId")
    @Expose
    private String accountId;
    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("MallRetailerId")
    @Expose
    private String mallRetailerId;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMallRetailerId() {
        return mallRetailerId;
    }

    public void setMallRetailerId(String mallRetailerId) {
        this.mallRetailerId = mallRetailerId;
    }

}
