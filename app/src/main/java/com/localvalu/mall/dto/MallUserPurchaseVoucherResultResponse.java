package com.localvalu.mall.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallUserPurchaseVoucherResultResponse {
    @SerializedName("MallUserPurchaseVoucherResult")
    @Expose
    private MallUserPurchaseVoucherResult mallVoucherInsertUpdateResult;

    public MallUserPurchaseVoucherResult getMallVoucherInsertUpdateResult() {
        return mallVoucherInsertUpdateResult;
    }

    public void setMallVoucherInsertUpdateResult(MallUserPurchaseVoucherResult mallVoucherInsertUpdateResult) {
        this.mallVoucherInsertUpdateResult = mallVoucherInsertUpdateResult;
    }
}
