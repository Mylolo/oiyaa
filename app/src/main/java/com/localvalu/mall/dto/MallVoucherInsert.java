package com.localvalu.mall.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallVoucherInsert {

    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("VoucherDenominationId")
    @Expose
    private String voucherDenominationId;
    @SerializedName("UserAccountId")
    @Expose
    private String userAccountId;
    @SerializedName("MallRetailerId")
    @Expose
    private String mallRetailerId;
    @SerializedName("Quantity")
    @Expose
    private Long quantity;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getVoucherDenominationId() {
        return voucherDenominationId;
    }

    public void setVoucherDenominationId(String voucherDenominationId) {
        this.voucherDenominationId = voucherDenominationId;
    }

    public String getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(String userAccountId) {
        this.userAccountId = userAccountId;
    }

    public String getMallRetailerId() {
        return mallRetailerId;
    }

    public void setMallRetailerId(String mallRetailerId) {
        this.mallRetailerId = mallRetailerId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

}