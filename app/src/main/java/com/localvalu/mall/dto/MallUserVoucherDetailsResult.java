package com.localvalu.mall.dto;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallUserVoucherDetailsResult {

    @SerializedName("ErrorDetails")
    @Expose
    private ErrorDetails errorDetails;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("MallUserVoucherDetailsList")
    @Expose
    private List<MallUserVoucherDetailsList> mallUserVoucherDetailsList = null;
    @SerializedName("message")
    @Expose
    private String message;

    public ErrorDetails getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MallUserVoucherDetailsList> getMallUserVoucherDetailsList() {
        return mallUserVoucherDetailsList;
    }

    public void setMallUserVoucherDetailsList(List<MallUserVoucherDetailsList> mallUserVoucherDetailsList) {
        this.mallUserVoucherDetailsList = mallUserVoucherDetailsList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}