package com.localvalu.mall.dto;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallUserPurchaseVoucherResult {

    @SerializedName("ErrorDetails")
    @Expose
    private ErrorDetails errorDetails;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("MallUserPurchaseVoucherList")
    @Expose
    private List<MallUserPurchaseVoucherList> mallUserPurchaseVoucherList = null;
    @SerializedName("message")
    @Expose
    private String message;

    public ErrorDetails getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MallUserPurchaseVoucherList> getMallUserPurchaseVoucherList() {
        return mallUserPurchaseVoucherList;
    }

    public void setMallUserPurchaseVoucherList(List<MallUserPurchaseVoucherList> mallUserPurchaseVoucherList) {
        this.mallUserPurchaseVoucherList = mallUserPurchaseVoucherList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}