package com.localvalu.mall.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallVoucherInsertUpdateResultResponse {

    @SerializedName("MallVoucherInsertUpdateResult")
    @Expose
    private MallVoucherInsertUpdateResult mallVoucherInsertUpdateResult;

    public MallVoucherInsertUpdateResult getMallVoucherInsertUpdateResult() {
        return mallVoucherInsertUpdateResult;
    }

    public void setMallVoucherInsertUpdateResult(MallVoucherInsertUpdateResult mallVoucherInsertUpdateResult) {
        this.mallVoucherInsertUpdateResult = mallVoucherInsertUpdateResult;
    }
}




