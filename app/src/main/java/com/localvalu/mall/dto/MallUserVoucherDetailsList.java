package com.localvalu.mall.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallUserVoucherDetailsList {

    @SerializedName("MallRetailerId")
    @Expose
    private Long mallRetailerId;
    @SerializedName("AccountId")
    @Expose
    private Long accountId;
    @SerializedName("VoucherDemoninationId")
    @Expose
    private Long voucherDemoninationId;
    @SerializedName("DenominationValue")
    @Expose
    private Long denominationValue;
    @SerializedName("CurrentDiscountLT")
    @Expose
    private Double currentDiscountLT;
    @SerializedName("TotalAvailableVoucher")
    @Expose
    private Long totalAvailableVoucher;
    @SerializedName("CurrentAvailableVoucher")
    @Expose
    private Long currentAvailableVoucher;
    @SerializedName("Quantity")
    @Expose
    private Long quantity;
    @SerializedName("SavingAmount")
    @Expose
    private Long savingAmount;
    @SerializedName("TotalAmount")
    @Expose
    private Long totalAmount;
    @SerializedName("TotalValue")
    @Expose
    private Long totalValue;
    @SerializedName("VoucherStatus")
    @Expose
    private String voucherStatus;

    public Long getMallRetailerId() {
        return mallRetailerId;
    }

    public void setMallRetailerId(Long mallRetailerId) {
        this.mallRetailerId = mallRetailerId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getVoucherDemoninationId() {
        return voucherDemoninationId;
    }

    public void setVoucherDemoninationId(Long voucherDemoninationId) {
        this.voucherDemoninationId = voucherDemoninationId;
    }

    public Long getDenominationValue() {
        return denominationValue;
    }

    public void setDenominationValue(Long denominationValue) {
        this.denominationValue = denominationValue;
    }

    public Double getCurrentDiscountLT() {
        return currentDiscountLT;
    }

    public void setCurrentDiscountLT(Double currentDiscountLT) {
        this.currentDiscountLT = currentDiscountLT;
    }

    public Long getTotalAvailableVoucher() {
        return totalAvailableVoucher;
    }

    public void setTotalAvailableVoucher(Long totalAvailableVoucher) {
        this.totalAvailableVoucher = totalAvailableVoucher;
    }

    public Long getCurrentAvailableVoucher() {
        return currentAvailableVoucher;
    }

    public void setCurrentAvailableVoucher(Long currentAvailableVoucher) {
        this.currentAvailableVoucher = currentAvailableVoucher;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getSavingAmount() {
        return savingAmount;
    }

    public void setSavingAmount(Long savingAmount) {
        this.savingAmount = savingAmount;
    }

    public Long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Long getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Long totalValue) {
        this.totalValue = totalValue;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

}
