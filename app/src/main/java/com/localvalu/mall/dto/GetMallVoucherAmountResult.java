package com.localvalu.mall.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetMallVoucherAmountResult {

    @SerializedName("GetMallVoucherAmountResult")
    @Expose
    private GetMallVoucherAmountResultResponse getMallVoucherAmountResult;

    public GetMallVoucherAmountResultResponse getGetMallVoucherAmountResult() {
        return getMallVoucherAmountResult;
    }

    public void setGetMallVoucherAmountResult(GetMallVoucherAmountResultResponse getMallVoucherAmountResult) {
        this.getMallVoucherAmountResult = getMallVoucherAmountResult;
    }

}

