package com.localvalu.mall.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallUserPurchaseVoucherDetails {

    @SerializedName("Token")
    @Expose
    private String token;
    @SerializedName("MerchantID")
    @Expose
    private String merchantID;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

}