package com.localvalu.mall.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallUserVoucherDetails {
    @SerializedName("MallUserVoucherDetailsResult")
    @Expose
    private MallUserVoucherDetailsResult mallUserVoucherDetailsResult;

    public MallUserVoucherDetailsResult getMallUserVoucherDetailsResult() {
        return mallUserVoucherDetailsResult;
    }

    public void setMallUserVoucherDetailsResult(MallUserVoucherDetailsResult mallUserVoucherDetailsResult) {
        this.mallUserVoucherDetailsResult = mallUserVoucherDetailsResult;
    }
}
