package com.localvalu.mall.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MallMerchantSearchList {

    @SerializedName("LMR_ID_Auto_PK")
    @Expose
    private Long lMRIDAutoPK;
    @SerializedName("TradingName")
    @Expose
    private String tradingName;
    @SerializedName("Business_Type")
    @Expose
    private String businessType;
    @SerializedName("Main_CAT_ID_FK")
    @Expose
    private Long mainCATIDFK;
    @SerializedName("Business_Account_No")
    @Expose
    private Long businessAccountNo;
    @SerializedName("NumberofOutlets")
    @Expose
    private Long numberofOutlets;
    @SerializedName("NumberofEmployees")
    @Expose
    private Long numberofEmployees;
    @SerializedName("Website")
    @Expose
    private String website;
    @SerializedName("Business_Logo")
    @Expose
    private String businessLogo;
    @SerializedName("SalePerson_Name")
    @Expose
    private String salePersonName;
    @SerializedName("SP_Comment")
    @Expose
    private String sPComment;
    @SerializedName("Retailer_Type")
    @Expose
    private String retailerType;
    @SerializedName("VAT_Reg_Number")
    @Expose
    private String vATRegNumber;
    @SerializedName("Mobile_Number")
    @Expose
    private String mobileNumber;
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("County")
    @Expose
    private String county;
    @SerializedName("PostCode")
    @Expose
    private String postCode;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("ContactEmail")
    @Expose
    private String contactEmail;
    @SerializedName("Contact_Phone")
    @Expose
    private String contactPhone;
    @SerializedName("Promotion_Percentage")
    @Expose
    private String promotionPercentage;

    public Long getLMRIDAutoPK() {
        return lMRIDAutoPK;
    }

    public void setLMRIDAutoPK(Long lMRIDAutoPK) {
        this.lMRIDAutoPK = lMRIDAutoPK;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Long getMainCATIDFK() {
        return mainCATIDFK;
    }

    public void setMainCATIDFK(Long mainCATIDFK) {
        this.mainCATIDFK = mainCATIDFK;
    }

    public Long getBusinessAccountNo() {
        return businessAccountNo;
    }

    public void setBusinessAccountNo(Long businessAccountNo) {
        this.businessAccountNo = businessAccountNo;
    }

    public Long getNumberofOutlets() {
        return numberofOutlets;
    }

    public void setNumberofOutlets(Long numberofOutlets) {
        this.numberofOutlets = numberofOutlets;
    }

    public Long getNumberofEmployees() {
        return numberofEmployees;
    }

    public void setNumberofEmployees(Long numberofEmployees) {
        this.numberofEmployees = numberofEmployees;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getBusinessLogo() {
        return businessLogo;
    }

    public void setBusinessLogo(String businessLogo) {
        this.businessLogo = businessLogo;
    }

    public String getSalePersonName() {
        return salePersonName;
    }

    public void setSalePersonName(String salePersonName) {
        this.salePersonName = salePersonName;
    }

    public String getSPComment() {
        return sPComment;
    }

    public void setSPComment(String sPComment) {
        this.sPComment = sPComment;
    }

    public String getRetailerType() {
        return retailerType;
    }

    public void setRetailerType(String retailerType) {
        this.retailerType = retailerType;
    }

    public String getVATRegNumber() {
        return vATRegNumber;
    }

    public void setVATRegNumber(String vATRegNumber) {
        this.vATRegNumber = vATRegNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getPromotionPercentage() {
        return promotionPercentage;
    }

    public void setPromotionPercentage(String promotionPercentage) {
        this.promotionPercentage = promotionPercentage;
    }

}
