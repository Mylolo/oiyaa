package com.localvalu.registration.repository

import com.localvalu.base.BaseRepository
import com.localvalu.registration.api.PassionListApi
import com.localvalu.user.dto.PassionListRequest

class PassionListRepository (private val api: PassionListApi): BaseRepository(api)
{
    suspend fun passionList(passionListRequest: PassionListRequest) = safeApiCall {
        api.fetchPassionList(passionListRequest)
    }
}