package com.localvalu.registration.repository

import com.localvalu.home.api.CuisinesCategoryListApi
import com.localvalu.base.BaseRepository
import com.localvalu.home.model.CuisinesCategoryRequest
import com.localvalu.registration.api.UserExistApi
import com.localvalu.registration.model.UserExistRequest

class UserExistRepository (private val api: UserExistApi): BaseRepository(api)
{
    suspend fun userExistOrNot(userExistRequest: UserExistRequest) = safeApiCall {
        api.userExistOrNot(userExistRequest)
    }
}