package com.localvalu.registration.repository

import com.localvalu.home.api.CuisinesCategoryListApi
import com.localvalu.base.BaseRepository
import com.localvalu.home.model.CuisinesCategoryRequest
import com.localvalu.registration.api.RegisterUserApi
import com.localvalu.registration.api.UserExistApi
import com.localvalu.registration.model.RegisterRequest
import com.localvalu.registration.model.UserExistRequest

class RegisterRepository (private val api:RegisterUserApi): BaseRepository(api)
{
    suspend fun registerUser(registerRequest: RegisterRequest) = safeApiCall {
        api.registerUser(registerRequest)
    }
}