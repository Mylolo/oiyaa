package com.localvalu.registration.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.base.Resource
import com.localvalu.common.model.SearchRequest
import com.localvalu.common.model.SearchResponse
import com.localvalu.home.repository.SearchListRepository
import com.localvalu.registration.model.UserExistRequest
import com.localvalu.registration.repository.PassionListRepository
import com.localvalu.registration.repository.UserExistRepository
import com.localvalu.user.dto.PassionListRequest
import com.localvalu.user.wrapper.PassionListResultWrapper
import com.localvalu.user.wrapper.RegisterExistsCustomerResultWrapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PassionListViewModel @Inject constructor(private val repository:PassionListRepository) : ViewModel()
{
    private val _passionListResponse: MutableLiveData<Resource<PassionListResultWrapper>> = MutableLiveData()
    val passionListResponse: LiveData<Resource<PassionListResultWrapper>>
        get() = _passionListResponse

    fun fetchPassionList(passionListRequest: PassionListRequest) = viewModelScope.launch {
        _passionListResponse.value = Resource.Loading
        _passionListResponse.value = repository.passionList(passionListRequest)
    }

}