package com.localvalu.registration.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.base.Resource
import com.localvalu.common.model.SearchRequest
import com.localvalu.common.model.SearchResponse
import com.localvalu.home.repository.SearchListRepository
import com.localvalu.registration.model.UserExistRequest
import com.localvalu.registration.repository.UserExistRepository
import com.localvalu.user.wrapper.RegisterExistsCustomerResultWrapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserExistViewModel @Inject constructor(private val repository:UserExistRepository) : ViewModel()
{
    private val _userExistResponse: MutableLiveData<Resource<RegisterExistsCustomerResultWrapper>> = MutableLiveData()
    val userExistResponse: LiveData<Resource<RegisterExistsCustomerResultWrapper>>
        get() = _userExistResponse

    fun checkUserExists(userExistRequest:UserExistRequest) = viewModelScope.launch {
        _userExistResponse.value = Resource.Loading
        _userExistResponse.value = repository.userExistOrNot(userExistRequest)
    }

}