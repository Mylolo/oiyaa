package com.localvalu.registration.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.localvalu.base.Resource
import com.localvalu.common.model.SearchRequest
import com.localvalu.common.model.SearchResponse
import com.localvalu.home.repository.SearchListRepository
import com.localvalu.registration.model.RegisterRequest
import com.localvalu.registration.model.UserExistRequest
import com.localvalu.registration.repository.RegisterRepository
import com.localvalu.registration.repository.UserExistRepository
import com.localvalu.storage.UserPreferences
import com.localvalu.user.dto.UserBasicInfo
import com.localvalu.user.wrapper.RegisterCustomerResultWrapper
import com.localvalu.user.wrapper.RegisterExistsCustomerResultWrapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterUserViewModel @Inject constructor(private val repository:RegisterRepository,
                                                private val userPreferences: UserPreferences,
                                                private val gson: Gson) : ViewModel()
{
    private val _registerUserResponse: MutableLiveData<Resource<RegisterCustomerResultWrapper>> = MutableLiveData()
    val registerUserResponse: LiveData<Resource<RegisterCustomerResultWrapper>>
        get() = _registerUserResponse

    fun registerUser(registerUserRequest: RegisterRequest) = viewModelScope.launch {
        _registerUserResponse.value = Resource.Loading
        _registerUserResponse.value = repository.registerUser(registerUserRequest)
    }

    fun registerUserInLocal(userBasicInfo: UserBasicInfo)
    {
        viewModelScope.launch {
            val userString:String = gson.toJson(userBasicInfo,UserBasicInfo::class.java)
            userPreferences.saveUserObject(userString)
        }
    }

}