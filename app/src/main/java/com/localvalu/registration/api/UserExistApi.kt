package com.localvalu.registration.api

import com.localvalu.base.BaseApi
import com.localvalu.common.model.SearchRequest
import com.localvalu.common.model.SearchResponse
import com.localvalu.registration.model.UserExistRequest
import com.localvalu.user.wrapper.RegisterExistsCustomerResultWrapper
import retrofit2.http.Body
import retrofit2.http.POST

interface UserExistApi : BaseApi
{
    @POST("UserRegistration/UserRegistrationFirstStep")
    suspend fun userExistOrNot(@Body userExistRequest: UserExistRequest): RegisterExistsCustomerResultWrapper
}