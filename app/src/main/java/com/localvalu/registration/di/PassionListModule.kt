package com.localvalu.registration.di

import android.content.Context
import com.localvalu.home.api.CuisinesCategoryListApi
import com.localvalu.base.RemoteDataSource
import com.localvalu.home.repository.CuisinesCategoryListRepository
import com.localvalu.registration.api.PassionListApi
import com.localvalu.registration.api.UserExistApi
import com.localvalu.registration.repository.PassionListRepository
import com.localvalu.registration.repository.UserExistRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class PassionListModule
{
    @Provides
    fun providesPassionListApi(remoteDataSource: RemoteDataSource,
                            @ApplicationContext context: Context): PassionListApi
    {
        return remoteDataSource.buildApi(PassionListApi::class.java,context)
    }

    @Provides
    fun providesPassionListRepository(passionListApi: PassionListApi):PassionListRepository
    {
        return PassionListRepository(passionListApi)
    }
}