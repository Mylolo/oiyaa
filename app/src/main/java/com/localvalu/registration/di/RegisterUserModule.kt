package com.localvalu.registration.di

import android.content.Context
import com.localvalu.home.api.CuisinesCategoryListApi
import com.localvalu.base.RemoteDataSource
import com.localvalu.home.repository.CuisinesCategoryListRepository
import com.localvalu.registration.api.RegisterUserApi
import com.localvalu.registration.api.UserExistApi
import com.localvalu.registration.repository.RegisterRepository
import com.localvalu.registration.repository.UserExistRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class RegisterUserModule
{
    @Provides
    fun providesRegisterUserApi(remoteDataSource: RemoteDataSource,
                            @ApplicationContext context: Context): RegisterUserApi
    {
        return remoteDataSource.buildApi(RegisterUserApi::class.java,context)
    }

    @Provides
    fun providesUserExistRepository(registerUserApi:RegisterUserApi):RegisterRepository
    {
        return RegisterRepository(registerUserApi)
    }
}