package com.localvalu.registration.di

import android.content.Context
import com.localvalu.home.api.CuisinesCategoryListApi
import com.localvalu.base.RemoteDataSource
import com.localvalu.home.repository.CuisinesCategoryListRepository
import com.localvalu.registration.api.UserExistApi
import com.localvalu.registration.repository.UserExistRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class UserExistModule
{
    @Provides
    fun providesUserExistApi(remoteDataSource: RemoteDataSource,
                            @ApplicationContext context: Context): UserExistApi
    {
        return remoteDataSource.buildApi(UserExistApi::class.java,context)
    }

    @Provides
    fun providesUserExistRepository(userExistApi: UserExistApi):UserExistRepository
    {
        return UserExistRepository(userExistApi)
    }
}