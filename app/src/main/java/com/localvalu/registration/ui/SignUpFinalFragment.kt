package com.localvalu.registration.ui

import android.os.Bundle
import com.localvalu.R
import androidx.core.content.res.ResourcesCompat
import android.graphics.PorterDuff
import android.text.SpannableStringBuilder
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.TextView
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.localvalu.base.Resource
import com.localvalu.databinding.FragmentSignUpFinalBinding
import com.localvalu.registration.viewmodel.RegisterUserViewModel
import com.localvalu.registration.model.RegisterRequest
import com.utility.*
import java.util.regex.Pattern

class SignUpFinalFragment : Fragment()
{
    private val TAG = SignUpFinalFragment::class.simpleName
    private lateinit var registerRequest:RegisterRequest
    private var binding: FragmentSignUpFinalBinding? = null
    private var colorBlack = 0
    private var colorWhite = 0
    private val viewModel: RegisterUserViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        colorBlack = resources.getColor(R.color.colorBlack)
        colorWhite = resources.getColor(R.color.colorWhite)
        setProperties()
        readFromBundle()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentSignUpFinalBinding.inflate(inflater,container,false)
        return binding?.root
    }

    private fun readFromBundle()
    {
        if (arguments!=null)
        {
            registerRequest = arguments?.getParcelable(AppUtils.BUNDLE_REGISTER_REQUEST)!!
        }
    }

    private fun setProperties()
    {
        val type = ResourcesCompat.getFont(requireContext(), R.font.comfortaa_regular)
        binding!!.etPassword.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        binding!!.etPassword.typeface = type
        binding!!.etConfirmPassword.inputType =
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        binding!!.etConfirmPassword.typeface = type
        val drawable = binding!!.imgBack.drawable
        if (drawable != null)
        {
            drawable.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP)
            binding!!.imgBack.setImageDrawable(drawable)
        }
        setPasswordTextInstructions()
        binding!!.btnCreateAccount.setOnClickListener(_OnClickListener)
        binding!!.nestedScrollView.setOnClickListener(_OnClickListener)
        binding!!.imgBack.setOnClickListener(_OnClickListener)
        observeSubscribers()
    }

    var _OnClickListener = View.OnClickListener { view -> onButtonClick(view) }

    private fun setPasswordTextInstructions()
    {
        val builder = SpannableStringBuilder()
        val str1 = SpannableString(getString(R.string.lbl_password_one))
        str1.setSpan(ForegroundColorSpan(colorBlack), 0, str1.length, 0)
        builder.append(str1).append(" ")
        val str2 = SpannableString(getString(R.string.lbl_password_one_a_white))
        str2.setSpan(ForegroundColorSpan(colorWhite), 0, str2.length, 0)
        builder.append(str2).append(" ")
        binding!!.tvPasswordTextOne.setText(builder, TextView.BufferType.SPANNABLE)
    }

    fun onButtonClick(view: View)
    {
        when (view.id)
        {
            R.id.imgBack            -> onDestroy()
            R.id.btnCreateAccount   -> if (isValid)
            {
                updateRegisterRequest()
            }
        }
    }

    private val isValid: Boolean
        private get()
        {
            if (binding!!.etPassword.text.toString().isEmpty())
            {
                binding!!.etPassword.requestFocus()
                binding!!.etPassword.error = getString(R.string.enter_pass)
                return false
            }
            else if (!checkRegex(binding!!.etPassword.text.toString().trim { it <= ' ' }))
            {
                binding!!.etPassword.requestFocus()
                binding!!.etPassword.error = getString(R.string.greater_pass)
                return false
            }
            else if (binding!!.etConfirmPassword.text.toString().isEmpty())
            {
                binding!!.etConfirmPassword.requestFocus()
                binding!!.etConfirmPassword.error = getString(R.string.enter_pass)
            }
            else if (!checkRegex(binding!!.etConfirmPassword.text.toString().trim { it <= ' ' }))
            {
                binding!!.etConfirmPassword.requestFocus()
                binding!!.etConfirmPassword.error = getString(R.string.greater_pass)
                return false
            }
            else if (binding!!.etConfirmPassword.text.toString() != binding!!.etPassword.text.toString())
            {
                binding!!.etConfirmPassword.requestFocus()
                binding!!.etConfirmPassword.error = getString(R.string.pass_not_matched)
                return false
            }
            return true
        }

    private fun checkRegex(password: String): Boolean
    {
        // Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
        val PASSWORD_PATTERN =
            "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[~`#@$!%*?^&])[A-Za-z\\d~`#@$!%*?&]{8,}$"
        val pattern = Pattern.compile(PASSWORD_PATTERN)
        val matcher = pattern.matcher(password.trim { it <= ' ' })
        return matcher.matches()
    }

    private fun observeSubscribers()
    {
        viewModel.registerUserResponse.observe(viewLifecycleOwner, Observer {
           when(it)
           {
               is Resource.Loading->
               {
                   showProgress()
               }
               is Resource.Success->
               {
                   showContents()
                   if (it.value.registerCustomerResult.errorDetails.errorCode == 0)
                   {
                       viewModel.registerUserInLocal(it.value.registerCustomerResult.userBasicInfo)
                       //callLoginAPI(response.body()!!.registerCustomerResult.userBasicInfo.noOfTokens)
                   }
                   else
                   {
                       showMessage(it.value.registerCustomerResult.errorDetails.errorMessage)
                   }
               }
               is Resource.Failure->
               {
                   handleAPIError(it){registerTheUser()}
               }
           }
        });
    }

    private fun showProgress()
    {
        binding?.layoutProgress?.layoutRoot?.visibility = View.VISIBLE
        binding?.layoutError?.layoutRoot?.visibility = View.GONE
        binding?.nestedScrollView?.visibility = View.GONE
    }

    private fun showError(message:String,retry: Boolean)
    {
        binding?.layoutProgress?.layoutRoot?.visibility = View.GONE
        binding?.layoutError?.layoutRoot?.visibility = View.VISIBLE
        binding?.nestedScrollView?.visibility = View.GONE
    }

    private fun showContents()
    {
        binding?.layoutProgress?.layoutRoot?.visibility = View.GONE
        binding?.layoutError?.layoutRoot?.visibility = View.GONE
        binding?.nestedScrollView?.visibility = View.VISIBLE
    }

    private fun updateRegisterRequest()
    {
        registerRequest?.password = binding?.etPassword?.text.toString()
    }

    private fun registerTheUser()
    {
        viewModel.registerUser(registerRequest)
    }

    companion object
    {
        private val TAG = SignUpFinalFragment::class.java.simpleName
    }
}