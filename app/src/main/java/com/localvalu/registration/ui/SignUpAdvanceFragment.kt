package com.localvalu.registration.ui

import com.localvalu.user.dto.RegisterDataDto
import android.os.Bundle
import com.localvalu.R
import androidx.core.content.ContextCompat
import android.view.WindowManager
import androidx.core.content.res.ResourcesCompat
import android.graphics.PorterDuff
import android.text.SpannableStringBuilder
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.localvalu.databinding.FragmentSignupAdvanceBinding
import com.utility.AppUtils
import com.utility.ActivityController
import com.localvalu.user.SignUpFinalActivity
import com.localvalu.user.SignUpAdvanceActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignUpAdvanceFragment : Fragment()
{
    private var registerDataDto: RegisterDataDto? = null
    private var binding: FragmentSignupAdvanceBinding? = null
    private var colorWhite = 0
    private var colorBlack = 0

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        colorBlack = ContextCompat.getColor(requireContext(), R.color.colorBlack)
        colorWhite = ContextCompat.getColor(requireContext(), R.color.colorWhite)
        readFromBundle()
        setProperties()
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    private fun readFromBundle()
    {
        if (arguments!= null)
        {
            registerDataDto = arguments?.getSerializable("registerDataDto") as RegisterDataDto?
            println("registerDataDto " + registerDataDto.toString())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentSignupAdvanceBinding.inflate(inflater,container,false)
        return binding!!.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
    }

    private fun setProperties()
    {
        setPassionsText()
        val drawableBack = ResourcesCompat.getDrawable(resources, R.drawable.ic_app_arrow, null)
        drawableBack!!.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP)
        binding!!.imgBack.setImageDrawable(drawableBack)
        binding!!.imgBack.setOnClickListener(_OnClickListener)
        binding!!.btnMoreDialogPassions.setOnClickListener(_OnClickListener)
        binding!!.btnContinueDialogPassions.setOnClickListener(_OnClickListener)
    }

    private fun setPassionsText()
    {
        val builder = SpannableStringBuilder()
        val str1 = SpannableString(getString(R.string.lbl_sign_up_dialog_passions_text))
        str1.setSpan(ForegroundColorSpan(colorBlack), 0, str1.length, 0)
        builder.append(str1).append(" ")
        val str2 = SpannableString(getString(R.string.lbl_sign_up_dialog_passions_text_a_white))
        str2.setSpan(ForegroundColorSpan(colorWhite), 0, str2.length, 0)
        builder.append(str2).append(" ")
        val str3 = SpannableString(getString(R.string.lbl_sign_up_dialog_passions_text_a_black))
        str3.setSpan(ForegroundColorSpan(colorBlack), 0, str3.length, 0)
        builder.append(str3).append(" ")
        binding!!.tvPassionsTextOne.setText(builder, TextView.BufferType.SPANNABLE)
        val builder2 = SpannableStringBuilder()
        val str4 = SpannableString(getString(R.string.lbl_sign_up_dialog_passions_text_b_black))
        str4.setSpan(ForegroundColorSpan(colorBlack), 0, str4.length, 0)
        builder2.append(str4).append(" ")
        val str5 = SpannableString(getString(R.string.lbl_sign_up_dialog_passions_text_b_white))
        str5.setSpan(ForegroundColorSpan(colorWhite), 0, str5.length, 0)
        builder2.append(str5).append(" ")
        val str6 = SpannableString(getString(R.string.lbl_sign_up_dialog_passions_text_b_black_two))
        str6.setSpan(ForegroundColorSpan(colorBlack), 0, str6.length, 0)
        builder2.append(str6).append(" ")
        binding!!.tvPassionsTextTwo.setText(builder2, TextView.BufferType.SPANNABLE)
    }

    var _OnClickListener = View.OnClickListener { view -> onButtonClick(view) }
    fun onButtonClick(view: View)
    {
        when (view.id)
        {
            R.id.imgBack -> onDestroy()
            R.id.btnContinueDialogPassions ->
            {
                registerDataDto!!.passionFilled = AppUtils.PASSION_NOT_FILLED
                val bundleSubmit = Bundle()
                bundleSubmit.putSerializable("registerDataDto", registerDataDto)
                ActivityController.startNextActivity(
                    requireActivity(),
                    SignUpFinalActivity::class.java,
                    bundleSubmit,
                    false
                )
            }
            R.id.btnMoreDialogPassions ->
            {
                val bundleMore = Bundle()
                bundleMore.putSerializable("registerDataDto", registerDataDto)
                ActivityController.startNextActivity(
                    requireActivity(),
                    SignUpAdvanceActivity::class.java,
                    bundleMore,
                    false
                )
            }
        }
    }

    companion object
    {
        private val TAG = SignUpAdvanceFragment::class.java.name
    }
}