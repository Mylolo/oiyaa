package com.localvalu.registration.ui


import android.app.Activity
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.textfield.TextInputEditText
import com.google.zxing.integration.android.IntentIntegrator
import com.localvalu.R
import com.localvalu.base.Resource
import com.localvalu.databinding.FragmentSignUpBinding
import com.localvalu.registration.viewmodel.UserExistViewModel
import com.localvalu.registration.model.UserExistRequest
import com.localvalu.user.SmallCaptureActivity
import com.localvalu.user.ValidationUtils
import com.localvalu.user.dto.RegisterDataDto
import com.utility.*
import com.utility.validators.RangeValidator
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class SignUpFragment : Fragment()
{
    private lateinit var binding:FragmentSignUpBinding
    private var full_name: String? = null
    private var mobile: String? = null
    private var email: String? = null
    private var qrCodeResult: String? = null
    private var selectedGender: String? = null
    private var selectedAgeGroup: String? = null
    private var referralCode: String? = null
    private var registerDataDto: RegisterDataDto? = null
    private val CUSTOMIZED_REQUEST_CODE = 0x0000ffff
    private var colorBlack = 0
    private var colorGray = 0
    private var colorWhite = 0
    private var strDOB: String? = null
    private var dobDate: Date? = null
    private val startTime = Calendar.getInstance()
    private val endTime = Calendar.getInstance()
    private val viewModel: UserExistViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        colorBlack = ContextCompat.getColor(requireContext(), R.color.colorBlack)
        colorGray = ContextCompat.getColor(requireContext(), R.color.colorGray)
        colorWhite = ContextCompat.getColor(requireContext(), R.color.colorWhite)
        startTime[1950, 0] = 2
        endTime[2002, 11] = 31
        readFromBundle()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentSignUpBinding.inflate(inflater,container,false)
        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
        observeSubscribers()
    }

    private fun readFromBundle()
    {
        if (arguments != null)
        {
            full_name = requireArguments().getString(AppUtils.BUNDLE_FULL_NAME)
            mobile = requireArguments().getString(AppUtils.BUNDLE_MOBILE)
            email = requireArguments().getString(AppUtils.BUNDLE_EMAIL)
            qrCodeResult = requireArguments().getString(AppUtils.BUNDLE_QR_CODE_RESULT)
            binding.etFullName.setText(full_name)
            binding.etMobileNumber.setText(mobile)
            binding.etEmail.setText(email)
            if (qrCodeResult != null)
            {
                if (qrCodeResult!!.isEmpty())
                {
                    binding!!.etReferralCode.setText("IDL")
                }
                else
                {
                    binding!!.etReferralCode.setText(qrCodeResult)
                    binding!!.etReferralCode.isEnabled = false
                }
            }
            else binding.etReferralCode.text = qrCodeResult
        }
        else
        {
            binding.etReferralCode.setText("IDL")
        }
    }

    var _OnClickListener = View.OnClickListener { view -> onButtonClick(view) }

    private fun setProperties()
    {
        setFilters(binding.etFullName, "[a-zA-Z ]+")
        setFilters(binding.etHomePostCode, "[0-9a-zA-Z ]+")
        setFilters(binding.etWorkPostCode, "[0-9a-zA-Z ]+")
        dobDate = Date()
        val drawableBack = ResourcesCompat.getDrawable(resources, R.drawable.ic_app_arrow, null)
        drawableBack!!.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP)
        binding!!.imgBack.setImageDrawable(drawableBack)
        binding!!.imgBack.setOnClickListener(_OnClickListener)
        binding!!.btnSignUpSubmit.setOnClickListener(_OnClickListener)
        binding!!.etDOB.setOnClickListener(_OnClickListener)
        binding!!.tvGenderError.visibility = View.GONE
        binding!!.cnlMain.setOnClickListener(_OnClickListener)
        val drawable = ResourcesCompat.getDrawable(resources, R.drawable.menu_scanpay, null)
        drawable!!.setColorFilter(colorGray, PorterDuff.Mode.SRC_ATOP)
        binding!!.tilReferralCode.endIconDrawable = drawable
        binding!!.tilReferralCode.setEndIconOnClickListener { startScan() }
        binding!!.radGroupAge.setOnCheckedChangeListener { radioButton, checked ->
            if (checked)
            {
                binding!!.tvAgeError.visibility = View.GONE
                when (radioButton.id)
                {
                    R.id.radioButton16To24 -> selectedAgeGroup =
                        binding!!.radioButton16To24.text.toString()
                    R.id.radioButton25To34 -> selectedAgeGroup =
                        binding!!.radioButton25To34.text.toString()
                    R.id.radioButton35To44 -> selectedGender =
                        binding!!.radioButton35To44.text.toString()
                    R.id.radioButton45To54 -> selectedGender =
                        binding!!.radioButton45To54.text.toString()
                    R.id.radioButton55To64 -> selectedGender =
                        binding!!.radioButton55To64.text.toString()
                    R.id.radioButton65Plus -> selectedGender =
                        binding!!.radioButton65Plus.text.toString()
                }
            }
        }
        binding!!.radGroupGender.setOnCheckedChangeListener { radioGroup, radioButtonId ->
            binding!!.tvGenderError.visibility = View.GONE
            when (radioButtonId)
            {
                R.id.radioButtonMale -> selectedGender = binding!!.radioButtonMale.text.toString()
                R.id.radioButtonFemale -> selectedGender =
                    binding!!.radioButtonFemale.text.toString()
                R.id.radioButtonOther -> selectedGender = "Other"
            }
        }
        /*binding.radGroupGender.setOnCheckedChangeListener(new RadioGridGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioButton radioButton, boolean checked)
            {
                binding.tvGenderError.setVisibility(View.GONE);

                switch (radioButton.getId())
                {
                    case R.id.radioButtonMale:
                        selectedGender = binding.radioButtonMale.getText().toString();
                        break;
                    case R.id.radioButtonFemale:
                        selectedGender = binding.radioButtonFemale.getText().toString();
                        break;
                    case R.id.radioButtonOther:
                        selectedGender = "Other";
                        break;
                }
            }
        });*/registerDataDto = RegisterDataDto()
        binding!!.etFullName.requestFocus()
    }

    private fun observeSubscribers()
    {
        viewModel.userExistResponse.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            when(it)
            {
                is Resource.Loading->
                {
                    showProgress()
                }
                is Resource.Success->
                {
                    showContents()
                    if(it.value.registerCustomerResult.errorDetails.errorCode==0)
                    {
                        val emailExists: String = it.value.registerCustomerResult.registerExistsCustomer.emailExists
                        val mobileExists: String = it.value.registerCustomerResult.registerExistsCustomer.mobileExists
                        val referrFlagValid: String = it.value.registerCustomerResult.registerExistsCustomer.referrFlagValid

                        if (emailExists == "false" && mobileExists == "false")
                        {
                            //Check for referral code
                            if (binding.etReferralCode.text!!.length > 0)
                            {
                                if (referrFlagValid == "false")
                                {
                                    //Show Snackbar message
                                    showMessage(getString(R.string.lbl_invalid_referral_code))
                                }
                            }
                        }
                        else if (emailExists == "true")
                        {
                            showMessage(getString(R.string.msg_email_already_exists))
                        }
                        else
                        {
                            showMessage(getString(R.string.msg_mobile_number_already_exists))
                        }
                    }
                }
                is Resource.Failure->
                {
                    handleAPIError(it){checkUserExist()}
                }
            }
        })
    }

    private fun showProgress()
    {
        binding.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding.layoutError.layoutRoot.visibility = View.GONE
        binding.nestedScrollView.visibility = View.GONE
    }

    private fun showError(message:String,retry: Boolean)
    {
        binding.layoutProgress.layoutRoot.visibility = View.GONE
        binding.layoutError.layoutRoot.visibility = View.VISIBLE
        binding.nestedScrollView.visibility = View.GONE
    }

    private fun showContents()
    {
        binding.layoutProgress.layoutRoot.visibility = View.GONE
        binding.layoutError.layoutRoot.visibility = View.GONE
        binding.nestedScrollView.visibility = View.VISIBLE
    }

    private fun showDOBPicker()
    {
        val builder: MaterialDatePicker.Builder<*> = MaterialDatePicker.Builder.datePicker()
        builder.setCalendarConstraints(dobDateLimitRange().build())
        val picker = builder.build()
        picker.show(parentFragmentManager, picker.toString())
        picker.addOnPositiveButtonClickListener { selection ->
            val milliseconds = selection as Long
            val selectedDate = Calendar.getInstance()
            selectedDate.timeInMillis = milliseconds
            dobDate = selectedDate.time
            strDOB = convertToStringInServerFormat(selectedDate.time)
            binding!!.etDOB.setText(convertDateToStringInUKFormat(selectedDate.time))
        }
    }

    private fun getUserExistRequest():UserExistRequest
    {
        val userExistRequest = UserExistRequest("",
                                                 binding.etEmail.text.toString().trim(),
                                                 binding.etMobileNumber.text.toString().trim(),
                                                 binding.etReferralCode.text.toString().trim())
        return userExistRequest;
    }

    private fun checkUserExist()
    {
        if (isValid)
        {
            binding!!.tvGenderError.visibility = View.GONE
            viewModel.checkUserExists(getUserExistRequest())
        }
    }

    fun onButtonClick(view: View)
    {
        when (view.id)
        {
            R.id.btnSignUpSubmit -> checkUserExist()
            R.id.imgBack         -> onDestroy()
            R.id.cnlMain         -> CommonUtility.hideKeyboard(requireActivity())
            R.id.etDOB           -> showDOBPicker()
        }
    }

    private val isValid: Boolean
        private get()
        {
            if (binding!!.etFullName.text.toString().trim { it <= ' ' }.isEmpty())
            {
                binding!!.etFullName.error = getString(R.string.msg_enter_your_full_name)
                binding!!.etFullName.requestFocus()
                return false
            }
            else if (binding!!.etMobileNumber.text.toString().trim { it <= ' ' }.isEmpty())
            {
                binding!!.etMobileNumber.error = getString(R.string.msg_enter_your_mobile_no)
                binding!!.etMobileNumber.requestFocus()
                return false
            }
            else if (!ValidationUtils.isValidMobileNo(
                    binding!!.etMobileNumber.text.toString().trim { it <= ' ' })
            )
            {
                binding!!.etMobileNumber.error = getString(R.string.msg_enter_valid_mobile_no)
                binding!!.etMobileNumber.requestFocus()
                return false
            }
            else if (!Patterns.EMAIL_ADDRESS.matcher(
                    binding!!.etEmail.text.toString().trim { it <= ' ' }).matches()
            )
            {
                binding!!.etEmail.error = getString(R.string.msg_enter_valid_email_id)
                binding!!.etEmail.requestFocus()
                return false
            }
            else if (!binding!!.etReferralCode.text.toString().equals("")&&
                !binding.etReferralCode.getText().toString().trim().contains("IDL"))
            {
                binding.etHomePostCode.error = getString(R.string.msg_valid_referral_code)
                binding.etHomePostCode.requestFocus()
                return false
            }
            else if (binding!!.etHomePostCode.text.toString().trim { it <= ' ' } == "")
            {
                binding!!.etHomePostCode.error = getString(R.string.msg_enter_your_home_post_code)
                binding!!.etHomePostCode.requestFocus()
                return false
            }
            else if (!ValidationUtils.isValidPostCode(
                    binding!!.etHomePostCode.text.toString().trim { it <= ' ' }
                        .toUpperCase())
            )
            {
                binding!!.etHomePostCode.error = getString(R.string.msg_enter_valid_post_code)
                binding!!.etHomePostCode.requestFocus()
                return false
            }
            else if (binding!!.radGroupGender.checkedRadioButtonId == -1)
            {
                binding!!.tvGenderError.text = getString(R.string.msg_select_gender)
                binding!!.tvGenderError.visibility = View.VISIBLE
                return false
            }
            /* else if (binding.etAddress.getText().toString().equals(""))
            {
            binding.etAddress.setError(getString(R.string.msg_enter_address));
            binding.etAddress.requestFocus();
            return false;
            }
            else if (binding.etCity.getText().toString().equals(""))
            {
            binding.etCity.setError(getString(R.string.msg_enter_city));
            binding.etCity.requestFocus();
            return false;
            }*/
            if (binding!!.etWorkPostCode.text.toString().trim { it <= ' ' } != "")
            {
                if (!ValidationUtils.isValidPostCode(
                        binding!!.etWorkPostCode.text.toString().trim { it <= ' ' }
                            .toUpperCase()))
                {
                    binding!!.etWorkPostCode.error = getString(R.string.msg_enter_valid_post_code)
                    binding!!.etWorkPostCode.requestFocus()
                    return false
                }
            }
            if (binding!!.etDOB.text.toString().trim { it <= ' ' } == "")
            {
                binding!!.etDOB.error = getString(R.string.msg_select_dob)
                binding!!.etDOB.visibility = View.VISIBLE
                return false
            }
            return true
        }

    private fun setFilters(textInputEditText: TextInputEditText, regEx: String)
    {
        textInputEditText.filters = arrayOf(
            InputFilter { cs, start, end, spanned, dStart, dEnd -> // TODO Auto-generated method stub
                if (cs == "")
                { // for backspace
                    return@InputFilter cs
                }
                if (cs.toString().contains(regEx))
                {
                    cs
                }
                else ""
            }
        )
    }

    private fun startScan()
    {
        val integrator = IntentIntegrator(requireActivity())
        integrator.setOrientationLocked(false)
        integrator.captureActivity = SmallCaptureActivity::class.java
        integrator.setRequestCode(CUSTOMIZED_REQUEST_CODE)
        integrator.initiateScan()
    }

    /*
      Limit selectable Date range
    */
    private fun dobDateLimitRange(): CalendarConstraints.Builder
    {
        val constraintsBuilderRange = CalendarConstraints.Builder()
        constraintsBuilderRange.setStart(startTime.timeInMillis)
        constraintsBuilderRange.setEnd(endTime.timeInMillis)
        constraintsBuilderRange.setValidator(
            RangeValidator(
                startTime.timeInMillis,
                endTime.timeInMillis
            )
        )
        return constraintsBuilderRange
    }

    private var _OnItemSelectedListener: OnItemSelectedListener = object : OnItemSelectedListener
    {
        override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long)
        {
            if (position != 0)
            {
                if (binding!!.spinnerDays.selectedItemPosition != 0 && binding!!.spinnerMonths.selectedItemPosition != 0 && binding!!.spinnerYear.selectedItemPosition != 0)
                {
                    val selectedDate = Calendar.getInstance()
                    val format = SimpleDateFormat("dd MMM yyyy")
                    val day = binding!!.spinnerDays.selectedItem.toString().toInt()
                    val MMM = binding!!.spinnerMonths.selectedItemPosition
                    val year = binding!!.spinnerYear.selectedItem.toString().toInt()
                    selectedDate[year, MMM] = day
                    binding!!.tvDOBError.visibility = View.GONE
                    registerDataDto!!.dob = year.toString() + "-" + pad(MMM) + "-" + pad(day)
                    //Log.d(TAG, "onItemSelected: dob-" + registerDataDto.dob);
                }
            }
        }

        override fun onNothingSelected(parent: AdapterView<*>?)
        {
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        if (requestCode != CUSTOMIZED_REQUEST_CODE && requestCode != IntentIntegrator.REQUEST_CODE)
        {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data)
            return
        }
        when (requestCode)
        {
            CUSTOMIZED_REQUEST_CODE ->
            {
            }
            else                    ->
            {
            }
        }
        val result = IntentIntegrator.parseActivityResult(resultCode, data)
        if (result.contents == null)
        {
            Log.d("MainActivity", "Cancelled scan")
            Toast.makeText(requireContext(), "Cancelled", Toast.LENGTH_LONG).show()
        }
        else
        {
            Log.d("MainActivity", "Scanned" + result.contents)
            binding!!.etReferralCode.setText(result.contents)
        }
    }

    private fun getAgeGroup(age: Int): String
    {
        if (age >= 16 && age <= 24) return getString(R.string.lbl_16_to_24)
        else if (age >= 25 && age <= 34) return getString(
            R.string.lbl_25_to_34
        )
        else if (age >= 35 && age <= 44) return getString(R.string.lbl_35_to_44)
        else if (age >= 44 && age <= 54) return getString(
            R.string.lbl_45_to_54
        )
        else if (age >= 55 && age <= 64) return getString(R.string.lbl_55_to_64)
        else if (age >= 65) return getString(
            R.string.lbl_65_plus
        )
        return ""
    }

    private fun hideKeyboard(activity: Activity?)
    {
        if (activity != null && activity.window != null)
        {
            activity.window.decorView
            val imm = activity.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.hideSoftInputFromWindow(activity.window.decorView.windowToken, 0)
        }
    }

    private fun convertDateToStringInUKFormat(date: Date): String
    {
        val format = SimpleDateFormat("dd/MM/yyyy")
        return format.format(date)
    }

    private fun convertDateStringInUKFormat(strDate: String): String
    {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd")
        return try
        {
            val date = inputFormat.parse(strDate)
            convertDateToStringInUKFormat(date)
        }
        catch (ex: Exception)
        {
            ""
        }
    }

    private fun getDateFromString(strDate: String): Date
    {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd")
        return try
        {
            inputFormat.parse(strDate)
        }
        catch (ex: Exception)
        {
            Date()
        }
    }

    private fun convertToStringInServerFormat(date: Date): String
    {
        val format = SimpleDateFormat("yyyy-MM-dd")
        return format.format(date)
    }

    companion object
    {
        private val TAG = SignUpFragment::class.java.simpleName
        fun pad(data: Int): String
        {
            return if (data >= 10) data.toString() else "0$data"
        }
    }

}