package com.localvalu.registration.ui

import com.localvalu.user.adapter.PassionListAdapter
import com.localvalu.user.dto.PassionDetails
import com.localvalu.user.dto.RegisterDataDto
import androidx.recyclerview.widget.LinearLayoutManager
import android.os.Bundle
import com.localvalu.R
import androidx.core.content.ContextCompat
import android.view.WindowManager
import androidx.core.content.res.ResourcesCompat
import android.graphics.PorterDuff
import androidx.recyclerview.widget.RecyclerView
import android.text.SpannableStringBuilder
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.TextView
import com.localvalu.user.SignUpFinalActivity
import com.localvalu.user.dto.PassionCategory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.localvalu.user.dto.PassionListRequest
import android.widget.CheckBox
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.localvalu.base.Resource
import com.localvalu.databinding.FragmentSignupDialogPassionsBinding
import com.localvalu.registration.viewmodel.PassionListViewModel
import com.utility.*
import dagger.hilt.android.AndroidEntryPoint
import java.lang.StringBuilder
import java.util.ArrayList

@AndroidEntryPoint
class DialogPassionsFragment : Fragment()
{
    private val viewModel: PassionListViewModel by viewModels()
    private var passionListAdapter: PassionListAdapter? = null
    private var passionDetailsList: MutableList<PassionDetails>? = null
    private var registerDataDto: RegisterDataDto? = null
    private var layoutManager: LinearLayoutManager? = null
    private var strPassions: String? = null
    private var binding: FragmentSignupDialogPassionsBinding? = null
    private var colorWhite = 0
    private var colorBlack = 0

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        colorBlack = ContextCompat.getColor(requireContext(), R.color.colorBlack)
        colorWhite = ContextCompat.getColor(requireContext(), R.color.colorWhite)
        readFromBundle()
        requireActivity().window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }

    private fun readFromBundle()
    {
        if (arguments != null)
        {
            registerDataDto = arguments?.getSerializable("registerDataDto") as RegisterDataDto?
            println("registerDataDto " + registerDataDto.toString())
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentSignupDialogPassionsBinding.inflate(inflater,container,false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
    }

    private fun setProperties()
    {
        val drawableBack = ResourcesCompat.getDrawable(resources, R.drawable.ic_app_arrow, null)
        drawableBack!!.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP)
        binding!!.imgBack.setImageDrawable(drawableBack)
        binding!!.imgBack.setOnClickListener(_OnClickListener)
        binding!!.layoutError.btnRetry.setOnClickListener(_OnClickListener)
        binding!!.btnContinueSignUpAdvance.setOnClickListener(_OnClickListener)
        layoutManager = LinearLayoutManager(requireContext())
        layoutManager!!.orientation = RecyclerView.VERTICAL
        binding!!.rvPassionList.layoutManager = layoutManager
        setPassionsText()
        fetchList()
    }

    private fun setPassionsText()
    {
        val builder = SpannableStringBuilder()
        val str1 = SpannableString(getString(R.string.lbl_sign_up_advance_header))
        str1.setSpan(ForegroundColorSpan(colorBlack), 0, str1.length, 0)
        builder.append(str1).append(" ")
        val str2 = SpannableString(getString(R.string.lbl_sign_up_advance_header_a_white))
        str2.setSpan(ForegroundColorSpan(colorWhite), 0, str2.length, 0)
        builder.append(str2).append(" ")
        val str3 = SpannableString(getString(R.string.lbl_sign_up_advance_header_a_black))
        str3.setSpan(ForegroundColorSpan(colorBlack), 0, str3.length, 0)
        builder.append(str3).append(" ")
        binding!!.tvPassionMessageOne.setText(builder, TextView.BufferType.SPANNABLE)
        binding!!.tvPassionMessageTwo.text = getString(R.string.lbl_sign_up_advance_header_b_black)
        binding!!.tvPassionMessageThree.text =
            getString(R.string.lbl_sign_up_advance_header_c_black)
    }

    var _OnClickListener = View.OnClickListener { v -> onButtonClick(v) }

    fun onButtonClick(view: View)
    {
        when (view.id)
        {
            R.id.imgBack -> onDestroy()
            R.id.btnContinueSignUpAdvance -> if (isValid)
            {
                registerDataDto!!.passionFilled = AppUtils.PASSION_FILLED
                registerDataDto!!.passionDetails = strPassions
                val bundle = Bundle()
                bundle.putSerializable("registerDataDto", registerDataDto)
                ActivityController.startNextActivity(
                    requireActivity(),
                    SignUpFinalActivity::class.java,
                    bundle,
                    false
                )
            }
        }
    }

    private fun observeSubscribers()
    {
        viewModel.passionListResponse.observe(viewLifecycleOwner, Observer {
            when(it)
            {
                is Resource.Loading->
                {
                    showProgress()
                }
                is Resource.Success->
                {
                    showList()
                    if (it.value.passionsListResult.errorDetails.errorCode == 0)
                    {
                        setData(it.value.passionsListResult.passionCategoryList)
                    }
                    else
                    {
                        showMessage(it.value.passionsListResult.errorDetails.errorMessage)
                    }
                }
                is Resource.Failure->
                {
                    handleAPIError(it){
                        fetchList()
                    }
                }
            }
        })
    }

    private fun fetchList()
    {
        viewModel.fetchPassionList(PassionListRequest())
    }

    private fun setData(passionCategoryList: List<PassionCategory>)
    {
        passionDetailsList = ArrayList()
        for (i in passionCategoryList.indices)
        {
            val category = passionCategoryList[i]
            val passionDetailsHeader = PassionDetails()
            passionDetailsHeader.type = PassionListAdapter.HEADER_VIEW_HOLDER
            passionDetailsHeader.passion = category.passionCategory
            passionDetailsList?.add(passionDetailsHeader)
            for (j in category.passionDetailsList.indices)
            {
                val tempPassionDetails = category.passionDetailsList[j]
                val passionDetailsItem = PassionDetails()
                passionDetailsItem.type = PassionListAdapter.ITEM_VIEW_HOLDER
                passionDetailsItem.passion = tempPassionDetails.passion
                passionDetailsItem.checked = false
                passionDetailsList?.add(passionDetailsItem)
            }
        }
        if (passionDetailsList?.size!! > 0)
        {
            showList()
            passionListAdapter = PassionListAdapter(passionDetailsList)
            binding!!.rvPassionList.adapter = passionListAdapter
        }
    }

    private fun showProgress()
    {
        binding!!.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding!!.cnlSignUpAdvanceMain.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showList()
    {
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.cnlSignUpAdvanceMain.visibility = View.VISIBLE
        binding!!.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showError(strMessage: String, retry: Boolean)
    {
        binding!!.layoutProgress.layoutRoot.visibility = View.GONE
        binding!!.cnlSignUpAdvanceMain.visibility = View.GONE
        binding!!.layoutError.layoutRoot.visibility = View.VISIBLE
        binding!!.layoutError.tvError.text = strMessage
        if (retry) binding!!.layoutError.btnRetry.visibility = View.VISIBLE
        else binding!!.layoutError.btnRetry.visibility = View.GONE
    }

    private fun getValueFromCheckBox(cb: CheckBox)
    {
        if (cb.isChecked)
        {
            if (registerDataDto!!.passionDetails.trim { it <= ' ' }.isEmpty())
            {
                registerDataDto!!.passionDetails = cb.tag.toString()
            }
            else
            {
                registerDataDto!!.passionDetails =
                    registerDataDto!!.passionDetails + "," + cb.tag.toString()
            }
        }
    }

    private val isValid: Boolean
        private get()
        {
            val strPassionStringBuilder = StringBuilder()
            var valid = false
            for (i in passionDetailsList!!.indices)
            {
                val passionDetails = passionDetailsList!![i]
                if (passionDetails.checked)
                {
                    valid = true
                    strPassionStringBuilder.append(passionDetails.passion)
                    strPassionStringBuilder.append(",")
                }
            }
            return if (valid)
            {
                strPassionStringBuilder.deleteCharAt(strPassionStringBuilder.length - 1)
                strPassions = strPassionStringBuilder.toString()
                true
            }
            else false
        }

    companion object
    {
        private const val TAG = "FragmentDialogPassions"

        fun pad(data: Int): String
        {
            return if (data >= 10) data.toString() else "0$data"
        }
    }
}