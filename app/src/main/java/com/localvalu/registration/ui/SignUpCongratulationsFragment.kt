package com.localvalu.registration.ui

import android.os.Bundle
import com.localvalu.R
import androidx.core.content.ContextCompat
import com.utility.AppUtils
import androidx.core.content.res.ResourcesCompat
import android.graphics.PorterDuff
import com.localvalu.user.dto.UserBasicInfo
import com.utility.PreferenceUtility
import com.google.zxing.WriterException
import kotlin.Throws
import android.graphics.Bitmap
import android.graphics.Color
import android.view.View
import androidx.fragment.app.Fragment
import com.google.zxing.common.BitMatrix
import com.google.zxing.MultiFormatWriter
import com.google.zxing.BarcodeFormat
import com.localvalu.databinding.ActivitySignUpCongratulationsBinding
import com.utility.ActivityController
import com.localvalu.user.dashboard.ui.DashboardActivity
import java.lang.IllegalArgumentException
import java.lang.StringBuilder

class SignUpCongratulationsFragment : Fragment()
{
    private var strNumberOfTokens = "0"
    private var binding: ActivitySignUpCongratulationsBinding? = null
    private var colorWhite = 0

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        colorWhite = ContextCompat.getColor(requireContext(), R.color.colorWhite)
        readFromBundle()
        setProperties()
    }

    private fun readFromBundle()
    {
        if (arguments != null)
        {
            strNumberOfTokens = requireArguments()
                .getString(AppUtils.BUNDLE_NEW_TOKEN_BALANCE, "0")
        }
    }

    private fun setProperties()
    {
        val drawableBack = ResourcesCompat.getDrawable(resources, R.drawable.ic_app_arrow, null)
        drawableBack!!.setColorFilter(colorWhite, PorterDuff.Mode.SRC_ATOP)
        binding!!.imgBack.setImageDrawable(drawableBack)
        binding!!.imgBack.setOnClickListener(_OnClickListener)
        binding!!.cnlMain.setOnClickListener(_OnClickListener)
        binding!!.btnGotoHome.setOnClickListener(_OnClickListener)
        val userBasicInfo = PreferenceUtility.getObjectInAppPreference(
            requireActivity(),
            PreferenceUtility.APP_PREFERENCE_NAME
        ) as UserBasicInfo
        binding!!.tvReferralCode.text = userBasicInfo.qRCodeOutput
        val strTokenBalance = StringBuilder()
        strTokenBalance.append(getString(R.string.lbl_congratulations_msg_one)).append(" ")
            .append(getString(R.string.lbl_currency_letter_euro)).append(" ")
            .append(strNumberOfTokens).append(" ")
            .append(getString(R.string.lbl_congratulations_msg_two))
        binding!!.tvCongratsOne.text = strTokenBalance.toString()
        try
        {
            binding!!.ivQRImage.setImageBitmap(encodeAsBitmap(userBasicInfo.qRCodeOutput))
        }
        catch (e: WriterException)
        {
            e.printStackTrace()
        }
    }

    var _OnClickListener = View.OnClickListener { view -> onButtonClick(view) }

    @Throws(WriterException::class) fun encodeAsBitmap(str: String?): Bitmap?
    {
        val result: BitMatrix
        result = try
        {
            MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, 120, 120, null)
        }
        catch (iae: IllegalArgumentException)
        {
            // Unsupported format
            return null
        }
        val w = result.width
        val h = result.height
        val pixels = IntArray(w * h)
        for (y in 0 until h)
        {
            val offset = y * w
            for (x in 0 until w)
            {
                pixels[offset + x] = if (result[x, y]) Color.BLACK else Color.WHITE
            }
        }
        val bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        bitmap.setPixels(pixels, 0, 120, 0, 0, w, h)
        return bitmap
    }

    fun onButtonClick(view: View)
    {
        when (view.id)
        {
            R.id.imgBack            -> onDestroy()
            R.id.btnGotoHome        -> ActivityController.startNextActivity(
                requireActivity(),
                DashboardActivity::class.java,
                false
            )
            R.id.linearLayout_login ->
            {
                //Redirect to Login(New Architecture)
            }
        }
    }
}