package com.localvalu.registration.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
class RegisterRequest(@SerializedName("Token") private var token:String,
                      @SerializedName("userName") val userName:String,
                      @SerializedName("Mobile") val mobile:String,
                      @SerializedName("ReferralCode") val referralCode:String,
                      @SerializedName("password") var password:String,
                      @SerializedName("Dateofbirth") var dateOfBirth:String,
                      @SerializedName("Agegroup") var ageGroup:String,
                      @SerializedName("gender") var gender:String,
                      @SerializedName("Address") var address:String,
                      @SerializedName("city") var city:String,
                      @SerializedName("country") var country:String,
                      @SerializedName("Home_postcode") var homePostcode:String,
                      @SerializedName("Work_postcode") var workPostcode:String,
                      @SerializedName("Passion_filled") var passionFilled:String,
                      @SerializedName("passionDetails") var passionDetails:String,
                      @SerializedName("SourceFrom") private var sourceFrom:String) : Parcelable
{
    init
    {
        token = "/3+YFd5QZdSK9EKsB8+TlA==";
        sourceFrom = "Android"
    }
}