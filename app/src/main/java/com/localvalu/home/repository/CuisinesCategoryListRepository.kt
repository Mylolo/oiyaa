package com.localvalu.home.repository

import com.localvalu.home.api.CuisinesCategoryListApi
import com.localvalu.base.BaseRepository
import com.localvalu.home.model.CuisinesCategoryRequest

class CuisinesCategoryListRepository (private val api: CuisinesCategoryListApi): BaseRepository(api)
{
    suspend fun cuisinesCategoryList(cuisinesCategoryRequest: CuisinesCategoryRequest) = safeApiCall {
        api.cuisinesCategoryList(cuisinesCategoryRequest)
    }
}