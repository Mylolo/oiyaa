package com.localvalu.home.repository

import com.localvalu.home.api.SearchListApi
import com.localvalu.base.BaseRepository
import com.localvalu.common.model.SearchRequest
import com.utility.AppUtils

class SearchListRepository (private val api: SearchListApi) : BaseRepository(api)
{
    suspend fun search(searchRequest:SearchRequest,actionMode:String) = safeApiCall {
        if(actionMode==AppUtils.TEXT_SEARCH_MODE) api.search(searchRequest)
        else api.searchFilter(searchRequest)
    }
}