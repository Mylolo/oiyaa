package com.localvalu.home.repository

import com.localvalu.base.BaseRepository
import com.localvalu.home.api.HomeListApi
import com.localvalu.home.model.HomePageListRequest

class HomeListRepository (private val api: HomeListApi) : BaseRepository(api)
{
    suspend fun homeList(homePageRequest: HomePageListRequest) = safeApiCall {
        api.homeList(homePageRequest)
    }
}