package com.localvalu.home.di

import android.content.Context
import com.localvalu.home.api.CuisinesCategoryListApi
import com.localvalu.base.RemoteDataSource
import com.localvalu.home.repository.CuisinesCategoryListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class CuisinesCategoryListModule
{
    @Provides
    fun providesCuisinesCategoryListApi(remoteDataSource: RemoteDataSource,
                            @ApplicationContext context: Context): CuisinesCategoryListApi
    {
        return remoteDataSource.buildApi(CuisinesCategoryListApi::class.java,context)
    }
    @Provides
    fun providesSearchListRepository(cuisinesCategoryListApi: CuisinesCategoryListApi):CuisinesCategoryListRepository
    {
        return CuisinesCategoryListRepository(cuisinesCategoryListApi)
    }
}