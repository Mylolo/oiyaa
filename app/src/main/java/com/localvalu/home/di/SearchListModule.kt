package com.localvalu.home.di

import android.content.Context
import com.localvalu.home.api.SearchListApi
import com.localvalu.base.RemoteDataSource
import com.localvalu.home.repository.SearchListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module()
@InstallIn(SingletonComponent::class)
class SearchListModule
{
    @Provides
    fun providesSearchListApi(remoteDataSource: RemoteDataSource,
                            @ApplicationContext context: Context): SearchListApi
    {
        return remoteDataSource.buildApi(SearchListApi::class.java,context)
    }

    @Provides
    fun providesSearchListRepository(searchListApi: SearchListApi):SearchListRepository
    {
        return SearchListRepository(searchListApi)
    }
}