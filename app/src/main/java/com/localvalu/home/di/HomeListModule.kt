package com.localvalu.home.di

import android.content.Context
import com.localvalu.base.RemoteDataSource
import com.localvalu.home.api.HomeListApi
import com.localvalu.home.repository.HomeListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module()
@InstallIn(SingletonComponent::class)
class HomeListModule
{
    @Provides
    fun providesHomeListApi(remoteDataSource: RemoteDataSource,
                            @ApplicationContext context: Context): HomeListApi
    {
        return remoteDataSource.buildApi(HomeListApi::class.java,context)
    }

    @Provides
    fun providesHomeListRepository(api: HomeListApi): HomeListRepository
    {
        return HomeListRepository(api)
    }
}