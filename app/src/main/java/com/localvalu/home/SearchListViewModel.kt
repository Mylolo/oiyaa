package com.localvalu.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.base.Resource
import com.localvalu.common.model.SearchRequest
import com.localvalu.common.model.SearchResponse
import com.localvalu.home.repository.SearchListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchListViewModel @Inject constructor(private val repository:SearchListRepository) : ViewModel()
{
    private val _searchList: MutableLiveData<Resource<SearchResponse>> = MutableLiveData()
    val searchList: LiveData<Resource<SearchResponse>>
        get() = _searchList

    fun search(searchRequest: SearchRequest,actionMode:String) = viewModelScope.launch {
        _searchList.value = Resource.Loading
        _searchList.value = repository.search(searchRequest,actionMode)
    }

}