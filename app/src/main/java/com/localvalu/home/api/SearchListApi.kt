package com.localvalu.home.api

import com.localvalu.base.BaseApi
import com.localvalu.common.model.SearchRequest
import com.localvalu.common.model.SearchResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface SearchListApi : BaseApi
{
    @POST("Websitesearch/search")
    suspend fun search(@Body searchRequest: SearchRequest?): SearchResponse

    @POST("Websitesearch/Cuisinesearch")
    suspend fun searchFilter(@Body searchRequest: SearchRequest?):SearchResponse
}