package com.localvalu.home.api

import com.localvalu.base.BaseApi
import com.localvalu.directory.listing.wrapper.DashboardListResultWrapper
import com.localvalu.home.model.HomePageListRequest
import retrofit2.http.Body
import retrofit2.http.POST

interface HomeListApi:BaseApi
{
    @POST("Websitesearch/GetWebsiteComplete")
    suspend fun homeList(@Body homePageListRequest: HomePageListRequest): DashboardListResultWrapper
}