package com.localvalu.home.api

import com.localvalu.base.BaseApi
import com.localvalu.common.model.cuisinescategory.CuisinesCategoryListResponse
import com.localvalu.home.model.CuisinesCategoryRequest
import retrofit2.http.Body
import retrofit2.http.POST

interface CuisinesCategoryListApi : BaseApi
{
    @POST("Websitesearch/cuisinesList")
    suspend fun cuisinesCategoryList(@Body cuisinesCategoryRequest: CuisinesCategoryRequest): CuisinesCategoryListResponse
}