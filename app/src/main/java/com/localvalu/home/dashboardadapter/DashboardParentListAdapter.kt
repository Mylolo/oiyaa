package com.localvalu.home.dashboardadapter

import com.localvalu.directory.listing.dto.HomeListParent
import com.localvalu.home.dashboardadapter.DashboardParentListAdapter.DashboardListParentViewHolder
import com.localvalu.eats.listing.adapter.EatsHomeChildClickListener
import com.localvalu.common.listeners.CategoryClicksListener
import com.localvalu.eats.listing.adapter.EatsHomeParentClickListener
import com.localvalu.R
import android.annotation.SuppressLint
import android.content.Context
import android.view.ViewGroup
import android.view.LayoutInflater
import com.utility.GridSpacingItemDecoration
import androidx.core.content.ContextCompat
import com.localvalu.common.CategoryAdapter
import com.utility.AppUtils
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.button.MaterialButton
import androidx.constraintlayout.widget.ConstraintLayout
import com.localvalu.directory.listing.dto.BusinessListDto
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.*
import com.localvalu.common.model.cuisinescategory.CuisineCategoryData
import com.utility.HorizontalSpacingItemDecoration
import java.util.ArrayList

class DashboardParentListAdapter : ListAdapter<HomeListParent, DashboardListParentViewHolder>(DIFF_CALLBACK), DashboardChildClickListener, CategoryClicksListener
{
    private val data: ArrayList<HomeListParent>? = null
    private var context: Context? = null
    private var parentClickListener: DashboardParentClickListener? = null
    private var childClickListener: DashboardChildClickListener? = null
    private var categoryClicksListener: CategoryClicksListener? = null
    private var horizontalSpace = 0
    private var type:Int=AppUtils.BMT_EATS;

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context
        horizontalSpace = context!!.getResources().getDimension(R.dimen._10sdp).toInt()
    }

    @SuppressLint("LongLogTag")
    override fun getItemViewType(position: Int): Int
    {
        if (position < itemCount)
        {
            Log.d(TAG, "getItemViewType->size-> $itemCount, pos->$position")
            return when (getItem(position)!!.viewType)
            {
                VIEW_TYPE_BUSINESS -> VIEW_TYPE_BUSINESS
                VIEW_TYPE_FILTERS -> VIEW_TYPE_FILTERS
                else -> -1
            }
        }
        return -1
    }

    fun setType(type:Int)
    {
        this.type = type
    }

    fun setParentClickListener(parentClickListener: DashboardParentClickListener?)
    {
        this.parentClickListener = parentClickListener
    }

    fun setChildClickListener(childClickListener: DashboardChildClickListener?)
    {
        this.childClickListener = childClickListener
    }

    fun setCategoryClicksListener(categoryClicksListener: CategoryClicksListener?)
    {
        this.categoryClicksListener = categoryClicksListener
    }

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        position: Int
    ): DashboardListParentViewHolder
    {
        when (getItemViewType(position))
        {
            VIEW_TYPE_BUSINESS, VIEW_TYPE_FILTERS ->
            {
                val view = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.view_item_home_list_parent, viewGroup, false)
                return DashboardListParentViewHolder(view)
            }
        }
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.view_item_home_list_parent, viewGroup, false)
        return DashboardListParentViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: DashboardListParentViewHolder, position: Int)
    {
        val homeListParent = getItem(position)
        viewHolder.tvHeading.text = homeListParent.strHeader
        viewHolder.btnViewAll.visibility = View.VISIBLE
        when (getItemViewType(position))
        {
            VIEW_TYPE_BUSINESS -> if (homeListParent!!.businessList != null)
            {
                if (homeListParent.businessList.size > 0)
                {
                    val dashboardParentListAdapter = DashboardChildListAdapter(itemWidth)
                    viewHolder.rvHomeListChild.apply {
                        layoutManager  = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
                        adapter = dashboardParentListAdapter
                        val space = context?.resources?.getDimension(R.dimen._5sdp)?.toInt()
                        viewHolder.rvHomeListChild.addItemDecoration(HorizontalSpacingItemDecoration(space ?:0))
                        /*addItemDecoration(
                                GridSpacingItemDecoration(
                                2,
                                horizontalSpace,
                                false
                        ))*/
                    }
                    dashboardParentListAdapter.setType(type)
                    dashboardParentListAdapter.setChildClickListener(this)
                    dashboardParentListAdapter.submitList(homeListParent.businessList)
                    viewHolder.tvEmptyListDashboard.visibility = View.GONE
                    viewHolder.cnlDashboardItem.visibility = View.VISIBLE
                    viewHolder.rvHomeListChild.invalidate()
                }
                else
                {
                    viewHolder.tvEmptyListDashboard.visibility = View.VISIBLE
                    viewHolder.cnlDashboardItem.visibility = View.GONE
                    when (homeListParent.headerId)
                    {
                        2 -> viewHolder.tvEmptyListDashboard.text =
                            context!!.getString(R.string.lbl_no_nearby_restaurants_found)
                        3 -> viewHolder.tvEmptyListDashboard.text =
                            context!!.getString(R.string.lbl_no_top_rated_restaurants_found)
                        4 -> viewHolder.tvEmptyListDashboard.text =
                            context!!.getString(R.string.lbl_no_popular_business_found)
                    }
                }
            }
            else
            {
                viewHolder.tvEmptyListDashboard.visibility = View.VISIBLE
                viewHolder.tvEmptyListDashboard.setTextColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.colorLocal
                    )
                )
                viewHolder.cnlDashboardItem.visibility = View.GONE
                when (homeListParent.headerId)
                {
                    2 -> viewHolder.tvEmptyListDashboard.text =
                        context!!.getString(R.string.lbl_no_nearby_restaurants_found)
                    3 -> viewHolder.tvEmptyListDashboard.text =
                        context!!.getString(R.string.lbl_no_top_rated_restaurants_found)
                    4 -> viewHolder.tvEmptyListDashboard.text =
                        context!!.getString(R.string.lbl_no_popular_restaurants_found)
                }
            }
            VIEW_TYPE_FILTERS -> if (homeListParent!!.cuisinesCategoryDataArrayList != null)
            {
                if (homeListParent.cuisinesCategoryDataArrayList.size > 0)
                {
                    val categoryAdapter = CategoryAdapter(
                        homeListParent.cuisinesCategoryDataArrayList,type
                    )
                    viewHolder.rvHomeListChild.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    viewHolder.rvHomeListChild.adapter = categoryAdapter
                    categoryAdapter.setOnCategoryClicksListener(this)
                    viewHolder.tvEmptyListDashboard.visibility = View.GONE
                    viewHolder.cnlDashboardItem.visibility = View.VISIBLE
                }
                else
                {
                    viewHolder.tvEmptyListDashboard.visibility = View.VISIBLE
                    viewHolder.cnlDashboardItem.visibility = View.GONE
                    when (homeListParent.headerId)
                    {
                        1 -> viewHolder.tvEmptyListDashboard.text =
                            context!!.getString(R.string.lbl_no_cuisines_found)
                    }
                }
            }
            else
            {
                viewHolder.tvEmptyListDashboard.visibility = View.VISIBLE
                viewHolder.tvEmptyListDashboard.setTextColor(
                    ContextCompat.getColor(
                        context!!,
                        R.color.colorLocal
                    )
                )
                viewHolder.cnlDashboardItem.visibility = View.GONE
                when (homeListParent.headerId)
                {
                    1 -> viewHolder.tvEmptyListDashboard.text =
                        context!!.getString(R.string.lbl_no_cuisines_found)
                }
            }
        }
    }

    fun removeItem(position: Int)
    {
        data!!.removeAt(position)
        notifyItemRemoved(position)
    }

    inner class DashboardListParentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        var tvHeading: AppCompatTextView = itemView.findViewById(R.id.tvHeading)
        var tvEmptyListDashboard: AppCompatTextView = itemView.findViewById(R.id.tvEmptyListDashboard)
        var btnViewAll: MaterialButton = itemView.findViewById(R.id.btnViewAll)
        var rvHomeListChild: RecyclerView = itemView.findViewById(R.id.rvHomeListChild)
        var cnlDashboardItem: ConstraintLayout = itemView.findViewById(R.id.cnlDashboardItem)
        init
        {
            btnViewAll.setOnClickListener{
                when(getItemViewType(layoutPosition))
                {
                    VIEW_TYPE_BUSINESS->
                    {
                        parentClickListener?.onViewAllButtonClicked(getItem(layoutPosition))
                    }
                    VIEW_TYPE_FILTERS->
                    {
                        parentClickListener?.onViewAllButtonClicked(getItem(layoutPosition))
                    }
                }
            }
        }
    }

    private val itemWidth: Int
        private get()
        {
            var displayMetrics = DisplayMetrics()
            displayMetrics = context!!.resources.displayMetrics
            val width = displayMetrics.widthPixels
            return (width / 2.25).toInt()
        }

    override fun onCategoryClicked(cuisineCategoryData: CuisineCategoryData, navigationType: Int)
    {
        if (categoryClicksListener != null)
        {
            categoryClicksListener!!.onCategoryClicked(cuisineCategoryData, navigationType)
        }
    }

    companion object
    {
        private const val TAG = "DashboardParentListAdapter"
        const val VIEW_TYPE_BUSINESS = 1
        const val VIEW_TYPE_FILTERS = 2
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<HomeListParent> =
            object : DiffUtil.ItemCallback<HomeListParent>()
            {
                override fun areItemsTheSame(
                    oldItem: HomeListParent,
                    newItem: HomeListParent
                ): Boolean
                {
                    return oldItem.headerId == newItem.headerId
                }

                override fun areContentsTheSame(
                    oldItem: HomeListParent,
                    newItem: HomeListParent
                ): Boolean
                {
                    return Integer.toString(oldItem.headerId) == Integer.toString(newItem.headerId)
                }
            }
    }

    override fun onItemClicked(type: Int,businessListDto: BusinessListDto, childPosition: Int)
    {
        if (childClickListener != null)
        {
            childClickListener!!.onItemClicked(type,businessListDto, childPosition)
        }
    }

}