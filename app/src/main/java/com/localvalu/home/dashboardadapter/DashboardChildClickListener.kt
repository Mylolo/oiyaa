package com.localvalu.home.dashboardadapter

import com.localvalu.directory.listing.dto.BusinessListDto

interface DashboardChildClickListener
{
    fun onItemClicked(type:Int,businessListDto: BusinessListDto, childPosition: Int)
}