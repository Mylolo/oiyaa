package com.localvalu.home.dashboardadapter

import com.localvalu.directory.listing.dto.HomeListParent

interface DashboardParentClickListener
{
    fun onViewAllButtonClicked(homeListParent: HomeListParent);
}