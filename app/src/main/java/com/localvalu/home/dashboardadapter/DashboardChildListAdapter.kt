package com.localvalu.home.dashboardadapter

import com.localvalu.directory.listing.dto.BusinessListDto
import com.localvalu.home.dashboardadapter.DashboardChildListAdapter.DashboardChildViewHolder
import androidx.recyclerview.widget.RecyclerView
import com.localvalu.R
import android.view.ViewGroup
import android.view.LayoutInflater
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PorterDuff
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.cardview.widget.CardView
import com.utility.AppUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import java.lang.StringBuilder
import java.util.ArrayList

class DashboardChildListAdapter(private val itemWidth: Int) :ListAdapter<BusinessListDto, DashboardChildViewHolder>(DIFF_CALLBACK)
{
    private val data: ArrayList<BusinessListDto>? = null
    private var context: Context? = null
    private var childClickListener: DashboardChildClickListener? = null
    private var colorBlack = 0
    private var type:Int = AppUtils.BMT_EATS
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context
        colorBlack = context!!.getResources().getColor(R.color.colorBlack)
    }

    fun setChildClickListener(childClickListener: DashboardChildClickListener?)
    {
        this.childClickListener = childClickListener
    }

    fun setType(type:Int)
    {
        this.type = type;
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): DashboardChildViewHolder
    {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.view_item_home_list_child, viewGroup, false)
        return DashboardChildViewHolder(view)
    }

    override fun onBindViewHolder(
        viewHolder: DashboardChildViewHolder,
        @SuppressLint("RecyclerView") position: Int
    )
    {
        val businessListDto = getItem(viewHolder.adapterPosition)
        viewHolder.ivOrderOnline.drawable.setColorFilter(colorBlack, PorterDuff.Mode.SRC_ATOP)
        viewHolder.ivOrderAtTable.drawable.setColorFilter(colorBlack, PorterDuff.Mode.SRC_ATOP)
        viewHolder.ivBusinessLogo.setImageDrawable(context!!.resources.getDrawable(R.drawable.app_logo))
        viewHolder.cvEatsHomeChildListRoot.layoutParams =
            getCardViewLayoutParams(viewHolder.cvEatsHomeChildListRoot)
        var currentPromotionValue: String? = "0.0"
        if (businessListDto!!.currentPromotionValue != "")
        {
            currentPromotionValue = businessListDto.currentPromotionValue
        }
        val strCurrentPromotionValue = String.format("%.1f", java.lang.Float.valueOf(currentPromotionValue))
        if (strCurrentPromotionValue != "0.0")
        {
            viewHolder.tvPercentage.visibility = View.VISIBLE
            viewHolder.tvPercentage.text = businessListDto.currentPromotionValue + "%"
        }
        else
        {
            viewHolder.tvPercentage.visibility = View.GONE
        }
        viewHolder.tvPercentage.background = ContextCompat.getDrawable((context)!!, R.drawable.top_left_rounded_background_local)
        viewHolder.tvBusinessName.text = businessListDto.tradingname
        viewHolder.tvAddress.text = businessListDto.address1 + "," + businessListDto.country
        viewHolder.tvDeliveryTime.text = "Delivery Time - 35 Mins"
        val reviewsCount = businessListDto.overAllreviewCount
        val totalReviews = StringBuilder()
        totalReviews.append(reviewsCount).append(" ")
            .append(context!!.getString(R.string.lbl_reviews))
        viewHolder.tvReviews.text = totalReviews.toString()
        val orderOnlineAvailable = isOrderNow(businessListDto.bookingTypeId)
        val orderAtTableAvailable = isOrderAtTable(businessListDto.bookingTypeId)
        if (orderOnlineAvailable) viewHolder.ivOrderOnline.visibility =
            View.VISIBLE else viewHolder.ivOrderOnline.visibility = View.INVISIBLE
        if (orderAtTableAvailable) viewHolder.ivOrderAtTable.visibility =
            View.VISIBLE else viewHolder.ivOrderAtTable.visibility = View.INVISIBLE

        if(type==AppUtils.BMT_LOCAL)
        {
            val orderAtStoreAvailable: Boolean = isOrderAtStore(businessListDto.bookingTypeId)
            if (orderAtStoreAvailable) viewHolder.ivOrderAtStore.visibility =
                View.VISIBLE else viewHolder.ivOrderAtStore.visibility =
                View.INVISIBLE
        }

        if (businessListDto.getLogo("eats").trim { it <= ' ' } != "")
        {
            Picasso.with(context).load(businessListDto.getLogo("eats"))
                .placeholder(R.drawable.progress_animation).error(R.drawable.app_logo)
                .into(viewHolder.ivBusinessLogo)
            viewHolder.ivBusinessLogo.scaleType = ImageView.ScaleType.CENTER_CROP
            viewHolder.ivBusinessLogo.setBackgroundColor(context!!.resources.getColor(R.color.colorWhite))
        }
        else
        {
            viewHolder.ivBusinessLogo.setImageResource(R.drawable.app_logo)
            viewHolder.ivBusinessLogo.scaleType = ImageView.ScaleType.CENTER_INSIDE
            viewHolder.ivBusinessLogo.setBackgroundColor(context!!.resources.getColor(R.color.colorEats))
        }
        viewHolder.itemView.setOnClickListener(View.OnClickListener {
            if (childClickListener != null)
            {
                childClickListener!!.onItemClicked(type,(businessListDto), viewHolder.adapterPosition)
            }
        })
    }

    fun removeItem(position: Int)
    {
        data!!.removeAt(position)
        notifyItemRemoved(position)
    }

    inner class DashboardChildViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        var ivBusinessLogo: AppCompatImageView = itemView.findViewById(R.id.ivBusinessLogo)
        var tvPercentage: AppCompatTextView = itemView.findViewById(R.id.tvPercentage)
        var tvBusinessName: AppCompatTextView = itemView.findViewById(R.id.tvBusinessName)
        var tvReviews: AppCompatTextView = itemView.findViewById(R.id.tvReviews)
        var tvDeliveryTime: AppCompatTextView = itemView.findViewById(R.id.tvDeliveryTime)
        var tvAvailableOptions: AppCompatTextView = itemView.findViewById(R.id.tvAvailableOptions)
        var tvAddress: AppCompatTextView = itemView.findViewById(R.id.tvAddress)
        var cvEatsHomeChildListRoot: CardView = itemView.findViewById(R.id.cvEatsHomeChildListRoot)
        var ivOrderOnline: AppCompatImageView = itemView.findViewById(R.id.ivOrderOnline)
        var ivOrderAtTable: AppCompatImageView = itemView.findViewById(R.id.ivOrderAtTable)
        var ivOrderAtStore: AppCompatImageView = itemView.findViewById(R.id.ivOrderAtStore)

        init
        {
            ivOrderOnline.visibility = View.GONE
            ivOrderAtTable.visibility = View.GONE
            ivOrderAtStore.visibility = View.GONE
        }
    }

    private fun isOrderNow(bookingTypeId: String): Boolean
    {
        return bookingTypeId.contains(AppUtils.ORDER_ONLINE)
    }

    private fun isOrderAtTable(bookingTypeId: String): Boolean
    {
        return bookingTypeId.contains(AppUtils.ORDER_AT_TABLE)
    }

    private fun isOrderAtStore(bookingTypeId: String): Boolean
    {
        return bookingTypeId.contains(AppUtils.ORDER_AT_STORE)
    }

    private fun getCardViewLayoutParams(cardView: CardView): RecyclerView.LayoutParams
    {
        val layoutParams = cardView.layoutParams as RecyclerView.LayoutParams
        layoutParams.width = itemWidth
        return layoutParams
    }

    private fun setAvailableOptionsText(
        textView: AppCompatTextView,
        businessListDto: BusinessListDto
    )
    {
        val strOrderOnlineAvailableOptions = StringBuilder()
        val orderOnlineAvailable = isOrderNow(businessListDto.bookingTypeId)
        val orderAtTableAvailable = isOrderAtTable(businessListDto.bookingTypeId)
        if (orderOnlineAvailable)
        {
            strOrderOnlineAvailableOptions.append(context!!.getString(R.string.lbl_order_online))
                .append(" : ").append(
                context!!.getString(R.string.lbl_available)
            ).append('\n')
        } else
        {
            strOrderOnlineAvailableOptions.append(context!!.getString(R.string.lbl_order_online))
                .append(" : ").append(
                context!!.getString(R.string.lbl_unavailable)
            ).append('\n')
        }
        if (orderAtTableAvailable)
        {
            strOrderOnlineAvailableOptions.append(context!!.getString(R.string.lbl_order_at_table))
                .append(" : ").append(
                context!!.getString(R.string.lbl_available)
            )
        } else
        {
            strOrderOnlineAvailableOptions.append(context!!.getString(R.string.lbl_order_at_table))
                .append(" : ").append(
                context!!.getString(R.string.lbl_unavailable)
            )
        }
        textView.text = strOrderOnlineAvailableOptions.toString()
    }

    companion object
    {
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<BusinessListDto> =
            object : DiffUtil.ItemCallback<BusinessListDto>()
            {
                override fun areItemsTheSame(
                    oldItem: BusinessListDto,
                    newItem: BusinessListDto
                ): Boolean
                {
                    return oldItem.businessId === newItem.businessId
                }

                override fun areContentsTheSame(
                    oldItem: BusinessListDto,
                    newItem: BusinessListDto
                ): Boolean
                {
                    return (oldItem.businessId == newItem.businessId)
                }
            }
    }
}