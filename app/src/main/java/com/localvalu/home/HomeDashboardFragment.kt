package com.localvalu.home

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.localvalu.base.Resource
import com.localvalu.common.SearchFragment
import com.localvalu.common.listeners.CategoryClicksListener
import com.localvalu.common.model.cuisinescategory.CuisineCategoryData
import com.localvalu.databinding.FragmentEatsDashboardBinding
import com.localvalu.directory.listing.dto.BusinessListDto
import com.localvalu.directory.listing.dto.HomeListParent
import com.localvalu.eats.detail.EatsDetailFragment
import com.localvalu.eats.listing.model.BusinessViewAll
import com.localvalu.home.dashboardadapter.DashboardParentListAdapter
import com.localvalu.home.viewmodel.HomeListViewModel
import com.utility.AppUtils
import com.utility.AppUtils.VIEW_TYPE_BUSINESS
import com.utility.VerticalSpacingItemDecoration
import dagger.hilt.android.AndroidEntryPoint
import com.localvalu.R
import com.localvalu.base.HostActivityListener
import com.localvalu.base.LocationViewModel
import com.localvalu.directory.detail.DirectoryDetailFragment
import com.localvalu.directory.listing.DirectoryFilterSearchFragment
import com.localvalu.directory.listing.DirectoryListingFragment
import com.localvalu.eats.listing.EatsFilterSearchFragment
import com.localvalu.eats.listing.EatsListingFragment
import com.localvalu.home.dashboardadapter.DashboardChildClickListener
import com.localvalu.home.dashboardadapter.DashboardParentClickListener

@AndroidEntryPoint
class HomeDashboardFragment : Fragment(), DashboardParentClickListener, DashboardChildClickListener,CategoryClicksListener
{
    private var mLocationPermissionsGranted: Boolean = false
    private val TAG = "HomeDashboardFragment"
    private lateinit var binding:FragmentEatsDashboardBinding
    private lateinit var listAdapter: DashboardParentListAdapter
    private val verticalSpace = 0
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    private val viewModel by viewModels<HomeListViewModel>()
    private val locationViewModel by activityViewModels<LocationViewModel>()
    private var type = AppUtils.BMT_EATS;
    private val _fineLocation = Manifest.permission.ACCESS_FINE_LOCATION
    private val _coarseLocation = Manifest.permission.ACCESS_COARSE_LOCATION
    private var gotLocation:Boolean = false
    private lateinit var hostActivityListener:HostActivityListener;

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        hostActivityListener = context as HostActivityListener
    }

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentEatsDashboardBinding.inflate(inflater,container,false)
        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
    }

    private fun readFromBundle()
    {
        arguments?.let {
            type = it.getInt(AppUtils.BUNDLE_NAVIGATION_TYPE)
        }
    }

    private fun setUpActionBar()
    {
        val typeface = ResourcesCompat.getFont(requireContext(), R.font.comfortaa_regular)
        val themeColor = ContextCompat.getColor(requireContext(), R.color.colorEats)
        binding.toolbarLayout.toolbar.setNavigationIcon(R.drawable.ic_home_profile)
        binding.toolbarLayout.toolbar.navigationIcon!!.setColorFilter(
            themeColor,
            PorterDuff.Mode.SRC_ATOP
        )
        binding.toolbarLayout.toolbar.setNavigationOnClickListener {
            hostActivityListener.onMenuClicked()
        }
        val currentHintTextColor = ContextCompat.getColor(requireContext(), R.color.colorHintText)
        binding.toolbarLayout.tilSearch.endIconDrawable!!
            .setColorFilter(currentHintTextColor, PorterDuff.Mode.SRC_ATOP)
        binding.toolbarLayout.tilSearch.startIconDrawable!!
            .setColorFilter(currentHintTextColor, PorterDuff.Mode.SRC_ATOP)
        binding.toolbarLayout.etSearch.setOnClickListener(onClickListener)
    }

    private fun setProperties()
    {
        readFromBundle()
        listAdapter = DashboardParentListAdapter()
        listAdapter.setType(type)
        setUpActionBar()
        binding.rvEatsHome.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = listAdapter
            val verticalSpacingItemDecoration = VerticalSpacingItemDecoration(verticalSpace)
            addItemDecoration(verticalSpacingItemDecoration)
        }
        listAdapter.setParentClickListener(this)
        listAdapter.setChildClickListener(this)
        listAdapter.setCategoryClicksListener(this)
        binding.layoutError.btnRetry.setOnClickListener(onClickListener)
        binding.toolbarLayout.etSearch.setOnClickListener(onClickListener)
        observeSubscribers()
        getLocationPermission()
    }

    private fun observeSubscribers()
    {
        viewModel.homeListItemsLiveData.observe(viewLifecycleOwner, Observer {
            when(it)
            {
                is Resource.Loading->
                {
                    showProgress()
                }
                is Resource.Success->
                {
                    showList()
                    if (it.value != null)
                    {
                        if (it.value.isNotEmpty())
                        {
                            updateAdapter(it.value)
                        }
                    }
                }
                is Resource.Failure->
                {
                    showError(getString(R.string.network_unavailable),false)
                }
            }
        })
        locationViewModel.location.observe(viewLifecycleOwner, Observer { location->
            this.latitude= location.latitude
            this.longitude=location.longitude
            if(!gotLocation)
            {
                viewModel.getHomeListItems(type,location.latitude, location.longitude)
                gotLocation=true
            }
        })
    }

    var onClickListener =
        View.OnClickListener { v -> onButtonClick(v) }

    fun onButtonClick(view: View)
    {
        when (view.id)
        {
             R.id.etSearch->
             {
                 hostActivityListener.updateFragment(SearchFragment.newInstance(type,
                     AppUtils.TEXT_SEARCH_MODE, ""),
                     true,
                     SearchFragment::class.simpleName,true);
             }
        }
    }

    private fun updateAdapter(homeListParent: List<HomeListParent>)
    {
        listAdapter?.submitList(homeListParent)
    }

    override fun onDestroy()
    {
        super.onDestroy()
    }

    private fun showProgress()
    {
        binding.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding.cnlMain.visibility = View.GONE
        binding.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun showList()
    {
        binding.layoutProgress.layoutRoot.visibility = View.GONE
        binding.cnlMain.visibility = View.VISIBLE
        binding.layoutError.layoutRoot.visibility = View.GONE
    }

    private fun getLocationPermission()
    {
        Log.d(TAG, "getLocationPermission: getting location permissions")
        val permissions = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )
        if (ContextCompat.checkSelfPermission(requireContext(), _fineLocation) == PackageManager.PERMISSION_GRANTED)
        {
            if (ContextCompat.checkSelfPermission(requireContext(),_coarseLocation) == PackageManager.PERMISSION_GRANTED)
            {
                Log.d(TAG,"Already have permissions")
                mLocationPermissionsGranted = true
                locationViewModel.startRequest()
            }
            else
            {
                Log.d(TAG,"Ask for permissions")
                locationPermissions.launch(permissions)
            }
        }
        else
        {
            Log.d(TAG,"Ask for permissions")
            locationPermissions.launch(permissions)
        }
    }

    private val locationPermissions = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()){ permissions->
        var permissionDenied = false
        permissions.forEach { actionMap ->
            when (actionMap.key)
            {
                _fineLocation ->
                {
                    if (!actionMap.value)
                    {
                        permissionDenied = true
                    }
                }
                _coarseLocation ->
                {
                    if (!actionMap.value)
                    {
                        permissionDenied = true
                    }
                }
            }
        }
        if(permissionDenied)
        {
            viewModel.getHomeListItems(type,latitude,longitude)
        }
        else
        {
            locationViewModel.startRequest()
        }
    }

    private fun showError(strMessage: String, retry: Boolean)
    {
        binding.layoutProgress.layoutRoot.visibility = View.GONE
        binding.cnlMain.visibility = View.GONE
        binding.layoutError.layoutRoot.visibility = View.VISIBLE
        binding.layoutError.tvError.text = strMessage
        if (retry) binding.layoutError.btnRetry.visibility =
            View.VISIBLE else binding.layoutError.btnRetry.visibility =
            View.GONE
    }

    override fun onItemClicked(type:Int,businessListDto: BusinessListDto, childPosition: Int)
    {
        val bundle = Bundle()
        bundle.putString("businessID", businessListDto.businessId)
        when(type)
        {
            AppUtils.BMT_EATS->
            {
                val eatsDetailFragment = EatsDetailFragment.newInstance()
                eatsDetailFragment.arguments = bundle
                hostActivityListener.updateFragment(eatsDetailFragment,
                    true,eatsDetailFragment.javaClass.name,true)
            }
            AppUtils.BMT_LOCAL->
            {
                val directoryDetailFragment = DirectoryDetailFragment.newInstance()
                directoryDetailFragment.arguments = bundle
                hostActivityListener.updateFragment(directoryDetailFragment,
                    true,directoryDetailFragment.javaClass.name,true)

            }
        }

    }

    override fun onViewAllButtonClicked(homeListParent: HomeListParent)
    {
        Log.d(TAG,"onViewAllButtonClicked")
        when (homeListParent.viewType)
        {
            VIEW_TYPE_BUSINESS         ->
            {
                val businessViewAll = BusinessViewAll()
                businessViewAll.latitude = latitude
                businessViewAll.longitude = longitude
                businessViewAll.header = homeListParent.strHeader
                businessViewAll.businessList=homeListParent.businessList
                businessViewAll.viewResultType = homeListParent.viewTypeResult
                businessViewAll.isFromAllFilter = false
                when(type)
                {
                    AppUtils.BMT_EATS->
                    {
                        val eatsListingFragment = EatsListingFragment.newInstance(null, businessViewAll)
                        hostActivityListener.updateFragment(
                            eatsListingFragment, true,
                            EatsListingFragment::class.java.name, true)
                    }
                    AppUtils.BMT_LOCAL->
                    {
                        val directoryFilterSearchFragment = DirectoryListingFragment.newInstance(null, businessViewAll)
                        hostActivityListener.updateFragment(
                            directoryFilterSearchFragment, true,
                            EatsListingFragment::class.java.name, true)
                    }
                }

            }
            AppUtils.VIEW_TYPE_FILTERS->
            {
                when(type)
                {
                    AppUtils.BMT_EATS  ->
                    {
                        val eatsFilterSearchFragment = EatsFilterSearchFragment.newInstance()
                        hostActivityListener.updateFragment(
                            eatsFilterSearchFragment, true,
                            EatsFilterSearchFragment::class.java.name, true
                        )
                    }
                    AppUtils.BMT_LOCAL ->
                    {
                        val directoryFilterSearchFragment =
                            DirectoryFilterSearchFragment.newInstance()
                        hostActivityListener.updateFragment(
                            directoryFilterSearchFragment, true,
                            DirectoryFilterSearchFragment::class.java.name, true
                        )
                    }
                }

            }
        }
    }

    override fun onCategoryClicked(cuisineCategoryData: CuisineCategoryData, navigationType: Int)
    {
        val searchFragment = SearchFragment.newInstance(
            navigationType,
            AppUtils.FILTER_SEARCH_MODE, cuisineCategoryData.name
        )
        hostActivityListener.updateFragment(
            searchFragment, true,
            SearchFragment::class.java.name, true
        )
    }

    override fun onConfigurationChanged(newConfig: Configuration)
    {
        super.onConfigurationChanged(newConfig)
        Log.d(TAG,"onConfigurationChanged")
    }
}