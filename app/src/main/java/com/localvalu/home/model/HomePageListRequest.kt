package com.localvalu.home.model

import com.google.gson.annotations.SerializedName

data class HomePageListRequest(@SerializedName("token")var token:String,
                               @SerializedName("type") val type:Int,
                               @SerializedName("requestfor") val requestFor:Int,
                               @SerializedName("Latitude") val latitude:Double,
                               @SerializedName("Longitude") val longitude:Double)
{
    init
    {
        token ="/3+YFd5QZdSK9EKsB8+TlA=="
    }
}