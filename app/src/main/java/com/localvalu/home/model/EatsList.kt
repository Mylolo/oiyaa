package com.localvalu.home.model

import com.google.gson.annotations.SerializedName

data class EatsList(@SerializedName("token") private var token:String,
                    @SerializedName("type") private val type:Int,
                    @SerializedName("requestfor") private val requestFor:Int,
                    @SerializedName("Latitude") private val latitude:Double,
                    @SerializedName("Longitude") private val Longitude:Double)
{
    init
    {
        token = "/3+YFd5QZdSK9EKsB8+TlA==";
    }
}
