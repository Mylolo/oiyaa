package com.localvalu.home.viewmodel

import android.content.Context
import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.localvalu.R
import com.localvalu.base.Resource
import com.localvalu.directory.listing.dto.HomeListParent
import com.localvalu.home.model.CuisinesCategoryRequest
import com.localvalu.home.model.HomePageListRequest
import com.localvalu.home.repository.CuisinesCategoryListRepository
import com.localvalu.home.repository.HomeListRepository
import com.localvalu.locationservices.LocationRepository
import com.utility.AppUtils
import com.utility.AppUtils.VIEW_TYPE_BUSINESS
import com.utility.AppUtils.VIEW_TYPE_FILTERS
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeListViewModel @Inject constructor(@ApplicationContext val context: Context,
    private val homeListRepository: HomeListRepository,
    private val cuisinesCategoryListRepository: CuisinesCategoryListRepository
) : ViewModel()
{
    private val _homeListItemsLiveData = MutableLiveData<Resource<List<HomeListParent>>>()
    val homeListItemsLiveData: LiveData<Resource<List<HomeListParent>>>
        get() = _homeListItemsLiveData
    val locationLiveData:LiveData<Location> = LocationRepository.locationUpdate

    fun getHomeListItems(type:Int,
                         latitude:Double,
                         longitude:Double) = viewModelScope.launch {

        val nearBy= HomePageListRequest("",type,AppUtils.NEARBY,latitude, longitude)
        val popular= HomePageListRequest("",type,AppUtils.POPULAR,latitude, longitude)
        _homeListItemsLiveData.postValue(Resource.Loading)
        val cuisinesCategoryList = async { cuisinesCategoryListRepository
            .cuisinesCategoryList(CuisinesCategoryRequest("",type)) }
        val nearByDeferred = async { homeListRepository.homeList(nearBy) }
        val popularDeferred = async { homeListRepository.homeList(popular) }

        val cuisinesCategoryListResult = cuisinesCategoryList.await()
        val nearByResult = nearByDeferred.await()
        val popularResult = popularDeferred.await()

        val homeItemsList = mutableListOf<HomeListParent>()

        if (cuisinesCategoryListResult is Resource.Success)
        {
            if (cuisinesCategoryListResult.value.cuisinesCategoryListResult.errorDetails.errorCode == 0L)
            {
                val homeListCuisinesCategory = HomeListParent()
                homeListCuisinesCategory.headerId == 1
                if(type==AppUtils.BMT_EATS)
                {
                    homeListCuisinesCategory.strHeader = context.getString(R.string.lbl_your_cuisines)
                    homeListCuisinesCategory.viewTypeResult = AppUtils.CUISINES
                }
                else
                {
                    homeListCuisinesCategory.strHeader = context.getString(R.string.lbl_your_categories)
                    homeListCuisinesCategory.viewTypeResult = AppUtils.CATEGORY
                }

                homeListCuisinesCategory.cuisinesCategoryDataArrayList =
                    cuisinesCategoryListResult.value.cuisinesCategoryListResult.cuisinesCategoryListResults
                homeListCuisinesCategory.viewType = VIEW_TYPE_FILTERS
                homeItemsList.add(homeListCuisinesCategory)
            }
        }

        if (nearByResult is Resource.Success)
        {
            if (nearByResult.value.getWebsiteComplete.errorDetails.errorCode == 0)
            {
                val homeListParentNearBy = HomeListParent()
                homeListParentNearBy.apply {
                    headerId == 2
                    strHeader = if(type==AppUtils.BMT_EATS) context.getString(R.string.lbl_nearby_restaurants)
                    else context.getString(R.string.lbl_nearby_business)
                    businessList = nearByResult.value.getWebsiteComplete.businessListDto
                    viewType= VIEW_TYPE_BUSINESS
                    viewTypeResult = AppUtils.NEARBY
                }
                homeItemsList.add(homeListParentNearBy)
            }
        }

        if (popularResult is Resource.Success)
        {
            if (popularResult.value.getWebsiteComplete.errorDetails.errorCode == 0)
            {
                val homeListParentPopular = HomeListParent()
                homeListParentPopular.apply {
                    headerId == 3
                    strHeader = if(type==AppUtils.BMT_EATS) context.getString(R.string.lbl_popular_restaurants)
                    else context.getString(R.string.lbl_popular_business)
                    businessList = popularResult.value.getWebsiteComplete.businessListDto
                    viewType= VIEW_TYPE_BUSINESS
                    viewTypeResult = AppUtils.NEARBY
                }
                homeItemsList.add(homeListParentPopular)
            }
        }

        _homeListItemsLiveData.postValue(Resource.Success(homeItemsList))
    }

    fun startService()
    {
        LocationRepository.startService(context)
    }

    fun stopService()
    {
        LocationRepository.stopService(context)
    }

}