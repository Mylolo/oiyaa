package com.localvalu.di

import android.content.Context
import androidx.room.Room
import com.google.gson.Gson
import com.localvalu.base.LocalDataSource
import com.localvalu.base.OiyaaApplication
import com.localvalu.base.RemoteDataSource
import com.localvalu.db.AppDatabase
import com.localvalu.storage.UserPreferences
import com.localvalu.user.model.User
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule
{

    @Provides
    fun provideApplication(application: OiyaaApplication): OiyaaApplication = application

    @Provides
    @Singleton
    fun providesPreferences(@ApplicationContext context: Context) : UserPreferences
    {
        return UserPreferences(context)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return Gson()
    }

    @Singleton
    @Provides
    fun providesRemoteDataSource():RemoteDataSource
    {
        return RemoteDataSource()
    }

    @Singleton
    @Provides
    fun providesLocalDataSource():LocalDataSource
    {
        return LocalDataSource()
    }

}