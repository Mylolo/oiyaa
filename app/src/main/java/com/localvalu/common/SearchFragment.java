package com.localvalu.common;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.common.model.ErrorDetails;
import com.localvalu.common.model.SearchCompleteResult;
import com.localvalu.common.model.SearchRequest;
import com.localvalu.common.model.SearchResponse;
import com.localvalu.common.repositories.OiyaaSearch;
import com.localvalu.databinding.FragmentSearchBinding;
import com.localvalu.directory.detail.DirectoryDetailFragment;
import com.localvalu.directory.listing.adapter.DirectoryBusinessListAdapter;
import com.localvalu.directory.listing.dto.BusinessListDto;
import com.localvalu.eats.detail.EatsDetailFragment;
import com.localvalu.eats.listing.adapter.EatsBusinessListAdapter;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.VerticalSpacingItemDecoration;

import java.util.ArrayList;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

public class SearchFragment extends BaseFragment
{
    private static final String TAG = "SearchFragment";

    private FragmentSearchBinding binding;
    private EatsBusinessListAdapter eatsBusinessListAdapter;
    private DirectoryBusinessListAdapter directoryBusinessListAdapter;
    private ArrayList<BusinessListDto> businessList = new ArrayList<>();
    private HostActivityListener hostActivityListener;
    private Typeface typeface;
    private int verticalItemSpace;
    private int navigationType;
    private String actionMode;
    private String strSearch;

    private OiyaaSearch oiyaaSearch;
    private int colorGray;
    private int themeColor;
    private boolean addToBackStack=false;

    public static SearchFragment newInstance(int navigationType,String searchActionMode, String strSearch)
    {
        SearchFragment searchFragment = new SearchFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, navigationType);
        bundle.putString(AppUtils.BUNDLE_SEARCH_ACTION_MODE,searchActionMode);
        bundle.putString(AppUtils.BUNDLE_SEARCH_STRING,strSearch);
        searchFragment.setArguments(bundle);
        return searchFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");
        typeface = ResourcesCompat.getFont(requireContext(), R.font.comfortaa_regular);
        colorGray = ContextCompat.getColor(requireContext(),R.color.colorLightGray);
        readFromBundle();
        oiyaaSearch = OiyaaSearch.getInstance();
        verticalItemSpace = (int) getResources().getDimension(R.dimen._10sdp);
    }

    private void readFromBundle()
    {
        navigationType = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
        actionMode = getArguments().getString(AppUtils.BUNDLE_SEARCH_ACTION_MODE);
        strSearch = getArguments().getString(AppUtils.BUNDLE_SEARCH_STRING);
        switch (navigationType)
        {
            case AppUtils.BMT_EATS:
            case AppUtils.BMT_LOCAL:
                 themeColor = ContextCompat.getColor(requireContext(),R.color.colorEats);
                 break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentSearchBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: ");
        setUpActionBar();
        setProperties();
    }

    private void setUpActionBar()
    {
        int themeColor = ContextCompat.getColor(requireContext(), R.color.colorEats);
        binding.toolbarSearch.toolbar.setNavigationIcon(R.drawable.ic_app_arrow);
        binding.toolbarSearch.toolbar.getNavigationIcon().setColorFilter(themeColor, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarSearch.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });
    }

    private void setProperties()
    {
        switch (actionMode)
        {
            case AppUtils.TEXT_SEARCH_MODE:
                 showTextSearchUI();
                 addToBackStack=true;
                 break;
            case AppUtils.FILTER_SEARCH_MODE:
                 showFilterSearchUI();
                 addToBackStack=false;
                 break;
        }
        binding.rvSearch.setLayoutManager(new LinearLayoutManager(requireContext(),LinearLayoutManager.VERTICAL,false));
        binding.rvSearch.addItemDecoration(new VerticalSpacingItemDecoration(verticalItemSpace));
        binding.layoutError.btnRetry.setOnClickListener(_OnClickListener);
        switch (navigationType)
        {
            case AppUtils.BMT_EATS:
                 eatsBusinessListAdapter = new EatsBusinessListAdapter();
                 eatsBusinessListAdapter.setOnClickListener(new EatsBusinessListAdapter.OnItemClickListener()
                 {
                    @Override
                    public void onItemCall(final int position,final BusinessListDto businessListDto)
                    {
                        Log.d(TAG, "Phone : " + businessListDto.contact_person_phone);
                        if (!businessListDto.contact_person_phone.equals(""))
                        {
                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                            callIntent.setData(Uri.parse("tel:" + businessListDto.contact_person_phone));
                            startActivity(callIntent);
                        }
                    }

                    @Override
                    public void onItemClick(int position,final BusinessListDto businessListDto)
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString("businessID", businessListDto.businessId);
                        EatsDetailFragment eatsDetailFragment = EatsDetailFragment.newInstance();
                        eatsDetailFragment.setArguments(bundle);
                        hostActivityListener.updateFragment(eatsDetailFragment, true, EatsDetailFragment.class.getName(), true);
                    }
                });
                 binding.rvSearch.setAdapter(eatsBusinessListAdapter);
                 break;
            case AppUtils.BMT_LOCAL:
                 directoryBusinessListAdapter = new DirectoryBusinessListAdapter();
                 directoryBusinessListAdapter.setOnClickListener(new DirectoryBusinessListAdapter.OnItemClickListener()
                 {
                    @Override
                    public void onItemCall(final int position,final BusinessListDto businessListDto)
                    {
                        Log.d(TAG, "Phone : " + businessListDto.contact_person_phone);
                        if (!businessListDto.contact_person_phone.equals(""))
                        {
                            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                            callIntent.setData(Uri.parse("tel:" +businessListDto.contact_person_phone));
                            startActivity(callIntent);
                        }
                    }

                    @Override
                    public void onItemClick(final int position,final BusinessListDto businessListDto)
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString("businessID", businessListDto.businessId);
                        DirectoryDetailFragment directoryDetailFragment = DirectoryDetailFragment.newInstance();
                        directoryDetailFragment.setArguments(bundle);
                        hostActivityListener.updateFragment(directoryDetailFragment, true, DirectoryDetailFragment.class.getName(), true);
                    }
                });
                 binding.rvSearch.setAdapter(directoryBusinessListAdapter);
                 break;
        }

    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.btnRetry:
                     search();
                     break;
            }
        }
    };

    private void showTextSearchUI()
    {
        int currentHintTextColor = ContextCompat.getColor(requireContext(),R.color.colorHintText);
        binding.toolbarSearch.tilSearch.getEndIconDrawable().setColorFilter(currentHintTextColor, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarSearch.tilSearch.getStartIconDrawable().setColorFilter(currentHintTextColor, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarSearch.etSearch.setMaxLines(1);
        binding.toolbarSearch.etSearch.setImeOptions(EditorInfo.IME_ACTION_DONE);
        binding.toolbarSearch.etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView textView, int action, KeyEvent keyEvent)
            {
                switch (action)
                {
                    case EditorInfo.IME_ACTION_DONE:
                        search();
                        break;
                }
                return false;
            }
        });
        binding.toolbarSearch.etSearch.requestFocus();
        showKeyboard();
    }

    private void showFilterSearchUI()
    {
        binding.toolbarSearch.tilSearch.setVisibility(View.GONE);
        binding.toolbarSearch.viewSeparator.setVisibility(View.GONE);
        binding.toolbarSearch.toolbar.setTitle(strSearch);
        binding.toolbarSearch.toolbar.setTitleTextColor(themeColor);
        search();
    }

    public SearchRequest getSearchApiPayload()
    {
        SearchRequest searchRequest = new SearchRequest();

        switch (actionMode)
        {
            case AppUtils.FILTER_SEARCH_MODE:
                 searchRequest.setCuisine(strSearch);
                 searchRequest.setType(navigationType);
                 break;
            case AppUtils.TEXT_SEARCH_MODE:
                 searchRequest.setBusiness(binding.toolbarSearch.etSearch.getText().toString());
                 searchRequest.setTypeId(navigationType);
                 break;
        }
        return searchRequest;
    }

    public void search()
    {
        hideKeyBoard();
        showProgress();
        binding.toolbarSearch.etSearch.clearFocus();
        binding.rvSearch.requestFocus();
        oiyaaSearch.search(getSearchApiPayload(),actionMode)
                .subscribe(new SingleObserver<SearchResponse>()
                {
                    @Override
                    public void onSubscribe(Disposable d)
                    {

                    }

                    @Override
                    public void onSuccess(SearchResponse searchResponse)
                    {
                        SearchCompleteResult searchCompleteResult = searchResponse.getSearchCompleteResult();

                        ErrorDetails errorDetails = searchCompleteResult.getErrorDetails();

                        if (errorDetails.getErrorCode() == 0)
                        {

                            showList();

                            if(searchCompleteResult.getSearchResultDetails()!=null)
                            {
                                ArrayList<BusinessListDto> searchResultDetails = searchCompleteResult.getSearchResultDetails();

                                if(searchResultDetails.size()>0)
                                {
                                    switch (navigationType)
                                    {
                                        case AppUtils.BMT_EATS:
                                             eatsBusinessListAdapter.submitList(searchResultDetails);
                                             break;
                                        case AppUtils.BMT_LOCAL:
                                             directoryBusinessListAdapter.submitList(searchResultDetails);
                                             break;
                                    }
                                }
                            }
                        }
                        else
                        {
                           showError(getString(R.string.lbl_coming_soon),false);
                        }
                    }

                    @Override
                    public void onError(Throwable e)
                    {
                        if(!CommonUtilities.checkConnectivity(requireContext()))
                        {
                            showError(getString(R.string.network_unavailable),true);
                        }
                        else
                        {
                            showError(getString(R.string.server_alert),true);
                        }
                    }
                });

    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                 getActivity().onBackPressed();
                 break;
            case R.id.btn_next:
                 search();//if(isValid())search();
                 break;
        }
    }

    public void showKeyboard()
    {
        InputMethodManager inputMethodManager = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private void hideKeyBoard()
    {
        InputMethodManager imm =
                (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(binding.toolbarSearch.etSearch.getWindowToken(), 0);
    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.rvSearch.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showList()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvSearch.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage, boolean retry)
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvSearch.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if (retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }


}
