package com.localvalu.common.review;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.appbar.AppBarLayout;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentReviewListBinding;
import com.localvalu.directory.scanpay.dto.BusinessDetail;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.directory.scanpay.dto.BusinessDetailResult;
import com.localvalu.directory.scanpay.wrapper.BusinessDetailResultWrapper;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.squareup.picasso.Picasso;
import com.utility.AppUtils;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewsListFragment extends BaseFragment
{
    private static final String TAG = "ReviewsListFragment";

    private int colorEats;
    private String businessId;
    private HostActivityListener hostActivityListener;
    private FragmentReviewListBinding binding;
    private int navigationType;
    private UserBasicInfo userBasicInfo;
    private BusinessDetail businessDetail;
    private BusinessDetailInfo businessDetailInfo;
    private ApiInterface apiInterface;
    private ViewPagerAdapter adapter;

    public static ReviewsListFragment newInstance()
    {
        ReviewsListFragment fragment = new ReviewsListFragment();
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");

        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentReviewListBinding.inflate(inflater,container,false);
        readFromBundle();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        binding.tabPagerHeading.setupWithViewPager(binding.viewPagerLists);
        setUpActionBar();
    }

    private void readFromBundle()
    {
        businessId = getArguments().getString(AppUtils.BUNDLE_BUSINESS_ID);
        navigationType = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
        fetchBusinessInfo(businessId);
    }

    /**
     * Get the business information from server
     *
     * @param businessId
     */
    private void fetchBusinessInfo(String businessId)
    {

        JSONObject objMain = new JSONObject();

        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("BusinessId", businessId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        showProgress();
        Call<BusinessDetailResultWrapper> dtoCall = apiInterface.businessDetailFrom(body);
        dtoCall.enqueue(new Callback<BusinessDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<BusinessDetailResultWrapper> call, Response<BusinessDetailResultWrapper> response)
            {

                try
                {
                    if (response.isSuccessful())
                    {
                        BusinessDetailResultWrapper businessDetailResultWrapper = response.body();

                        if (businessDetailResultWrapper != null)
                        {
                            BusinessDetailResult businessDetailResult = businessDetailResultWrapper.businessDetailResult;

                            if (businessDetailResult.errorDetails.errorCode == 0)
                            {
                                businessDetail = businessDetailResult.businessDetails;

                                if (businessDetail != null)
                                {
                                    businessDetailInfo = businessDetail.businessDetails.get(0);

                                    if (businessDetailInfo != null)
                                    {
                                        showReviewList();
                                        setBasicInfo();
                                        setupViewPager(binding.viewPagerLists);
                                    }
                                }
                            }

                            else
                            {
                                showError(businessDetailResult.errorDetails.errorMessage, false);
                            }
                        }
                    }
                    else
                    {
                        showError(getString(R.string.server_alert),false);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<BusinessDetailResultWrapper> call, Throwable t)
            {
                Log.d(TAG, t.getMessage());
                if(isAdded())
                {
                    if(t instanceof IOException) showError(getString(R.string.network_unavailable),true);
                    else showError(getString(R.string.server_alert),false);
                }
            }
        });

    }

    /**
     * Set Basic Image
     * @param
     */
    public void setBasicInfo()
    {
        if (!businessDetailInfo.logo.trim().equals(""))
            Picasso.with(getActivity()).load(businessDetailInfo.logo).placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo ).into(binding.ivBusinessLogo);
        else
            binding.ivBusinessLogo.setImageResource(R.drawable.app_logo );
        binding.tvBusinessName.setText(businessDetailInfo.tradingname);
        binding.tvType.setText(businessDetailInfo.businessType);
        binding.tvAddress.setText(getAddress());

    }

    private String getAddress()
    {
        StringBuilder strAddress = new StringBuilder();
        if (businessDetailInfo.address1.isEmpty())
        {
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        else
        {
            strAddress.append(businessDetailInfo.address1.trim());
            strAddress.append(" ");
            if(!businessDetailInfo.address2.trim().isEmpty())
            {
                strAddress.append(businessDetailInfo.address2.trim());
                strAddress.append(" ");
            }
        }
        if (!businessDetailInfo.city.isEmpty())
        {
            strAddress.append(businessDetailInfo.city.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.country.isEmpty())
        {
            strAddress.append(businessDetailInfo.country.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.postcode.isEmpty())
        {
            strAddress.append(businessDetailInfo.postcode.trim());
        }
        return strAddress.toString();
    }

    private void setUpActionBar()
    {

        binding.tvTitle.setTextColor(colorEats);
        binding.toolbar.getNavigationIcon().setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });
        binding.appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener()
        {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset)
            {
                if (Math.abs(verticalOffset)-appBarLayout.getTotalScrollRange() == 0)
                {
                    //Collapsed
                    adjustUIList(false);
                }
                else
                {
                    //Expanded
                    adjustUIList(true);
                }
            }
        });
    }

    private Bundle getBundle(int type)
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppUtils.BUNDLE_BUSINESS_DETAIL_INFO,businessDetailInfo);
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, navigationType);
        bundle.putInt(AppUtils.BUNDLE_REVIEW_TYPE,type);
        return bundle;
    }

    private void setupViewPager(ViewPager viewPager)
    {
        adapter = new ViewPagerAdapter(getChildFragmentManager());

        ReviewsFragment myReviewsFragment = new ReviewsFragment();
        myReviewsFragment.setArguments(getBundle(AppUtils.REVIEW_TYPE_MY));

        ReviewsFragment allReviewsFragment = new ReviewsFragment();
        allReviewsFragment.setArguments(getBundle(AppUtils.REVIEW_TYPE_ALL));

        adapter.addFragment(myReviewsFragment, getString(R.string.lbl_my_reviews));
        adapter.addFragment(allReviewsFragment, getString(R.string.lbl_all_reviews));
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {

            }

            @Override
            public void onPageSelected(int position)
            {
                switch (position)
                {
                    case 0:
                         showSnackbar();
                         break;
                    case 1:
                         removeSnackbar();
                         break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });
    }

    private void removeSnackbar()
    {
        if(adapter!=null && adapter.getCount()>0)
        {
            Fragment fragment = adapter.getItem(0);
            if(fragment instanceof ReviewsFragment)
            {
                ReviewsFragment reviewsFragment = (ReviewsFragment) fragment;
                reviewsFragment.removeSnackbar();
            }
        }
    }

    public void showSnackbar()
    {
        if(binding.viewPagerLists.getCurrentItem()==0)
        {
            if(adapter!=null && adapter.getCount()>0)
            {
                Fragment fragment = adapter.getItem(0);
                if(fragment instanceof ReviewsFragment)
                {
                    ReviewsFragment reviewsFragment = (ReviewsFragment) fragment;
                    reviewsFragment.showSnackbar();
                }
            }
        }
    }

    public void adjustUIList(boolean expanded)
    {
        if(adapter!=null && adapter.getCount()>0)
        {
            Fragment fragment = adapter.getItem(binding.viewPagerLists.getCurrentItem());
            if(fragment instanceof ReviewsFragment)
            {
                ReviewsFragment reviewsFragment = (ReviewsFragment) fragment;
                reviewsFragment.setProgressErrorViewMargins(expanded);
            }
        }
    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.viewPagerLists.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showReviewList()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.viewPagerLists.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage, boolean retry)
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.viewPagerLists.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if (retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager)
        {
            super(manager);
        }

        @Override
        public Fragment getItem(int position)
        {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount()
        {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title)
        {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return mFragmentTitleList.get(position);
        }
    }

}
