package com.localvalu.common.review.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.review.dto.ReviewPostResult;

import java.io.Serializable;

public class ReviewPostResultWrapper implements Serializable {

    @SerializedName("ReviewResults")
    @Expose
    public ReviewPostResult reviewPostResult;
}
