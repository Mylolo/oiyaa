package com.localvalu.common.review.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseTokens
{
    @SerializedName("ReviewTokens")
    @Expose
    public String reviewTokens;

    @SerializedName("Currentbalance")
    @Expose
    public String Currentbalance;
}
