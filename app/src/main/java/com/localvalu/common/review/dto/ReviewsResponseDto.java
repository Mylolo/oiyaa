package com.localvalu.common.review.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ReviewsResponseDto implements Serializable {

    @SerializedName("MyReviewDetails")
    @Expose
    public List<ReviewData> myReviewDetails;

    @SerializedName("AllReviewDetails")
    @Expose
    public List<ReviewData> allReviewDetails;

    @SerializedName("particularReviewsResult")
    @Expose
    public List<ReviewData> particularReviewsResult;

    @SerializedName("particularReviewsCommentResult")
    @Expose
    public List<ReviewData> particularReviewsCommentResult;

    @SerializedName("ResponseTokens")
    @Expose
    public ResponseTokens responseTokens;

}
