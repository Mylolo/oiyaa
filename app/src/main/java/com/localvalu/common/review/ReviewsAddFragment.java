package com.localvalu.common.review;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.common.review.wrapper.ReviewPostResultWrapper;
import com.localvalu.databinding.FragmentReviewAddBinding;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.squareup.picasso.Picasso;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReviewsAddFragment extends BaseFragment
{
    private static final String TAG = "ReviewsAddFragment";

    private FragmentReviewAddBinding binding;
    private int colorEats;
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private HostActivityListener hostActivityListener;
    private BusinessDetailInfo businessDetailInfo;
    private int navigationType;

    // TODO: Rename and change types and number of parameters
    public static ReviewsAddFragment newInstance()
    {
        return new ReviewsAddFragment();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate: ");
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        buildObjectForHandlingAPI();
        readFromBundle();
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding =FragmentReviewAddBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
    }

    private void readFromBundle()
    {
        businessDetailInfo = (BusinessDetailInfo) getArguments().getSerializable(AppUtils.BUNDLE_BUSINESS_DETAIL_INFO);
        navigationType = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
    }

    private void setProperties()
    {

        if (!businessDetailInfo.logo.trim().equals(""))
            Picasso.with(getActivity()).load(businessDetailInfo.logo).placeholder(R.drawable.progress_animation)
                    .error(R.drawable.app_logo ).into(binding.ivBusinessLogo);
        else
            binding.ivBusinessLogo.setImageResource(R.drawable.app_logo );
        binding.tvBusinessName.setText(businessDetailInfo.tradingname);
        binding.tvType.setText(businessDetailInfo.businessType);
        binding.tvAddress.setText(getAddress());

        binding.btnSubmit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isValidForReviewPost())
                {
                    callAPIForReviewPost();
                }
            }
        });
        binding.etComment.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                // TODO Auto-generated method stub
                int length = binding.etComment.length();
                String convert = String.valueOf(length);
                binding.tvCount.setText("[" + convert + "/1200]");
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                // TODO Auto-generated method stub
            }
        });
    }

    private void setUpActionBar()
    {
        binding.tvTitle.setTextColor(colorEats);
        binding.toolbar.getNavigationIcon().setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private String getAddress()
    {
        StringBuilder strAddress = new StringBuilder();
        if (businessDetailInfo.address1.isEmpty())
        {
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        else
        {
            strAddress.append(businessDetailInfo.address1.trim());
            strAddress.append(" ");
            if(!businessDetailInfo.address2.trim().isEmpty())
            {
                strAddress.append(businessDetailInfo.address2.trim());
                strAddress.append(" ");
            }
        }
        if (!businessDetailInfo.city.isEmpty())
        {
            strAddress.append(businessDetailInfo.city.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.country.isEmpty())
        {
            strAddress.append(businessDetailInfo.country.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.postcode.isEmpty())
        {
            strAddress.append(businessDetailInfo.postcode.trim());
        }
        return strAddress.toString();
    }

    private boolean isValidForReviewPost()
    {
        if (binding.rbRatingProduct.getRating() == 0)
        {
            Toast.makeText(getActivity(), "Please give rating of product.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (binding.rbRatingService.getRating() == 0)
        {
            Toast.makeText(getActivity(), "Please give rating of service.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (binding.etComment.getText().toString().isEmpty())
        {
            Toast.makeText(getActivity(), "Please write your review.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (binding.etComment.getText().toString().length() > 1200)
        {
            Toast.makeText(getActivity(), "Review must be up to 1200 characters long.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!CommonUtilities.checkConnectivity(getActivity()))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.network_unavailable), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void callAPIForReviewPost()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId", businessDetailInfo.business_id);
            objMain.put("order_id", businessDetailInfo.orderId);
            objMain.put("reviewComments", binding.etComment.getText().toString());
            objMain.put("sectionType", businessDetailInfo.sectionType);
            objMain.put("productRate", binding.rbRatingProduct.getRating());
            objMain.put("serviceRate", binding.rbRatingService.getRating());
            objMain.put("requestFor", "PostReview");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<ReviewPostResultWrapper> dtoCall = apiInterface.review(body);
        dtoCall.enqueue(new Callback<ReviewPostResultWrapper>()
        {
            @Override
            public void onResponse(Call<ReviewPostResultWrapper> call, Response<ReviewPostResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().reviewPostResult.errorDetails.errorCode == 0)
                    {
                        //callAPI();
                        Bundle bundle = new Bundle();
                        bundle.putString(AppUtils.BUNDLE_NEW_TOKEN_BALANCE, "" +  response.body().reviewPostResult.reviewsResponse.responseTokens.Currentbalance);
                        bundle.putString(AppUtils.BUNDLE_REVIEW_TOKENS, "" +  response.body().reviewPostResult.reviewsResponse.responseTokens.reviewTokens);
                        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,navigationType);
                        bundle.putString(AppUtils.BUNDLE_BUSINESS_ID,businessDetailInfo.business_id);
                        gotoFragmentReviewSuccess(bundle);
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().reviewPostResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<ReviewPostResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk("No Transaction for this retailer", getActivity());
//                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void gotoFragmentReviewSuccess(Bundle bundle)
    {
        ReviewSuccessFragment reviewSuccessFragment = new ReviewSuccessFragment();
        reviewSuccessFragment.setArguments(bundle);
        //hostActivityListener.updateFragment(reviewSuccessFragment,true,
                //reviewSuccessFragment.getClass().getName(),false);
        reviewSuccessFragment.show(getChildFragmentManager(),reviewSuccessFragment.getClass().getName());
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }

}
