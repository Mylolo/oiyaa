package com.localvalu.common.review;

import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.material.snackbar.Snackbar;
import com.localvalu.R;
import com.localvalu.base.MainActivity;
import com.localvalu.common.review.dto.ReviewData;
import com.localvalu.common.review.wrapper.ReviewPostResultWrapper;
import com.localvalu.common.review.adapter.ReviewsListAdapter;
import com.localvalu.databinding.AddReviewCustomSnackbarBinding;
import com.localvalu.databinding.FragmentReviewsBinding;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReviewsFragment extends Fragment
{
    private static final String TAG = "ReviewsFragment";


    private GridLayoutManager gridLayoutManager;
    private ReviewsListAdapter eatsReviewsListAdapter;
    private FragmentReviewsBinding binding;

    private UserBasicInfo userBasicInfo;
    private ApiInterface apiInterface;

    private BusinessDetailInfo businessDetailInfo;
    private int navigationType;
    private int reviewType;
    private Snackbar snackbar;

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentReviewsBinding.inflate(inflater,container,false);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        buildObjectForHandlingAPI();
        readFromBundle();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setProperties();
    }

    private void readFromBundle()
    {
        businessDetailInfo = (BusinessDetailInfo) getArguments().getSerializable(AppUtils.BUNDLE_BUSINESS_DETAIL_INFO);
        navigationType  = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
        reviewType = getArguments().getInt(AppUtils.BUNDLE_REVIEW_TYPE);
        fetchAPI(businessDetailInfo.business_id);
    }

    private void setProperties()
    {
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        binding.rvReviews.setLayoutManager(gridLayoutManager);
        showSnackbar();
    }

    private int getActionBarSize()
    {
        // Calculate ActionBar height
        int actionBarHeight = (int) getResources().getDimension(R.dimen._48sdp);
        TypedValue tv = new TypedValue();
        if (requireActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }

    private void gotoAddReviewFragment()
    {
        final Bundle bundle = new Bundle();
        businessDetailInfo.sectionType="Info";
        bundle.putSerializable(AppUtils.BUNDLE_BUSINESS_DETAIL_INFO,businessDetailInfo);
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,navigationType);
        ReviewsAddFragment reviewsAddFragment = ReviewsAddFragment.newInstance();
        reviewsAddFragment.setArguments(bundle);
        ((MainActivity) getActivity()).addFragment(reviewsAddFragment, true, ReviewsAddFragment.class.getName(), true);
    }

    private void fetchAPI(String retailerId)
    {
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId", retailerId);
            if(reviewType==AppUtils.REVIEW_TYPE_MY)
            {
                objMain.put("requestFor", "Myreviews");
            }
            else
            {
                objMain.put("requestFor", "AllReviews");
            }

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        showProgress();
        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<ReviewPostResultWrapper> dtoCall = apiInterface.review(body);
        dtoCall.enqueue(new Callback<ReviewPostResultWrapper>()
        {
            @Override
            public void onResponse(Call<ReviewPostResultWrapper> call, Response<ReviewPostResultWrapper> response)
            {
                try
                {
                    if (response.body().reviewPostResult.errorDetails.errorCode == 0)
                    {

                        if(reviewType==AppUtils.REVIEW_TYPE_MY)
                        {
                            if (response.body().reviewPostResult.reviewsResponse.myReviewDetails != null &&
                                    response.body().reviewPostResult.reviewsResponse.myReviewDetails.size() > 0)
                            {
                                showReviewList();
                                setAdapter(response.body().reviewPostResult.reviewsResponse.myReviewDetails);
                            }
                            else
                            {
                                showError(getString(R.string.lbl_no_reviews_found),false);
                            }
                        }
                        else
                        {
                            if (response.body().reviewPostResult.reviewsResponse.allReviewDetails != null &&
                                    response.body().reviewPostResult.reviewsResponse.allReviewDetails.size() > 0)
                            {
                                showReviewList();
                                setAdapter(response.body().reviewPostResult.reviewsResponse.allReviewDetails);
                            }
                            else
                            {
                                showError(getString(R.string.lbl_no_reviews_found),false);
                            }
                        }
                    }
                    else
                    {
                        showError(response.body().reviewPostResult.errorDetails.errorMessage,false);
                    }
                }
                catch (Exception e)
                {
                    showError(getString(R.string.server_alert),false);
                }
            }

            @Override
            public void onFailure(Call<ReviewPostResultWrapper> call, Throwable t)
            {
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    showError(getString(R.string.network_unavailable),true);
                else showError(getString(R.string.server_alert),false);

            }
        });
    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.rvReviews.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showReviewList()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvReviews.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage, boolean retry)
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvReviews.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if (retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

    public void showSnackbarDefault()
    {
        if(reviewType==AppUtils.REVIEW_TYPE_MY)
        {
            if(snackbar==null)
            {
                snackbar = Snackbar.make(getParentFragment().getView(), getString(R.string.lbl_add_review_message), Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.lbl_add_review, new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                if(businessDetailInfo!=null)
                                {
                                    gotoAddReviewFragment();
                                }
                            }
                        });
                setRecyclerViewMargin(getActionBarSize());
            }
            snackbar.show();
        }
    }

    public void showSnackbar()
    {
        if(reviewType==AppUtils.REVIEW_TYPE_MY)
        {
            if(snackbar==null)
            {
                // create an instance of the snackbar
                snackbar = Snackbar.make(getParentFragment().getView(), "", Snackbar.LENGTH_INDEFINITE);
                final AddReviewCustomSnackbarBinding binding = DataBindingUtil.inflate(getLayoutInflater(),
                        R.layout.add_review_custom_snackbar,null,false);
                binding.tvAddReviewMessage.setText(getString(R.string.lbl_add_review_message));
                binding.btnAddReview.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        if(businessDetailInfo!=null)
                        {
                            gotoAddReviewFragment();
                        }
                    }
                });
                // now change the layout of the snackbar
                Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) snackbar.getView();
                // set padding of the all corners as 0
                snackbarLayout.setPadding(0, 0, 0, 0);
                snackbarLayout.addView(binding.getRoot());
                setRecyclerViewMargin(getActionBarSize());
            }
            snackbar.show();
        }
    }

    public void removeSnackbar()
    {
        if(snackbar!=null)
        {
            snackbar.dismiss();
            setRecyclerViewMargin((int)getResources().getDimension(R.dimen._10sdp));
        }
    }

    private void setRecyclerViewMargin(int size)
    {
        binding.rvReviews.post(new Runnable()
        {
            @Override
            public void run()
            {
                ConstraintLayout.LayoutParams layoutParams =(ConstraintLayout.LayoutParams)
                        binding.rvReviews.getLayoutParams();
                layoutParams.bottomMargin = size;
                binding.rvReviews.setLayoutParams(layoutParams);
            }
        });
    }

    public void setProgressErrorViewMargins(boolean expanded)
    {
        final ConstraintLayout.LayoutParams layoutParamsProgress =(ConstraintLayout.LayoutParams)
                binding.layoutProgress.layoutRoot.getLayoutParams();
        final ConstraintLayout.LayoutParams layoutParamsError =(ConstraintLayout.LayoutParams)
                binding.layoutError.layoutRoot.getLayoutParams();

        if(expanded)
        {
            layoutParamsProgress.bottomMargin = getActionBarSize()*4;
            layoutParamsError.bottomMargin = getActionBarSize()*4;
        }
        else
        {
            layoutParamsProgress.bottomMargin = 0;
            layoutParamsError.bottomMargin = 0;
        }
        binding.layoutError.layoutRoot.post(new Runnable()
        {
            @Override
            public void run()
            {
                binding.layoutError.layoutRoot.setLayoutParams(layoutParamsError);
            }
        });
        binding.layoutProgress.layoutRoot.post(new Runnable()
        {
            @Override
            public void run()
            {
                binding.layoutProgress.layoutRoot.setLayoutParams(layoutParamsProgress);
            }
        });
    }

    private void setAdapter(final List<ReviewData> reviewList)
    {
        eatsReviewsListAdapter = new ReviewsListAdapter(getActivity(), new ArrayList<>(reviewList));
        eatsReviewsListAdapter.setOnClickListener(new ReviewsListAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClick(int position)
            {
                ReviewDetailFragment reviewDetailFragment = ReviewDetailFragment.newInstance();
                Bundle bundle = new Bundle();
                bundle.putString("reviewId", reviewList.get(position).reviewId);
                bundle.putString("businessId", reviewList.get(position).businessId);
                reviewDetailFragment.setArguments(bundle);
                ((MainActivity) getActivity()).addFragment(reviewDetailFragment, true, ReviewDetailFragment.class.getName(), true);
            }
        });
        binding.rvReviews.setAdapter(eatsReviewsListAdapter);
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }
}
