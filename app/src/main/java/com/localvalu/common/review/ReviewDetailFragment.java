package com.localvalu.common.review;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.MainActivity;
import com.localvalu.base.HostActivityListener;
import com.localvalu.common.review.dto.CommentsDto;
import com.localvalu.common.review.wrapper.ReviewPostResultWrapper;
import com.localvalu.eats.detail.EatsDetailFragment;
import com.localvalu.common.review.adapter.ReviewCommentsListAdapter;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewDetailFragment extends BaseFragment
{
    private static final String TAG = "ReviewDetailFragment";

    //Toolbar
    private Toolbar toolbar;
    private TextView tvTitle;
    private int colorEats;

    public View view;
    public ArrayList<CommentsDto> comments;
    private RecyclerView rv_lists;
    private GridLayoutManager gridLayoutManager;
    private ReviewCommentsListAdapter reviewCommentsListAdapter;
    private TextView tvUserName, tvReview;
    private AppCompatRatingBar ratingBarProduct, ratingBarService;
    private TextView tv_cnt;
    private EditText et_comment;
    private String reviewId, businessId;
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private Button btnReply;
    private HostActivityListener hostActivityListener;

    // TODO: Rename and change types and number of parameters
    public static ReviewDetailFragment newInstance()
    {
        return new ReviewDetailFragment();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");

        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);

    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_review_detail, container, false);
        buildObjectForHandlingAPI();
        readFromBundle();
        initView();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        fetchAPI(reviewId);
    }

    private void readFromBundle()
    {
        reviewId = getArguments().getString("reviewId");
        businessId = getArguments().getString("businessId");
    }

    private void initView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        tvUserName = view.findViewById(R.id.tvUserName);
        tvReview = view.findViewById(R.id.tvReview);
        ratingBarProduct = view.findViewById(R.id.ratingBarProduct);
        ratingBarService = view.findViewById(R.id.ratingBarService);
        rv_lists = view.findViewById(R.id.rv_lists);
        tv_cnt = view.findViewById(R.id.tv_cnt);
        et_comment = view.findViewById(R.id.et_comment);
        btnReply = view.findViewById(R.id.btnReply);
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        rv_lists.setLayoutManager(gridLayoutManager);
        btnReply.setOnClickListener(_OnClickListener);

        et_comment.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                // TODO Auto-generated method stub
                int length = et_comment.length();
                String convert = String.valueOf(length);
                tv_cnt.setText("[" + convert + "/120]");
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                // TODO Auto-generated method stub
            }
        });
    }

    private void setUpActionBar()
    {
        toolbar = view.findViewById(R.id.toolbar);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setTextColor(colorEats);
        toolbar.getNavigationIcon().setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    private void fetchAPI(String reviewId)
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("ReviewId", reviewId);
            objMain.put("BusinessId", businessId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("orderId", "0");
            objMain.put("reviewComments", et_comment.getText().toString());
            objMain.put("sectionType", "info");
            objMain.put("productRate", ratingBarProduct.getRating());
            objMain.put("serviceRate", ratingBarService.getRating());
            objMain.put("requestFor", "GetParticularReviewComments");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<ReviewPostResultWrapper> dtoCall = apiInterface.review(body);
        dtoCall.enqueue(new Callback<ReviewPostResultWrapper>()
        {
            @Override
            public void onResponse(Call<ReviewPostResultWrapper> call, Response<ReviewPostResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().reviewPostResult.errorDetails.errorCode == 0)
                    {
                        if (response.body().reviewPostResult.status.equalsIgnoreCase("true"))
                        {
                            tvUserName.setText(response.body().reviewPostResult.reviewsResponse.particularReviewsResult.get(0).userName);
                            tvReview.setText(response.body().reviewPostResult.reviewsResponse.particularReviewsResult.get(0).reviewComments);

                            if (response.body().reviewPostResult.reviewsResponse.particularReviewsResult.get(0).ratingTypeId.equals("1,2"))
                            {
                                String[] split = response.body().reviewPostResult.reviewsResponse.particularReviewsResult.get(0).rate.split(",");

                                ratingBarProduct.setRating(Float.parseFloat(split[0]));
                                ratingBarService.setRating(Float.parseFloat(split[1]));
                            }

//                        comments = response.body().reviewPostResult.reviewsResponse.particularReviewsResult;
//                        setAdapter(comments);
                        }
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().reviewPostResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<ReviewPostResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void setAdapter(ArrayList<CommentsDto> comments)
    {
        reviewCommentsListAdapter = new ReviewCommentsListAdapter(getActivity(), comments);
        reviewCommentsListAdapter.setOnClickListener(new ReviewCommentsListAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClick(int position)
            {

            }
        });
        rv_lists.setAdapter(reviewCommentsListAdapter);
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
            case R.id.btnReply:
                if (isValidForCommentPost())
                {
                    callAPIForCommentPost();
                }
                break;
        }
    }

    private boolean isValidForCommentPost()
    {
        if (et_comment.getText().toString().isEmpty())
        {
            Toast.makeText(getActivity(), "Please write your comment.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!CommonUtilities.checkConnectivity(getActivity()))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.network_unavailable), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void callAPIForCommentPost()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("ReviewId", reviewId);
            objMain.put("BusinessId", businessId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("orderId", "0");
            objMain.put("reviewComments", et_comment.getText().toString());
            objMain.put("sectionType", "info");
            objMain.put("productRate", ratingBarProduct.getRating());
            objMain.put("serviceRate", ratingBarService.getRating());
            objMain.put("requestFor", "GetParticularReviewComments");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<ReviewPostResultWrapper> dtoCall = apiInterface.review(body);
        dtoCall.enqueue(new Callback<ReviewPostResultWrapper>()
        {
            @Override
            public void onResponse(Call<ReviewPostResultWrapper> call, Response<ReviewPostResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().reviewPostResult.errorDetails.errorCode == 0)
                    {
                        et_comment.setText("");
                        tv_cnt.setText("[0/120]");

//                    comments.add(response.body().reviewCommentPostResult.comments.get(0));
//                    directoryReviewCommentsListAdapter.notifyDataSetChanged();
//                    directoryReviewCommentsListAdapter.changeData(response.body().reviewCommentPostResult.comments);

                        Bundle bundle = new Bundle();
                        bundle.putString("businessID", businessId);

                        EatsDetailFragment eatsDetailFragment = EatsDetailFragment.newInstance();
                        eatsDetailFragment.setArguments(bundle);
                        ((MainActivity) getActivity()).addFragment(eatsDetailFragment, true, EatsDetailFragment.class.getName(), true);
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().reviewPostResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<ReviewPostResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

}
