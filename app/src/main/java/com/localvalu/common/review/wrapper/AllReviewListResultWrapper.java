package com.localvalu.common.review.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.review.dto.AllReviewListResult;

import java.io.Serializable;

public class AllReviewListResultWrapper implements Serializable {

    @SerializedName("SubmitedAllReviews")
    @Expose
    public AllReviewListResult allReviewListResult;
}
