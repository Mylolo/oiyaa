package com.localvalu.common.review.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReviewData implements Serializable {

    @SerializedName("commentId")
    @Expose
    public String commentId;

    @SerializedName(value = "reviewId", alternate = {"ReviewId"})
    @Expose
    public String reviewId;

    @SerializedName("userId")
    @Expose
    public String userId;

    @SerializedName("userName")
    @Expose
    public String userName;

    @SerializedName("profile_pic")
    @Expose
    public String profile_pic;

    @SerializedName("businessId")
    @Expose
    public String businessId;

    @SerializedName("orderId")
    @Expose
    public String orderId;

    @SerializedName("placeDate")
    @Expose
    public String placeDate;

    @SerializedName(value = "reviewComments", alternate = {"ReviewComment"})
    @Expose
    public String reviewComments;

    @SerializedName("RatingTypeId")
    @Expose
    public String ratingTypeId;

    @SerializedName("rate")
    @Expose
    public String rate;
}
