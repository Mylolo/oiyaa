package com.localvalu.common.review.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.review.dto.MyReviewListResult;

import java.io.Serializable;

public class MyReviewListResultWrapper implements Serializable {

    @SerializedName("SubmitedReviews")
    @Expose
    public MyReviewListResult myReviewListResult;
}
