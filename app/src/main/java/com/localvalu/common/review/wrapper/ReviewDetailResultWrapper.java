package com.localvalu.common.review.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.review.dto.ReviewDetailResult;

import java.io.Serializable;

public class ReviewDetailResultWrapper implements Serializable {

    @SerializedName("ReviewRateDetail")
    @Expose
    public ReviewDetailResult reviewDetailResult;
}
