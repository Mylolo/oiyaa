package com.localvalu.common.review.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.localvalu.R;
import com.localvalu.common.review.dto.ReviewData;
import com.squareup.picasso.Picasso;
import com.utility.AppUtils;
import com.utility.DateUtility;

import java.util.ArrayList;

import at.blogc.android.views.ExpandableTextView;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class ReviewsListAdapter extends RecyclerView.Adapter<ReviewsListAdapter.ViewHolder>
{
    public static final String TAG = ReviewsListAdapter.class.getSimpleName();
    private Activity activity;
    private OnItemClickListener onClickListener;
    private ArrayList<ReviewData> reviewList;

    public ReviewsListAdapter(Activity activity, ArrayList<ReviewData> reviewList)
    {
        this.activity = activity;
        this.reviewList = reviewList;
    }

    @NonNull
    @Override
    public ReviewsListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_review_list, viewGroup, false);
        return new ReviewsListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewsListAdapter.ViewHolder viewHolder, final int i)
    {
        if (!reviewList.get(viewHolder.getAdapterPosition()).profile_pic.trim().equals(""))
            Picasso.with(activity).load(reviewList.get(i).profile_pic).placeholder(R.drawable.progress_animation)
                    .error(R.drawable.noimg_profile).into(viewHolder.iv_user_image);
        else
            viewHolder.iv_user_image.setImageResource(R.drawable.noimg_profile);
        viewHolder.tv_user_name.setText(reviewList.get(viewHolder.getAdapterPosition()).userName);
        if(reviewList.get(i).placeDate!=null)
        {
            String strPlacedDate = reviewList.get(viewHolder.getAdapterPosition()).placeDate;
            if(!strPlacedDate.isEmpty())
            {
                try
                {
                    StringBuilder strReviewdDate = new StringBuilder();
                    strReviewdDate.append(activity.getString(R.string.lbl_on)).append(" ")
                            .append(DateUtility.convertDataToDateCommaMonthYearFormat(strPlacedDate));
                    viewHolder.tv_reviewed_date.setText(strReviewdDate.toString());
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        }

        if (reviewList.get(viewHolder.getAdapterPosition()).ratingTypeId.equals("1,2"))
        {
            String[] split = reviewList.get(i).rate.split(",");

            viewHolder.ratingBarProduct.setRating(Float.parseFloat(split[0]));
            viewHolder.ratingBarService.setRating(Float.parseFloat(split[1]));
        }
        else if(reviewList.get(viewHolder.getAdapterPosition()).ratingTypeId.equals("2,1"))
        {
            String[] split = reviewList.get(i).rate.split(",");

            viewHolder.ratingBarProduct.setRating(Float.parseFloat(split[1]));
            viewHolder.ratingBarService.setRating(Float.parseFloat(split[0]));
        }
        setReviewText(viewHolder, reviewList.get(viewHolder.getAdapterPosition()).reviewComments);
        //viewHolder.tv_review.setText(reviewList.get(i).reviewComments);
        viewHolder.cnl_review_detail.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemClick(viewHolder.getAdapterPosition());
            }
        });
    }

    /**
     * Set Review Content
     *
     * @param strComment
     */
    public void setReviewText(ReviewsListAdapter.ViewHolder viewHolder, String strComment)
    {
        if (strComment != null && (!strComment.isEmpty()))
        {
            viewHolder.tv_review.setText(strComment);
            viewHolder.btnToggleReviewText.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    expandOrCollapseText(viewHolder.tv_review,viewHolder.btnToggleReviewText);
                }
            });
        }
        else viewHolder.tv_review.setText("");

        viewHolder.tv_review.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {

                int lineCount = viewHolder.tv_review.getLineCount();

                // Use lineCount here
                // Drawing happens after layout so we can assume getLineCount() returns the correct value
                if (lineCount > 2)
                {
                    viewHolder.dividerReviewText.setVisibility(View.VISIBLE);
                    viewHolder.btnToggleReviewText.setVisibility(View.VISIBLE);
                    viewHolder.invisibleReviewTextView.setVisibility(View.GONE);
                }
                else
                {
                    viewHolder.dividerReviewText.setVisibility(View.GONE);
                    viewHolder.btnToggleReviewText.setVisibility(View.GONE);
                    viewHolder.invisibleReviewTextView.setVisibility(View.VISIBLE);
                }
            }
        }, 500);
    }

    private void expandOrCollapseText(ExpandableTextView tv_review,Button btnToggleReviewText)
    {
        if (tv_review.isExpanded())
        {
            tv_review.collapse();
            btnToggleReviewText.setText(R.string.lbl_expand);
        }
        else
        {
            tv_review.expand();
            btnToggleReviewText.setText(R.string.lbl_collapse);
        }
    }

    @Override
    public int getItemCount()
    {
        return reviewList.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        ConstraintLayout cnl_review_detail = itemView.findViewById(R.id.cnl_review_detail);
        ImageView iv_user_image = itemView.findViewById(R.id.iv_user_image);
        TextView tv_user_name = itemView.findViewById(R.id.tv_user_name);
        TextView tv_reviewed_date = itemView.findViewById(R.id.tv_reviewed_date);
        AppCompatRatingBar ratingBarProduct = itemView.findViewById(R.id.ratingBarProduct);
        AppCompatRatingBar ratingBarService = itemView.findViewById(R.id.ratingBarService);
        ExpandableTextView tv_review = itemView.findViewById(R.id.tv_review);
        Button btnToggleReviewText = itemView.findViewById(R.id.btnToggleReviewText);
        View invisibleReviewTextView = itemView.findViewById(R.id.invisibleReviewTextView);
        View dividerReviewText = itemView.findViewById(R.id.dividerReviewText);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }
}
