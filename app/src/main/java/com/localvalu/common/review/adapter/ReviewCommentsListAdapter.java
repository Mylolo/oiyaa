package com.localvalu.common.review.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.common.review.dto.CommentsDto;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ReviewCommentsListAdapter extends RecyclerView.Adapter<ReviewCommentsListAdapter.ViewHolder>
{
    public static final String TAG = ReviewCommentsListAdapter.class.getSimpleName();
    private Activity activity;
    private OnItemClickListener onClickListener;
    private ArrayList<CommentsDto> commentsList;

    public ReviewCommentsListAdapter(Activity activity, ArrayList<CommentsDto> commentsList)
    {
        this.activity = activity;
        this.commentsList = commentsList;
    }

    public ReviewCommentsListAdapter(FragmentActivity activity, ArrayList<CommentsDto> comments)
    {
    }

    @NonNull
    @Override
    public ReviewCommentsListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_comment_list, viewGroup, false);
        return new ReviewCommentsListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewCommentsListAdapter.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i)
    {
        if (!commentsList.get(i).image.trim().equals(""))
            Picasso.with(activity).load(commentsList.get(i).image).placeholder(R.drawable.progress_animation)
                    .error(R.drawable.ic_person_black_24dp).into(viewHolder.iv_user_image);
        else
            viewHolder.iv_user_image.setImageResource(R.drawable.ic_person_black_24dp);
        viewHolder.tv_user_name.setText(commentsList.get(i).name);
        viewHolder.tv_post_date.setText(commentsList.get(i).placeDate);
        viewHolder.tv_review.setText(commentsList.get(i).comment);
        viewHolder.ll_review_detail.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onClickListener.onItemClick(i);
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return commentsList.size();
    }

    public void changeData(ArrayList<CommentsDto> comments)
    {
        this.commentsList = comments;
        notifyDataSetChanged();
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        LinearLayout ll_review_detail = itemView.findViewById(R.id.ll_review_detail);
        ImageView iv_user_image = itemView.findViewById(R.id.iv_user_image);
        TextView tv_user_name = itemView.findViewById(R.id.tv_user_name);
        TextView tv_post_date = itemView.findViewById(R.id.tv_post_date);
        TextView tv_review = itemView.findViewById(R.id.tv_review);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }
}
