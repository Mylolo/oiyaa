package com.localvalu.common.review.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.user.dto.ErrorDetailsDto;

import java.io.Serializable;
import java.util.List;

public class ReviewPostResult implements Serializable {

    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("msg")
    @Expose
    public String message;

    @SerializedName("PostReviewResults")
    @Expose
    public ReviewsResponseDto reviewsResponse;
}
