package com.localvalu.common.review.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CommentsDto implements Serializable {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("comment")
    @Expose
    public String comment;
    @SerializedName("place_date")
    @Expose
    public String placeDate;
}
