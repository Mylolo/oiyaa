package com.localvalu.common.review.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReviewListDto implements Serializable {
    @SerializedName("review_id")
    @Expose
    public String reviewId;
    @SerializedName("section_type")
    @Expose
    public String sectionType;
    @SerializedName("user_name")
    @Expose
    public String customerName;
    @SerializedName("image")
    @Expose
    public String customerImage;
    @SerializedName("product_rate")
    @Expose
    public String productRate;
    @SerializedName("service_rate")
    @Expose
    public String serviceRate;
    @SerializedName("review")
    @Expose
    public String review;
    @SerializedName("place_date")
    @Expose
    public String placeDate;
}
