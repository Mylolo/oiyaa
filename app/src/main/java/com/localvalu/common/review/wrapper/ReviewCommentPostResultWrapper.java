package com.localvalu.common.review.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.review.dto.ReviewCommentPostResult;

import java.io.Serializable;

public class ReviewCommentPostResultWrapper implements Serializable
{

    @SerializedName("SubmitReviewComment")
    @Expose
    public ReviewCommentPostResult reviewCommentPostResult;
}
