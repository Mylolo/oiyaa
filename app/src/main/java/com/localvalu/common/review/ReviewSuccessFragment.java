package com.localvalu.common.review;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.localvalu.R;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.PopupSuccessForReviewSubmitBinding;
import com.utility.AppUtils;
import com.utility.CommonUtilities;

public class ReviewSuccessFragment extends DialogFragment
{
    private String currentToken;
    private String reviewTokens;
    private TextView tv_token;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private PopupSuccessForReviewSubmitBinding binding;
    private int navigationType;
    private String businessId;
    private HostActivityListener hostActivityListener;

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        readFromBundle();
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        CommonUtilities.alertSound(requireActivity(), R.raw.coin_drops);
        setStyle(DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_Black_NoTitleBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = PopupSuccessForReviewSubmitBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setProperties();
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null)
        {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    private void readFromBundle()
    {
        final Bundle bundle = getArguments();
        currentToken = bundle.getString(AppUtils.BUNDLE_NEW_TOKEN_BALANCE);
        reviewTokens = bundle.getString(AppUtils.BUNDLE_REVIEW_TOKENS);
        navigationType = bundle.getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
        businessId = bundle.getString(AppUtils.BUNDLE_BUSINESS_ID);
    }

    private void setProperties()
    {
        binding.tvNewLoyaltyBalance.setText(strCurrencyLetter + String.format("%,.2f", (Float.parseFloat(currentToken))));
        binding.tvBonus.setText(strCurrencyLetter + String.format("%,.2f", (Float.parseFloat(reviewTokens))));
        binding.btnCollect.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onButtonClick(v);
            }
        });
    }

    public void onButtonClick(View v)
    {
        switch (v.getId())
        {
            case R.id.iv_close:
            case R.id.btnCollect:
                 dismiss();
                 gotoReviewsFragment();
                 break;
        }
    }

    /**
     * Move to the reviews fragment
     */
    public void gotoReviewsFragment()
    {
        final Bundle bundle = new Bundle();
        System.out.println("businessID :: " + businessId);
        ReviewsListFragment reviewsListFragment = ReviewsListFragment.newInstance();
        bundle.putString(AppUtils.BUNDLE_BUSINESS_ID,businessId);
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,AppUtils.BMT_EATS);
        reviewsListFragment.setArguments(bundle);
        hostActivityListener.updateFragment(reviewsListFragment, true, ReviewsListFragment.class.getName(), true);
    }

}