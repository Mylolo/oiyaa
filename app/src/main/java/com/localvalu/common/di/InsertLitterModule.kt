package com.localvalu.common.di

import android.content.Context
import com.localvalu.base.RemoteDataSource
import com.localvalu.common.api.InsertLitterApi
import com.localvalu.common.repository.InsertLitterRepository
import com.localvalu.registration.api.PassionListApi
import com.localvalu.registration.repository.PassionListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class InsertLitterModule
{
    @Provides
    fun providesInsertLitterApi(remoteDataSource: RemoteDataSource,
                               @ApplicationContext context: Context
    ): InsertLitterApi
    {
        return remoteDataSource.buildApi(InsertLitterApi::class.java,context)
    }

    @Provides
    fun providesInsertLitterRepository(insertLitterApi: InsertLitterApi): InsertLitterRepository
    {
        return InsertLitterRepository(insertLitterApi)
    }
}