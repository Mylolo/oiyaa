package com.localvalu.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.viewpager.widget.ViewPager;

import com.localvalu.BuildConfig;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentTableBookBinding;
import com.localvalu.directory.detail.adapter.DirectoryTimeSliderViewPagerAdapter;
import com.localvalu.directory.detail.wrapper.SaveRequestSlotResultWrapper;
import com.localvalu.eats.detail.adapter.EatsTimeSliderViewPagerAdapter;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TableBookFragment extends BaseFragment
{
    private static final String TAG = "TableBookFragment";

    private FragmentTableBookBinding binding;
    private String timeSlotRequest = "";
    private String dateSlotRequest = "";
    private String currentDate;
    private EatsTimeSliderViewPagerAdapter eatsTimeSliderViewPagerAdapter;
    private DirectoryTimeSliderViewPagerAdapter directoryTimeSliderViewPagerAdapter;
    private String timeArrayList[] = new String[]{
            "12:00 PM", "12:30 PM", "01:00 PM", "01:30 PM", "02:00 PM", "02:30 PM",
            "03:00 PM", "03:30 PM", "04:00 PM", "04:30 PM", "05:00 PM", "05:30 PM",
            "06:00 PM", "06:30 PM", "07:00 PM", "07:30 PM", "08:00 PM", "08:30 PM",
            "09:00 PM", "09:30 PM", "10:00 PM", "10:30 PM", "11:00 PM", "11:30 PM",
            "12:00 AM", "12:30 AM", "01:00 AM", "01:30 AM", "02:00 AM", "02:30 AM",
            "03:00 AM", "03:30 AM", "04:00 AM", "04:30 AM", "05:00 AM", "05:30 AM",
            "06:00 AM", "06:30 AM", "07:00 AM", "07:30 AM", "08:00 AM", "08:30 AM",
            "09:00 AM", "09:30 AM", "10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM"};
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private String strAddToBasket;
    private boolean locationEnabled = false;
    private HostActivityListener hostActivityListener;
    private Typeface typeface;
    private String businessID;
    private int navigationType;
    private int selectedPos;

    public static TableBookFragment newInstance(String businessID,int navigationType)
    {
        TableBookFragment tableBookFragment = new TableBookFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppUtils.BUNDLE_BUSINESS_ID, businessID);
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, navigationType);
        tableBookFragment.setArguments(bundle);
        return tableBookFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: ");

        buildObjectForHandlingAPI();

        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);

        typeface = ResourcesCompat.getFont(requireContext(), R.font.comfortaa_regular);

        readFromBundle();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date cal = (Date) Calendar.getInstance().getTime();
        currentDate = formatter.format(cal);
        dateSlotRequest = currentDate;
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        strAddToBasket = getString(R.string.lbl_add_to_basket);
    }

    private void readFromBundle()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        businessID = getArguments().getString(AppUtils.BUNDLE_BUSINESS_ID);
        navigationType = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentTableBookBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated: ");
        setProperties();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        binding.getRoot().requestLayout();
    }

    private void setProperties()
    {
        Calendar calendar = Calendar.getInstance();
        long minDateInSeconds = calendar.getTimeInMillis();
        calendar.add(Calendar.MONTH,2);
        //calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DATE));
        long maxDateInSeconds = calendar.getTimeInMillis();
        binding.calendarView.setMinDate(minDateInSeconds);
        binding.calendarView.setMaxDate(maxDateInSeconds);
        binding.calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener()
        {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int monthNo, int dayOfMonth)
            {
                String m = "", d = "";
                int month = monthNo + 1;
                //Toast.makeText(getActivity(), year + "-" + month + "-" + dayOfMonth, 0).show();// TODO Auto-generated method stub
                if (String.valueOf(month).length() == 1)
                {
                    m = "0" + month;
                }
                else
                {
                    m = "" + month;
                }
                if (String.valueOf(dayOfMonth).length() == 1)
                {
                    d = "0" + dayOfMonth;
                }
                else
                {
                    d = "" + dayOfMonth;
                }
                dateSlotRequest = year + "-" + m + "-" + d;
            }
        });
        switch (navigationType)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
            case AppUtils.BMT_EATS:
                 eatsTimeSliderViewPagerAdapter = new EatsTimeSliderViewPagerAdapter(getActivity(), timeArrayList, true);
                 binding.viewPagerTimeSelection.setAdapter(eatsTimeSliderViewPagerAdapter);
                 eatsTimeSliderViewPagerAdapter.setOnPagerItemClickListener(new EatsTimeSliderViewPagerAdapter.OnPagerItemClickListener()
                 {
                    @Override
                    public void onPagerItemClick(int position)
                    {
                        timeSlotRequest = DateUtility.convertTimeTo24Hrs(timeArrayList[position]);
                        System.out.println("aaaaaaaaa : " + timeSlotRequest);
                        eatsTimeSliderViewPagerAdapter.notifyDataSetChanged();
                    }
                 });
                 break;
            case AppUtils.BMT_LOCAL:
                 directoryTimeSliderViewPagerAdapter = new DirectoryTimeSliderViewPagerAdapter(getActivity(), timeArrayList, true);
                 binding.viewPagerTimeSelection.setAdapter(directoryTimeSliderViewPagerAdapter);
                 directoryTimeSliderViewPagerAdapter.setOnPagerItemClickListener(new DirectoryTimeSliderViewPagerAdapter.OnPagerItemClickListener()
                 {
                    @Override
                    public void onPagerItemClick(int position)
                    {
                        timeSlotRequest = DateUtility.convertTimeTo24Hrs(timeArrayList[position]);
                        System.out.println("aaaaaaaaa : " + timeSlotRequest);
                        directoryTimeSliderViewPagerAdapter.notifyDataSetChanged();
                    }
                 });
                 break;
        }

        binding.viewPagerTimeSelection.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
            }

            @Override
            public void onPageSelected(int position)
            {
                if(BuildConfig.DEBUG)
                {
                    Log.d(TAG,"onPageSelected-"+position);
                }
                selectedPos = position;
            }

            @Override
            public void onPageScrollStateChanged(int state)
            {
            }
        });
        binding.btnBookTableSubmit.setOnClickListener(_OnClickListener);
        binding.btnReset.setOnClickListener(_OnClickListener);
        binding.ivLeft.setOnClickListener(_OnClickListener);
        binding.ivRight.setOnClickListener(_OnClickListener);
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.btnBookTableSubmit:
                     if(isValidForBookTable())
                     {
                         callAPIForBookTable();
                     }
                     break;
                case R.id.btnReset:
                     reset();
                     break;
                case R.id.ivLeft:
                     moveLeft();
                     break;
                case R.id.ivRight:
                     moveRight();
                     break;
            }
        }
    };

    private void moveLeft()
    {
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"moveLeft");
        }
        int maxSize = binding.viewPagerTimeSelection.getAdapter().getCount();
        if(selectedPos>0 && selectedPos<maxSize)
        {
            binding.viewPagerTimeSelection.post(new Runnable()
            {
                @Override
                public void run()
                {
                    binding.viewPagerTimeSelection.setCurrentItem(selectedPos-1);
                }
            });
        }
    }

    private void moveRight()
    {
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"moveRight");
        }
        int maxSize = binding.viewPagerTimeSelection.getAdapter().getCount();
        if(selectedPos>0 && selectedPos<maxSize)
        {
            binding.viewPagerTimeSelection.post(new Runnable()
            {
                @Override
                public void run()
                {
                    binding.viewPagerTimeSelection.setCurrentItem(selectedPos+1);
                }
            });
        }
    }

    private boolean isValidForBookTable()
    {
        if (dateSlotRequest.equals(""))
        {
            Toast.makeText(getActivity(), "Please choose your preferred date.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (timeSlotRequest.equals(""))
        {
            Toast.makeText(getActivity(), "Please choose your preferred time slot.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!DateUtility.compareDateBefore(dateSlotRequest, timeSlotRequest))
        {
            Toast.makeText(getActivity(), "Please choose any future date.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (binding.etNoOfGuests.getText().toString().equals(""))
        {
            Toast.makeText(getActivity(), "Please specify your number of guests.", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (Integer.parseInt(binding.etNoOfGuests.getText().toString()) == 0)
        {
            Toast.makeText(getActivity(), "Number of guests cannot be zero", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (Integer.parseInt(binding.etNoOfGuests.getText().toString()) > 999)
        {
            Toast.makeText(getActivity(), "We allow upto 999 guests", Toast.LENGTH_SHORT).show();
            return false;
        }
        /*else if (et_special.getText().toString().equals(""))
        {
            Toast.makeText(getActivity(), "Please specify any special requests.", Toast.LENGTH_SHORT).show();
            return false;
        }*/
        else if (!CommonUtilities.checkConnectivity(getActivity()))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.network_unavailable), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void callAPIForBookTable()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId", businessID);
            objMain.put("requestFor", "Tablebook");
            objMain.put("PreferredDate", dateSlotRequest);
            objMain.put("PreferredTime", timeSlotRequest);
            objMain.put("NoOfGuest", binding.etNoOfGuests.getText().toString());
            objMain.put("SpecialRequest", binding.etAnythingSpecial.getText().toString());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<SaveRequestSlotResultWrapper> dtoCall = apiInterface.requestAppointment(body);
        dtoCall.enqueue(new Callback<SaveRequestSlotResultWrapper>()
        {
            @Override
            public void onResponse(Call<SaveRequestSlotResultWrapper> call, Response<SaveRequestSlotResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().requestResponseResult.errorDetails.errorCode == 0)
                    {
                        reset();
                        Toast.makeText(getActivity(), "Your table book DashboardRequest sent successfully.", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().requestResponseResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {
                    if(BuildConfig.DEBUG)
                    {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SaveRequestSlotResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                {
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                }
                else
                {
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                }
            }
        });
    }

    private void reset()
    {
        binding.calendarView.setDate(Calendar.getInstance().getTimeInMillis(), false, true);
        switch (navigationType)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
            case AppUtils.BMT_EATS:
                eatsTimeSliderViewPagerAdapter.selectedPos = -1;
                eatsTimeSliderViewPagerAdapter.notifyDataSetChanged();
                break;
            case AppUtils.BMT_LOCAL:
                directoryTimeSliderViewPagerAdapter.selectedPos = -1;
                directoryTimeSliderViewPagerAdapter.notifyDataSetChanged();
                break;
        }

        dateSlotRequest = currentDate;
        timeSlotRequest = "";
        binding.etNoOfGuests.setText("");
        binding.etAnythingSpecial.setText("");

    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }


}
