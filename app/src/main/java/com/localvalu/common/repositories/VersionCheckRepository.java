package com.localvalu.common.repositories;

import com.localvalu.common.model.appversion.VersionCheckRequest;
import com.localvalu.common.model.appversion.VersionCheckResponse;
import com.requestHandler.ApiClient;
import com.requestHandler.versioncheck.VersionCheckApi;
import com.utility.AppUtils;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class VersionCheckRepository
{
    private static VersionCheckRepository instance;
    private VersionCheckApi versionCheckApi;

    public static VersionCheckRepository getInstance()
    {
        if(instance==null)
        {
            instance = new VersionCheckRepository();

        }
        return instance;
    }

    public Single<VersionCheckResponse> checkVersion(VersionCheckRequest versionCheckRequest)
    {
        versionCheckApi = ApiClient.getApiClientNew().create(VersionCheckApi.class);
        return versionCheckApi.checkVersion(versionCheckRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
