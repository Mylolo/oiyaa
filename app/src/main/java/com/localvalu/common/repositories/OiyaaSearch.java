package com.localvalu.common.repositories;

import com.localvalu.common.model.SearchRequest;
import com.localvalu.common.model.SearchResponse;
import com.requestHandler.ApiClient;
import com.requestHandler.search.SearchApi;
import com.utility.AppUtils;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class OiyaaSearch
{
    private static OiyaaSearch instance;
    private SearchApi searchApi;

    public static OiyaaSearch getInstance()
    {
        if(instance==null)
        {
            instance = new OiyaaSearch();

        }
        return instance;
    }

    public Single<SearchResponse> search(SearchRequest searchRequest,String actionMode)
    {
        searchApi = ApiClient.getApiClientNew().create(SearchApi.class);
        if(actionMode.equals(AppUtils.TEXT_SEARCH_MODE))
        {
            return searchApi.search(searchRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }
        else
        {
            return searchApi.searchFilter(searchRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }
    }

}
