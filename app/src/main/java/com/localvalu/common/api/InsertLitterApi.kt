package com.localvalu.common.api

import com.localvalu.base.BaseApi
import com.localvalu.common.model.SearchRequest
import com.localvalu.common.model.SearchResponse
import com.localvalu.common.model.litter.InsertLitterRequest
import com.localvalu.common.model.litter.InsertLitterResponse
import com.localvalu.registration.model.UserExistRequest
import com.localvalu.user.dto.PassionListRequest
import com.localvalu.user.wrapper.PassionListResultWrapper
import com.localvalu.user.wrapper.RegisterExistsCustomerResultWrapper
import retrofit2.http.Body
import retrofit2.http.POST

interface InsertLitterApi : BaseApi
{
    @POST("LitterPicker/InsertLitterdetails")
    suspend fun insertLitter(@Body insertLitterRequest: InsertLitterRequest): InsertLitterResponse
}