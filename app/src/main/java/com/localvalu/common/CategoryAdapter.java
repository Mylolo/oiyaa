package com.localvalu.common;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.localvalu.R;
import com.localvalu.common.listeners.CategoryClicksListener;
import com.localvalu.common.model.cuisinescategory.CuisineCategoryData;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder>
{
    public static final String TAG = CategoryAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<CuisineCategoryData> categoryDataArrayList;
    private CategoryClicksListener onCategoryClicksListener;
    private int navigationType;

    public CategoryAdapter(ArrayList<CuisineCategoryData> categoryDataArrayList, int navigationType)
    {
       this.categoryDataArrayList = categoryDataArrayList;
       this.navigationType = navigationType;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView)
    {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_item_cuisines_home, viewGroup, false);
        return new CategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryAdapter.ViewHolder viewHolder, final int position)
    {
        CuisineCategoryData data = categoryDataArrayList.get(position);
        viewHolder.btnCuisine.setText(data.getName());
        viewHolder.btnCuisine.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(onCategoryClicksListener !=null)
                {
                    onCategoryClicksListener.onCategoryClicked(data,navigationType);
                }
            }
        });
    }

    @Override
    public int getItemCount()
    {
        return categoryDataArrayList.size();
    }

    public void setOnCategoryClicksListener(CategoryClicksListener onCategoryClicksListener)
    {
        this.onCategoryClicksListener = onCategoryClicksListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        MaterialButton btnCuisine = itemView.findViewById(R.id.btnCuisine);
        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }


}
