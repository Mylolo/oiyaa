package com.localvalu.common;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.localvalu.R;
import com.localvalu.directory.detail.wrapper.SaveRequestSlotResultWrapper;
import com.localvalu.user.ValidationUtils;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;
import com.utility.validators.RangeValidator;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactMeActivity extends AppCompatActivity
{
    String TAG = "ContactMeFragment";

    public View view;
    private CoordinatorLayout clContactMeRoot;
    private TextView tvTitle, etPreferDate, etPreferTime;
    private EditText etFullName, etMobileNumber, etEmailId, etComment;
    private Toolbar toolbar;
    private MaterialButton btnContactMeSubmit;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private String businessID;
    private Date contactMePreferredDate;
    private int navigationType;
    private int themeColor;
    private int themeColorBackground;
    Calendar calendarStart = Calendar.getInstance();
    Calendar calendarEnd = Calendar.getInstance();

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        readFromBundle();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_contact_me);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getApplicationContext(), PreferenceUtility.APP_PREFERENCE_NAME);
        buildObjectForHandlingAPI();

        clContactMeRoot = findViewById(R.id.clContactMeRoot);
        tvTitle = findViewById(R.id.tvTitle);
        toolbar = findViewById(R.id.toolbar);
        etPreferDate = findViewById(R.id.etPreferDate);
        etPreferTime = findViewById(R.id.etPreferTime);
        etFullName = findViewById(R.id.etFullName);
        etMobileNumber = findViewById(R.id.etMobileNumber);
        etEmailId = findViewById(R.id.etEmailId);
        etComment = findViewById(R.id.etComment);
        btnContactMeSubmit = findViewById(R.id.btnContactMeSubmit);


        setUpActionBar();
        btnContactMeSubmit.setOnClickListener(_OnClickListener);
        etPreferDate.setOnClickListener(_OnClickListener);
        etPreferTime.setOnClickListener(_OnClickListener);
        clContactMeRoot.setBackgroundColor(themeColorBackground);
    }

    private void setUpActionBar()
    {
        tvTitle.setTextColor(themeColor);
        tvTitle.setText(getString(R.string.lbl_contact_me));
        tvTitle.setTextColor(themeColor);
        toolbar.getNavigationIcon().setColorFilter(themeColor, PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                finish();
            }
        });
    }

    private void readFromBundle()
    {
        try
        {
            businessID = getIntent().getExtras().getString(AppUtils.BUNDLE_BUSINESS_ID);
            navigationType = getIntent().getExtras().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);

            switch (navigationType)
            {
                case AppUtils.BMT_TRACK_AND_TRACE:
                case AppUtils.BMT_EATS:
                    themeColor = ContextCompat.getColor(this, R.color.colorEats);
                    themeColorBackground = ContextCompat.getColor(this, R.color.colorEatsLightBackground);
                    setTheme(R.style.AppTheme);
                    break;
                case AppUtils.BMT_LOCAL:
                    themeColor = ContextCompat.getColor(this, R.color.colorEats);
                    themeColorBackground = ContextCompat.getColor(this, R.color.colorLocalLightBackground);
                    setTheme(R.style.AppThemeLocal);
                    break;
                case AppUtils.BMT_MALL:
                    themeColor = ContextCompat.getColor(this, R.color.colorMall);
                    themeColorBackground = ContextCompat.getColor(this, R.color.colorMallLightBackground);
                    setTheme(R.style.AppThemeMall);
                    break;
            }
        }
        catch (Exception e)
        {
            businessID = "";
        }
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.btnContactMeSubmit:
                    if (areAllInputsValid())
                    {
                        callAPIForGetQuote();
                    }
                    break;
                case R.id.etPreferDate:
                    if (contactMePreferredDate == null) contactMePreferredDate = new Date();
                    showDatePicker(etPreferDate);
                    break;
                case R.id.etPreferTime:
                    if (contactMePreferredDate == null) contactMePreferredDate = new Date();
                    showContactMeTimePicker(etPreferTime);
                    break;
            }
        }
    };

    /**
     * Call for get the quote from server
     * Contact Me Api.
     */
    private void callAPIForGetQuote()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId", businessID);
            objMain.put("requestFor", "quotebook");
            objMain.put("fullName", etFullName.getText().toString());
            objMain.put("mobile", etMobileNumber.getText().toString());
            objMain.put("email", etEmailId.getText().toString());
            String strPreferDate = DateUtility.getServerFormatDateByDate(contactMePreferredDate);
            String strPreferTime = DateUtility.get24HoursTimeFormat(contactMePreferredDate);
            objMain.put("PreferredDate", strPreferDate);
            objMain.put("PreferredTime", strPreferTime);
            objMain.put("comment", etComment.getText().toString());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<SaveRequestSlotResultWrapper> dtoCall = apiInterface.requestCallback(body);
        dtoCall.enqueue(new Callback<SaveRequestSlotResultWrapper>()
        {
            @Override
            public void onResponse(Call<SaveRequestSlotResultWrapper> call, Response<SaveRequestSlotResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().requestResponseResult.errorDetails.errorCode == 0)
                    {
                        etFullName.setText("");
                        etMobileNumber.setText("");
                        etEmailId.setText("");
                        etPreferDate.setText("");
                        etPreferTime.setText("");
                        etComment.setText("");

                        Toast.makeText(getApplicationContext(), "Your callback DashboardRequest saved successfully.", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().requestResponseResult.errorDetails.errorMessage, ContactMeActivity.this);
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<SaveRequestSlotResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getApplicationContext()))
                {
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), ContactMeActivity.this);
                }
                else
                {
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), ContactMeActivity.this);
                }
            }
        });
    }

    public boolean areAllInputsValid()
    {
        if (isValidUserName() && isValidPhoneNumber() && isValidEmail() && isValidPreferDate() && isValidPreferTime() && isValidComment())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isValidUserName()
    {
        if (etFullName.getText().toString().isEmpty())
        {
            etFullName.setError(getString(R.string.lbl_name_empty));
            etFullName.requestFocus();
            return false;
        }
        else
        {
            etFullName.setError(null);
            return true;
        }
    }

    public boolean isValidPhoneNumber()
    {
        if (etMobileNumber.getText().toString().isEmpty())
        {
            etMobileNumber.setError(getString(R.string.lbl_mobile_number_empty));
            etMobileNumber.requestFocus();
            return false;
        }
        else if (!ValidationUtils.isValidMobileNo(etMobileNumber.getText().toString().trim()))
        {
            etMobileNumber.setError(getString(R.string.lbl_invalid_mobile_number));
            etMobileNumber.requestFocus();
            return false;
        }
        else
        {
            etMobileNumber.setError(null);
            return true;
        }
    }

    public boolean isValidEmail()
    {
        if (etEmailId.getText().toString().isEmpty())
        {
            etMobileNumber.setError(getString(R.string.lbl_mobile_number_empty));
            etMobileNumber.requestFocus();
            return false;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(etEmailId.getText().toString().trim()).matches())
        {
            etMobileNumber.setError(getString(R.string.msg_enter_valid_email_id));
            etMobileNumber.requestFocus();
            return false;
        }
        else
        {
            etMobileNumber.setError(null);
            return true;
        }
    }

    public boolean isValidPreferDate()
    {
        if (etPreferDate.getText().toString().isEmpty())
        {
            etPreferDate.setError(getString(R.string.lbl_prefer_date_empty));
            etPreferDate.requestFocus();
            return false;
        }
        else
        {
            etPreferDate.setError(null);
            return true;
        }
    }

    public boolean isValidPreferTime()
    {
        if (etPreferTime.getText().toString().isEmpty())
        {
            etPreferTime.setError(getString(R.string.lbl_prefer_time_empty));
            etPreferTime.requestFocus();
            return false;
        }
        else
        {
            etPreferTime.setError(null);
            return true;
        }
    }

    public boolean isValidComment()
    {
        if (etComment.getText().toString().isEmpty())
        {
            etComment.setError(getString(R.string.lbl_comment_empty));
            etComment.requestFocus();
            return false;
        }
        else
        {
            etComment.setError(null);
            return true;
        }
    }

    private void showContactMeDatePicker(TextView textView)
    {
        final Calendar cldr = Calendar.getInstance();
        cldr.setTime(contactMePreferredDate);
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        // date picker dialog
        DatePickerDialog picker = new DatePickerDialog(this,
                R.style.AppTheme, new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {
                String month;
                String day;

                if ((monthOfYear + 1) < 10)
                {
                    month = "0" + String.valueOf(monthOfYear + 1);
                }
                else
                {
                    month = String.valueOf(monthOfYear + 1);
                }

                if (dayOfMonth < 10)
                {
                    day = "0" + String.valueOf(dayOfMonth);
                }
                else
                {
                    day = String.valueOf(dayOfMonth);
                }
                System.out.println(year + "-" + month + "-" + dayOfMonth);
                textView.setText(dayOfMonth + "-" + month + "-" + year);
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                contactMePreferredDate = calendar.getTime();
            }
        }, year, month, day);
        picker.getDatePicker().setMinDate(new Date().getTime());
        picker.show();
    }

    private void showDatePicker(TextView textView)
    {
        calendarEnd.add(Calendar.YEAR, 5);
        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        //builder.setTheme(R.style.AppThemeLocal);
        builder.setCalendarConstraints(contactMeDateLimitRange().build());
        MaterialDatePicker picker = builder.build();


        picker.show(getSupportFragmentManager(), picker.toString());

        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener()
        {
            @Override
            public void onPositiveButtonClick(Object selection)
            {
                long milliseconds = (long) selection;
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTimeInMillis(milliseconds);
                textView.setText(convertDateToStringInUKFormat(selectedDate.getTime()));
                contactMePreferredDate = selectedDate.getTime();
            }
        });
    }

    /*
     Limit selectable Date range
   */
    private CalendarConstraints.Builder contactMeDateLimitRange()
    {
        CalendarConstraints.Builder constraintsBuilderRange = new CalendarConstraints.Builder();

        constraintsBuilderRange.setStart(calendarStart.getTimeInMillis());
        constraintsBuilderRange.setEnd(calendarEnd.getTimeInMillis());
        constraintsBuilderRange.setValidator(new RangeValidator(calendarStart.getTimeInMillis(), calendarEnd.getTimeInMillis()));

        return constraintsBuilderRange;
    }

    private String convertDateToStringInUKFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    private void showContactMeTimePicker(TextView textView)
    {
        final Calendar cldr = Calendar.getInstance();
        //cldr.setTime(contactMePreferredDate);
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        int hour = cldr.get(Calendar.HOUR_OF_DAY);
        int minute = cldr.get(Calendar.MINUTE);
        int second = cldr.get(Calendar.SECOND);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this, _OnTimeSetListener, hour, minute, false);
        timePickerDialog.show();
    }

    TimePickerDialog.OnTimeSetListener _OnTimeSetListener = new TimePickerDialog.OnTimeSetListener()
    {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute)
        {
            final Calendar cldr = Calendar.getInstance();
            cldr.setTime(contactMePreferredDate);
            cldr.set(Calendar.HOUR_OF_DAY, hourOfDay);
            cldr.set(Calendar.MINUTE, minute);
            contactMePreferredDate = cldr.getTime();
            etPreferTime.setText(DateUtility.get12HoursTimeFormat(contactMePreferredDate));
        }
    };

}
