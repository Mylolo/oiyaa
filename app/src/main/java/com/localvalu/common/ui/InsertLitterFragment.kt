package com.localvalu.common.ui


import android.app.Activity
import android.app.TimePickerDialog
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.text.InputFilter
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.TimePicker
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.localvalu.BuildConfig
import com.localvalu.R
import com.localvalu.base.BaseFragment
import com.localvalu.base.HostActivityListener
import com.localvalu.base.Resource
import com.localvalu.common.model.litter.InsertLitterRequest
import com.localvalu.common.viewmodel.InsertLitterViewModel
import com.localvalu.databinding.FragmentInsertLitterBinding
import com.localvalu.extensions.convertDateTimeToStringInDisplayFormat
import com.localvalu.extensions.convertDateTimeToStringInServerFormat
import com.localvalu.extensions.getDifferenceInMinutes
import com.localvalu.extensions.getTodayDate
import com.localvalu.user.ValidationUtils
import com.localvalu.user.dto.RegisterDataDto
import com.localvalu.user.dto.UserBasicInfo
import com.utility.*
import com.utility.validators.RangeValidator
import dagger.hilt.android.AndroidEntryPoint
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.min

@AndroidEntryPoint
class InsertLitterFragment : BaseFragment()
{
    private lateinit var binding: FragmentInsertLitterBinding
    private var colorBlack = 0
    private var colorEats = 0
    private var colorGray = 0
    private var colorWhite = 0
    private val viewModel: InsertLitterViewModel by viewModels()

    private var startTime: Date? = null;
    private var endTime: Date? = null;
    private var userBasicInfo: UserBasicInfo? = null
    private var hostActivityListener: HostActivityListener? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        colorBlack = ContextCompat.getColor(requireContext(), R.color.colorBlack)
        colorGray = ContextCompat.getColor(requireContext(), R.color.colorGray)
        colorWhite = ContextCompat.getColor(requireContext(), R.color.colorWhite)
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats)
        startTime = null
        endTime = null
        Log.d(TAG, "StartTime : $startTime")
        Log.d(TAG, "EndTime : $endTime")
        userBasicInfo = PreferenceUtility.getObjectInAppPreference(
            activity,
            PreferenceUtility.APP_PREFERENCE_NAME
        ) as UserBasicInfo
    }

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        hostActivityListener = context as HostActivityListener;
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        binding = FragmentInsertLitterBinding.inflate(inflater, container, false)
        return binding.root;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        setProperties()
        observeSubscribers()
    }

    var onClickListener = View.OnClickListener { view -> onClickFunction(view) }

    private fun setProperties()
    {
        //setFilters(binding.etPostCode, "[0-9a-zA-Z ]+")
        val drawableBack = ResourcesCompat.getDrawable(resources, R.drawable.ic_app_arrow, null)
        drawableBack!!.setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP)
        binding!!.toolbarLayout.toolbar.navigationIcon = drawableBack
        binding!!.toolbarLayout.tvTitle.text = getString(R.string.lbl_litter_picking)
        binding!!.toolbarLayout.toolbar.setTitleTextColor(colorEats)
        binding!!.toolbarLayout.toolbar.setNavigationOnClickListener {
            hostActivityListener?.showBottomNavigation()
            hostActivityListener?.onBackButtonPressed()
        }
        binding!!.etStartTime.setOnClickListener(onClickListener)
        binding!!.etEndTime.setOnClickListener(onClickListener)
        binding!!.btnInsertLitterSubmit.setOnClickListener(onClickListener)
        binding!!.cnlMain.setOnClickListener(onClickListener)
        binding!!.etPostCode.doAfterTextChanged {
            if(it.toString().isNotEmpty())
            {
                if (!ValidationUtils.isValidPostCode(binding.etPostCode.text.toString()))
                {
                    binding!!.tilPostCode.error = getString(R.string.msg_enter_valid_post_code)
                }
                else binding!!.tilPostCode.error = null
            }
        }
        val drawable = ResourcesCompat.getDrawable(resources, R.drawable.menu_scanpay, null)
        drawable!!.setColorFilter(colorGray, PorterDuff.Mode.SRC_ATOP)
        hostActivityListener?.hideBottomNavigation()
    }

    private fun observeSubscribers()
    {
        viewModel.insertLitterResponse.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            when (it)
            {
                is Resource.Loading ->
                {
                    showProgress()
                }
                is Resource.Success ->
                {
                    showContents()
                    if (it.value.insertLitterDetails.insertLitterResult.errorDetails.errorCode == 0L)
                    {
                        val strSuccessMsg = StringBuilder()
                        strSuccessMsg.append(getString(R.string.lbl_you_have_earned)).append(" ")
                            .append(it.value.insertLitterDetails.insertLitterResult.tokenEarned)
                            .append(" ")
                            .append(getString(R.string.lbl_tokens))
                        showMessage(strSuccessMsg.toString())
                        clearText()
                    }
                    else
                    {
                        showMessage(it.value.insertLitterDetails.insertLitterResult.errorDetails.errorMessage)
                    }
                }
                is Resource.Failure ->
                {
                    handleAPIError(it) { insertLitter() }
                }
            }
        })
    }

    private fun showProgress()
    {
        binding.layoutProgress.layoutRoot.visibility = View.VISIBLE
        binding.layoutError.layoutRoot.visibility = View.GONE
        binding.nestedScrollView.visibility = View.GONE
    }

    private fun showError(message: String, retry: Boolean)
    {
        binding.layoutProgress.layoutRoot.visibility = View.GONE
        binding.layoutError.layoutRoot.visibility = View.VISIBLE
        binding.nestedScrollView.visibility = View.GONE
    }

    private fun showContents()
    {
        binding.layoutProgress.layoutRoot.visibility = View.GONE
        binding.layoutError.layoutRoot.visibility = View.GONE
        binding.nestedScrollView.visibility = View.VISIBLE
    }

    private fun showStartTimePicker()
    {
        val calendarTemp = Calendar.getInstance()
        var hours = calendarTemp.get(Calendar.HOUR)
        val minutes = calendarTemp.get(Calendar.MINUTE)
        val amPm = calendarTemp.get(Calendar.AM_PM)
        Log.d(TAG, " Hours : $hours, Minutes : $minutes")
        if (amPm == Calendar.PM) hours += 12
        val picker =
            MaterialTimePicker.Builder()
                .setTimeFormat(TimeFormat.CLOCK_12H)
                .setHour(hours)
                .setMinute(minutes)
                .build()
        picker.show(childFragmentManager, "STARTTIME")

        picker.addOnPositiveButtonClickListener {

            var pickedHour: Int = picker.hour
            var pickedamPm: Int = 0;
            if (pickedHour > 12)
            {
                pickedHour -= 12
                pickedamPm = 1
            }
            val pickedMinute: Int = picker.minute
            Log.d(TAG, " pickedHour : $pickedHour, pickedMinute : $pickedHour")

            calendarTemp.set(Calendar.HOUR, pickedHour)
            calendarTemp.set(Calendar.MINUTE, pickedMinute)
            calendarTemp.set(Calendar.AM_PM, pickedamPm)
            Log.d(TAG, "startTime : ${calendarTemp.time}")
            binding.tilStartTime.error = null
            startTime = calendarTemp.time
            binding?.etStartTime.setText(startTime?.convertDateTimeToStringInDisplayFormat())
            isValidStartTime(calendarTemp.time)
            isValidEndTime(endTime)
        }
    }

    private fun isValidStartTime(selectedStartTime: Date?): Boolean
    {
        if (selectedStartTime?.after(Date()) == true)
        {
            binding.tilStartTime.error =  getString(R.string.msg_start_time_should_not_be_future_time)
            return false
        }
        else
        {
            if(endTime!=null)
            {
                if (selectedStartTime?.after(endTime) == true)
                {
                    binding.tilStartTime.error =  getString(R.string.msg_start_time_should_be_less_time)
                    return false
                }
            }
        }
        return true
    }

    private fun showEndTimePicker()
    {
        val calendarTemp = Calendar.getInstance()
        var hours = calendarTemp.get(Calendar.HOUR)
        val minutes = calendarTemp.get(Calendar.MINUTE)
        val amPm = calendarTemp.get(Calendar.AM_PM)
        Log.d(TAG, " Hours : $hours, Minutes : $minutes")
        if (amPm == Calendar.PM) hours += 12
        val picker =
            MaterialTimePicker.Builder()
                .setTimeFormat(TimeFormat.CLOCK_12H)
                .setHour(hours)
                .setMinute(minutes)
                .build()
        picker.show(childFragmentManager, "ENDTIME")
        picker.addOnPositiveButtonClickListener {
            var pickedHour: Int = picker.hour
            var pickedamPm: Int = 0;
            if (pickedHour > 12)
            {
                pickedHour -= 12
                pickedamPm = 1
            }
            val pickedMinute: Int = picker.minute
            Log.d(TAG, " pickedHour : $pickedHour, pickedMinute : $pickedHour")

            calendarTemp.set(Calendar.HOUR, pickedHour)
            calendarTemp.set(Calendar.MINUTE, pickedMinute)
            calendarTemp.set(Calendar.AM_PM, pickedamPm)
            Log.d(TAG, "endTime : ${calendarTemp.time}")
            binding.tilEndTime.error = null
            endTime = calendarTemp.time
            binding?.etEndTime.setText(endTime?.convertDateTimeToStringInDisplayFormat())
            isValidEndTime(calendarTemp.time!!)
        }
    }

    private fun isValidEndTime(selectedEndTime: Date?): Boolean
    {
        if(selectedEndTime==null) return false;
        startTime?.let {
            if (selectedEndTime!!.before(startTime))
            {
                binding.tilEndTime.error =
                    getString(R.string.msg_end_time_should_not_be_less_start_time)
                return false
            }
            if(selectedEndTime.getDifferenceInMinutes(it)==0L)
            {
                binding.tilEndTime.error =
                    getString(R.string.msg_end_time_start_time_should_not_be_same)
                return false
            }
            else if (selectedEndTime.after(Date()))
            {
                binding.tilEndTime.error = getString(R.string.msg_end_time_should_not_be_future_time)
                return false
            }
            binding.tilEndTime.error=null
            return true
        }
        return false
    }

    private fun insertLitterRequest(): InsertLitterRequest?
    {
        startTime?.let { pStartTime ->
            endTime?.let { pEndTime ->
                userBasicInfo?.let {
                    val strServerStartTime = StringBuilder()
                    strServerStartTime.append(pStartTime.convertDateTimeToStringInServerFormat())
                    val strServerEndTime = StringBuilder()
                    strServerEndTime.append(pEndTime.convertDateTimeToStringInServerFormat())

                    val insertLitterRequest = userBasicInfo?.accountId?.let { accountId ->
                        InsertLitterRequest(
                            "",
                            accountId,
                            strServerStartTime.toString(),
                            strServerEndTime.toString(),
                            binding.etPostCode.text.toString().trim()
                        )
                    }
                    return insertLitterRequest;
                }
            }
        }
        return null
    }

    private fun insertLitter()
    {
        hideKeyboard(requireActivity())
        if (isValid)
        {
            val insertLitterRequest: InsertLitterRequest? = insertLitterRequest();
            if (BuildConfig.DEBUG)
            {
                Log.d(TAG, "insetLitterRequest-" + insertLitterRequest?.token)
                Log.d(TAG, "insetLitterRequest-" + insertLitterRequest?.accountId)
                Log.d(TAG, "insetLitterRequest-" + insertLitterRequest?.startTime)
                Log.d(TAG, "insetLitterRequest-" + insertLitterRequest?.endTime)
            }

            insertLitterRequest?.let {
                viewModel.insertLitter(it)
            }
        }
    }

    private fun onClickFunction(view: View)
    {
        when (view.id)
        {
            R.id.btnInsertLitterSubmit -> insertLitter()
            R.id.imgBack               ->
            {
                hostActivityListener?.showBottomNavigation()
                hostActivityListener?.onBackButtonPressed()

            }
            //R.id.etStartDate     -> showStartDatePicker(true)
            //R.id.etEndDate       -> showStartDatePicker(false)
            R.id.etStartTime           -> showStartTimePicker()
            R.id.etEndTime             -> showEndTimePicker()

        }
    }

    private val isValid: Boolean
        private get()
        {

            if (startTime == null)
            {
                binding!!.tilStartTime.error = getString(R.string.msg_select_start_time)
                return false
            }
            else if (endTime == null)
            {
                binding!!.tilEndTime.error = getString(R.string.msg_select_end_time)
                return false
            }
            else if(!isValidStartTime(startTime)){return false}
            else if(!isValidEndTime(endTime)){return false}
            else if (binding!!.etPostCode.text!!.isEmpty())
            {
                binding!!.tilPostCode.error = getString(R.string.msg_enter_valid_post_code)
                binding!!.etPostCode.requestFocus()
                return false
            }
            else if (!ValidationUtils.isValidPostCode(binding.etPostCode.text?.trim().toString()))
            {
                binding!!.tilPostCode.error = getString(R.string.msg_enter_valid_post_code)
                binding!!.etPostCode.requestFocus()
                return false
            }
            binding.tilStartTime.error=null
            binding.tilEndTime.error=null
            binding.tilStartTime.error=null
            return true
        }

    private fun setFilters(textInputEditText: TextInputEditText, regEx: String)
    {
        textInputEditText.filters = arrayOf(
            InputFilter { cs, start, end, spanned, dStart, dEnd -> // TODO Auto-generated method stub
                if (cs == "")
                { // for backspace
                    return@InputFilter cs
                }
                if (cs.toString().contains(regEx))
                {
                    cs
                }
                else ""
            }
        )
    }

    private fun hideKeyboard(activity: Activity?)
    {
        if (activity != null && activity.window != null)
        {
            activity.window.decorView
            val imm = activity.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.hideSoftInputFromWindow(activity.window.decorView.windowToken, 0)
        }
    }

    private fun clearText()
    {
        binding.etStartTime.setText("")
        binding.etEndTime.setText("")
        binding.etPostCode.setText("")
        binding.etPostCode.error=null
        binding.tvLitterPickingMsgTwo.requestFocus()
        startTime = Date().getTodayDate()
        endTime = Date().getTodayDate()
    }

    companion object
    {
        private val TAG = InsertLitterFragment::class.java.simpleName
        fun pad(data: Int): String
        {
            return if (data >= 10) data.toString() else "0$data"
        }
    }

}