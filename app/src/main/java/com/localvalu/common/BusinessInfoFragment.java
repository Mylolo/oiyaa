package com.localvalu.common;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.CustomMapFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentBusinessInfoBinding;
import com.localvalu.directory.detail.dto.DataParser;
import com.localvalu.directory.scanpay.dto.BusinessDetail;
import com.localvalu.directory.scanpay.dto.BusinessDetailInfo;
import com.localvalu.directory.scanpay.dto.BusinessDetailResult;
import com.localvalu.directory.scanpay.dto.BusinessImages;
import com.localvalu.directory.scanpay.dto.RetailerPromotion;
import com.localvalu.directory.scanpay.wrapper.BusinessDetailResultWrapper;
import com.localvalu.eats.detail.adapter.EatsDiscountListAdapter;
import com.localvalu.eats.detail.adapter.EatsImageSliderViewPagerAdapter;
import com.localvalu.eats.detail.adapter.EatsServiceListAdapter;
import com.localvalu.eats.detail.dto.BusinessDetailDiscountForEatsDto;
import com.localvalu.eats.detail.dto.BusinessDetailServiceForEatsDto;
import com.localvalu.common.review.ReviewsAddFragment;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.squareup.picasso.Picasso;
import com.utility.AppUtils;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BusinessInfoFragment extends BaseFragment
{
    private static final String TAG = "BusinessInfoFragment";

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;

    private static final int MY_MAKE_CALL_PERMISSION_REQUEST_CODE = 123;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 12345;

    private HostActivityListener hostActivityListener;

    private FragmentBusinessInfoBinding binding;
    private UserBasicInfo userBasicInfo;
    private ApiInterface apiInterface;
    private EatsImageSliderViewPagerAdapter eatsImageSliderViewPagerAdapter;
    private EatsDiscountListAdapter eatsDiscountListAdapter;
    private EatsServiceListAdapter eatsServiceListAdapter;

    private GoogleMap mMap;
    private FusedLocationProviderClient fusedLocationClient;
    private Boolean mLocationPermissionsGranted = false;
    private boolean locationEnabled = false;
    private LatLng myLocation, retailerLocation;
    private Double latitude, longitude;
    private List<Polyline> polyLines = new ArrayList<>();

    private BusinessDetail businessDetail;
    private BusinessDetailInfo businessDetailInfo;
    private int navigationType;
    private int themeColor;
    private int themeColorBackground;

    public static BusinessInfoFragment newInstance(BusinessDetail businessDetail, int navigationType)
    {
        BusinessInfoFragment businessInfoFragment = new BusinessInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppUtils.BUNDLE_BUSINESS_DETAIL, businessDetail);
        bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, navigationType);
        businessInfoFragment.setArguments(bundle);
        return businessInfoFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        readFromBundle();
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        binding = FragmentBusinessInfoBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        setProperties();
    }

    private void setProperties()
    {
        binding.cnlBusinessInfoRoot.setBackgroundColor(themeColorBackground);
        setUpActionBar();
        setGPS();
        binding.layoutBusinessInfo.btnAddReview.setOnClickListener(_OnClickListener);
        binding.layoutBusinessInfo.btnToggleAbout.setOnClickListener(_OnClickListener);
        binding.layoutBusinessInfo.tvPhoneNumber.setOnClickListener(_OnClickListener);
        binding.layoutBusinessInfo.tvWebsite.setOnClickListener(_OnClickListener);
        binding.layoutError.btnRetry.setOnClickListener(_OnClickListener);
        if (businessDetail != null)
        {
            BusinessDetailInfo businessDetailInfo = businessDetail.businessDetails.get(0);

            if (businessDetailInfo != null)
            {
                setAbout(businessDetailInfo);
                setAddress(businessDetailInfo);
                setDiscount(businessDetailInfo);
                setTimingsAndSplitTimings(businessDetailInfo);
                setLatLngValues(businessDetailInfo);
                setHygieneImage(businessDetailInfo);
                //showOnMap(businessDetailInfo);
            }

            setImageAndPromotions(businessDetail);

            showContents();
        }
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            onButtonClick(view);
        }
    };

    private void setUpActionBar()
    {
        binding.toolbarLayout.tvTitle.setTextColor(themeColor);
        binding.toolbarLayout.tvTitle.setText(getString(R.string.lbl_details));
        binding.toolbarLayout.toolbar.getNavigationIcon().setColorFilter(themeColor, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private void readFromBundle()
    {
        if (getArguments() != null)
        {
            businessDetail = (BusinessDetail) getArguments().getSerializable(AppUtils.BUNDLE_BUSINESS_DETAIL);
            businessDetailInfo = businessDetail.businessDetails.get(0);
            navigationType = getArguments().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);

            switch (navigationType)
            {
                case AppUtils.BMT_TRACK_AND_TRACE:
                case AppUtils.BMT_EATS:
                    themeColor = ContextCompat.getColor(requireContext(), R.color.colorEats);
                    themeColorBackground = ContextCompat.getColor(requireContext(), R.color.colorEatsLightBackground);
                    break;
                case AppUtils.BMT_LOCAL:
                    themeColor = ContextCompat.getColor(requireContext(), R.color.colorLocal);
                    themeColorBackground = ContextCompat.getColor(requireContext(), R.color.colorLocalLightBackground);
                    break;
                case AppUtils.BMT_MALL:
                    themeColor = ContextCompat.getColor(requireContext(), R.color.colorMall);
                    themeColorBackground = ContextCompat.getColor(requireContext(), R.color.colorMallLightBackground);
                    break;
            }
        }
    }

    public void setGPS()
    {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            getLocationPermission();
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>()
                {
                    @Override
                    public void onSuccess(Location location)
                    {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null)
                        {
                            // Logic to handle location object
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                            Log.d(TAG, "onSuccess: latitude-" + latitude);
                            Log.d(TAG, "onSuccess: longitude-" + longitude);
                            locationEnabled = true;
                        }
                        else
                        {
                            Log.d(TAG, "onSuccess: location - " + null);
                            locationEnabled = false;
                        }
                    }
                });

    }

    private void getLocationPermission()
    {
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                mLocationPermissionsGranted = true;
                setGPS();
            }
            else
            {
                ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSION_REQUEST_CODE);
            }
        }
        else
        {
            ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    /**
     * Get the business information from server
     *
     * @param businessId
     */
    private void fetchBusinessInfo(String businessId)
    {
        showProgress();

        JSONObject objMain = new JSONObject();

        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("BusinessId", businessId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<BusinessDetailResultWrapper> dtoCall = apiInterface.businessDetailFrom(body);
        dtoCall.enqueue(new Callback<BusinessDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<BusinessDetailResultWrapper> call, Response<BusinessDetailResultWrapper> response)
            {
                //mProgressDialog.dismiss();

                try
                {
                    if (response.isSuccessful())
                    {
                        BusinessDetailResultWrapper businessDetailResultWrapper = response.body();

                        if (businessDetailResultWrapper != null)
                        {
                            BusinessDetailResult businessDetailResult = businessDetailResultWrapper.businessDetailResult;

                            if (businessDetailResult.errorDetails.errorCode == 0)
                            {
                                showContents();

                                BusinessDetail businessDetail = businessDetailResult.businessDetails;

                                if (businessDetail != null)
                                {
                                    BusinessDetailInfo businessDetailInfo = businessDetail.businessDetails.get(0);

                                    if (businessDetailInfo != null)
                                    {
                                        setAbout(businessDetailInfo);
                                        setAddress(businessDetailInfo);
                                        setDiscount(businessDetailInfo);
                                        setTimingsAndSplitTimings(businessDetailInfo);
                                        setLatLngValues(businessDetailInfo);
                                        setHygieneImage(businessDetailInfo);
                                        //showOnMap(businessDetailInfo);
                                    }
                                    setImageAndPromotions(businessDetail);
                                }
                            }

                            else
                            {
                                showError(businessDetailResult.errorDetails.errorMessage, false);
                            }
                        }
                    }
                    else
                    {
                        binding.layoutBusinessInfo.ivHygine.setVisibility(View.GONE);
                        // error case
                        //Toast.makeText(getActivity(), getString(R.string.server_alert), Toast.LENGTH_SHORT).show();
                        //showError(getString(R.string.server_alert),false);
                        //getActivity().onBackPressed();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<BusinessDetailResultWrapper> call, Throwable t)
            {
                //mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());
                if (t instanceof IOException)
                    showError(getString(R.string.network_unavailable), true);
                else showError(getString(R.string.server_alert), false);
            }
        });

    }

    /**
     * Set About Content
     *
     * @param businessDetailInfo
     */
    public void setAbout(BusinessDetailInfo businessDetailInfo)
    {
        if (businessDetailInfo.about != null && (!businessDetailInfo.about.isEmpty()))
        {
            binding.layoutBusinessInfo.tvAboutDesc.setText(businessDetailInfo.about);
        }
        else binding.layoutBusinessInfo.tvAboutDesc.setText("");

        binding.layoutBusinessInfo.tvAboutDesc.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                int lineCount = binding.layoutBusinessInfo.tvAboutDesc.getLineCount();

                // Use lineCount here
                // Drawing happens after layout so we can assume getLineCount() returns the correct value
                if (lineCount > 2)
                {
                    binding.layoutBusinessInfo.dividerAbout.setVisibility(View.VISIBLE);
                    binding.layoutBusinessInfo.btnToggleAbout.setVisibility(View.VISIBLE);
                    binding.layoutBusinessInfo.invisibleAboutView.setVisibility(View.GONE);
                }
                else
                {
                    binding.layoutBusinessInfo.dividerAbout.setVisibility(View.GONE);
                    binding.layoutBusinessInfo.btnToggleAbout.setVisibility(View.GONE);
                    binding.layoutBusinessInfo.invisibleAboutView.setVisibility(View.VISIBLE);
                }
            }
        }, 500);
    }

    private void setAddress(BusinessDetailInfo businessDetailInfo)
    {
        StringBuilder strBusinessType = new StringBuilder();
        strBusinessType.append(getString(R.string.lbl_business_type)).append(" : ")
                .append(businessDetailInfo.businessType);

        StringBuilder strAddress = new StringBuilder();
        if (businessDetailInfo.address1.isEmpty())
        {
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        else
        {
            strAddress.append(businessDetailInfo.address1.trim());
            strAddress.append(" ");
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.city.isEmpty())
        {
            strAddress.append(businessDetailInfo.city.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.country.isEmpty())
        {
            strAddress.append(businessDetailInfo.country.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.postcode.isEmpty())
        {
            strAddress.append(businessDetailInfo.postcode.trim());
        }
        String businessAddress = strAddress.toString();
        binding.layoutBusinessInfo.tvAddress.setText(strAddress.toString());
        String phoneNumber = businessDetailInfo.contact_person_phone;
        String website = businessDetailInfo.website;
        binding.layoutBusinessInfo.tvPhoneNumber.setText(phoneNumber);
        binding.layoutBusinessInfo.tvWebsite.setText(website);
    }

    private void setDiscount(BusinessDetailInfo businessDetailInfo)
    {
        if (businessDetailInfo.currentPromotionValue != null)
        {
            String discount = businessDetailInfo.currentPromotionValue;
            if (discount.trim().equals("")) discount = "0";
            StringBuilder strDiscount = new StringBuilder();
            strDiscount.append(getString(R.string.lbl_the)).append(" ");
            strDiscount.append(discount).append(getString(R.string.lbl_percentage_symbol));
            strDiscount.append(getString(R.string.lbl_discount_message_info));
            binding.layoutBusinessInfo.tvDiscount.setText(strDiscount.toString());
        }
    }

    private void setTimingsAndSplitTimings(BusinessDetailInfo businessDetailInfo)
    {
        JSONObject timing = null;
        JSONObject splitTimings = null;
        StringBuilder monTime = new StringBuilder();
        StringBuilder tueTime = new StringBuilder();
        StringBuilder wedTime = new StringBuilder();
        StringBuilder thuTime = new StringBuilder();
        StringBuilder friTime = new StringBuilder();
        StringBuilder satTime = new StringBuilder();
        StringBuilder sunTime = new StringBuilder();

        try
        {

            if (businessDetailInfo.timings != null)
            {
                if (!businessDetailInfo.equals(""))
                {
                    timing = new JSONObject(businessDetailInfo.timings);
                    monTime.append(timing.optString("Monday"));
                    tueTime.append(timing.optString("Tuesday"));
                    wedTime.append(timing.optString("Wednesday"));
                    thuTime.append(timing.optString("Thursday"));
                    friTime.append(timing.optString("Friday"));
                    satTime.append(timing.optString("Saturday"));
                    sunTime.append(timing.optString("Sunday"));
                }
            }

            if (businessDetailInfo.splitTimings != null)
            {
                if (!businessDetailInfo.splitTimings.equals(""))
                {
                    splitTimings = new JSONObject(businessDetailInfo.splitTimings);
                    monTime.append(" | ").append(splitTimings.optString("Monday"));
                    tueTime.append(" | ").append(splitTimings.optString("Tuesday"));
                    wedTime.append(" | ").append(splitTimings.optString("Wednesday"));
                    thuTime.append(" | ").append(splitTimings.optString("Thursday"));
                    friTime.append(" | ").append(splitTimings.optString("Friday"));
                    satTime.append(" | ").append(splitTimings.optString("Saturday"));
                    sunTime.append(" | ").append(splitTimings.optString("Sunday"));
                }

            }

            binding.layoutBusinessInfo.tvTimeMon.setText(monTime.toString());
            binding.layoutBusinessInfo.tvTimeTue.setText(tueTime.toString());
            binding.layoutBusinessInfo.tvTimeWed.setText(wedTime.toString());
            binding.layoutBusinessInfo.tvTimeThu.setText(thuTime.toString());
            binding.layoutBusinessInfo.tvTimeFri.setText(friTime.toString());
            binding.layoutBusinessInfo.tvTimeSat.setText(satTime.toString());
            binding.layoutBusinessInfo.tvTimeSun.setText(sunTime.toString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setLatLngValues(BusinessDetailInfo businessDetailInfo)
    {
        Double lat, lng;
        if (businessDetailInfo.lat.equals(""))
        {
            lat = Double.parseDouble("0.0");
        }
        else
        {
            lat = Double.parseDouble(businessDetailInfo.lat);
        }
        if (businessDetailInfo.lng.equals(""))
        {
            lng = Double.parseDouble("0.0");
        }
        else
        {
            lng = Double.parseDouble(businessDetailInfo.lng);
        }


    }

    private void setImageAndPromotions(final BusinessDetail businessDetail)
    {
        if (businessDetail.businessImages != null)
        {
            if (businessDetail.businessImages.size() > 0)
            {
                setAdapterForImages(businessDetail.businessImages);
            }
            else binding.layoutBusinessInfo.cvImages.setVisibility(View.GONE);
        }

        else binding.layoutBusinessInfo.cvImages.setVisibility(View.GONE);

        if (businessDetail.retailerPromotions != null)
        {
            setRetailerPromotions(businessDetail.retailerPromotions);
        }
    }

    /**
     * setHeygenie Image
     *
     * @param businessDetailInfo
     */
    private void setHygieneImage(BusinessDetailInfo businessDetailInfo)
    {
        if (!businessDetailInfo.hygieneImageUrl.equals(""))
        {
            binding.layoutBusinessInfo.ivHygine.setVisibility(View.VISIBLE);

            Picasso.with(getActivity()).load(businessDetailInfo.hygieneImageUrl)
                    .placeholder(R.drawable.progress_animation).error(R.drawable.app_logo).into(binding.layoutBusinessInfo.ivHygine);
        }
        else
        {
            binding.layoutBusinessInfo.ivHygine.setVisibility(View.GONE);
        }
    }

    /**
     * Set Retailor Promotions
     *
     * @param retailerPromotions
     */
    private void setRetailerPromotions(List<RetailerPromotion> retailerPromotions)
    {
        if (retailerPromotions != null)
        {
            if (retailerPromotions.size() > 0)
            {
                for (RetailerPromotion retailerPromotion : retailerPromotions)
                {
                    addLayout(retailerPromotion);
                }
            }
            else setDefaultPromotions();
        }
        else setDefaultPromotions();

    }

    /**
     * Set default promotions if promotions not available
     */
    private void setDefaultPromotions()
    {
        List<RetailerPromotion> defaultPromotions = getDefaultPromotions();

        for (RetailerPromotion retailerPromotion : defaultPromotions)
        {
            addLayout(retailerPromotion);
        }
    }

    private void setEmptyPromotions()
    {
        TextView tvEmptyText = new TextView(getActivity());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
        tvEmptyText.setText(getString(R.string.lbl_no_rewards));
        binding.layoutBusinessInfo.llLVRewardsData.addView(tvEmptyText);
    }

    /**
     * show error message when display business info
     *
     * @param strMesssage
     */
    private void showInfoErrorMessage(String strMesssage)
    {
        DialogUtility.showMessageWithOk(strMesssage, getActivity());
        getActivity().onBackPressed();
    }

    /**
     * Get default promotions to show the user.
     *
     * @return
     */
    public List<RetailerPromotion> getDefaultPromotions()
    {
        List<RetailerPromotion> promotions = new ArrayList<>();
        RetailerPromotion retailerPromotion = new RetailerPromotion();
        retailerPromotion.promotionName = "";
        retailerPromotion.startdate = "";
        retailerPromotion.enddate = "";
        retailerPromotion.MONMorning = "";
        retailerPromotion.MONAfternoon = "";
        retailerPromotion.MONEvening = "";
        retailerPromotion.MONFullday = "";

        retailerPromotion.TUEMorning = "";
        retailerPromotion.TUEAfternoon = "";
        retailerPromotion.TUEEvening = "";
        retailerPromotion.TUEFullday = "";

        retailerPromotion.WEDMorning = "";
        retailerPromotion.WEDAfternoon = "";
        retailerPromotion.WEDEvening = "";
        retailerPromotion.WEDFullday = "";

        retailerPromotion.THUMorning = "";
        retailerPromotion.THUAfternoon = "";
        retailerPromotion.THUEvening = "";
        retailerPromotion.THUFullday = "";

        retailerPromotion.FRIMorning = "";
        retailerPromotion.FRIAfternoon = "";
        retailerPromotion.FRIEvening = "";
        retailerPromotion.FRIFullday = "";

        retailerPromotion.SATMorning = "";
        retailerPromotion.SATAfternoon = "";
        retailerPromotion.SATEvening = "";
        retailerPromotion.SATFullday = "";

        retailerPromotion.SUNMorning = "";
        retailerPromotion.SUNAfternoon = "";
        retailerPromotion.SUNEvening = "";
        retailerPromotion.SUNFullday = "";
        promotions.add(retailerPromotion);
        return promotions;
    }

    /**
     * Set Images for business info
     *
     * @param imagesList
     */
    private void setAdapterForImages(final List<BusinessImages> imagesList)
    {
        eatsImageSliderViewPagerAdapter = new EatsImageSliderViewPagerAdapter(getActivity(), imagesList, true);
        eatsImageSliderViewPagerAdapter.setOnPagerItemClickListener(new EatsImageSliderViewPagerAdapter.OnPagerItemClickListener()
        {
            @Override
            public void onPagerItemClick(int position)
            {

            }
        });
        binding.layoutBusinessInfo.viewPagerImages.setAdapter(eatsImageSliderViewPagerAdapter);
    }

    private void setAdapterForDiscounts(final ArrayList<BusinessDetailDiscountForEatsDto> discountList)
    {
        eatsDiscountListAdapter = new EatsDiscountListAdapter(getActivity(), discountList);
        eatsDiscountListAdapter.setOnClickListener(new EatsDiscountListAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClick(int position)
            {

            }
        });
        binding.layoutBusinessInfo.rvDiscount.setAdapter(eatsDiscountListAdapter);
    }

    private void setAdapterForServices(final ArrayList<BusinessDetailServiceForEatsDto> serviceList)
    {
        eatsServiceListAdapter = new EatsServiceListAdapter(getActivity(), serviceList);
        eatsServiceListAdapter.setOnClickListener(new EatsServiceListAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClick(int position)
            {

            }
        });
        binding.layoutBusinessInfo.rvServices.setAdapter(eatsServiceListAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionsGranted = false;

        switch (requestCode)
        {
            case LOCATION_PERMISSION_REQUEST_CODE:
            {
                if (grantResults.length > 0)
                {
                    for (int i = 0; i < grantResults.length; i++)
                    {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                        {
                            mLocationPermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionsGranted = true;
                    setGPS();
                    //initialize our map
                }
            }
            break;
            case MY_MAKE_CALL_PERMISSION_REQUEST_CODE:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    // Permission granted
                    CallContact();
                }
                else
                {
                    // Permission denied
                }
            }
            break;
        }
    }

    public void initMap()
    {
        Log.d(TAG, "initMap: initializing map");
        if (mMap == null)
        {
            CustomMapFragment mapFragment = new CustomMapFragment();
            getChildFragmentManager().beginTransaction().replace(R.id.mapContainer, mapFragment, "CMF").commit();
            mapFragment.getMapAsync(new OnMapReadyCallback()
            {
                @Override
                public void onMapReady(GoogleMap googleMap)
                {
                    mMap = googleMap;
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    mMap.getUiSettings().setZoomControlsEnabled(true);
                    //Handling scroll and map and scroll view
                    mapFragment.setListener(new CustomMapFragment.OnTouchListener()
                    {
                        @Override
                        public void onTouch()
                        {
                            binding.nestedScrollView.requestDisallowInterceptTouchEvent(true);
                        }
                    });
                }
            });
        }
    }

    /**
     * Add marker to map
     */
    public void showOnMap(final BusinessDetailInfo businessDetailInfo)
    {
        if (mMap != null)
        {
            double retailerLat = Double.parseDouble(businessDetailInfo.lat);
            double retailerLng = Double.parseDouble(businessDetailInfo.lng);
            retailerLocation = new LatLng(retailerLat, retailerLng);
            mMap.addMarker(new MarkerOptions().position(retailerLocation)
                    .title(businessDetailInfo.tradingname));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(retailerLocation, 16));
            if (latitude != 0 && longitude != 0)
            {
                if (isAdded())
                {
                    myLocation = new LatLng(latitude, longitude);//55.9402087,-3.1713107
                    mMap.addMarker(new MarkerOptions().position(myLocation)
                            .title(getString(R.string.lbl_your_location)));
                    //FetchUrl fetchUrl = new FetchUrl();
                    //fetchUrl.execute(getUrl(myLocation,retailerLocation));
                }
            }
        }
        else
        {
            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    showOnMap(businessDetailInfo);
                }
            }, 3000);
        }
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btnToggleAbout:
                 expandOrCollapseText();
                 break;
            case R.id.tvPhoneNumber:
                 if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                 {
                    checkPermission();
                 }
                 else
                 {
                    CallContact();
                 }
                 break;
            case R.id.tvWebsite:
                 openWebsite(binding.layoutBusinessInfo.tvWebsite.getText().toString());
                 break;
            case R.id.btnRetry:
                 fetchBusinessInfo(businessDetail.businessDetails.get(0).business_id);
                 break;
            case R.id.btnActionContactMe:
                 gotoContactMeFragment();
                 break;
            case R.id.btnAddReview:
                 goToAddReviewFragment();
                 break;
        }
    }

    private void gotoContactMeFragment()
    {
        //ContactMeFragment contactMeFragment = ContactMeFragment.newInstance(businessID);
        //hostActivityListener.updateFragment(contactMeFragment,false,ContactMeFragment.class.getName(),true);

        Intent intent = new Intent(requireActivity(), ContactMeActivity.class);
        intent.putExtra(AppUtils.BUNDLE_BUSINESS_ID, businessDetailInfo.business_id);
        startActivity(intent);
    }

    /**
     * Move to the add review fragment
     */
    public void goToAddReviewFragment()
    {
        final Bundle bundle = new Bundle();
        bundle.putString("section_type", "info");
        bundle.putString("order_id", "");
        bundle.putString("businessID", businessDetailInfo.business_id);
        bundle.putString("businessImage", businessDetailInfo.businessCoverImage);
        bundle.putString("businessName", businessDetailInfo.tradingname);
        bundle.putString("businessType", businessDetailInfo.businessType);
        bundle.putString("businessAddress", getAddress());

        switch (navigationType)
        {
            case AppUtils.BMT_EATS:
            case AppUtils.BMT_LOCAL:
                ReviewsAddFragment reviewsAddFragment = ReviewsAddFragment.newInstance();
                 reviewsAddFragment.setArguments(bundle);
                 hostActivityListener.updateFragment(reviewsAddFragment, true, ReviewsAddFragment.class.getName(), true);
                 break;
        }
    }

    private void expandOrCollapseText()
    {
        if (binding.layoutBusinessInfo.tvAboutDesc.isExpanded())
        {
            binding.layoutBusinessInfo.tvAboutDesc.collapse();
            binding.layoutBusinessInfo.btnToggleAbout.setText(R.string.lbl_expand);
        }
        else
        {
            binding.layoutBusinessInfo.tvAboutDesc.expand();
            binding.layoutBusinessInfo.btnToggleAbout.setText(R.string.lbl_collapse);
        }
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }

    private void addLayout(RetailerPromotion retailerPromotion)
    {
        try
        {
            if (getContext() != null)
            {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.view_lolo_rewards, null, false);

                TextView tv_name = view.findViewById(R.id.tv_name);
                TextView tv_start = view.findViewById(R.id.tv_start);
                TextView tv_end = view.findViewById(R.id.tv_end);
                TextView tv_mm = view.findViewById(R.id.tv_mm);
                TextView tv_mf = view.findViewById(R.id.tv_mf);
                TextView tv_me = view.findViewById(R.id.tv_me);
                TextView tv_tm = view.findViewById(R.id.tv_tm);
                TextView tv_tf = view.findViewById(R.id.tv_tf);
                TextView tv_te = view.findViewById(R.id.tv_te);
                TextView tv_wm = view.findViewById(R.id.tv_wm);
                TextView tv_wf = view.findViewById(R.id.tv_wf);
                TextView tv_we = view.findViewById(R.id.tv_we);
                TextView tv_thm = view.findViewById(R.id.tv_thm);
                TextView tv_thf = view.findViewById(R.id.tv_thf);
                TextView tv_the = view.findViewById(R.id.tv_the);
                TextView tv_fm = view.findViewById(R.id.tv_fm);
                TextView tv_ff = view.findViewById(R.id.tv_ff);
                TextView tv_fe = view.findViewById(R.id.tv_fe);
                TextView tv_sm = view.findViewById(R.id.tv_sm);
                TextView tv_sf = view.findViewById(R.id.tv_sf);
                TextView tv_se = view.findViewById(R.id.tv_se);
                TextView tv_sum = view.findViewById(R.id.tv_sum);
                TextView tv_suf = view.findViewById(R.id.tv_suf);
                TextView tv_sue = view.findViewById(R.id.tv_sue);

                tv_name.setText(retailerPromotion.promotionName);

                DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
                DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");

                try
                {
                    String start = retailerPromotion.startdate;

                    if (start != null)
                    {
                        if (!start.equals(""))
                            tv_start.setText(": " + outputFormat.format(formatter.parse(start)));
                    }


                    String end = retailerPromotion.enddate;

                    if (end != null)
                    {
                        if (!end.equals(""))
                            tv_end.setText(": " + outputFormat.format(formatter.parse(end)));
                    }
                }

                catch (Exception ex)
                {
                    ex.printStackTrace();
                }

                tv_mm.setText(retailerPromotion.MONMorning);
                tv_mf.setText(retailerPromotion.MONAfternoon);
                tv_me.setText(retailerPromotion.MONEvening);
                tv_tm.setText(retailerPromotion.TUEMorning);
                tv_tf.setText(retailerPromotion.TUEAfternoon);
                tv_te.setText(retailerPromotion.TUEEvening);
                tv_wm.setText(retailerPromotion.WEDMorning);
                tv_wf.setText(retailerPromotion.WEDAfternoon);
                tv_we.setText(retailerPromotion.WEDEvening);
                tv_thm.setText(retailerPromotion.THUMorning);
                tv_thf.setText(retailerPromotion.THUAfternoon);
                tv_the.setText(retailerPromotion.THUEvening);
                tv_fm.setText(retailerPromotion.FRIMorning);
                tv_ff.setText(retailerPromotion.FRIAfternoon);
                tv_fe.setText(retailerPromotion.FRIEvening);
                tv_sm.setText(retailerPromotion.SATMorning);
                tv_sf.setText(retailerPromotion.SATAfternoon);
                tv_se.setText(retailerPromotion.SATEvening);
                tv_sum.setText(retailerPromotion.SUNMorning);
                tv_suf.setText(retailerPromotion.SUNAfternoon);
                tv_sue.setText(retailerPromotion.SUNEvening);

                binding.layoutBusinessInfo.llLVRewardsData.addView(view);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    // Custom method to call a phone number programmatically using dialer app
    protected void CallContact()
    {

        // Initialize an intent to make phone call using dialer app
        // It open the dialer app and make call to the given phone number automatically
        Intent intent = new Intent(Intent.ACTION_CALL);
        // Send phone number to intent as data
        intent.setData(Uri.parse("tel:" + binding.layoutBusinessInfo.tvPhoneNumber.getText().toString()));
        // Start the dialer app activity to make phone call
        startActivity(intent);

    }

    private void openWebsite(String strUrl)
    {
        if (strUrl != null)
        {
            if (!strUrl.isEmpty())
            {
                if (!strUrl.startsWith("http://") && !strUrl.startsWith("https://"))
                    strUrl = "http://" + strUrl;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(strUrl));
                startActivity(browserIntent);
            }
        }
    }

    protected void checkPermission()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (getActivity().checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)
            {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE))
                {
                    // show an alert dialog
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage("Call Phone permission is required.");
                    builder.setTitle("Please grant permission");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i)
                        {
                            ActivityCompat.requestPermissions(
                                    getActivity(),
                                    new String[]{Manifest.permission.CALL_PHONE},
                                    MY_MAKE_CALL_PERMISSION_REQUEST_CODE
                            );
                        }
                    });
                    builder.setNeutralButton("Cancel", null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else
                {
                    // Request permission
                    ActivityCompat.requestPermissions(
                            getActivity(),
                            new String[]{Manifest.permission.CALL_PHONE},
                            MY_MAKE_CALL_PERMISSION_REQUEST_CODE
                    );
                }
            }
            else
            {
                // Permission already granted
                CallContact();
            }
        }
    }

    @Override
    public void locationEnabled(double latitude, double longitude)
    {
        super.locationEnabled(latitude, longitude);
        if (!locationEnabled)
        {
            getLocationPermission();
        }
    }

    @Override
    public void onStop()
    {
        super.onStop();
        locationEnabled = false;
    }

    private void showProgress()
    {
        binding.layoutBusinessInfo.cnlBusinessInfo.setVisibility(View.GONE);
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showContents()
    {
        binding.layoutBusinessInfo.cnlBusinessInfo.setVisibility(View.VISIBLE);
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage, boolean retry)
    {
        binding.layoutBusinessInfo.cnlBusinessInfo.setVisibility(View.GONE);
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if (retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

    private String getAddress()
    {
        StringBuilder strAddress = new StringBuilder();
        if (businessDetailInfo.address1.isEmpty())
        {
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        else
        {
            strAddress.append(businessDetailInfo.address1.trim());
            strAddress.append(" ");
            strAddress.append(businessDetailInfo.address2.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.city.isEmpty())
        {
            strAddress.append(businessDetailInfo.city.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.country.isEmpty())
        {
            strAddress.append(businessDetailInfo.country.trim());
            strAddress.append(" ");
        }
        if (!businessDetailInfo.postcode.isEmpty())
        {
            strAddress.append(businessDetailInfo.postcode.trim());
        }
        return strAddress.toString();
    }

}
