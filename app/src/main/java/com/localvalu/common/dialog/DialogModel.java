package com.localvalu.common.dialog;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class DialogModel implements Parcelable
{
    private String title;
    private String message;
    private String positiveButtonText;
    private String negativeButtonText;
    private String neutralButtonText;
    private boolean fromActivity;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getPositiveButtonText()
    {
        return positiveButtonText;
    }

    public void setPositiveButtonText(String positiveButtonText)
    {
        this.positiveButtonText = positiveButtonText;
    }

    public String getNegativeButtonText()
    {
        return negativeButtonText;
    }

    public void setNegativeButtonText(String negativeButtonText)
    {
        this.negativeButtonText = negativeButtonText;
    }

    public String getNeutralButtonText()
    {
        return neutralButtonText;
    }

    public void setNeutralButtonText(String neutralButtonText)
    {
        this.neutralButtonText = neutralButtonText;
    }

    public boolean isFromActivity()
    {
        return fromActivity;
    }

    public void setFromActivity(boolean fromActivity)
    {
        this.fromActivity = fromActivity;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(this.title);
        dest.writeString(this.message);
        dest.writeString(this.positiveButtonText);
        dest.writeString(this.negativeButtonText);
        dest.writeString(this.neutralButtonText);
    }

    public DialogModel()
    {
    }

    protected DialogModel(Parcel in)
    {
        this.title = in.readString();
        this.message = in.readString();
        this.positiveButtonText = in.readString();
        this.negativeButtonText = in.readString();
        this.neutralButtonText = in.readString();
    }

    public static final Creator<DialogModel> CREATOR = new Creator<DialogModel>()
    {
        @Override
        public DialogModel createFromParcel(Parcel source)
        {
            return new DialogModel(source);
        }

        @Override
        public DialogModel[] newArray(int size)
        {
            return new DialogModel[size];
        }
    };
}
