package com.localvalu.common.dialog;

public interface DialogCallBack
{
    void onPositiveButtonClicked();
    void onNegativeButtonClicked();
    void onNeutralButtonClicked();
}
