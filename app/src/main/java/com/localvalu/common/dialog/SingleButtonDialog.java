package com.localvalu.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.localvalu.BuildConfig;
import com.localvalu.R;
import com.localvalu.databinding.FragmentAddNewAddressBinding;
import com.localvalu.databinding.FragmentDialogSingleButtonBinding;
import com.localvalu.directory.scanpay.dto.BusinessDetail;
import com.localvalu.user.manageaddress.ui.AddressUpdatedListener;
import com.utility.AppUtils;

public class SingleButtonDialog extends DialogFragment
{
    public static final String TAG = SingleButtonDialog.class.getName();

    private FragmentDialogSingleButtonBinding binding;
    private DialogCallBack dialogCallBack;
    private DialogModel dialogModel;

    public static SingleButtonDialog newInstance(DialogModel dialogModel)
    {
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppUtils.BUNDLE_DIALOG_MODEL,dialogModel);
        SingleButtonDialog singleButtonDialog = new SingleButtonDialog();
        singleButtonDialog.setArguments(bundle);
        return singleButtonDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        readFromBundle();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        try
        {
            dialogCallBack = (DialogCallBack) context;
        }
        catch (Exception ex)
        {
            if(BuildConfig.DEBUG)
            {
                Log.d(TAG," Host class must implement dialog listener\"");
            }

        }

    }

    private void readFromBundle()
    {
        if (getArguments() != null)
        {
            dialogModel =  getArguments().getParcelable(AppUtils.BUNDLE_DIALOG_MODEL);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        binding = FragmentDialogSingleButtonBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        if(!dialogModel.isFromActivity())
        {
            dialogCallBack = (DialogCallBack) getTargetFragment();
        }
        if(dialogModel!=null)
        {
            binding.tvDialogTitle.setText(dialogModel.getTitle());
            binding.tvDialogMessage.setText(dialogModel.getMessage());
        }
        binding.btnOK.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if(dialogCallBack!=null)
                {
                    dismiss();
                    dialogCallBack.onNeutralButtonClicked();
                }
            }
        });
    }

}
