package com.localvalu.common.referfriend;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.localvalu.BuildConfig;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentReferFriendsBinding;
import com.localvalu.user.dto.UserBasicInfo;
import com.utility.PreferenceUtility;

public class ReferFriendsFragment extends BaseFragment
{

    private int colorEats;
    private UserBasicInfo userBasicInfo;
    private HostActivityListener hostActivityListener;
    private FragmentReferFriendsBinding binding;

    public static ReferFriendsFragment newInstance()
    {
        return new ReferFriendsFragment();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentReferFriendsBinding.inflate(inflater,container,false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setProperties();
    }

    private void setProperties()
    {
        if (userBasicInfo.qRCodeOutput == null)
        {
            userBasicInfo.qRCodeOutput = "0";
        }
        binding.tvMyCode.setText("My Code : " + userBasicInfo.qRCodeOutput);

        binding.tvFacebook.setOnClickListener(_OnClickListener);
        binding.tvTwitter.setOnClickListener(_OnClickListener);
        binding.tvLinkedIn.setOnClickListener(_OnClickListener);
        binding.tvInstagram.setOnClickListener(_OnClickListener);
        binding.btnShare.setOnClickListener(_OnClickListener);
        binding.tvTikTok.setOnClickListener(_OnClickListener);

        try
        {
            binding.ivQRCode.setImageBitmap(encodeAsBitmap(userBasicInfo.qRCodeOutput));
        }
        catch (WriterException e)
        {
            e.printStackTrace();
        }

    }

    private void setUpActionBar()
    {
        binding.toolbarLayout.tvTitle.setTextColor(colorEats);
        binding.toolbarLayout.tvTitle.setText(getString(R.string.lbl_refer_and_earn));
        binding.toolbarLayout.toolbar.getNavigationIcon().setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                 getActivity().onBackPressed();
                 break;
            case R.id.tvFacebook:
                 openSocialMediaPage("https://www.facebook.com/login.php?skip_api_login=1&api_key=966242223397117&signed_next=1&next=https%3A%2F%2Fwww.facebook.com%2Fsharer.php%3Fu%3Dhttps%253A%252F%252Fwww.oiyaa.com%252Fhome%252FuserSignUp%252FSURMMTAwMDAzNDg%253D&cancel_url=https%3A%2F%2Fwww.facebook.com%2Fdialog%2Fclose_window%2F%3Fapp_id%3D966242223397117%26connect%3D0%23_%3D_&display=popup&locale=ta_IN");
                 break;
            case R.id.tvTwitter:
                 openSocialMediaPage("https://twitter.com/intent/tweet?url=https%3A%2F%2Fwww.oiyaa.com%2Fhome%2FuserSignUp%2FSURMMTAwMDAzNDg%3D&text=Simple%20Share%20Buttons&hashtags=simplesharebuttons");
                 break;
            case R.id.tvInstagram:
                 openSocialMediaPage("https://www.instagram.com/wearelocalvalu/");
                 break;
            case R.id.tvLinkedIn:
                 openSocialMediaPage("https://www.linkedin.com/uas/login?session_redirect=https%3A%2F%2Fwww.linkedin.com%2FshareArticle%3Fmini%3Dtrue%26url%3Dhttps%3A%2F%2Fwww.oiyaa.com%2Fhome%2FuserSignUp%2FSURMMTAwMDAzNDg%3D");
                 break;
            case R.id.tvTikTok:
                 openSocialMediaPage("https://vm.tiktok.com/ZM87pnvsm/");
                 break;
            case R.id.btnShare:
                 shareTheApp();
                 break;
        }
    }

    private void shareTheApp()
    {
        try
        {
            StringBuilder strUrl = new StringBuilder();
            strUrl.append("https://www.oiyaa.com/").append("\n");
            strUrl.append("Download the app at " ).append("https://play.google.com/store/apps/details?id=");
            strUrl.append(BuildConfig.APPLICATION_ID).append("\n").append("Use the referral code - ");
            strUrl.append(userBasicInfo.qRCodeOutput);

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            shareIntent.putExtra(Intent.EXTRA_TEXT, strUrl.toString());
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        }
        catch(Exception e)
        {
            //e.toString();
        }

    }

    private void openSocialMediaPage(String strUrl)
    {
        try
        {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(strUrl));
            startActivity(intent);
        }
        catch (Exception ex)
        {

        }

    }

    private Bitmap encodeAsBitmap(String str) throws WriterException
    {
        BitMatrix result;
        try
        {
            result = new MultiFormatWriter().encode(str, BarcodeFormat.QR_CODE, 120, 120, null);
        }
        catch (IllegalArgumentException iae)
        {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++)
        {
            int offset = y * w;
            for (int x = 0; x < w; x++)
            {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 120, 0, 0, w, h);
        return bitmap;
    }


}
