package com.localvalu.common;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.ViewContactMeBinding;
import com.localvalu.directory.detail.wrapper.SaveRequestSlotResultWrapper;
import com.localvalu.user.ValidationUtils;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AppUtils;
import com.utility.CommonUtilities;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;
import com.utility.validators.RangeValidator;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactMeFragmentEmbedded extends BaseFragment
{
    String TAG = ContactMeFragmentEmbedded.class.getName();

    public View view;
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private ViewContactMeBinding binding;
    private int colorEats;
    private HostActivityListener hostActivityListener;
    private String businessID;
    private Date contactMePreferredDate;
    private int originalMode;
    Calendar calendarStart = Calendar.getInstance();
    Calendar calendarEnd = Calendar.getInstance();

    public static ContactMeFragmentEmbedded newInstance(String paramBusinessId)
    {
        Bundle bundle = new Bundle();
        bundle.putString(AppUtils.BUNDLE_BUSINESS_ID,paramBusinessId);
        ContactMeFragmentEmbedded contactMeFragment = new ContactMeFragmentEmbedded();
        contactMeFragment.setArguments(bundle);
        return contactMeFragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        buildObjectForHandlingAPI();
        readFromBundle();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = ViewContactMeBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        binding.btnContactMeSubmit.setOnClickListener(_OnClickListener);
        binding.etPreferDate.setOnClickListener(_OnClickListener);
        binding.etPreferTime.setOnClickListener(_OnClickListener);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        binding.getRoot().requestLayout();
    }

    private void readFromBundle()
    {
        try
        {
            businessID = getArguments().getString(AppUtils.BUNDLE_BUSINESS_ID);
        }
        catch (Exception e)
        {
            businessID = "";
        }
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.btnContactMeSubmit:
                     if(areAllInputsValid())
                     {
                         callAPIForGetQuote();
                     }
                     break;
                case R.id.etPreferDate:
                     if(contactMePreferredDate==null) contactMePreferredDate = new Date();
                     showContactMeDatePicker(binding.etPreferDate);
                     break;
                case R.id.etPreferTime:
                     if(contactMePreferredDate==null) contactMePreferredDate = new Date();
                     showContactMeTimePicker(binding.etPreferTime);
                     break;
            }
        }
    };

    /**
     * Call for get the quote from server
     * Contact Me Api.
     */
    private void callAPIForGetQuote()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("userId", userBasicInfo.userId);
            objMain.put("BusinessId", businessID);
            objMain.put("requestFor", "quotebook");
            objMain.put("fullName", binding.etFullName.getText().toString());
            objMain.put("mobile", binding.etMobileNumber.getText().toString());
            objMain.put("email", binding.etEmailId.getText().toString());
            String strPreferDate = DateUtility.getServerFormatDateByDate(contactMePreferredDate);
            String strPreferTime = DateUtility.get24HoursTimeFormat(contactMePreferredDate);
            objMain.put("PreferredDate",strPreferDate);
            objMain.put("PreferredTime", strPreferTime);
            objMain.put("comment", binding.etComment.getText().toString());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());
        Call<SaveRequestSlotResultWrapper> dtoCall = apiInterface.requestCallback(body);
        dtoCall.enqueue(new Callback<SaveRequestSlotResultWrapper>()
        {
            @Override
            public void onResponse(Call<SaveRequestSlotResultWrapper> call, Response<SaveRequestSlotResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().requestResponseResult.errorDetails.errorCode == 0)
                    {
                        binding.etFullName.setText("");
                        binding.etMobileNumber.setText("");
                        binding.etEmailId.setText("");
                        binding.etPreferDate.setText("");
                        binding.etPreferTime.setText("");
                        binding.etComment.setText("");

                        Toast.makeText(getActivity(), "Your callback DashboardRequest saved successfully.", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().requestResponseResult.errorDetails.errorMessage, getActivity());
                    }
                } catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<SaveRequestSlotResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                {
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                }
                else
                {
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
                }
            }
        });
    }

    public boolean areAllInputsValid()
    {
        if(isValidUserName() && isValidPhoneNumber() && isValidEmail() && isValidPreferDate() && isValidPreferTime() && isValidComment())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean isValidUserName()
    {
        if(binding.etFullName.getText().toString().isEmpty())
        {
            binding.etFullName.setError(getString(R.string.lbl_name_empty));
            binding.etFullName.requestFocus();
            return false;
        }
        else
        {
            binding.etFullName.setError(null);
            return true;
        }
    }

    public boolean isValidPhoneNumber()
    {
        if(binding.etMobileNumber.getText().toString().isEmpty())
        {
            binding.etMobileNumber.setError(getString(R.string.lbl_mobile_number_empty));
            binding.etMobileNumber.requestFocus();
            return false;
        }
        else if(!ValidationUtils.isValidMobileNo(binding.etMobileNumber.getText().toString().trim()))
        {
            binding.etMobileNumber.setError(getString(R.string.lbl_invalid_mobile_number));
            binding.etMobileNumber.requestFocus();
            return false;
        }
        else
        {
            binding.etMobileNumber.setError(null);
            return true;
        }
    }

    public boolean isValidEmail()
    {
        if(binding.etEmailId.getText().toString().isEmpty())
        {
            binding.etMobileNumber.setError(getString(R.string.lbl_mobile_number_empty));
            binding.etMobileNumber.requestFocus();
            return false;
        }
        else if(!Patterns.EMAIL_ADDRESS.matcher(binding.etEmailId.getText().toString().trim()).matches())
        {
            binding.etMobileNumber.setError(getString(R.string.msg_enter_valid_email_id));
            binding.etMobileNumber.requestFocus();
            return false;
        }
        else
        {
            binding.etMobileNumber.setError(null);
            return true;
        }
    }

    public boolean isValidPreferDate()
    {
        if(binding.etPreferDate.getText().toString().isEmpty())
        {
            binding.etPreferDate.setError(getString(R.string.lbl_prefer_date_empty));
            binding.etPreferDate.requestFocus();
            return false;
        }
        else
        {
            binding.etPreferDate.setError(null);
            return true;
        }
    }

    public boolean isValidPreferTime()
    {
        if(binding.etPreferTime.getText().toString().isEmpty())
        {
            binding.etPreferTime.setError(getString(R.string.lbl_prefer_time_empty));
            binding.etPreferTime.requestFocus();
            return false;
        }
        else
        {
            binding.etPreferTime.setError(null);
            return true;
        }
    }

    public boolean isValidComment()
    {
        if(binding.etComment.getText().toString().isEmpty())
        {
            binding.etComment.setError(getString(R.string.lbl_comment_empty));
            binding.etComment.requestFocus();
            return false;
        }
        else
        {
            binding.etComment.setError(null);
            return true;
        }
    }

    private void showContactMeDatePicker(TextView textView)
    {

        calendarEnd.add(Calendar.YEAR, 5);

        calendarEnd.add(Calendar.YEAR, 5);
        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        //builder.setTheme(R.style.AppThemeLocal);
        builder.setCalendarConstraints(contactMeDateLimitRange().build());
        MaterialDatePicker picker = builder.build();


        picker.show(getChildFragmentManager(), picker.toString());
        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener()
        {
            @Override
            public void onPositiveButtonClick(Object selection)
            {
                long milliseconds = (long) selection;
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTimeInMillis(milliseconds);
                textView.setText(convertDateToStringInUKFormat(selectedDate.getTime()));
                contactMePreferredDate = selectedDate.getTime();
            }
        });
    }

    private void showContactMeTimePicker(TextView textView)
    {
        final Calendar cldr = Calendar.getInstance();
        cldr.setTime(contactMePreferredDate);
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);
        int hour = cldr.get(Calendar.HOUR);
        int minute = cldr.get(Calendar.MINUTE);
        int second = cldr.get(Calendar.SECOND);

        TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),
                R.style.AppTheme,_OnTimeSetListener,hour,minute,false);
        timePickerDialog.show();
    }

    /*
    Limit selectable Date range
  */
    private CalendarConstraints.Builder contactMeDateLimitRange()
    {
        CalendarConstraints.Builder constraintsBuilderRange = new CalendarConstraints.Builder();

        constraintsBuilderRange.setStart(calendarStart.getTimeInMillis());
        constraintsBuilderRange.setEnd(calendarEnd.getTimeInMillis());
        constraintsBuilderRange.setValidator(new RangeValidator(calendarStart.getTimeInMillis(), calendarEnd.getTimeInMillis()));

        return constraintsBuilderRange;
    }

    private String convertDateToStringInUKFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    TimePickerDialog.OnTimeSetListener _OnTimeSetListener = new TimePickerDialog.OnTimeSetListener()
    {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute)
        {
            final Calendar cldr = Calendar.getInstance();
            cldr.setTime(contactMePreferredDate);
            cldr.set(Calendar.HOUR_OF_DAY,hourOfDay);
            cldr.set(Calendar.MINUTE,minute);
            contactMePreferredDate = cldr.getTime();
            binding.etPreferTime.setText(DateUtility.get12HoursTimeFormat(contactMePreferredDate));
        }
    };

}
