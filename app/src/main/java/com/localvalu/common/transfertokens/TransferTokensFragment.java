package com.localvalu.common.transfertokens;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.localvalu.BuildConfig;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.common.mytransaction.wrapper.UserDetailResultWrapper;
import com.localvalu.databinding.FragmentTransferTokensBinding;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.CommonUtilities;
import com.utility.CommonUtility;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransferTokensFragment extends BaseFragment
{
    private static final String TAG = "TransferTokensFragment";

    //Toolbar
    private int colorEats;

    private FragmentTransferTokensBinding binding;
    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;

    private String current_bal, transfer_amnt, available_bal;

    private String strCurrencySymbol;
    private String strCurrencyLetter;
    private HostActivityListener hostActivityListener;

    @Override
    public void onAttach(@NonNull Context context)
    {
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"onAttach");
        }
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"onCreate");
        }
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"onCreateView");
        }
        binding = FragmentTransferTokensBinding.inflate(inflater,container,false);
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        buildObjectForHandlingAPI();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"onViewCreated");
        }
        setUpActionBar();
        binding.tvPlus.setOnClickListener(_OnClickListener);
        binding.tvMinus.setOnClickListener(_OnClickListener);
        binding.btnSend.setOnClickListener(_OnClickListener);
        binding.btnCancel.setOnClickListener(_OnClickListener);
        fetchAPI();
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            onButtonClick(v);
        }
    };

    private void setUpActionBar()
    {
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"setUpActionBar");
        }
        binding.toolbarLayout.tvTitle.setText(getString(R.string.lbl_transfer_loyality_tokens));
        binding.toolbarLayout.tvTitle.setTextColor(colorEats);
        binding.toolbarLayout.toolbar.getNavigationIcon().setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"onDestroyView");
        }
    }

    private void fetchAPI()
    {
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"fetchAPI");
        }

        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "CustomerBalanceEnquiry");
            objMain.put("AccountId", userBasicInfo.accountId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                        if (response.body().userDetailResult.status.equalsIgnoreCase("true"))
                        {
                            current_bal = String.valueOf(response.body().userDetailResult.customerBalanceEnquiryData.currentTokenBalance);
                            transfer_amnt = "1";
                            available_bal = String.valueOf(Double.valueOf(response.body().userDetailResult.customerBalanceEnquiryData.currentTokenBalance) - 1);

                            binding.tvCurrentBalance.setText(strCurrencyLetter + current_bal);
                            binding.tvTransferAmount.setText("1");
                            binding.tvBalanceAfterTransfer.setText(strCurrencyLetter + available_bal);
                        }
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void callAPI()
    {
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"callAPI");
        }
        hideKeyboard();
        if(binding.etCustomerUniqueCode.getText().toString().isEmpty())
        {
            Toast.makeText(requireContext(),getString(R.string.lbl_customer_unique_code_empty),Toast.LENGTH_LONG).show();
            return;
        }
        else if(binding.etCustomerUniqueCode.getText().toString().equals(userBasicInfo.qRCodeOutput))
        {
            Toast.makeText(requireContext(),getString(R.string.lbl_not_allowed_to_this_account),Toast.LENGTH_LONG).show();
            return;
        }
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "TransferLTFriends");
            objMain.put("AccountId", userBasicInfo.accountId.toString().trim());
            objMain.put("CurrentBalance", current_bal);
            objMain.put("TransferAmount", transfer_amnt);
            objMain.put("BalanceAfterTransfer", available_bal);
            objMain.put("LTtoAccountID", binding.etCustomerUniqueCode.getText().toString().trim());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                        if (response.body().userDetailResult.status.equalsIgnoreCase("true"))
                        {
                            Toast.makeText(getActivity(), response.body().userDetailResult.msg, Toast.LENGTH_LONG).show();
                            CommonUtilities.alertSound(getActivity(), R.raw.coin_drops);

                            current_bal = String.valueOf(response.body().userDetailResult.transferLTFriendResults.newBalance);
                            transfer_amnt = "1";
                            available_bal = String.valueOf(Double.parseDouble(response.body().userDetailResult.transferLTFriendResults.newBalance) - 1);

                            binding.tvCurrentBalance.setText(strCurrencyLetter + current_bal);
                            binding.tvTransferAmount.setText("1");
                            binding.tvBalanceAfterTransfer.setText(strCurrencyLetter + available_bal);
                            binding.etCustomerUniqueCode.setText("IDL");
                        }
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk("Invalid  AccountID Or Customer Unique Code", getActivity());
//                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                 hostActivityListener.onBackButtonPressed();
                 break;
            case R.id.cnst_root:
                 CommonUtility.hideKeyboard(getActivity());
                 break;
            case R.id.ll_root:
                 CommonUtility.hideKeyboard(getActivity());
                 break;
            case R.id.btnSend:
                 callAPI();
                 break;
            case R.id.btnCancel:
                 binding.etCustomerUniqueCode.setText("IDL");
                 break;
            case R.id.tvMinus:
                if (Double.parseDouble(transfer_amnt) > 1)
                {
                    transfer_amnt = String.valueOf(Double.parseDouble(transfer_amnt) - 1);
                    available_bal = String.valueOf(Double.parseDouble(current_bal) - Double.parseDouble(transfer_amnt));
                }
                binding.tvCurrentBalance.setText(strCurrencyLetter + current_bal);
                binding.tvTransferAmount.setText(transfer_amnt);
                binding.tvBalanceAfterTransfer.setText(strCurrencyLetter + available_bal);
                break;
            case R.id.tvPlus:
                if (Double.parseDouble(transfer_amnt) < Double.parseDouble(current_bal))
                {
                    transfer_amnt = String.valueOf(Double.parseDouble(transfer_amnt) + 1);
                    available_bal = String.valueOf(Double.parseDouble(current_bal) - Double.parseDouble(transfer_amnt));
                }
                binding.tvCurrentBalance.setText(strCurrencyLetter + current_bal);
                binding.tvTransferAmount.setText(transfer_amnt);
                binding.tvBalanceAfterTransfer.setText(strCurrencyLetter + available_bal);
                break;
        }
    }

}
