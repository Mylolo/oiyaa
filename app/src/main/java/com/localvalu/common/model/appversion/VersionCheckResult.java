package com.localvalu.common.model.appversion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VersionCheckResult
{
    @SerializedName("AppVersionResult")
    @Expose
    private VersionCheck versionCheck;

    public VersionCheck getVersionCheck()
    {
        return versionCheck;
    }

    public void setVersionCheck(VersionCheck versionCheck)
    {
        this.versionCheck = versionCheck;
    }
}
