package com.localvalu.common.model.cuisinescategory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CuisineCategoryData
{
    @SerializedName("ID")
    @Expose
    private String id;

    @SerializedName("name")
    @Expose
    private String name;

    public boolean checkStatus;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
