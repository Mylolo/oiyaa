package com.localvalu.common.model.cuisinescategory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.model.ErrorDetails;

import java.util.ArrayList;

public class CuisinesCategoryListResponse
{
    @SerializedName(value = "cuisinesList",alternate = {"SearchResult"})
    @Expose
    private CuisinesCategoryListResult cuisinesCategoryListResult;

    public CuisinesCategoryListResult getCuisinesCategoryListResult()
    {
        return cuisinesCategoryListResult;
    }

    public void setCuisinesCategoryListResult(CuisinesCategoryListResult cuisinesCategoryListResult)
    {
        this.cuisinesCategoryListResult = cuisinesCategoryListResult;
    }
}
