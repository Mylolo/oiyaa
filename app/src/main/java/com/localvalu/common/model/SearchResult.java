package com.localvalu.common.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchResult
{
    @SerializedName("business_id")
    @Expose
    private String businessId;

    @SerializedName("HygieneRate")
    @Expose
    private String HygieneRate;

    @SerializedName("HygieneImageUrl")
    @Expose
    private String HygieneImageUrl;


    @SerializedName("firstname")
    @Expose
    private String firstname;


    @SerializedName("lastname")
    @Expose
    private String lastname;

    @SerializedName("BusinessCoverImage")
    @Expose
    private String BusinessCoverImage;

    @SerializedName("retailerId")
    @Expose
    private String retailerId;

    @SerializedName("tradingname")
    @Expose
    private String tradingname;

    @SerializedName("BusinessType")
    @Expose
    private String BusinessType;

    @SerializedName("RetailerType")
    @Expose
    private String RetailerType;

    @SerializedName("business_id")
    @Expose
    private String logo;

    @SerializedName("Website")
    @Expose
    private String Website;

    @SerializedName("contact_person_name")
    @Expose
    private String contactPersonName;

    @SerializedName("contact_person_Email")
    @Expose
    private String contactPersonEmail;

    @SerializedName("contact_person_phone")
    @Expose
    private String contactPersonPhone;

    @SerializedName("address1")
    @Expose
    private String address1;

    @SerializedName("address2")
    @Expose
    private String address2;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("county")
    @Expose
    private String county;

    @SerializedName("postcode")
    @Expose
    private String postcode;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("about")
    @Expose
    private String about;

    @SerializedName("timings")
    @Expose
    private String timings;

    @SerializedName("categories")
    @Expose
    private String categories;

    @SerializedName("typeoffoods")
    @Expose
    private String typeoffoods;

    @SerializedName("minOrderVall")
    @Expose
    private String minOrderVall;

    @SerializedName("lat")
    @Expose
    private String lat;

    @SerializedName("lng")
    @Expose
    private String lng;

    @SerializedName("CurrentPromotionValue")
    @Expose
    private String CurrentPromotionValue;

    @SerializedName("Is_Active")
    @Expose
    private String Is_Active;

    @SerializedName("UpdatedDate")
    @Expose
    private String UpdatedDate;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("OverAllreviewCount")
    @Expose
    private String OverAllreviewCount;

    @SerializedName("OverAllreviewRate")
    @Expose
    private String OverAllreviewRate;

    @SerializedName("OverAllProductRate")
    @Expose
    private String OverAllProductRate;

    @SerializedName("OverAllServiceRate")
    @Expose
    private String OverAllServiceRate;


    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getHygieneRate() {
        return HygieneRate;
    }

    public void setHygieneRate(String hygieneRate) {
        HygieneRate = hygieneRate;
    }

    public String getHygieneImageUrl() {
        return HygieneImageUrl;
    }

    public void setHygieneImageUrl(String hygieneImageUrl) {
        HygieneImageUrl = hygieneImageUrl;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBusinessCoverImage() {
        return BusinessCoverImage;
    }

    public void setBusinessCoverImage(String businessCoverImage) {
        BusinessCoverImage = businessCoverImage;
    }

    public String getRetailerId() {
        return retailerId;
    }

    public void setRetailerId(String retailerId) {
        this.retailerId = retailerId;
    }

    public String getTradingname() {
        return tradingname;
    }

    public void setTradingname(String tradingname) {
        this.tradingname = tradingname;
    }

    public String getBusinessType() {
        return BusinessType;
    }

    public void setBusinessType(String businessType) {
        BusinessType = businessType;
    }

    public String getRetailerType() {
        return RetailerType;
    }

    public void setRetailerType(String retailerType) {
        RetailerType = retailerType;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getWebsite() {
        return Website;
    }

    public void setWebsite(String website) {
        Website = website;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public void setContactPersonEmail(String contactPersonEmail) {
        this.contactPersonEmail = contactPersonEmail;
    }

    public String getContactPersonPhone() {
        return contactPersonPhone;
    }

    public void setContactPersonPhone(String contactPersonPhone) {
        this.contactPersonPhone = contactPersonPhone;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getTimings() {
        return timings;
    }

    public void setTimings(String timings) {
        this.timings = timings;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getTypeoffoods() {
        return typeoffoods;
    }

    public void setTypeoffoods(String typeoffoods) {
        this.typeoffoods = typeoffoods;
    }

    public String getMinOrderVall() {
        return minOrderVall;
    }

    public void setMinOrderVall(String minOrderVall) {
        this.minOrderVall = minOrderVall;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getCurrentPromotionValue() {
        return CurrentPromotionValue;
    }

    public void setCurrentPromotionValue(String currentPromotionValue) {
        CurrentPromotionValue = currentPromotionValue;
    }

    public String getIs_Active() {
        return Is_Active;
    }

    public void setIs_Active(String is_Active) {
        Is_Active = is_Active;
    }

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        UpdatedDate = updatedDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getOverAllreviewCount() {
        return OverAllreviewCount;
    }

    public void setOverAllreviewCount(String overAllreviewCount) {
        OverAllreviewCount = overAllreviewCount;
    }

    public String getOverAllreviewRate() {
        return OverAllreviewRate;
    }

    public void setOverAllreviewRate(String overAllreviewRate) {
        OverAllreviewRate = overAllreviewRate;
    }

    public String getOverAllProductRate() {
        return OverAllProductRate;
    }

    public void setOverAllProductRate(String overAllProductRate) {
        OverAllProductRate = overAllProductRate;
    }

    public String getOverAllServiceRate() {
        return OverAllServiceRate;
    }

    public void setOverAllServiceRate(String overAllServiceRate) {
        OverAllServiceRate = overAllServiceRate;
    }
}
