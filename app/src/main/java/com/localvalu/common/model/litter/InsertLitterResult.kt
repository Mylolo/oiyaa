package com.localvalu.common.model.litter

import com.google.gson.annotations.SerializedName
import com.localvalu.common.model.ErrorDetails

class InsertLitterResult(@SerializedName("ErrorDetails") val errorDetails:ErrorDetails,
                         @SerializedName("TokenEarned") val tokenEarned:String)
{

}