package com.localvalu.common.model.litter

import com.google.gson.annotations.SerializedName
import com.localvalu.common.model.ErrorDetails

class InsertLitterResponse(@SerializedName("InsertLitterdetails") val insertLitterDetails: InsertLitterDetails)
{
    
}