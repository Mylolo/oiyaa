package com.localvalu.common.model.appversion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.model.ErrorDetails;

public class VersionCheck
{
    @SerializedName("ErrorDetails")
    @Expose
    private ErrorDetails errorDetails;

    @SerializedName("versionNo")
    @Expose
    private String versionNo;

    @SerializedName("versioncode")
    @Expose
    private String versioncode;

    public String getVersionNo()
    {
        return versionNo;
    }

    public void setVersionNo(String versionNo)
    {
        this.versionNo = versionNo;
    }

    public ErrorDetails getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public String getVersioncode()
    {
        return versioncode;
    }

    public void setVersioncode(String versioncode)
    {
        this.versioncode = versioncode;
    }
}
