package com.localvalu.common.model.appversion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VersionCheckRequest
{
    @SerializedName("Token")
    @Expose
    public String token="/3+YFd5QZdSK9EKsB8+TlA==";

    @SerializedName("AccountId")
    private String accountId = "";

    @SerializedName("AppName")
    private final String appName = "1"; //For android

    @SerializedName("DeviceName")
    private final String deviceName = "1"; //For consumer

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getAccountId()
    {
        return accountId;
    }

    public void setAccountId(String accountId)
    {
        this.accountId = accountId;
    }

    public String getAppName()
    {
        return appName;
    }

    public String getDeviceName()
    {
        return deviceName;
    }
}
