package com.localvalu.common.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchRequest
{
    @SerializedName("Token")
    @Expose
    private String token="/3+YFd5QZdSK9EKsB8+TlA==";

    @SerializedName("Business")
    @Expose
    private String business;

    @SerializedName("Cuisine")
    @Expose
    private String cuisine;

    @SerializedName("Type")
    @Expose
    private int type;

    @SerializedName("typeid")
    @Expose
    private int typeId;

    public String getToken()
    {
        return token;
    }

    public void setToken(String token)
    {
        this.token = token;
    }

    public String getBusiness()
    {
        return business;
    }

    public void setBusiness(String business)
    {
        this.business = business;
    }

    public String getCuisine()
    {
        return cuisine;
    }

    public void setCuisine(String cuisine)
    {
        this.cuisine = cuisine;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public int getTypeId()
    {
        return typeId;
    }

    public void setTypeId(int typeId)
    {
        this.typeId = typeId;
    }
}