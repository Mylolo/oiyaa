package com.localvalu.common.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.listing.dto.BusinessListDto;

import java.util.ArrayList;

public class SearchCompleteResult
{
    @SerializedName("ErrorDetails")
    @Expose
    private ErrorDetails errorDetails;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName("SearchResultDetails")
    @Expose
    private ArrayList<BusinessListDto> searchResultDetails;

    public ErrorDetails getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public ArrayList<BusinessListDto> getSearchResultDetails()
    {
        return searchResultDetails;
    }

    public void setSearchResultDetails(ArrayList<BusinessListDto> searchResultDetails)
    {
        this.searchResultDetails = searchResultDetails;
    }

}
