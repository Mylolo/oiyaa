package com.localvalu.common.model.cuisinescategory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.model.ErrorDetails;
import com.localvalu.common.model.SearchResults;

import java.util.ArrayList;

public class CuisinesCategoryListResult
{
    @SerializedName("ErrorDetails")
    @Expose
    private ErrorDetails errorDetails;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("msg")
    @Expose
    private String msg;

    @SerializedName(value = "cuisinesListResults",alternate = {"SearchResult"})
    @Expose
    private ArrayList<CuisineCategoryData> cuisinesCategoryListResults;

    public ErrorDetails getErrorDetails()
    {
        return errorDetails;
    }

    public void setErrorDetails(ErrorDetails errorDetails)
    {
        this.errorDetails = errorDetails;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public ArrayList<CuisineCategoryData> getCuisinesCategoryListResults()
    {
        return cuisinesCategoryListResults;
    }

    public void setCuisinesCategoryListResults(ArrayList<CuisineCategoryData> cuisinesCategoryListResults)
    {
        this.cuisinesCategoryListResults = cuisinesCategoryListResults;
    }
}
