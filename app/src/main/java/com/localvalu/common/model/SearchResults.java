package com.localvalu.common.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.directory.listing.dto.BusinessListDto;

import java.util.ArrayList;
import java.util.List;

public class SearchResults
{
    @SerializedName("EatsSearchResults")
    @Expose
    private ArrayList<BusinessListDto> searchResult;

    public ArrayList<BusinessListDto> getSearchResult()
    {
        return searchResult;
    }

    public void setSearchResult(ArrayList<BusinessListDto> searchResult)
    {
        this.searchResult = searchResult;
    }
}
