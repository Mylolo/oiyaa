package com.localvalu.common.model.litter

import com.google.gson.annotations.SerializedName

/**
 *
 * startTime - Format : yyyy-mm-dd hh:mm,startTime should be within last five days.
 * endTime - Format : yyyy-mm-dd hh:mm endTime should not be future date.
 */
class InsertLitterRequest(@SerializedName("Token") var token:String,
                          @SerializedName("AccountId")var accountId:String,
                          @SerializedName("startTime")var startTime:String,
                          @SerializedName("endTime")var endTime:String,
                          @SerializedName("postcode")var postCode:String)
{
    init
    {
        token = "/3+YFd5QZdSK9EKsB8+TlA=="
    }
}