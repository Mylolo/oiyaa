package com.localvalu.common.model.appversion;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VersionCheckResponse
{
    @SerializedName("Appversion")
    @Expose
    private VersionCheckResult versionCheckResult;

    public VersionCheckResult getVersionCheckResult()
    {
        return versionCheckResult;
    }

    public void setVersionCheckResult(VersionCheckResult versionCheckResult)
    {
        this.versionCheckResult = versionCheckResult;
    }
}
