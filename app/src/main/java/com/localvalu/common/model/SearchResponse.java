package com.localvalu.common.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchResponse
{
    @SerializedName(value = "WebsiteSerachCompleteResult",alternate = {"WebSiteSearchResult","SearchResult"})
    @Expose
    private SearchCompleteResult searchCompleteResult;

    public SearchCompleteResult getSearchCompleteResult()
    {
        return searchCompleteResult;
    }

    public void setSearchCompleteResult(SearchCompleteResult searchCompleteResult)
    {
        this.searchCompleteResult = searchCompleteResult;
    }
}
