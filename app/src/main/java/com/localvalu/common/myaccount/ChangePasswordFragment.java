package com.localvalu.common.myaccount;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.common.mytransaction.wrapper.UserDetailResultWrapper;
import com.localvalu.user.LoginActivity;
import com.localvalu.user.dto.ChangePasswordResultDto;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.AlertDialogCallBack;
import com.utility.CommonUtilities;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordFragment extends BaseFragment
{
    private static final String TAG = ChangePasswordFragment.class.getSimpleName();

    private TextInputEditText et_oldpassword, et_password, et_conf_password;
    private Button btn_next;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private UserBasicInfo userBasicInfo;

    // TODO: Rename and change types and number of parameters
    public static ChangePasswordFragment newInstance()
    {
        return new ChangePasswordFragment();
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loads...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_changepass, container, false);
        buildObjectForHandlingAPI();
        initView();
        return view;
    }

    private void initView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        et_oldpassword = view.findViewById(R.id.et_oldpassword);
        et_password = view.findViewById(R.id.et_password);
        et_conf_password = view.findViewById(R.id.et_conf_password);

        Typeface type =  ResourcesCompat.getFont(getContext(), R.font.comfortaa_regular);
        et_oldpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        et_oldpassword.setTypeface(type);

        et_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        et_password.setTypeface(type);

        et_conf_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        et_conf_password.setTypeface(type);

        btn_next = view.findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (isValid())
                    callAPI();
            }
        });
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }

    private void callAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("RequestFor", "ChangePassword");
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("OldPassword", et_oldpassword.getText().toString().trim());
            objMain.put("Password", et_password.getText().toString().trim());
            objMain.put("ConfirmPassword", et_conf_password.getText().toString().trim());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                        ChangePasswordResultDto changePasswordResultDto = response.body().userDetailResult.changePasswordResultDto;

                        DialogUtility.showMessageOkWithCallback("Your password has been changed successfully.", getActivity(), new AlertDialogCallBack()
                        {
                            @Override
                            public void onSubmit()
                            {
                                et_oldpassword.getText().clear();
                                et_password.getText().clear();
                                et_conf_password.getText().clear();

                                //((MainActivity) getActivity()).addFragment(EatsAccountFragment.newInstance(), false, EatsAccountFragment.class.getName(), false);
                                gotoLoginActivity();
                            }

                            @Override
                            public void onCancel()
                            {

                            }
                        });
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    /*Validity Checking*/
    private boolean isValid()
    {
        if (et_oldpassword.getText().toString().trim().length() == 0)
        {
            et_oldpassword.requestFocus();
            et_oldpassword.setError(getString(R.string.enter_pass));
            return false;
        }
        else if (et_password.getText().toString().trim().length() == 0)
        {
            et_password.requestFocus();
            et_password.setError(getString(R.string.enter_pass));
            return false;
        }
        else if (!checkRegex(et_password.getText().toString().trim()))
        {
            et_password.requestFocus();
            et_password.setError(getString(R.string.greater_pass));
            return false;
        }
        else if (et_conf_password.getText().toString().trim().length() == 0)
        {
            et_conf_password.requestFocus();
            et_conf_password.setError(getString(R.string.enter_pass));
            return false;
        }
        else if (!checkRegex(et_conf_password.getText().toString().trim()))
        {
            et_conf_password.requestFocus();
            et_conf_password.setError(getString(R.string.greater_pass));
            return false;
        }
        else if (!et_password.getText().toString().trim().equals(et_conf_password.getText().toString().trim()))
        {
            et_conf_password.requestFocus();
            et_conf_password.setError(getString(R.string.pass_not_matched));
            return false;
        }
        else if (!CommonUtilities.checkConnectivity(getActivity()))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.network_unavailable), Toast.LENGTH_SHORT).show();
            return false;
        }
        else
            return true;
    }

    private boolean checkRegex(String password)
    {
        // Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character
        final String PASSWORD_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password.trim());
        return matcher.matches();
    }

    public void gotoLoginActivity()
    {
        Intent intent = new Intent(requireActivity(),LoginActivity.class);
        startActivity(intent);
        requireActivity().finish();
    }
}
