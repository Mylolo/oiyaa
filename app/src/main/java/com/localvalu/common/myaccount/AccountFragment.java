package com.localvalu.common.myaccount;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentMyAccoutBinding;

import java.util.ArrayList;
import java.util.List;


public class AccountFragment extends BaseFragment
{
    private FragmentMyAccoutBinding binding;
    private int colorEats;
    private HostActivityListener hostActivityListener;


    public static AccountFragment newInstance()
    {
        AccountFragment fragment = new AccountFragment();
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        binding = FragmentMyAccoutBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setupViewPager(binding.viewpager);
        binding.tabPagerHeading.setupWithViewPager(binding.viewpager);
    }

    private void setUpActionBar()
    {
        binding.tvTitle.setTextColor(colorEats);
        binding.toolbar.getNavigationIcon().setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private void setupViewPager(ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new InfoFragment(), "My Info");
        adapter.addFragment(new ChangePasswordFragment(), "Change Password");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter
    {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager)
        {
            super(manager);
        }

        @Override
        public Fragment getItem(int position)
        {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount()
        {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title)
        {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            return mFragmentTitleList.get(position);
        }
    }

}
