package com.localvalu.common.myaccount;

import static com.utility.DateUtility.getDisplayFormatFromDate;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.MainActivity;
import com.localvalu.databinding.FragmentMyinfoBinding;
import com.localvalu.user.ValidationUtils;
import com.localvalu.user.adapter.PassionListAdapter;
import com.localvalu.user.adapter.PassionListCommonAdapter;
import com.localvalu.user.dto.PassionCategory;
import com.localvalu.user.dto.PassionDetails;
import com.localvalu.user.dto.PassionListRequest;
import com.localvalu.user.dto.UserBasicInfo;
import com.localvalu.user.wrapper.MyAccountResultWrapper;
import com.localvalu.user.wrapper.PassionListResultWrapper;
import com.localvalu.user.wrapper.UpdateCustomerResultWrapper;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.CommonUtilities;
import com.utility.CommonUtility;
import com.utility.Config;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.MarshMallowPermission;
import com.utility.PreferenceUtility;
import com.utility.customView.RadioGridGroup;
import com.utility.validators.RangeValidator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoFragment extends BaseFragment
{
    private static final String TAG = InfoFragment.class.getSimpleName();

    private String sAgeGroup, sGender, sPassionDetails = "";
    private JSONArray sPassionDetailsArray = new JSONArray();
    private LinearLayoutManager layoutManager;

    private PassionListCommonAdapter passionListAdapter;
    private List<PassionDetails> passionDetailsList;
    private String strPassions;
    private String selectedAgeGroup;

    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private UserBasicInfo userBasicInfo;
    private String strDOB;
    private Date dobDate;
    private Calendar startTime = Calendar.getInstance();
    private Calendar endTime = Calendar.getInstance();

    private boolean isChecking = true;
    private AppCompatImageView ivCamera;
    private FragmentMyinfoBinding binding;

    // TODO: Rename and change types and number of parameters
    public static InfoFragment newInstance()
    {
        return new InfoFragment();
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        startTime.set(1922,0,2);
        endTime.set(2002,11,31);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentMyinfoBinding.inflate(inflater,container,false);
        //MarshMallowPermission.checkPermission(getActivity());
        buildObjectForHandlingAPI();

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setProperties();
        fetchAPI();
    }

    private void setProperties()
    {
        dobDate = new Date();
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        layoutManager = new LinearLayoutManager(requireContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.rvPassionListProfile.setLayoutManager(layoutManager);
        binding.etEmail.setEnabled(false);
        binding.etMobileNumber.setEnabled(false);
        binding.etDOB.setEnabled(false);
        initAgeAndGender();

        binding.etDOB.setOnClickListener(_OnClickListener);
        binding.btnSubmitProfile.setOnClickListener(_OnClickListener);
        binding.layoutError.btnRetry.setOnClickListener(_OnClickListener);

    }

    private void initAgeAndGender()
    {
        binding.tvTextAgeGroup.setVisibility(View.GONE);
        binding.radGroupAge.setVisibility(View.GONE);
        binding.radGroupAge.setOnCheckedChangeListener(new RadioGridGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioButton radioButton, boolean checked)
            {
                if (checked)
                {
                    //tvAgeError.setVisibility(View.GONE);
                    switch (radioButton.getId())
                    {
                        case R.id.radioButton16To24:
                            sAgeGroup = binding.radioButton16To24.getText().toString();
                            break;
                        case R.id.radioButton25To34:
                            sAgeGroup = binding.radioButton25To34.getText().toString();
                            break;
                        case R.id.radioButton35To44:
                            sAgeGroup = binding.radioButton35To44.getText().toString();
                            break;
                        case R.id.radioButton45To54:
                            sAgeGroup = binding.radioButton45To54.getText().toString();
                            break;
                        case R.id.radioButton55To64:
                            sAgeGroup = binding.radioButton55To64.getText().toString();
                            break;
                        case R.id.radioButton65Plus:
                            sAgeGroup = binding.radioButton65Plus.getText().toString();
                            break;
                    }
                }
            }
        });
        binding.radGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int radioButtonId)
            {
                //tvGenderError.setVisibility(View.GONE);
                switch (radioButtonId)
                {
                    case R.id.radioButtonMale:
                        sGender =binding. radioButtonMale.getText().toString();
                        break;
                    case R.id.radioButtonFemale:
                        sGender = binding.radioButtonFemale.getText().toString();
                        break;
                    case R.id.radioButtonOther:
                        sGender = "None";//binding.radioButtonOther.getText().toString();
                        break;
                }
            }
        });
        if(userBasicInfo!=null)
        {
            if (userBasicInfo.dob!=null)
            {
                if(!userBasicInfo.dob.isEmpty())
                {
                    dobDate = DateUtility.convertedDateFromDateString(userBasicInfo.dob);
                    strDOB = DateUtility.convertDateToServerFormatDate(userBasicInfo.dob);
                    sAgeGroup = getAgeGroup(DateUtility.ageCalculator(dobDate));
                }
            }
        }
    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            switch (view.getId())
            {
                case R.id.btnSubmitProfile:
                     if (isValid())
                     {
                        callAPI();
                    }
                     break;
                case R.id.etDOB:
                     //showDOBPicker();
                     break;
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK)
        {
            if (requestCode == 1)
            {
                if (data != null)
                {

                }
            }
        }
    }

    private void localMaskImage(Bitmap bitmap, ImageView imageView, int backgroundImageID)
    {
        bitmap = CommonUtility.getAspectBitmap(getActivity(), bitmap, imageView, backgroundImageID);
        imageView.setImageBitmap(bitmap);
    }

    private void showDOBPicker()
    {
        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        builder.setCalendarConstraints(dobDateLimitRange().build());
        MaterialDatePicker picker = builder.build();

        picker.show(getChildFragmentManager(), picker.toString());
        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener()
        {
            @Override
            public void onPositiveButtonClick(Object selection)
            {
                long milliseconds = (long) selection;
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTimeInMillis(milliseconds);
                dobDate = selectedDate.getTime();
                strDOB = convertToStringInServerFormat(selectedDate.getTime());
                binding.etDOB.setText(convertDateToStringInUKFormat(selectedDate.getTime()));
            }
        });
    }

    /*
     Limit selectable Date range
   */
    private CalendarConstraints.Builder dobDateLimitRange()
    {
        CalendarConstraints.Builder constraintsBuilderRange = new CalendarConstraints.Builder();

        constraintsBuilderRange.setStart(startTime.getTimeInMillis());
        constraintsBuilderRange.setEnd(endTime.getTimeInMillis());
        constraintsBuilderRange.setValidator(new RangeValidator(startTime.getTimeInMillis(), endTime.getTimeInMillis()));

        return constraintsBuilderRange;
    }

    private void setValue()
    {
        binding.etFullName.setText(userBasicInfo.name);
        binding.etMobileNumber.setText(userBasicInfo.mobileNo);
        binding.etEmail.setText(userBasicInfo.email);
        binding.etHomePostCode.setText(userBasicInfo.homePostcode);
        binding.etWorkPostCode.setText(userBasicInfo.workPostcode);
        if (userBasicInfo.dob != null)
        {
            if (!userBasicInfo.dob.isEmpty())
            {
                dobDate = DateUtility.convertedDateFromDateString(userBasicInfo.dob);
                strDOB = DateUtility.convertDateToServerFormatDate(userBasicInfo.dob);
                sAgeGroup = getAgeGroup(DateUtility.ageCalculator(dobDate));
                binding.etDOB.setText(getDisplayFormatFromDate(dobDate));
            }
        }

        setGenderAndAgeValue();
    }

    private void setGenderAndAgeValue()
    {
        sAgeGroup = userBasicInfo.ageGroup;
        sGender = userBasicInfo.gender;
        if (binding.radioButton16To24.getText().equals(userBasicInfo.ageGroup))
        {
            binding.radioButton16To24.performClick();
        }
        else if (binding.radioButton25To34.getText().equals(userBasicInfo.ageGroup))
        {
            binding.radioButton25To34.performClick();
        }
        else if (binding.radioButton35To44.getText().equals(userBasicInfo.ageGroup))
        {
            binding.radioButton35To44.performClick();
        }
        else if (binding.radioButton45To54.getText().equals(userBasicInfo.ageGroup))
        {
            binding.radioButton45To54.performClick();
        }
        else if (binding.radioButton55To64.getText().equals(userBasicInfo.ageGroup))
        {
            binding.radioButton55To64.performClick();
        }
        else if (binding.radioButton65Plus.getText().equals(userBasicInfo.ageGroup))
        {
            binding.radioButton65Plus.performClick();
        }
        if (binding.radioButtonMale.getText().toString().toLowerCase().equals(userBasicInfo.gender.toLowerCase()))
        {
            binding.radioButtonMale.setChecked(true);
        }
        else if (binding.radioButtonFemale.getText().toString().toLowerCase().equals(userBasicInfo.gender.toLowerCase()))
        {
            binding.radioButtonFemale.setChecked(true);
        }
        else if ("none".equals(userBasicInfo.gender.toLowerCase()))
        {
            binding.radioButtonOther.setChecked(true);
        }
    }

    private void setData(List<PassionCategory> passionCategoryList, String[] passionsList)
    {
        passionDetailsList  = new ArrayList<PassionDetails>();

        for(int i=0;i<passionCategoryList.size();i++)
        {
            PassionCategory category = passionCategoryList.get(i);

            PassionDetails passionDetailsHeader = new PassionDetails();
            passionDetailsHeader.type = PassionListAdapter.HEADER_VIEW_HOLDER;
            passionDetailsHeader.passion= category.passionCategory;
            passionDetailsList.add(passionDetailsHeader);

            for(int j=0;j<category.passionDetailsList.size();j++)
            {
                PassionDetails tempPassionDetails = category.passionDetailsList.get(j);
                PassionDetails passionDetailsItem = new PassionDetails();
                passionDetailsItem.type = PassionListAdapter.ITEM_VIEW_HOLDER;
                passionDetailsItem.passion= tempPassionDetails.passion;
                passionDetailsItem.checked=isCheckedPassion(passionsList,passionDetailsItem.passion);
                passionDetailsList.add(passionDetailsItem);
            }
        }
        if(passionDetailsList.size()>0)
        {
            showList();
            passionListAdapter = new PassionListCommonAdapter(passionDetailsList);
            binding. rvPassionListProfile.setAdapter(passionListAdapter);
        }
    }

    private boolean isCheckedPassion(String[] passionsList,String passion)
    {
        List<String> list = Arrays.asList(passionsList);
        Iterator iterator = list.iterator();
        while (iterator.hasNext())
        {
            String element = (String) iterator.next();
            if(element.equals(passion)) return true;
        }
        return false;
    }

    private void getPassionList()
    {
        SharedPreferences pref = requireActivity().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        showProgress();

        PassionListRequest passionListRequest = new PassionListRequest();

        Call<PassionListResultWrapper> dtoCall = apiInterface.getPassionList(passionListRequest);
        dtoCall.enqueue(new Callback<PassionListResultWrapper>()
        {
            @Override
            public void onResponse(Call<PassionListResultWrapper> call, Response<PassionListResultWrapper> response)
            {
                try
                {
                    if (response.code() != 400)
                    {
                        if (response.body().passionsListResult.errorDetails.errorCode == 0)
                        {
                            if (userBasicInfo.passionDetails != null)
                            {
                                String[] passionLists = userBasicInfo.passionDetails.split(",");
                                setData(response.body().passionsListResult.passionCategoryList,passionLists);
                            }

                        }
                        else
                        {
                            showError(response.body().passionsListResult.errorDetails.errorMessage,false);
                        }
                    }
                    else
                    {
                        showError(getString(R.string.server_alert),false);
                    }
                }
                catch (Exception e)
                {
                    showError(getString(R.string.server_alert),false);
                }
            }

            @Override
            public void onFailure(Call<PassionListResultWrapper> call, Throwable t)
            {
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(requireContext()))
                    showError(getString(R.string.network_unavailable),true);
                else
                    showError(getString(R.string.server_alert),true);
            }
        });

    }

    private void showProgress()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.VISIBLE);
        binding.rvPassionListProfile.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showList()
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvPassionListProfile.setVisibility(View.VISIBLE);
        binding.layoutError.layoutRoot.setVisibility(View.GONE);
    }

    private void showError(String strMessage,boolean retry)
    {
        binding.layoutProgress.layoutRoot.setVisibility(View.GONE);
        binding.rvPassionListProfile.setVisibility(View.GONE);
        binding.layoutError.layoutRoot.setVisibility(View.VISIBLE);
        binding.layoutError.tvError.setText(strMessage);
        if(retry) binding.layoutError.btnRetry.setVisibility(View.VISIBLE);
        else binding.layoutError.btnRetry.setVisibility(View.GONE);
    }

    private boolean isValid()
    {
        sAgeGroup = getAgeGroup(DateUtility.ageCalculator(dobDate));

        if (binding.etFullName.getText().toString().trim().length() == 0)
        {
            binding.etFullName.requestFocus();
            binding.etFullName.setError("Enter Your Full Name");
            return false;
        }
        else if (binding.etMobileNumber.getText().toString().trim().length() == 0)
        {
            binding.etMobileNumber.requestFocus();
            binding.etMobileNumber.setError("Enter Your Mobile No");
            return false;
        }
        else if (binding.etEmail.getText().toString().trim().length() == 0)
        {
            binding.etEmail.requestFocus();
            binding.etEmail.setError("Enter Your Valid Email ID");
            return false;
        }
        /*else if (et_address.getText().toString().trim().length() == 0)
        {
            et_address.requestFocus();
            et_address.setError("Enter Your Address");
            return false;
        }
        else if (et_city.getText().toString().trim().length() == 0)
        {
            et_city.requestFocus();
            et_city.setError("Enter Your City");
            return false;
        }
        else if (et_country.getText().toString().trim().length() == 0)
        {
            et_country.requestFocus();
            et_country.setError("Enter Your Country");
            return false;
        }*/
        else if (binding.etHomePostCode.getText().toString().trim().length() == 0)
        {
            binding.etHomePostCode.requestFocus();
            binding.etHomePostCode.setError("Enter Your Home Postcode");
            return false;
        }
        else if (!ValidationUtils.isValidPostCode(binding.etHomePostCode.getText().toString().toUpperCase()))
        {
            binding.etHomePostCode.requestFocus();
            binding.etHomePostCode.setError("Enter Valid Postcode");
            return false;
        }
        /*else if (binding.radGroupAge.getCheckedRadioButtonId() == -1)
        {
            Toast.makeText(getActivity(), "Please Select Your Age", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(sAgeGroup.isEmpty())
        {
            Toast.makeText(getActivity(), "Please Select Your DOB", Toast.LENGTH_SHORT).show();
            return false;
        }*/
        else if (binding.radGroupGender.getCheckedRadioButtonId() == -1)
        {
            Toast.makeText(getActivity(), "Please Select Your Gender", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!binding.etWorkPostCode.getText().toString().trim().equals(""))
        {
            if (!ValidationUtils.isValidPostCode(binding.etWorkPostCode.getText().toString().toUpperCase().trim()))
            {
                binding.etWorkPostCode.requestFocus();
                binding.etWorkPostCode.setError("Enter Valid Postcode");
                return false;
            }
        }
        return true;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();
    }

    //Fetch Profile API
    private void fetchAPI()
    {
        //API
        Call<MyAccountResultWrapper> call;
        mProgressDialog.show();

        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("AccountId", userBasicInfo.accountId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        call = apiInterface.myAccount(body);
        call.enqueue(new Callback<MyAccountResultWrapper>()
        {
            @Override
            public void onResponse(Call<MyAccountResultWrapper> call, Response<MyAccountResultWrapper> response)
            {
                try
                {
                    if (response.body().myAccountResult.errorDetails.errorCode == 0)
                    {
                        userBasicInfo = response.body().myAccountResult.userBasicInfo;
                        PreferenceUtility.saveObjectInAppPreference(getActivity(), userBasicInfo, PreferenceUtility.APP_PREFERENCE_NAME);
                        getPassionList();
                        setValue();
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().myAccountResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
                mProgressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MyAccountResultWrapper> call, Throwable t)
            {
                if (!CommonUtilities.checkConnectivity(getActivity()))
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.network_unavailable), getActivity());
                }
                else
                {
                    DialogUtility.showMessageWithOk(getResources().getString(R.string.server_alert), getActivity());
                }
                mProgressDialog.dismiss();
            }
        });
    }

    private void callAPI()
    {
        mProgressDialog.show();
        getPassions();
        JSONObject objMain = new JSONObject();

        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("userid", userBasicInfo.userId);
            objMain.put("userName", binding.etFullName.getText().toString().trim());
            objMain.put("AccountID", userBasicInfo.accountId);
            objMain.put("Mobile", binding.etMobileNumber.getText().toString().trim());
            objMain.put("sEmail", binding.etEmail.getText().toString().trim());
            objMain.put("Address", userBasicInfo.address);
            objMain.put("city", userBasicInfo.city.trim());
            objMain.put("country", userBasicInfo.country.trim());
            objMain.put("sHomePostcode", binding.etHomePostCode.getText().toString());
            objMain.put("sWorkPostcode", binding.etWorkPostCode.getText().toString().trim());
            objMain.put("sAgeGroup", sAgeGroup.trim());
            objMain.put("sGender", sGender.trim());
            //objMain.put("sPassionDetails", sPassionDetails.trim());
            objMain.put("sPassionDetails", sPassionDetailsArray);
            objMain.put("DOB", strDOB);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UpdateCustomerResultWrapper> dtoCall = apiInterface.userUpdateDetailsAPI(body);
        dtoCall.enqueue(new Callback<UpdateCustomerResultWrapper>()
        {
            @Override
            public void onResponse(Call<UpdateCustomerResultWrapper> call, Response<UpdateCustomerResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.code() != 400)
                    {
                        if (response.body().updateCustomerDetails.errorDetails.errorCode == 0)
                        {
                            ((MainActivity) getActivity()).addFragment(AccountFragment.newInstance(), true, AccountFragment.class.getName(), true);
                            //fetchAPI();
                        }
                        else
                        {
                            DialogUtility.showMessageWithOk(response.body().updateCustomerDetails.errorDetails.errorMessage, getActivity());
                        }
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "You have entered wrong data.", Toast.LENGTH_SHORT).show();
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UpdateCustomerResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getActivity()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });

    }

    private String convertDateToStringInUKFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    private String convertDateStringInUKFormat(String strDate)
    {
        return convertDateToStringInUKFormat(getDateFromString(strDate));
    }

    private Date getDateFromString(String strDate)
    {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date date = inputFormat.parse(strDate);
            return date;
        }
        catch (Exception ex)
        {
            return new Date();
        }
    }

    private String convertToStringInServerFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }

    private void getPassions()
    {
        if(passionDetailsList!=null)
        {
            if(passionDetailsList.size()>0)
            {
                for(int i=0;i<passionDetailsList.size();i++)
                {
                    PassionDetails passionDetails = passionDetailsList.get(i);

                    if(passionDetails.checked)
                    {
                        sPassionDetailsArray.put(passionDetails.passion);
                    }
                }

            }
        }
    }

    private String getAgeGroup(int age)
    {
        if (age >= 16 && age <= 24) return getString(R.string.lbl_16_to_24);
        else if (age >= 25 && age <= 34) return getString(R.string.lbl_25_to_34);
        else if (age >= 35 && age <= 44) return getString(R.string.lbl_35_to_44);
        else if (age >= 44 && age <= 54) return getString(R.string.lbl_45_to_54);
        else if (age >= 55 && age <= 64) return getString(R.string.lbl_55_to_64);
        else if (age >= 65) return getString(R.string.lbl_65_plus);
        return "";
    }
}
