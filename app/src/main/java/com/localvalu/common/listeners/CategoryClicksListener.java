package com.localvalu.common.listeners;

import com.localvalu.common.model.cuisinescategory.CuisineCategoryData;

public interface CategoryClicksListener
{
    void onCategoryClicked(CuisineCategoryData cuisineCategoryData, int navigationType);
}

