package com.localvalu.common.mytransaction.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DashBoardAPIResult implements Serializable {

    @SerializedName("nameAccountId")
    @Expose
    public String nameAccountId;

    @SerializedName("TotalTransactions")
    @Expose
    public Integer totalTransactions;

    @SerializedName("TotalRedeemed")
    @Expose
    public String totalRedeemed;

    @SerializedName("TotalEarned")
    @Expose
    public String totalEarned;

    @SerializedName("ReferralTokenearned")
    @Expose
    public String referralTokenearned;

    @SerializedName("ReviewTokenearned")
    @Expose
    public String reviewTokenearned;

    @SerializedName("CurrentBalanceToken")
    @Expose
    public String currentBalanceToken;

    @SerializedName("BonusToken")
    @Expose
    public String bonusToken;

    @SerializedName("CashSavedOiyaa")
    @Expose
    public String cashSavedOiyaa;

}
