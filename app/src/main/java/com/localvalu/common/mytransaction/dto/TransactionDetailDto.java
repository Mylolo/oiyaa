package com.localvalu.common.mytransaction.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TransactionDetailDto implements Serializable {

    @SerializedName("Receipt_Datetime")
    @Expose
    public String receiptDatetime;

    @SerializedName("M_TRadingName")
    @Expose
    public String mTRadingName;

    @SerializedName("TRTime")
    @Expose
    public String trTime;

    @SerializedName("ReceiptValue")
    @Expose
    public Float receiptValue;

    @SerializedName("Token_Redeemed")
    @Expose
    public Float tokenRedeemed;

    @SerializedName("FinalPay")
    @Expose
    public Float finalPay;

    @SerializedName("Bonus_Token")
    @Expose
    public Float bonusToken;

    @SerializedName("PaymentMode")
    @Expose
    public String paymentMode;
}
