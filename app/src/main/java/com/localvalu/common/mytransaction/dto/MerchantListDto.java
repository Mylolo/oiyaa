package com.localvalu.common.mytransaction.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MerchantListDto implements Serializable {

    @SerializedName("Merchant_Trading_Name")
    @Expose
    public String merchantTradingName;
    @SerializedName("RetailerID")
    @Expose
    public Integer retailerID;
    @SerializedName("Merchantstatus")
    @Expose
    public String merchantstatus;

    public static MerchantListDto all() {
        MerchantListDto retVal = new MerchantListDto();
        retVal.merchantTradingName = "All";
        retVal.retailerID = 0;

        return retVal;
    }
}
