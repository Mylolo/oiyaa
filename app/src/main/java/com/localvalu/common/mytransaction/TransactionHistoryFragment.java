package com.localvalu.common.mytransaction;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;

import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.localvalu.R;
import com.localvalu.base.MainActivity;
import com.localvalu.common.mytransaction.adapter.TransactionHistoryListAdapter;
import com.localvalu.common.mytransaction.dto.MerchantListDto;
import com.localvalu.common.mytransaction.dto.TransactionHistoryListDto;
import com.localvalu.common.mytransaction.wrapper.UserDetailResultWrapper;
import com.localvalu.databinding.FragmentTransactionHistoryBinding;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.CommonUtilities;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;
import com.utility.validators.RangeValidator;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionHistoryFragment extends Fragment
{
    private static final String TAG = "EatsTransactionHistoryF";


    private ArrayList<MerchantListDto> merchantArray;
    private GridLayoutManager gridLayoutManager;
    private TransactionHistoryListAdapter transactionHistoryListAdapter;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private String strFromDate, strToDate;
    private Date fromDate,toDate;
    private String merchant = "";
    private FragmentTransactionHistoryBinding binding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);
        merchantArray = new ArrayList<>();
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentTransactionHistoryBinding.inflate(inflater, container, false);
        buildObjectForHandlingAPI();
        setView();
        return binding.getRoot();
    }

    private void setView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        gridLayoutManager = new GridLayoutManager(getActivity(), 1);
        binding.rvLists.setLayoutManager(gridLayoutManager);

        fromDate = new Date();
        toDate = new Date();

        fromDate = DateUtility.getPastLocalDateTimeAsDate();
        toDate = DateUtility.getLocalDateTimeAsDate();

        strFromDate = DateUtility.getPastLocalDateTime();
        strToDate = DateUtility.getLocalDateTime();

        binding.toDate.setText(convertDateStringInUKFormat(strToDate));
        binding.fromDate.setText(convertDateStringInUKFormat(strFromDate));

        binding.ddMerchant.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                System.out.println("Id :::: " + merchantArray.get(position).merchantTradingName);
                merchant = String.valueOf(merchantArray.get(position).retailerID);
                callAPI(merchant);
            }
        });

        fetchAPI();

        binding.fromDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    showFromDatePicker();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        binding.toDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    showToDatePicker();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        binding.toDate.addTextChangedListener(new TextWatcher()
        {

            public void afterTextChanged(Editable s)
            {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                callAPI(merchant);
            }
        });

        binding.fromDate.addTextChangedListener(new TextWatcher()
        {

            public void afterTextChanged(Editable s)
            {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                callAPI(merchant);
            }
        });
    }

    private void showFromDatePicker()
    {
        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        builder.setCalendarConstraints(dateLimitRangeStart().build());
        MaterialDatePicker picker = builder.build();
        picker.show(getChildFragmentManager(),picker.toString());
        builder.setTitleText(getString(R.string.lbl_select_from_date));
        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener()
        {
            @Override
            public void onPositiveButtonClick(Object selection)
            {
                long milliseconds = (long) selection;
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTimeInMillis(milliseconds);
                fromDate = selectedDate.getTime();
                strFromDate = convertToStringInServerFormat(selectedDate.getTime());
                binding.fromDate.setText(convertDateToStringInUKFormat(selectedDate.getTime()));
            }
        });
    }

    private void showToDatePicker()
    {
        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        builder.setCalendarConstraints(dateLimitRangeEnd().build());
        MaterialDatePicker picker = builder.build();
        builder.setTitleText(getString(R.string.lbl_select_to_date));
        picker.show(getChildFragmentManager(),picker.toString());
        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener()
        {
            @Override
            public void onPositiveButtonClick(Object selection)
            {
                long milliseconds = (long) selection;
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTimeInMillis(milliseconds);
                toDate = selectedDate.getTime();
                strToDate = convertToStringInServerFormat(selectedDate.getTime());
                binding.toDate.setText(convertDateToStringInUKFormat(selectedDate.getTime()));
            }
        });
    }

    /*
    Limit selectable Date range
  */
    private CalendarConstraints.Builder dateLimitRangeStart()
    {
        Calendar calendarStart = Calendar.getInstance();
        calendarStart.add(Calendar.YEAR,-2);
        Calendar calendarEnd = Calendar.getInstance();
        CalendarConstraints.Builder constraintsBuilderRange = new CalendarConstraints.Builder();

        constraintsBuilderRange.setStart(calendarStart.getTimeInMillis());
        constraintsBuilderRange.setEnd(calendarEnd.getTimeInMillis());
        constraintsBuilderRange.setValidator(new RangeValidator(calendarStart.getTimeInMillis(), calendarEnd.getTimeInMillis()));

        return constraintsBuilderRange;
    }

    /*
 Limit selectable Date range
*/
    private CalendarConstraints.Builder dateLimitRangeEnd()
    {
        Calendar calendarStart = Calendar.getInstance();
        calendarStart.setTime(fromDate);
        Calendar calendarEnd = Calendar.getInstance();
        CalendarConstraints.Builder constraintsBuilderRange = new CalendarConstraints.Builder();

        constraintsBuilderRange.setStart(calendarStart.getTimeInMillis());
        constraintsBuilderRange.setEnd(calendarEnd.getTimeInMillis());
        constraintsBuilderRange.setValidator(new RangeValidator(calendarStart.getTimeInMillis(), calendarEnd.getTimeInMillis()));

        return constraintsBuilderRange;
    }

    private void setAdapter(final ArrayList<TransactionHistoryListDto> transactionHistoryList)
    {
        transactionHistoryListAdapter = new TransactionHistoryListAdapter(getActivity(), transactionHistoryList);
        transactionHistoryListAdapter.setOnClickListener(new TransactionHistoryListAdapter.OnItemClickListener()
        {
            @Override
            public void onItemClick(int position)
            {
                Bundle bundle = new Bundle();
                String id = String.valueOf(transactionHistoryList.get(position).sirTransID);
                bundle.putString("id", id);
                TransactionDetailFragment transactionDetailFragment = TransactionDetailFragment.newInstance();
                transactionDetailFragment.setArguments(bundle);
                ((MainActivity) getActivity()).addFragment(transactionDetailFragment, true, TransactionDetailFragment.class.getName(), true);
            }
        });
        binding.rvLists.setAdapter(transactionHistoryListAdapter);
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    private void fetchAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "MerchantList");
            objMain.put("AccountId", userBasicInfo.accountId);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                        if (response.body().userDetailResult.status.equalsIgnoreCase("true"))
                        {
                            if(merchantArray!=null)
                            {
                                merchantArray.clear();
                                merchantArray.add(0, MerchantListDto.all());
                                merchantArray.addAll(response.body().userDetailResult.merchantList);
                                binding.ddMerchant.setItems(merchantArray);

                                if (merchantArray != null && !merchantArray.isEmpty())
                                {
                                    binding.ddMerchant.setSelectedPosition(0);
                                    merchant = merchantArray.get(0).retailerID.toString();
                                    callAPI(merchant);
                                }
                            }
                        }
                    }
                    else
                    {
                            //DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    private void callAPI(String merchant)
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "TransactionHistory");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("MerchantId", merchant);
            objMain.put("Fromdate", strFromDate);
            objMain.put("Todate", strToDate);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                        if (response.body().userDetailResult.status.equalsIgnoreCase("true"))
                        {
                            if(response.body().userDetailResult.transactionHistoryList!=null)
                            {
                                if(response.body().userDetailResult.transactionHistoryList.size()>0)
                                {
                                    setAdapter(response.body().userDetailResult.transactionHistoryList);
                                    binding.tvEmptyText.setVisibility(View.GONE);
                                }
                                else
                                {
                                    binding.tvEmptyText.setText(getString(R.string.lbl_no_transactions_found));
                                    binding.tvEmptyText.setVisibility(View.VISIBLE);
                                }
                            }
                            else
                            {
                                binding.tvEmptyText.setVisibility(View.GONE);
                            }
                        }
                        else
                        {
                            binding.tvEmptyText.setVisibility(View.GONE);
                        }
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());
                        setAdapter(new ArrayList<TransactionHistoryListDto>());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());

                setAdapter(new ArrayList<TransactionHistoryListDto>());
            }
        });
    }

    private String pad(int data)
    {
        return data <= 9 ? ("0" + data) : (data + "");
    }

    private String convertDateToStringInUKFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    private String convertDateStringInUKFormat(String strDate)
    {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date date = inputFormat.parse(strDate);
            return convertDateToStringInUKFormat(date);
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private Date getDateFromString(String strDate)
    {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date date = inputFormat.parse(strDate);
            return date;
        }
        catch (Exception ex)
        {
            return new Date();
        }
    }

    private String convertToStringInServerFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }
}
