package com.localvalu.common.mytransaction;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.localvalu.R;
import com.localvalu.common.mytransaction.wrapper.UserDetailResultWrapper;
import com.localvalu.databinding.FragmentTransactionDashboardBinding;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.squareup.picasso.Picasso;
import com.utility.CommonUtilities;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;
import com.utility.validators.RangeValidator;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TransactionDashboardFragment extends Fragment
{
    private static final String TAG = TransactionDashboardFragment.class.getSimpleName();
    public View view;

    private FragmentTransactionDashboardBinding binding;

    private UserBasicInfo userBasicInfo;
    private String strFromDate, strToDate;
    private Date fromDate,toDate;

    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;
    private String strCurrencySymbol;
    private String strCurrencyLetter;

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        binding = FragmentTransactionDashboardBinding.inflate(inflater,container,false);
        strCurrencySymbol = getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter = getString(R.string.lbl_currency_letter_euro);
        buildObjectForHandlingAPI();
        setView();
        return binding.getRoot();
    }

    private void setView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);

        if (userBasicInfo.image != null)
        {
            if (!userBasicInfo.image.trim().equals(""))
            {
                Picasso.with(getActivity()).load(userBasicInfo.image)
                        .placeholder(R.drawable.progress_animation).error(R.drawable.noimg_profile).into(binding.ivProfile);
            }
            else
            {
                binding.ivProfile.setImageResource(R.drawable.noimg_profile);
            }
        }
        else
        {
            binding.ivProfile.setImageResource(R.drawable.noimg_profile);
        }
        binding.tvProfile.setText(userBasicInfo.qRCodeOutput);
        binding.tvName.setText(userBasicInfo.name);

        fromDate = new Date();
        toDate = new Date();

        fromDate = DateUtility.getPastLocalDateTimeAsDate();
        toDate = DateUtility.getLocalDateTimeAsDate();

        strFromDate = DateUtility.getPastLocalDateTime();
        strToDate = DateUtility.getLocalDateTime();

        binding.toDate.setText(convertDateStringInUKFormat(strToDate));
        binding.fromDate.setText(convertDateStringInUKFormat(strFromDate));

        callAPI();

        binding.fromDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    showFromDatePicker();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        binding.toDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                try
                {
                    showToDatePicker();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });

        binding.toDate.addTextChangedListener(new TextWatcher()
        {

            public void afterTextChanged(Editable s)
            {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                callAPI();
            }
        });

        binding.fromDate.addTextChangedListener(new TextWatcher()
        {

            public void afterTextChanged(Editable s)
            {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                callAPI();
            }
        });

    }

    private void showFromDatePicker()
    {
        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        builder.setCalendarConstraints(dateLimitRangeStart().build());
        builder.setTitleText(getString(R.string.lbl_select_from_date));
        MaterialDatePicker picker = builder.build();
        picker.show(requireFragmentManager(),picker.toString());
        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener()
        {
            @Override
            public void onPositiveButtonClick(Object selection)
            {
                long milliseconds = (long) selection;
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTimeInMillis(milliseconds);
                fromDate = selectedDate.getTime();
                strFromDate = convertToStringInServerFormat(selectedDate.getTime());
                binding.fromDate.setText(convertDateToStringInUKFormat(selectedDate.getTime()));
            }
        });
    }

    private void showToDatePicker()
    {
        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        builder.setCalendarConstraints(dateLimitRangeEnd().build());
        builder.setTitleText(getString(R.string.lbl_select_to_date));
        MaterialDatePicker picker = builder.build();
        picker.show(requireFragmentManager(),picker.toString());
        picker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener()
        {
            @Override
            public void onPositiveButtonClick(Object selection)
            {
                long milliseconds = (long) selection;
                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTimeInMillis(milliseconds);
                toDate = selectedDate.getTime();
                strToDate = convertToStringInServerFormat(selectedDate.getTime());
                binding.toDate.setText(convertDateToStringInUKFormat(selectedDate.getTime()));
            }
        });
    }

    /*
 Limit selectable Date range
*/
    private CalendarConstraints.Builder dateLimitRangeStart()
    {
        Calendar calendarStart = Calendar.getInstance();
        calendarStart.add(Calendar.YEAR,-2);
        Calendar calendarEnd = Calendar.getInstance();
        CalendarConstraints.Builder constraintsBuilderRange = new CalendarConstraints.Builder();

        constraintsBuilderRange.setStart(calendarStart.getTimeInMillis());
        constraintsBuilderRange.setEnd(calendarEnd.getTimeInMillis());
        constraintsBuilderRange.setValidator(new RangeValidator(calendarStart.getTimeInMillis(), calendarEnd.getTimeInMillis()));

        return constraintsBuilderRange;
    }

    /*
 Limit selectable Date range
*/
    private CalendarConstraints.Builder dateLimitRangeEnd()
    {
        Calendar calendarStart = Calendar.getInstance();
        calendarStart.setTime(fromDate);
        Calendar calendarEnd = Calendar.getInstance();
        CalendarConstraints.Builder constraintsBuilderRange = new CalendarConstraints.Builder();

        constraintsBuilderRange.setStart(calendarStart.getTimeInMillis());
        constraintsBuilderRange.setEnd(calendarEnd.getTimeInMillis());
        constraintsBuilderRange.setValidator(new RangeValidator(calendarStart.getTimeInMillis(), calendarEnd.getTimeInMillis()));

        return constraintsBuilderRange;
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    private void callAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "TransactionDashboard");
            objMain.put("Todate", strToDate);
            objMain.put("Fromdate", strFromDate);
            objMain.put("AccountId", userBasicInfo.accountId);

        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                    /*    PhotosAdapter photosAdapter = new PhotosAdapter(response.body().photoDtos, getActivity());
                        rv_photos.setAdapter(photosAdapter);*/

                        binding.tvTransactions.setText("" + response.body().userDetailResult.dashBoardAPIResult.totalTransactions);
                        binding.tvTokensReceived.setText("" + response.body().userDetailResult.dashBoardAPIResult.totalRedeemed);
                        binding.tvTokensEarned.setText("" + response.body().userDetailResult.dashBoardAPIResult.totalEarned);
                        binding.tvRefTokensEarned.setText("" + response.body().userDetailResult.dashBoardAPIResult.referralTokenearned);
                        binding.tvReviewTokensEarned.setText("" + response.body().userDetailResult.dashBoardAPIResult.reviewTokenearned);
                        binding.tvCurrentBal.setText("" + response.body().userDetailResult.dashBoardAPIResult.currentBalanceToken);
                        binding.tvBonusToken.setText("" + response.body().userDetailResult.dashBoardAPIResult.bonusToken);
                        binding.tvCashSave.setText(strCurrencySymbol + response.body().userDetailResult.dashBoardAPIResult.cashSavedOiyaa);
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());

                        binding.tvTransactions.setText("0");
                        binding.tvTokensReceived.setText("0");
                        binding.tvTokensEarned.setText("0");
                        binding.tvRefTokensEarned.setText("0");
                        binding.tvReviewTokensEarned.setText("0");
                        binding.tvCurrentBal.setText("0");
                        binding.tvBonusToken.setText("0");
                        binding.tvCashSave.setText(strCurrencySymbol+"0");
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());

                binding.tvTransactions.setText("0");
                binding.tvTokensReceived.setText("0");
                binding.tvTokensEarned.setText("0");
                binding.tvRefTokensEarned.setText("0");
                binding.tvReviewTokensEarned.setText("0");
                binding.tvCurrentBal.setText("0");
                binding.tvBonusToken.setText("0");
                binding.tvCashSave.setText(strCurrencySymbol+"0");
            }
        });
    }

    private String pad(int data)
    {
        return data <= 9 ? ("0" + data) : (data + "");
    }

    private String convertDateToStringInUKFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    private String convertDateStringInUKFormat(String strDate)
    {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date date = inputFormat.parse(strDate);
            return convertDateToStringInUKFormat(date);
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private Date getDateFromString(String strDate)
    {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date date = inputFormat.parse(strDate);
            return date;
        }
        catch (Exception ex)
        {
            return new Date();
        }
    }

    private String convertToStringInServerFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }
}
