package com.localvalu.common.mytransaction.adapter;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.common.mytransaction.dto.TransactionHistoryListDto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

//import butterknife.BindView;
//import butterknife.ButterKnife;

public class TransactionHistoryListAdapter extends RecyclerView.Adapter<TransactionHistoryListAdapter.ViewHolder>
{
    public static final String TAG = TransactionHistoryListAdapter.class.getSimpleName();
    private Activity activity;
    private OnItemClickListener onClickListener;
    private ArrayList<TransactionHistoryListDto> transactionHistoryList;
    private String strCurrencySymbol;
    private String strCurrencyLetter;

    public TransactionHistoryListAdapter(Activity activity, ArrayList<TransactionHistoryListDto> transactionHistoryList)
    {
        this.activity = activity;
        this.transactionHistoryList = transactionHistoryList;
        strCurrencySymbol=activity.getString(R.string.lbl_currency_symbol_euro);
        strCurrencyLetter=activity.getString(R.string.lbl_currency_letter_euro);
    }

    @NonNull
    @Override
    public TransactionHistoryListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_eats_transaction_history_list, viewGroup, false);
        return new TransactionHistoryListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionHistoryListAdapter.ViewHolder viewHolder, final int i)
    {
        try
        {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            DateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

            viewHolder.tv_date.setText(outputFormat.format(formatter.parse(transactionHistoryList.get(i).transactionDate)));

            double saleAmount = transactionHistoryList.get(i).saleAmount;
            StringBuilder strSaleAmount = new StringBuilder();
            strSaleAmount.append(strCurrencySymbol);
            strSaleAmount.append(String.format("%.2f",saleAmount));

            viewHolder.tv_price.setText(strSaleAmount.toString());
            viewHolder.tv_view.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    onClickListener.onItemClick(i);
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount()
    {
        return transactionHistoryList.size();
    }

    public void setOnClickListener(OnItemClickListener onClickListener)
    {
        this.onClickListener = onClickListener;
    }

    public interface OnItemClickListener
    {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tv_date = itemView.findViewById(R.id.tv_date);
        TextView tv_price = itemView.findViewById(R.id.tv_price);
        TextView tv_view = itemView.findViewById(R.id.tv_view);

        public ViewHolder(@NonNull View itemView)
        {
            super(itemView);
        }
    }
}
