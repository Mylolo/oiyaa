package com.localvalu.common.mytransaction.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CustomerBalanceEnquiryData implements Serializable {

    @SerializedName("AccountId")
    @Expose
    public String accountId;

    @SerializedName("CurrentTokenBalance")
    @Expose
    public String currentTokenBalance;

    @SerializedName("TotalToken_earned")
    @Expose
    public String totalTokenEarned;

    @SerializedName("TotalToken_Redeemed")
    @Expose
    public String totalTokenRedeemed;
}
