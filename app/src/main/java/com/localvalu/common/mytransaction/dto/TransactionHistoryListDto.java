package com.localvalu.common.mytransaction.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TransactionHistoryListDto implements Serializable {

    @SerializedName("TransactionDate")
    @Expose
    public String transactionDate;

    @SerializedName("SIRTransID")
    @Expose
    public Integer sirTransID;

    @SerializedName("SaleAmount")
    @Expose
    public Float saleAmount;
}
