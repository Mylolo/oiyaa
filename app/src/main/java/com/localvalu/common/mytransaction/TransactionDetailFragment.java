package com.localvalu.common.mytransaction;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.localvalu.R;
import com.localvalu.base.BaseFragment;
import com.localvalu.base.HostActivityListener;
import com.localvalu.common.mytransaction.wrapper.UserDetailResultWrapper;
import com.localvalu.user.dto.UserBasicInfo;
import com.requestHandler.ApiClient;
import com.requestHandler.ApiInterface;
import com.utility.CommonUtilities;
import com.utility.DateUtility;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionDetailFragment extends BaseFragment
{
    private static final String TAG = "EatsTransactionDetailFr";

    //Toolbar
    private Toolbar toolbar;
    private TextView tvTitle;
    private int colorEats;
    private HostActivityListener hostActivityListener;

    public View view;
    private String id;

    private TextView tv_date, tv_merchant, tv_time, tv_payment_mode, tv_full_transaction_amount, tv_token_reedemed,
            tv_final_payment_made, tv_bonus_token_earned, tv_review_tokens_earned;

    private UserBasicInfo userBasicInfo;
    private ProgressDialog mProgressDialog;
    private ApiInterface apiInterface;

    // TODO: Rename and change types and number of parameters
    public static TransactionDetailFragment newInstance()
    {
        return new TransactionDetailFragment();
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        colorEats = ContextCompat.getColor(requireContext(), R.color.colorEats);
    }

    private void buildObjectForHandlingAPI()
    {
        apiInterface = ApiClient.getApiClientNew().create(ApiInterface.class);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.fragment_transaction_detail, container, false);
        buildObjectForHandlingAPI();
        initView();
        readFromBundle();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
    }

    private void initView()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);

        tv_date = view.findViewById(R.id.tv_date);
        tv_merchant = view.findViewById(R.id.tv_merchant);
        tv_time = view.findViewById(R.id.tv_time);
        tv_payment_mode = view.findViewById(R.id.tv_payment_mode);
        tv_full_transaction_amount = view.findViewById(R.id.tv_full_transaction_amount);
        tv_token_reedemed = view.findViewById(R.id.tv_token_reedemed);
        tv_final_payment_made = view.findViewById(R.id.tv_final_payment_made);
        tv_bonus_token_earned = view.findViewById(R.id.tv_bonus_token_earned);
        tv_review_tokens_earned = view.findViewById(R.id.tv_review_tokens_earned);
    }

    private void setUpActionBar()
    {
        toolbar = view.findViewById(R.id.toolbar);
        tvTitle = view.findViewById(R.id.tvTitle);
        tvTitle.setTextColor(colorEats);
        tvTitle.setText(getString(R.string.lbl_my_transaction));
        toolbar.getNavigationIcon().setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onBackButtonPressed();
            }
        });

    }

    private void readFromBundle()
    {
        id = getArguments().getString("id");
        callAPI();
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        // unbind the view to free some memory
    }

    private void callAPI()
    {
        mProgressDialog.show();
        JSONObject objMain = new JSONObject();
        try
        {
            objMain.put("Token", "/3+YFd5QZdSK9EKsB8+TlA==");
            objMain.put("RequestFor", "ViewTransaction");
            objMain.put("AccountId", userBasicInfo.accountId);
            objMain.put("TransID", id);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }


        final RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), objMain.toString());

        Call<UserDetailResultWrapper> dtoCall = apiInterface.getUserDetailFor(body);
        dtoCall.enqueue(new Callback<UserDetailResultWrapper>()
        {
            @Override
            public void onResponse(Call<UserDetailResultWrapper> call, Response<UserDetailResultWrapper> response)
            {
                mProgressDialog.dismiss();

                try
                {
                    if (response.body().userDetailResult.errorDetails.errorCode == 0)
                    {
                        DecimalFormat df = new DecimalFormat("0.00");
                        tv_date.setText("" + DateUtility.convertDateTimeFormat(response.body().userDetailResult.viewTransactionDetail.get(0).receiptDatetime));
                        tv_merchant.setText("" + response.body().userDetailResult.viewTransactionDetail.get(0).mTRadingName);
                        tv_time.setText("" + DateUtility.convertTimeToAmPm(response.body().userDetailResult.viewTransactionDetail.get(0).trTime));
                        tv_payment_mode.setText(response.body().userDetailResult.viewTransactionDetail.get(0).paymentMode);
                        tv_full_transaction_amount.setText("£" + df.format(response.body().userDetailResult.viewTransactionDetail.get(0).receiptValue));
                        tv_token_reedemed.setText("" + response.body().userDetailResult.viewTransactionDetail.get(0).tokenRedeemed);
                        tv_final_payment_made.setText("£" + df.format(response.body().userDetailResult.viewTransactionDetail.get(0).finalPay));
                        tv_bonus_token_earned.setText("" + response.body().userDetailResult.viewTransactionDetail.get(0).bonusToken);
                        tv_review_tokens_earned.setText(" ");
                    }
                    else
                    {
                        DialogUtility.showMessageWithOk(response.body().userDetailResult.errorDetails.errorMessage, getActivity());
                    }
                }
                catch (Exception e)
                {

                }
            }

            @Override
            public void onFailure(Call<UserDetailResultWrapper> call, Throwable t)
            {
                mProgressDialog.dismiss();
                Log.d(TAG, t.getMessage());

                if (!CommonUtilities.checkConnectivity(getContext()))
                    DialogUtility.showMessageWithOk(getString(R.string.network_unavailable), getActivity());
                else
                    DialogUtility.showMessageWithOk(getString(R.string.server_alert), getActivity());
            }
        });
    }

    public void onButtonClick(View view)
    {
        switch (view.getId())
        {
            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
        }
    }

}
