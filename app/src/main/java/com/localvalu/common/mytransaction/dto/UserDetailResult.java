package com.localvalu.common.mytransaction.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.eats.transfertokens.dto.TransferLTFriendResult;
import com.localvalu.user.dto.ChangePasswordResultDto;
import com.localvalu.user.dto.ErrorDetailsDto;
import com.localvalu.user.dto.ForgotPasswordResultDto;

import java.io.Serializable;
import java.util.ArrayList;

public class UserDetailResult implements Serializable {

    @SerializedName("ErrorDetails")
    @Expose
    public ErrorDetailsDto errorDetails;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("msg")
    @Expose
    public String msg;

    @SerializedName("CustomerBalanceEnquiryResult")
    @Expose
    public CustomerBalanceEnquiryData customerBalanceEnquiryData;

    @SerializedName("UserDashboardByRequestForDetails")
    @Expose
    public ChangePasswordResultDto changePasswordResultDto;

    @SerializedName("ForgotPasswordResult")
    @Expose
    public ForgotPasswordResultDto forgotPasswordResultDto;

    @SerializedName("MerchantListResult")
    @Expose
    public ArrayList<MerchantListDto> merchantList;

    @SerializedName("TransactionHistoryListResult")
    @Expose
    public ArrayList<TransactionHistoryListDto> transactionHistoryList;

    @SerializedName("ViewTransactionList")
    @Expose
    public ArrayList<TransactionDetailDto> viewTransactionDetail;

    @SerializedName("TransactionDashboardResult")
    @Expose
    public DashBoardAPIResult dashBoardAPIResult;

    @SerializedName("TransferLTFriendResult")
    @Expose
    public TransferLTFriendResult transferLTFriendResults;
}
