package com.localvalu.common.mytransaction.wrapper;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.localvalu.common.mytransaction.dto.UserDetailResult;

import java.io.Serializable;

public class UserDetailResultWrapper implements Serializable {

    @SerializedName("UserDashboardByRequestForDetailsResult")
    @Expose
    public UserDetailResult userDetailResult;
}
