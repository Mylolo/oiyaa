package com.localvalu.common.repository

import com.localvalu.base.BaseRepository
import com.localvalu.common.api.InsertLitterApi
import com.localvalu.common.model.litter.InsertLitterRequest
import com.localvalu.registration.api.PassionListApi
import com.localvalu.user.dto.PassionListRequest

class InsertLitterRepository (private val api: InsertLitterApi): BaseRepository(api)
{
    suspend fun insertLitter(insertLitterRequest: InsertLitterRequest) = safeApiCall {
        api.insertLitter(insertLitterRequest)
    }
}