package com.localvalu.common.viewmodel

import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import com.localvalu.R
import com.localvalu.base.Resource
import com.localvalu.common.model.SearchRequest
import com.localvalu.common.model.SearchResponse
import com.localvalu.common.model.litter.InsertLitterRequest
import com.localvalu.common.model.litter.InsertLitterResponse
import com.localvalu.common.repository.InsertLitterRepository
import com.localvalu.common.ui.TestInsertLitterFragment
import com.localvalu.extensions.convertDateTimeToStringInDisplayFormat
import com.localvalu.extensions.getDifferenceInMinutes
import com.localvalu.home.repository.SearchListRepository
import com.localvalu.registration.model.UserExistRequest
import com.localvalu.registration.repository.PassionListRepository
import com.localvalu.registration.repository.UserExistRepository
import com.localvalu.user.ValidationUtils
import com.localvalu.user.dto.PassionListRequest
import com.localvalu.user.wrapper.PassionListResultWrapper
import com.localvalu.user.wrapper.RegisterExistsCustomerResultWrapper
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class InsertLitterViewModel @Inject constructor(@ApplicationContext private val context:Context,
                                                private val repository:InsertLitterRepository) : ViewModel()
{
    private val TAG = "InsertLitterViewModel"

    private val _startTime:MutableLiveData<Date> = MutableLiveData()
    val observeDisplayStarTime:LiveData<String> = Transformations.map(_startTime){
        it?.convertDateTimeToStringInDisplayFormat()
    };

    private val _endTime:MutableLiveData<Date> = MutableLiveData()
    val observeDisplayEndTime:LiveData<String> = Transformations.map(_endTime){
        it?.convertDateTimeToStringInDisplayFormat()
    };

    private val _postCode:MutableLiveData<String> = MutableLiveData()
    val observePostCode:LiveData<String>
        get() = _postCode

    init
    {
        _startTime.value = null
        _endTime.value = null
    }

    private val _insertLitterResponse: MutableLiveData<Resource<InsertLitterResponse>> = MutableLiveData()
    val insertLitterResponse: LiveData<Resource<InsertLitterResponse>>
        get() = _insertLitterResponse

    fun insertLitter(insertLitterRequest: InsertLitterRequest) = viewModelScope.launch {
        _insertLitterResponse.value = Resource.Loading
        _insertLitterResponse.value = repository.insertLitter(insertLitterRequest)
    }

    private val _startTimePickerClicked :MutableLiveData<Boolean> = MutableLiveData()
    val observeStartTimerPickerClicked:LiveData<Boolean>
        get() = _startTimePickerClicked

    private val _endTimePickerClicked :MutableLiveData<Boolean> = MutableLiveData()
    val observeEndTimerPickerClicked:LiveData<Boolean>
        get() = _endTimePickerClicked

    private val _showStartTimeError:MutableLiveData<String> = MutableLiveData()
    val observeShowStartTimeError:LiveData<String>
        get() = _showStartTimeError

    private val _showEndTimeError:MutableLiveData<String> = MutableLiveData()
    val observeShowEndTimeError:LiveData<String>
        get() = _showEndTimeError

    private val _showPostCodeError:MutableLiveData<String> = MutableLiveData()
    val observeShowPostCodeError:LiveData<String>
        get() = _showPostCodeError

    private val _validStartTime:MutableLiveData<Boolean> = MutableLiveData()
    val validStartTime:LiveData<Boolean>
        get() = _validStartTime

    private val _validEndTime:MutableLiveData<Boolean> = MutableLiveData()
    val validEndTime:LiveData<Boolean>
        get() = _validEndTime

    private val _validPostCode:MutableLiveData<Boolean> = MutableLiveData()
    val validPostCode:LiveData<Boolean>
        get() = _validPostCode

    private val _areValidInputs:MutableLiveData<Boolean> = MutableLiveData()
    val areValidInputs:LiveData<Boolean>
        get() = _areValidInputs

    fun showHideStartPicker(value:Boolean)
    {
        _startTimePickerClicked.value = value
    }

    fun showHideEndPicker(value: Boolean)
    {
        _endTimePickerClicked.value = value
    }

    fun setStartTime(value:Date)
    {
        _startTime.value =value
    }

    fun setEndTime(value:Date)
    {
        _endTime.value = value
    }

    fun setPostCode(value:String)
    {
        _postCode.postValue(value)
        validatePostCode()
    }

    fun validateStartTime()
    {
        if(_startTime.value==null)
        {
            _showStartTimeError.value = context.getString(R.string.msg_select_start_time)
            _validStartTime.value=false
            return
        }
        else if(_startTime!!.value?.after(Date())!!)
        {
            _showStartTimeError.value = (context.getString(R.string.msg_start_time_should_not_be_future_time))
            _validStartTime.value=false
            return
        }
        _endTime?.value?.let {
            if(_startTime!!.value?.after(_endTime.value)!!)
            {
                _showStartTimeError.value = context.getString(R.string.msg_start_time_should_be_less_time)
                _validStartTime.value=false
                return
            }
            else if(_startTime!!.value?.getDifferenceInMinutes(_endTime.value!!)==0L)
            {
                _showStartTimeError.value = context.getString(R.string.msg_end_time_start_time_should_not_be_same)
                _validStartTime.value=false
                return
            }
        }
        _validStartTime.value=true
    }

    fun validateEndTime()
    {
        if(_endTime.value==null)
        {
            _showEndTimeError.value = context.getString(R.string.msg_select_end_time)
            _validEndTime.value=false
            return
        }
        else if(_endTime!!.value?.after(Date())!!)
        {
            _showEndTimeError.value = context.getString(R.string.msg_end_time_should_not_be_future_time)
            _validEndTime.value=false
            return
        }
        _startTime?.value?.let{
            if(_endTime!!.value?.before(_startTime.value)!!)
            {
                _showEndTimeError.value = context.getString(R.string.msg_end_time_should_not_be_less_start_time)
                _validEndTime.value=false
                return
            }
            else if(_startTime!!.value?.getDifferenceInMinutes(_endTime.value!!)==0L)
            {
                _showEndTimeError.value = context.getString(R.string.msg_end_time_start_time_should_not_be_same)
                _validEndTime.value=false
                return
            }
        }
        _validEndTime.value=true
    }

    private fun validatePostCode()
    {
        if(_postCode.value.isNullOrEmpty())
        {
            _showPostCodeError.value = context.getString(R.string.msg_enter_post_code)
            _validPostCode.value=false
            return
        }
        else if(!ValidationUtils.isValidPostCode(_postCode.value))
        {
            _showPostCodeError.value = context.getString(R.string.msg_enter_valid_post_code)
            _validPostCode.value=false
            return
        }
        _validPostCode.value=true
    }

    fun setTime(isStartTime:Boolean,hour:Int,minute:Int)
    {
        var pickedHour: Int = hour
        var pickedamPm: Int = 0;
        /*if (pickedHour > 12)
        {
            pickedHour -= 12
            pickedamPm = 1
        }*/
        val pickedMinute: Int = minute
        Log.d(TAG, " pickedHour : $pickedHour, pickedMinute : $pickedHour")
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR, pickedHour)
        calendar.set(Calendar.MINUTE, pickedMinute)
        //calendar.set(Calendar.AM_PM, pickedamPm)
        if(isStartTime)
        {
            Log.d(TAG, "startTime : ${calendar.time}")
            _startTime.value = calendar.time
            validateStartTime()
        }
        else
        {
            Log.d(TAG, "endTime : ${calendar.time}")
            _endTime.value = calendar.time
            validateEndTime()
        }
    }

}