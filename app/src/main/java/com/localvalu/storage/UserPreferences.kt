package com.localvalu.storage

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.utility.AppUtils
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

// At the top level of your kotlin file:
val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = AppUtils.APP_PREFERENCES)

class UserPreferences @Inject constructor(context: Context)
{
    val APP_USER_OBJECT = stringPreferencesKey("App_Object_User")

    private val applicationContext = context.applicationContext

    val userString: Flow<String>? = applicationContext.dataStore.data.map {
        it[APP_USER_OBJECT]?: ""
    }

    suspend fun saveUserObject(strUser: String)
    {
        applicationContext.dataStore.edit { it ->
           it[APP_USER_OBJECT] = strUser
        }
    }

    suspend fun clear() {
        applicationContext.dataStore.edit { it ->
            it.clear()
        }
    }

}