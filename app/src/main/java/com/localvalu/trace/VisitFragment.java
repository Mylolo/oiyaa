package com.localvalu.trace;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.localvalu.R;
import com.localvalu.base.HostActivityListener;
import com.localvalu.databinding.FragmentVisitBinding;
import com.localvalu.user.dto.UserBasicInfo;
import com.utility.AppUtils;
import com.utility.PreferenceUtility;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link VisitFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VisitFragment extends Fragment
{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FragmentVisitBinding binding;
    private UserBasicInfo userBasicInfo;
    private int colorEats;
    private HostActivityListener hostActivityListener;

    public VisitFragment()
    {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static VisitFragment newInstance()
    {
        VisitFragment fragment = new VisitFragment();
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        hostActivityListener = (HostActivityListener) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        colorEats = ContextCompat.getColor(requireContext(),R.color.colorEats);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        binding = FragmentVisitBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        setUpActionBar();
        setUserInfo();
    }

    private void setUpActionBar()
    {
        binding.toolbarLayout.tvTitle.setTextColor(colorEats);
        binding.toolbarLayout.tvTitle.setText(getString(R.string.title_tab_track_and_trance));
        binding.toolbarLayout.toolbar.getNavigationIcon().setColorFilter(colorEats, PorterDuff.Mode.SRC_ATOP);
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                hostActivityListener.onMenuClicked();
            }
        });
    }

    private void setUserInfo()
    {
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(requireContext(), PreferenceUtility.APP_PREFERENCE_NAME);
        if (userBasicInfo.qRCodeOutput == null)
        {
            userBasicInfo.qRCodeOutput = "0";
        }
        binding.ivQRImage.setImageBitmap(AppUtils.generateQRCode(userBasicInfo.qRCodeOutput));
        binding.tvQRCode.setText(userBasicInfo.qRCodeOutput);
    }
}