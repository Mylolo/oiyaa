package com.localvalu.base

import android.content.Context
import com.localvalu.BuildConfig
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RemoteDataSource
{
    fun <Api> buildApi(api: Class<Api>,context: Context):Api
    {
        return Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
            .client(getRetrofitClient(null))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(api);
    }

    private fun buildTokenApi(): TokenRefreshApi
    {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(getRetrofitClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TokenRefreshApi::class.java)
    }

    private fun getRetrofitClient(authenticator: Authenticator? = null):OkHttpClient
    {
        return OkHttpClient.Builder()
            .readTimeout(60,TimeUnit.SECONDS)
            .addInterceptor { chain ->
                chain.proceed(chain.request().newBuilder().also {
                    it.addHeader("Accept", "application/json")
                }.build())
            }.also { client ->
                authenticator?.let { client.authenticator(it) }
                if (BuildConfig.DEBUG) {
                    val logging = HttpLoggingInterceptor()
                    logging.setLevel(HttpLoggingInterceptor.Level.BODY)
                    client.addInterceptor(logging)
                }
            }.build()
    }
}