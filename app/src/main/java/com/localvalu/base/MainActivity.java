package com.localvalu.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.localvalu.BuildConfig;
import com.localvalu.R;
import com.localvalu.common.review.ReviewsListFragment;
import com.localvalu.common.transfertokens.TransferTokensFragment;
import com.localvalu.common.ui.InsertLitterFragment;
import com.localvalu.common.ui.TestInsertLitterFragment;
import com.localvalu.databinding.ActivityMainBinding;
import com.localvalu.directory.notification.DirectoryNotifcationFragment;
import com.localvalu.directory.scanpay.ScanPayFragment;
import com.localvalu.directory.webview.DirectoryWebviewActivity;
import com.localvalu.common.myaccount.AccountFragment;
import com.localvalu.eats.myorder.ui.MyOrdersListFragment;
import com.localvalu.common.mytransaction.TransactionListFragment;
import com.localvalu.eats.notification.EatsNotifcationFragment;
import com.localvalu.common.referfriend.ReferFriendsFragment;
import com.localvalu.eats.webview.EatsWebviewActivity;
import com.localvalu.home.HomeDashboardFragment;
import com.localvalu.mall.MallDashboardFragment;
import com.localvalu.mall.mytransaction.MallMyTransactionListFragment;
import com.localvalu.mall.myvouchers.MallMyVouchersFragment;
import com.localvalu.mall.notification.MallNotifcationFragment;
import com.localvalu.mall.webview.MallWebviewActivity;
import com.localvalu.requestappointment.ui.RequestAppointmentMainFragment;
import com.localvalu.tablebooking.ui.TableBookingFragment;
import com.localvalu.trace.VisitFragment;
import com.localvalu.user.LoginActivity;
import com.localvalu.user.dto.UserBasicInfo;
import com.utility.ActivityController;
import com.utility.AppUtils;
import com.utility.PreferenceUtility;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

import static com.utility.AppUtils.NOTIFICATION_ORDER_PLACED;
import static com.utility.AppUtils.NOTIFICATION_REQUEST_APPOINTMENT;
import static com.utility.AppUtils.NOTIFICATION_REVIEWED_BUSINESS;
import static com.utility.AppUtils.NOTIFICATION_TABLE_BOOKED;

import org.json.JSONObject;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, HostActivityListener
{
    private static final String TAG = "MainActivity";

    public String activityName = "";
    public BottomNavigationView bottomNavigation;
    //For API Call
    public ProgressDialog mProgressDialog;
    public String fragment_tag = "";//Stop to opening same page muti
    public int fragment_no = 0;//Set the direction of page animation
    private Fragment selectedFragment = null;
    //private CircleImageView imageView;
    private AppCompatImageView ivQRCode, imgMenu, imgBack;
    private ConstraintLayout cnlSearchToolbar;
    private MaterialButton btnSearchText, btnSearchLocation, btnSearchFilter;
    private int colorBottomMenuNormal,colorBottomMenuSelected;
    private TextView tv_back, tv_header, tvUserName, tvUserEmail, tvQRCode;
    private AppBarLayout include1;
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    //public View view_footer;
    private boolean locationEnabled = false;
    private int bottomMenuType;
    private UserBasicInfo userBasicInfo;
    int[][] textColorStates = new int[][]{
            new int[]{-android.R.attr.state_checked},
            new int[]{android.R.attr.state_checked}
    };
    private int calledHeaderMethod = -1;
    private String previousTitle = "";
    private int previousType = -1;
    private int previousOption = -1;

    private String currentTitle = "";
    private int currentType = -1;
    private int currentOption = -1;
    private boolean activityToolbar = false;

    private Drawable drawableNavItemBackground;
    private int colorLeftNavViewBackgroundNormal;
    private int colorLeftNavViewBackgroundSelected;

    private int previouslyCalledHeaderMethod = -1;

    private List<NavigateDetails> navigateList = new ArrayList<>(20);

    private boolean clearList = false;

    private boolean addToList = false;

    private ActivityMainBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate");
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(this, PreferenceUtility.APP_PREFERENCE_NAME);
        navigateList = new ArrayList<>(20);
        colorBottomMenuNormal = ContextCompat.getColor(this, R.color.colorBottomMenuNormal);
        colorBottomMenuSelected = ContextCompat.getColor(this, R.color.colorBottomMenuSelected);

        colorLeftNavViewBackgroundNormal = ContextCompat.getColor(this, R.color.colorTransparent);
        colorLeftNavViewBackgroundSelected = ContextCompat.getColor(this, R.color.colorLightGray);
        binding.bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        binding.bottomNavigation.setItemIconTintList(null);
        if(BuildConfig.DEBUG)
        {
            binding.bottomNavigation.getMenu().findItem(R.id.navigation_mall).setVisible(true);
        }
        else
        {
            binding.bottomNavigation.getMenu().findItem(R.id.navigation_mall).setVisible(false);
        }

        bouldObjectForHandlingAPI();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        binding.navigationView.setNavigationItemSelectedListener(this);
        View headerView = binding.navigationView.getHeaderView(0);
        ivQRCode = headerView.findViewById(R.id.ivQRCode);
        tvQRCode = headerView.findViewById(R.id.tvQRCode);
        tvUserName = headerView.findViewById(R.id.tvUserName);
        tvUserEmail = headerView.findViewById(R.id.tvUserEmail);

        System.out.println(userBasicInfo.toString());
        if (userBasicInfo.image != null)
        {
            try
            {
                if (userBasicInfo.qRCodeOutput == null)
                {
                    userBasicInfo.qRCodeOutput = "0";
                }

                Bitmap qrCodeImage = generateQRCode(userBasicInfo.qRCodeOutput);
                ivQRCode.setImageBitmap(qrCodeImage);
                tvQRCode.setText(userBasicInfo.qRCodeOutput);
            }
            catch (Exception e)
            {

            }
        }
        tvUserName.setText(userBasicInfo.name);
        tvUserEmail.setText(userBasicInfo.email);

        if (savedInstanceState == null)
        {

        }
        else
        {
            selectedFragment = this.getSupportFragmentManager().findFragmentById(R.id.flMain);
        }
        readFromBundle();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener()
    {

        @SuppressLint("ResourceAsColor")
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item)
        {
            hideKeyboard();
            Menu nav_Menu = binding.navigationView.getMenu();
            int colorSelected = 0;
            int windowBackgroundColor = 0;

            switch (item.getItemId())
            {
                /*case R.id.bottomNavigation_scanpay:
                    //if (!fragment_tag.equals(ScanPayFragment.class.getName()))
                    // {
                    removeAllFragment();
                    setMenuItemColor();
                    fragment_tag = ScanPayFragment.class.getName();
                    addFragment(ScanPayFragment.newInstance(), false, ScanPayFragment.class.getName(), fragment_no < 1);
                    fragment_no = 0;
                    //}
                    setTheme(R.style.AppThemeScanPay);
                    colorSelected = ContextCompat.getColor(MainActivity.this,R.color.colorScanAndPay);
                    setIconColor(colorSelected);
                    setTabTextColor("Scan & Pay", R.id.bottomNavigation_scanpay, colorSelected);
                    setLeftNavigationIconColor(colorSelected);
                    fl_main.setBackgroundColor(ContextCompat.getColor(MainActivity.this,R.color.colorScanAndPayLightBackground));
                    setHeaderLayoutTextColor(getResources().getColor(R.color.colorScanAndPay));
                    nav_Menu.findItem(R.id.nav_voucher).setVisible(false);
                    nav_Menu.findItem(R.id.nav_scanpay).setVisible(false);
                    nav_Menu.findItem(R.id.nav_cart).setVisible(false);
                    nav_Menu.findItem(R.id.nav_order).setVisible(false);
                    return true;*/
                case R.id.navigation_eats:
                    //if (!fragment_tag.equals(EatsDashboardFragment.class.getName()))
                    //{
                    removeAllFragment();
                    setMenuItemColor();
                    //fragment_tag = EatsDashboardFragment.class.getName();
                    //addFragment(EatsDashboardFragment.newInstance(), false, EatsDashboardFragment.class.getName(), fragment_no < 1);
                    Bundle bundle = new Bundle();
                    bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,AppUtils.BMT_EATS);
                    fragment_tag = HomeDashboardFragment.class.getName();
                    HomeDashboardFragment homeDashboardFragment = new HomeDashboardFragment();
                    homeDashboardFragment.setArguments(bundle);
                    addFragment(homeDashboardFragment, false, HomeDashboardFragment.class.getName(), fragment_no < 1);
                    fragment_no = 1;
                    //}
                    setTheme(R.style.AppTheme);
                    binding.navigationView.setCheckedItem(R.id.nav_home);
                    colorSelected = ContextCompat.getColor(MainActivity.this, R.color.colorBlack);
                    setIconColor(colorBottomMenuSelected);
                    setLeftNavigationIconColor(colorSelected);
                    setTabTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorEats));
                    binding.flMain.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorEatsLightBackground));
                    setHeaderLayoutTextColor(getResources().getColor(R.color.colorBlack));
                    showEatsMenu(nav_Menu);
                    return true;
                case R.id.navigation_local:
                    //if (!fragment_tag.equals(DirectoryDashboardFragment.class.getName()))
                    //{
                    removeAllFragment();
                    removeAllFragment();
                    setMenuItemColor();
                    //fragment_tag = DirectoryDashboardFragment.class.getName();
                    //addFragment(DirectoryDashboardFragment.newInstance(), false, DirectoryDashboardFragment.class.getName(), fragment_no < 2);
                    Bundle bundleLocal = new Bundle();
                    bundleLocal.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,AppUtils.BMT_LOCAL);
                    fragment_tag = HomeDashboardFragment.class.getName();
                    HomeDashboardFragment homeDashboardFragmentLocal = new HomeDashboardFragment();
                    homeDashboardFragmentLocal.setArguments(bundleLocal);
                    addFragment(homeDashboardFragmentLocal, false, HomeDashboardFragment.class.getName(), fragment_no < 2);
                    fragment_no = 2;
                    //}
                    setTheme(R.style.AppTheme);
                    binding.navigationView.setCheckedItem(R.id.nav_home);
                    colorSelected = ContextCompat.getColor(MainActivity.this, R.color.colorBlack);
                    setIconColor(colorBottomMenuSelected);
                    setLeftNavigationIconColor(colorSelected);
                    setTabTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorLocal));
                    binding.flMain.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorLocalLightBackground));
                    setHeaderLayoutTextColor(getResources().getColor(R.color.colorBlack));
                    showLocalMenu(nav_Menu);
                    return true;
                case R.id.navigation_mall:
                    //if (!fragment_tag.equals(MallDashboardFragment.class.getName()))
                    //{
                    removeAllFragment();
                    setMenuItemColor();
                    fragment_tag = MallDashboardFragment.class.getName();
                    addFragment(MallDashboardFragment.newInstance(), false, MallDashboardFragment.class.getName(), fragment_no < 3);
                    fragment_no = 3;
                    //}
                    //setTheme(R.style.AppThemeMall);
                    binding.navigationView.setCheckedItem(R.id.nav_home);
                    colorSelected = ContextCompat.getColor(MainActivity.this, R.color.colorMall);
                    setIconColor(colorBottomMenuSelected);
                    setLeftNavigationIconColor(colorSelected);
                    setTabTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorMall));
                    binding.flMain.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorMallLightBackground));
                    setHeaderLayoutTextColor(getResources().getColor(R.color.colorMall));
                    showMallMenu(nav_Menu);
                    return true;
                case R.id.navigation_track_and_trace:
                    removeAllFragment();
                    setMenuItemColor();
                    fragment_tag = MallDashboardFragment.class.getName();
                    addFragment(VisitFragment.newInstance(), false, MallDashboardFragment.class.getName(), fragment_no < 3);
                    fragment_no = 3;
                    //}
                    setTheme(R.style.AppTheme);
                    binding.navigationView.setCheckedItem(R.id.nav_home);
                    colorSelected = ContextCompat.getColor(MainActivity.this, R.color.colorBlack);
                    setIconColor(colorBottomMenuSelected);
                    setLeftNavigationIconColor(colorSelected);
                    setTabTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorEats));
                    binding.flMain.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.colorEatsLightBackground));
                    setHeaderLayoutTextColor(getResources().getColor(R.color.colorBlack));
                    showEatsMenu(nav_Menu);
                    return true;
                case R.id.navigation_more:
                    showLeftMenu();
                    return true;
            }
            return false;
        }
    };

    private void showTrackAndTraceMenu(Menu nav_Menu)
    {
        nav_Menu.findItem(R.id.nav_voucher).setVisible(false);
        nav_Menu.findItem(R.id.nav_cart).setVisible(false);
        nav_Menu.findItem(R.id.nav_order).setVisible(true);
    }

    private void showEatsMenu(Menu nav_Menu)
    {
        nav_Menu.findItem(R.id.nav_voucher).setVisible(false);
        nav_Menu.findItem(R.id.nav_cart).setVisible(false);
        nav_Menu.findItem(R.id.nav_order).setVisible(true);
        nav_Menu.findItem(R.id.nav_table_booking).setVisible(true);
    }

    private void showLocalMenu(Menu nav_Menu)
    {
        nav_Menu.findItem(R.id.nav_voucher).setVisible(false);
        nav_Menu.findItem(R.id.nav_cart).setVisible(false);
        nav_Menu.findItem(R.id.nav_order).setVisible(true);
        nav_Menu.findItem(R.id.nav_table_booking).setVisible(true);
    }

    private void showMallMenu(Menu nav_Menu)
    {
        nav_Menu.findItem(R.id.nav_voucher).setVisible(true);
        nav_Menu.findItem(R.id.nav_cart).setVisible(false);
        nav_Menu.findItem(R.id.nav_order).setVisible(false);
        nav_Menu.findItem(R.id.nav_table_booking).setVisible(false);
    }

    private void showLeftMenu()
    {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        }
        else
        {
            binding.drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    public static void setLightStatusBar(boolean isLight, Activity activity, View view)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            View decor = activity.getWindow().getDecorView();
            if (isLight)
            {
                //  delayTransition(view);
                //  include1.setVisibility(View.VISIBLE);
                decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                activity.getWindow().setStatusBarColor(Color.WHITE);
            }
            else
            {
                // We want to change tint color to white again.
                // You can also record the flags in advance so that you can turn UI back completely if
                // you have set other flags before, such as translucent or full screen.
                decor.setSystemUiVisibility(0);
                activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDark));
            }
        }
    }

    @SuppressLint("RestrictedApi")
    public static void disableShiftMode(BottomNavigationView view)
    {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try
        {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++)
            {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        }
        catch (NoSuchFieldException e)
        {
            //Timber.e(e, "Unable to get shift mode field");
        }
        catch (IllegalAccessException e)
        {
            //Timber.e(e, "Unable to change value of shift mode");
        }
    }

    private void setTabTextColor(int color)
    {
        int[] textColors = new int[]{colorBottomMenuNormal, color};
        ColorStateList colorStateList = new ColorStateList(textColorStates, textColors);
        binding.bottomNavigation.setItemTextColor(colorStateList);
    }

    private void setIconColor(int color)
    {
        int[] textColors = new int[]{colorBottomMenuNormal, color};
        ColorStateList colorStateList = new ColorStateList(textColorStates, textColors);
        binding.bottomNavigation.setItemIconTintList(colorStateList);
    }

    private void setLeftNavigationIconColor(int color)
    {
        int[] iconColors = new int[]{color, colorBottomMenuNormal};
        ColorStateList colorStateListIcon = new ColorStateList(textColorStates, iconColors);
        binding.navigationView.setItemIconTintList(colorStateListIcon);

        int[] textColors = new int[]{colorBottomMenuNormal, color};
        ColorStateList colorStateListText = new ColorStateList(textColorStates, textColors);
        binding.navigationView.setItemTextColor(colorStateListText);

        binding.navigationView.setItemBackground(makeSelector(colorLeftNavViewBackgroundSelected));
    }

    public StateListDrawable makeSelector(int color)
    {
        StateListDrawable res = new StateListDrawable();
        res.setExitFadeDuration(400);
        res.setAlpha(45);
        res.addState(new int[]{android.R.attr.state_pressed}, new ColorDrawable(color));
        res.addState(new int[]{android.R.attr.state_checked}, new ColorDrawable(color));
        res.addState(new int[]{}, new ColorDrawable(Color.TRANSPARENT));
        return res;
    }

    private void setHeaderLayoutTextColor(int color)
    {
        tvUserName.setTextColor(color);
        tvUserEmail.setTextColor(color);
        tvQRCode.setTextColor(color);
    }

    private Bitmap generateQRCode(String data)
    {
        Bitmap retVal;

        try
        {
            int size = 420;

            BitMatrix bitMatrix = new MultiFormatWriter().encode(data, BarcodeFormat.QR_CODE, size, size, null);

            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();

            int[] pixels = new int[width * height];

            for (int i = 0; i < height; i++)
            {
                int offset = i * width;

                for (int j = 0; j < width; j++)
                {
                    pixels[offset + j] = bitMatrix.get(j, i) ? BLACK : WHITE;
                }
            }

            retVal = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            retVal.setPixels(pixels, 0, size, 0, 0, width, height);
        }
        catch (Exception e)
        {
            return null;
        }


        return retVal;
    }

    private void readFromBundle()
    {

        try
        {
            bottomMenuType = getIntent().getExtras().getInt(AppUtils.BUNDLE_NAVIGATION_TYPE);
        }
        catch (Exception e)
        {
            bottomMenuType = -1;
        }
        switch (bottomMenuType)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
                 fragment_no = 0;
                 binding.bottomNavigation.setSelectedItemId(R.id.navigation_track_and_trace);
                 break;
            case AppUtils.BMT_EATS:
                 fragment_no = 1;
                 binding.bottomNavigation.setSelectedItemId(R.id.navigation_eats);
                 break;
            case AppUtils.BMT_LOCAL:
                 fragment_no = 2;
                 binding.bottomNavigation.setSelectedItemId(R.id.navigation_local);
                 break;
            case AppUtils.BMT_MALL:
                 fragment_no = 3;
                 binding.bottomNavigation.setSelectedItemId(R.id.navigation_mall);
                 break;

        }

    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"onNewIntent");
        }
        boolean isFromNotification=false;
        try
        {
            isFromNotification = intent.getExtras().getBoolean(AppUtils.BUNDLE_IS_FROM_NOTIFICATION_PAGE);
            if(isFromNotification)
            {
                int action = intent.getExtras().getInt(AppUtils.BUNDLE_NOTIFICATION_ACTION);
                switch (action)
                {
                    case NOTIFICATION_ORDER_PLACED:
                         break;
                    case NOTIFICATION_TABLE_BOOKED:
                         gotoTableBookingListFragment(3);
                         break;
                    case NOTIFICATION_REQUEST_APPOINTMENT:
                         gotoRequestAppointmentListFragment(3);
                         break;
                    case NOTIFICATION_REVIEWED_BUSINESS:
                         String jsonString = intent.getExtras().getString(AppUtils.BUNDLE_NOTIFICATION_DATA);
                         try
                         {
                             JSONObject jsonObject = new JSONObject(jsonString);
                             String businessId = jsonObject.optString("businessId");
                             gotoMyReviewsFragment(businessId);
                         }
                         catch (Exception ex)
                         {
                             ex.printStackTrace();
                         }
                         break;
                    default:
                         break;
                }
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public void setMenuItemColor()
    {
        /*bottomNavigation.getMenu().findItem(R.id.bottomNavigation_eats).setIcon(R.drawable.tab_loloeats);
        bottomNavigation.getMenu().findItem(R.id.bottomNavigation_local).setIcon(R.drawable.tab_lololocal);
        bottomNavigation.getMenu().findItem(R.id.bottomNavigation_mall).setIcon(R.drawable.tab_lolomall);
        bottomNavigation.getMenu().findItem(R.id.bottomNavigation_more).setIcon(R.drawable.tab_lolomore);

        int colorWhite = ContextCompat.getColor(this, R.color.colorWhite);
        setTabTextColor(colorWhite);
        setTabTextColor(colorWhite);
        setTabTextColor(colorWhite);
        setTabTextColor(colorWhite);*/
    }

    public void setHeader(String title, int type)
    {
        include1.setVisibility(View.VISIBLE);

        currentTitle = tv_header.getText().toString();
        previousTitle = currentTitle;
        currentTitle = title;

        previousType = currentType;
        currentType = type;

        previousOption = currentOption;

        previouslyCalledHeaderMethod = calledHeaderMethod;

        imgMenu.setVisibility(View.VISIBLE);
        imgBack.setVisibility(View.GONE);
        tv_back.setVisibility(View.GONE);
        tv_header.setVisibility(View.GONE);

        tv_header.setText(title);
        switch (type)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
                imgMenu.setColorFilter(getResources().getColor(R.color.colorEats));
                hideSearchBarMenu();
                break;
            case AppUtils.BMT_EATS:
                imgMenu.setColorFilter(getResources().getColor(R.color.colorEats));
                showSearchMenu();
                setSearchBarMenuColor(ContextCompat.getColor(this, R.color.colorEats));
                break;
            case AppUtils.BMT_LOCAL:
                imgMenu.setColorFilter(getResources().getColor(R.color.colorLocal));
                showSearchMenu();
                setSearchBarMenuColor(ContextCompat.getColor(this, R.color.colorLocal));
                break;
            case AppUtils.BMT_MALL:
                imgMenu.setColorFilter(getResources().getColor(R.color.colorMall));
                hideSearchBarMenu();
                break;
        }

        calledHeaderMethod = 0;

        addToNavigateList();
    }

    public void setHeaderWithBackFilterButton(int type, int option)
    {
        include1.setVisibility(View.VISIBLE);

        previousType = currentType;
        currentType = type;

        previousOption = currentOption;
        currentOption = option;

        previousTitle = currentTitle;

        previouslyCalledHeaderMethod = calledHeaderMethod;

        imgMenu.setVisibility(View.GONE);
        imgBack.setVisibility(View.VISIBLE);
        tv_back.setVisibility(View.GONE);
        tv_header.setVisibility(View.GONE);
        showSearchMenu();
        setSearchBarMenuColor(colorBottomMenuNormal);

        switch (type)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
            case AppUtils.BMT_EATS:
                imgBack.setColorFilter(getResources().getColor(R.color.colorEats));
                tv_back.setTextColor(getResources().getColor(R.color.colorEats));
                setOptionFilterButton(option, getResources().getColor(R.color.colorEats), colorBottomMenuNormal);
                break;
            case AppUtils.BMT_LOCAL:
                imgBack.setColorFilter(getResources().getColor(R.color.colorLocal));
                tv_back.setTextColor(getResources().getColor(R.color.colorLocal));
                setOptionFilterButton(option, getResources().getColor(R.color.colorLocal), colorBottomMenuNormal);
                break;
            case AppUtils.BMT_MALL:
                imgBack.setColorFilter(getResources().getColor(R.color.colorMall));
                tv_back.setTextColor(getResources().getColor(R.color.colorMall));
                setOptionFilterButton(option, getResources().getColor(R.color.colorMall), colorBottomMenuNormal);
                break;
            default:
                imgBack.setColorFilter(getResources().getColor(R.color.colorWhite));
                tv_back.setTextColor(getResources().getColor(R.color.colorWhite));
                btnSearchText.setTextColor(getResources().getColor(R.color.colorBottomMenuNormal));
                btnSearchLocation.setTextColor(getResources().getColor(R.color.colorBottomMenuNormal));
                btnSearchFilter.setTextColor(getResources().getColor(R.color.colorBottomMenuNormal));
                break;
        }

        calledHeaderMethod = 1;

        addToNavigateList();
    }

    private void setOptionFilterButton(int option, int selectedColor, int normalColor)
    {
        switch (option)
        {
            case AppUtils.OPTION_SEARCH:
                btnSearchText.setTextColor(selectedColor);
                btnSearchText.getIcon().setColorFilter(selectedColor, PorterDuff.Mode.SRC_ATOP);
                btnSearchLocation.setTextColor(normalColor);
                btnSearchLocation.getIcon().setColorFilter(normalColor, PorterDuff.Mode.SRC_ATOP);
                btnSearchFilter.setTextColor(normalColor);
                btnSearchFilter.getIcon().setColorFilter(normalColor, PorterDuff.Mode.SRC_ATOP);
                break;
            case AppUtils.OPTION_LOCATION:
                btnSearchText.setTextColor(normalColor);
                btnSearchText.getIcon().setColorFilter(normalColor, PorterDuff.Mode.SRC_ATOP);
                btnSearchLocation.setTextColor(selectedColor);
                btnSearchLocation.getIcon().setColorFilter(selectedColor, PorterDuff.Mode.SRC_ATOP);
                btnSearchFilter.setTextColor(normalColor);
                btnSearchFilter.getIcon().setColorFilter(normalColor, PorterDuff.Mode.SRC_ATOP);
                break;
            case AppUtils.OPTION_FILTER:
                btnSearchText.setTextColor(normalColor);
                btnSearchText.getIcon().setColorFilter(normalColor, PorterDuff.Mode.SRC_ATOP);
                btnSearchLocation.setTextColor(normalColor);
                btnSearchLocation.getIcon().setColorFilter(normalColor, PorterDuff.Mode.SRC_ATOP);
                btnSearchFilter.setTextColor(selectedColor);
                btnSearchFilter.getIcon().setColorFilter(selectedColor, PorterDuff.Mode.SRC_ATOP);
                break;
            default:
                btnSearchText.setTextColor(normalColor);
                btnSearchText.getIcon().setColorFilter(normalColor, PorterDuff.Mode.SRC_ATOP);
                btnSearchLocation.setTextColor(normalColor);
                btnSearchLocation.getIcon().setColorFilter(normalColor, PorterDuff.Mode.SRC_ATOP);
                btnSearchFilter.setTextColor(normalColor);
                btnSearchFilter.getIcon().setColorFilter(normalColor, PorterDuff.Mode.SRC_ATOP);
                break;
        }
    }

    public void setHeaderWithBackIconButton(int type)
    {
        include1.setVisibility(View.VISIBLE);

        previousType = currentType;
        currentType = type;

        previousTitle = currentTitle;
        previousOption = currentOption;

        previouslyCalledHeaderMethod = calledHeaderMethod;

        imgMenu.setVisibility(View.GONE);
        imgBack.setVisibility(View.VISIBLE);
        tv_back.setVisibility(View.VISIBLE);
        tv_header.setVisibility(View.GONE);
        hideSearchBarMenu();

        switch (type)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
            case AppUtils.BMT_EATS:
                imgBack.setColorFilter(getResources().getColor(R.color.colorEats));
                tv_back.setTextColor(getResources().getColor(R.color.colorEats));
                break;
            case AppUtils.BMT_LOCAL:
                imgBack.setColorFilter(getResources().getColor(R.color.colorLocal));
                tv_back.setTextColor(getResources().getColor(R.color.colorLocal));
                break;
            case AppUtils.BMT_MALL:
                imgBack.setColorFilter(getResources().getColor(R.color.colorMall));
                tv_back.setTextColor(getResources().getColor(R.color.colorMall));
                break;
            default:
                imgBack.setColorFilter(getResources().getColor(R.color.colorWhite));
                tv_back.setTextColor(getResources().getColor(R.color.colorWhite));
                break;

        }

        calledHeaderMethod = 2;

        addToNavigateList();

    }

    public void setHeaderWithBackIcon(String strTitle, int type)
    {
        include1.setVisibility(View.VISIBLE);

        currentTitle = tv_header.getText().toString();
        previousTitle = currentTitle;
        currentTitle = strTitle;

        previousType = currentType;
        currentType = type;

        previousOption = currentOption;

        previouslyCalledHeaderMethod = calledHeaderMethod;

        imgMenu.setVisibility(View.GONE);
        imgBack.setVisibility(View.VISIBLE);
        tv_back.setVisibility(View.GONE);
        tv_header.setVisibility(View.VISIBLE);
        hideSearchBarMenu();
        tv_header.setText(strTitle);

        switch (type)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
            case AppUtils.BMT_EATS:
                imgBack.setColorFilter(getResources().getColor(R.color.colorEats));
                tv_header.setTextColor(getResources().getColor(R.color.colorEats));
                break;
            case AppUtils.BMT_LOCAL:
                imgBack.setColorFilter(getResources().getColor(R.color.colorLocal));
                tv_header.setTextColor(getResources().getColor(R.color.colorLocal));
                break;
            case AppUtils.BMT_MALL:
                imgBack.setColorFilter(getResources().getColor(R.color.colorMall));
                tv_header.setTextColor(getResources().getColor(R.color.colorMall));
                break;
            default:
                imgBack.setColorFilter(getResources().getColor(R.color.colorWhite));
                tv_header.setTextColor(getResources().getColor(R.color.colorWhite));
                break;

        }

        calledHeaderMethod = 3;

        addToNavigateList();

    }

    public void setOnlyTitleMenuBarHeader(String title, int type)
    {
        include1.setVisibility(View.VISIBLE);

        currentTitle = tv_header.getText().toString();
        previousTitle = currentTitle;
        currentTitle = title;

        previousType = currentType;
        currentType = type;

        previousOption = currentOption;

        previouslyCalledHeaderMethod = calledHeaderMethod;

        imgMenu.setVisibility(View.VISIBLE);
        imgBack.setVisibility(View.GONE);
        tv_back.setVisibility(View.GONE);
        tv_header.setVisibility(View.VISIBLE);
        hideSearchBarMenu();
        tv_header.setText(title);

        switch (type)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
            case AppUtils.BMT_EATS:
                imgMenu.setColorFilter(getResources().getColor(R.color.colorEats));
                tv_header.setTextColor(getResources().getColor(R.color.colorEats));
                break;
            case AppUtils.BMT_LOCAL:
                imgMenu.setColorFilter(getResources().getColor(R.color.colorLocal));
                tv_header.setTextColor(getResources().getColor(R.color.colorLocal));
                break;
            case AppUtils.BMT_MALL:
                imgMenu.setColorFilter(getResources().getColor(R.color.colorMall));
                tv_header.setTextColor(getResources().getColor(R.color.colorMall));
                break;
            default:
                tv_header.setTextColor(getResources().getColor(R.color.colorWhite));
                break;
        }

        calledHeaderMethod = 4;

        addToNavigateList();

    }

    public void setOnlyTitleHeader(String title, int type)
    {
        include1.setVisibility(View.VISIBLE);

        currentTitle = tv_header.getText().toString();
        previousTitle = currentTitle;
        currentTitle = title;

        previousType = currentType;
        currentType = type;

        previousOption = currentOption;

        previouslyCalledHeaderMethod = calledHeaderMethod;

        imgMenu.setVisibility(View.GONE);
        imgBack.setVisibility(View.GONE);
        tv_back.setVisibility(View.GONE);
        tv_header.setVisibility(View.VISIBLE);
        hideSearchBarMenu();
        tv_header.setText(title);
        switch (type)
        {
            case AppUtils.BMT_TRACK_AND_TRACE:
            case AppUtils.BMT_EATS:
                tv_header.setTextColor(getResources().getColor(R.color.colorEats));
                break;
            case AppUtils.BMT_LOCAL:
                tv_header.setTextColor(getResources().getColor(R.color.colorLocal));
                break;
            case AppUtils.BMT_MALL:
                tv_header.setTextColor(getResources().getColor(R.color.colorMall));
                break;
            default:
                tv_header.setTextColor(getResources().getColor(R.color.colorWhite));
                break;

        }

        calledHeaderMethod = 5;

        addToNavigateList();
    }

    private void showSearchMenu()
    {
        cnlSearchToolbar.setVisibility(View.VISIBLE);
        cnlSearchToolbar.invalidate();
    }

    private void setSearchBarMenuColor(int color)
    {
        btnSearchText.getIcon().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        btnSearchText.setTextColor(color);
        btnSearchLocation.getIcon().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        btnSearchLocation.setTextColor(color);
        btnSearchFilter.getIcon().setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        btnSearchFilter.setTextColor(color);
    }

    private void hideSearchBarMenu()
    {
        btnSearchText.setVisibility(View.GONE);
        btnSearchLocation.setVisibility(View.GONE);
        btnSearchFilter.setVisibility(View.GONE);

    }

    private void bouldObjectForHandlingAPI()
    {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);
    }

    private void applyHeader(NavigateDetails navigateDetails)
    {
        addToList = false;

        if (!navigateDetails.isFragmentToolbar())
        {
            include1.setVisibility(View.VISIBLE);
            switch (navigateDetails.getCalledMethod())
            {
                case 0:
                    setHeader(navigateDetails.getTitle(), navigateDetails.getType());
                    break;
                case 1:
                    setHeaderWithBackFilterButton(navigateDetails.getType(), navigateDetails.getOption());
                    break;
                case 2:
                    setHeaderWithBackIconButton(navigateDetails.getType());
                    break;
                case 3:
                    setHeaderWithBackIcon(navigateDetails.getTitle(), navigateDetails.getType());
                    break;
                case 4:
                    setOnlyTitleMenuBarHeader(navigateDetails.getTitle(), navigateDetails.getType());
                    break;
                case 5:
                    setOnlyTitleHeader(navigateDetails.getTitle(), navigateDetails.getType());
                    break;
            }
        }
        else include1.setVisibility(View.GONE);

    }

    private void addToNavigateList()
    {
        if (clearList)
        {
            navigateList.clear();
            navigateList = new ArrayList<>();
        }

        if (addToList)
        {
            NavigateDetails navigateDetails = new NavigateDetails();
            navigateDetails.setOption(currentOption);
            navigateDetails.setTitle(currentTitle);
            navigateDetails.setType(currentType);
            navigateDetails.setCalledMethod(calledHeaderMethod);
            navigateDetails.setFragmentToolbar(false);
            navigateList.add(navigateDetails);
            /*Log.d(TAG, "Stack Count ==> " + getSupportFragmentManager().getBackStackEntryCount());
            for(NavigateDetails itemNavDetails  : navigateList)
            {
                Log.d(TAG, "addToNavigateList: itemNavDetails-CalledMethod ==> " + itemNavDetails.getCalledMethod());
                Log.d(TAG, "addToNavigateList: itemNavDetails-Title ==> " + itemNavDetails.getTitle());
            }*/
        }
    }


    private void initializer()
    {

        imgMenu = findViewById(R.id.imgMenu);
        imgBack = findViewById(R.id.imgBack);
        tv_back = findViewById(R.id.tvBack);
        tv_header = findViewById(R.id.tvCenterTitle);
        cnlSearchToolbar = findViewById(R.id.cnlSearchToolbar);

        include1 = findViewById(R.id.include1);

        btnSearchText = findViewById(R.id.btnSearchText);
        btnSearchLocation = findViewById(R.id.btnSearchLocation);
        btnSearchFilter = findViewById(R.id.btnSearchFilter);

        btnSearchText.setOnClickListener(_OnClickListener);
        btnSearchLocation.setOnClickListener(_OnClickListener);
        btnSearchFilter.setOnClickListener(_OnClickListener);

    }

    View.OnClickListener _OnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            onButtonClick(view);
        }
    };

    /**
     * Hide the keyboard.
     */
    private void hideKeyboard()
    {
        try
        {
            InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
            if (imm != null && getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null)
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onActivityResult(int, int, android.content.Intent)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        System.out.println("requestCode  " + requestCode + " resultCode " + resultCode);
        super.onActivityResult(requestCode, resultCode, intent);
        if (mFragmentManager.findFragmentById(R.id.flMain) != null)
        {
            (mFragmentManager.findFragmentById(R.id.flMain)).onActivityResult(requestCode, resultCode, intent);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        //No call for super(). Bug on API Level > 11.
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // If DashboardRequest is cancelled, the result arrays are empty.
        if (mFragmentManager.findFragmentById(R.id.flMain) != null)
        {
            (mFragmentManager.findFragmentById(R.id.flMain)).onRequestPermissionsResult(requestCode, permissions,
                    grantResults);
        }
    }

    public void onButtonClick(View view)
    {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        switch (view.getId())
        {
            case R.id.imgBack:
            case R.id.tvBack:
                 onBackPressed();
                 break;
            case R.id.imgMenu:
                 break;
        }
        if (mFragmentManager.findFragmentById(R.id.flMain) != null)
        {
            //((BaseFragment) mFragmentManager.findFragmentById(R.id.fl_main)).onButtonClick(view);
        }
    }

    /* (non-Javadoc)
     * @see android.support.v4.app.FragmentActivity#onBackPressed()
     */
    @Override
    public void onBackPressed()
    {
        if (mProgressDialog != null && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }
        else
        {
            if (MyApplication.isBackPressedEnabled)
            {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                {
                    getSupportFragmentManager().popBackStack();
                }
                else
                {
                    this.finish();
                }
            }
        }


    }

    /**
     * Adds the fragment.
     *
     * @param mFragment        the m fragment
     * @param isAddToBackStack the is add to back stack
     * @param tag              the tag
     */
    public void addFragment(Fragment mFragment, boolean isAddToBackStack, String tag, boolean isRight)
    {

        //setLightStatusBar(fl_main,this);
        mFragmentTransaction = mFragmentManager.beginTransaction();
        if (isAddToBackStack)
            mFragmentTransaction.addToBackStack(tag);
        //mFragmentTransaction.add(R.id.fl_container, mFragment, tag);
        Log.i("Add Fragment", tag);
        /*if (isRight)
        {
            mFragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
        }
        else
        {
            mFragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.slide_in_right, R.anim.slide_out_left);
        }*/
        if (isAddToBackStack) mFragmentTransaction.add(R.id.flMain, mFragment, tag);
        else mFragmentTransaction.replace(R.id.flMain, mFragment, tag);
        mFragmentTransaction.commitAllowingStateLoss();
        if (isHomeFragment(mFragment)) clearList = true;
        else clearList = false;
        addToList = true;
    }

    public void addFragment_Full_screen(Fragment mFragment, boolean isAddToBackStack, String tag)
    {
        // bottomNavigation.setVisibility(View.GONE);

        mFragmentTransaction = mFragmentManager.beginTransaction();
        if (isAddToBackStack)
            mFragmentTransaction.addToBackStack(tag);
        //mFragmentTransaction.add(R.id.fl_container, mFragment, tag);
        Log.i("Add Fragment", tag);
        mFragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left,
                R.anim.slide_out_right);
        mFragmentTransaction.replace(R.id.flMain, mFragment, tag);
        mFragmentTransaction.commitAllowingStateLoss();
    }

    /**
     * Replace fragment.
     *
     * @param mFragment        the m fragment
     * @param isAddToBackStack the is add to back stack
     * @param tag              the tag
     */
    public void replaceFragment(Fragment mFragment, boolean isAddToBackStack, String tag)
    {
        mFragmentTransaction = mFragmentManager.beginTransaction();
        //mFragmentTransaction.replace(R.id.fl_container, mFragment, tag);
        Log.i("Replace Fragment", tag);
        if (isAddToBackStack)
            mFragmentTransaction.addToBackStack(tag);
        mFragmentTransaction.replace(R.id.flMain, mFragment, tag);
        mFragmentTransaction.commit();
    }

    /**
     * Removes the fragment.
     */
    public void removeFragment()
    {
        mFragmentManager.popBackStack();
    }

    /**
     * Removes the all fragment.
     */
    public void removeAllFragment()
    {
        mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    public boolean isHomeFragment(Fragment fragment)
    {
        if (fragment instanceof VisitFragment || fragment instanceof HomeDashboardFragment || fragment instanceof MallDashboardFragment)
        {
            return true;
        }
        else return false;
    }

    /**
     * Sets the activity name.
     *
     * @param activityName the new activity name
     */
    public void setActivityName(String activityName)
    {
        this.activityName = activityName;
    }

    /**
     * Begin transit.
     */
    public void beginTransit()
    {
        mFragmentTransaction = mFragmentManager.beginTransaction();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item)
    {
        // Handle bottomNavigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home)
        {

        }
        else if (id == R.id.nav_scanpay)
        {
            Bundle bundle = new Bundle();

            if (fragment_no == 1)
                bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_EATS);
            else if (fragment_no == 2)
                bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_LOCAL);
            else if (fragment_no == 3)
                bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_MALL);
            ScanPayFragment scanPayFragment = ScanPayFragment.newInstance();
            scanPayFragment.setArguments(bundle);
            addFragment(scanPayFragment, true, ScanPayFragment.class.getName(), fragment_no < 1);
        }
        else if (id == R.id.nav_sale)
        {

        }
        else if (id == R.id.nav_account)
        {
            if (fragment_no == 1)
                addFragment(AccountFragment.newInstance(), true, AccountFragment.class.getName(), fragment_no < 1);
            else if (fragment_no == 2)
                addFragment(AccountFragment.newInstance(), true, AccountFragment.class.getName(), fragment_no < 1);
            else if (fragment_no == 3)
                addFragment(AccountFragment.newInstance(), true, AccountFragment.class.getName(), fragment_no < 1);
        }
        else if (id == R.id.nav_order)
        {
            switch (fragment_no)
            {
                case 1:
                     addFragment(MyOrdersListFragment.newInstance(AppUtils.BMT_EATS), true, MyOrdersListFragment.class.getName(), fragment_no < 1);
                     break;
                case 2:
                     addFragment(MyOrdersListFragment.newInstance(AppUtils.BMT_LOCAL), true, MyOrdersListFragment.class.getName(), fragment_no < 1);
                     break;
            }
        }
        else if (id == R.id.nav_table_booking)
        {
           gotoTableBookingListFragment(0);
        }
        else if (id == R.id.nav_my_appointments)
        {
            gotoRequestAppointmentListFragment(0);
        }
        else if (id == R.id.nav_cart)
        {

        }
        else if (id == R.id.nav_voucher)
        {
            if (fragment_no == 3)
                addFragment(MallMyVouchersFragment.newInstance(), true, MallMyVouchersFragment.class.getName(), fragment_no < 1);
        }
        else if (id == R.id.nav_transaction)
        {
            if (fragment_no == 1)
                addFragment(TransactionListFragment.newInstance(), true, TransactionListFragment.class.getName(), fragment_no < 1);
            else if (fragment_no == 2)
                addFragment(TransactionListFragment.newInstance(), true, TransactionListFragment.class.getName(), fragment_no < 1);
            else if (fragment_no == 3)
                addFragment(TransactionListFragment.newInstance(), true, MallMyTransactionListFragment.class.getName(), fragment_no < 1);
        }
        else if (id == R.id.nav_referearn)
        {
            if (fragment_no == 1)
                addFragment(ReferFriendsFragment.newInstance(), true, ReferFriendsFragment.class.getName(), fragment_no < 1);
            else if (fragment_no == 2)
                addFragment(ReferFriendsFragment.newInstance(), true, ReferFriendsFragment.class.getName(), fragment_no < 1);
            else if (fragment_no == 3)
                addFragment(ReferFriendsFragment.newInstance(), true, ReferFriendsFragment.class.getName(), fragment_no < 1);
        }
        else if (id == R.id.nav_faq)
        {
            if (fragment_no == 1)
            {
                Intent intent = new Intent(MainActivity.this, EatsWebviewActivity.class);
                intent.putExtra("getTitle", "FAQ");
                intent.putExtra("setURL", "https://www.localvalu.com/Eats/page/index/faq");
                startActivity(intent);
            }
            else if (fragment_no == 2)
            {
                Intent intent = new Intent(MainActivity.this, DirectoryWebviewActivity.class);
                intent.putExtra("getTitle", "FAQ");
                intent.putExtra("setURL", "https://www.localvalu.com/Eats/page/index/faq");
                startActivity(intent);
            }
            else if (fragment_no == 3)
            {
                Intent intent = new Intent(MainActivity.this, MallWebviewActivity.class);
                intent.putExtra("getTitle", "FAQ");
                intent.putExtra("setURL", "https://www.localvalu.com/Eats/page/index/faq");
                startActivity(intent);
            }
        }
        else if (id == R.id.nav_notification)
        {
            if (fragment_no == 1)
                addFragment(EatsNotifcationFragment.newInstance(), true, EatsNotifcationFragment.class.getName(), fragment_no < 1);
            else if (fragment_no == 2)
                addFragment(DirectoryNotifcationFragment.newInstance(), true, DirectoryNotifcationFragment.class.getName(), fragment_no < 1);
            else if (fragment_no == 3)
                addFragment(MallNotifcationFragment.newInstance(), true, MallNotifcationFragment.class.getName(), fragment_no < 1);
        }
        else if (id == R.id.nav_transfertokens)
        {
            TransferTokensFragment transferTokensFragment = new TransferTokensFragment();
            addFragment(transferTokensFragment, true, TransferTokensFragment.class.getName(),fragment_no<1);
        }
        else if (id == R.id.nav_insert_litter)
        {
            gotoInsertLitterFragment();
        }
        else if (id == R.id.nav_logout)
        {
            PreferenceUtility.saveObjectInAppPreference(MainActivity.this, null, PreferenceUtility.APP_PREFERENCE_NAME);
            PreferenceUtility.saveBooleanInPreference(MainActivity.this, PreferenceUtility.TERMS_AND_CONDITION, true);
            ActivityController.startNextActivity(this, LoginActivity.class, true);
        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void gotoTableBookingListFragment(int status)
    {
        Bundle bundle = new Bundle();
        if (fragment_no == 1)
            bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_EATS);
        else if (fragment_no == 2)
            bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_LOCAL);
        else if (fragment_no == 3)
            bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_MALL);
        bundle.putInt(AppUtils.BUNDLE_TABLE_BOOKING_LIST_STATUS,status);
        TableBookingFragment tableBookingFragment = TableBookingFragment.newInstance();
        tableBookingFragment.setArguments(bundle);
        addFragment(tableBookingFragment, true, TableBookingFragment.class.getName(), fragment_no < 1);
    }

    private void gotoRequestAppointmentListFragment(int status)
    {
        Bundle bundle = new Bundle();

        if (fragment_no == 1)
            bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_EATS);
        else if (fragment_no == 2)
            bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_LOCAL);
        else if (fragment_no == 3)
            bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_MALL);
        bundle.putInt(AppUtils.BUNDLE_REQUEST_APPOINTMENT_LIST_STATUS,status);
        RequestAppointmentMainFragment requestAppointmentMainFragment = RequestAppointmentMainFragment.newInstance();
        requestAppointmentMainFragment.setArguments(bundle);
        addFragment(requestAppointmentMainFragment, true, RequestAppointmentMainFragment.class.getName(), fragment_no < 1);
    }

    private void gotoMyReviewsFragment(String businessId)
    {
        Bundle bundle = new Bundle();

        if (fragment_no == 1)
            bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_EATS);
        else if (fragment_no == 2)
            bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_LOCAL);
        else if (fragment_no == 3)
            bundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE, AppUtils.BMT_MALL);
        ReviewsListFragment reviewsListFragment = ReviewsListFragment.newInstance();
        bundle.putString(AppUtils.BUNDLE_BUSINESS_ID,businessId);
        reviewsListFragment.setArguments(bundle);
        addFragment(reviewsListFragment, true, ReviewsListFragment.class.getName(), fragment_no < 1);
    }

    private void gotoInsertLitterFragment()
    {
        Bundle bundle = new Bundle();
        fragment_tag = TestInsertLitterFragment.class.getName();
        InsertLitterFragment insertLitterFragment = new InsertLitterFragment();
        updateFragment(insertLitterFragment, true, InsertLitterFragment.class.getName(),true);
    }

    @Override
    public void hideBottomNavigation()
    {
        binding.bottomNavigation.setVisibility(View.GONE);
    }

    @Override
    public void showBottomNavigation()
    {
        binding.bottomNavigation.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackButtonPressed()
    {
        onBackPressed();
    }

    @Override
    public void onMenuClicked()
    {
        showLeftMenu();
    }

    @Override
    public void updateFragment(Fragment mFragment, boolean isAddToBackStack, String tag, boolean isRight)
    {
        addFragment(mFragment, isAddToBackStack, tag, isRight);
    }
}
