package com.localvalu.base;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.localvalu.R;
import com.localvalu.common.review.ReviewsListFragment;


/**
 * The Class BaseFragment that extends Fragment class
 */
public class BaseFragment extends Fragment
{

    private static final String TAG = "BaseFragment";
    /**
     * CustomButton
     */
    /*private ImageView img_back;
    protected TextView tv_header;*/

    /**
     * View.
     */
    public View view;

    /**
     * The animate first listener.
     */
    @SuppressWarnings("unused")

    //private UserDto userDto;

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

       /* String languageToLoad = PreferenceUtility.getStringFromPreference(getActivity(), "prefLang"); // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.setLocale(locale);
        getActivity().getBaseContext().getResources().updateConfiguration(config, getActivity().getBaseContext().getResources().getDisplayMetrics()); */

    }

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onActivityResult(int, int, android.content.Intent)
     */
    public void onActivityResult(int requestCode, int resultCode, Intent intent)
    {

    }

    public void onNewIntent(Intent intent)
    {

    }

    public void locationEnabled(double latitude,double longitude)
    {
        //Log.d(TAG, "locationEnabled: ");
    }

    /**
     * Sliding menu toggle.
     */
    public void slidingMenuToggle()
    {
        System.out.println("slidingMenuToggle");
    }

    /**
     * onCreateView
     *
     * @param inflater           the LayoutInflater
     * @param container          the ViewGroup
     * @param savedInstanceState the Bundle
     * @return view the View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        MyApplication.isBackPressedEnabled = true;
        //userDto = (UserDto) PreferenceUtility.getObjectInAppPreference(getActivity(), PreferenceUtility.APP_PREFERENCE_NAME);
        ((MainActivity) getActivity()).setActivityName("");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * Call parent method.
     */
    public void callParentMethod()
    {
        getActivity().onBackPressed();
    }

    /**
     * Initialize the views
     */
    private void initializer()
    {
        /*img_back = view.findViewById(R.id.img_back);
        tv_header = view.findViewById(R.id.tv_header);*/
    }


    /*protected void setHeader(String title) {
        initializer();
        img_back.setVisibility(View.GONE);
        tv_header.setVisibility(View.VISIBLE);
        tv_header.setText(title);
    }

    protected void setInnerHeader(String title) {
        initializer();
        img_back.setVisibility(View.VISIBLE);
        tv_header.setText(title);
        tv_header.setVisibility(View.VISIBLE);
        img_back.setClickable(true);
    }*/

    /**
     * Sets the header with back button.
     *
     * @param headerText the header String
     */
    protected void setHeaderWithBackButton(String headerText, String buttonText)
    {
        initializer();

    }

    /**
     * On button click.
     *
     * @param view the View
     */

    /**
     * Call base methods.
     */
    public void callBaseMethods()
    {
        ((MainActivity) getActivity()).beginTransit();
        ((MainActivity) getActivity()).removeFragment();
    }

    /**
     * Show the keyboard.
     *
     * @param et the EditText
     */
    public void showKeyboard(EditText et)
    {
        try
        {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
            imm.showSoftInput(et, 0);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Hide the keyboard.
     */
    public void hideKeyboard()
    {
        try
        {
            View currentView = requireActivity().getCurrentFocus();
            if(currentView!=null)
            {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(currentView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * On resume fragment.
     *
     * @param isBackPressed the is back pressed
     */
    public void onResumeFragment(boolean isBackPressed)
    {

    }

    /**
     * On button click.
     *
     * @param view the View
     */
    public void onButtonClick(View view)
    {

    }

    /**
     * On open the sliding menu.
     */
    public void onOpen()
    {
        enableDisableViewGroup((ViewGroup) view.findViewById(R.id.cnst_root), false);
    }

    /**
     * On close the sliding menu.
     */
    public void onClose()
    {
        enableDisableViewGroup((ViewGroup) view.findViewById(R.id.cnst_root), true);
    }


    /**
     * Enables/Disables all child views in a view group.
     *
     * @param viewGroup the view group
     * @param enabled   <code>true</code> to enable, <code>false</code> to disable
     *                  the views.
     */
    private void enableDisableViewGroup(ViewGroup viewGroup, boolean enabled)
    {
        if (view.findViewById(R.id.imgBack) != null)
            view.findViewById(R.id.imgBack).setEnabled(true);
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++)
        {
            View view = viewGroup.getChildAt(i);
            view.setEnabled(enabled);
            if (view instanceof ViewGroup)
            {
                enableDisableViewGroup((ViewGroup) view, enabled);
            }
        }
    }

    /**
     * On pause fragment.
     */
    public void onPauseFragment()
    {

    }

    /**
     * On back pressed.
     */
    public void onBackPressed()
    {

    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        Fragment fragment = requireActivity().getSupportFragmentManager().findFragmentById(R.id.flMain);
        if(fragment!=null)
        {
            if(fragment instanceof ReviewsListFragment)
            {
                ReviewsListFragment reviewsListFragment = (ReviewsListFragment) fragment;
                reviewsListFragment.showSnackbar();
            }
        }
    }
}
