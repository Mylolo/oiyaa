package com.localvalu.base

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.api.GoogleApiClient
import android.os.Bundle
import kotlin.jvm.Synchronized
import com.google.android.gms.common.api.ResolvableApiException
import android.content.IntentSender.SendIntentException
import android.content.Intent
import androidx.core.app.ActivityCompat
import android.content.pm.PackageManager
import android.location.Location
import android.os.Looper
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.localvalu.BuildConfig


open class BaseActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, LocationListener,
    ResultCallback<LocationSettingsResult>
{
    /**
     * Provides the entry point to Google Play services.
     */
    private var mGoogleApiClient: GoogleApiClient? = null

    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private var mLocationRequest: LocationRequest? = null

    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    private var mLocationSettingsRequest: LocationSettingsRequest? = null

    /**
     * Represents a geographical location.
     */
    private var mCurrentLocation: Location? = null

    /**
     * Tracks the status of the location updates DashboardRequest. Value changes when the user presses the
     * Start Updates and Stop Updates buttons.
     */
    protected var mRequestingLocationUpdates = false

    private var fusedLocationProviderClient: FusedLocationProviderClient? = null

    private val viewModel: LocationViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate: ")
        // Kick off the process of building the GoogleApiClient, LocationRequest, and
        // LocationSettingsRequest objects.
        subscribeObservers()
        buildGoogleApiClient()
        createLocationRequest()
        buildLocationSettingsRequest()
        checkLocationSettings()
    }

    private fun subscribeObservers()
    {
        viewModel.startRequestLocation.observe(this, Observer {
            if(it) startLocationUpdates()
        })
        viewModel.checkSettings.observe(this, Observer {
            if(it) checkLocationSettings()
        })
    }

    /**
     * Builds a GoogleApiClient. Uses the `#addApi` method to DashboardRequest the
     * LocationServices API.
     */
    @Synchronized protected fun buildGoogleApiClient()
    {
        Log.i(TAG, "Building GoogleApiClient")
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
    }

    /**
     * Sets up the location DashboardRequest. Android has two location DashboardRequest settings:
     * `ACCESS_COARSE_LOCATION` and `ACCESS_FINE_LOCATION`. These settings control
     * the accuracy of the current location. This sample uses ACCESS_FINE_LOCATION, as defined in
     * the AndroidManifest.xml.
     *
     *
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     *
     *
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    private fun createLocationRequest()
    {
        mLocationRequest = LocationRequest()

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest!!.interval = UPDATE_INTERVAL_IN_MILLISECONDS

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest!!.fastestInterval =
            FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest!!.priority =
            LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /**
     * Uses a [com.google.android.gms.location.LocationSettingsRequest.Builder] to build
     * a [com.google.android.gms.location.LocationSettingsRequest] that is used for checking
     * if a device has the needed location settings.
     */
    private fun buildLocationSettingsRequest()
    {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        mLocationSettingsRequest = builder.build()
    }

    /**
     * Check if the device's location settings are adequate for the app's needs using the
     * [com.google.android.gms.location.SettingsApi.checkLocationSettings] method, with the results provided through a `PendingResult`.
     */
    private fun checkLocationSettings()
    {
        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(mLocationSettingsRequest)
        task.addOnSuccessListener(this) { startLocationUpdates() }
        task.addOnFailureListener(this) { e ->
            if (e is ResolvableApiException)
            {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                // Show the dialog by calling startResolutionForResult(),
                // and check the result in onActivityResult().
                val resolvable = e
                e.startResolutionForResult(
                    this@BaseActivity,
                    REQUEST_CHECK_SETTINGS
                )
            }
        }
    }

    fun onLocationServiceEnabled(latitude: Double, longitude: Double)
    {
        //Log.d(TAG, "onLocationEnabled: Location service is enabled");
    }

    /**
     * The callback invoked when
     * [com.google.android.gms.location.SettingsApi.checkLocationSettings] is called. Examines the
     * [com.google.android.gms.location.LocationSettingsResult] object and determines if
     * location settings are adequate. If they are not, begins the process of presenting a location
     * settings dialog to the user.
     */
    override fun onResult(locationSettingsResult: LocationSettingsResult)
    {
        val status = locationSettingsResult.status
        when (status.statusCode)
        {
            LocationSettingsStatusCodes.SUCCESS                     ->
            {
                Log.i(TAG, "All location settings are satisfied.")
                startLocationUpdates()
            }
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED         ->
            {
                Log.i(
                    TAG, "Location settings are not satisfied. Show the user a dialog to" +
                            "upgrade location settings "
                )
                try
                {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(this@BaseActivity, REQUEST_CHECK_SETTINGS)
                }
                catch (e: SendIntentException)
                {
                    Log.i(TAG, "PendingIntent unable to execute DashboardRequest.")
                }
            }
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.i(
                TAG, "Location settings are inadequate, and cannot be fixed here. Dialog " +
                        "not created."
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode)
        {
            REQUEST_CHECK_SETTINGS -> when (resultCode)
            {
                RESULT_OK       ->
                {
                    Log.i(TAG, "User agreed to make required location settings changes.")
                    startLocationUpdates()
                }
                RESULT_CANCELED -> Log.i(
                    TAG,
                    "User chose not to make required location settings changes."
                )
            }
        }
    }

    /**
     * Requests location updates from the FusedLocationApi.
     */
    private fun startLocationUpdates()
    {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED)
        {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationProviderClient?.requestLocationUpdates(
            mLocationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }

    private var locationCallback: LocationCallback = object : LocationCallback()
    {
        override fun onLocationResult(locationResult: LocationResult)
        {
            super.onLocationResult(locationResult)
            if (locationResult.lastLocation != null)
            {
                val latitude = locationResult.locations[0].latitude
                val longitude = locationResult.locations[0].longitude
                onLocationServiceEnabled(latitude, longitude)
                mRequestingLocationUpdates = true
                if (BuildConfig.DEBUG)
                {
                    Log.d(TAG, "onLocationResult lat: " + locationResult.lastLocation.latitude);
                    Log.d(TAG, "onLocationResult long: " + locationResult.lastLocation.longitude);
                }
                viewModel?.location.value=locationResult.locations[0]
            }
            else
            {
                mRequestingLocationUpdates = false
                if (BuildConfig.DEBUG)
                {
                    //Log.d(TAG, "onLocationResult long: " + locationResult);
                }
            }
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    private fun stopLocationUpdates()
    {
        // It is a good practice to remove location requests when the activity is in a paused or
        // stopped state. Doing so helps battery performance and is especially
        // recommended in applications that DashboardRequest frequent location updates.
        if (fusedLocationProviderClient != null)
        {
            fusedLocationProviderClient!!.removeLocationUpdates(locationCallback)
        }
    }

    override fun onStart()
    {
        super.onStart()
        mGoogleApiClient!!.connect()
    }

    public override fun onResume()
    {
        super.onResume()
        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates if the user has requested them.
        if (mGoogleApiClient!!.isConnected && mRequestingLocationUpdates)
        {
            startLocationUpdates()
        }
    }

    override fun onPause()
    {
        super.onPause()
        // Stop location updates to save battery, but don't disconnect the GoogleApiClient object.
        if (mGoogleApiClient!!.isConnected)
        {
            stopLocationUpdates()
        }
    }

    override fun onStop()
    {
        super.onStop()
        mGoogleApiClient!!.disconnect()
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */
    override fun onConnected(connectionHint: Bundle?)
    {
        Log.i(TAG, "Connected to GoogleApiClient")

        // If the initial location was never previously requested, we use
        // FusedLocationApi.getLastLocation() to get it. If it was previously requested, we store
        // its value in the Bundle and check for it in onCreate(). We
        // do not DashboardRequest it again unless the user specifically requests location updates by pressing
        // the Start Updates button.
        //
        // Because we cache the value of the initial location in the Bundle, it means that if the
        // user launches the activity,
        // moves to a new location, and then changes the device orientation, the original location
        // is displayed as the activity is re-created.
    }

    /**
     * Callback that fires when the location changes.
     */
    override fun onLocationChanged(location: Location)
    {
        mCurrentLocation = location
    }

    override fun onConnectionSuspended(cause: Int)
    {
        Log.i(TAG, "Connection suspended")
    }

    override fun onConnectionFailed(result: ConnectionResult)
    {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.errorCode)
    }

    companion object
    {
        private const val TAG = "BaseActivity"

        /**
         * Constant used in the location settings dialog.
         */
        protected const val REQUEST_CHECK_SETTINGS = 0x1

        /**
         * The desired interval for location updates. Inexact. Updates may be more or less frequent.
         */
        const val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000

        /**
         * The fastest rate for active location updates. Exact. Updates will never be more frequent
         * than this value.
         */
        const val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2
    }
}