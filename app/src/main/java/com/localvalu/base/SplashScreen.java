package com.localvalu.base;

import static com.google.android.play.core.install.model.AppUpdateType.IMMEDIATE;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.WindowManager;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnFailureListener;
import com.google.android.play.core.tasks.Task;
import com.localvalu.BuildConfig;
import com.localvalu.R;
import com.localvalu.user.LoginActivity;
import com.localvalu.user.SplashInfoActivity;
import com.localvalu.user.dto.UserBasicInfo;
import com.utility.ActivityController;
import com.utility.AlertDialogCallBack;
import com.utility.AppUtils;
import com.utility.DialogUtility;
import com.utility.PreferenceUtility;

/**
 * Created by Developer on 09-02-2018.
 */

public class SplashScreen extends AppCompatActivity
{
    private static final String TAG = "SplashScreen";

    private int PLASH_TIME_OUT = 3000;
    private AppUpdateManager appUpdateManager;
    private Task<AppUpdateInfo> appUpdateInfoTask;
    private static int APP_UPDATE_REQUEST = 44;
    //private MotionLayout mlSplash;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //For Making Full Screen
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        appUpdateManager = AppUpdateManagerFactory.create(this.getApplicationContext());
        appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();
        checkUpdateAvailability();
        //mlSplash=findViewById(R.id.mlSplash);
        //mlSplash.transitionToEnd();
        /*if(!checkNotification())
        {
            moveToNext();
        }
        else startNextActivity(true);*/
    }

    public void moveToNext()
    {
        // Move on another activity after a time span
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                //displayFireBaseRegId();
                startNextActivity(false);
            }
        }, PLASH_TIME_OUT);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        System.out.println("onResume");
        /*performNotificationTask();
        appUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(
                        appUpdateInfo -> {
                            if (appUpdateInfo.updateAvailability()
                                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                                // If an in-app update is already running, resume the update.
                                try
                                {
                                    appUpdateManager.startUpdateFlowForResult(
                                            appUpdateInfo,
                                            IMMEDIATE,
                                            this,
                                            APP_UPDATE_REQUEST);
                                }
                                catch (Exception e)
                                {
                                    e.printStackTrace();
                                    moveToNext();
                                }
                            }
                        });*/
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
    }

    private void startNextActivity(boolean isFromNotification)
    {
        UserBasicInfo userBasicInfo = (UserBasicInfo) PreferenceUtility.getObjectInAppPreference(this,
                PreferenceUtility.APP_PREFERENCE_NAME);
        if (userBasicInfo == null)
        {
            ActivityController.startNextActivity(SplashScreen.this, LoginActivity.class, true);
        }
        else
        {
            if(isFromNotification)
            {
                Bundle bundle = getIntent().getExtras();
                ActivityController.startNextActivity(SplashScreen.this, MainActivity.class, bundle,true);
            }
            else
            {
                ActivityController.startNextActivity(SplashScreen.this, SplashInfoActivity.class, true);
            }
        }
    }

    public boolean checkNotification()
    {
        Log.d(TAG, "checkNotification: ");
        try
        {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null)
            {
                boolean isFromNotification = bundle.getBoolean(AppUtils.BUNDLE_IS_FROM_NOTIFICATION_PAGE);
                if(!isFromNotification) return false;
                else return true;
            }
            return false;
        }

        catch (Exception e)
        {
            return false;
        }
    }

    private void checkUpdateAvailability()
    {
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"checkUpdateAvailability");
        }

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    // This example applies an immediate update. To apply a flexible update
                    // instead, pass in AppUpdateType.FLEXIBLE
                    && appUpdateInfo.isUpdateTypeAllowed(IMMEDIATE))
            {
                // Request the update.
                try
                {
                    if(BuildConfig.DEBUG)
                    {
                        Log.d(TAG,"checkUpdateAvailability - Update available,requested for update");
                    }

                    appUpdateManager.startUpdateFlowForResult(
                            // Pass the intent that is returned by 'getAppUpdateInfo()'.
                            appUpdateInfo,
                            // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                            IMMEDIATE,
                            // The current activity making the update request.
                            this,
                            // Include a request code to later monitor this update request.
                            APP_UPDATE_REQUEST);
                }
                catch (Exception e)
                {
                    if(BuildConfig.DEBUG)
                    {
                        e.printStackTrace();
                    }
                    moveToNext();
                }
            }
            else
            {
                if(BuildConfig.DEBUG)
                {
                    Log.d(TAG,"checkUpdateAvailability - Update Not Available");
                }

                moveToNext();
            }
        }).addOnFailureListener(new OnFailureListener()
        {
            @Override
            public void onFailure(Exception e)
            {
                if(BuildConfig.DEBUG)
                {
                    e.printStackTrace();
                }
                moveToNext();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"onActivityResult-" + requestCode);
            Log.d(TAG,"onActivityResult-" + resultCode);
            Log.d(TAG,"onActivityResult-" + data);
        }

        if (requestCode == APP_UPDATE_REQUEST)
        {
            if (resultCode != RESULT_OK)
            {
                Log.d(TAG, "Update flow failed! Result code: " + resultCode);
                // If the update is cancelled or fails,
                // you can request to start the update again.
                showMandateUpdate();
            }
            else
            {
                moveToNext();
            }
        }
        else
        {
            moveToNext();
        }
    }

    private void showMandateUpdate()
    {
        DialogUtility.showMessageWithOkWithCallback(getString(R.string.dialog_title),
                getString(R.string.lbl_update_available),
                SplashScreen.this,new AlertDialogCallBack()
                {
                    @Override
                    public void onSubmit()
                    {
                        redirectToPlayStore();
                    }

                    @Override
                    public void onCancel()
                    {

                    }
                },false);
    }

    private void redirectToPlayStore()
    {
        final String appPackageName = getPackageName(); // package name of the app
        try
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        }
        catch (android.content.ActivityNotFoundException anfe)
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

}
