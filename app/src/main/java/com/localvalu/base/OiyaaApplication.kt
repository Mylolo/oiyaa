package com.localvalu.base

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class OiyaaApplication : Application()
{
}