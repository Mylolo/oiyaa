package com.localvalu.base

import android.content.Context
import androidx.room.Room
import com.localvalu.BuildConfig
import com.localvalu.db.AppDatabase
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class LocalDataSource
{
    fun buildLocalSource(context: Context) : AppDatabase
    {
        return Room.databaseBuilder(context, AppDatabase::class.java,"OiyaaAppDatabase").build()
    }
}