package com.localvalu.base;

public class ProgressModel
{
    private boolean show;

    public boolean isShow()
    {
        return show;
    }

    public void setShow(boolean show)
    {
        this.show = show;
    }

}
