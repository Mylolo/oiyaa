package com.localvalu.base;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import com.utility.PreferenceUtility;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;


public class MyApplication extends Application
{


    public static boolean isBackPressedEnabled = true;

    public static void checkPreviousAndCurrentUser(Context mContext, String previousUserID, String currentUserID)
    {
        if (!previousUserID.equals(currentUserID))
        {
            PreferenceUtility.saveObjectInAppPreference(mContext, null, PreferenceUtility.APP_PREFERENCE_NAME);
        }
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
    }

    private void sendLogFile(Throwable ex)
    {
        String fullName = getFile(ex);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"arpan.sarkar@vaptech.in"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "MyApp log file");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + fullName));
        intent.putExtra(Intent.EXTRA_TEXT, "Log file attached."); // do this so some login_email clients don't complain about empty body.
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        System.exit(1);
    }

    private String getFile(Throwable ex)
    {
        File nFile = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "LogFile.txt");

        try
        {
            if (nFile.exists())
                nFile.delete();
            nFile.createNewFile();
            PrintWriter mFileWriter = new PrintWriter(nFile);
            ex.printStackTrace(mFileWriter);
            mFileWriter.flush();
            mFileWriter.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return nFile.getAbsolutePath();
    }
}