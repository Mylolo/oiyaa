package com.localvalu.base;

import androidx.fragment.app.Fragment;

public interface HostActivityListener
{
    void onBackButtonPressed();
    void onMenuClicked();
    void updateFragment(Fragment mFragment, boolean isAddToBackStack, String tag, boolean isRight);
    void hideBottomNavigation();
    void showBottomNavigation();
}
