package com.localvalu.base;

public class NavigateDetails
{
    private String title;
    private int type;
    private int option;
    private int calledMethod;
    private boolean fragmentToolbar;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public int getOption()
    {
        return option;
    }

    public void setOption(int option)
    {
        this.option = option;
    }

    public int getCalledMethod()
    {
        return calledMethod;
    }

    public void setCalledMethod(int calledMethod)
    {
        this.calledMethod = calledMethod;
    }

    public boolean isFragmentToolbar()
    {
        return fragmentToolbar;
    }

    public void setFragmentToolbar(boolean fragmentToolbar)
    {
        this.fragmentToolbar = fragmentToolbar;
    }
}
