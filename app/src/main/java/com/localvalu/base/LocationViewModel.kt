package com.localvalu.base

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class LocationViewModel:ViewModel()
{
    val location:MutableLiveData<Location> = MutableLiveData()

    private val _startRequestLocation:MutableLiveData<Boolean> = MutableLiveData()
    val startRequestLocation:LiveData<Boolean>
         get() = _startRequestLocation

    private val _checkSettings:MutableLiveData<Boolean> = MutableLiveData()
    val checkSettings:LiveData<Boolean>
        get() = _checkSettings

    fun startRequest()
    {
        this._startRequestLocation.value = true
    }

    fun stopRequest()
    {
        this._startRequestLocation.value = true
    }

    fun checkSettings()
    {
        this._checkSettings.value=true
    }

}