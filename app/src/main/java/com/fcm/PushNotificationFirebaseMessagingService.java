package com.fcm;

import static com.utility.AppUtils.NOTIFICATION_ORDER_PLACED;
import static com.utility.AppUtils.NOTIFICATION_REQUEST_APPOINTMENT;
import static com.utility.AppUtils.NOTIFICATION_REVIEWED_BUSINESS;
import static com.utility.AppUtils.NOTIFICATION_TABLE_BOOKED;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.localvalu.BuildConfig;
import com.localvalu.base.MainActivity;
import com.localvalu.eats.payment.EatsSuccessPopupForOrderSubmit;
import com.utility.AppUtils;
import com.utility.Config;
import com.utility.NotificationUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class PushNotificationFirebaseMessagingService extends FirebaseMessagingService
{

    private static final String TAG = PushNotificationFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;


    @Override
    public void onNewToken(String refreshedToken)
    {
        super.onNewToken(refreshedToken);
        storeRegIdInPref(refreshedToken);
        //Applozic.getInstance(this).setDeviceRegistrationId(refreshedToken);

        // sending reg id to your server
        //sendRegistrationToServer(refreshedToken); -- Need to remove this comment to work FCM

        // Notify UI that registration has completed, so the progress indicator can be hidden. -- Need to remove this comment to work FCM
        /*Intent registrationComplete = new Intent(Config.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);*/
    }

    private void storeRegIdInPref(String token)
    {
        System.out.println("storeRegIdInPref : " + token);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.apply();
    }

    private void sendRegistrationToServer(final String token)
    {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage)
    {
        Log.d(TAG,"onMessageReceived");
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG, "Received onMessageReceived()" + remoteMessage);
            Log.d(TAG, "Bundle data: " + remoteMessage.getData());
            Log.d(TAG, "From: " + remoteMessage.getFrom());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null)
        {
            if(BuildConfig.DEBUG)
            {
                Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            }
            //handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0)
        {
            if(BuildConfig.DEBUG)
            {
                Log.d(TAG, "Data Payload: " + remoteMessage.getData().toString());
            }
            try
            {
                JSONObject json = new JSONObject(remoteMessage.getData());
                handleDataMessage(json);
            }
            catch (Exception e)
            {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message)
    {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext()))
        {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            //notificationUtils.playNotificationSound();
            //        } else {
            // If the app is in background, fireBase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) throws JSONException
    {
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"handleDataMessage");
            Log.i(TAG, "notificationData : " + json.toString());
        }
        try
        {
            String title = json.optString("title");
            String message = json.optString("body");
            String imageUrl = json.optString("image");
            String timestamp = json.optString("timestamp");
            String strAction = json.optString("action");
            int action=-1;
            if (strAction!=null)
            {
                if(!strAction.equals(""))
                {
                    action = Integer.parseInt(json.optString("action"));
                }
            }

            if(BuildConfig.DEBUG)
            {
                Log.e(TAG, "title: " + title);
                Log.e(TAG, "message: " + message);
                Log.e(TAG, "imageUrl: " + imageUrl);
                Log.e(TAG, "timestamp: " + timestamp);
            }

            Bundle newBundle = new Bundle();

            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            switch (action)
            {
                case NOTIFICATION_ORDER_PLACED:
                     resultIntent = new Intent(getApplicationContext(),EatsSuccessPopupForOrderSubmit.class);
                     newBundle.putString(AppUtils.BUNDLE_OIYAA_BONUS, json.getString("oiyaaBonus"));
                     newBundle.putString(AppUtils.BUNDLE_BONUS_TOKENS, json.getString("bonusTokens"));
                     newBundle.putString(AppUtils.BUNDLE_NEW_TOKEN_BALANCE, json.getString("newTokenBalance"));
                     String orderFrom = json.optString("orderFrom");
                     if(orderFrom.equalsIgnoreCase("Eats"))
                     {
                         newBundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,AppUtils.BMT_EATS);
                     }
                     else
                     {
                         newBundle.putInt(AppUtils.BUNDLE_NAVIGATION_TYPE,AppUtils.BMT_LOCAL);
                     }
                     resultIntent.putExtras(newBundle);
                     break;
                case NOTIFICATION_TABLE_BOOKED:
                     break;
                case NOTIFICATION_REQUEST_APPOINTMENT:
                     break;
                case NOTIFICATION_REVIEWED_BUSINESS:
                     break;
            }
            resultIntent.putExtra(AppUtils.BUNDLE_NOTIFICATION_ACTION, action);
            resultIntent.putExtra(AppUtils.BUNDLE_IS_FROM_NOTIFICATION_PAGE, true);
            resultIntent.putExtra(AppUtils.BUNDLE_NOTIFICATION_DATA,json.toString());
            showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent);
        }
        catch (Exception e)
        {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent)
    {
        if(BuildConfig.DEBUG)
        {
            Log.d(TAG,"showNotificationMessage");
        }
        notificationUtils = new NotificationUtils(context);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl)
    {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

}
