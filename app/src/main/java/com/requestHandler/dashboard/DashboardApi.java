package com.requestHandler.dashboard;

import com.localvalu.user.dashboard.request.HomePageRequest;
import com.localvalu.user.dashboard.response.HomePageResponse;
import com.localvalu.user.manageaddress.request.NewAddressRequest;
import com.localvalu.user.manageaddress.response.DeliveryAddressResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface DashboardApi
{
    @POST("Auth/HomePage")
    Observable<HomePageResponse> homePageContent(@Body HomePageRequest homePageRequest);

}
