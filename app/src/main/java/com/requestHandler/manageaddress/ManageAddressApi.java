package com.requestHandler.manageaddress;

import com.localvalu.user.manageaddress.request.AddressListRequest;
import com.localvalu.user.manageaddress.request.NewAddressRequest;
import com.localvalu.user.manageaddress.request.UpdateAddressRequest;
import com.localvalu.user.manageaddress.response.DeliveryAddressResponse;
import com.localvalu.user.manageaddress.response.OrderNowResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ManageAddressApi
{
    @POST("Order/DeliveryAddressDetailsInsert")
    Observable<DeliveryAddressResponse> addNewAddress(@Body NewAddressRequest newAddressRequest);

    @POST("Order/DeliveryAddressDetailsInsert")
    Observable<DeliveryAddressResponse> updateAddress(@Body UpdateAddressRequest updateAddressRequest);

    @POST("UserRegistration/OrderNow")
    Observable<OrderNowResponse> getDeliveryAddressList(@Body AddressListRequest addressListRequest);

    @POST("OrderLocal/ordernow")
    Observable<OrderNowResponse> getDeliveryAddressListLocal(@Body AddressListRequest addressListRequest);
}
