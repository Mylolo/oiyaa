package com.requestHandler.auth;

import com.localvalu.user.dashboard.request.HomePageRequest;
import com.localvalu.user.dashboard.response.HomePageResponse;
import com.localvalu.user.dto.forgetpassword.ForgetPasswordRequest;
import com.localvalu.user.dto.forgetpassword.ForgetPasswordResponse;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ForgetPasswordApi
{
    @POST("Local/forgetPassword")
    Single<ForgetPasswordResponse> forgetPassword(@Body ForgetPasswordRequest forgetPasswordRequest);
}
