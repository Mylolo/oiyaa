package com.requestHandler.search;

import com.localvalu.common.model.SearchRequest;
import com.localvalu.common.model.SearchResponse;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SearchApi
{
    @POST("Websitesearch/search")
    Single<SearchResponse> search(@Body SearchRequest searchRequest);

    @POST("Websitesearch/Cuisinesearch")
    Single<SearchResponse> searchFilter(@Body SearchRequest searchRequest);
}
