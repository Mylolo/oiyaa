package com.requestHandler.versioncheck;

import com.localvalu.common.model.appversion.VersionCheckRequest;
import com.localvalu.common.model.appversion.VersionCheckResponse;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface VersionCheckApi
{
    @POST("Webadmin/AppVersion")
    Single<VersionCheckResponse> checkVersion(@Body VersionCheckRequest versionCheckRequest);
}
