package com.requestHandler.placesapi;

import com.localvalu.placesapi.PlaceDetailsResponse;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface PlacesApi
{
    @POST("place/details/json?key=AIzaSyBOSV6yZbaq9O29T1ngplTKEeiTClqxzvE")
    Observable<PlaceDetailsResponse> getPlaceDetails(@Query("place_id")String placeId);

}
