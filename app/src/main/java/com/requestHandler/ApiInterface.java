package com.requestHandler;

import com.localvalu.common.model.cuisinescategory.CuisinesCategoryListResponse;
import com.localvalu.common.review.wrapper.ReviewPostResultWrapper;
import com.localvalu.directory.detail.wrapper.SaveRequestSlotResultWrapper;
import com.localvalu.directory.listing.wrapper.DashboardListResultWrapper;
import com.localvalu.directory.notification.wrapper.NotificationListResultWrapper;
import com.localvalu.directory.scanpay.wrapper.BusinessDetailResultWrapper;
import com.localvalu.directory.scanpay.wrapper.StarPrinterScanQRCodeResultWrapper;
import com.localvalu.eats.detail.wrapper.OrderNowResultResponse;
import com.localvalu.eats.listing.model.BusinessListResponse;
import com.localvalu.eats.myorder.dto.request.MyOrdersChangeOrderStatusRequest;
import com.localvalu.eats.myorder.dto.request.MyOrdersRequest;
import com.localvalu.eats.myorder.dto.request.MyOrdersSingleOrderDetailsRequest;
import com.localvalu.eats.myorder.dto.response.MyOrderSingleDetailsResponse;
import com.localvalu.eats.myorder.dto.response.MyOrdersChangeOrderStatusResponse;
import com.localvalu.eats.myorder.dto.response.MyOrdersResponse;
import com.localvalu.eats.myorder.wrapper.OrderCancelResultWrapper;
import com.localvalu.eats.myorder.wrapper.OrderListResultWrapper;
import com.localvalu.common.mytransaction.wrapper.UserDetailResultWrapper;
import com.localvalu.eats.payment.dto.OnlinePaymentResponseOne;
import com.localvalu.eats.payment.dto.OnlinePaymentResponseTwo;
import com.localvalu.eats.payment.dto.OnlinePaymentWrapper;
import com.localvalu.eats.payment.dto.VoucherPurchaseRequest;
import com.localvalu.eats.payment.dto.PaymentIntegrationApi;
import com.localvalu.eats.payment.dto.PaymentIntegrationEat;
import com.localvalu.eats.payment.dto.VoucherPurchaseResponse;
import com.localvalu.mall.dto.GetMallVoucherAmountRequest;
import com.localvalu.mall.dto.GetMallVoucherAmountResult;
import com.localvalu.mall.dto.MallMerchantSearch;
import com.localvalu.mall.dto.MallMerchantSearchResultResponse;
import com.localvalu.mall.dto.MallUserPurchaseVoucherDetails;
import com.localvalu.mall.dto.MallUserPurchaseVoucherResultResponse;
import com.localvalu.mall.dto.MallUserVoucherDetails;
import com.localvalu.mall.dto.MallVoucherInsert;
import com.localvalu.mall.dto.MallVoucherInsertUpdateResultResponse;
import com.localvalu.mall.myvouchers.dto.MallRetailersListResponse;
import com.localvalu.mall.myvouchers.dto.MallRetailersVoucherDetailsResponse;
import com.localvalu.mall.payment.dto.PaymentRequest;
import com.localvalu.mall.payment.dto.VoucherPurchaseOnlinePayResponse;
import com.localvalu.mall.payment.dto.request.OnlinePayMallVoucherPurchaseRequest;
import com.localvalu.mall.payment.dto.response.OnlinePayMallVoucherPurchaseResponse;
import com.localvalu.requestappointment.dto.RequestAppointmentCancelRequest;
import com.localvalu.requestappointment.dto.RequestAppointmentCancelResponse;
import com.localvalu.requestappointment.dto.RequestAppointmentListRequest;
import com.localvalu.requestappointment.dto.RequestAppointmentListResponse;
import com.localvalu.tablebooking.dto.TableBookingCancelRequest;
import com.localvalu.tablebooking.dto.TableBookingCancelResponse;
import com.localvalu.tablebooking.dto.TableBookingListRequest;
import com.localvalu.tablebooking.dto.TableBookingListResponse;
import com.localvalu.user.dto.PassionListRequest;
import com.localvalu.user.wrapper.MyAccountResultWrapper;
import com.localvalu.user.wrapper.PassionListResultWrapper;
import com.localvalu.user.wrapper.RegisterCustomerResultWrapper;
import com.localvalu.user.wrapper.RegisterExistsCustomerResultWrapper;
import com.localvalu.user.wrapper.UpdateCustomerResultWrapper;
import com.localvalu.user.wrapper.UserWrapper;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Developer on 23-02-2018.
 */

public interface ApiInterface {
    //  @FormUrlEncoded
    @POST("Auth/UserLogin")
    Call<UserWrapper> loginAPI(@Body RequestBody json);

    @POST("UserRegistration/UserRegistrationFirstStep")
    Call<RegisterExistsCustomerResultWrapper> userSignUpValidate(@Body RequestBody json);

    @POST("UserRegistration/IdlUserRegistration")
    Call<RegisterCustomerResultWrapper> userSignUpAPI(@Body RequestBody json);

    @POST("UserRegistration/UpdateUserDetails")
    Call<UpdateCustomerResultWrapper> userUpdateDetailsAPI(@Body RequestBody json);

    @POST("UserRegistration/GetUserDetailsForDashBoard")
    Call<MyAccountResultWrapper> myAccount(@Body RequestBody json);

    @POST("UserRegistration/GetUserDetailsForDashBoardDetailsByRequestFor")
    Call<UserDetailResultWrapper> getUserDetailFor(@Body RequestBody json);

    @POST("ScanQr/ScanAndPay")
    Call<StarPrinterScanQRCodeResultWrapper> scanPay(@Body RequestBody json);

    @POST("UserRegistration/GetBusinessInfo")
    Call<BusinessDetailResultWrapper> businessDetailFrom(@Body RequestBody json);

    @POST("Websitesearch/GetWebsiteComplete")
    Call<DashboardListResultWrapper> dashboardListResult(@Body RequestBody json);

    @POST("Websitesearch/cuisinesList")
    Call<CuisinesCategoryListResponse> cuisinesCategoryList(@Body RequestBody json);

    @POST("LoloWebsiteCompleteHomePage/GetLoloWebsiteNearBy")
    Call<BusinessListResponse> dashboardNearByResult(@Body RequestBody json);

    @POST("Local/requestSlot")
    Call<SaveRequestSlotResultWrapper> requestAppointment(@Body RequestBody json);

    @POST("UserRegistration/requestSlot")
    Call<SaveRequestSlotResultWrapper> requestCallback(@Body RequestBody json);

    @POST("UserRegistration/Review")
    Call<ReviewPostResultWrapper> review(@Body RequestBody json);

    @POST("UserRegistration/requestSlotDetail")
    Call<SaveRequestSlotResultWrapper> bookAppointment(@Body RequestBody json);

    /**************** Order For Eats Starts *************************/

    @POST("order/OrderNow")
    Call<OrderNowResultResponse> orderNow(@Body RequestBody json);

    @POST("order/orderTablenow")
    Call<OrderNowResultResponse> orderNowTable(@Body RequestBody json);

    @POST("order/OrderTableNow")
    Call<OrderNowResultResponse> addToCartTable(@Body RequestBody json);

    @POST("UserRegistration/OrderNow")
    Call<OrderNowResultResponse> orderNowPayment(@Body RequestBody json);

    //Order now module at restaurant or cash on delivery
    @POST("Order/placeOrderSecond")
    Call<OnlinePaymentWrapper> cashOnDelivery(@Body PaymentIntegrationApi paymentIntegrationApi);

    //Order at table module at restaurant
    @POST("Order/placeOrderTableSecond")
    Call<OnlinePaymentWrapper> orderAtTableManualPayment(@Body PaymentIntegrationApi paymentIntegrationApi);

    /**************** Order For Eats Ends *************************/

    /**************** Order For Local Starts *************************/

    @POST("OrderLocal/ordernow")
    Call<OrderNowResultResponse> orderOnlineLocal(@Body RequestBody json);

    @POST("OrderLocal/OrderTableNow")
    Call<OrderNowResultResponse> orderAtStoreLocal(@Body RequestBody json);

    @POST("OrderLocal/placeOrderSecond")
    Call<OnlinePaymentWrapper> orderOnlineLocalCOD(@Body PaymentIntegrationApi paymentIntegrationApi);

    @POST("OrderLocal/placeOrderTableSecond")
    Call<OnlinePaymentWrapper> orderAtStoreLocalCOD(@Body PaymentIntegrationApi paymentIntegrationApi);


    /**************** Order For Local Ends *************************/


    //Mall start
    @POST("webapi/api/MallMerchantSearch")
    Call<MallMerchantSearchResultResponse> getMallMerchantSearch(@Body MallMerchantSearch mallMerchantSearch);

    @POST("webapi/api/mallUserPurchaseVoucherDetails")
    Call<MallUserPurchaseVoucherResultResponse> getmallUserPurchaseVoucherDetails(@Body MallUserPurchaseVoucherDetails mallUserPurchaseVoucherDetails);

    @POST("webapi/api/MallVoucherInsert")
    Call<MallVoucherInsertUpdateResultResponse> getMallVoucherInsert(@Body MallVoucherInsert mallVoucherInsert);

    @POST("webapi/api/MallUserVoucherDetails")
    Call<MallUserVoucherDetails> getMallUserVoucherDetails(@Body MallMerchantSearch mallMerchantSearch);

    @POST("webapi/api/MallRetailersVoucherDetails")
    Call<MallRetailersVoucherDetailsResponse> getMallRetailersVoucherDetails(@Body RequestBody json);

    @POST("webapi/api/MallRetailersList")
    Call<MallRetailersListResponse> getMallRetailersList(@Body RequestBody json);

    @POST("UserRegistration/OnlinePayMallVoucherPurchase")
    Call<VoucherPurchaseOnlinePayResponse> getOnlinePayMallVoucherPurchase(@Body PaymentRequest paymentRequest);

    @POST("webapi/api/GetMallVoucherAmount")
    Call<GetMallVoucherAmountResult> getMallVoucherAmount(@Body GetMallVoucherAmountRequest getMallVoucherAmountRequest);


    //Mall end

    // This logic has been commented in old state
    @POST("cart/orders")
    Call<OrderListResultWrapper> getOrderListsResult(@Body RequestBody json);

    @POST("cart/cancel_order")
    Call<OrderCancelResultWrapper> setOrderCancelResult(@Body RequestBody json);

    @POST("notify/user_notify_pref")
    Call<NotificationListResultWrapper> getNotificationListsResult(@Body RequestBody json);

    @POST("notify/save_user_prefs")
    Call<NotificationListResultWrapper> setNotificationListsResult(@Body RequestBody json);

    //Eats OnlinePayment
    @POST("order/placeOrderOne")
    Call<OnlinePaymentResponseOne> startOnlinePayment(@Body PaymentIntegrationEat paymentIntegrationEat);

    //Eats OnlinePayment for order at table
    @POST("order/placeOrderTableOne")
    Call<OnlinePaymentResponseOne> startOnlinePaymentTable(@Body PaymentIntegrationEat paymentIntegrationEat);

    //Local OnlinePayment
    @POST("OrderLocal/placeOrderOne")
    Call<OnlinePaymentResponseOne> startOnlinePaymentLocal(@Body PaymentIntegrationEat paymentIntegrationEat);

    //Local OnlinePayment for order at store
    @POST("OrderLocal/placeOrderTableOne")
    Call<OnlinePaymentResponseOne> startOnlinePaymentLocalStore(@Body PaymentIntegrationEat paymentIntegrationEat);

    //Online paymall voucher hpp request
    @POST("Voucher/OnlinePayMallVoucherHPPRequest")
    Call<VoucherPurchaseResponse> requestForVoucherPurchase(@Body VoucherPurchaseRequest voucherPurchaseRequest);

    //Eats OnlinePayment finish
    @POST("order/placeOrderSecond")
    Call<OnlinePaymentResponseTwo> finishOnlinePayment(@Body PaymentIntegrationEat paymentIntegrationEat);

    //Eats OnlinePayment OrderAtTable finish
    @POST("order/placeOrderTableSecond")
    Call<OnlinePaymentResponseTwo> finishOnlinePaymentTable(@Body PaymentIntegrationEat paymentIntegrationEat);

    //Local OnlinePayment finish
    @POST("OrderLocal/placeOrderSecond")
    Call<OnlinePaymentResponseTwo> finishOnlinePaymentLocal(@Body PaymentIntegrationEat paymentIntegrationEat);

    //Local OnlinePayment finish for order at store
    @POST("OrderLocal/placeOrderTableSecond")
    Call<OnlinePaymentResponseTwo> finishOnlinePaymentLocalStore(@Body PaymentIntegrationEat paymentIntegrationEat);


    //Mall OnlinePayment finish
    @POST("Voucher/OnlinePayMallVoucherPurchase")
    Call<OnlinePayMallVoucherPurchaseResponse> finishOnlinePaymentMall(@Body OnlinePayMallVoucherPurchaseRequest onlinePayMallVoucherPurchaseRequest);

    //My OrdersList
    @POST("UserRegistration/GetUserDetailsForDashBoardDetailsByRequestFor")
    Call<MyOrdersResponse> getMyOrders(@Body MyOrdersRequest myOrdersRequest);

    //My OrdersDetails
    @POST("UserRegistration/GetUserDetailsForDashBoardDetailsByRequestFor")
    Call<MyOrderSingleDetailsResponse> getOrderDetails(@Body MyOrdersSingleOrderDetailsRequest myOrdersSingleOrderDetailsRequest);

    //My Orders Change Order Status
    @POST("UserRegistration/GetUserDetailsForDashBoardDetailsByRequestFor")
    Call<MyOrdersChangeOrderStatusResponse> changeOrderStatus(@Body MyOrdersChangeOrderStatusRequest myOrdersChangeOrderStatusRequest);

    //Get PassionsList
    @POST("UserRegistration/GetPassionsList")
    Call<PassionListResultWrapper> getPassionList(@Body PassionListRequest passionListRequest);

    //Get TableBookingList
    @POST("UserRegistration/TableBookingDetail")
    Call<TableBookingListResponse> getTableBookingList(@Body TableBookingListRequest tableBookingListRequest);

    //Get Request Appointment List
    @POST("UserRegistration/requestSlotDetail")
    Call<RequestAppointmentListResponse> getRequestAppointmentList(@Body RequestAppointmentListRequest requestAppointmentListRequest);

    //Cancel TableBooking
    @POST("UserRegistration/TableBookingCancel")
    Call<TableBookingCancelResponse> cancelTableBooking(@Body TableBookingCancelRequest tableBookingCancelRequest);

    //Cancel Appointment
    @POST("UserRegistration/slotBookingCancel")
    Call<RequestAppointmentCancelResponse> cancelRequestAppointment(@Body RequestAppointmentCancelRequest requestAppointmentCancelRequest);

}
