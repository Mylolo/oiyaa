package com.requestHandler.payment;

import com.localvalu.eats.payment.dto.Pay360Request;
import com.localvalu.eats.payment.dto.Pay360Response;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PaymentApi
{
    @POST("Pay360/CreatePayment")
    Single<Pay360Response> startPayment(@Body Pay360Request pay360Request);
}
