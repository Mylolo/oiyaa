package com.requestHandler;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientForUpload {
    public static final String BASE_URL_RETROFIT = "http://vtdesignz.co/dev/ci/viral/api/";

    public static Retrofit retrofit = null;
    public static HttpLoggingInterceptor interceptor = null;
    public static OkHttpClient client = null;
    public static Gson gson;

    /**
     * ThiS Class Will Give an Instance of Retrofit
     */
    public static Retrofit getApiClient() {

        if (interceptor == null) {
            interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }
        if (client == null) {
            client = new OkHttpClient.Builder().addInterceptor(interceptor)
                    .connectTimeout(10, TimeUnit.MINUTES)
                    .writeTimeout(10, TimeUnit.MINUTES)
                    .readTimeout(10, TimeUnit.MINUTES).build();
        }
        if (gson == null) {
            gson = new GsonBuilder()
                    .setLenient()
                    .create();
        }

        Log.d("Retrofit", "Creating Instance");
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL_RETROFIT)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }
}
