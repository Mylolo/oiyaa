package com.requestHandler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.localvalu.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient
{
    //public static final String BASE_URL_RETROFIT_NEW = "https://www.localvalu.com/webservice/api/";
    public static final String BASE_URL_RETROFIT_NEW = BuildConfig.BASE_URL;
    public static final String BASE_URL_RETROFIT_MALL = BuildConfig.BASE_URL_MALL;
    public static final String BASE_URL_GOOGLE_PLACES = "https://maps.googleapis.com/maps/api/";

    public static Retrofit retrofitNew = null;
    public static Retrofit retrofitMall = null;
    public static Retrofit retrofitGooglePlaces = null;
    public static HttpLoggingInterceptor interceptor = null;
    public static OkHttpClient client = null;
    public static Gson gson;

    public static Retrofit getApiClientNew()
    {

        if (interceptor == null)
        {
            interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }

        if (client == null)
        {
            if(BuildConfig.DEBUG)
            {
                client = new OkHttpClient.Builder().addInterceptor(interceptor)
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS).build();
            }
            else
            {
                client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS).build();
            }

        }

        if (gson == null)
        {
            gson = new GsonBuilder()
                    .setLenient()
                    .create();
        }

        if (retrofitNew == null)
        {
            retrofitNew = new Retrofit.Builder()
                    .baseUrl(BASE_URL_RETROFIT_NEW)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }

        return retrofitNew;
    }

    public static Retrofit getApiClientMall()
    {
        if (interceptor == null)
        {
            interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }

        if (client == null)
        {
            if(BuildConfig.DEBUG)
            {
                client = new OkHttpClient.Builder().addInterceptor(interceptor)
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS).build();
            }
            else
            {
                client = new OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS).build();
            }
        }

        if (gson == null)
        {
            gson = new GsonBuilder()
                    .setLenient()
                    .create();
        }

        if (retrofitMall == null)
        {
            retrofitMall = new Retrofit.Builder()
                    .baseUrl(BASE_URL_RETROFIT_MALL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }

        return retrofitMall;
    }

    public static Retrofit getApiClientGooglePlaces()
    {
        if (interceptor == null)
        {
            interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }

        if (client == null)
        {
            client = new OkHttpClient.Builder().addInterceptor(interceptor)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS).build();
        }

        if (gson == null)
        {
            gson = new GsonBuilder()
                    .setLenient()
                    .create();
        }

        if (retrofitMall == null)
        {
            retrofitMall = new Retrofit.Builder()
                    .baseUrl(BASE_URL_GOOGLE_PLACES)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }

        return retrofitMall;
    }
}
